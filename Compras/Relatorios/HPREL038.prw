#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL038 
Relat�rio Rateio por Centro de Custos
@author Ronaldo Pereira
@since 06/11/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL038()

Private oReport
Private cPergCont	:= 'HPREL038' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Ronaldo Pereira
@since 06 de Novembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
	
Static Function ReportDef()

	Local oReport
	Local oSection1

	oReport := TReport():New( 'RCC', 'ENTRADA - RATEIO POR CENTRO DE CUSTO', , {|oReport| ReportPrint( oReport ), 'ENTRADA - RATEIO POR CENTRO DE CUSTO' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'ENTRADA - RATEIO POR CENTRO DE CUSTO', {'RCC','SDE','SF1','SD1','SA2','CTT','SED'})
	
	TRCell():New( oSection1, 'DATA_DIGITACAO'	    ,'RCC', 		'DATA_DIGITACAO',					"@!"                        ,15)		
	TRCell():New( oSection1, 'FILIAL'	    	    ,'RCC', 		'FILIAL',							"@!"                        ,04)
	TRCell():New( oSection1, 'DOCUMENTO'			,'RCC', 		'DOCUMENTO',       					"@!"		                ,15)
	TRCell():New( oSection1, 'COD_FORNEC'			,'RCC', 	    'COD_FORNEC',       				"@!"		                ,09)
	TRCell():New( oSection1, 'FORNCECEDOR' 		    ,'RCC', 		'FORNCECEDOR',              		"@!"						,60)
	TRCell():New( oSection1, 'NATUREZA' 		    ,'RCC', 		'NATUREZA',              			"@!"						,15)
	TRCell():New( oSection1, 'ITEM'         		,'RCC', 		'ITEM',			           		    "@!"						,06)
	TRCell():New( oSection1, 'PRODUTO'         		,'RCC', 		'PRODUTO',			           		"@!"						,15)
	TRCell():New( oSection1, 'PERCENTUAL'         	,'RCC', 	    'PERCENTUAL',			           	"@E 999,99.99"				,15)
	TRCell():New( oSection1, 'VALOR' 				,'RCC', 	    'VALOR',     						"@E 999,99.9999"		    ,15)
	TRCell():New( oSection1, 'COD_CC' 				,'RCC', 	    'COD_CC',     						"@!"		    		    ,15)
	TRCell():New( oSection1, 'CENTRO_CUSTOS'      	,'RCC', 		'CENTRO_CUSTOS',      				"@!"				    	,45)
		
Return( oReport )
	
//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Ronaldo Pereira
@since 06 de Novembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("RCC") > 0
		RCC->(dbCloseArea())
	Endif         
    
    cQuery := " SELECT CONVERT(CHAR, CAST(F1.F1_DTDIGIT AS SMALLDATETIME), 103) AS DATA_DIGITACAO, "
    cQuery += "        DE.DE_FILIAL AS FILIAL, "
    cQuery += "        DE.DE_DOC AS DOCUMENTO, "
    cQuery += "        DE.DE_FORNECE AS COD_FORNEC, "
    cQuery += "        A2.A2_NOME AS FORNCECEDOR, "
    cQuery += " 	   ED.ED_DESCRIC AS NATUREZA, "
    cQuery += "        DE.DE_ITEMNF AS ITEM, "
    cQuery += "        D1.D1_COD AS PRODUTO, "
    cQuery += "        DE.DE_PERC AS PERCENTUAL, "
    cQuery += "        ((DE.DE_PERC * D1.D1_TOTAL)/100) AS VALOR, "
    cQuery += "        DE.DE_CC AS COD_CC, "
    cQuery += "        CC.CTT_DESC01 AS CENTRO_CUSTOS "
    cQuery += " FROM "+RetSqlName("SDE")+" DE WITH (NOLOCK) "
    cQuery += " INNER JOIN "+RetSqlName("SF1")+" F1 WITH (NOLOCK) ON (F1.F1_FILIAL = DE.DE_FILIAL "
    cQuery += "                                        AND F1.F1_DOC = DE.DE_DOC "
    cQuery += "                                        AND F1.F1_FORNECE = DE.DE_FORNECE "
    cQuery += "                                        AND F1.F1_LOJA = DE.DE_LOJA "
    cQuery += "                                        AND F1.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SD1")+" D1 WITH (NOLOCK) ON (D1.D1_FILIAL = DE.DE_FILIAL "
    cQuery += "                                        AND D1.D1_DOC = DE.DE_DOC "
    cQuery += "                                        AND D1.D1_FORNECE = DE.DE_FORNECE "
    cQuery += "                                        AND D1.D1_LOJA = DE.DE_LOJA "
    cQuery += "                                        AND D1.D1_ITEM = DE.DE_ITEMNF "
    cQuery += "                                        AND D1.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SA2")+" A2 WITH (NOLOCK) ON (A2.A2_COD = DE.DE_FORNECE "
    cQuery += "                                        AND A2.A2_LOJA = DE.DE_LOJA "
    cQuery += "                                        AND A2.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("CTT")+" CC WITH (NOLOCK) ON (CC.CTT_CUSTO = DE.DE_CC "
    cQuery += "                                        AND CC.CTT_FILIAL = '01' "
    cQuery += "                                        AND CC.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SED")+" ED WITH (NOLOCK) ON (ED.ED_CODIGO = A2.A2_NATUREZ "
    cQuery += "                                        AND ED.D_E_L_E_T_ = '') "
    cQuery += " WHERE DE.D_E_L_E_T_ = '' "
    cQuery += "   AND DE.DE_FILIAL = '0101' "
    cQuery += "   AND F1.F1_DTDIGIT BETWEEN '"+ DTOS(MV_PAR01) +"'  AND '"+ DTOS(MV_PAR02) + "' "
    
    IF !EMPTY(MV_PAR03)	
		cQuery += " AND E2_NUMDE.DE_DOC = '"+MV_PAR03+"' "	
	ENDIF
    
    cQuery += " ORDER BY F1.F1_DTDIGIT, "
    cQuery += "          DE.DE_DOC, "
    cQuery += " 		 DE.DE_FORNECE, "
    cQuery += "          DE.DE_ITEMNF "
       	
       	
	TCQUERY cQuery NEW ALIAS RCC
	
	While RCC->(!EOF())
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()
		
		oSection1:Cell("DATA_DIGITACAO"):SetValue(RCC->DATA_DIGITACAO)
		oSection1:Cell("DATA_DIGITACAO"):SetAlign("LEFT")

		oSection1:Cell("FILIAL"):SetValue(RCC->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")

		oSection1:Cell("DOCUMENTO"):SetValue(RCC->DOCUMENTO)
		oSection1:Cell("DOCUMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_FORNEC"):SetValue(RCC->COD_FORNEC)
		oSection1:Cell("COD_FORNEC"):SetAlign("LEFT")		
					
		oSection1:Cell("FORNCECEDOR"):SetValue(RCC->FORNCECEDOR)
		oSection1:Cell("FORNCECEDOR"):SetAlign("LEFT")
		
		oSection1:Cell("NATUREZA"):SetValue(RCC->NATUREZA)
		oSection1:Cell("NATUREZA"):SetAlign("LEFT")
		
		oSection1:Cell("ITEM"):SetValue(RCC->ITEM)
		oSection1:Cell("ITEM"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTO"):SetValue(RCC->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("PERCENTUAL"):SetValue(RCC->PERCENTUAL)
		oSection1:Cell("PERCENTUAL"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR"):SetValue(RCC->VALOR)
		oSection1:Cell("VALOR"):SetAlign("LEFT")
		
		oSection1:Cell("COD_CC"):SetValue(RCC->COD_CC)
		oSection1:Cell("COD_CC"):SetAlign("LEFT")

		oSection1:Cell("CENTRO_CUSTOS"):SetValue(RCC->CENTRO_CUSTOS)
		oSection1:Cell("CENTRO_CUSTOS"):SetAlign("LEFT")
												
		oSection1:PrintLine()
		
		RCC->(DBSKIP()) 
	enddo
	RCC->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Ronaldo Pereira
@since 06 de Novembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Data Digita��o de ?"		        		,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Data Digita��o ate ?"                       ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Documento ?"		        				,""		,""		,"mv_ch3","C",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	
	
return