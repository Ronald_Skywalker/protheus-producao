
//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio Tabela de pre�o Fornecedor.
Rotina para analise de manuten��o de pre�o;

@author Ronaldo Pereira
@since 19 de Outubro de 2021
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL126()

	Local aRet  := {}
	Local aPerg := {}

	aAdd(aPerg,{1,"Cod. Fornecedor: " ,Space(6),""		,""		,"SA2"	,""		,0	,.F.})
    aAdd(aPerg,{1,"Cod. Tabela: " 	  ,Space(3),""		,""		,"AIA"	,""		,0	,.F.})
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    	
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'TABELA PRECO'+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Base"
    Local cTable1    	:= "Tabela de Pre�o Fornecedor" 


    //Dados tabela 1          
    cQry := " SELECT IA.AIA_FILIAL AS FILIAL, "
    cQry += "        IA.AIA_CODFOR AS COD_FORN, "
    cQry += "        A2.A2_NOME AS FORNECEDOR, "
    cQry += "        IA.AIA_CODTAB AS TABELA, "
    cQry += "        IA.AIA_DESCRI AS DESC_TABELA, "
    cQry += "        CONVERT(CHAR, CAST(IA.AIA_DATDE AS SMALLDATETIME), 103) AS DT_VALID_INI, "
    cQry += "        CONVERT(CHAR, CAST(IA.AIA_DATATE AS SMALLDATETIME), 103) AS DT_VALID_FIM, "
    cQry += "        IA.AIA_CONDPG AS COND_PGTO, "
    cQry += "        IB.AIB_ITEM AS ITEM, "
    cQry += "        IB.AIB_CODPRO AS PRODUTO, "
    cQry += "        B4.B4_DESC AS DESCRICAO, "
    cQry += "        LEFT(AIB_CODPRO,8) AS REFERENCIA, "
    cQry += "        ISNULL(BV_DESCRI,'-') AS COR, "
    cQry += "        CASE WHEN RIGHT(IB.AIB_CODPRO,4) LIKE '000%' THEN REPLACE(RIGHT(IB.AIB_CODPRO,4),'000','') "
    cQry += "                WHEN RIGHT(IB.AIB_CODPRO,4) LIKE '00%' THEN REPLACE(RIGHT(IB.AIB_CODPRO,4),'00','') "
    cQry += "                WHEN RIGHT(IB.AIB_CODPRO,4) LIKE '0%' THEN SUBSTRING(RIGHT(IB.AIB_CODPRO,4),2,4) "
    cQry += "        ELSE REPLACE(RIGHT(IB.AIB_CODPRO,4),'00','') END TAMANHO, "
    cQry += "        IB.AIB_PRCCOM AS PRECO, "
    cQry += "        IB.AIB_QTDLOT AS FAIXA, "
    cQry += "        IB.AIB_MOEDA AS MOEDA, "
    cQry += "        CONVERT(CHAR, CAST(IB.AIB_DATVIG AS SMALLDATETIME), 103) AS DTA_VIG_PRODUTO, "
    cQry += "        IB.AIB_FRETE AS FRETE "
    cQry += "    FROM "+RetSqlName("AIA")+" AS IA WITH (NOLOCK) " 
    cQry += "    INNER JOIN "+RetSqlName("AIB")+" AS IB WITH (NOLOCK) ON (IB.AIB_FILIAL = IA.AIA_FILIAL "
    cQry += "                                        AND IB.AIB_CODFOR = IA.AIA_CODFOR "
    cQry += "                                        AND IB.AIB_CODTAB = IA.AIA_CODTAB "
    cQry += "                                        AND IB.D_E_L_E_T_ = '') "
    cQry += "    INNER JOIN "+RetSqlName("SA2")+" AS A2 WITH (NOLOCK) ON (A2.A2_COD = IA.AIA_CODFOR "
    cQry += "                                        AND A2.D_E_L_E_T_ = '') "
    cQry += "    INNER JOIN "+RetSqlName("SB4")+" AS B4 WITH (NOLOCK) ON (B4.B4_COD = LEFT(IB.AIB_CODPRO,8) "
    cQry += "                                        AND B4.D_E_L_E_T_ = '') "
    cQry += "    LEFT  JOIN "+RetSqlName("SBV")+" AS BV WITH (NOLOCK) ON (SUBSTRING(IB.AIB_CODPRO,9,3) = BV_CHAVE " 
    cQry += "                                        AND BV_TABELA = 'COR' "
    cQry += "                                        AND BV.D_E_L_E_T_ = '') "
    cQry += "    WHERE IA.D_E_L_E_T_ = '' "

    If !Empty(aRet[1])
    	cQry += "  AND IA.AIA_CODFOR = '"+Alltrim(aRet[1])+"' "
    EndIf

    If !Empty(aRet[2])
    	cQry += "  AND IA.AIA_CODTAB = '"+Alltrim(aRet[2])+"' "
    EndIf

    cQry += "    ORDER BY IA.AIA_FILIAL, "
    cQry += "             IA.AIA_CODFOR, "
    cQry += "             IA.AIA_CODTAB, "
    cQry += "             IB.AIB_CODPRO "
                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_FORN",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FORNECEDOR",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TABELA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESC_TABELA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_VALID_INI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_VALID_FIM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COND_PGTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ITEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRODUTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCRICAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"REFERENCIA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COR",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TAMANHO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRECO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FAIXA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MOEDA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DTA_VIG_PRODUTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FRETE",1)
        
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;                                             
                                               FILIAL,;
                                               COD_FORN,;
                                               FORNECEDOR,;
                                               TABELA,;
                                               DESC_TABELA,;
                                               DT_VALID_INI,;
                                               DT_VALID_FIM,;
                                               COND_PGTO,;
                                               ITEM,;
                                               PRODUTO,;
                                               DESCRICAO,;
                                               REFERENCIA,;
                                               COR,;
                                               TAMANHO,; 
                                               PRECO,; 
                                               FAIXA,;
                                               MOEDA,;
                                               DTA_VIG_PRODUTO,;                                             
                                               FRETE;                                               
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return
