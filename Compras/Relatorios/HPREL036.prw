#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

/*/{Protheus.doc} HPREL036 
Relat�rio de Followup do Fornecedor 
@author T.I - HOPE
@since 10/11/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL036()

Private oReport
Private cPergCont	:= 'HPREL036' 

/************************
*Monta pergunte do Log *
************************/

//AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
Endif

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 10 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	//Local oSection2

	oReport := TReport():New( 'FLW', 'FOLLOW-UP DE COMPRAS', , {|oReport| ReportPrint( oReport ), 'FOLLOW-UP DE COMPRAS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'FOLLOW-UP DE COMPRAS', { 'FLW', 'SC1','SB4','CTT','SBV','SBM','SC7','SA2'})
	
	TRCell():New( oSection1, 'CODFORSC'					,'FLW', 		'CODFORSC',		  			        "@!"				        ,06)		
	TRCell():New( oSection1, 'FORNE_SC'					,'FLW', 		'FORNE_SC',		  			        "@!"				        ,35)		
	TRCell():New( oSection1, 'CODFORPED'				,'FLW', 		'CODFORPED',		  				"@!"				        ,06)		
	TRCell():New( oSection1, 'FORNE_PED'				,'FLW', 		'FORNE_PED',		  			    "@!"				        ,35)		
	TRCell():New( oSection1, 'UF'						,'FLW', 		'UF',		  			    		"@!"				        ,02)
	TRCell():New( oSection1, 'FILIAL_PEDIDO'			,'FLW', 		'FILIAL_PEDIDO',				    "@!"                        ,10)
	TRCell():New( oSection1, 'FILIAL_ENTREGA'			,'FLW', 		'FILIAL_ENTREGA',	  			    "@!"				        ,10)
	TRCell():New( oSection1, 'TIPO'						,'FLW', 		'TIPO',				  			    "@!"				        ,04)
	TRCell():New( oSection1, 'SKU'						,'FLW', 		'SKU',				  			    "@!"				        ,15)
	TRCell():New( oSection1, 'PRODUTO'					,'FLW', 		'PRODUTO',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'DESCRICAO'				,'FLW', 		'DESCRICAO',		  			    "@!"				        ,35)
	TRCell():New( oSection1, 'GRUPO_PRODUTO'			,'FLW', 		'GRUPO_PRODUTO',			  		"@!"				        ,30)
	TRCell():New( oSection1, 'COD_COR'					,'FLW', 		'COD_COR',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DESC_COR'					,'FLW', 		'DESC_COR',			  			    "@!"				        ,35)
	TRCell():New( oSection1, 'TAM'						,'FLW', 		'TAM',				  			    "@!"				        ,06)
	TRCell():New( oSection1, 'UNID_MEDIDA'				,'FLW', 		'UNID_MEDIDA',		  			    "@!"				        ,04)
	TRCell():New( oSection1, 'SOLICITACAO'				,'FLW', 		'SOLICITACAO',		  			    "@!"				        ,10)
	TRCell():New( oSection1, 'SC_COMPRADOR'				,'FLW', 		'SC_COMPRADOR',		  			    "@!"				        ,20)
	TRCell():New( oSection1, 'SOLICITANTE'				,'FLW', 		'SOLICITANTE',		  			    "@!"				        ,25)
	TRCell():New( oSection1, 'DAT_EMISSAO_SC'			,'FLW', 		'DAT_EMISSAO_SC',     			    ""							,08)
	TRCell():New( oSection1, 'QTDE_SOLICITADA'			,'FLW', 		'QTDE_SOLICITADA',	  			    "@E 999,999,999.999999"     ,16)

	TRCell():New( oSection1, 'TIPO_REQUISICAO'			,'FLW', 		'TIPO_REQUISICAO',	  				"@!"				        ,03)
	TRCell():New( oSection1, 'COLECAO' 					,'FLW', 		'COLECAO',		  		   			"@!"				        ,50)
	TRCell():New( oSection1, 'SUB_COLECAO' 				,'FLW', 		'SUB_COLECAO',		  				"@!"				        ,50)
	TRCell():New( oSection1, 'DAT_NEC_DEMANDA' 			,'FLW', 		'DAT_NEC_DEMANDA',			 	 	""					        ,08)
	TRCell():New( oSection1, 'QUANT_MOSTRUARIO' 		,'FLW', 		'QUANT_MOSTRUARIO',	   				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'DT_NEC_MOSTRUARIO'		,'FLW',		 	'DT_NEC_MOSTRUARIO',				""   				        ,08)

	TRCell():New( oSection1, 'PEDIDO'					,'FLW', 		'PEDIDO',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'PED_COMPRADOR'			,'FLW', 		'PED_COMPRADOR',		  			"@!"					    ,20)
	TRCell():New( oSection1, 'STATUS_APROVACAO'			,'FLW', 		'STATUS_APROVACAO',	  				"@!"				        ,06)
	//TRCell():New( oSection1, 'FORNECEDOR'				,'FLW', 		'FORNECEDOR',		  			  	"@!"				        ,35)
	TRCell():New( oSection1, 'CCUSTO'					,'FLW', 		'CCUSTO',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DT_EMISSAO_PED'			,'FLW', 		'DT_EMISSAO_PED',	  				""  				        ,08)
	TRCell():New( oSection1, 'COND_PGTO'				,'FLW', 		'COND_PGTO',			  			"@!"				        ,05)
	TRCell():New( oSection1, 'DESC_COND_PGTO'			,'FLW', 		'DESC_COND_PGTO',			  		"@!"				        ,50)
	TRCell():New( oSection1, 'VLR_TOTAL_ITEM'			,'FLW', 		'VLR_TOTAL_ITEM',	  				"@E 999,999,999.99"		    ,14)
	TRCell():New( oSection1, 'QTDE_PEDIDO'				,'FLW', 		'QTDE_PEDIDO',		  				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'DATA_ENTREGA'				,'FLW', 		'DATA_ENTREGA',		  				""				            ,08)
	TRCell():New( oSection1, 'QTDE_ENTREGUE'			,'FLW', 		'QTDE_ENTREGUE',	  				"@E 999,999,999.999999"	    ,16)
	TRCell():New( oSection1, 'SALDO'					,'FLW', 		'SALDO',			  			    "@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'CUSTO'					,'FLW', 		'CUSTO',			  			    "@E 99,999.99"		       	,08)
	TRCell():New( oSection1, 'QUITADO'					,'FLW', 		'QUITADO',			  			 	"@!"				     	,06)
	TRCell():New( oSection1, 'QTDE_DEVOLVIDA'			,'FLW', 		'QTDE_DEVOLVIDA',	  				"@E 999,999,999.999999"	    ,16)
	TRCell():New( oSection1, 'NF_ORIGEM_DEV'			,'FLW', 		'NF_ORIGEM_DEV',			  		"@!"				     	,60)
	TRCell():New( oSection1, 'NF_DEVOLUCAO'				,'FLW', 		'NF_DEVOLUCAO',			  			"@!"				     	,60)
	TRCell():New( oSection1, 'ENCERRADO'				,'FLW', 		'ENCERRADO',			  			"@!"				       	,01)

    // Nota 1
	TRCell():New( oSection1, 'NOTA_1FISCAL'				,'FLW', 		'NOTA_1FISCAL',		  			    "@!"				        ,09)
	TRCell():New( oSection1, 'DATA_1FATURA'				,'FLW', 		'DATA_1FATURA',		  			    ""				        	,08)
	TRCell():New( oSection1, 'QTDE_1FATURA'				,'FLW', 		'QTDE_1FATURA',		  			    "@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_1'					,'FLW', 		'MODAL_1',				  		    "@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_1'					,'FLW', 		'OBSERV_1',			  		   	    "@!"				        ,90)
	TRCell():New( oSection1, 'PREV_1RECEBIMENTO'		,'FLW', 		'PREV_1RECEBIMENTO',	  		    ""				        	,08)
	TRCell():New( oSection1, 'SALDO_1'					,'FLW', 		'SALDO_1',				  		    "@E 999,999,999.999999"	    ,16)
	TRCell():New( oSection1, 'DATA_1SALDO'				,'FLW', 		'DATA_1SALDO',			  		    ""				        	,08)
	
	// Nota 2
	TRCell():New( oSection1, 'NOTA_2FSCAL'				,'FLW', 		'NOTA_2FISCAL',		  			    "@!"				        ,09)
	TRCell():New( oSection1, 'DATA_2FATURA'				,'FLW', 		'DATA_2FATURA',		  			    ""				        	,08)
	TRCell():New( oSection1, 'QTDE_2FATURA'				,'FLW', 		'QTDE_2FATURA',		  			    "@E 999,999,999.999999"	    ,16)
	TRCell():New( oSection1, 'MODAL_2'					,'FLW', 		'MODAL_2',				  		    "@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_2'					,'FLW', 		'OBSERV_2',			  		    	"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_2RECEBIMENTO'		,'FLW', 		'PREV_2RECEBIMENTO',	  		    ""				        	,08)
	TRCell():New( oSection1, 'SALDO_2'					,'FLW', 		'SALDO_2',				  		    "@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'DATA_2SALDO'				,'FLW', 		'DATA_2SALDO',			  		    ""				        	,08)
	
	// Nota 3
	TRCell():New( oSection1, 'NOTA_3FISCAL'				,'FLW', 		'NOTA_3FISCAL'	,		  		   	"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_3FATURA'				,'FLW', 		'DATA_3FATURA',			  		    ""				        	,08)
	TRCell():New( oSection1, 'QTDE_3FATURA'				,'FLW', 		'QTDE_3FATURA',			  		    "@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_3'					,'FLW', 		'MODAL_3',				  			"@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_3'					,'FLW', 		'OBSERV_3',			  		   		"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_3RECEBIMENTO'		,'FLW', 		'PREV_3RECEBIMENTO',	  			""				        	,08)
	TRCell():New( oSection1, 'SALDO_3'					,'FLW', 		'SALDO_3',				  			"@E 999,999,999.999999"	    ,16)
	TRCell():New( oSection1, 'DATA_3SALDO'				,'FLW', 		'DATA_3SALDO',			  			""				       		,08)
	
	// Nota 4
	TRCell():New( oSection1, 'NOTA_4FISCAL'				,'FLW', 		'NOTA_4FISCAL',		  				"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_4FATURA'				,'FLW', 		'DATA_4FATURA',		  				""				      		,08)
	TRCell():New( oSection1, 'QTDE_4FATURA'				,'FLW', 		'QTDE_4FATURA',		  				"@E 999,999,999.999999"	 	,16)
	TRCell():New( oSection1, 'MODAL_4'					,'FLW', 		'MODAL_4',				  			"@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_4'					,'FLW', 		'OBSERV_4',			  		   		"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_4RECEBIMENTO'		,'FLW', 		'PREV_4RECEBIMENTO',	  			""				        	,08)
	TRCell():New( oSection1, 'SALDO_4'					,'FLW', 		'SALDO_4',				  			"@E 999,999,999.999999"		,09)
	TRCell():New( oSection1, 'DATA_4SALDO'				,'FLW', 		'DATA_4SALDO',			  			""							,16)
	
	// Nota 5
	TRCell():New( oSection1, 'NOTA_5FISCAL'				,'FLW', 		'NOTA_5FISCAL',		  				"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_5FATURA'				,'FLW', 		'DATA_5FATURA',		  				""				        	,08)
	TRCell():New( oSection1, 'QTDE_5FATURA'				,'FLW', 		'QTDE_5FATURA',		  				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_5'					,'FLW', 		'MODAL_5',				  			"@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_5'					,'FLW', 		'OBSERV_5',			  		    	"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_5RECEBIMENTO'		,'FLW', 		'PREV_5RECEBIMENTO',	  			""				        	,08)
	TRCell():New( oSection1, 'SALDO_5'					,'FLW', 		'SALDO_5',				  			"@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'DATA_5SALDO'				,'FLW', 		'DATA_5SALDO',			  			""  				        ,08)

	// Nota 6	
	TRCell():New( oSection1, 'NOTA_6FISCAL'				,'FLW', 		'NOTA_6FISCAL',		  				"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_6FATURA'				,'FLW', 		'DATA_6FATURA',		  				""				        	,08)
	TRCell():New( oSection1, 'QTDE_6FATURA'				,'FLW', 		'QTDE_6FATURA',		  				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_6'					,'FLW', 		'MODAL_6',				  			"@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_6'					,'FLW', 		'OBSERV_6',			  		   		"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_6RECEBIMENTO'		,'FLW', 		'PREV_6RECEBIMENTO',	  			""				        	,08)
	TRCell():New( oSection1, 'SALDO_6'					,'FLW', 		'SALDO_6',				  			"@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'DATA_6SALDO'				,'FLW', 		'DATA_6SALDO',			  			""  				        ,08)

	// Nota 7
	TRCell():New( oSection1, 'NOTA_7FISCAL'				,'FLW', 		'NOTA_7FISCAL',		  				"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_7FATURA'				,'FLW', 		'DATA_7FATURA',		  				""				        	,08)
	TRCell():New( oSection1, 'QTDE_7FATURA'				,'FLW', 		'QTDE_7FATURA',		  				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_7'					,'FLW', 		'MODAL_7',				  			"@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_7'					,'FLW', 		'OBSERV_7',			  		    	"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_7RECEBIMENTO'		,'FLW', 		'PREV_7RECEBIMENTO',	  			""				        	,08)
	TRCell():New( oSection1, 'SALDO_7'					,'FLW', 		'SALDO_7',				  			"@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'DATA_7SALDO'				,'FLW', 		'DATA_7SALDO',			  			""  				        ,08)

	// Nota 8
	TRCell():New( oSection1, 'NOTA_8FISCAL'				,'FLW', 		'NOTA_8FISCAL',		  				"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_8FATURA'				,'FLW', 		'DATA_8FATURA',		  				""				        	,08)
	TRCell():New( oSection1, 'QTDE_8FATURA'				,'FLW', 		'QTDE_8FATURA',		  				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_8'					,'FLW', 		'MODAL_8',				  			"@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_8'					,'FLW', 		'OBSERV_8',			  		    	"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_8RECEBIMENTO'		,'FLW', 		'PREV_8RECEBIMENTO',	  			""				        	,08)
	TRCell():New( oSection1, 'SALDO_8'					,'FLW', 		'SALDO_8',				  			"@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'DATA_8SALDO'				,'FLW', 		'DATA_8SALDO',			  			""  				        ,08)

	// Nota 9
	TRCell():New( oSection1, 'NOTA_9FISCAL'				,'FLW', 		'NOTA_9FISCAL',		  				"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_9FATURA'				,'FLW', 		'DATA_9FATURA',		  				""				        	,08)
	TRCell():New( oSection1, 'QTDE_9FATURA'				,'FLW', 		'QTDE_9FATURA',		  				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_9'					,'FLW', 		'MODAL_9',				  			"@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_9'					,'FLW', 		'OBSERV_9',			  		  		"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_9RECEBIMENTO'		,'FLW', 		'PREV_9RECEBIMENTO',	  			""				        	,08)
	TRCell():New( oSection1, 'SALDO_9'					,'FLW', 		'SALDO_9',				  			"@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'DATA_9SALDO'				,'FLW', 		'DATA_9SALDO',			  			""  				        ,08)

	// Nota 10
	TRCell():New( oSection1, 'NOTA_10FISCAL'			,'FLW', 		'NOTA_10FISCAL',	  				"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_10FATURA'			,'FLW', 		'DATA_10FATURA',	  				""				        	,08)
	TRCell():New( oSection1, 'QTDE_10FATURA'			,'FLW', 		'QTDE_10FATURA',	  				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_10'					,'FLW', 		'MODAL_10',				  			"@!"				        ,12)
	TRCell():New( oSection1, 'OBSERV_10'				,'FLW', 		'OBSERV_10',			  		    "@!"				        ,90)
	TRCell():New( oSection1, 'PREV_10RECEBIMENTO'		,'FLW', 		'PREV_10RECEBIMENTO',	  			""				        	,08)
	TRCell():New( oSection1, 'SALDO_10'					,'FLW', 		'SALDO_10',				  			"@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'DATA_10SALDO'				,'FLW', 		'DATA_10SALDO',			  			""  				        ,08)

	TRCell():New( oSection1, 'TRANSITO'			   		,'FLW', 		'TRANSITO',			  	     		"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'SALDO_FINAL'				,'FLW', 		'SALDO_FINAL',			  	  	  	"@E 999,999,999.999999"		,16)
    TRCell():New( oSection1, 'PERCENTUAL'				,'FLW', 		'PERCENTUAL',			  	  	  	"@E 999.99"					,16)    

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 10 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	//Local oSection2 := oReport:Section(2)
	Local cQuery 	:= ""
	Local lEncerra	:= .f.
	Local cDscSubC	:= ""
	Local nQtd1		:= 0
	Local nQtd2		:= 0
	Local nQtd3		:= 0	
	Local nQtd4		:= 0
	Local nQtd5		:= 0
	Local nQtd6		:= 0
	Local nQtd7		:= 0
	Local nQtd8		:= 0
	Local nQtd9		:= 0
	Local nQtd10	:= 0
	Local nSldFim   := 0
	Local nPercent  := 0

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	If Select("FLW") > 0
		FLW->(dbCloseArea())
	Endif
      
	cQuery += "SELECT  "+ CRLF
	
	cQuery += "ISNULL(SC7.C7_NUM ,'') AS PEDIDO, "+ CRLF
	cQuery += "SC1.C1_NUM AS SOLICITACAO,"+ CRLF
	cQuery += "SC1.C1_USER AS SC_COMPRADOR , "+ CRLF
	cQuery += "SC7.C7_USER AS PED_COMPRADOR , "+ CRLF
	cQuery += "SC1.C1_FILIAL AS FILIAL_PEDIDO, "+ CRLF
	cQuery += "SC7.C7_FILENT AS FILIAL_ENTREGA, "+ CRLF
	cQuery += "SB1.B1_TIPO AS TIPO, "+ CRLF
	cQuery += "SC1.C1_PRODUTO AS SKU, "+ CRLF
	cQuery += "SB1.B1_COD AS PRODUTO, "+ CRLF
	cQuery += "SB1.B1_DESC AS DESCRICAO, "+ CRLF
	cQuery += "SBM.BM_DESC AS GRUPO_PRODUTO, "+ CRLF
	cQuery += "SUBSTRING(SC1.C1_PRODUTO,9,3) AS COD_COR, "+ CRLF
	cQuery += "SBV.BV_DESCRI AS DESC_COR, "+ CRLF
	cQuery += "RIGHT(SC1.C1_PRODUTO,4)  AS TAM, "+ CRLF
	cQuery += "SB1.B1_UM AS UNID_MEDIDA, "+ CRLF
	cQuery += "SC1.C1_SOLICIT AS SOLICITANTE, "+ CRLF
	cQuery += "SC1.C1_EMISSAO AS DAT_EMISSAO_SC, "+ CRLF
	cQuery += "SC1.C1_QUANT AS QTDE_SOLICITADA, "+ CRLF
	cQuery += "SC1.C1_XTPREQU AS TIPO_REQUISICAO,  "+ CRLF
	cQuery += "SC1.C1_XCOLECA AS COLECAO,  "+ CRLF
	cQuery += "SC1.C1_XSBCOLE AS SUB_COLECAO,  "+ CRLF
	cQuery += "SC1.C1_XDTNPCP AS DAT_NEC_DEMANDA, "+ CRLF
	cQuery += "SC1.C1_XQTDMOS AS QUANT_MOSTRUARIO,  "+ CRLF
	cQuery += "SC1.C1_XDTPCPM AS DT_NEC_MOSTRUARIO, "+ CRLF
	cQuery += "SC7.C7_CONAPRO AS STATUS_APROVACAO, "+ CRLF
 	cQuery += "SA2.A2_COD  AS CODFOR, "+ CRLF
	cQuery += "SA2.A2_NOME AS FORNE_SC, "+ CRLF
	cQuery += "SC7.C7_FORNECE  AS CODFORPED, "+ CRLF
	cQuery += "SA2.A2_EST AS UF, "+ CRLF
	cQuery += "CTT.CTT_DESC01 AS CCUSTO, "+ CRLF
	cQuery += "SC7.C7_EMISSAO AS DT_EMISSAO_PED, "+ CRLF
	cQuery += "SC7.C7_COND AS COND_PGTO, "+ CRLF
	cQuery += "SE4.E4_DESCRI AS DESC_COND_PGTO, "+ CRLF	
	cQuery += "SC7.C7_TOTAL AS VLR_TOTAL_ITEM, "+ CRLF
	cQuery += "SC7.C7_QUANT AS QTDE_PEDIDO, "+ CRLF
	cQuery += "SC7.C7_DATPRF AS DATA_ENTREGA, "+ CRLF
	cQuery += "SC7.C7_QUJE AS QTDE_ENTREGUE , "+ CRLF
	cQuery += "(SC7.C7_QUJE - SC7.C7_QUANT) AS SALDO, "+ CRLF
	cQuery += "(SC7.C7_TOTAL / SC7.C7_QUANT ) AS CUSTO, "+ CRLF
	cQuery += "SC7.C7_RESIDUO AS QUITADO,  "+ CRLF
	
	//Devolu��o
	cQuery += "ISNULL((SELECT SUM(D2_QUANT) "+ CRLF
	cQuery += "FROM "+RetSqlName('SD2')+" AS D2 WITH (NOLOCK) "+ CRLF
	cQuery += "INNER JOIN "+RetSqlName('SD1')+" AS D1 WITH (NOLOCK) ON (D1.D1_FILIAL = D2.D2_FILIAL "+ CRLF 
	cQuery += "                                   					AND D1.D1_FORNECE = D2.D2_CLIENTE "+ CRLF
	cQuery += "									  					AND D1.D1_DOC = D2.D2_NFORI "+ CRLF
	cQuery += "														AND D1.D1_COD = D2.D2_COD "+ CRLF
	cQuery += "									  					AND D1.D_E_L_E_T_ = '') "+ CRLF
	cQuery += "WHERE D2.D_E_L_E_T_ = '' AND D1.D1_FILIAL = SC7.C7_FILIAL AND D2_TIPO = 'D' AND D2_CLIENTE = SA2.A2_COD AND D1_PEDIDO = SC7.C7_NUM AND D2_COD = SC7.C7_PRODUTO "+ CRLF
	cQuery += "GROUP BY D1_FILIAL,D1_PEDIDO,D1_FORNECE,D2_COD),0) AS QTDE_DEVOLVIDA, "+ CRLF

    // Nota 1
	cQuery += "SC7.C7_X1NF AS NOTA_1FISCAL, "+ CRLF
	cQuery += "SC7.C7_X1DT_FA AS DATA_1FATURA, "+ CRLF
	cQuery += "SC7.C7_X1QTD_F AS QTDE_1FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X1MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X1MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_1, "+ CRLF	
	cQuery += "SC7.C7_X1OBS   AS OBSERV_1, "+ CRLF
	cQuery += "SC7.C7_X1PRREC AS PREV_1RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X1SALDO AS SALDO_1, "+ CRLF
	cQuery += "SC7.C7_X1DTSAL AS DATA_1SALDO, "+ CRLF

    // Nota 2
	cQuery += "SC7.C7_X2NF AS NOTA_2FISCAL, "+ CRLF
	cQuery += "SC7.C7_X2DT_FA AS DATA_2FATURA, "+ CRLF
	cQuery += "SC7.C7_X2QTD_F AS QTDE_2FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X2MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X2MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_2, "+ CRLF
	cQuery += "SC7.C7_X2OBS   AS OBSERV_2, "+ CRLF
	cQuery += "SC7.C7_X2PRREC AS PREV_2RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X2SALDO AS SALDO_2, "+ CRLF
	cQuery += "SC7.C7_X2DTSA AS DATA_2SALDO, "+ CRLF
	
    // Nota 3
	cQuery += "SC7.C7_X3NF AS NOTA_3FISCAL, "+ CRLF
	cQuery += "SC7.C7_X3DT_FA AS DATA_3FATURA, "+ CRLF
	cQuery += "SC7.C7_X3QTD_F AS QTDE_3FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X3MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X3MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_3, "+ CRLF
	cQuery += "SC7.C7_X3OBS   AS OBSERV_3, "+ CRLF
	cQuery += "SC7.C7_X3PRREC AS PREV_3RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X3SALDO AS SALDO_3, "+ CRLF
	cQuery += "SC7.C7_X3DTSAL AS DATA_3SALDO, "+ CRLF
    
    // Nota 4
	cQuery += "SC7.C7_X4NF AS NOTA_4FISCAL, "+ CRLF
	cQuery += "SC7.C7_X4D_FA AS DATA_4FATURA, "+ CRLF
	cQuery += "SC7.C7_X4QTD_F AS QTDE_4FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X4MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X4MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_4, "+ CRLF
	cQuery += "SC7.C7_X4OBS   AS OBSERV_4, "+ CRLF
	cQuery += "SC7.C7_X4PRRE AS PREV_4RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X4SALDO AS SALDO_4, "+ CRLF
	cQuery += "SC7.C7_X4DTSA AS DATA_4SALDO, "+ CRLF
    
    // Nota 5
	cQuery += "SC7.C7_X5NF AS NOTA_5FISCAL, "+ CRLF
	cQuery += "SC7.C7_X5D_FA AS DATA_5FATURA, "+ CRLF
	cQuery += "SC7.C7_X5QTD_F AS QTDE_5FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X5MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X5MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_5, "+ CRLF
	cQuery += "SC7.C7_X5OBS   AS OBSERV_5, "+ CRLF
	cQuery += "SC7.C7_X5PRRE AS PREV_5RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X5SALDO AS SALDO_5, "+ CRLF
	cQuery += "SC7.C7_X5DSAL AS DATA_5SALDO, "+ CRLF
	
	// Nota 6
	cQuery += "SC7.C7_X6NF 		AS NOTA_6FISCAL, "+ CRLF
	cQuery += "SC7.C7_X6DT_FA 	AS DATA_6FATURA, "+ CRLF
	cQuery += "SC7.C7_X6QTD_F 	AS QTDE_6FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X6MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X6MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_6, "+ CRLF
	cQuery += "SC7.C7_X6OBS     AS OBSERV_6, "+ CRLF
	cQuery += "SC7.C7_X6PRREC 	AS PREV_6RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X6SALDO 	AS SALDO_6, "+ CRLF
	cQuery += "SC7.C7_X6DTSAL 	AS DATA_6SALDO, "+ CRLF
	
	// Nota 7
	cQuery += "SC7.C7_X7NF 		AS NOTA_7FISCAL, "+ CRLF
	cQuery += "SC7.C7_X7DT_FA 	AS DATA_7FATURA, "+ CRLF
	cQuery += "SC7.C7_X7QTD_F 	AS QTDE_7FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X7MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X7MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_7, "+ CRLF
	cQuery += "SC7.C7_X7OBS     AS OBSERV_7, "+ CRLF
	cQuery += "SC7.C7_X7PRREC 	AS PREV_7RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X7SALDO 	AS SALDO_7, "+ CRLF
	cQuery += "SC7.C7_X7DTSAL 	AS DATA_7SALDO, "+ CRLF

	// Nota 8
	cQuery += "SC7.C7_X8NF 		AS NOTA_8FISCAL, "+ CRLF
	cQuery += "SC7.C7_X8DT_FA 	AS DATA_8FATURA, "+ CRLF
	cQuery += "SC7.C7_X8QTD_F 	AS QTDE_8FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X8MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X8MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_8, "+ CRLF
	cQuery += "SC7.C7_X8OBS     AS OBSERV_8, "+ CRLF
	cQuery += "SC7.C7_X8PRREC 	AS PREV_8RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X8SALDO 	AS SALDO_8, "+ CRLF
	cQuery += "SC7.C7_X8DTSAL 	AS DATA_8SALDO, "+ CRLF

	// Nota 9
	cQuery += "SC7.C7_X9NF 		AS NOTA_9FISCAL, "+ CRLF
	cQuery += "SC7.C7_X9DT_FA 	AS DATA_9FATURA, "+ CRLF
	cQuery += "SC7.C7_X9QTD_F 	AS QTDE_9FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X9MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X9MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_9, "+ CRLF
	cQuery += "SC7.C7_X9OBS     AS OBSERV_9, "+ CRLF
	cQuery += "SC7.C7_X9PRREC 	AS PREV_9RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X9SALDO 	AS SALDO_9, "+ CRLF
	cQuery += "SC7.C7_X9DTSAL 	AS DATA_9SALDO, "+ CRLF

	// Nota 10
	cQuery += "SC7.C7_XANF 		AS NOTA_10FISCAL, "+ CRLF
	cQuery += "SC7.C7_XADT_FA 	AS DATA_10FATURA, "+ CRLF
	cQuery += "SC7.C7_XAQTD_F 	AS QTDE_10FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_XAMODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_XAMODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_10, "+ CRLF
	cQuery += "SC7.C7_X10OBS    AS OBSERV_10, "+ CRLF
	cQuery += "SC7.C7_XAPRREC 	AS PREV_10RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_XASALDO 	AS SALDO_10, "+ CRLF
	cQuery += "SC7.C7_XADTSAL 	AS DATA_10SALDO "+ CRLF

	cQuery += " FROM "+RetSqlName('SC1')+" SC1 WITH (NOLOCK) "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SB1')+" SB1 WITH (NOLOCK) ON SB1.B1_COD=SC1.C1_PRODUTO AND SB1.D_E_L_E_T_='' " + CRLF
	cQuery += " LEFT JOIN "+RetSqlName('CTT')+" CTT WITH (NOLOCK) ON CTT.CTT_CUSTO=SC1.C1_CC AND CTT.CTT_FILIAL=RIGHT(SC1.C1_FILIAL,2) AND CTT.D_E_L_E_T_='' "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SBV')+" SBV WITH (NOLOCK) ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC1.C1_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SBM')+" SBM WITH (NOLOCK) ON SBM.BM_GRUPO=SB1.B1_GRUPO AND SBM.D_E_L_E_T_='' "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SC7')+" SC7 WITH (NOLOCK) ON SC7.C7_NUMSC=SC1.C1_NUM AND SC7.C7_ITEMSC=SC1.C1_ITEM AND SC7.C7_FILIAL=SC1.C1_FILIAL AND  SC7.D_E_L_E_T_='' " + CRLF
	
	If MV_PAR06 == 1 // 1 = Aberto || 2 = Finalizado || 3 = Todos
		cQuery += " AND SC7.C7_QUJE < SC7.C7_QUANT "+ CRLF//QTDE_ENTREGUE < QTDE_PEDIDO" 
	Elseif  MV_PAR06 == 2 //Finalizado
		cQuery += " AND SC7.C7_QUJE = SC7.C7_QUANT "+ CRLF //AND QTDE_ENTREGUE = QTDE_PEDIDO"
	Endif

	If MV_PAR07 == 1 // 1 = Pedido || 2 = Todos ||
		cQuery += " AND SC7.C7_EMISSAO BETWEEN '"+ DTOS(mv_par08) +"' AND '"+ DTOS(mv_par09) +"' " + CRLF
	Endif
	 
	cQuery += " LEFT JOIN "+RetSqlName('SA2')+" SA2 WITH (NOLOCK) ON  SA2.A2_COD=SC1.C1_FORNECE AND SA2.A2_LOJA = SC1.C1_LOJA  AND SA2.D_E_L_E_T_=''"+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SE4')+" SE4 WITH (NOLOCK) ON (SE4.E4_CODIGO = SC7.C7_COND AND SE4.D_E_L_E_T_ = '')"+ CRLF
	cQuery += " WHERE SC1.D_E_L_E_T_= ''  " + CRLF

	//Tipo
	If !Empty(mv_par01)
		cQuery += " AND SB1.B1_TIPO  IN "+Formatin(Upper(Alltrim(mv_par01)),";")+"  " + CRLF
	Endif

 	//Codigo Base do Produto (Referencia)
	If !Empty(mv_par03) 
		cQuery += " AND SB1.B1_COD = '"+Alltrim(mv_par03)+"' " + CRLF
	Endif

	//Numero da Solicitacao
	If !Empty(mv_par02)
		cQuery += " AND SC1.C1_NUM = '"+Alltrim(mv_par02)+"' " + CRLF
	Endif
	
	//Emissao SC
	If !Empty(mv_par04)
		cQuery += " AND SC1.C1_EMISSAO BETWEEN '"+ DTOS(mv_par04) +"' AND '"+ DTOS(mv_par05) +"' " + CRLF
	Endif
	
	//cQuery += " AND SC7.C7_NUM = '658354' " //TESTE
	
	cQuery += "UNION  " + CRLF

	cQuery += "SELECT"+ CRLF	
	cQuery += "SC7.C7_NUM AS PEDIDO, "+ CRLF
	cQuery += "'' AS SOLICITACAO,"+ CRLF
	cQuery += "'' AS SC_COMPRADOR , "+ CRLF
	cQuery += "SC7.C7_USER AS PED_COMPRADOR , "+ CRLF
	cQuery += "SC7.C7_FILIAL AS FILIAL_PEDIDO, "+ CRLF
	cQuery += "SC7.C7_FILENT AS FILIAL_ENTREGA, "+ CRLF
	cQuery += "SB1.B1_TIPO AS TIPO, "+ CRLF
	cQuery += "SC7.C7_PRODUTO AS SKU, "+ CRLF
	cQuery += "SB1.B1_COD AS PRODUTO, "+ CRLF
	cQuery += "SB1.B1_DESC AS DESCRICAO, "+ CRLF
	cQuery += "SBM.BM_DESC AS GRUPO_PRODUTO, "+ CRLF
	cQuery += "SUBSTRING(SC7.C7_PRODUTO,9,3) AS COD_COR, "+ CRLF
	cQuery += "SBV.BV_DESCRI AS DESC_COR, "+ CRLF
	cQuery += "RIGHT(SC7.C7_PRODUTO,4)  AS TAM, "+ CRLF
	cQuery += "SB1.B1_UM AS UNID_MEDIDA, "+ CRLF
	cQuery += "'' AS SOLICITANTE, "+ CRLF
	cQuery += "'' AS DAT_EMISSAO_SC, "+ CRLF
	cQuery += "SC7.C7_QUANT AS QTDE_SOLICITADA, "+ CRLF
	cQuery += "'' AS TIPO_REQUISICAO,  "+ CRLF
	cQuery += "'' AS COLECAO,  "+ CRLF
	cQuery += "'' AS SUB_COLECAO,  "+ CRLF
	cQuery += "'' AS DAT_NEC_DEMANDA, "+ CRLF
	cQuery += "'' AS QUANT_MOSTRUARIO,  "+ CRLF
	cQuery += "'' AS DT_NEC_MOSTRUARIO, "+ CRLF
	cQuery += "SC7.C7_CONAPRO AS STATUS_APROVACAO, "+ CRLF
	cQuery += "SA2.A2_COD  AS CODFOR, "+ CRLF
	cQuery += "SA2.A2_NOME AS FORNE_SC, "+ CRLF
	cQuery += "SC7.C7_FORNECE  AS CODFORPED, "+ CRLF
	cQuery += "SA2.A2_EST AS UF, "+ CRLF
	cQuery += "CTT.CTT_DESC01 AS CCUSTO, "+ CRLF
	cQuery += "SC7.C7_EMISSAO AS DT_EMISSAO_PED, "+ CRLF
	cQuery += "SC7.C7_COND AS COND_PGTO, "+ CRLF
	cQuery += "SE4.E4_DESCRI AS DESC_COND_PGTO, "+ CRLF
	cQuery += "SC7.C7_TOTAL AS VLR_TOTAL_ITEM, "+ CRLF
	cQuery += "SC7.C7_QUANT AS QTDE_PEDIDO, "+ CRLF
	cQuery += "SC7.C7_DATPRF AS DATA_ENTREGA, "+ CRLF
	cQuery += "SC7.C7_QUJE AS QTDE_ENTREGUE , "+ CRLF
	cQuery += "(SC7.C7_QUJE - SC7.C7_QUANT) AS SALDO, "+ CRLF
	cQuery += "(SC7.C7_TOTAL / SC7.C7_QUANT ) AS CUSTO, "+ CRLF
	cQuery += "SC7.C7_RESIDUO AS QUITADO,  "+ CRLF
	
	//Devolu��o
	cQuery += "ISNULL((SELECT SUM(D2_QUANT) "+ CRLF
	cQuery += "FROM "+RetSqlName('SD2')+" AS D2 WITH (NOLOCK) "+ CRLF
	cQuery += "INNER JOIN "+RetSqlName('SD1')+" AS D1 WITH (NOLOCK) ON (D1.D1_FILIAL = D2.D2_FILIAL "+ CRLF 
	cQuery += "                                   					AND D1.D1_FORNECE = D2.D2_CLIENTE "+ CRLF
	cQuery += "									  					AND D1.D1_DOC = D2.D2_NFORI "+ CRLF
	cQuery += "														AND D1.D1_COD = D2.D2_COD "+ CRLF
	cQuery += "									  					AND D1.D_E_L_E_T_ = '') "+ CRLF
	cQuery += "WHERE D2.D_E_L_E_T_ = '' AND D1.D1_FILIAL = SC7.C7_FILIAL AND D2_TIPO = 'D' AND D2_CLIENTE = SA2.A2_COD AND D1_PEDIDO = SC7.C7_NUM AND D2_COD = SC7.C7_PRODUTO "+ CRLF
	cQuery += "GROUP BY D1_FILIAL,D1_PEDIDO,D1_FORNECE,D2_COD),0) AS QTDE_DEVOLVIDA, "+ CRLF
	
	// Nota 1
	cQuery += "SC7.C7_X1NF AS NOTA_1FISCAL, "+ CRLF
	cQuery += "SC7.C7_X1DT_FA AS DATA_1FATURA, "+ CRLF
	cQuery += "SC7.C7_X1QTD_F AS QTDE_1FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X1MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X1MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_1, "+ CRLF	
	cQuery += "SC7.C7_X1OBS   AS OBSERV_1, "+ CRLF
	cQuery += "SC7.C7_X1PRREC AS PREV_1RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X1SALDO AS SALDO_1, "+ CRLF
	cQuery += "SC7.C7_X1DTSAL AS DATA_1SALDO, "+ CRLF
	
	// Nota 2
	cQuery += "SC7.C7_X2NF AS NOTA_2FISCAL, "+ CRLF
	cQuery += "SC7.C7_X2DT_FA AS DATA_2FATURA, "+ CRLF
	cQuery += "SC7.C7_X2QTD_F AS QTDE_2FATURA, 
	cQuery += "CASE WHEN SC7.C7_X2MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X2MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_2, "+ CRLF	
	cQuery += "SC7.C7_X2OBS   AS OBSERV_2, "+ CRLF
	cQuery += "SC7.C7_X2PRREC AS PREV_2RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X2SALDO AS SALDO_2, "+ CRLF
	cQuery += "SC7.C7_X2DTSA AS DATA_2SALDO, "+ CRLF
	
	// Nota 3
	cQuery += "SC7.C7_X3NF AS NOTA_3FISCAL, "+ CRLF
	cQuery += "SC7.C7_X3DT_FA AS DATA_3FATURA, "+ CRLF
	cQuery += "SC7.C7_X3QTD_F AS QTDE_3FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X3MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X3MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_3, "+ CRLF	
	cQuery += "SC7.C7_X3OBS   AS OBSERV_3, "+ CRLF
	cQuery += "SC7.C7_X3PRREC AS PREV_3RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X3SALDO AS SALDO_3, "+ CRLF
	cQuery += "SC7.C7_X3DTSAL AS DATA_3SALDO, "+ CRLF
	
	// Nota 4
	cQuery += "SC7.C7_X4NF AS NOTA_4FISCAL, "+ CRLF
	cQuery += "SC7.C7_X4D_FA AS DATA_4FATURA, "+ CRLF
	cQuery += "SC7.C7_X4QTD_F AS QTDE_4FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X4MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X4MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_4, "+ CRLF	
	cQuery += "SC7.C7_X4OBS   AS OBSERV_4, "+ CRLF
	cQuery += "SC7.C7_X4PRRE AS PREV_4RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X4SALDO AS SALDO_4, "+ CRLF
	cQuery += "SC7.C7_X4DTSA AS DATA_4SALDO, "+ CRLF
	
	// Nota 5
	cQuery += "SC7.C7_X5NF AS NOTA_5FISCAL, "+ CRLF
	cQuery += "SC7.C7_X5D_FA AS DATA_5FATURA, "+ CRLF
	cQuery += "SC7.C7_X5QTD_F AS QTDE_5FATURA, "+ CRLF
	cQuery += "CASE WHEN SC7.C7_X5MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X5MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_5, "+ CRLF	
	cQuery += "SC7.C7_X5OBS   AS OBSERV_5, "+ CRLF
	cQuery += "SC7.C7_X5PRRE AS PREV_5RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X5SALDO AS SALDO_5, "+ CRLF
	cQuery += "SC7.C7_X5DSAL AS DATA_5SALDO, "+ CRLF

	// Nota 6
	cQuery += "SC7.C7_X6NF 		AS NOTA_6FISCAL, "+ CRLF
	cQuery += "SC7.C7_X6DT_FA 	AS DATA_6FATURA, "+ CRLF
	cQuery += "SC7.C7_X6QTD_F 	AS QTDE_6FATURA, 
	cQuery += "CASE WHEN SC7.C7_X6MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X6MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_6, "+ CRLF	
	cQuery += "SC7.C7_X6OBS     AS OBSERV_6, "+ CRLF
	cQuery += "SC7.C7_X6PRREC 	AS PREV_6RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X6SALDO 	AS SALDO_6, "+ CRLF
	cQuery += "SC7.C7_X6DTSAL 	AS DATA_6SALDO, "+ CRLF

	// Nota 7
	cQuery += "SC7.C7_X7NF 		AS NOTA_7FISCAL, "+ CRLF
	cQuery += "SC7.C7_X7DT_FA 	AS DATA_7FATURA, "+ CRLF
	cQuery += "SC7.C7_X7QTD_F 	AS QTDE_7FATURA, 
	cQuery += "CASE WHEN SC7.C7_X7MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X7MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_7, "+ CRLF	
	cQuery += "SC7.C7_X7OBS     AS OBSERV_7, "+ CRLF
	cQuery += "SC7.C7_X7PRREC 	AS PREV_7RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X7SALDO 	AS SALDO_7, "+ CRLF
	cQuery += "SC7.C7_X7DTSAL 	AS DATA_7SALDO, "+ CRLF

	// Nota 8
	cQuery += "SC7.C7_X8NF 		AS NOTA_8FISCAL, "+ CRLF
	cQuery += "SC7.C7_X8DT_FA 	AS DATA_8FATURA, "+ CRLF
	cQuery += "SC7.C7_X8QTD_F 	AS QTDE_8FATURA, 
	cQuery += "CASE WHEN SC7.C7_X8MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X8MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_8, "+ CRLF	
	cQuery += "SC7.C7_X8OBS     AS OBSERV_8, "+ CRLF
	cQuery += "SC7.C7_X8PRREC 	AS PREV_8RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X8SALDO 	AS SALDO_8, "+ CRLF
	cQuery += "SC7.C7_X8DTSAL 	AS DATA_8SALDO, "+ CRLF

	// Nota 9
	cQuery += "SC7.C7_X9NF 		AS NOTA_9FISCAL, "+ CRLF
	cQuery += "SC7.C7_X9DT_FA 	AS DATA_9FATURA, "+ CRLF
	cQuery += "SC7.C7_X9QTD_F 	AS QTDE_9FATURA, 
	cQuery += "CASE WHEN SC7.C7_X9MODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_X9MODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_9, "+ CRLF	
	cQuery += "SC7.C7_X9OBS     AS OBSERV_9, "+ CRLF
	cQuery += "SC7.C7_X9PRREC 	AS PREV_9RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X9SALDO 	AS SALDO_9, "+ CRLF
	cQuery += "SC7.C7_X9DTSAL 	AS DATA_9SALDO, "+ CRLF

	// Nota 10
	cQuery += "SC7.C7_XANF 		AS NOTA_10FISCAL, "+ CRLF
	cQuery += "SC7.C7_XADT_FA 	AS DATA_10FATURA, "+ CRLF
	cQuery += "SC7.C7_XAQTD_F 	AS QTDE_10FATURA, 
	cQuery += "CASE WHEN SC7.C7_XAMODAL = 1 THEN 'AEREO' "+ CRLF
	cQuery += "     WHEN SC7.C7_XAMODAL = 2 THEN 'RODOVIARIO' "+ CRLF
	cQuery += " 	ELSE '' END AS MODAL_10, "+ CRLF	
	cQuery += "SC7.C7_X10OBS    AS OBSERV_10, "+ CRLF
	cQuery += "SC7.C7_XAPRREC 	AS PREV_10RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_XASALDO 	AS SALDO_10, "+ CRLF
	cQuery += "SC7.C7_XADTSAL 	AS DATA_10SALDO "+ CRLF

	cQuery += " FROM "+RetSqlName('SC7')+" SC7 WITH (NOLOCK) "+ CRLF
	
	cQuery += " LEFT JOIN "+RetSqlName('SB1')+" SB1 WITH (NOLOCK) ON SB1.B1_COD=SC7.C7_PRODUTO AND SB1.D_E_L_E_T_='' "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('CTT')+" CTT WITH (NOLOCK) ON  CTT.CTT_CUSTO=SC7.C7_CC AND CTT.CTT_FILIAL=RIGHT(SC7.C7_FILIAL,2) AND CTT.D_E_L_E_T_='' "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SBV')+" SBV WITH (NOLOCK) ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC7.C7_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "+ CRLF 
	cQuery += " LEFT JOIN "+RetSqlName('SBM')+" SBM WITH (NOLOCK) ON SBM.BM_GRUPO=SB1.B1_GRUPO AND SBM.D_E_L_E_T_=''  "+ CRLF
	//cQuery += " LEFT JOIN "+RetSqlName('SC1')+" SC1 WITH (NOLOCK) ON SC1.C1_NUM=SC7.C7_NUMSC " + CRLF
	//cQuery += " AND SC1.C1_ITEM=SC7.C7_ITEMSC " + CRLF
	//cQuery += " AND SC1.C1_FILIAL=SC7.C7_FILIAL " + CRLF
	//cQuery += " AND SC1.D_E_L_E_T_= ''  " + CRLF

   //Numero da Solicitacao
	/*If !Empty(mv_par02)
		cQuery += " AND SC1.C1_NUM = '"+Alltrim(mv_par02)+"' " + CRLF
	Endif*/
	
	//Emissao SC
	/*If !Empty(mv_par04)
		cQuery += " AND SC1.C1_EMISSAO BETWEEN '"+ DTOS(mv_par04) +"' AND '"+ DTOS(mv_par05) +"' " + CRLF
	Endif*/

  	If MV_PAR06 == 1 // 1 = Aberto || 2 = Finalizado || 3 = Todos
		cQuery += " AND SC7.C7_QUJE < SC7.C7_QUANT "+ CRLF//QTDE_ENTREGUE < QTDE_PEDIDO" 
	Elseif  MV_PAR06 == 2 //Finalizado
		cQuery += " AND SC7.C7_QUJE = SC7.C7_QUANT "+ CRLF //AND QTDE_ENTREGUE = QTDE_PEDIDO"
	Endif

	cQuery += " LEFT JOIN "+RetSqlName('SA2')+" SA2 WITH (NOLOCK) ON (SA2.A2_COD=SC7.C7_FORNECE AND SA2.A2_LOJA = SC7.C7_LOJA AND SA2.D_E_L_E_T_='')"+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SE4')+" SE4 WITH (NOLOCK) ON (SE4.E4_CODIGO = SC7.C7_COND AND SE4.D_E_L_E_T_ = '')"+ CRLF

	cQuery += "WHERE  "
	cQuery += " SC7.D_E_L_E_T_=''"    + CRLF
	cQuery += " AND SC7.C7_NUMSC = '' "+ CRLF
	
	//Tipo
	If !Empty(mv_par01)
		cQuery += " AND SB1.B1_TIPO  IN "+Formatin(Upper(Alltrim(mv_par01)),";")+"  " + CRLF
	Endif

 	//Codigo Base do Produto (Referencia)
	If !Empty(mv_par03) 
		cQuery += " AND SB1.B1_COD = '"+Alltrim(mv_par03)+"' " + CRLF
	Endif

 	//Emissao PEDIDO
	cQuery += " AND SC7.C7_EMISSAO BETWEEN '"+ DTOS(mv_par08) +"' AND '"+ DTOS(mv_par09) +"' " + CRLF
	
	//cQuery += " AND SC7.C7_NUM = '659274' " //TESTE
	
	cQuery += "ORDER BY PEDIDO" + CRLF

	TCQUERY cQuery NEW ALIAS FLW

	While FLW->(!EOF())
	
	    NfsOriDev 	 := NfOrigem(FLW->CODFOR,FLW->PEDIDO,FLW->PRODUTO) 
	    NfsDevolucao := NfDevolucao(FLW->CODFOR,FLW->PEDIDO,FLW->PRODUTO)

		If oReport:Cancel()
			Exit
		Endif

		//If !(EmptyFLW->PEDIDO)

			oReport:IncMeter()

			oSection1:Cell("FILIAL_PEDIDO"):SetValue(FLW->FILIAL_PEDIDO)
			oSection1:Cell("FILIAL_PEDIDO"):SetAlign("LEFT")

			oSection1:Cell("FILIAL_ENTREGA"):SetValue(FLW->FILIAL_ENTREGA)
			oSection1:Cell("FILIAL_ENTREGA"):SetAlign("LEFT")
			
			oSection1:Cell("TIPO"):SetValue(FLW->TIPO)
			oSection1:Cell("TIPO"):SetAlign("LEFT")
			
			oSection1:Cell("SKU"):SetValue(FLW->SKU)
			oSection1:Cell("SKU"):SetAlign("LEFT")
			
			oSection1:Cell("PRODUTO"):SetValue(FLW->PRODUTO)
			oSection1:Cell("PRODUTO"):SetAlign("LEFT")
			
			oSection1:Cell("DESCRICAO"):SetValue(FLW->DESCRICAO)
			oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
			
			oSection1:Cell("GRUPO_PRODUTO"):SetValue(FLW->GRUPO_PRODUTO)
			oSection1:Cell("GRUPO_PRODUTO"):SetAlign("LEFT")
			
			oSection1:Cell("COD_COR"):SetValue(FLW->COD_COR)
			oSection1:Cell("COD_COR"):SetAlign("LEFT")
			
			oSection1:Cell("DESC_COR"):SetValue(FLW->DESC_COR)
			oSection1:Cell("DESC_COR"):SetAlign("LEFT")
			
			oSection1:Cell("TAM"):SetValue(FLW->TAM)
			oSection1:Cell("TAM"):SetAlign("LEFT")
			
			oSection1:Cell("UNID_MEDIDA"):SetValue(FLW->UNID_MEDIDA)
			oSection1:Cell("UNID_MEDIDA"):SetAlign("LEFT")
			
			cSC1Comp:= Posicione('SY1',3,xFilial('SY1')+FLW->SC_COMPRADOR,"Y1_NOME")

			oSection1:Cell("SC_COMPRADOR"):SetValue(cSC1Comp)
			oSection1:Cell("SC_COMPRADOR"):SetAlign("LEFT")
			
			oSection1:Cell("SOLICITACAO"):SetValue(FLW->SOLICITACAO)
			oSection1:Cell("SOLICITACAO"):SetAlign("LEFT")
			
			oSection1:Cell("SOLICITANTE"):SetValue(FLW->SOLICITANTE)
			oSection1:Cell("SOLICITANTE"):SetAlign("LEFT")
			
			oSection1:Cell("DAT_EMISSAO_SC"):SetValue(STOD(FLW->DAT_EMISSAO_SC))
			oSection1:Cell("DAT_EMISSAO_SC"):SetAlign("LEFT")
			
			oSection1:Cell("QTDE_SOLICITADA"):SetValue(FLW->QTDE_SOLICITADA)
			oSection1:Cell("QTDE_SOLICITADA"):SetAlign("RIGHT")
			
			oSection1:Cell("TIPO_REQUISICAO"):SetValue(FLW->TIPO_REQUISICAO)
			oSection1:Cell("TIPO_REQUISICAO"):SetAlign("LEFT")	
			
			oSection1:Cell("COLECAO"):SetValue(FLW->COLECAO)
			oSection1:Cell("COLECAO"):SetAlign("LEFT")
			
			//+ Inserido por Robson, anteriormente estava trazendo C1_XSBCOLE e a SC1 encontra-se sem descricoes fieis
			cDscSubC:= Posicione('SB1',1,xFilial('SB1')+FLW->PRODUTO,"B1_YDESSCO")

			//oSection1:Cell("SUB_COLECAO"):SetValue(FLW->SUB_COLECAO)
			oSection1:Cell("SUB_COLECAO"):SetValue(cDscSubC)
			oSection1:Cell("SUB_COLECAO"):SetAlign("LEFT")
			
			oSection1:Cell("DAT_NEC_DEMANDA"):SetValue(STOD(FLW->DAT_NEC_DEMANDA))
			oSection1:Cell("DAT_NEC_DEMANDA"):SetAlign("LEFT")
			
			oSection1:Cell("QUANT_MOSTRUARIO"):SetValue(FLW->QUANT_MOSTRUARIO)
			oSection1:Cell("QUANT_MOSTRUARIO"):SetAlign("RIGHT")

			oSection1:Cell("DT_NEC_MOSTRUARIO"):SetValue(STOD(FLW->DT_NEC_MOSTRUARIO))
			oSection1:Cell("DT_NEC_MOSTRUARIO"):SetAlign("LEFT")

			oSection1:Cell("PEDIDO"):SetValue(FLW->PEDIDO)
			oSection1:Cell("PEDIDO"):SetAlign("LEFT")

			cSC7Comp:= Posicione('SY1',3,xFilial('SY1')+FLW->PED_COMPRADOR,"Y1_NOME") 

			oSection1:Cell("PED_COMPRADOR"):SetValue(cSC7Comp)
			oSection1:Cell("PED_COMPRADOR"):SetAlign("LEFT")
			
			oSection1:Cell("STATUS_APROVACAO"):SetValue(FLW->STATUS_APROVACAO)
			oSection1:Cell("STATUS_APROVACAO"):SetAlign("LEFT")
			
			oSection1:Cell("CODFORSC"):SetValue(FLW->CODFOR)
			oSection1:Cell("CODFORSC"):SetAlign("LEFT")
			
			oSection1:Cell("FORNE_SC"):SetValue(FLW->FORNE_SC)
			oSection1:Cell("FORNE_SC"):SetAlign("LEFT")

			oSection1:Cell("CODFORPED"):SetValue(FLW->CODFORPED)
			oSection1:Cell("CODFORPED"):SetAlign("LEFT")
			
			oSection1:Cell("FORNE_PED"):SetValue(FLW->FORNE_SC)
			oSection1:Cell("FORNE_PED"):SetAlign("LEFT")

			oSection1:Cell("UF"):SetValue(FLW->UF)
			oSection1:Cell("UF"):SetAlign("LEFT")
			
			oSection1:Cell("CCUSTO"):SetValue(FLW->CCUSTO)
			oSection1:Cell("CCUSTO"):SetAlign("LEFT")
			
			oSection1:Cell("DT_EMISSAO_PED"):SetValue(STOD(FLW->DT_EMISSAO_PED))
			oSection1:Cell("DT_EMISSAO_PED"):SetAlign("LEFT")
			
			oSection1:Cell("COND_PGTO"):SetValue(FLW->COND_PGTO)
			oSection1:Cell("COND_PGTO"):SetAlign("LEFT")
			
			oSection1:Cell("DESC_COND_PGTO"):SetValue(FLW->DESC_COND_PGTO)
			oSection1:Cell("DESC_COND_PGTO"):SetAlign("LEFT")
						
			oSection1:Cell("VLR_TOTAL_ITEM"):SetValue(FLW->VLR_TOTAL_ITEM)
			oSection1:Cell("VLR_TOTAL_ITEM"):SetAlign("RIGHT")
			
			oSection1:Cell("QTDE_PEDIDO"):SetValue(FLW->QTDE_PEDIDO)
			oSection1:Cell("QTDE_PEDIDO"):SetAlign("RIGHT")
			
			oSection1:Cell("DATA_ENTREGA"):SetValue(STOD(FLW->DATA_ENTREGA))
			oSection1:Cell("DATA_ENTREGA"):SetAlign("LEFT")
			
			oSection1:Cell("QTDE_ENTREGUE"):SetValue(FLW->QTDE_ENTREGUE)
			oSection1:Cell("QTDE_ENTREGUE"):SetAlign("RIGHT")
			
			If  FLW->QUITADO == "S"
				oSection1:Cell("SALDO"):SetValue(0)
			Else
				oSection1:Cell("SALDO"):SetValue(FLW->SALDO)
			Endif
	
			oSection1:Cell("SALDO"):SetAlign("RIGHT")
			
			oSection1:Cell("CUSTO"):SetValue(FLW->CUSTO)
			oSection1:Cell("CUSTO"):SetAlign("RIGHT")
			
			oSection1:Cell("QUITADO"):SetValue(FLW->QUITADO)
			oSection1:Cell("QUITADO"):SetAlign("LEFT")
				
			//Devolu��o
			oSection1:Cell("QTDE_DEVOLVIDA"):SetValue(FLW->QTDE_DEVOLVIDA)
			oSection1:Cell("QTDE_DEVOLVIDA"):SetAlign("LEFT")	
			
			oSection1:Cell("NF_ORIGEM_DEV"):SetValue(NfsOriDev)
			oSection1:Cell("NF_ORIGEM_DEV"):SetAlign("LEFT")
			
			oSection1:Cell("NF_DEVOLUCAO"):SetValue(NfsDevolucao)
			oSection1:Cell("NF_DEVOLUCAO"):SetAlign("LEFT")			

			//Verifica se existe SC7 para gravar o status do pedido (a = aberto, e = encerrado)  Solicitado pela Gilselly 22/01/2019
			If !Empty(Alltrim(FLW->PEDIDO))
				If FLW->SALDO < 0 .and. FLW->QUITADO <> "S"
					lEncerra:= .f.
					oSection1:Cell("ENCERRADO"):SetValue("")
					oSection1:Cell("ENCERRADO"):SetAlign("LEFT")	
				Else
					lEncerra:= .t.
					oSection1:Cell("ENCERRADO"):SetValue("E")
					oSection1:Cell("ENCERRADO"):SetAlign("LEFT")	
				Endif
			Endif
            
            //Nota 1
			oSection1:Cell("NOTA_1FISCAL"):SetValue(FLW->NOTA_1FISCAL)
			oSection1:Cell("NOTA_1FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_1FATURA"):SetValue(STOD(FLW->DATA_1FATURA))
			oSection1:Cell("DATA_1FATURA"):SetAlign("LEFT")

			oSection1:Cell("QTDE_1FATURA"):SetValue(FLW->QTDE_1FATURA)
			oSection1:Cell("QTDE_1FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_1"):SetValue(FLW->MODAL_1)
			oSection1:Cell("MODAL_1"):SetAlign("LEFT")

			oSection1:Cell("OBSERV_1"):SetValue(FLW->OBSERV_1)
			oSection1:Cell("OBSERV_1"):SetAlign("LEFT")

			oSection1:Cell("PREV_1RECEBIMENTO"):SetValue(STOD(FLW->PREV_1RECEBIMENTO))
			oSection1:Cell("PREV_1RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_1"):SetValue(Val(FLW->SALDO_1))
			oSection1:Cell("SALDO_1"):SetAlign("RIGHT")

			oSection1:Cell("DATA_1SALDO"):SetValue(STOD(FLW->DATA_1SALDO))
			oSection1:Cell("DATA_1SALDO"):SetAlign("LEFT")
			
            //Nota 2
			oSection1:Cell("NOTA_2FISCAL"):SetValue(FLW->NOTA_2FISCAL)
			oSection1:Cell("NOTA_2FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_2FATURA"):SetValue(STOD(FLW->DATA_2FATURA))
			oSection1:Cell("DATA_2FATURA"):SetAlign("LEFT")

			oSection1:Cell("QTDE_2FATURA"):SetValue(FLW->QTDE_2FATURA)
			oSection1:Cell("QTDE_2FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_2"):SetValue(FLW->MODAL_2)
			oSection1:Cell("MODAL_2"):SetAlign("LEFT")

			oSection1:Cell("OBSERV_2"):SetValue(FLW->OBSERV_2)
			oSection1:Cell("OBSERV_2"):SetAlign("LEFT")

			oSection1:Cell("PREV_2RECEBIMENTO"):SetValue(STOD(FLW->PREV_2RECEBIMENTO))
			oSection1:Cell("PREV_2RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_2"):SetValue(Val(FLW->SALDO_2))
			oSection1:Cell("SALDO_2"):SetAlign("RIGHT")

			oSection1:Cell("DATA_2SALDO"):SetValue(STOD(FLW->DATA_2SALDO))
			oSection1:Cell("DATA_2SALDO"):SetAlign("LEFT")
            
            //Nota 3
			oSection1:Cell("NOTA_3FISCAL"):SetValue(FLW->NOTA_3FISCAL)
			oSection1:Cell("NOTA_3FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_3FATURA"):SetValue(STOD(FLW->DATA_3FATURA))
			oSection1:Cell("DATA_3FATURA"):SetAlign("LEFT")

			oSection1:Cell("QTDE_3FATURA"):SetValue(FLW->QTDE_3FATURA)
			oSection1:Cell("QTDE_3FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_3"):SetValue(FLW->MODAL_3)
			oSection1:Cell("MODAL_3"):SetAlign("LEFT")

			oSection1:Cell("OBSERV_3"):SetValue(FLW->OBSERV_3)
			oSection1:Cell("OBSERV_3"):SetAlign("LEFT")

			oSection1:Cell("PREV_3RECEBIMENTO"):SetValue(STOD(FLW->PREV_3RECEBIMENTO))
			oSection1:Cell("PREV_3RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_3"):SetValue(Val(FLW->SALDO_3))
			oSection1:Cell("SALDO_3"):SetAlign("RIGHT")

			oSection1:Cell("DATA_3SALDO"):SetValue(STOD(FLW->DATA_3SALDO))
			oSection1:Cell("DATA_3SALDO"):SetAlign("LEFT")

            //Nota 4
			oSection1:Cell("NOTA_4FISCAL"):SetValue(FLW->NOTA_4FISCAL)
			oSection1:Cell("NOTA_4FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_4FATURA"):SetValue(STOD(FLW->DATA_4FATURA))
			oSection1:Cell("DATA_4FATURA"):SetAlign("LEFT")

			oSection1:Cell("QTDE_4FATURA"):SetValue(FLW->QTDE_4FATURA)
			oSection1:Cell("QTDE_4FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_4"):SetValue(FLW->MODAL_4)
			oSection1:Cell("MODAL_4"):SetAlign("LEFT")

			oSection1:Cell("OBSERV_4"):SetValue(FLW->OBSERV_4)
			oSection1:Cell("OBSERV_4"):SetAlign("LEFT")

			oSection1:Cell("PREV_4RECEBIMENTO"):SetValue(STOD(FLW->PREV_4RECEBIMENTO))
			oSection1:Cell("PREV_4RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_4"):SetValue(Val(FLW->SALDO_4))
			oSection1:Cell("SALDO_4"):SetAlign("RIGHT")

			oSection1:Cell("DATA_4SALDO"):SetValue(STOD(FLW->DATA_4SALDO))
			oSection1:Cell("DATA_4SALDO"):SetAlign("LEFT")

            //Nota 5
			oSection1:Cell("NOTA_5FISCAL"):SetValue(FLW->NOTA_5FISCAL)
			oSection1:Cell("NOTA_5FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_5FATURA"):SetValue(STOD(FLW->DATA_5FATURA))
			oSection1:Cell("DATA_5FATURA"):SetAlign("LEFT")
			
			oSection1:Cell("QTDE_5FATURA"):SetValue(FLW->QTDE_5FATURA)
			oSection1:Cell("QTDE_5FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_5"):SetValue(FLW->MODAL_5)
			oSection1:Cell("MODAL_5"):SetAlign("LEFT")

			oSection1:Cell("OBSERV_5"):SetValue(FLW->OBSERV_5)
			oSection1:Cell("OBSERV_5"):SetAlign("LEFT")

			oSection1:Cell("PREV_5RECEBIMENTO"):SetValue(STOD(FLW->PREV_5RECEBIMENTO))
			oSection1:Cell("PREV_5RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_5"):SetValue(Val(FLW->SALDO_5))
			oSection1:Cell("SALDO_5"):SetAlign("RIGHT")

			oSection1:Cell("DATA_5SALDO"):SetValue(STOD(FLW->DATA_5SALDO))
			oSection1:Cell("DATA_5SALDO"):SetAlign("LEFT")

			// Nota 6
			oSection1:Cell("NOTA_6FISCAL"):SetValue(FLW->NOTA_6FISCAL)
			oSection1:Cell("NOTA_6FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_6FATURA"):SetValue(STOD(FLW->DATA_6FATURA))
			oSection1:Cell("DATA_6FATURA"):SetAlign("LEFT")

			oSection1:Cell("QTDE_6FATURA"):SetValue(FLW->QTDE_6FATURA)
			oSection1:Cell("QTDE_6FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_6"):SetValue(FLW->MODAL_6)
			oSection1:Cell("MODAL_6"):SetAlign("LEFT")
			
			oSection1:Cell("OBSERV_6"):SetValue(FLW->OBSERV_6)
			oSection1:Cell("OBSERV_6"):SetAlign("LEFT")

			oSection1:Cell("PREV_6RECEBIMENTO"):SetValue(STOD(FLW->PREV_6RECEBIMENTO))
			oSection1:Cell("PREV_6RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_6"):SetValue(Val(FLW->SALDO_6))
			oSection1:Cell("SALDO_6"):SetAlign("RIGHT")

			oSection1:Cell("DATA_6SALDO"):SetValue(STOD(FLW->DATA_6SALDO))
			oSection1:Cell("DATA_6SALDO"):SetAlign("LEFT")

			// Nota 7
			oSection1:Cell("NOTA_7FISCAL"):SetValue(FLW->NOTA_7FISCAL)
			oSection1:Cell("NOTA_7FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_7FATURA"):SetValue(STOD(FLW->DATA_7FATURA))
			oSection1:Cell("DATA_7FATURA"):SetAlign("LEFT")

			oSection1:Cell("QTDE_7FATURA"):SetValue(FLW->QTDE_7FATURA)
			oSection1:Cell("QTDE_7FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_7"):SetValue(FLW->MODAL_7)
			oSection1:Cell("MODAL_7"):SetAlign("LEFT")
			
			oSection1:Cell("OBSERV_7"):SetValue(FLW->OBSERV_7)
			oSection1:Cell("OBSERV_7"):SetAlign("LEFT")

			oSection1:Cell("PREV_7RECEBIMENTO"):SetValue(STOD(FLW->PREV_7RECEBIMENTO))
			oSection1:Cell("PREV_7RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_7"):SetValue(Val(FLW->SALDO_7))
			oSection1:Cell("SALDO_7"):SetAlign("RIGHT")

			oSection1:Cell("DATA_7SALDO"):SetValue(STOD(FLW->DATA_7SALDO))
			oSection1:Cell("DATA_7SALDO"):SetAlign("LEFT")

			// Nota 8
			oSection1:Cell("NOTA_8FISCAL"):SetValue(FLW->NOTA_8FISCAL)
			oSection1:Cell("NOTA_8FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_8FATURA"):SetValue(STOD(FLW->DATA_8FATURA))
			oSection1:Cell("DATA_8FATURA"):SetAlign("LEFT")

			oSection1:Cell("QTDE_8FATURA"):SetValue(FLW->QTDE_8FATURA)
			oSection1:Cell("QTDE_8FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_8"):SetValue(FLW->MODAL_8)
			oSection1:Cell("MODAL_8"):SetAlign("LEFT")
			
			oSection1:Cell("OBSERV_8"):SetValue(FLW->OBSERV_8)
			oSection1:Cell("OBSERV_8"):SetAlign("LEFT")

			oSection1:Cell("PREV_8RECEBIMENTO"):SetValue(STOD(FLW->PREV_8RECEBIMENTO))
			oSection1:Cell("PREV_8RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_8"):SetValue(Val(FLW->SALDO_8))
			oSection1:Cell("SALDO_8"):SetAlign("RIGHT")

			oSection1:Cell("DATA_8SALDO"):SetValue(STOD(FLW->DATA_8SALDO))
			oSection1:Cell("DATA_8SALDO"):SetAlign("LEFT")

			// Nota 9
			oSection1:Cell("NOTA_9FISCAL"):SetValue(FLW->NOTA_9FISCAL)
			oSection1:Cell("NOTA_9FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_9FATURA"):SetValue(STOD(FLW->DATA_9FATURA))
			oSection1:Cell("DATA_9FATURA"):SetAlign("LEFT")

			oSection1:Cell("QTDE_9FATURA"):SetValue(FLW->QTDE_9FATURA)
			oSection1:Cell("QTDE_9FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_9"):SetValue(FLW->MODAL_9)
			oSection1:Cell("MODAL_9"):SetAlign("LEFT")
			
			oSection1:Cell("OBSERV_9"):SetValue(FLW->OBSERV_9)
			oSection1:Cell("OBSERV_9"):SetAlign("LEFT")

			oSection1:Cell("PREV_9RECEBIMENTO"):SetValue(STOD(FLW->PREV_9RECEBIMENTO))
			oSection1:Cell("PREV_9RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_9"):SetValue(Val(FLW->SALDO_9))
			oSection1:Cell("SALDO_9"):SetAlign("RIGHT")

			oSection1:Cell("DATA_9SALDO"):SetValue(STOD(FLW->DATA_9SALDO))
			oSection1:Cell("DATA_9SALDO"):SetAlign("LEFT")

			// Nota 10
			oSection1:Cell("NOTA_10FISCAL"):SetValue(FLW->NOTA_10FISCAL)
			oSection1:Cell("NOTA_10FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_10FATURA"):SetValue(STOD(FLW->DATA_10FATURA))
			oSection1:Cell("DATA_10FATURA"):SetAlign("LEFT")

			oSection1:Cell("QTDE_10FATURA"):SetValue(FLW->QTDE_10FATURA)
			oSection1:Cell("QTDE_10FATURA"):SetAlign("RIGHT")

			oSection1:Cell("MODAL_10"):SetValue(FLW->MODAL_10)
			oSection1:Cell("MODAL_10"):SetAlign("LEFT")
			
			oSection1:Cell("OBSERV_10"):SetValue(FLW->OBSERV_10)
			oSection1:Cell("OBSERV_10"):SetAlign("LEFT")

			oSection1:Cell("PREV_10RECEBIMENTO"):SetValue(STOD(FLW->PREV_10RECEBIMENTO))
			oSection1:Cell("PREV_10RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_10"):SetValue(Val(FLW->SALDO_10))
			oSection1:Cell("SALDO_10"):SetAlign("RIGHT")

			oSection1:Cell("DATA_10SALDO"):SetValue(STOD(FLW->DATA_10SALDO))
			oSection1:Cell("DATA_10SALDO"):SetAlign("LEFT")

			nTotFat := (FLW->QTDE_1FATURA+FLW->QTDE_2FATURA+FLW->QTDE_3FATURA+FLW->QTDE_4FATURA+FLW->QTDE_5FATURA+FLW->QTDE_6FATURA+FLW->QTDE_7FATURA+FLW->QTDE_8FATURA+FLW->QTDE_9FATURA+FLW->QTDE_10FATURA) 
            nTotTra := nTotFat - FLW->QTDE_ENTREGUE
			
			//Transito - Ajuste chamado 12775 - Ronaldo Pereira 20/05/20	
			If nTotTra <= 0 .Or. FLW->QTDE_ENTREGUE >= nTotFat
				nTransito:= 0
			Else
			    nTransito:= nTotTra
			EndIf				

			//Adicionado a pedido da Magda (Pedidos encerrados devem ser impressos com '0' Zero)
			If !lEncerra
				oSection1:Cell("TRANSITO"):SetValue(nTransito)
			Else
				oSection1:Cell("TRANSITO"):SetValue(0)
			Endif
			oSection1:Cell("TRANSITO"):SetAlign("RIGHT")

			//Saldo Final - Ajuste chamado 12775 - Ronaldo Pereira 20/05/20		
			If  FLW->SALDO >= 0 .OR. FLW->QUITADO == "S"
				oSection1:Cell("SALDO_FINAL"):SetValue(0)
				oSection1:Cell("SALDO_FINAL"):SetAlign("RIGHT")
			Else
			    nSldFim:= ABS(FLW->SALDO) - nTransito
				oSection1:Cell("SALDO_FINAL"):SetValue(nSldFim)
				oSection1:Cell("SALDO_FINAL"):SetAlign("RIGHT")
			EndIf
			
			//Percentual SALDO FINAL / QTDE_ENTREGUE
			nPercent := (nSldFim / FLW->QTDE_ENTREGUE)*100
			oSection1:Cell("PERCENTUAL"):SetValue(nPercent)
			oSection1:Cell("PERCENTUAL"):SetAlign("RIGHT")

			oSection1:PrintLine()

		FLW->(DBSKIP()) 

	enddo

	FLW->(DBCLOSEAREA())

Return( Nil )

//TODO NFs de Origem da Devolu�ao do Pedido de Compras. Ronaldo Pereira 12/08/2020
Static function NfOrigem(cCodFor,cPedCom,cProd)

Local NfsOrigem := ""
Local NfOrigem  := ""
Local cQry      := ""

    If Select("NFO") > 0
			NFO->(DbCloseArea())
	EndIf
		
	cQry := "SELECT D1.D1_DOC AS NF_ORIGEM_DEV "
	cQry += "FROM "+RetSqlName('SD2')+" AS D2 WITH (NOLOCK) "
	cQry += "INNER JOIN "+RetSqlName('SD1')+" AS D1 WITH (NOLOCK) ON (D1.D1_FILIAL = D2.D2_FILIAL " 
	cQry += "                                      AND D1.D1_FORNECE = D2.D2_CLIENTE "
	cQry += "									   AND D1.D1_DOC = D2.D2_NFORI "
	cQry += "									   AND D1.D1_COD = D2.D2_COD "
	cQry += "									   AND D1.D_E_L_E_T_ = '') "
	cQry += "WHERE D2.D_E_L_E_T_ = '' AND D2_TIPO = 'D' AND D2_CLIENTE = '"+cCodFor+"' AND D1_PEDIDO = '"+cPedCom+"' AND D2_COD = '"+cProd+"' "

    TCQUERY cQry NEW ALIAS NFO
    
    IF !Empty(NFO->NF_ORIGEM_DEV)		    
    	While NFO->(!EOF()) 
    		NfOrigem := NFO->NF_ORIGEM_DEV
			NfsOrigem += NfOrigem +"/"
		   	DbSkip()
	    EndDo	
	    NfsOrigem := SubStr(NfsOrigem,1,Len(NfsOrigem)-1)	
	EndIf
	
Return NfsOrigem



//TODO NFs de Devolu�ao do Pedido de Compras. Ronaldo Pereira 12/08/2020
Static function NfDevolucao(cCodFor,cPedCom,cProd)

Local NfsDev := ""
Local NfDev  := ""
Local cQry   := ""

    If Select("NFD") > 0
			NFD->(DbCloseArea())
	EndIf
		
	cQry := "SELECT D2.D2_DOC AS NF_DEVOLUCAO "
	cQry += "FROM "+RetSqlName('SD2')+" AS D2 WITH (NOLOCK) "
	cQry += "INNER JOIN "+RetSqlName('SD1')+" AS D1 WITH (NOLOCK) ON (D1.D1_FILIAL = D2.D2_FILIAL " 
	cQry += "                                      AND D1.D1_FORNECE = D2.D2_CLIENTE "
	cQry += "									   AND D1.D1_DOC = D2.D2_NFORI "
	cQry += "									   AND D1.D1_COD = D2.D2_COD "
	cQry += "									   AND D1.D_E_L_E_T_ = '') "
	cQry += "WHERE D2.D_E_L_E_T_ = '' AND D2_TIPO = 'D' AND D2_CLIENTE = '"+cCodFor+"' AND D1_PEDIDO = '"+cPedCom+"' AND D2_COD = '"+cProd+"' "
	cQry += "GROUP BY D1_FILIAL,D1_PEDIDO,D1_FORNECE,D2_COD,D2.D2_DOC "

    TCQUERY cQry NEW ALIAS NFD
    
    IF !Empty(NFD->NF_DEVOLUCAO)		    
    	While NFD->(!EOF()) 
    		NfDev := NFD->NF_DEVOLUCAO
			NfsDev += NfDev +"/"
		   	DbSkip()
	    EndDo	
	    NfsDev := SubStr(NfsDev,1,Len(NfsDev)-1)	
	EndIf
	
Return NfsDev

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 10 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Tipo de Produto?"            		,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Solicitacao?"		    			,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Referencia ?"	                	,""		,""		,"mv_ch3","C",15,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Data da Solicitacao de?"	  	  	,""		,""		,"mv_ch4","D",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Data da Solicitacao ate?"	    	,""		,""		,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "06","Situacao"	   						,""		,""		,"mv_ch6","N",01,0,1,"C",""	,""	,"","","mv_par06","Aberto","","","","Finalizado","","","","Todos","","","","","","","")
Return
