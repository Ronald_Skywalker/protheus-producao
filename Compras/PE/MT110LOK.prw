#include 'protheus.ch'

/*/{Protheus.doc} MT110LOK
// Obrigatoriedade do centro de custo solicita��o de compra
@author Jos� Carlos Ferrari
@since 30/12/2019
@version 1.0
@type function
/*/

User Function MT110LOK()

	// Declaração de variaveis
	Local _lRet		:= .T.
	Local _nPosCc	:= aScan(aHeader,{|x| AllTrim(x[2]) == 'C1_CC'})
	Local _nPosRat  := aScan(aHeader,{|x| AllTrim(x[2]) == 'C1_RATEIO'})
	Local nCols 	:= ''
	
	For nCols:= 1 to Len(aCols)
		If Empty(Alltrim(aCols[n][_nPosCc]))
			If Alltrim(aCols[n][_nPosRat]) == '2'
				_lRet := .F.
			
			EndIf
			
		Endif	
		
	Next

	If _lRet == .F.
		MsgStop('Preencha o Centro de Custo do Item caso não tenha Rateio!!')

	EndIf

Return _lRet