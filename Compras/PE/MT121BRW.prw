#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#include "TOTVS.CH"

#define MB_ICONHAND                 16
#define MB_ICONEXCLAMATION          48

/*/{Protheus.doc} MT121BRW
	Ponto de entrada para altera��o de campos no pedido de compras sem solicitar aprova��o. 
																	
	@param xParam Parameter Description
	@return xRet Return Description
	@author  Weskley Silva	                                               
	@since 15/07/2018                                                   
/*/

User Function MT121BRW()

aAdd( aRotina, { "Alt Pedido"		, "U_FUNCAOALT()"	, 4, 0, 4 } ) 
aAdd( aRotina, { "Eliminar Residuo"	, "U_ELIMIN()"		, 5, 0, 5 } ) 
aAdd( aRotina, { "Reenviar WorkFlow", "U_WFREENV()"		, 5, 0, 5 } ) 
	
return

User Function FUNCAOALT()

	Local oButton1
	Local oButton2
	Local oButton3
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oSay1
	Local oSay2
	Local lAltera		:= .T.
	Private cGetPedido	:= SC7->C7_NUM
	Private cFornece    := SC7->C7_FORNECE
	Private cLoja       := SC7->C7_LOJA
	Private cGetFornece	:= POSICIONE("SA2",1,xFilial("SA2")+SC7->C7_FORNECE+SC7->C7_LOJA,"A2_NREDUZ")
	Private cGetCodPrd	:= Space(8)
	Private cColuna		:= Space(6)
		
	Static oDlg
		
		DEFINE MSDIALOG oDlg TITLE "Follow up Compras " 	FROM 000, 000  TO 555, 990 COLORS 0, 16777215 PIXEL
	
		
		@ 006, 007 SAY oSay1 PROMPT "Pedido:"				SIZE 037, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 004, 044 MSGET oGet1 VAR cGetPedido				SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 006, 118 SAY oSay2 PROMPT "Fornecedor:"			SIZE 031, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 004, 149 MSGET oGet2 VAR cGetFornece				SIZE 341, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		
		fMSNewGe1(0,lAltera)
		
		@ 258, 408 BUTTON oButton2 PROMPT "Cancela"			SIZE 037, 012 ACTION Close(oDlg)	OF oDlg PIXEL
		@ 258, 453 BUTTON oButton1 PROMPT "Confirma"		SIZE 037, 012 ACTION Save()			OF oDlg PIXEL
		
		
		ACTIVATE MSDIALOG oDlg CENTERED
	  	
Return

Static Function fMSNewGe1(_tp,Altera)

	Local nX
	Local aHeaderEx 	:= {}
	Local aColsEx 		:= {}
	Local aFieldFill 	:= {}

	Local aFields 		:= {"C7_ITEM","C7_PRODUTO","C7_DESCRI","C7_QUANT","C7_PRECO";
							,"C7_X1NF","C7_X1DT_FA","C7_X1QTD_F","C7_X1MODAL","C7_X1OBS","C7_X1PRREC","C7_X1SALDO","C7_X1DTSAL";
							,"C7_X2NF","C7_X2DT_FA","C7_X2QTD_F","C7_X2MODAL","C7_X2OBS","C7_X2PRREC","C7_X2SALDO","C7_X2DTSA";
							,"C7_X3NF","C7_X3DT_FA","C7_X3QTD_F","C7_X3MODAL","C7_X3OBS","C7_X3PRREC","C7_X3SALDO","C7_X3DTSAL";
							,"C7_X4NF","C7_X4D_FA","C7_X4QTD_F","C7_X4MODAL","C7_X4OBS","C7_X4PRRE","C7_X4SALDO","C7_X4DTSA";
							,"C7_X5NF","C7_X5D_FA","C7_X5QTD_F","C7_X5MODAL","C7_X5OBS","C7_X5PRRE","C7_X5SALDO","C7_X5DSAL";
							,"C7_X6NF","C7_X6DT_FA","C7_X6QTD_F","C7_X6MODAL","C7_X6PRREC","C7_X6SALDO","C7_X6DTSAL";
							,"C7_X7NF","C7_X7DT_FA","C7_X7QTD_F","C7_X7MODAL","C7_X7PRREC","C7_X7SALDO","C7_X7DTSAL";
							,"C7_X8NF","C7_X8DT_FA","C7_X8QTD_F","C7_X8MODAL","C7_X8PRREC","C7_X8SALDO","C7_X8DTSAL";
							,"C7_X9NF","C7_X9DT_FA","C7_X9QTD_F","C7_X9MODAL","C7_X9PRREC","C7_X9SALDO","C7_X9DTSAL";
							,"C7_XANF","C7_XADT_FA","C7_XAQTD_F","C7_XAMODAL","C7_XAPRREC","C7_XASALDO","C7_XADTSAL"}

   Local aAlterFields 	:= {"C7_X1NF","C7_X1DT_FA","C7_X1QTD_F","C7_X1MODAL","C7_X1OBS","C7_X1PRREC","C7_X1SALDO","C7_X1DTSAL";
   							,"C7_X2NF","C7_X2DT_FA","C7_X2QTD_F","C7_X2MODAL","C7_X2OBS","C7_X2PRREC","C7_X2SALDO","C7_X2DTSA","C7_X2DTSA";
							,"C7_X3NF","C7_X3DT_FA","C7_X3QTD_F","C7_X3MODAL","C7_X3OBS","C7_X3PRREC","C7_X3SALDO","C7_X3DTSAL";
							,"C7_X4NF","C7_X4D_FA","C7_X4QTD_F","C7_X4MODAL","C7_X4OBS","C7_X4PRRE","C7_X4SALDO","C7_X4DTSA";
							,"C7_X5NF","C7_X5D_FA","C7_X5QTD_F","C7_X5MODAL","C7_X5OBS","C7_X5PRRE","C7_X5SALDO","C7_X5DSAL";
							,"C7_X6NF","C7_X6DT_FA","C7_X6QTD_F","C7_X6MODAL","C7_X6PRREC","C7_X6SALDO","C7_X6DTSAL";
							,"C7_X7NF","C7_X7DT_FA","C7_X7QTD_F","C7_X7MODAL","C7_X7PRREC","C7_X7SALDO","C7_X7DTSAL";
							,"C7_X8NF","C7_X8DT_FA","C7_X8QTD_F","C7_X8MODAL","C7_X8PRREC","C7_X8SALDO","C7_X8DTSAL";
							,"C7_X9NF","C7_X9DT_FA","C7_X9QTD_F","C7_X9MODAL","C7_X9PRREC","C7_X9SALDO","C7_X9DTSAL";
							,"C7_XANF","C7_XADT_FA","C7_XAQTD_F","C7_XAMODAL","C7_XAPRREC","C7_XASALDO","C7_XADTSAL"}

	Local cProduto		:= SubString(cGetCodPrd,1,15)
	
	Static oMSGrade
		
	SX3->(DbSetOrder(2))
	For nX := 1 to len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			//Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"","R","",""})
			Aadd(aHeaderEx, {alltrim(X3Descric()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"","R","",""})
		Endif
	Next

	cNum	:= SC7->C7_NUM

	cQuery  := " SELECT * FROM "+RetSqlName("SC7")+" SC7 (NOLOCK) WHERE C7_NUM = '"+Alltrim(cNum)+"' AND D_E_L_E_T_ = '' AND C7_FILIAL = '"+xFILIAL("SC7")+"' "

	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	
	DbSelectArea("TMP")

	While TMP->(!EOF())

		Aadd(aFieldFill, TMP->C7_ITEM)						//01
		Aadd(aFieldFill, TMP->C7_PRODUTO)					//02
		Aadd(aFieldFill, TMP->C7_DESCRI)					//03
		
		Aadd(aFieldFill, TMP->C7_QUANT)						//04
		Aadd(aFieldFill, TMP->C7_PRECO)						//05
							
		Aadd(aFieldFill, TMP->C7_X1NF)						//06
		Aadd(aFieldFill, STOD(TMP->C7_X1DT_FA))				//07
		Aadd(aFieldFill, TMP->C7_X1QTD_F)					//08
		Aadd(aFieldFill, TMP->C7_X1MODAL)					//09
		Aadd(aFieldFill, TMP->C7_X1OBS)						//10
		Aadd(aFieldFill, STOD(TMP->C7_X1PRREC))				//11
		Aadd(aFieldFill, TMP->C7_X1SALDO)					//12
		Aadd(aFieldFill, STOD(TMP->C7_X1DTSAL))				//13
		Aadd(aFieldFill, TMP->C7_X2NF)						//14
		Aadd(aFieldFill, STOD(TMP->C7_X2DT_FA))				//05
		Aadd(aFieldFill, TMP->C7_X2QTD_F)					//06
		Aadd(aFieldFill, TMP->C7_X2MODAL)					//07
		Aadd(aFieldFill, TMP->C7_X2OBS)						//08
		Aadd(aFieldFill, STOD(TMP->C7_X2PRREC))				//09
		Aadd(aFieldFill, TMP->C7_X2SALDO)					//20
		Aadd(aFieldFill, STOD(TMP->C7_X2DTSA))				//01
		Aadd(aFieldFill, TMP->C7_X3NF)						//02
		Aadd(aFieldFill, STOD(TMP->C7_X3DT_FA))				//03
		Aadd(aFieldFill, TMP->C7_X3QTD_F)					//04
		Aadd(aFieldFill, TMP->C7_X3MODAL)					//05
		Aadd(aFieldFill, TMP->C7_X3OBS)						//06
		Aadd(aFieldFill, STOD(TMP->C7_X3PRREC))				//07
		Aadd(aFieldFill, TMP->C7_X3SALDO)					//08
		Aadd(aFieldFill, STOD(TMP->C7_X3DTSAL))				//09
		Aadd(aFieldFill, TMP->C7_X4NF)						//30
		Aadd(aFieldFill, STOD(TMP->C7_X4D_FA))				//01
		Aadd(aFieldFill, TMP->C7_X4QTD_F)					//02
		Aadd(aFieldFill, TMP->C7_X4MODAL)					//03
		Aadd(aFieldFill, TMP->C7_X4OBS)						//04
		Aadd(aFieldFill, STOD(TMP->C7_X4PRRE))				//05
		Aadd(aFieldFill, TMP->C7_X4SALDO)					//06
		Aadd(aFieldFill, STOD(TMP->C7_X4DTSA))				//07
		Aadd(aFieldFill, TMP->C7_X5NF)						//08
		Aadd(aFieldFill, STOD(TMP->C7_X5D_FA))				//09
		Aadd(aFieldFill, TMP->C7_X5QTD_F)					//==> 40
		Aadd(aFieldFill, TMP->C7_X5MODAL)					//01
		Aadd(aFieldFill, TMP->C7_X5OBS)						//02
		Aadd(aFieldFill, STOD(TMP->C7_X5PRRE))				//03
		Aadd(aFieldFill, TMP->C7_X5SALDO)					//04
		Aadd(aFieldFill, STOD(TMP->C7_X5DSAL))				//05

		Aadd(aFieldFill, TMP->C7_X6NF)						//06
		Aadd(aFieldFill, STOD(TMP->C7_X6DT_FA))				//07
		Aadd(aFieldFill, TMP->C7_X6QTD_F)					//08
		Aadd(aFieldFill, TMP->C7_X6MODAL)					//09
		Aadd(aFieldFill, STOD(TMP->C7_X6PRREC))				//==> 50
		Aadd(aFieldFill, TMP->C7_X6SALDO)					//01
		Aadd(aFieldFill, STOD(TMP->C7_X6DTSAL))				//02

		Aadd(aFieldFill, TMP->C7_X7NF)						//03
		Aadd(aFieldFill, STOD(TMP->C7_X7DT_FA))				//04
		Aadd(aFieldFill, TMP->C7_X7QTD_F)					//05
		Aadd(aFieldFill, TMP->C7_X7MODAL)					//06
		Aadd(aFieldFill, STOD(TMP->C7_X7PRREC))				//07
		Aadd(aFieldFill, TMP->C7_X7SALDO)					//08
		Aadd(aFieldFill, STOD(TMP->C7_X7DTSAL))				//09

		Aadd(aFieldFill, TMP->C7_X8NF)						//==> 60
		Aadd(aFieldFill, STOD(TMP->C7_X8DT_FA))				//01
		Aadd(aFieldFill, TMP->C7_X8QTD_F)					//02
		Aadd(aFieldFill, TMP->C7_X8MODAL)					//03
		Aadd(aFieldFill, STOD(TMP->C7_X8PRREC))				//04
		Aadd(aFieldFill, TMP->C7_X8SALDO)					//05
		Aadd(aFieldFill, STOD(TMP->C7_X8DTSAL))				//06

		Aadd(aFieldFill, TMP->C7_X9NF)						//07
		Aadd(aFieldFill, STOD(TMP->C7_X9DT_FA))				//08
		Aadd(aFieldFill, TMP->C7_X9QTD_F)					//09
		Aadd(aFieldFill, TMP->C7_X9MODAL)					//==> 70
		Aadd(aFieldFill, STOD(TMP->C7_X9PRREC))				//01
		Aadd(aFieldFill, TMP->C7_X9SALDO)					//02
		Aadd(aFieldFill, STOD(TMP->C7_X9DTSAL))				//03

		Aadd(aFieldFill, TMP->C7_XANF)						//04
		Aadd(aFieldFill, STOD(TMP->C7_XADT_FA))				//05
		Aadd(aFieldFill, TMP->C7_XAQTD_F)					//06
		Aadd(aFieldFill, TMP->C7_XAMODAL)					//07
		Aadd(aFieldFill, STOD(TMP->C7_XAPRREC))				//08
		Aadd(aFieldFill, TMP->C7_XASALDO)					//09
		Aadd(aFieldFill, STOD(TMP->C7_XADTSAL))				//==> 80

		Aadd(aFieldFill, 0)
		Aadd(aColsEx, aFieldFill)
		
		aFieldFill :={}
		
		DbSkip()
			
	End
	DbClosearea()
	
	oMSGrade := MsNewGetDados():New( 040, 007, 252, 491,GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
		
	//oMSGrade := MsNewGetDados():New( 040, 007, 252, 491, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
		
	//If _tp == 1
	//	oDlg:refresh()
	//	oMSGrade:refresh()
	//End

Return

Static Function Save()

Local i := 0

For i := 1 to Len(OMSGrade:ACOLS)

	dbSelectArea("SC7")
	SC7->(dbSetOrder(2))  
		
	If SC7->(dbSeek(xFilial("SC7")+OMSGrade:ACOLS[i,2]+cFornece+cLoja+cGetPedido)) 
	
		cQuant1 := OMSGrade:ACOLS[i,8]
		cQuant2 := OMSGrade:ACOLS[i,16]
		cQuant3 := OMSGrade:ACOLS[i,24]
		cQuant4 := OMSGrade:ACOLS[i,32]
		cQuant5 := OMSGrade:ACOLS[i,40]
		cQuant6 := OMSGrade:ACOLS[i,48]
		cQuant7 := OMSGrade:ACOLS[i,55]
		cQuant8 := OMSGrade:ACOLS[i,62]
		cQuant9 := OMSGrade:ACOLS[i,69]		
		cQuant10 := OMSGrade:ACOLS[i,76]

		nSaldo1 :=  SC7->C7_QUANT  - cQuant1
		nSaldo2 :=  nSaldo1 - cQuant2
		nSaldo3 :=  nSaldo2 - cQuant3
		nSaldo4 :=  nSaldo3 - cQuant4
		nSaldo5 :=  nSaldo4 - cQuant5
		nSaldo6 :=  nSaldo5 - cQuant6
		nSaldo7 :=  nSaldo6 - cQuant7
		nSaldo8 :=  nSaldo7 - cQuant8
		nSaldo9 :=  nSaldo8 - cQuant9
		nSaldo10 :=  nSaldo9 - cQuant10

		RecLock("SC7",.F.)     
	   	SC7->C7_X1NF	 := OMSGrade:ACOLS[i,6]
		SC7->C7_X1DT_FA	 := OMSGrade:ACOLS[i,7]
		SC7->C7_X1QTD_F	 := OMSGrade:ACOLS[i,8]
		SC7->C7_X1MODAL	 := OMSGrade:ACOLS[i,9]	
		SC7->C7_X1OBS	 := OMSGrade:ACOLS[i,10]
		SC7->C7_X1PRREC	 := OMSGrade:ACOLS[i,11]
		SC7->C7_X1SALDO	 := cValtoChar(nSaldo1)
		SC7->C7_X1DTSAL	 := OMSGrade:ACOLS[i,13]
		
		SC7->C7_X2NF	 := OMSGrade:ACOLS[i,14]
		SC7->C7_X2DT_FA	 := OMSGrade:ACOLS[i,15]
		SC7->C7_X2QTD_F	 := OMSGrade:ACOLS[i,16]
		SC7->C7_X2MODAL	 := OMSGrade:ACOLS[i,17]
		SC7->C7_X2OBS	 := OMSGrade:ACOLS[i,18]
		SC7->C7_X2PRREC	 := OMSGrade:ACOLS[i,19]
		SC7->C7_X2SALDO	 := cValtoChar(nSaldo2)
		SC7->C7_X2DTSA	 := OMSGrade:ACOLS[i,21]
		
		SC7->C7_X3NF	 := OMSGrade:ACOLS[i,22]
		SC7->C7_X3DT_FA	 := OMSGrade:ACOLS[i,23]
		SC7->C7_X3QTD_F	 := OMSGrade:ACOLS[i,24]
		SC7->C7_X3MODAL	 := OMSGrade:ACOLS[i,25]
		SC7->C7_X3OBS	 := OMSGrade:ACOLS[i,26]
		SC7->C7_X3PRREC	 := OMSGrade:ACOLS[i,27]
		SC7->C7_X3SALDO	 := cValtoChar(nSaldo3)
		SC7->C7_X3DTSAL	 := OMSGrade:ACOLS[i,29]
		
		SC7->C7_X4NF	 := OMSGrade:ACOLS[i,30]
		SC7->C7_X4D_FA	 := OMSGrade:ACOLS[i,31]
		SC7->C7_X4QTD_F	 := OMSGrade:ACOLS[i,32]
		SC7->C7_X4MODAL	 := OMSGrade:ACOLS[i,33]	
		SC7->C7_X4OBS	 := OMSGrade:ACOLS[i,34]
		SC7->C7_X4PRRE	 := OMSGrade:ACOLS[i,35]
		SC7->C7_X4SALDO	 := cValtoChar(nSaldo4)
		SC7->C7_X4DTSA	 := OMSGrade:ACOLS[i,37]
		
		SC7->C7_X5NF	 := OMSGrade:ACOLS[i,38]
		SC7->C7_X5D_FA	 := OMSGrade:ACOLS[i,39]
		SC7->C7_X5QTD_F	 := OMSGrade:ACOLS[i,40]
		SC7->C7_X5MODAL	 := OMSGrade:ACOLS[i,41]
		SC7->C7_X5OBS	 := OMSGrade:ACOLS[i,42]
		SC7->C7_X5PRRE	 := OMSGrade:ACOLS[i,43]
		SC7->C7_X5SALDO	 := cValtoChar(nSaldo5)
		SC7->C7_X5DSAL   := OMSGrade:ACOLS[i,45]	

	   	SC7->C7_X6NF	 := OMSGrade:ACOLS[i,46]
		SC7->C7_X6DT_FA	 := OMSGrade:ACOLS[i,47]
		SC7->C7_X6QTD_F	 := OMSGrade:ACOLS[i,48]
		SC7->C7_X6MODAL	 := OMSGrade:ACOLS[i,49]	
		SC7->C7_X6PRREC	 := OMSGrade:ACOLS[i,50]
		SC7->C7_X6SALDO	 := cValtoChar(nSaldo6)
		SC7->C7_X6DTSAL	 := OMSGrade:ACOLS[i,52]

	   	SC7->C7_X7NF	 := OMSGrade:ACOLS[i,53]
		SC7->C7_X7DT_FA	 := OMSGrade:ACOLS[i,54]
		SC7->C7_X7QTD_F	 := OMSGrade:ACOLS[i,55]
		SC7->C7_X7MODAL	 := OMSGrade:ACOLS[i,56]	
		SC7->C7_X7PRREC	 := OMSGrade:ACOLS[i,57]
		SC7->C7_X7SALDO	 := cValtoChar(nSaldo7)
		SC7->C7_X7DTSAL	 := OMSGrade:ACOLS[i,59]

	   	SC7->C7_X8NF	 := OMSGrade:ACOLS[i,60]
		SC7->C7_X8DT_FA	 := OMSGrade:ACOLS[i,61]
		SC7->C7_X8QTD_F	 := OMSGrade:ACOLS[i,62]
		SC7->C7_X8MODAL	 := OMSGrade:ACOLS[i,63]	
		SC7->C7_X8PRREC	 := OMSGrade:ACOLS[i,64]
		SC7->C7_X8SALDO	 := cValtoChar(nSaldo8)
		SC7->C7_X8DTSAL	 := OMSGrade:ACOLS[i,66]

	   	SC7->C7_X9NF	 := OMSGrade:ACOLS[i,67]
		SC7->C7_X9DT_FA	 := OMSGrade:ACOLS[i,68]
		SC7->C7_X9QTD_F	 := OMSGrade:ACOLS[i,69]
		SC7->C7_X9MODAL	 := OMSGrade:ACOLS[i,70]	
		SC7->C7_X9PRREC	 := OMSGrade:ACOLS[i,71]
		SC7->C7_X9SALDO	 := cValtoChar(nSaldo9)
		SC7->C7_X9DTSAL	 := OMSGrade:ACOLS[i,73]

	   	SC7->C7_XANF	 := OMSGrade:ACOLS[i,74]
		SC7->C7_XADT_FA	 := OMSGrade:ACOLS[i,75]
		SC7->C7_XAQTD_F	 := OMSGrade:ACOLS[i,76]
		SC7->C7_XAMODAL	 := OMSGrade:ACOLS[i,77]	
		SC7->C7_XAPRREC	 := OMSGrade:ACOLS[i,78]
		SC7->C7_XASALDO	 := cValtoChar(nSaldo10)
		SC7->C7_XADTSAL	 := OMSGrade:ACOLS[i,80]
		SC7->(MsUnlock())

	Endif

Next i
	
Close(oDlg)

Return

User Function ELIMIN()

	Local oButton1
	Local oButton2
	Local oButton3
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oSay1
	Local oSay2
	Local lAltera		:= .T.
	Private cGetPedido	:= SC7->C7_NUM
	Private cFornece    := SC7->C7_FORNECE
	Private cLoja       := SC7->C7_LOJA
	Private cGetFornece	:= POSICIONE("SA2",1,xFilial("SA2")+SC7->C7_FORNECE+SC7->C7_LOJA,"A2_NREDUZ")
	Private cGetCodPrd	:= Space(8)
	Private cColuna		:= Space(6)
		
	Static oDlg1
	
	fMSNewGe2(0,lAltera)
	 	
Return

Static Function fMSNewGe2(_tp,Altera)

Local _astru   := {}
Local _afields := {}     
Local _carq    := ""         
Local cQuery   := "" 
Local xFornec := ""
Local xLoja   := ""
Local xVecto  := ctod(space(8))
Local nValor  := 0
Local xItens  := ""
Local xRazao  := ""
Local cFiltraSCR
Local ca097Usr 	:= __cuserid//'000091'//RetCodUsr()

Private cDescXPTO := " Fornecedor: "+cGetFornece+" "  
Private lMarcar  := .F.
//Private oDlg
Private oMark
Private aRotina   := {} 
Private lContinua := .F.
Private cAtuar     := ""
Private cMark     := GetMark() 
Private aScreens  := getScreenRes()

IF Select("TMP") > 0
	TMP->(dbCloseArea())
Endif

cQuery := " SELECT C7_NUM,C7_ITEM,C7_PRODUTO,C7_DESCRI,C7_QUANT FROM "+RetSqlName("SC7")+" WHERE C7_NUM = '"+SC7->C7_NUM+"' AND C7_FORNECE = '"+SC7->C7_FORNECE+"' AND C7_RESIDUO = '' AND C7_ENCER = '' AND D_E_L_E_T_ = '' "

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.) 

//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"     ,"C",  2,0})
AADD(_astru,{"C7_NUM"    ,"C",  6,0})
AADD(_astru,{"C7_ITEM"    ,"C",  6,0})
AADD(_astru,{"C7_PRODUTO" ,"C",  15,0})
AADD(_astru,{"C7_DESCRI"   ,"C",  120,0})
AADD(_astru,{"C7_QUANT","N", 16,6})

cArqTrab  := CriaTrab(_astru)
If Select("XTRB") > 0
	XTRB->(DbCloseArea())
EndIf

dbUseArea( .T.,, cArqTrab, "XTRB", .F., .F. )

Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		DbSelectArea("XTRB")        
		RecLock("XTRB",.T.) 
		XTRB->C7_NUM     := TMP->C7_NUM 
		XTRB->C7_ITEM    := TMP->C7_ITEM                
		XTRB->C7_PRODUTO := TMP->C7_PRODUTO 
		XTRB->C7_DESCRI  := TMP->C7_DESCRI                
		XTRB->C7_QUANT   := TMP->C7_QUANT
		MsUnlock()        
		TMP->(DbSkip())
	EndDo
EndIf	

//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('XTRB')    

//define as colunas para o browse
aColunas := {;
{"Pedido"          ,"C7_NUM"     ,"C",  6,0,"@!"},;
{"Item"       	   ,"C7_ITEM"     ,"C",  6,0,"@!"},;
{"Produto"         ,"C7_PRODUTO"  ,"C",  15,0,"@!"},;
{"Descri��o"       ,"C7_DESCRI"   ,"C",  120,0,"@!"},;
{"Quantidade"      ,"C7_QUANT"    ,"N", 16,6,"@!"}}

If aScreens[2] >= 800		//resolu��o da tela
	DEFINE MSDIALOG oDlg1 TITLE "Eliminar Residuos" FROM 000, 000  TO 705, 1400 COLORS 0, 16777215 PIXEL
	_nTam		:= 74
Else
	DEFINE MSDIALOG oDlg1 TITLE "Eliminar Residuos" FROM 000, 000  TO 590, 1300 COLORS 0, 16777215 PIXEL
	_nTam		:= 66
End

oMark:SetOwner(oDlg1)

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.F.)

oMark:SetDescription(cDescXPTO)
oMark:SetFieldMark('MK_OK')
                              
oMark:AddButton("Eliminar Residuo ","U_SALVA(oMark,1)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_APRINVE(oMark:Mark(),)",,2,0)

//Indica o Code-Block executado ap�s a marca��o ou desmarca��o do registro
oMark:SetAfterMark({|| fMVCCONMK()})
oMark:SetDoubleClick({|| fMVCCONMK() })
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || fMVCCONMK()})
oMark:SetAllMark({ || MarkRec()})
oMark:SetTemporary()

//Ativando a janela
oMark:Activate()

oDlg1:lMaximized := .T. //Maximiza a janela
					
ACTIVATE MSDIALOG oDlg1 CENTERED

Return

Static Function fMVCCONMK()
Local cMarca:= oMark:Mark()
Local aAreaTRB:= XTRB->(GetArea())
Local nValMk  := 0
//Local cDescXPTO:= 'Aprova��o de Compras - '

dbSelectArea("XTRB")
XTRB->(dbGoTop())
While !XTRB->(Eof())
    If XTRB->MK_OK == cMarca
    Endif
    XTRB->(dbSkip())
EndDo

RestArea( aAreaTRB )

Return 

User Function APRINVE(cMarca)
Local aAreaTRB  := XTRB->(GetArea())
Local nValMk  := 0

lMarcar := !lMarcar 
 
dbSelectArea("XTRB")
XTRB->(dbGoTop())
While !XTRB->(Eof())
    RecLock("XTRB",.F.)
    XTRB->MK_OK := IIf(lMarcar,cMarca,'  ')
    If XTRB->MK_OK == cMarca
    Endif
    MsUnlock()
    XTRB->(dbSkip())
EndDo

RestArea( aAreaTRB )
oDlg1:Refresh()
Return 

User Function SALVA(oMark)
Local nCont := 0 
Local aRet := {} 
Local aAreaTRB := GetArea() 
Local i := 0                

IF __cuserid $ GETMV("HP_USERELI")

	XTRB->(DbGoTop())
	While XTRB->(!Eof())
		If !Empty(XTRB->MK_OK) //Se diferente de vazio, foi marcado
		aAdd(aRet,{XTRB->C7_NUM,XTRB->C7_ITEM,XTRB->C7_PRODUTO})       
			nCont++
		Endif
		XTRB->( dbSkip() )
	EndDo
		
	If nCont == 0
		Alert("Selecione pelo menos um registro.")
		RestArea(aAreaTRB)
		lContinua := .F.
		Return
	else
		if MsgYesNo("Confirma a elimina��o de residuos ?","Hope")
			For i := 1 to Len(aRet)
			cQuery:= "UPDATE "+RetSqlName("SC7")+" SET C7_RESIDUO = 'S' , C7_ENCER = 'E'  WHERE C7_NUM = '"+aRet[i][1]+"' AND C7_ITEM = '"+aRet[i][2]+"' AND C7_PRODUTO = '"+aRet[i][3]+"' and D_E_L_E_T_  = ''"
			TcSQLExec(cQuery)
				
			next i
		else
		
		Msgalert("Cancelado pelo usuario","Hope")

		endif
	Endif

ELSE

	MsgAlert("Voc� n�o tem permiss�o para eliminar residuos! Favor entrar em contato com o setor de compras","Hope")

ENDIF

CloseBrowse()
oMark:DeActivate()  

RestArea(aAreaTRB)

Return 

User Function WFREENV
Local cNum := SC7->C7_NUM

	DbSelectArea("SCR")		// Tabela de Alcada de Aprovacao
	SCR->(DbSetOrder(1))	// Indice CR_FILIAL+CR_TIPO+CR_NUM+CR_NIVEL
	If SCR->(DbSeek(xFilial("SCR")+"PC"+PadR(cNum,TamSx3("CR_NUM")[1])))
		If ( SCR->CR_STATUS $ "01/02")
			U_WFW120P()
			MSGINFO("WorkFlow Reeviado com sucesso")
		Else
			MsgSTOP("Nao foi possivel enviar WorkFlow. Nao tem pendencia de aprovacao!!!")
	    Endif 		
	Else
		MsgSTOP("Nao tem registro de aprovacao!!!")
	Endif 

Return

