#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'

/******************************************************************************
* Programa: MALTCLI          02/2021            Geyson Albano                 *
* Funcionalidade: PE para gravar a data de altera��o de clientes              *
*                                                                             *
******************************************************************************/

User Function MALTCLI()
		
	If Upper(Funname()) == "MATA030"
		If Altera
				Reclock("SA1", .F.)
					SA1->A1_XDATAAL := Dtos(Date())
				SA1->(MsUnlock())
		EndIf
	EndIf
Return
