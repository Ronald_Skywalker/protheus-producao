#INCLUDE "PROTHEUS.CH"
#INCLUDE "rwmake.ch"

User function MT120GRV() // - Continuar ou n�o a inclus�o, altera��o ou exclus�o ( [ ParamIxb[1] ], [ ParamIxb[2] ], [ ParamIxb[3] ], [ ParamIxb[4] ] ) --> lRet
Local oButton1
Local oButton2
Local oSay1 
Local xDeleta := PARAMIXB[4]       
Private cA120Num := PARAMIXB[1]           
Private cFilial  := space(04)              
Private cObser   := Space(30)
Private lRet     := .f.
Static oDlg
Static oMsg

If xDeleta   
   DEFINE MSDIALOG oDlg TITLE "DADOS OBRIGAT�RIOS NA EXCLUSAO DO PEDIDO DE COMPRAS" FROM 000, 000  TO 200, 500 COLORS 0, 16777215 PIXEL
   @ 005, 037 SAY    oSay1    PROMPT "DIGITE O MOTIVO DE EXCLUS�O "  SIZE 160, 007  OF oDlg COLORS 0, 16777215 PIXEL
   @ 018, 005 MSGET  oMsg     VAR  cObser   SIZE 200, 010 PIXEL   Picture "@!"  Valid !empty(cObser)  Message 'Digite entre 10 e 30 caracteres!'   Of oDlg 
   @ 060, 040 BUTTON oButton1 PROMPT "&Confirma" SIZE 036, 010 Action (GravaObs(cObser,cA120Num,4), oDlg:End()) OF oDlg PIXEL
   @ 060, 090 BUTTON oButton2 PROMPT "Cance&la"  SIZE 036, 010 Action oDlg:End()                                OF oDlg PIXEL
   ACTIVATE MSDIALOG oDlg CENTERED   
Else             

   lRet := .t.

EndIf            
  // If Opc  ==  2
  //    lRet := .f.
  // Endif   
Return(lRet)

Static Function GravaObs(nParFil,cPed,nTp)
Local aArea     := GetArea()
Local aAreaSC7  := SC7->(GetArea())
Local aReg      := {}
Local I         := 0
lRet := .F.
If Len(alltrim(cObser)) <= 9
	MsgStop("Digite o Motivo com mais de 10 caratcteres!. Exclus�o n�o efetuada! ", "Aten��o...!")
	lRet := .F.
Else
	DbSelectArea('SC7')
	SC7->(DbSetOrder(1))
	SC7->(DbGoTop())
	SC7->(DbSeek(xFilial("SC7")+cPed))
	IF SC7->C7_NUM == cPed
		While SC7->(!Eof()) .And. SC7->C7_FILIAL == xFilial("SC7") .And. SC7->C7_NUM == cPed
			aadd(aReg,SC7->(Recno()) )
			SC7->(DbSkip())
		EndDo
	EndIf
	
	For i := 1 to Len(aReg)
		SC7->(DbGoto(aReg[i]) )
		If SC7->C7_NUM == cPed
			SC7->(RecLock("SC7",.F.))
			SC7->C7_OBS     :=nParFil
			SC7->(MsUnlock())
			lRet := .t.
		Endif
	Next I             
Endif
RestArea(aAreaSC7)
RestArea(aArea)
RETURN(lRet)
                                               
  