#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"

User Function MT103FIM()

	Local nOpcao := PARAMIXB[1]			// Op��o Escolhida pelo usuario no aRotina
	Local nConfirma := PARAMIXB[2]		// Se o usuario confirmou a opera��o de grava��o da NFECODIGO DE APLICA��O DO USUARIO.....

	If nOpcao == 3 .AND. CFORMUL == "S"
		//Chama a rotina para atualizar os dados da expedi��o da NF.
		U_HCOMP4E(.T.)
		
		If SF1->F1_EST == 'EX'
			NWAtualCD5()
		EndIF
	End

Return


////////////////////////////////////////////////////////////////////////////////////////////////// 
////////////////////////////////////////////////////////////////////////////////////////////////// 
//                                                                                              // 
//      Data:      02/02/2018                                                                   //
//      Autor:     Daniel R. Melo                                                               // 
//      Fun��o:    Alimenta a Tabela CD5, quando for Importa��o, para o Sefaz Autorizar a Nota  // 
//      Altera��o: ACTUAL TREND / HOPE LINGERIE                                                 // 
//                                                                                              // 
////////////////////////////////////////////////////////////////////////////////////////////////// 
////////////////////////////////////////////////////////////////////////////////////////////////// 

//User Function AtuCD5()

//	dbSelectArea("SF1")
//	SF1->(dbSetOrder(1))  //Filial+Serie+Num NF+Fornecedor+Loja
//	SF1->(dbSeek(xFilial("SF1")+'0000239173  0005270001'))

//	NWAtualCD5()

//	SF1->(dbCloseArea())
	
//Return



Static Function NWAtualCD5()

	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Local oSay7
	Local oSay8
	Local oSay9
	Local oSay10
	Local oSay11
	Local oSay12
	Local oSay13
	Local oSay14
	Local oSay15
	Local oSay16
	Local oSay17
	Local oSay18
	Local oSay19
	Local oSay20
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oGet6
	Local oFImport
	Local oLocServ
	Local oTipoImp
	Local oVTransp
	Local oAtoCom
	Local ocDiDa
	Local odDiDa
	Local oImport
	Local oLocDesm
	Local oUfDes
	Local oDesmbar
	Local oBSalva
	Local oBCancela

	Public cNota		:= SF1->F1_DOC
	Public cSerie		:= SF1->F1_SERIE
	Public cFornec		:= SF1->F1_FORNECE
	Public cLoja		:= SF1->F1_LOJA
	Public cEspecie		:= SF1->F1_ESPECIE
	Public cNomeFor		:= POSICIONE("SA2",1,xFilial("SA2")+SF1->F1_FORNECE+SF1->F1_LOJA,"A2_NOME")
	Public cAtoCom		:= Space(20)
	Public cDiDa		:= Space(12)
	Public cImport		:= Space(10)
	Public cLocDesm		:= Space(30)
	Public cUfDes		:= Space(2)
	Public dDesmbar		:= Date()
	Public dDiDa		:= Date()
	Public nFImport		:= 4
	Public aFImport		:= {"1 - Importa��o por conta pr�pria","2 - Importa��o por conta e ordem","3 - Importa��o por encomenda","  "}
	Public nLocServ		:= 3
	Public aLocServ		:= {"0 - Executado no Pais","1 - Executado no Exterior, cujo result. se verifique no Pais","  "}
	Public nVTransp		:= 13
	Public aVTransp		:= {"1  - Mar�tima","2  - Fluvia","3  - Lacustre","4  - A�rea","5  - Postal","6  - Ferrovi�ria","7  - Rodovi�ria","8  - Conduto","9  - Meios Pr�prios","10 - Entrada/Saida Ficta","11 - Courier","12 - Handcarry","  "}
	Public nTipoImp		:= 3
	Public aTipoImp		:= {"0 - Declaracao de importacao","1 - Declaracao simplificada de importacao","  "}

	Static oDlg

	DEFINE MSDIALOG oDlg TITLE "Complemento de Importa��o" FROM 000, 000  TO 560, 1250 COLORS 0, 16777215 PIXEL

	@ 007, 007 SAY oSay1 PROMPT "N. Fiscal: "+cNota SIZE 100, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 007, 140 SAY oSay2 PROMPT "Serie NF.: "+cSerie SIZE 067, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 007, 266 SAY oSay3 PROMPT "Fornecedor: "+cFornec+" - "+cNomeFor SIZE 259, 007 OF oDlg COLORS 0, 16777215 PIXEL

	@ 022, 007 SAY oSay4 PROMPT "Tipo documento importacao:" SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 021, 085 MSCOMBOBOX oTipoImp VAR nTipoImp ITEMS aTipoImp SIZE 150, 013 OF oDlg COLORS 0, 16777215 PIXEL
	@ 022, 255 SAY oSay5 PROMPT "Numero doc. importacao:   " SIZE 066, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 021, 325 MSGET oImport VAR cImport SIZE 120, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 022, 460 SAY oSay12 PROMPT "N� Ato Concesso:" SIZE 054, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 021, 510 MSGET oAtoCom VAR cAtoCom SIZE 111, 010 OF oDlg COLORS 0, 16777215 PIXEL

	@ 037, 007 SAY oSay6 PROMPT "Local de exec. do servico:" SIZE 067, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 036, 085 MSCOMBOBOX oLocServ VAR nLocServ ITEMS aLocServ SIZE 150, 013 OF oDlg COLORS 0, 16777215 PIXEL
	@ 037, 255 SAY oSay7 PROMPT "No. da DI/DA:" SIZE 049, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 036, 325 MSGET ocDiDa VAR cDiDa SIZE 120, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 037, 460 SAY oSay8 PROMPT "Data Registro D.I.:" SIZE 050, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 036, 510 MSGET odDiDa VAR dDiDa SIZE 111, 010 OF oDlg COLORS 0, 16777215 PIXEL

	@ 052, 007 SAY oSay9 PROMPT "Local Desembarque:" SIZE 063, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 051, 085 MSGET oLocDesm VAR cLocDesm SIZE 150, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 052, 255 SAY oSay10 PROMPT "UF do Desembaraco:" SIZE 060, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 051, 325 MSGET oUfDes VAR cUfDes SIZE 120, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 052, 460 SAY oSay11 PROMPT "Dt.Desembar.:" SIZE 052, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 051, 510 MSGET oDesmbar VAR dDesmbar SIZE 111, 010 OF oDlg COLORS 0, 16777215 PIXEL

	@ 067, 007 SAY oSay13 PROMPT "Via de Transporte:" SIZE 068, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 066, 085 MSCOMBOBOX oVTransp VAR nVTransp ITEMS aVTransp SIZE 150, 013 OF oDlg COLORS 0, 16777215 PIXEL
	@ 067, 255 SAY oSay14 PROMPT "Forma Importa��o" SIZE 058, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 066, 325 MSCOMBOBOX oFImport VAR nFImport ITEMS aFImport SIZE 120, 013 OF oDlg COLORS 0, 16777215 PIXEL

	fWBrowse1()

	@ 250, 007 SAY oSay15 PROMPT "Base Pis"	SIZE 068, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 260, 007 MSGET oGet1 VAR nBsPis  			SIZE 065, 010 OF oDlg PICTURE "@E 999,999,999,999.99" COLORS 0, 16777215 READONLY PIXEL
	@ 250, 087 SAY oSay16 PROMPT "Valor Pis"	SIZE 068, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 260, 087 MSGET oGet2 VAR nValPis  		SIZE 065, 010 OF oDlg PICTURE "@E 999,999,999,999.99" COLORS 0, 16777215 READONLY PIXEL
	@ 250, 167 SAY oSay17 PROMPT "Base Cofins"	SIZE 068, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 260, 167 MSGET oGet3 VAR nBsCof  			SIZE 065, 010 OF oDlg PICTURE "@E 999,999,999,999.99" COLORS 0, 16777215 READONLY PIXEL
	@ 250, 247 SAY oSay18 PROMPT "Valor Cofins" SIZE 068, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 260, 247 MSGET oGet4 VAR nVlCof  			SIZE 065, 010 OF oDlg PICTURE "@E 999,999,999,999.99" COLORS 0, 16777215 READONLY PIXEL
	@ 250, 327 SAY oSay19 PROMPT "Vlr BC Impor" SIZE 068, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 260, 327 MSGET oGet5 VAR nBcImp  			SIZE 065, 010 OF oDlg PICTURE "@E 999,999,999,999.99" COLORS 0, 16777215 READONLY PIXEL
	@ 250, 407 SAY oSay20 PROMPT "Vlr Imp.Impo" SIZE 068, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 260, 407 MSGET oGet6 VAR nVlrII  			SIZE 065, 010 OF oDlg PICTURE "@E 999,999,999,999.99" COLORS 0, 16777215 READONLY PIXEL

	@ 256, 560 BUTTON oBSalva PROMPT "Salvar"     ACTION SalvaCD5() SIZE 045, 012 OF oDlg PIXEL

	ACTIVATE MSDIALOG oDlg CENTERED

Return


Static Function fWBrowse1()

	Local nX
	Local aHeaderEx			:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {"CD5_ITEM","D1_COD","CD5_VDESDI","CD5_DSPAD","CD5_VLRIOF","CD5_VAFRMM","CD5_NADIC","CD5_SQADIC"}
	Local aAlterFields		:= {"CD5_VDESDI","CD5_DSPAD","CD5_VLRIOF","CD5_VAFRMM","CD5_NADIC","CD5_SQADIC"}
	Local nI				:= 1
	Public nBcImp			:= 0
	Public nVlrII			:= 0
	Public nBsPis			:= 0
	Public nValPis			:= 0
	Public nBsCof			:= 0
	Public nVlCof			:= 0

	Static oWBrowse1

	SX3->(DbSetOrder(2))
	For nX := 1 to len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			//Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"","R","",""})
			Aadd(aHeaderEx, {alltrim(X3Descric()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"","R","",""})
		Endif
	Next

	dbSelectArea("CD5")
	CD5->(dbSetOrder(1))  //Filial+Serie+Num NF+Fornecedor+Loja
	CD5->(DbGoTop())
	CD5->(dbSeek(xFilial("CD5")+cNota+cSerie+cFornec+cLoja))

	If !CD5->(Found())    //Se n�o encontrou registro com aquela Nota e Serie
		
		dbSelectArea("SD1")
		SD1->(DbOrderNickName("SD1CD5"))  //Filia, Nota, Serie, Fornecedor, Loja, Item
		SD1->(dbSeek(xFilial("SD1")+cNota+cSerie+cFornec+cLoja))
		
		_nValZero	:= 0.00
		_nNadic		:= "1"
			
		While !SD1->(EOF()) .AND. SD1->D1_DOC == cNota .AND. SD1->D1_SERIE == cSerie .AND. SD1->D1_FORNECE = cFornec .AND. SD1->D1_LOJA == cLoja
			
			Aadd(aFieldFill, SD1->D1_ITEM)
			Aadd(aFieldFill, SD1->D1_COD)
			Aadd(aFieldFill, _nValZero)
			Aadd(aFieldFill, _nValZero)
			Aadd(aFieldFill, _nValZero)
			Aadd(aFieldFill, _nValZero)
			Aadd(aFieldFill, _nNadic)
			Aadd(aFieldFill, Alltrim(Str(nI)))
			Aadd(aFieldFill, SD1->D1_TOTAL - SD1->D1_II)
			Aadd(aFieldFill, SD1->D1_II)
			Aadd(aFieldFill, SD1->D1_BASIMP6)
			Aadd(aFieldFill, SD1->D1_ALQIMP6)
			Aadd(aFieldFill, SD1->D1_VALIMP6)
			Aadd(aFieldFill, SD1->D1_BASIMP5)
			Aadd(aFieldFill, SD1->D1_ALQIMP5)
			Aadd(aFieldFill, SD1->D1_VALIMP5)
			Aadd(aFieldFill, .F.)
			Aadd(aColsEx, aFieldFill)
			
			nBcImp		+= aFieldFill[9]
			nVlrII		+= aFieldFill[10]
			nBsPis		+= aFieldFill[11]
			nValPis		+= aFieldFill[13]
			nBsCof		+= aFieldFill[14]
			nVlCof		+= aFieldFill[16]

			aFieldFill	:={}
			DbSkip()
			
			nI++

		EndDo

	Else
		
		While !CD5->(EOF()) .AND. CD5->CD5_DOC == cNota .AND. CD5->CD5_SERIE == cSerie .AND. CD5->CD5_FORNECE = cFornec .AND. CD5->CD5_LOJA == cLoja
		
			dbSelectArea("SD1")
			SD1->(DbOrderNickName("SD1CD5"))  //Filia, Nota, Serie, Fornecedor, Loja, Item
			SD1->(dbSeek(xFilial("SD1")+cNota+cSerie+cFornec+cLoja+CD5->CD5_ITEM))

			If Found() //While !SD1->(EOF()) .AND. SD1->D1_DOC == cNota .AND. SD1->D1_SERIE == cSerie .AND. SD1->D1_FORNECE = cFornec .AND. SD1->D1_LOJA == cLoja .AND. SD1->D1_ITEM == CD5->CD5_ITEM
			
				Aadd(aFieldFill, CD5->CD5_ITEM)
				Aadd(aFieldFill, SD1->D1_COD)
				Aadd(aFieldFill, CD5->CD5_VDESDI)
				Aadd(aFieldFill, CD5->CD5_DSPAD)
				Aadd(aFieldFill, CD5->CD5_VLRIOF)
				Aadd(aFieldFill, CD5->CD5_VAFRMM)
				Aadd(aFieldFill, CD5->CD5_NADIC)
				Aadd(aFieldFill, CD5->CD5_SQADIC)
				Aadd(aFieldFill, CD5->CD5_BCIMP)
				Aadd(aFieldFill, CD5->CD5_VLRII)
				Aadd(aFieldFill, CD5->CD5_BSPIS)
				Aadd(aFieldFill, CD5->CD5_ALPIS)
				Aadd(aFieldFill, CD5->CD5_VLPIS)
				Aadd(aFieldFill, CD5->CD5_BSCOF)
				Aadd(aFieldFill, CD5->CD5_ALCOF)
				Aadd(aFieldFill, CD5->CD5_VLCOF)
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)

				nBcImp		+= aFieldFill[9]
				nVlrII		+= aFieldFill[10]
				nBsPis		+= aFieldFill[11]
				nValPis		+= aFieldFill[13]
				nBsCof		+= aFieldFill[14]
				nVlCof		+= aFieldFill[16]

				aFieldFill	:={}

				//SD1->(dbSkip())
				
			End
		
			CD5->(dbSkip())
		
		EndDo
		
		SD1->(dbCloseArea())
		CD5->(dbCloseArea())
				
	End

	oWBrowse1 := MsNewGetDados():New( 085, 007, 248, 620, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return


Static Function SalvaCD5()

Local i := 0
	
	For i := 1 to Len(oWBrowse1:ACOLS)

		dbSelectArea("CD5")
		CD5->(dbSetOrder(4))  //Filial+Serie+Num NF+Fornecedor+Loja+Item
		CD5->(dbSeek(xFilial("CD5")+cNota+cSerie+cFornec+cLoja+oWBrowse1:ACOLS[i,1]))

		If !Found()

			RecLock("CD5",.T.) //Inclus�o												//|X3_CAMPO			|X3_TIPO	|X3_TAMANHO	|X3_DECIMAL	|X3_TITULO          |
				Replace CD5_FILIAL			With xFilial("CD5")							//|CD5_FILIAL		|C			|4			|			|Filial             |
				Replace CD5_DOC				With cNota									//|CD5_DOC			|C			|9			|			|N�mero NF          |
				Replace CD5_SERIE			With cSerie									//|CD5_SERIE		|C			|3			|			|S�rie NF           |
				Replace CD5_ESPEC			With cEspecie								//|CD5_ESPEC		|C			|5			|			|Esp�cie NF         |
				Replace CD5_FORNEC			With cFornec								//|CD5_FORNEC		|C			|6			|			|Fornecedor         |
				Replace CD5_LOJA			With cLoja									//|CD5_LOJA			|C			|4			|			|C�digo loja        |
				Replace CD5_TPIMP			With Substring(nTipoImp,1,1)				//|CD5_TPIMP		|C			|1			|			|Tp. doc. imp       |
				Replace CD5_DOCIMP			With cImport								//|CD5_DOCIMP		|C			|50			|			|Doc. imp.          |
				Replace CD5_BSPIS			With oWBrowse1:ACOLS[i,11]					//|CD5_BSPIS		|N			|14			|2			|Base PIS           |
				Replace CD5_ALPIS			With oWBrowse1:ACOLS[i,12]					//|CD5_ALPIS		|N			|5			|2			|Al�q. PIS          |
				Replace CD5_VLPIS			With oWBrowse1:ACOLS[i,13]					//|CD5_VLPIS		|N			|14			|2			|Val. PIS           |
				Replace CD5_BSCOF			With oWBrowse1:ACOLS[i,14]					//|CD5_BSCOF		|N			|14			|2			|Base Cofins        |
				Replace CD5_ALCOF			With oWBrowse1:ACOLS[i,15]					//|CD5_ALCOF		|N			|5			|2			|Al�q. Cofins       |
				Replace CD5_VLCOF			With oWBrowse1:ACOLS[i,16]					//|CD5_VLCOF		|N			|14			|2			|Val. Cofins        |
				Replace CD5_LOCAL			With Substring(nLocServ,1,1)				//|CD5_LOCAL		|C			|1			|			|Local Serv         |
				Replace CD5_DTPCOF			With dDesmbar								//|CD5_DTPCOF		|D			|8			|			|Dt Pag Cofin       |
				Replace CD5_DTPPIS			With dDesmbar								//|CD5_DTPPIS		|D			|8			|			|Dt Pag Pis         |
				Replace CD5_NDI				With cDiDa									//|CD5_NDI			|C			|12			|			|No. da DI/DA       |
				Replace CD5_DTDI			With dDiDa									//|CD5_DTDI			|D			|8			|			|Registro DI        |
				Replace CD5_LOCDES			With cLocDesm								//|CD5_LOCDES		|C			|30			|			|Descr.Local        |
				Replace CD5_UFDES			With cUfDes									//|CD5_UFDES		|C			|2			|			|UF Desembara       |
				Replace CD5_DTDES			With dDesmbar								//|CD5_DTDES		|D			|8			|			|Dt Desembar.       |
				Replace CD5_CODEXP			With cFornec								//|CD5_CODEXP		|C			|6			|			|Exportador         |
				Replace CD5_NADIC			With oWBrowse1:ACOLS[i,7]					//|CD5_NADIC		|C			|3			|			|Adicao             |
				Replace CD5_SQADIC			With oWBrowse1:ACOLS[i,8]					//|CD5_SQADIC		|C			|3			|			|Seq Adicao         |
				Replace CD5_CODFAB			With cFornec								//|CD5_CODFAB		|C			|6			|			|Fabricante         |
				Replace CD5_BCIMP			With oWBrowse1:ACOLS[i,9]					//|CD5_BCIMP		|N			|15			|2			|Vlr BC Impor       |
				Replace CD5_VDESDI			With oWBrowse1:ACOLS[i,3]					//|CD5_VDESDI		|N			|15			|2			|Vlr Desconto       |
				Replace CD5_DSPAD			With oWBrowse1:ACOLS[i,4]					//|CD5_DSPAD		|N			|15			|2			|Vlr Desp.Adu       |
				Replace CD5_VLRII			With oWBrowse1:ACOLS[i,10]					//|CD5_VLRII		|N			|15			|2			|Vlr Imp.Impo       |
				Replace CD5_VLRIOF			With oWBrowse1:ACOLS[i,5]					//|CD5_VLRIOF		|N			|15			|2			|Vlr IOF            |
				Replace CD5_LOJFAB			With cLoja									//|CD5_LOJFAB		|C			|6			|			|Loja Fab.          |
				Replace CD5_LOJEXP			With cLoja									//|CD5_LOJEXP		|C			|4			|			|Loja Exp.          |
				Replace CD5_ACDRAW			With cAtoCom								//|CD5_ACDRAW		|C			|20			|			|Ato Concesso       |
				Replace CD5_VTRANS			With Substring(nVTransp,1,1)				//|CD5_VTRANS		|C			|2			|			|Via Transp         |
				Replace CD5_VAFRMM			With oWBrowse1:ACOLS[i,6]					//|CD5_VAFRMM		|N			|15			|2			|Val. AFRMM         |
				Replace CD5_INTERM			With Substring(nFImport,1,1)				//|CD5_INTERM		|C			|1			|			|Forma Import       |
				Replace CD5_CNPJAE			With SM0->M0_CGC							//|CD5_CNPJAE		|C			|14			|			|CNPJ Adqui.        |
				Replace CD5_UFTERC			With SM0->M0_ESTENT							//|CD5_UFTERC		|C			|2			|			|UF Terceiro        |
				Replace CD5_ITEM			With oWBrowse1:ACOLS[i,1]					//|CD5_ITEM			|C			|4			|			|Item da Nota       |
				Replace CD5_SDOC			With cSerie									//|CD5_SDOC			|C			|3			|			|S�rie Doc.         |
			CD5->(MsUnlock())

		Else

			RecLock("CD5",.F.) //Altera��o												//|X3_CAMPO			|X3_TIPO	|X3_TAMANHO	|X3_DECIMAL	|X3_TITULO          |
				Replace CD5_FILIAL			With xFilial("CD5")							//|CD5_FILIAL		|C			|4			|			|Filial             |
				Replace CD5_DOC				With cNota									//|CD5_DOC			|C			|9			|			|N�mero NF          |
				Replace CD5_SERIE			With cSerie									//|CD5_SERIE		|C			|3			|			|S�rie NF           |
				Replace CD5_ESPEC			With cEspecie								//|CD5_ESPEC		|C			|5			|			|Esp�cie NF         |
				Replace CD5_FORNEC			With cFornec								//|CD5_FORNEC		|C			|6			|			|Fornecedor         |
				Replace CD5_LOJA			With cLoja									//|CD5_LOJA			|C			|4			|			|C�digo loja        |
				Replace CD5_TPIMP			With Substring(nTipoImp,1,1)				//|CD5_TPIMP		|C			|1			|			|Tp. doc. imp       |
				Replace CD5_DOCIMP			With cImport								//|CD5_DOCIMP		|C			|50			|			|Doc. imp.          |
				Replace CD5_BSPIS			With oWBrowse1:ACOLS[i,11]					//|CD5_BSPIS		|N			|14			|2			|Base PIS           |
				Replace CD5_ALPIS			With oWBrowse1:ACOLS[i,12]					//|CD5_ALPIS		|N			|5			|2			|Al�q. PIS          |
				Replace CD5_VLPIS			With oWBrowse1:ACOLS[i,13]					//|CD5_VLPIS		|N			|14			|2			|Val. PIS           |
				Replace CD5_BSCOF			With oWBrowse1:ACOLS[i,14]					//|CD5_BSCOF		|N			|14			|2			|Base Cofins        |
				Replace CD5_ALCOF			With oWBrowse1:ACOLS[i,15]					//|CD5_ALCOF		|N			|5			|2			|Al�q. Cofins       |
				Replace CD5_VLCOF			With oWBrowse1:ACOLS[i,16]					//|CD5_VLCOF		|N			|14			|2			|Val. Cofins        |
				Replace CD5_LOCAL			With Substring(nLocServ,1,1)				//|CD5_LOCAL		|C			|1			|			|Local Serv         |
				Replace CD5_DTPCOF			With ddatabase								//|CD5_DTPCOF		|D			|8			|			|Dt Pag Cofin       |
				Replace CD5_DTPPIS			With ddatabase								//|CD5_DTPPIS		|D			|8			|			|Dt Pag Pis         |
				Replace CD5_NDI				With cDiDa									//|CD5_NDI			|C			|12			|			|No. da DI/DA       |
				Replace CD5_DTDI			With dDiDa									//|CD5_DTDI			|D			|8			|			|Registro DI        |
				Replace CD5_LOCDES			With cLocDesm								//|CD5_LOCDES		|C			|30			|			|Descr.Local        |
				Replace CD5_UFDES			With cUfDes									//|CD5_UFDES		|C			|2			|			|UF Desembara       |
				Replace CD5_DTDES			With dDesmbar								//|CD5_DTDES		|D			|8			|			|Dt Desembar.       |
				Replace CD5_CODEXP			With cFornec								//|CD5_CODEXP		|C			|6			|			|Exportador         |
				Replace CD5_NADIC			With oWBrowse1:ACOLS[i,9]					//|CD5_NADIC		|C			|3			|			|Adicao             |
				Replace CD5_SQADIC			With oWBrowse1:ACOLS[i,10]					//|CD5_SQADIC		|C			|3			|			|Seq Adicao         |
				Replace CD5_CODFAB			With cFornec								//|CD5_CODFAB		|C			|6			|			|Fabricante         |
				Replace CD5_BCIMP			With oWBrowse1:ACOLS[i,3]					//|CD5_BCIMP		|N			|15			|2			|Vlr BC Impor       |
				Replace CD5_VDESDI			With oWBrowse1:ACOLS[i,4]					//|CD5_VDESDI		|N			|15			|2			|Vlr Desconto       |
				Replace CD5_DSPAD			With oWBrowse1:ACOLS[i,5]					//|CD5_DSPAD		|N			|15			|2			|Vlr Desp.Adu       |
				Replace CD5_VLRII			With oWBrowse1:ACOLS[i,6]					//|CD5_VLRII		|N			|15			|2			|Vlr Imp.Impo       |
				Replace CD5_VLRIOF			With oWBrowse1:ACOLS[i,7]					//|CD5_VLRIOF		|N			|15			|2			|Vlr IOF            |
				Replace CD5_LOJFAB			With cLoja									//|CD5_LOJFAB		|C			|6			|			|Loja Fab.          |
				Replace CD5_LOJEXP			With cLoja									//|CD5_LOJEXP		|C			|4			|			|Loja Exp.          |
				Replace CD5_ACDRAW			With cAtoCom								//|CD5_ACDRAW		|C			|20			|			|Ato Concesso       |
				Replace CD5_VTRANS			With Substring(nVTransp,1,1)				//|CD5_VTRANS		|C			|2			|			|Via Transp         |
				Replace CD5_VAFRMM			With oWBrowse1:ACOLS[i,8]					//|CD5_VAFRMM		|N			|15			|2			|Val. AFRMM         |
				Replace CD5_INTERM			With Substring(nFImport,1,1)				//|CD5_INTERM		|C			|1			|			|Forma Import       |
				Replace CD5_CNPJAE			With SM0->M0_CGC							//|CD5_CNPJAE		|C			|14			|			|CNPJ Adqui.        |
				Replace CD5_UFTERC			With SM0->M0_ESTENT							//|CD5_UFTERC		|C			|2			|			|UF Terceiro        |
				Replace CD5_ITEM			With oWBrowse1:ACOLS[i,1]					//|CD5_ITEM			|C			|4			|			|Item da Nota       |
				Replace CD5_SDOC			With cSerie									//|CD5_SDOC			|C			|3			|			|S�rie Doc.         |
			CD5->(MsUnlock())

		End

	Next i
	
	Close(oDlg)

Return