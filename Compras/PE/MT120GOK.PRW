#INCLUDE "PROTHEUS.CH"
#INCLUDE "rwmake.ch"

User function MT120GOK()
Local oButton1
Local oButton2
Local oSay1 
Local lFaz := .f.
Local xInclui := PARAMIXB[2]
PRIVATE L120DELETA:=PARAMIXB[4]
Private cA120Num := PARAMIXB[1]           
Private cxFilial := space(04)              
Private cnFilial := space(25)              
Static oDlg
Static oMsg
Static oMsg1                            

If xInclui
   dbSelectArea("SC7")
   SC7->(dbSetOrder(1))
   If SC7->C7_NUM <> cA120Num
	  If !DbSeek(xFilial("SC7")+cA120Num)
	   	 MsgAlert("Pedido "+cA120Num+" n�o foi encontrado!")
   		 Return 
	  Else     
         lFaz := .t.
      ENDIF 
   Else
      lFaz := .t.                              
   Endif
   If lFaz
      If !MsgYesno("Confirma inclus�o deste Pedido na Filial "+ SC7->C7_FILIAL +" !","Confirma��o!")  
      
  	     DEFINE MSDIALOG oDlg TITLE "DADOS DO PEDIDO DE COMPRAS" FROM 000, 000  TO 200, 400 COLORS 0, 16777215 PIXEL
	     @ 005, 037 SAY    oSay1    PROMPT "DIGITE O CODIGO DA FILIAL CORRETA" SIZE 160, 007  OF oDlg COLORS 0, 16777215 PIXEL
	     @ 018, 005 MSGET  oMsg     VAR cxFilial SIZE 050, 010 F3 "SM0EMP" PICTURE "@!" VALID Mod2Vend(cxFilial, @cNFilial)  PIXEL OF oDlg
	     @ 018, 075 MSGET  oMsg1    VAR cnFilial When .F.                  SIZE 060, 010 PIXEL OF oDlg
	     @ 060, 040 BUTTON oButton1 PROMPT "&Confirma"                         SIZE 036, 010 Action (GravaFil(cxFilial,cA120Num,3), oDlg:End()) OF oDlg PIXEL
	     @ 060, 090 BUTTON oButton2 PROMPT "Cance&la"                          SIZE 036, 010 Action oDlg:End() OF oDlg PIXEL
	     ACTIVATE MSDIALOG oDlg CENTERED 
	  Endif   
   EndIf 
EndIf

Return

//-------------                           
Static Function GravaFil(nParFil,cPed,nTp)
Local aArea := GetArea()
Local aAreaSC7 := SC7->(GetArea())
Local aReg    := {}       
Local I := 0                             
Local cEmpAnt := cEmpAnt
Local cFilDe  := cFilAnt
Local cFilAte := cFilAnt
Local cFilOld := cFilAnt
Local cPedido := ""	                                                                            

DbSelectArea('SC7')
SC7->(DbSetOrder(1))
SC7->(DbGoTop())
SC7->(DbSeek(xFilial("SC7")+cPed))  
IF SC7->C7_NUM == cPed
   While SC7->(!Eof()) .And. SC7->C7_FILIAL+SC7->C7_NUM  == xFilial("SC7")+cPed 
   	  aadd(aReg,SC7->(Recno()) )
	  SC7->(DbSkip())
	EndDo
EndIf          
RollBackSx8() 

//SELECT MAX(C7_NUM) FROM SC7010 WHERE D_E_L_E_T_ = '' AND C7_FILIAL = '0101'

SM0->(MsSeek('01'+nParFil,.T.))
If !SM0->(Eof()) .And. alltrim(SM0->M0_CODIGO) == cEmpAnt .And. alltrim(SM0->M0_CODFIL) == alltrim(nParFil)
	cFilAnt := SM0->M0_CODFIL
	cPedido	:= GetNumSC7()  //GetSX8Num( "SC7" , "C7_NUM" )
	msgstop("Numero do Pedido Alterado para: "+cPedido,"Aten��o" )
//	SM0->(DbSkip())		
Endif 	

For i := 1 to Len(aReg)
   SC7->(DbGoto(aReg[i]) )  
   If SC7->C7_NUM == cPed  
      SC7->(RecLock("SC7",.F.)) 
      SC7->C7_FILIAL  := nParFil  
      SC7->C7_FILENT  := nParFil
      SC7->C7_FISCORI := nParFil
      SC7->C7_NUM     := cPedido 
   	  SC7->(MsUnlock())   
	  ConfirmSx8()
	Else
  	  RollBackSx8()
   Endif  
Next I

RestArea(aAreaSC7)
RestArea(aArea)
RETURN

Static Function Mod2Vend( cCodigo, cNFilial)
If ALLTRIM(SM0->M0_CODFIL) == ALLTRIM(cCodigo) 
	cNFilial := SM0->M0_FILIAL   //Posicione("SM0",1,xFilial("SA3")+cCodigo,"A3_NOME")
Endif
Return(!Empty(cNFilial))
     