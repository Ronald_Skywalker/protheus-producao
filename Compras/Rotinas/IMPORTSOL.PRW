#INCLUDE "RWMAKE.CH"
#INCLUDE "TBICODE.CH"
#INCLUDE "FONT.CH"

// Retornos poss�veis do MessageBox
#Define IDOK			    1
#Define IDCANCEL		    2
#Define IDYES			    6
#Define IDNO			    7
#Define CRLF  CHR(13)+CHR(10)

User Function ImpSolc()

    Local aPergs    := {}
    Private cArq       := ""
    Private cArqMacro  := "XLS2DBF.XLA"
    Private cTemp      := GetTempPath() //pega caminho do temp do client
    Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema
    Private aResps     := {}
    Private aArquivos  := {}
    Private nRet       := 0
    Private aTitle     := {}
    Private aLog     := {}

    aAdd(aTitle, "As seguintes solicita��es foram criados a partir dessa planilha" )
    aAdd(aLog,{} )
    aAdd(aLog[Len(aLog)], Padr("Solicita��o",12) + Padr("Dt. Nec. PCP",12))

    Aadd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.T.,""})

    If ParamBox(aPergs,"Parametros", @aResps)

        Aadd(aArquivos, AllTrim(aResps[1]))

        cArq := AllTrim(aResps[1])

        Processa({|| ProcMov()})

    EndIF
Return

Static Function ProcMov()

    Local nFile		:= 0
    Local cFile		:= AllTrim(aResps[1])
    Local cLinha  	:= ""
    Local nLintit	:= 1
    Local nLin 		:= 0
    Local aDados  	:= {}
    Local nHandle 	:= 0
    Local _x         := 0
    Local lcont     := .T.


    nFile := fOpen(cFile,0,0)

    If nFile == -1
        If !Empty(cFile)
            MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")
        EndIf
        Return
    EndIf

    nHandle := Ft_Fuse(cFile)
    Ft_FGoTop()
    nLinTot := FT_FLastRec()-1
    ProcRegua(nLinTot)

    While nLinTit > 0 .AND. !Ft_FEof()
        Ft_FSkip()
        nLinTit--
    EndDo

    Do While !Ft_FEof()
        IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))
        nLin++
        cLinha := Ft_FReadLn()

        If Empty(AllTrim(StrTran(cLinha,',','')))
            Ft_FSkip()
            Loop
        EndIf

        cLinha := StrTran(cLinha,'"',"'")
        cLinha := '{"'+cLinha+','+alltrim(str(nLin+1))+'"}'
        cLinha := StrTran(cLinha,';',',')
        cLinha := StrTran(cLinha,',','","')

        Aadd(aDados, &cLinha)
        FT_FSkip()
    EndDo

    FT_FUse()

    ProcRegua(0)

    For _x:= 1 to len(aDados) 
       If lcont 
            If Empty(aDados[_x][11])
                lcont := .F.
            ElseIf  Empty(aDados[_x][5])
                lcont := .F.
            ElseIf Empty(aDados[_x][3])
                lcont := .F.
            ElseIf Empty(aDados[_x][8])
                lcont := .F.
            EndIf
            Loop
        EndIf
        Loop

    Next _x

    If lcont

        nRet:= MessageBox("Deseja realmente importar o arquivo:" +Upper(cFile)+ " ?","Confirma��o",4)

        If nRet == 6
            U_xMata110(aDados)
        Else
            MessageBox("Cancelado pelo usu�rio !","Cancelado",16)
        EndIf
    EndIf
    If !lcont
        MessageBox("Favor revisar o arquivo est� faltando informa��es obrigat�rias Ex: QUANTIDADE, CENTRO DE CUSTO, DATA NECESSIDADE PCP ou TIPO REQ. ! ","Aten��o",16)
    EndIf
Return

//ROTINA DE INCLUSAO AUTOMATICA DE PEDIDOS
User Function xMata110(pMatriz)

//pMatriz --> matriz contendo os dados do arq csv

    Local nXT           := 0
    Local aLinha        := {}
    Local cSolict       := ""

    Private pMatrizped  := pMatriz
    Private lMsErroAuto := .F.
    Private cTipoped    := ""
    Private cCliente    := ""
    Private cLjcli      := ""
    Private cLjEnt      := ""
    Private dDtMost     := ""
    Private cItem       := ""
    Private cProd       := ""
    Private cQtd        := ""
    Private cQtdMos     := ""
    Private cPrcUni     := ""
    Private cValor      := ""
    Private cTipoRq     := ""
    Private cSubCol     := ""
    Private cCC         := ""
    Private dDtpcp      := ""
    Private lOK         := .F.
    Private cColec      := ""
    Private nRet2       := 0

    aCabec := {}   // monta SC5
    aItens := {}   // monta SC6

    For nXT:=1 to len(pMatrizped)


        cProd          := pMatrizped[nXT][1]
        cQtd           := Val(pMatrizped[nXT][3])
        cPrcUni        := Val(pMatrizped[nXT][4])
        cValor         := Round(cQtd * cPrcUni,2)
        cTipoRq        := pMatrizped[nXT][5]
        cColec         := pMatrizped[nXT][6]
        cSubCol        := pMatrizped[nXT][7]
        dDtpcp         := Dtos(Ctod(pMatrizped[nXT][8]))
        cQtdMos        := Val(pMatrizped[nXT][9])
        dDtMost        := Dtos(Ctod(pMatrizped[nXT][10]))
        cCC            := pMatrizped[nXT][11]
        cSolic         := Substr(cUserName,0,25)
        coBS           := pMatrizped[nXT][12]


        aLinha := {}
        cItem:= StrZero(len(aItens)+1,4)

        Aadd(aLinha,{"C1_ITEM"      ,citem                    ,Nil})
        Aadd(aLinha,{"C1_PRODUTO"   ,cProd                    ,Nil})
        Aadd(aLinha,{"C1_QUANT"     ,cQtd                     ,Nil})
        Aadd(aLinha,{"C1_XPRECO"    ,Round(cPrcUni,3)         ,Nil})
        Aadd(aLinha,{"C1_XTOTAL"    ,cValor                   ,Nil})
        Aadd(aLinha,{"C1_XTPREQU"   ,cTipoRq                  ,Nil})
        Aadd(aLinha,{"C1_XCOLECA"   ,cColec                   ,Nil})
        Aadd(aLinha,{"C1_XSBCOLE"   ,cSubCol                  ,Nil})
        Aadd(aLinha,{"C1_XTPREQU"   ,cTipoRq                  ,Nil})
        Aadd(aLinha,{"C1_XDTNPCP"   ,stod(dDtpcp)             ,Nil})
        Aadd(aLinha,{"C1_CC"        ,cCC                      ,Nil})
        Aadd(aLinha,{"C1_DATPRF"    ,stod(dDtpcp)            ,Nil})
        // qtd e data mostru�rio 
        Aadd(aLinha,{"C1_XDTPCPM"   ,stod(dDtMost)            ,Nil})

        If cQtdMos != 0 
            Aadd(aLinha,{"C1_XQTDMOS",cQtdMos                 ,Nil})
        Else  
             Aadd(aLinha,{"C1_XQTDMOS",0                      ,Nil})
        EndIf
        Aadd(aLinha,{"C1_WFOBS"     ,coBS                     ,Nil})
        Aadd(aLinha,{"C1_ORIGEM"    ,"IMPSOLC"                ,Nil})

        Aadd(aItens,aLinha)

        If nXT == len(pMatrizped)
            lOk := .T.
        ElseIf dDtpcp != Dtos(Ctod(pMatrizped[nXT+1][8]))
            lOk := .T.
        ElseIf  SUBSTR(cProd,0,4) == "MPBJ"
            If SUBSTR(cProd,0,8) != SUBSTR(pMatrizped[nXT+1][1],0,8)
                lOk := .T.
            EndIf
        EndIF

        aCabec := {}

        If lOK
            Aadd(aCabec,{"C1_SOLICIT",cSolic                   ,Nil})
            Aadd(aCabec,{"C1_EMISSAO",dDataBase                ,Nil})

            //* Inclusao Utilizando MsExecAuto

            If lOk
                MSExecAuto({|x,y| mata110(x,y)},aCabec,aItens)
                cSolict := SC1->C1_NUM
                lOK := .F.
            EndIf

            If !lMsErroAuto

                aAdd(aLog[Len(aLog)], Padr(SC1->C1_NUM,12) + Padr(pMatrizped[nXT][8],12))

                Logs("Solicita��o: " + SC1->C1_NUM)
                Logs("Dt. Nec. PCP: " + dDtpcp)
                Logs("==============================================================")

                While !Eof() .AND. SC1->C1_NUM == cSolict // SEM ISSO APENAS O PRIMEIRO ITEM NASCIA BLOQUEADO :(
                    RECLOCK("SC1",.F.)
                    SC1->C1_APROV := "B"
                    SC1->(DBSKIP())
                EndDo

                aItens := {}
                lOK := .F.
            ElseIf !lMsErroAuto
                aCabec := {}
                aItens := {}
                Loop
            Else
                // Se erro , avisa, e sai do FOR
                Messagebox("Erro na inclusao!","Erro",16)
                Mostraerro()
                aCabec := {}
                aItens := {}
                lOK := .F.
                nXT := len(pMatrizped)
                Loop
            EndIf
        EndIf

    Next nXT
    nRet2 := MessageBox("Deseja imprimir o LOG das solicita��es geradas ?","LOG",4)
    If nRet2 == 6
        fMakeLog( aLog , aTitle , NIL  , .T. , "ImpSolc" , NIL , NIL, NIL, NIL, .F. )
    Endif
Return(.T.)

/*
* Gera��o do arquivo de log.
* @Author Geyson Albano
* @Data 10/06/2020
*/

Static Function Logs(cTexto)
    Local cCodLog	:= SubStr(DtoS(Date()),1,6)
    Local cPasta	:= "\log\ImpSolc"
    Local FC_NORMAL := 0

    ConOut(DtoC(Date(),"dd/mm/yy")+"|"+Time()+"|"+cUserName+"| "+cTexto)

    FWMakeDir(cPasta)
    If File(cPasta+"\"+cCodLog+".txt")
        nHdl := FOpen(cPasta+"\"+cCodLog+".txt" , 2 + 64 )
    Else
        nHdl := FCreate(cPasta+"\"+cCodLog+".txt",FC_NORMAL)
    EndIf
    FSeek(nHdl, 0, 2)
    FWrite(nHdl, DtoC(Date(),"dd/mm/yy")+"|"+Time()+"|"+cUserName+"| "+cTexto+CRLF )

    FClose(nHdl)
Return
