#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"


user function HCOMP005(cProd)

	Local nPreco	

	nPreco :=  POSICIONE("SB1",1,XFILIAL("SB1")+cProd,"B1_CUSTD")
	
	IF nPreco <= 0
		nPreco :=  POSICIONE("DA1",1,XFILIAL("SB1")+"300"+cProd,"DA1_PRCVEN")
	ENDIF

return nPreco