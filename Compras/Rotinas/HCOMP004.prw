#include "rwmake.ch"        

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HCOMP004 	�Autor  �Bruno Parreira      � Data �  23/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     � Esta rotina atualiza os dados da expedicao da nota fiscal  ���
���          � ex: peso liquido, volume, transportadora..                 ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico HOPE - www.actualtrend.com.br                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HCOMP004()

cOpcao:="ALTERAR"
nOpcE:=2
nOpcG:=3
cCadastro := "Saida de Produtos na Expedicao"

aRotina := {	{ "Pesquisa"		,"AxPesqui"			,0, 1},;
				{ "Visualiza"		,'U_HCOMP4E(.F.)'	,0, 2},;
				{ "Expedi��o NF"	,'U_HCOMP4E(.T.)'	,0, 4}}

MBrowse(01,01,30,70,"SF1")

Return


/*
//���������������������������������������������������������������������������Ŀ
//�Rotina utilizada para alterar os dados da expedicao da nota fiscal de saida�
//�����������������������������������������������������������������������������
*/

User Function HCOMP4E(lExp)
				
Local   _aAlias        		:= Alias()
Private _nFiscal         	:= SF1->F1_DOC
Private _cTransportadora 	:= SF1->F1_TRANSP
Private _cData           	:= SF1->F1_EMISSAO
Private d_dtExp				:= DDATABASE
Private c_horas				:= SF1->F1_HORA
Private _xTamdt          	:= PesqPict("SF1","F1_EMISSAO",8)
Private nPesoBruto			:= SF1->F1_PBRUTO
Private nPesoLiq				:= SF1->F1_PLIQUI
Private cVolume				:= SF1->F1_VOLUME1
Private cCombo 				:= SF1->F1_ESPECI1
Private aCombo				:= {"CAIXA","PALLET","SACO","UNIDADE","CAIXA PAPELAO","BAU METAL","ROLOS"}
Private mmsgfis				:= FwCutOff(AllTrim(SF1->F1_MENNOTA), .T.)

DbSelectarea("SF1")

//���������������������������������������������������������������������������Ŀ
//� Criacao da Interface                                                      �
//�����������������������������������������������������������������������������

@ 156,367 To 700,934 Dialog mkwdlg Title OemToAnsi("Expedi��o de Nota Fiscal e Mensagem para Nota")
n_linha	:= 015
n_latu		:= 013

@ n_latu  ,018	Say OemToAnsi("Nota Fiscal:") Size 100,12
@ n_latu  ,063	say SF1->F1_DOC Size 100,12
n_latu +=  n_linha

@ n_latu  ,018	Say OemToAnsi("Data de Sa�da:") Size 100,12
@ n_latu+8,018	Get d_dtExp Size 100,12
@ n_latu  ,167	Say OemToAnsi("Hora de Sa�da:") Size 100,12
@ n_latu+8,167	Get c_horas Size 100,12
n_latu +=  n_linha+010

@ n_latu  ,018	Say OemToAnsi("Transportadora:") Size 100,12
@ n_latu+8,018	Get _cTransportadora pict "@!" F3 "SA4" Size 100,12
n_latu +=  n_linha+010
@ n_latu  ,018	Say OemToAnsi("Peso Bruto:") Size 100,12
@ n_latu+8,018	Get nPesoBruto Picture "999999.9999" Size 100,12
@ n_latu  ,167	Say OemToAnsi("Peso Liquido:") Size 100,12
@ n_latu+8,167	Get nPesoLiq  Picture "999999.9999" Size 100,12
n_latu +=  n_linha+010

@ n_latu  ,018	Say OemToAnsi("Volume:") Size 100,12
@ n_latu+8,018	Get cVolume Picture "999999" Size 100,12
@ n_latu  ,167	Say OemToAnsi("Especie:") Size 100,12
@ n_latu+8,167	ComboBox cCombo Items aCombo Size 100,12
n_latu +=  n_linha+010

@ n_latu  ,018	Say OemToAnsi("Mensagem complementar para Nota:") Size 100,12
@ n_latu+8,018	Get mmsgfis MEMO Size 249,80

If	lExp
	@ 249,18	Button OemToAnsi("_Salvar") Size 36,16 Action SALVAR() //VALID lExp
End
@ 249,65	Button OemToAnsi("_Cancelar") Size 36,16 Action CLOSE(mkwdlg)

@ 249,112	Button OemToAnsi("_Zerar FECOP") Size 40,16 Action U_Updfeco()

Activate Dialog mkwdlg CENTERED 

DbSelectArea(_aAlias)

Return()

                
/*
//���������������������������������������������������������������������������Ŀ
//�Atualiza dados de expedicao da NF.                                         �
//�����������������������������������������������������������������������������
*/

Static Function SALVAR()

Local _Alias          := Alias()

DbSelectarea("SF1")

RecLock("SF1",.F.)
	SF1->F1_TRANSP 		:= _cTransportadora
	SF1->F1_EMISSAO 		:= _cData
	SF1->F1_PBRUTO		:= nPesoBruto		
	SF1->F1_PLIQUI		:= nPesoLiq		
	SF1->F1_VOLUME1		:= cVolume
	SF1->F1_ESPECI1		:= cCombo 
	SF1->F1_HORA			:= c_horas
	SF1->F1_MENNOTA		:= mmsgfis
MSUNLOCK()

DbSelectArea(_Alias)

CLOSE(mkwdlg)

Return()

User Function Updfeco()

Local cnota := SF1->F1_DOC
Local cFornec := SF1->F1_FORNECE
Local cyfil  := SF1->F1_FILIAL	


		_qry := "UPDATE "+RetSqlName("SF1")+" SET F1_BASFECP = '0',F1_BSFCCMP = '0' WHERE F1_DOC = '"+Alltrim(cnota)+"' AND F1_FORNECE = '"+Alltrim(cFornec)+"' AND F1_FILIAL = '"+Alltrim(cyfil)+"' AND F1_SERIE = '3' AND D_E_L_E_T_ = ''  "
		nStatus := TcSqlExec(_qry)

		_qry := "UPDATE "+RetSqlName("SD1")+" SET D1_BASFECP='0',D1_ALQFECP='0',D1_VALFECP='0',D1_BSFCCMP='0',D1_ALFCCMP='0',D1_VFCPDIF='0' WHERE D1_DOC = '"+Alltrim(cnota)+"' AND D1_FILIAL = '"+Alltrim(cyfil)+"' AND D1_FORNECE = '"+Alltrim(cFornec)+"' AND D1_SERIE = '3' AND D_E_L_E_T_ = ''  "
		nStatus := TcSqlExec(_qry)

		_qry := "UPDATE "+RetSqlName("SF3")+" SET F3_BSFCCMP = '0' , F3_VFCPDIF = '0' WHERE F3_NFISCAL = '"+Alltrim(cnota)+"' AND F3_CLIEFOR = '"+Alltrim(cFornec)+"' AND F3_FILIAL = '"+Alltrim(cyfil)+"' AND F3_SERIE = '3' AND D_E_L_E_T_ = '' "
		nStatus := TcSqlExec(_qry)
		
		_qry := "UPDATE "+RetSqlName("SFT")+" SET FT_BSFCCMP = '0',FT_ALFCCMP = '0',FT_VFCPDIF = '0' WHERE FT_NFISCAL = '"+Alltrim(cnota)+"' AND FT_CLIEFOR = '"+Alltrim(cFornec)+"' AND FT_FILIAL = '"+Alltrim(cyfil)+"'  AND FT_SERIE = '3' AND D_E_L_E_T_ = '' "
		nStatus := TcSqlExec(_qry)

		_qry := "UPDATE "+RetSqlName("CD2")+" SET D_E_L_E_T_ = '*',R_E_C_D_E_L_ = R_E_C_N_O_ WHERE CD2_DOC = '"+Alltrim(cnota)+"' AND CD2_FILIAL = '"+Alltrim(cyfil)+"'  AND CD2_IMP = 'CMP' AND CD2_SERIE = '3' AND D_E_L_E_T_ = ''  "
		nStatus := TcSqlExec(_qry)

		MessageBox("FECOP zerado com sucesso !","HCOMP004",0)

Return

/*/{Protheus.doc} 
	(long_description)
	@type  Function
	@author Geyson Albano
	@since 05/07/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	@example
	(examples)
	@see (links_or_references)
	/*/
User Function Lmpd1op(cNumNf,cFornec,cSerie,cFild1)

Local _qry := ""
Local cNumNf := SF1->F1_DOC
Local cFornec := SF1->F1_FORNECE
Local cFild1  := SF1->F1_FILIAL	
Local cSerie :=  SF1->F1_SERIE
	
		_qry := "UPDATE "+RetSqlName("SD1")+" SET D1_OP = '' WHERE D1_DOC = '"+Alltrim(cNumNf)+"' AND D1_FORNECE = '"+Alltrim(cFornec)+"' AND D1_FILIAL = '"+Alltrim(cFild1)+"' AND D1_SERIE = '"+Alltrim(cSerie)+"' AND D_E_L_E_T_ = ''  "
		nStatus := TcSqlExec(_qry)

			MessageBox("Num OP zerado com sucesso !","HCOMP004",0)
Return 
