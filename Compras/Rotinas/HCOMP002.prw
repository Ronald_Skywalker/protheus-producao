#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
#include "tbiconn.ch"
#include "ap5mail.ch"
#include "xmlxfun.ch"

#Define CRLF  CHR(13)+CHR(10)

// #########################################################################################
// Projeto: HOPE
// Modulo : Compras
// Fonte  : ImpCte
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor Luiz Alberto| Descricao Importacao Conhecimentos de Transporte
// ---------+-------------------+-----------------------------------------------------------
// 01/07/16 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo
Permite a manuten��o de dados armazenados em .

@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     1/07/2016
/*/
//------------------------------------------------------------------------------------------
user function HCOMP002()
Local x
Local nArq
Local aRecnoSM0 := {}
Local aMarcadas := {}
Local aRetXML	:= {}
Private cPastaPen  := "\interface\NF_Cte\"
Private aLog := {}
Private aNfEntrada := {}
Private aTtEntrada := {}

/*
lOpen := .f.
If ( lOpen := MyOpenSm0(.T.) )

	dbSelectArea( "SM0" )

	While !SM0->( EOF() )
		// So adiciona no aRecnoSM0 se a empresa for diferente
		If aScan( aRecnoSM0, { |x| x[2] == SM0->M0_CODIGO .And. x[3] == SM0->M0_CODFIL } ) == 0 .And. !Empty(SM0->M0_CGC)
			aAdd( aRecnoSM0, { SM0->(Recno()), SM0->M0_CODIGO, SM0->M0_CODFIL } )
		EndIf
		SM0->( dbSkip() )
	End
	SM0->( dbCloseArea() )


	If lOpen
	*/
		aDir 	:= Directory(cPastaPen+"*.XML")
		aRetXML := {}
		For x:=1 To Len(aDir)
			aAdd(aRetXML, { cPastaPen, aDir[x][1] })
		Next x
//		For nIEmp := 1 To Len( aRecnoSM0 )
//			aLog := {}
//			RpcSetType( 3 )
//			RpcSetEnv( aRecnoSM0[nIEmp,2], aRecnoSM0[nIEmp,3] )
			
			For nArq:=1 To Len(aRetXML)
//				Conout("Processando Cte - " + aRetXML[nArq,2] + " - " + Time() + " - " + DtoC(Date()) + " - Empresa: " + aRecnoSM0[nIEmp,2] + " Filial: " + aRecnoSM0[nIEmp,3] + " Aguarde...")

				U_ImpXmlCte(aRetXML[nArq,1]+aRetXML[nArq,2])
				
			Next	

//			RpcClearEnv()

//		Next nIEmp
/*
	EndIf

Else

	lRet := .F.

EndIf
*/

	Alert ("Processo de importa��o Finalizado !")

Return Nil

User Function ImpXmlCte(cArquivo)
Local nI
Local cCodFor := GetNewPar("MV_XCODFOR",'002850')  
Local nAliqIcm 

cAno  := StrZero(Year(dDataBase),4)
cMes  := StrZero(Month(dDataBase),2)

cDirSucesso := 'Processados\'
cDirErro := 'Erro\'
	

If !File(cArquivo)
	Conout("Arquivo " + cArquivo + " N�o Localizado ou J� Processado !")
	Return .t.
Endif
lProc := .t.
//Converte String XML em Objeto XML
If lProc
	cAviso   := ""
	cErro    := ""
	oXmlTab1 := Nil
	oXmlTab1 := XmlParserFile( cArquivo ,"_" , @cAviso,@cErro )
	Do Case
		Case !Empty(cErro)
			Conout("Erro Na Leitura do Arquivo " + cArquivo + ' Erro: '+cErro)
			lProc := .F.
			
		Case !Empty(cAviso)
			Conout("Erro Na Leitura do Arquivo " + cArquivo + ' Aviso: '+cAviso)
			lProc := .F.
			
		Case Empty(oXmlTab1)
			Alert("Erro Na Leitura do Arquivo " + cArquivo + ' Objeto Xml Vazio ')
			//Conout("Erro Na Leitura do Arquivo " + cArquivo + ' Objeto Xml Vazio ')
			lProc := .F.

		OtherWise

			//Prepara Objeto pra receber XML
			oCTe := oXmlTab1
			If Type("oCTE:_CTE") <> "U"
				oCT := oCTE:_CTE:_INFCTE
			Else
				oCT  := oCTE:_CTEPROC:_CTE
				oCTE := oCTE:_CTEPROC
			Endif

			cCNPJ  := ""
			cDoc   := ""
			cSerie := ""
			cCod   := ""
			cLoja  := ""
			cNom   := ""
			_Tipo  := ""
			_Mod   := "CTE"
			_cData := ""
			_dData := ""
			
			_cData := SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,9,2)+SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,6,2)+SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,1,4)
			_dData := ALLTRIM(SUBSTR(_cData,1,2)+'/'+Substr(_cData,3,2)+'/'+SUBSTR(_cData,5,4))

			cStatus	:=	''
			cCNPJDEST := ''
			cCNPJREME := ''
			
			//// Testa a onde deve buscar o CNPJ
			If Type("oCTE:_CTE:_INFCTE:_DEST:_CNPJ:TEXT") <> "U"
				cCNPJDEST := oCTE:_CTE:_INFCTE:_DEST:_CNPJ:TEXT
			Endif
			If Type("oCTE:_CTE:_INFCTE:_REM:_CNPJ:TEXT") <> "U"
				cCNPJREME := oCTE:_CTE:_INFCTE:_REM:_CNPJ:TEXT
			Endif
			If Type("oCTE:_CTE:_INFCTE:_DEST:_CPF:TEXT") <> "U"
				cCNPJDEST := oCTE:_CTE:_INFCTE:_DEST:_CPF:TEXT
			Endif
			If Type("oCTE:_CTE:_INFCTE:_REM:_CPF:TEXT") <> "U"
				cCNPJREME := oCTE:_CTE:_INFCTE:_REM:_CPF:TEXT
			Endif
			If Type("oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT") <> "U"
				cCNPJREME := oCTE:_CTE:_INFCTE:_IDE:_TOMA4:_CNPJ:TEXT
			Endif
			
			If SM0->M0_CGC = cCNPJDEST
				cStatus := ''
			ElseIf SM0->M0_CGC = cCNPJREME
				cStatus := 'A'
			Else
				Alert("N�o Identificada Empresa HOPE nem em Remente nem em Destinatario " + PADL(ALLTRIM(oCTE:_CTE:_INFCTE:_IDE:_NCT:TEXT),9,'0') + " - Arquivo: " + cArquivo)
				//Conout("N�o Identificada Empresa HOPE nem em Remente nem em Destinatario " + PADL(ALLTRIM(oCTE:_CTE:_INFCTE:_IDE:_NCT:TEXT),9,'0') + " - Arquivo: " + cArquivo)
				Return .f.
			Endif
			
			//// Caso o CNPJ nao for da filial ativa nao carrega a nota.
			//If SM0->M0_CGC = Iif(Type("oDados:_CNPJ:TEXT")<>"U",oDados:_CNPJ:TEXT,oDados:_CPF:TEXT)
				cCNPJ  := oCTE:_CTE:_INFCTE:_EMIT:_CNPJ:TEXT
				cDoc   := PADL(ALLTRIM(oCTE:_CTE:_INFCTE:_IDE:_NCT:TEXT),9,'0')
				cSerie := PADR(oCTE:_CTE:_INFCTE:_IDE:_SERIE:TEXT,3)
				_cData := SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,9,2)+SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,6,2)+SUBSTR(oCTE:_CTE:_INFCTE:_IDE:_DHEMI:TEXT,1,4)
				_dData := ALLTRIM(SUBSTR(_cData,1,2)+'/'+Substr(_cData,3,2)+'/'+SUBSTR(_cData,5,4))
				
				//// VerIFica se o CNPJ e de Fornecedor ou de Cliente
				If SA2->(dbSetOrder(3), dbSeek(xFilial("SA2")+cCNPJ)) 
					
					If SA2->A2_COD $ cCodFor
						lProc := .F.
						Conout("Cte n�o importado devido ser fornecedor de frete de compra MP. Verificar par�metro MV_XCODFOR!")

						If !lProc
								cDirDest := Upper(AllTrim(cPastaPen)+cDirErro+cAno+"\"+cMes+"\")
								xFile := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
							Else
								cDirDest := Upper(AllTrim(cPastaPen)+cDirSucesso+cAno+"\"+cMes+"\")
								xFile := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
							EndIf
					
							//Caso nao exista, cria o diretorio no servidor
							MontaDir(cDirDest)
					
							If !File(xFile)
								Copy File &cArquivo To &xFile
							EndIf
					
							lDelOk := .F.
							While !lDelOk
								fErase(cArquivo)
								lDelOk := !File(cArquivo)
								If !lDelOk
									Conout("O XML est� aberto, feche o mesmo para que ele seja movido de pasta!")
								EndIf
							End					
					Else

						lEhFornec  := .T.
						//// VerIFica se a Nota j� existe
						If SF1->(dbSetOrder(1), dbSeek(xFilial("SF1")+cDoc+cSerie+SA2->A2_COD+SA2->A2_LOJA))
							Conout("Nota Fiscal " + cDoc + " Cte J� Lan�ada - Arquivo: " + cArquivo)
							lProc := .F.

							// Se Gerou Cte Com Sucesso Ent�o Joga para Pasta Processados
							
							If !lProc
								cDirDest := Upper(AllTrim(cPastaPen)+cDirErro+cAno+"\"+cMes+"\")
								xFile := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
							Else
								cDirDest := Upper(AllTrim(cPastaPen)+cDirSucesso+cAno+"\"+cMes+"\")
								xFile := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
							EndIf
					
							//Caso nao exista, cria o diretorio no servidor
							MontaDir(cDirDest)
					
							If !File(xFile)
								Copy File &cArquivo To &xFile
							EndIf
					
							lDelOk := .F.
							While !lDelOk
								fErase(cArquivo)
								lDelOk := !File(cArquivo)
								If !lDelOk
									Conout("O XML est� aberto, feche o mesmo para que ele seja movido de pasta!")
								EndIf
							End

						Else 
							lProc := .t.
							cCod  := SA2->A2_COD
							cLoja := SA2->A2_LOJA
							cNom  := SA2->A2_NREDUZ
							_Tipo := "F"
						Endif
					EndIf
				Else
					Alert("Transportadora Cte Nao Localizada " + cCNPJ + " Arquivo: " + cArquivo)
					//Conout("Transportadora Cte Nao Localizada " + cCNPJ + " Arquivo: " + cArquivo)
					lProc := .F.
				
				EndIf
			//Else
//				Conout("Cte Pertence a Outra Filial - Arquivo: " + cArquivo)
				//lProc := .F.
			//Endif
	EndCase
EndIf

//Verifica se produtos do XML estao cadastrados na tabela de relacionamento
If lProc
	aUnidMed  := {}
	aPrdDesc  := {}
	cCodProdut :=  GetNewPar("MV_XCTPRD",'FRETE')
	cTesProdut :=  GetNewPar("MV_XCTTES",'107')
	cCntProdut :=  GetNewPar("MV_XCTCNT",'5010107022          ')
	cCusProdut :=  GetNewPar("MV_XCTCUS",'430      ')
	cNaturez   :=  GetNewPar("MV_XCTNAT",'200      ')
	cCondPag   :=  GetNewPar("MV_XCTPAG",'001')
	
	If lProc .And. !SB1->(dbSetOrder(1), dbSeek(xFilial("SB1")+cCodProdut))
		Conout("Produto Nao Localizado " + cCodProdut + " Cte J� Lan�ada - Arquivo: " + cArquivo)
		lProc := .F.
	Endif

	aAux := {}
	aItens := {}
	If lProc
		If Type("oCTE:_CTEPROC") = "O"
			oCT := oCTE:_CTEPROC:_CTE:_INFCTE
			//// Pega a Chave
			oChaveNFE := oCTE:_CTEPROC:_PROTCTE:_INFPROT:_CHCTE:TEXT
		ElseIf Type("oCTE:_CTE:_INFCTE") = "O"
			oCT := oCTE:_CTE:_INFCTE
			oChaveNFE := SUBSTR(oCTE:_CTE:_SIGNATURE:_SIGNEDINFO:_REFERENCE:_URI:TEXT,5,48)
		ENDIF

		//// Pega o CFOP do Produto
		_CFOP := oCT:_IDE:_CFOP:TEXT
		
		If cFilAnt=='0101'				// Filial 0101 Cfop 2352 TES 107
			//cTesProdut	:=	'107'
			If Left(ALLTRIM(_CFOP),1) = "5"
				_CFOP := "1352"
			ElseIf Left(ALLTRIM(_CFOP),1) = "6"
				_CFOP := "2352"
			ElseIf Left(ALLTRIM(_CFOP),1) = "7"
				_CFOP := "3352"
			Endif
		ElseIf cFilAnt=='0102'			// Filial 0102 Cfop 2353 TES 109
			//cTesProdut	:=	'109'
			If Left(ALLTRIM(_CFOP),1) = "5"
				_CFOP := "1353"
			ElseIf Left(ALLTRIM(_CFOP),1) = "6"
				_CFOP := "2353"
			ElseIf Left(ALLTRIM(_CFOP),1) = "7"
				_CFOP := "3353"
			Endif
		Else
			//cTesProdut	:=	'107'
			If Left(ALLTRIM(_CFOP),1) = "5"
				_CFOP := "1352"
			ElseIf Left(ALLTRIM(_CFOP),1) = "6"
				_CFOP := "2352"
			ElseIf Left(ALLTRIM(_CFOP),1) = "7"
				_CFOP := "3352"
			Endif
		Endif
		cData := SUBSTR(oCT:_IDE:_DHEMI:TEXT,9,2)+SUBSTR(oCT:_IDE:_DHEMI:TEXT,6,2)+SUBSTR(oCT:_IDE:_DHEMI:TEXT,1,4)
		dData := CTOD(SUBSTR(cData,1,2)+'/'+Substr(cData,3,2)+'/'+SUBSTR(cData,5,4))
		
		cNfOri := ''
		If Type("oCT:_infCTeNorm:_infDoc:_infNF") = "O"
			cNfOri := oCT:_infCTeNorm:_infDoc:_infNF:_NDOC:TEXT
		Else
			If Type("oCT:_infCTeNorm:_infDoc:_infNFe") <> "U"
				If Type("oCT:_infCTeNorm:_infDoc:_infNFe") <> "A"
					cNfOri := substr(oCT:_infCTeNorm:_infDoc:_infNFe:_chave:text,26,9) +"-"+substr(oCT:_infCTeNorm:_infDoc:_infNFe:_chave:text,35,1)
				Else
					For nI := 1 To Len(oCT:_infCTeNorm:_infDoc:_infNFe)
						cNfOri := cNfOri += " "+SUBSTR(oCT:_infCTeNorm:_infDoc:_infNFe[nI]:_CHAVE:TEXT,26,9)+"-"+SUBSTR(oCT:_infCTeNorm:_infDoc:_infNFe[nI]:_CHAVE:TEXT,35,1)
					Next
				Endif
			Else
				If Type("oCT:_infCTeNorm:_infDoc:_infOutros") <> "U"
					If Type("oCT:_infCTeNorm:_infDoc:_infOutros") <> "A"
						If Type("oCT:_infCTeNorm:_infDoc:_infOutros:_nDOC") <> "U"
							cNfOri := AllTrim(oCT:_infCTeNorm:_infDoc:_infOutros:_nDOC:text)
						Else
							cNfOri	:= ""
						End
					Else
						For nI := 1 To Len(oCT:_infCTeNorm:_infDoc:_infOutros)
							cNfOri := cNfOri += " "+AllTrim(oCT:_infCTeNorm:_infDoc:_infOutros[nI]:_NDOC:TEXT)
						Next
					Endif
				Endif
			Endif
		Endif
	Endif

	// Valida��o para quando for fornecedores isentos de ICMS vincular uma TES que n�o calcula ICMS. 
	//If oCT:_IMP:_ICMS:_ICMS45 <> Nil 
	If (XmlChildEx ( XmlChildEx ( XmlChildEx ( oCT ,"_IMP") ,"_ICMS") ,"_ICMS45") <> NIL )
		If oCT:_IMP:_ICMS:_ICMS45:_CST:TEXT == "40"
			cTesProdut := '129'
		EndIf
	EndIf

	// Valida��o para buscar a al�quota contida no XML e atribuir no documento fiscal 
	If (XmlChildEx ( XmlChildEx ( XmlChildEx ( oCT ,"_IMP") ,"_ICMS") ,"_ICMS00") <> NIL )
		nAliqIcm := Val(oCT:_IMP:_ICMS:_ICMS00:_PICMS:TEXT)
	EndIf

	aCabec := {}
	aItens := {}
	aItensA:= {}
                
	aadd(aCabec,{"F1_TIPO"		,	"N"			    	})
	aadd(aCabec,{"F1_SERIE"		,	cSerie				})
	aadd(aCabec,{"F1_FORMUL"	,	"N"				    })
	aadd(aCabec,{"F1_DOC"		,	PADL(ALLTRIM(cDoc),9,'0')})
	aadd(aCabec,{"F1_EMISSAO"	,	dData	  		    })
	aadd(aCabec,{"F1_FORNECE"	,	SA2->A2_COD			})
	aadd(aCabec,{"F1_LOJA"		,	SA2->A2_LOJA		})
	aadd(aCabec,{"F1_EST"		,	SA2->A2_EST  		})
	aadd(aCabec,{"F1_ESPECIE"	, 	"CTE"		 	    })
	aadd(aCabec,{"F1_CHVNFE"	,	oChaveNFE			})
	aadd(aCabec,{"F1_HORA"		,	LEFT(TIME(),5)	    })
	aadd(aCabec,{"F1_DTDIGT"	,	dDataBase		    })
	aadd(aCabec,{"F1_MENNOTA"	,	"Referente a Nota Fiscal: "+cNfOri})
	aadd(aCabec,{"F1_COND"		,	cCondPag			})
	aadd(aCabec,{"F1_TPCTE"		,	"N"					})

	aadd(aCabec,{"F1_VALBRUT"	,	VAL(oCT:_VPREST:_VTPREST:TEXT)})
	aadd(aCabec,{"F1_VALMERC"	,	VAL(oCT:_VPREST:_VTPREST:TEXT)})
	
	
	aadd(aItensA,{"D1_COD" 		,SB1->B1_COD	,Nil})
	aadd(aItensA,{"D1_UM"	   	,SB1->B1_UM		,Nil})
	aadd(aItensA,{"D1_LOCAL"	,SB1->B1_LOCPAD	,Nil})
	aadd(aItensA,{"D1_QUANT"	,1				,Nil})
	aadd(aItensA,{"D1_VUNIT"	,VAL(oCT:_VPREST:_VTPREST:TEXT)	,Nil})
	aadd(aItensA,{"D1_TOTAL"	,VAL(oCT:_VPREST:_VTPREST:TEXT) ,Nil})
	//aAdd(aItensA,{"D1_TES"     	,Iif(!Empty(cStatus),cTesProdut,''), Nil})
	aAdd(aItensA,{"D1_TES"     	,cTesProdut, Nil})

    If (XmlChildEx ( XmlChildEx ( XmlChildEx ( oCT ,"_IMP") ,"_ICMS") ,"_ICMS00") <> NIL )
		aAdd(aItensA,{"D1_PICM"     ,nAliqIcm, Nil})
	EndIf

	aadd(aItens,aItensA)

	If Len(aCabec)> 0 .And. Len(aItens)> 0
		lMSErroAuto := .F.
		MSExecAuto({|x,y| mata103(x,y)},aCabec,aItens)

		If lMsErroAuto
			MostraErro()
			DisarmTransaction()

			MsgAlert("Erro ao Processar o XML","Aten��o")
		
		Else
			Conout("Nota Gerada Com Sucesso !!! - Cte " + PADL(ALLTRIM(oCT:_IDE:_NCT:TEXT),9,'0') + " - Arquivo: " + cArquivo)
			lProc := .t.
		
			// Se Gerou Cte Com Sucesso Ent�o Joga para Pasta Processados
		
			If !lProc
				cDirDest := Upper(AllTrim(cPastaPen)+cDirErro+cAno+"\"+cMes+"\")
				xFile := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
			Else
				cDirDest := Upper(AllTrim(cPastaPen)+cDirSucesso+cAno+"\"+cMes+"\")
				xFile := StrTran(Upper(cArquivo),Upper(AllTrim(cPastaPen)),cDirDest)
			EndIf

			//Caso nao exista, cria o diretorio no servidor
			MontaDir(cDirDest)

			If !File(xFile)
				Copy File &cArquivo To &xFile
			EndIf

			lDelOk := .F.
			While !lDelOk
				fErase(cArquivo)
				lDelOk := !File(cArquivo)
				If !lDelOk
					Conout("O XML est� aberto, feche o mesmo para que ele seja movido de pasta!")
				EndIf
			End
		Endif
	Endif
EndIf
/*
		begin transaction

		// Inclusao Nota CTE
	
		If RecLock("SF1",.t.)
			SF1->F1_FILIAL	:=	xFilial("SF1")
			SF1->F1_DOC		:=	PADL(ALLTRIM(oCT:_IDE:_NCT:TEXT),9,'0')
			SF1->F1_SERIE	:=	oCT:_IDE:_SERIE:TEXT
			SF1->F1_ESPECIE	:=	'CTE'
			SF1->F1_EST		:=	SA2->A2_EST
			SF1->F1_TIPO	:=	'N'
			SF1->F1_FORMUL	:=	'N'
			SF1->F1_FORNECE	:=	SA2->A2_COD
			SF1->F1_LOJA	:=	SA2->A2_LOJA
			SF1->F1_COND	:=	cCondPag
			SF1->F1_EMISSAO	:=	dData
			SF1->F1_DTDIGIT	:=	dData
			SF1->F1_VALBRUT	:=	VAL(oCT:_VPREST:_VTPREST:TEXT)
			SF1->F1_VALMERC	:=	VAL(oCT:_VPREST:_VTPREST:TEXT)
			SF1->F1_DUPL	:=	Iif(!Empty(cStatus),PADL(ALLTRIM(oCT:_IDE:_NCT:TEXT),9,'0'),'')
			SF1->F1_BASEICM	:=	Iif(TYPE("oCT:_IMP:_ICMS:_ICMS00:_VBC:TEXT")<>"U",VAL(oCT:_IMP:_ICMS:_ICMS00:_VBC:TEXT),0)
			SF1->F1_VALICM	:=	Iif(TYPE("oCT:_IMP:_ICMS:_ICMS00:_VICMS:TEXT")<>"U",VAL(oCT:_IMP:_ICMS:_ICMS00:_VICMS:TEXT),0)
			SF1->F1_STATUS	:=	cStatus
			SF1->F1_RECBMTO :=  dData
			SF1->F1_CHVNFE  :=  oChaveNFE
			SF1->F1_MENNOTA := 	"Referente a Nota Fiscal: "+cNfOri
			SF1->F1_VALIMP5 :=  Round((VAL(oCT:_VPREST:_VTPREST:TEXT) * 7.60)/100,2)
			SF1->F1_VALIMP6 :=  Round((VAL(oCT:_VPREST:_VTPREST:TEXT) * 1.65)/100,2)
			SF1->F1_BASIMP5 := 	VAL(oCT:_VPREST:_VTPREST:TEXT)
			SF1->F1_BASIMP6 :=	VAL(oCT:_VPREST:_VTPREST:TEXT)
			SF1->(MsUnlock())
		Endif

		// Inclusao Item CTE

		If RecLock("SD1",.t.)
			SD1->D1_FILIAL	:=	xFilial("SD1")
			SD1->D1_ITEM	:=	'0001'
			SD1->D1_DOC		:=	PADL(ALLTRIM(oCT:_IDE:_NCT:TEXT),9,'0')
			SD1->D1_SERIE	:=	oCT:_IDE:_SERIE:TEXT
			SD1->D1_FORNECE	:=	SA2->A2_COD
			SD1->D1_LOJA	:=	SA2->A2_LOJA
			SD1->D1_TIPO	:=	'N'
			SD1->D1_COD		:=	SB1->B1_COD
			SD1->D1_UM		:=	SB1->B1_UM
			SD1->D1_QUANT	:=	1
			SD1->D1_TES		:=	Iif(!Empty(cStatus),cTesProdut,'')
			SD1->D1_CF		:=	_CFOP
			SD1->D1_CONTA	:=	SA2->A2_CONTA
			SD1->D1_TP		:=	SB1->B1_TIPO
			SD1->D1_LOCAL	:=	SB1->B1_LOCPAD
			SD1->D1_VUNIT	:=	VAL(oCT:_VPREST:_VTPREST:TEXT)
			SD1->D1_TOTAL	:=	VAL(oCT:_VPREST:_VTPREST:TEXT)
			SD1->D1_BASEICM	:=	Iif(TYPE("oCT:_IMP:_ICMS:_ICMS00:_VBC:TEXT")<>"U",VAL(oCT:_IMP:_ICMS:_ICMS00:_VBC:TEXT),0)
			SD1->D1_VALICM	:=	Iif(TYPE("oCT:_IMP:_ICMS:_ICMS00:_VICMS:TEXT")<>"U",VAL(oCT:_IMP:_ICMS:_ICMS00:_VICMS:TEXT),0)
			SD1->D1_PICM	:=	Iif(TYPE("oCT:_IMP:_ICMS:_ICMS00:_PICMS:TEXT")<>"U",VAL(oCT:_IMP:_ICMS:_ICMS00:_PICMS:TEXT),0)

			// CALCULO DE PIS E COFINS SOBRE CTE�S
			
			SD1->D1_ALQPIS	:=	1.65	
			SD1->D1_ALQCOF	:=	7.60	
			SD1->D1_BASIMP5	:=	VAL(oCT:_VPREST:_VTPREST:TEXT)
			SD1->D1_BASIMP6	:=	VAL(oCT:_VPREST:_VTPREST:TEXT)
			SD1->D1_VALIMP5	:=	Round((VAL(oCT:_VPREST:_VTPREST:TEXT) * 7.60)/100,2)
			SD1->D1_ALQIMP5 :=  7.60
			SD1->D1_VALIMP6	:=	Round((VAL(oCT:_VPREST:_VTPREST:TEXT) * 1.65)/100,2)
			SD1->D1_ALQIMP6 :=  1.65
			
			//////////////////

			SD1->D1_EMISSAO	:=	dData
			SD1->D1_DTDIGIT	:=	dData
			SD1->(MsUnlock())
		Endif

		// Vencimento CTe se data emissao entre 1 a 15, ent�o vencimento no ultimo dia do m�s.
		// se emiss�o ap�s dia 15, ent�o vencimento dia 15 m�s subsequente.
				
		dVencto	:=	Iif(Day(SF1->F1_EMISSAO) >= 15, StoD(Left(DtoS(MonthSum(SF1->F1_EMISSAO,1)),6)+'15'), LastDate(SF1->F1_EMISSAO))
				

		// Inclusao do Titulo Cte

		If !SE2->(dbSetOrder(1), dbSeek(xFilial("SE2")+SF1->F1_SERIE+SF1->F1_DOC))
			If RecLock("SE2",.T.)
				SE2->E2_FILIAL	:=	xFilial("SE2")
				SE2->E2_PREFIXO	:=	SF1->F1_SERIE
				SE2->E2_NUM		:=	SF1->F1_DOC
				SE2->E2_TIPO	:=	'PR'
				SE2->E2_PARCELA	:=	''
				SE2->E2_FORNECE	:=	SA2->A2_COD
				SE2->E2_LOJA	:=	SA2->A2_LOJA
				SE2->E2_EMISSAO	:=	dData
				SE2->E2_EMIS1	:=	dData
				SE2->E2_HIST	:=	'CTE AUTO'
				SE2->E2_NATUREZ :=  Iif(!Empty(SA2->A2_NATUREZ),SA2->A2_NATUREZ,'216110001')
				SE2->E2_MOEDA	:=	1
				SE2->E2_VENCTO	:=	dVencto
				SE2->E2_VENCREA	:=	dVencto
				SE2->E2_VALOR	:=	VAL(oCT:_VPREST:_VTPREST:TEXT)
				SE2->E2_SALDO	:=	VAL(oCT:_VPREST:_VTPREST:TEXT)
				SE2->E2_VLCRUZ	:=	VAL(oCT:_VPREST:_VTPREST:TEXT)
				SE2->E2_ORIGEM	:=	"MATA100"
				SE2->E2_FLUXO	:=	'S'
				SE2->E2_NOMFOR	:=	SA2->A2_NOME
				SE2->E2_XFLAGPG :=  'X'  
				SE2->(MsUnlock())
			Endif
		Endif

		If cStatus=='A'	// Nota Normal Processa Livro
			
			// Gera Livro Automatico
				
			aArea := GetArea()
				
			Pergunte("MTA930",.F.)
				
			MV_PAR01	:=	SF1->F1_EMISSAO
			MV_PAR02	:=	SF1->F1_EMISSAO
			MV_PAR03	:=	1
			MV_PAR04	:=	SF1->F1_DOC
			MV_PAR05	:=	SF1->F1_DOC
			MV_PAR06	:=	SF1->F1_SERIE
			MV_PAR07	:=	SF1->F1_SERIE
			MV_PAR08	:=	SF1->F1_FORNECE
			MV_PAR09	:=	SF1->F1_FORNECE
			MV_PAR10	:=	SF1->F1_LOJA
			MV_PAR11	:=	SF1->F1_LOJA
			MV_PAR12	:=	SM0->M0_CODFIL
			MV_PAR13	:=	SM0->M0_CODFIL
			MV_PAR14	:=	2
			MV_PAR15	:=	2
				
			lAutoA930	:=	.t.
				
			A930RPEntrada(lAutoA930,,.F.)
				
			RestArea(aArea) 	
	
			// Ajuste Tabela SF3 e SFT
			
			_cSql := " UPDATE SFT "
			_cSql += " SET FT_ALQIMP5 = D1_ALQIMP5, "
			_cSql += " 	   FT_ALQIMP6 = D1_ALQIMP6, "
			_cSql += " 	   FT_VALIMP5 = D1_VALIMP5, "
			_cSql += " 	   FT_VALIMP6 = D1_VALIMP6, "
			_cSql += " 	   FT_ALIQPIS = D1_ALQPIS, "
			_cSql += " 	   FT_ALIQCOF = D1_ALQCOF, "
			_cSql += " 	   FT_BASIMP5 = D1_BASIMP5, "
			_cSql += " 	   FT_BASIMP6 = D1_BASIMP6 "
			_cSql += " FROM " + RetSqlName("SFT") + " SFT, " + RetSqlName("SD1") + " SD1 "
			_cSql += " WHERE D1_DOC = FT_NFISCAL "
			_cSql += " AND D1_SERIE = FT_SERIE "
			_cSql += " AND D1_CLIENTE = FT_CLIEFOR "
			_cSql += " AND D1_LOJA = FT_LOJA "
			_cSql += " AND D1_COD = FT_PRODUTO "
			_cSql += " AND D1_ITEM = FT_ITEM "
			_cSql += " AND D1_CF = FT_CFOP "
			_cSql += " AND D1_FILIAL = '" + xFilial("SD2") + "' "
			_cSql += " AND D1_FILIAL = FT_FILIAL "
			_cSql += " AND SFT.D_E_L_E_T_ = '' "
			_cSql += " AND SD1.D_E_L_E_T_ = '' "
			_cSql += " AND D1_DOC = '" + SF1->F1_DOC + "' "
			_cSql += " AND D1_SERIE = '" + SF1->F1_SERIE + "' "
			_cSql += " AND D1_FORNECE = '" + SF1->F1_FORNECE + "' "
			_cSql += " AND D1_LOJA = '" + SF1->F1_LOJA + "' "
			
			TCSqlExec( _cSql )
			TcRefresh("SFT")
			
			_cSql := " UPDATE SF3 "
			_cSql += " SET F3_VALIMP5 = ISNULL((SELECT SUM(FT_VALIMP5) FROM " + RetSqlName("SFT") + " SFT WHERE FT_FILIAL = F3_FILIAL AND FT_CFOP = F3_CFO AND FT_ALIQICM = F3_ALIQICM AND FT_NFISCAL = F3_NFISCAL AND FT_SERIE = F3_SERIE AND FT_CLIEFOR = F3_CLIEFOR AND SFT.D_E_L_E_T_ = ''),0), "
			_cSql += " 	F3_VALIMP6 = ISNULL((SELECT SUM(FT_VALIMP6) FROM " + RetSqlName("SFT") + " SFT WHERE FT_FILIAL = F3_FILIAL AND FT_CFOP = F3_CFO AND FT_ALIQICM = F3_ALIQICM AND FT_NFISCAL = F3_NFISCAL AND FT_SERIE = F3_SERIE AND FT_CLIEFOR = F3_CLIEFOR AND SFT.D_E_L_E_T_ = ''),0), "
			_cSql += " 	F3_BASIMP5 = ISNULL((SELECT SUM(FT_BASIMP5) FROM " + RetSqlName("SFT") + " SFT WHERE FT_FILIAL = F3_FILIAL AND FT_CFOP = F3_CFO AND FT_ALIQICM = F3_ALIQICM AND FT_NFISCAL = F3_NFISCAL AND FT_SERIE = F3_SERIE AND FT_CLIEFOR = F3_CLIEFOR AND SFT.D_E_L_E_T_ = ''),0), "
			_cSql += " 	F3_BASIMP6 = ISNULL((SELECT SUM(FT_BASIMP6) FROM " + RetSqlName("SFT") + " SFT WHERE FT_FILIAL = F3_FILIAL AND FT_CFOP = F3_CFO AND FT_ALIQICM = F3_ALIQICM AND FT_NFISCAL = F3_NFISCAL AND FT_SERIE = F3_SERIE AND FT_CLIEFOR = F3_CLIEFOR AND SFT.D_E_L_E_T_ = ''),0), "
			_cSql += " 	F3_REPROC = 'N'  "
			_cSql += " FROM " + RetSqlName("SF3") + " SF3 "
			_cSql += " WHERE SF3.F3_FILIAL = '" + xFilial("SF3") + "' "
			_cSql += " AND SF3.D_E_L_E_T_ = '' "
			_cSql += " AND SF3.F3_NFISCAL = '" + SF1->F1_DOC + "' "
			_cSql += " AND SF3.F3_SERIE = '" + SF1->F1_SERIE + "' "
			_cSql += " AND SF3.F3_CLIEFOR = '" + SF1->F1_FORNECE + "' "
			_cSql += " AND SF3.F3_LOJA = '" + SF1->F1_LOJA + "' "
			
			TCSqlExec( _cSql )
			TcRefresh("SF3")
			
			RestArea(aArea)
		Endif

		End Transaction
*/  

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
��� Programa � MyOpenSM0� Autor � TOTVS Protheus     � Data �  04/07/2011 ���
�������������������������������������������������������������������������͹��
��� Descricao� Funcao de processamento abertura do SM0 modo exclusivo     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
��� Uso      � MyOpenSM0  - Gerado por EXPORDIC / Upd. V.4.7.2 EFS        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
/*
Static Function MyOpenSM0(lShared)

Local lOpen := .F.
Local nLoop := 0

For nLoop := 1 To 20
	dbUseArea( .T., , "SIGAMAT.EMP", "SM0", lShared, .F. )

	If !Empty( Select( "SM0" ) )
		lOpen := .T.
		dbSetIndex( "SIGAMAT.IND" )
		Exit
	EndIf

	Sleep( 500 )

Next nLoop

If !lOpen
	MsgStop( "N�o foi poss�vel a abertura da tabela " + ;
	IIf( lShared, "de empresas (SM0).", "de empresas (SM0) de forma exclusiva." ), "ATEN��O" )
EndIf

Return lOpen

*/
