#Include 'Rwmake.ch'
   
/*
�����������������������������������������������������������������������������
���Programa  �MT150END  �Autor  �Pedro Augusto       � Data �  20/07/2012 ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao para tratar o campo C8_WF                             ��
�������������������������������������������������������������������������͹��
���Uso       � TV Alphaville                                              ���
�����������������������������������������������������������������������������
*/

User Function MT150END        
	Local _cNum 	:= SC8->C8_NUM             
	Local _cFornece := SC8->C8_FORNECE
	Local _cLoja	:= SC8->C8_LOJA 
	
	
	
	If ParamIXB[1] == 3 // Atualizacao da cotacao

		DBSELECTAREA("SC8")
		DBSetOrder(1)
		DBSeek(xFilial("SC8")+_cNum + _cFornece + _cLoja)
		While !SC8->(EOF()) .AND. SC8->(C8_FILIAL +  C8_NUM + C8_FORNECE + C8_LOJA) == xFilial("SC8") + _cNum + _cFornece + _cLoja   

			Reclock("SC8",.F.)
			SC8->C8_WF		:= " " 
			MSUnlock()
			SC8->(dbSkip()) 
			 
		Enddo	
	Endif
	Return .t.
	
