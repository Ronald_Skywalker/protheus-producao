#Include 'Protheus.ch'

User Function CN100VST()

	Local cNew  := PARAMIXB[1] // PROX STATUS

	Local lRet := .T.

	If cNew == "05" .AND. CN9->CN9_YAPROV != "A"
		lRet := .F.
		MessageBox("Contrato pendente de aprova��o","CN100VST",16)
	EndIf

	If cNew != "05" .AND. CN9->CN9_YAPROV == "A" // CASO O CONTRATO APROVADO SEJA ESTORNARDO O MESMO PRECISAR� DE UMA NOVA APROVA��O
		RecLock("CN9",.F.)
		CN9->CN9_YAPROV := ""
		CN9->CN9_YURINC := ""
		CN9->(MsUnlock())
	EndIf


Return lRet
