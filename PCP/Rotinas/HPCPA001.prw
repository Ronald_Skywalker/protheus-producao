#INCLUDE "protheus.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPCPA001  �Autor  �Bruno Parreira      � Data �  03/11/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa para geracao dos pedidos de venda de remessa para  ���
���          �beneficiamento nas faccoes.                                 ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HPCPA001()

	Private cFornece := ""

	cAlias	:= "SC2"
	chkFile(cAlias)
	dbSELECTArea(cAlias)
	dbSetOrder(1)
	private cCadastro := "Gera��o de Pedidos de Beneficiamento"

	aRotina := {;
		{ "Pesquisar" 			, "AxPesqui"			, 0, 1},;
		{ "Remanejamento"		, "U_HREMANJ(SC2->C2_NUM)" , 0, 1},;
		{ "Ger.Pedido"			, "U_HLIB(SC2->C2_NUM)"	, 0, 3}}

	dbSELECTArea(cAlias)
	mBrowse( 6, 1, 22, 75, cAlias)

Return

USer Function HLib(_op)

	Local cPerg   := "HPCPA001"
	Local cQuery  := ""
	Local cGrpExc := AllTrim(SuperGetMv("MV_XGRPEXC",.F.,"'MP90','BN00','MP05'")) //Grupos de produto que serao excluidos do pedido de beneficiamento
	Local cNroPed := ""
	Local nGerado := 0
	Local nErro   := 0
//Local cTabPrc := AllTrim(SuperGetMv("MV_XTBPRBN",.F.,"303")) //Tabela de preco para pedidos de beneficiamento
	Local nPrcTab := 0
	Local cTpOper := AllTrim(SuperGetMv("MV_XTPOPBN",.F.,"04")) //Tipo de Operacao para Pedido de Benefiamento
	Local cTES    := ""
	Local cOperac := AllTrim(SuperGetMv("MV_XOPERBN",.F.,"20")) //Operacao que deve ser apontada para permitir nota de beneficiamento
	Local cOPErro := ""
	Local aErro   := {}
	Local cPoliti := AllTrim(SuperGetMv("MV_XPOLIBN",.F.,"099")) //Politica Comercial pedido de beneficiamento
	Local cTpPedi := AllTrim(SuperGetMv("MV_XTPPDBN",.F.,"070")) //Tipo de pedido para pedido de beneficiamento
	Local cCondPg := AllTrim(SuperGetMv("MV_XCONDBN",.F.,"B01")) //Condicao de pagamento padrao pedido de beneficiamento
	Local dEntreg := DaySum(DDATABASE,Val(SuperGetMv("MV_XENTRBN",.F.,"15"))) //Dias da data de entrega do pedido
	//Local cNature := AllTrim(SuperGetMv("MV_XNATUBN",.F.,"21617801")) //Natureza padrao caso nao tenha amarrada ao fornecedor
	Local nx      := 0

	Private lMsErroAuto := .F.

	Private cLog := ""

	cTime   := Time()
	cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

	If Pergunte(cPerg)

		If  Alltrim(MV_PAR01) != "002878" .AND. !Empty(Alltrim(MV_PAR01))

			If MsgYesNo("Confirma atualiza��o do Fornecedor Externo ?","Confirma��o")
				_QRY := "UPDATE "+RETSQLNAME("SC2")+" SET C2_YFORNEC = '"+MV_PAR01+"' ,C2_YLOJAFO = '"+MV_PAR02+"', C2_TPPR = 'E' WHERE C2_NUM = '"+_OP+"' AND D_E_L_E_T_ = ''"
				TCSQLEXEC(_QRY)

				_qry := "UPDATE "+RetSqlName("SC2")+" SET C2_RECURSO = '"+MV_PAR03+"' FROM "+RetSqlName("SC2")+" SC2 "
				_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = C2_PRODUTO "
				_qry += "WHERE SC2.D_E_L_E_T_ = '' AND B1_TIPO IN ('PI','PF') AND C2_NUM = '"+_OP+"' "
				TcSqlExec(_qry)

			Endif
		EndIf	


		GravaLog("In�cio da rotina")

		cQuery := "SELECT D4_COD,SUBSTRING(D4_OP,1,11) AS D4_OP, D4_LOCAL,C2_YFORNEC,C2_YLOJAFO,SUM(D4_QTDEORI) AS D4_QTDEORI,SUM(D4_QUANT) AS D4_QUANT FROM ( "
		cQuery += CRLF + "SELECT D4_COD,D4_OP,D4_LOCAL,D4_DATA,D4_QTDEORI,D4_QUANT,D4_LOTECTL,D4_NUMLOTE,D4_SITUACA,C2_YFORNEC,C2_YLOJAFO,C2_NUM,C2_PRODUTO,C2_ITEM "
		cQuery += CRLF + "FROM "+RetSqlName("SD4")+" SD4 (NOLOCK) "
		cQuery += CRLF + "INNER JOIN "+RetSqlName("SB1")+" SB1 (NOLOCK) "
		cQuery += CRLF + "ON B1_COD = D4_COD "
		cQuery += CRLF + "AND B1_GRUPO NOT IN ("+cGrpExc+") "
		cQuery += CRLF + "AND SB1.D_E_L_E_T_   = '' "
		cQuery += CRLF + "INNER JOIN "+RetSqlName("SC2")+" SC2 (NOLOCK) "
		cQuery += CRLF + "ON C2_NUM      = SUBSTRING(D4_OP,1,6) "
		cQuery += CRLF + "AND C2_ITEM    = SUBSTRING(D4_OP,7,2) "
		cQuery += CRLF + "AND C2_SEQUEN  = SUBSTRING(D4_OP,9,3) "
		cQuery += CRLF + "AND C2_ITEMGRD = SUBSTRING(D4_OP,12,3) "
		cQuery += CRLF + "AND SC2.D_E_L_E_T_   = '' "
		cQuery += CRLF + "LEFT JOIN "+RetSqlName("SG1")+" SG1 (NOLOCK) "
		cQuery += CRLF + "ON G1_COD = C2_PRODUTO "
		cQuery += CRLF + "AND G1_COMP = D4_COD AND G1_COD+''+G1_REVINI NOT IN (SELECT G5_PRODUTO+''+G5_REVISAO FROM SG5010 WHERE D_E_L_E_T_='' AND G5_STATUS=2) "
		cQuery += CRLF + "AND SG1.D_E_L_E_T_ = '' "
		cQuery += CRLF + "INNER JOIN "+RetSqlName("SH6")+" SH6 (NOLOCK) "
		cQuery += CRLF + "ON H6_OP = D4_OP "
		cQuery += CRLF + "AND H6_PRODUTO = C2_PRODUTO "
		cQuery += CRLF + "AND H6_OPERAC >= '"+cOperac+"' "
		cQuery += CRLF + "AND SH6.D_E_L_E_T_   = '' "
		cQuery += CRLF + "WHERE SD4.D_E_L_E_T_ = '' "
		cQuery += CRLF + "AND D4_FILIAL  = '"+xFilial("SD4")+"' "
		cQuery += CRLF + "AND D4_QUANT   > 0 "
		cQuery += CRLF + "AND D4_OP      LIKE '"+_op+"%' ) X "
		cQuery += CRLF + "GROUP BY D4_COD,SUBSTRING(D4_OP,1,11), D4_LOCAL,C2_YFORNEC,C2_YLOJAFO "
		cQuery += CRLF + "ORDER BY D4_OP,D4_COD "

		MemoWrite("HPCPA001.txt",cQuery)

		cQuery := ChangeQuery(cQuery)

		If SELECT("TRB") > 0
			TRB->(DbCloseArea())
		EndIf

		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)

		GravaLog("Parametros: OP de: "+mv_par01+" OP ate: "+mv_par02)

		DbSELECTArea("TRB")

		If (TRB->(!EOF()) .AND. TRB->C2_YFORNEC == "002878") .OR. (TRB->(!EOF()) .AND. Alltrim(MV_PAR01) == "002878")

			If MsgYesNo("Confirma atualiza��o do Fornecedor Interno ?","Confirma��o")
				_QRY := "UPDATE "+RETSQLNAME("SC2")+" SET C2_YFORNEC = '"+MV_PAR01+"' ,C2_YLOJAFO = '"+MV_PAR02+"', C2_TPPR = 'I' WHERE C2_NUM = '"+_OP+"' AND D_E_L_E_T_ = ''"
				TCSQLEXEC(_QRY)

				_qry := "UPDATE "+RetSqlName("SC2")+" SET C2_RECURSO = '"+MV_PAR03+"' FROM "+RetSqlName("SC2")+" SC2 "
				_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = C2_PRODUTO "
				_qry += "WHERE SC2.D_E_L_E_T_ = '' AND B1_TIPO IN ('PI','PF') AND C2_NUM = '"+_OP+"' "
				TcSqlExec(_qry)
				MessageBox("Fornecedor Atualizado com sucesso! ","TOTVS",0)
			Endif
			Return

		Elseif TRB->(EOF())

			MsgAlert("OP n�o tem a opera��o 20 apontada.","A T E N � � O")
			GravaLog("OP n�o tem a opera��o 20 apontada.")

			TRB->(DbCloseArea())

			GravaLog("Fim do processamento.")

			MemoWrite("\log_benef\PEDBEN_"+cDtTime+".log",cLog)
			Return
		EndIf

	EndIf

	If (TRB->(!EOF()) .AND. TRB->C2_YFORNEC != "002878") .OR. (TRB->(!EOF()) .AND. Alltrim(MV_PAR01) != "002878")

		If !MsgYesNo("Confirma gera��o dos pedidos?","Confirma��o")
			TRB->(DbCloseArea())
			GravaLog("Cancelado pelo usu�rio.")
			Return
		EndIf

		cOP := ""

		While TRB->(!EOF())

			If Empty(TRB->C2_YFORNEC)
				GravaLog("OP sem fornecedor amarrado. "+TRB->D4_OP)
				TRB->(DbSkip())
				Loop
			EndIf

			IF SELECT("TMP") > 0
				TMP->(DbCloseArea())
			EndIf

			_cqry := " SELECT C5_XNUMOP AS OP FROM "+RetSqlName("SC5")+" WHERE C5_XNUMOP = '"+TRB->D4_OP+"' AND D_E_L_E_T_ = ''  "

			DbUseArea( .T., 'TOPCONN', TCGENQRY(,,_cqry),"TMP", .F., .T.)

			IF !Empty(Alltrim(TMP->OP))
				MsgAlert("J� existe um pedido de beneficiamento gerado para essa OP "+TRB->D4_OP+" " ,"Hope")
				return
			Endif

			DbSELECTArea("SB1")
			DbSeek(xFilial("SB1")+TRB->D4_COD)

			nPrcTab := 0

			If SB1->B1_CUSTD > 0
				nPrcTab := SB1->B1_CUSTD
			Else
				aAdd(aErro,{TRB->D4_OP,TRB->D4_COD,SB1->B1_DESC})
				cOPErro := TRB->D4_OP
				TRB->(DbSkip())
				Loop
			EndIf

			DbSELECTArea("SA2")
			DbSetOrder(1)
			DbSeek(xFilial("SA2")+TRB->C2_YFORNEC+TRB->C2_YLOJAFO)

			If cOP <> TRB->D4_OP
				If !Empty(cNroPed)
					RollBackSX8()
				EndIf

				DbSELECTArea("SC5")
				cNroPed := GetSX8Num("SC5","C5_NUM",,1)

				GravaLog("Montando array para a OP: "+TRB->D4_OP+" Fornecedor: "+TRB->C2_YFORNEC+"/"+TRB->C2_YLOJAFO)

				aCabec := {}
				aadd(aCabec,{"C5_TIPO"   ,"B"            ,Nil})
				aadd(aCabec,{"C5_CLIENTE",TRB->C2_YFORNEC,Nil})
				aadd(aCabec,{"C5_LOJACLI",TRB->C2_YLOJAFO,Nil})
				aadd(aCabec,{"C5_LOJAENT",TRB->C2_YLOJAFO,Nil})
				aadd(aCabec,{"C5_POLCOM" ,cPoliti        ,Nil})
				aadd(aCabec,{"C5_TPPED"  ,cTpPedi        ,Nil})
				aadd(aCabec,{"C5_CONDPAG",cCondPg        ,Nil})
				aadd(aCabec,{"C5_NATUREZ",SA2->A2_NATUREZ,Nil})
				aadd(aCabec,{"C5_XNUMOP" ,TRB->D4_OP     ,Nil})
				aadd(aCabec,{"C5_XBLQ"   ,"L"            ,Nil})

				aItens := {}

				cIt := "00"

				cOP := TRB->D4_OP
			EndIf

			cIt := SOMA1(cIt)

			//cTES  := MaTesInt(2,cTpOper,TRB->C2_YFORNEC,TRB->C2_YLOJAFO,"F",SB1->B1_COD,) // RETIRADA FUN��O AP�S A VIRADA PARA 27
			cTES  := "751"																	// ALINHADO COM O IGOR E BRUNA CHAPAR A TES 751 E-MAIL DIA 07/09 para protheus@hopelingerie.com.br
			cEstFor:= Posicione("SA2",1,xFilial("SA2")+TRB->C2_YFORNEC+TRB->C2_YLOJAFO,"A2_EST")
			
			If cEstFor != "CE"
				cCF	  :="6901"
			Else
				cCF	  :="5901"	
			EndIf

			aLinha := {}
			aadd(aLinha,{"C6_ITEM"   ,cIt          ,Nil})
			aadd(aLinha,{"C6_PRODUTO",SB1->B1_COD  ,Nil})
			aadd(aLinha,{"C6_QTDVEN" ,TRB->D4_QUANT,Nil})
			aadd(aLinha,{"C6_PRCVEN" ,nPrcTab      ,Nil})
			aadd(aLinha,{"C6_PRUNIT" ,nPrcTab      ,Nil})
			aadd(aLinha,{"C6_VALOR"  ,ROUND(TRB->D4_QUANT*nPrcTab,2),Nil})
			aadd(aLinha,{"C6_OPER"   ,cTpOper      ,Nil})
			aadd(aLinha,{"C6_TES"    ,cTES         ,Nil})
			aadd(aLinha,{"C6_CF"    ,cCF   		   ,Nil})
			aadd(aLinha,{"C6_ENTREG" ,dEntreg      ,Nil})
			aadd(aLinha,{"C6_LOCAL"  ,TRB->D4_LOCAL,Nil})
			aadd(aItens,aLinha)

			GravaLog("OP: "+TRB->D4_OP+" Item: "+SB1->B1_COD)

			TRB->(DbSkip())

			If cOP <> TRB->D4_OP
				If cOp <> cOPErro
					PROCESSA({|| MsExecAuto({|x, y, z| MATA410(x, y, z)}, aCabec, aItens,3) }, "Gerando Pedido ...  ",,.F. ) 

					If !lMsErroAuto
						GravaLog("Pedido "+SC5->C5_NUM+" inclu�do com sucesso para a OP "+TRB->D4_OP)
						DbSELECTArea("SC5")
						ConfirmSX8()
						nGerado++
						DbSELECTArea("SC2")
						DbSetOrder(1)
						If DbSeek(xFilial("SC2")+cOP)
							While SC2->(!EOF()) .AND. SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN = cOP
								RecLock("SC2",.F.)
								SC2->C2_XPEDBEN := SC5->C5_NUM
								MsUnlock()
								SC2->(DbSkip())
							EndDo
						EndIf
					Else
						GravaLog("Ocorreu erro na inclusao do pedido "+SC5->C5_NUM+" para a OP "+TRB->D4_OP)
						MostraErro()
						RollBackSX8()
						nErro++
					EndIf
					lMsErroAuto := .F.

					cNroPed := ""
				EndIf
				cOPErro := ""
			EndIf
		EndDo

		If Len(aErro) > 0
			cMsgErro := "As OPs e produtos abaixo n�o tem custo standard cadastrado:"
			For nx := 1 to Len(aErro)
				cMsgErro += "OP: "+aErro[nx][1]+" Prod.:"+aErro[nx][2]+" - "+aErro[nx][3]
			Next
			MsgAlert(cMsgErro,"Aten��o")
		EndIf

		MsgInfo("Pedidos gerados: "+AllTrim(Str(nGerado))+". Pedidos com erro:"+AllTrim(Str(nErro))+"."+CRLF+"Para mais detalhes, verifique o log.","Aviso")
		GravaLog("Pedidos gerados: "+AllTrim(Str(nGerado))+". Pedidos com erro:"+AllTrim(Str(nErro))+".")

	Elseif TRB->(EOF())

		MsgAlert("OP n�o tem a opera��o 20 apontada.","A T E N � � O")
		GravaLog("OP n�o tem a opera��o 20 apontada.")
	EndIf

	TRB->(DbCloseArea())



	GravaLog("Fim do processamento.")

	MemoWrite("\log_benef\PEDBEN_"+cDtTime+".log",cLog)

Return

Static Function HSALDO(cProduto,cArmazem)
	Local nRet := 0

	DbSELECTArea("SB2")
	DbSetOrder(1)
	If DbSeek(xFilial("SB2")+cProduto+cArmazem)
		nRet := SB2->B2_QATU-SB2->B2_RESERVA-SB2->B2_QEMP-SB2->B2_XRESERV 
	EndIf

Return nRet

Static Function GravaLog(cMsg)
	Local cHora   := ""
	Local cDtHora := ""

	cHora   := Time()
	cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

	Conout(cMsg)
	cLog += CRLF+cDtHora+": HPCPA001 - "+cMsg

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 03/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

	Local aAreaAtu	:= GetArea()
	Local aAreaSX1	:= SX1->( GetArea() )

	PutSx1(cPerg,"01","N�mero da OP de ? ","N�mero da OP de ? ","N�mero da OP de ? ","Mv_ch1",TAMSX3("D4_OP")[3],6,TAMSX3("D4_OP")[2],0,"G","","SC2","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o n�mero da OP de.         ",""},{""},{""},"")
	PutSx1(cPerg,"02","N�mero da OP at� ?","N�mero da OP at� ?","N�mero da OP at� ?","Mv_ch2",TAMSX3("D4_OP")[3],6,TAMSX3("D4_OP")[2],0,"G","","SC2","","N","Mv_par02","","","","","","","","","","","","","","","","",{"Informe o n�mero da OP at�.        ",""},{""},{""},"")

	RestArea( aAreaSX1 )
	RestArea( aAreaAtu )

Return(cPerg)

User Function HREMANJ(_op)

    Local aPergs := {}
    Local aRet := {}
	Local cQuery  := ""
	Local cGrpExc := AllTrim(SuperGetMv("MV_XGRPEXC",.F.,"'MP90','BN00','MP05'")) //Grupos de produto que serao excluidos do pedido de beneficiamento
	Local cNroPed := ""
	Local nGerado := 0
	Local nErro   := 0
	Local nPrcTab := 0
	Local cTpOper := AllTrim(SuperGetMv("MV_XTPOPBN",.F.,"04")) //Tipo de Operacao para Pedido de Benefiamento
	Local cTES    := ""
	Local cOperac := AllTrim(SuperGetMv("MV_XOPERBN",.F.,"20")) //Operacao que deve ser apontada para permitir nota de beneficiamento
	Local cOPErro := ""
	Local aErro   := {}
	Local cPoliti := AllTrim(SuperGetMv("MV_XPOLIBN",.F.,"099")) //Politica Comercial pedido de beneficiamento
	Local cTpPedi := AllTrim(SuperGetMv("MV_XTPPDBN",.F.,"070")) //Tipo de pedido para pedido de beneficiamento
	Local cCondPg := AllTrim(SuperGetMv("MV_XCONDBN",.F.,"B01")) //Condicao de pagamento padrao pedido de beneficiamento
	Local dEntreg := DaySum(DDATABASE,Val(SuperGetMv("MV_XENTRBN",.F.,"15"))) //Dias da data de entrega do pedido
	Local nx      := 0
	Local aItens := {}

	Private lMsErroAuto := .F.

	Private cLog := ""

	cTime   := Time()
	cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
	Private oReport
    Private cPergCont	:= 'HREMANJ'

/************************
*Monta pergunte do Log *
************************/

aAdd(aPergs,{1,"Fornecedor",Space(6),"","ExistCPO('SA2')","SA2","",50,.F.}) // Tipo caract
aAdd(aPergs,{1,"Loja",Space(4),"","","","",50,.F.}) // Tipo caract
aAdd(aPergs,{1, "Recurso", Space(6), "", "ExistCPO('SH1')","SH1", "", 0, .F.}) //ParamBox + ExistCPO

    If  ParamBox(aPergs,"Par�metros",@aRet,{||.T.},,,,,,"U_HREMANJ",.T.,.T.)
		 cFornece 	:= AllTrim(aRet[1])
		 cLoja 		:= AllTrim(aRet[2])
		 cRecurso 	:= AllTrim(aRet[3])
    Else
   		 MessageBox("Cancelado Pelo Usu�rio!!!", "TOTVS", 16)
		Return	
    Endif

	GravaLog("In�cio da rotina HREMANJ !!!")

		cQuery := "SELECT D4_COD,SUBSTRING(D4_OP,1,11) AS D4_OP, D4_LOCAL,C2_YFORNEC,C2_YLOJAFO,SUM(D4_QTDEORI) AS D4_QTDEORI,SUM(D4_QUANT) AS D4_QUANT FROM ( "
		cQuery += CRLF + "SELECT D4_COD,D4_OP,D4_LOCAL,D4_DATA,D4_QTDEORI,D4_QUANT,D4_LOTECTL,D4_NUMLOTE,D4_SITUACA,C2_YFORNEC,C2_YLOJAFO,C2_NUM,C2_PRODUTO,C2_ITEM "
		cQuery += CRLF + "FROM "+RetSqlName("SD4")+" SD4 (NOLOCK) "
		cQuery += CRLF + "INNER JOIN "+RetSqlName("SB1")+" SB1 (NOLOCK) "
		cQuery += CRLF + "ON B1_COD = D4_COD "
		cQuery += CRLF + "AND B1_GRUPO NOT IN ("+cGrpExc+") "
		cQuery += CRLF + "AND SB1.D_E_L_E_T_   = '' "
		cQuery += CRLF + "INNER JOIN "+RetSqlName("SC2")+" SC2 (NOLOCK) "
		cQuery += CRLF + "ON C2_NUM      = SUBSTRING(D4_OP,1,6) "
		cQuery += CRLF + "AND C2_ITEM    = SUBSTRING(D4_OP,7,2) "
		cQuery += CRLF + "AND C2_SEQUEN  = SUBSTRING(D4_OP,9,3) "
		cQuery += CRLF + "AND C2_ITEMGRD = SUBSTRING(D4_OP,12,3) "
		cQuery += CRLF + "AND SC2.D_E_L_E_T_   = '' "
		cQuery += CRLF + "LEFT JOIN "+RetSqlName("SG1")+" SG1 (NOLOCK) "
		cQuery += CRLF + "ON G1_COD = C2_PRODUTO "
		cQuery += CRLF + "AND G1_COMP = D4_COD AND G1_COD+''+G1_REVINI NOT IN (SELECT G5_PRODUTO+''+G5_REVISAO FROM SG5010 WHERE D_E_L_E_T_='' AND G5_STATUS=2) "
		cQuery += CRLF + "AND SG1.D_E_L_E_T_ = '' "
		cQuery += CRLF + "INNER JOIN "+RetSqlName("SH6")+" SH6 (NOLOCK) "
		cQuery += CRLF + "ON H6_OP = D4_OP "
		cQuery += CRLF + "AND H6_PRODUTO = C2_PRODUTO "
		cQuery += CRLF + "AND H6_OPERAC >= '"+cOperac+"' "
		cQuery += CRLF + "AND SH6.D_E_L_E_T_   = '' "
		cQuery += CRLF + "WHERE SD4.D_E_L_E_T_ = '' "
		cQuery += CRLF + "AND D4_FILIAL  = '"+xFilial("SD4")+"' "
		cQuery += CRLF + "AND D4_QUANT   > 0 "
		cQuery += CRLF + "AND D4_OP      LIKE '"+_op+"%' ) X "
		cQuery += CRLF + "GROUP BY D4_COD,SUBSTRING(D4_OP,1,11), D4_LOCAL,C2_YFORNEC,C2_YLOJAFO "
		cQuery += CRLF + "ORDER BY D4_OP,D4_COD "

		MemoWrite("HREMANJ.txt",cQuery)

		cQuery := ChangeQuery(cQuery)

		If SELECT("TRB") > 0
			TRB->(DbCloseArea())
		EndIf

		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)

		DbSELECTArea("TRB")

		If (TRB->(!EOF()) .AND. cFornece == "002878") 

			If MsgYesNo("Confirma atualiza��o do Fornecedor Interno ?","Confirma��o")
				_QRY := "UPDATE "+RETSQLNAME("SC2")+" SET C2_YFORNEC = '"+cFornece+"' ,C2_YLOJAFO = '"+cLoja+"', C2_TPPR = 'I',C2_XPEDBEN = '' WHERE C2_NUM = '"+_OP+"' AND D_E_L_E_T_ = ''"
				TCSQLEXEC(_QRY)

				_qry := "UPDATE "+RetSqlName("SC2")+" SET C2_RECURSO = '"+cRecurso+"' FROM "+RetSqlName("SC2")+" SC2 "
				_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = C2_PRODUTO "
				_qry += "WHERE SC2.D_E_L_E_T_ = '' AND B1_TIPO IN ('PI','PF') AND C2_NUM = '"+_OP+"' "
				TcSqlExec(_qry)
			Endif
			

		ElseIf (TRB->(!EOF()) .AND. cFornece != "002878") 

			If MsgYesNo("Confirma atualiza��o do Fornecedor Externo ?","Confirma��o")
				_QRY := "UPDATE "+RETSQLNAME("SC2")+" SET C2_YFORNEC = '"+cFornece+"' ,C2_YLOJAFO = '"+cLoja+"', C2_TPPR = 'E' WHERE C2_NUM = '"+_OP+"' AND D_E_L_E_T_ = ''"
				TCSQLEXEC(_QRY)

				_qry := "UPDATE "+RetSqlName("SC2")+" SET C2_RECURSO = '"+cRecurso+"',C2_XPEDBEN = ''  FROM "+RetSqlName("SC2")+" SC2 "
				_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = C2_PRODUTO "
				_qry += "WHERE SC2.D_E_L_E_T_ = '' AND B1_TIPO IN ('PI','PF') AND C2_NUM = '"+_OP+"' "
				TcSqlExec(_qry)

			Endif
		
		Elseif TRB->(EOF())

			MsgAlert("OP n�o tem a opera��o 20 apontada.","A T E N � � O")
			GravaLog("OP n�o tem a opera��o 20 apontada.")

			TRB->(DbCloseArea())

			GravaLog("Fim do processamento.")

			MemoWrite("\log_benef\PEDBEN_"+cDtTime+".log",cLog)
			Return
		EndIf

	If TRB->(!EOF()) 
		If  cFornece != "002878"
			If !MsgYesNo("Confirma gera��o dos pedidos?","Confirma��o")
				TRB->(DbCloseArea())
				GravaLog("Cancelado pelo usu�rio.")
				Return
			EndIf
		EndIf
		cOP := ""

			_cqry := " SELECT C5_XNUMOP AS OP, * FROM "+RetSqlName("SC5")+" WHERE C5_XNUMOP = '"+TRB->D4_OP+"' AND D_E_L_E_T_ = ''  "

				If SELECT("PED") > 0
					PED->(DbCloseArea())
				EndIf

			DbUseArea( .T., 'TOPCONN', TCGENQRY(,,_cqry),"PED", .F., .T.)


			While PED->(!EOF()) 
					If !Empty(Alltrim(PED->OP)) .AND. Empty(Alltrim(PED->C5_NOTA)) 
						cPedido := PED->C5_NUM 
						aCabec := {}
					
							aAdd( aCabec, {"C5_NUM"          ,PED->C5_NUM         , Nil} )
							aAdd( aCabec, {"C5_TIPO"     	 ,PED->C5_TIPO        , Nil} )
							aAdd( aCabec, {"C5_CLIENTE"      ,PED->C5_CLIENTE  	  , Nil} )
							aAdd( aCabec, {"C5_LOJACLI"      ,PED->C5_LOJACLI  	  , Nil} )
							aAdd( aCabec, {"C5_LOJAENT"      ,PED->C5_LOJAENT  	  , Nil} )
							aAdd( aCabec, {"C5_CONDPAG"      ,PED->C5_CONDPAG  	  , Nil} )

							dbSelectArea("SC6")
							dbSetOrder(1)
							SC6->(dbSeek(PED->C5_FILIAL+cPedido))

						While !SC6->(EOF()) .AND. SC6->(C6_FILIAL+C6_NUM) == (PED->C5_FILIAL+cPedido)

							aLinha := {}
							
							aAdd(aLinha,{"C6_FILIAL" 	,SC6->C6_FILIAL  	,Nil})
							aAdd(aLinha,{"C6_ITEM"   	,SC6->C6_ITEM 		,Nil})
							aAdd(aLinha,{"C6_PRODUTO"	,SC6->C6_PRODUTO  	,Nil})
							aAdd(aLinha,{"C6_QTDVEN" 	,SC6->C6_QTDVEN  	,Nil})               
							AADD(aLinha,{"C6_QTDLIB"	,SC6->C6_QTDLIB		,nil})
							aAdd(aLinha,{"C6_PRCVEN"	,SC6->C6_PRCVEN		,Nil})
							aAdd(aLinha,{"C6_TES" 		,SC6->C6_TES  		,Nil})  
							
							aAdd(aItens,aLinha)	

							SC6->(dbSkip())		
					
						EndDo		
				
						lMsErroAuto := .F.

						PROCESSA({|| MsExecAuto({|x, y, z| MATA410(x, y, z)}, aCabec, aItens,4) }, "Estornando Pedido... " +cPedido+ " ",,.F. ) 
						PROCESSA({|| MsExecAuto({|x, y, z| MATA410(x, y, z)}, aCabec, aItens,5) }, "Excluindo Pedido.... " +cPedido+ " ",,.F. ) 
						
						If lMsErroAuto
								MostraErro()
						Else
								GravaLog("Pedido "+cPedido+" exclu�do com sucesso para a OP "+TRB->D4_OP)
						Endif
					EndIf
					aItens := {}
				PED->(DbSkip())	
			EndDo

		While (TRB->(!EOF()) .AND. cFornece != "002878")  
			If Empty(cFornece)
				GravaLog("OP sem fornecedor amarrado. "+TRB->D4_OP)
				TRB->(DbSkip())
				Loop
			EndIf

			IF SELECT("TMP") > 0
				TMP->(DbCloseArea())
			EndIf

		
			DbSELECTArea("SB1")
			DbSeek(xFilial("SB1")+TRB->D4_COD)

			nPrcTab := 0

			If SB1->B1_CUSTD > 0
				nPrcTab := SB1->B1_CUSTD
			Else
				aAdd(aErro,{TRB->D4_OP,TRB->D4_COD,SB1->B1_DESC})
				cOPErro := TRB->D4_OP
				TRB->(DbSkip())
				Loop
			EndIf

			DbSELECTArea("SA2")
			DbSetOrder(1)
			DbSeek(xFilial("SA2")+cFornece+cLoja)

			If cOP <> TRB->D4_OP
				If !Empty(cNroPed)
					RollBackSX8()
				EndIf

				DbSELECTArea("SC5")
				cNroPed := GetSX8Num("SC5","C5_NUM",,1)

				GravaLog("Montando array para a OP Remanejada: "+TRB->D4_OP+" Fornecedor: "+cFornece+"/"+cLoja)

				aCabec := {}
				aadd(aCabec,{"C5_TIPO"   ,"B"            ,Nil})
				aadd(aCabec,{"C5_CLIENTE",cFornece		 ,Nil})
				aadd(aCabec,{"C5_LOJACLI",cLoja			 ,Nil})
				aadd(aCabec,{"C5_LOJAENT",cLoja 		 ,Nil})
				aadd(aCabec,{"C5_POLCOM" ,cPoliti        ,Nil})
				aadd(aCabec,{"C5_TPPED"  ,cTpPedi        ,Nil})
				aadd(aCabec,{"C5_CONDPAG",cCondPg        ,Nil})
				aadd(aCabec,{"C5_NATUREZ",SA2->A2_NATUREZ,Nil})
				aadd(aCabec,{"C5_XNUMOP" ,TRB->D4_OP     ,Nil})
				aadd(aCabec,{"C5_XBLQ"   ,"L"            ,Nil})

				aItens := {}

				cIt := "00"

				cOP := TRB->D4_OP
			EndIf

			cIt := SOMA1(cIt)

			cTES  := "751"																	
			cEstFor:= Posicione("SA2",1,xFilial("SA2")+cFornece+cLoja,"A2_EST")
			
			If cEstFor != "CE"
				cCF	  :="6901"
			Else
				cCF	  :="5901"	
			EndIf

			aLinha := {}
			aadd(aLinha,{"C6_ITEM"   ,cIt          ,Nil})
			aadd(aLinha,{"C6_PRODUTO",SB1->B1_COD  ,Nil})
			aadd(aLinha,{"C6_QTDVEN" ,TRB->D4_QUANT,Nil})
			aadd(aLinha,{"C6_PRCVEN" ,nPrcTab      ,Nil})
			aadd(aLinha,{"C6_PRUNIT" ,nPrcTab      ,Nil})
			aadd(aLinha,{"C6_VALOR"  ,ROUND(TRB->D4_QUANT*nPrcTab,2),Nil})
			aadd(aLinha,{"C6_OPER"   ,cTpOper      ,Nil})
			aadd(aLinha,{"C6_TES"    ,cTES         ,Nil})
			aadd(aLinha,{"C6_CF"    ,cCF   		   ,Nil})
			aadd(aLinha,{"C6_ENTREG" ,dEntreg      ,Nil})
			aadd(aLinha,{"C6_LOCAL"  ,TRB->D4_LOCAL,Nil})
			aadd(aItens,aLinha)

			GravaLog("OP: "+TRB->D4_OP+" Item: "+SB1->B1_COD)

			TRB->(DbSkip())

			If cOP <> TRB->D4_OP
				If cOp <> cOPErro
					PROCESSA({|| MsExecAuto({|x, y, z| MATA410(x, y, z)}, aCabec, aItens,3) }, "Gerando novo pedido de benef...  ",,.F. ) 

					If !lMsErroAuto
						GravaLog("Pedido "+SC5->C5_NUM+" inclu�do com sucesso para a OP "+TRB->D4_OP)
						DbSELECTArea("SC5")
						ConfirmSX8()
						nGerado++
						DbSELECTArea("SC2")
						DbSetOrder(1)
						If DbSeek(xFilial("SC2")+cOP)
							While SC2->(!EOF()) .AND. SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN = cOP
								RecLock("SC2",.F.)
								SC2->C2_XPEDBEN := SC5->C5_NUM
								MsUnlock()
								SC2->(DbSkip())
							EndDo
						EndIf
					Else
						GravaLog("Ocorreu erro na inclusao do pedido "+SC5->C5_NUM+" para a OP "+TRB->D4_OP)
						MostraErro()
						RollBackSX8()
						nErro++
					EndIf
					lMsErroAuto := .F.

					cNroPed := ""
				EndIf
				cOPErro := ""
			EndIf
		EndDo

		If Len(aErro) > 0
			cMsgErro := "As OPs e produtos abaixo n�o tem custo standard cadastrado:"
			For nx := 1 to Len(aErro)
				cMsgErro += "OP: "+aErro[nx][1]+" Prod.:"+aErro[nx][2]+" - "+aErro[nx][3]
			Next
			MsgAlert(cMsgErro,"Aten��o")
		EndIf

		MsgInfo("Pedidos gerados: "+AllTrim(Str(nGerado))+". Pedidos com erro:"+AllTrim(Str(nErro))+"."+CRLF+"Para mais detalhes, verifique o log.","Aviso")
		GravaLog("Pedidos gerados: "+AllTrim(Str(nGerado))+". Pedidos com erro:"+AllTrim(Str(nErro))+".")

	Elseif TRB->(EOF())

		MsgAlert("OP n�o tem a opera��o 20 apontada.","A T E N � � O")
		GravaLog("OP n�o tem a opera��o 20 apontada.")
	EndIf

	TRB->(DbCloseArea())

	GravaLog("Fim do processamento.")

	MemoWrite("\log_benef\PEDBEN_"+cDtTime+".log",cLog)

Return
