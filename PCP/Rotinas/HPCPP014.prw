#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#include 'totvs.ch'

User Function HPCPP014

	Private aCposBrw
	U_FILTRO1()

Return


User Function VISOP(_op)

	DbSelectArea("SC2")
	DbSetOrder(1)
	DbSeek(xfilial("SC2")+_op)

	cErro := ""
	While !EOF() .and. SC2->C2_NUM = _op
		If alltrim(SC2->C2_SEQPAI) = ""
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(Xfilial("SB1")+SC2->C2_PRODUTO)
			cErro += "Prod: "+SC2->C2_PRODUTO+" - "+alltrim(SB1->B1_DESC)+" - Qtd: "+alltrim(str(SC2->C2_QUANT))+ Chr(13) + Chr(10)
		EndIf

		DbSelectarea("SC2")
		DbSkip()
	End

	EECView(cErro)

Return

User Function CHKEST(_op)

	Local   _qry1 := ""
	Local lchope  := .F.
	Private oGet1
	Private cGet1 := Space(9)
	Private oGet2
	Private cGet2 := Space(10)
	Private Plano
	Private Previsao
	Private oGet3
	Private cGet3 := Space(9)
	Private oGet4
	Private cGet4 :=  Space(10)
//	Private _op   := ""	

	Static oDlg

	DbSelectArea("SC2")
	DbSetOrder(1)
	DbSeek(xfilial("SC2")+_op)

	cGrpLib := AllTrim(SuperGetMV("MV_XGRPNLB",.F.,"MP05|MP16|MP17|MP91|MP92|MP30|MP98")) //Grupo de Produtos que n�o devem ser considerados para libera��o de OP
	cTpLib  := AllTrim(SuperGetMV("MV_XTPNLB",.F.,"MI|PI|SV")) // Tipo de Produtos que n�o devem ser considerados na libera��o da OP
	cArmLib := AllTrim(SuperGetMV("MV_XARMLIB",.F.,"A1")) // Armaz�m que deve ser considerado para a Libera��o da OP
	xGrpLib := StrTran(cGrpLib,"|","','")
	xTpLib := StrTran(cTpLib,"|","','")


	If Found()
		If alltrim(SC2->C2_XRES) <> ""
			Alert("OP j� Liberada!")
			Return
		EndIf
	EndIf

	lchope := U_CheckOpe(_op)

	If !lchope
		Return
	Endif

	_qry1 := "SELECT * FROM ( "
	_qry1 += "SELECT C2_NUM,left(C2_PRODUTO,11) as C2_PRODUTO, D4_COD, B1_DESC, SUM(D4_QUANT) AS EMPENHO, (B2_QATU - B2_QACLASS - B2_RESERVA - B2_XRES) AS EMP_RES, B2_QATU, B2_XRES,B2_QACLASS, B2_RESERVA FROM "+RetSqlName("SD4")+" (NOLOCK) SD4 "
	_qry1 += "JOIN "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) ON SB1.D_E_L_E_T_ = '' AND B1_COD = D4_COD "
	_qry1 += "JOIN "+RetSqlName("SC2")+" SC2 WITH(NOLOCK) ON SC2.D_E_L_E_T_ = '' AND C2_NUM = LEFT(D4_OP,6) AND C2_ITEM = '01' AND C2_SEQUEN = '001' AND C2_ITEMGRD = SUBSTRING(D4_OP,12,3) "
	_qry1 += "LEFT JOIN "+RetSqlName("SB2")+" SB2  WITH(NOLOCK) ON SB2.D_E_L_E_T_ = '' AND B2_COD = D4_COD AND B2_LOCAL = B1_LOCPAD AND B2_FILIAL = '"+xfilial("SB2")+"'  AND B2_LOCAL = '"+cArmLib+"' "
	_qry1 += "WHERE SD4.D_E_L_E_T_ = '' AND D4_FILIAL = '"+xFilial("SD4")+"'  "
	_qry1 += "AND LEFT(D4_OP,6) = '"+_op+"' "
	_qry1 += "AND D4_COD NOT LIKE 'B%' AND B1_TIPO NOT IN ('"+xTpLib+"') "
	_qry1 += "AND B1_GRUPO not in ('"+xGrpLib+"') "
	_qry1 += "GROUP BY C2_NUM,left(C2_PRODUTO,11),D4_COD, B1_DESC, B2_QATU, B2_XRES, B2_RESERVA,B2_QACLASS HAVING B2_QATU IS NULL) AS S "

	If Select("TMPSLD") > 0
		TMPSLD->(dbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry1),"TMPSLD",.T.,.T.)
	DbSelectArea("TMPSLD")
	DbGoTop()

	If TMPSLD->(EOF())

		_qry := "SELECT * FROM ( "
		_qry += "SELECT C2_NUM,left(C2_PRODUTO,11) as C2_PRODUTO, D4_COD, B1_DESC, SUM(D4_QUANT) AS EMPENHO, (B2_QATU - B2_QACLASS - B2_RESERVA - B2_XRES) AS EMP_RES, B2_QATU, B2_XRES,B2_QACLASS, B2_RESERVA FROM "+RetSqlName("SD4")+" (NOLOCK) SD4 "
		_qry += "JOIN "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) ON SB1.D_E_L_E_T_ = '' AND B1_COD = D4_COD "

		_qry += "JOIN "+RetSqlName("SC2")+" SC2 WITH(NOLOCK) ON SC2.D_E_L_E_T_ = '' AND C2_NUM = LEFT(D4_OP,6) AND C2_ITEM = '01' AND C2_SEQUEN = '001' AND C2_ITEMGRD = SUBSTRING(D4_OP,12,3) "
		_qry += "LEFT JOIN "+RetSqlName("SB2")+" SB2  WITH(NOLOCK) ON SB2.D_E_L_E_T_ = '' AND B2_COD = D4_COD AND B2_LOCAL = B1_LOCPAD AND B2_FILIAL = '"+xfilial("SB2")+"'  AND B2_LOCAL = '"+cArmLib+"' "
		_qry += "WHERE SD4.D_E_L_E_T_ = '' AND D4_FILIAL = '"+xFilial("SD4")+"'  "
		_qry += "AND LEFT(D4_OP,6) = '"+_op+"' "
		_qry += "AND D4_COD NOT LIKE 'B%' AND B1_TIPO NOT IN ('"+xTpLib+"') "
		_qry += "AND B1_GRUPO not in ('"+xGrpLib+"') "
		_qry += "GROUP BY C2_NUM,left(C2_PRODUTO,11),D4_COD, B1_DESC, B2_QATU, B2_XRES, B2_RESERVA,B2_QACLASS) AS M "
		_qry += "WHERE (EMPENHO > (B2_QATU - B2_QACLASS - B2_RESERVA - B2_XRES) AND B2_XRES >= 0) OR "
		_QRY += "(EMPENHO > (B2_QATU - B2_QACLASS - B2_RESERVA) AND B2_XRES < 0) "

		If Select("TMPSD4") > 0
			TMPSD4->(dbCloseArea())
		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSD4",.T.,.T.)
		DbSelectArea("TMPSD4")
		DbGoTop()


		If TMPSD4->(!EOF())
			cErro := ""
			While TMPSD4->(!EOF())
				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+TMPSD4->D4_COD)

				cErro += " Produto com estoque insuficiente "+TMPSD4->D4_COD+" - "+alltrim(SB1->B1_DESC)+"!"+ Chr(13) + Chr(10)
				cErro += " Do produto "+TMPSD4->C2_PRODUTO+" - Quantidade solicitada: "+alltrim(str(TMPSD4->EMPENHO)) + Chr(13) + Chr(10)
				cErro += " Quantidade em Estoque: "+alltrim(str(TMPSD4->(B2_QATU - B2_QACLASS - B2_RESERVA - B2_XRES))) + Chr(13) + Chr(10) + Chr(13) + Chr(10)
				DbSelectArea("TMPSD4")
				DbSkip()
			End
			EECView(cErro)
			If Select("TMPSD4") > 0
				TMPSD4->(dbCloseArea())
			EndIf
		Else
			If MsgYesNo("Estoque suficiente. Deseja liberar a OP?","Libera��o de OP")
				If Select("TMPSD4") > 0
					TMPSD4->(dbCloseArea())
				EndIf

				DEFINE MSDIALOG oDlg TITLE "Planos de Produ��o" FROM 000, 000  TO 230, 250 COLORS 0, 16777215 PIXEL

				@ 021, 008 SAY Plano PROMPT "Plano" SIZE 017, 007 OF oDlg COLORS 0, 16777215 PIXEL
				@ 020, 034 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 VALID Gatilhos(1) F3 "Plano" PIXEL
				@ 036, 008 SAY Data PROMPT "Data" SIZE 016, 007 OF oDlg COLORS 0, 16777215 PIXEL
				@ 035, 034 MSGET oGet2 VAR cGet2 SIZE 060, 010 OF oDlg COLORS 0, 16777215 VALID Gatilhos(2) PIXEL
				@ 053, 008 SAY Previsao PROMPT "Previsao" SIZE 023, 007 OF oDlg COLORS 0, 16777215 PIXEL
				@ 048, 034 MSGET oGet3 VAR cGet3 SIZE 060, 010 OF oDlg COLORS 0, 16777215 VALID Gatilhos(3) F3 "SC4"  PIXEL
				@ 064, 008 SAY Data1 PROMPT "Data" SIZE 020, 007 OF oDlg COLORS 0, 16777215 PIXEL
				@ 063, 034 MSGET oGet4 VAR cGet4 SIZE 060, 010 OF oDlg COLORS 0, 16777215 VALID Gatilhos(4) PIXEL
				@ 088, 015 BUTTON oButton1 PROMPT "Confirmar" SIZE 037, 012 ACTION (u_SalvaPla(cGet1,cGet2,cGet3,cGet4,_op),oDlg:End()) OF oDlg PIXEL
				@ 088, 070 BUTTON oButton2 PROMPT "Cancelar" SIZE 037, 012 ACTION Close(oDlg) OF oDlg PIXEL

				ACTIVATE MSDIALOG oDlg CENTERED


				//_qry := "update "+RetSqlName("SC2")+" set C2_XRES = 'L' ,C2_XDOCPLA = '"+cGet1+"', C2_XDTLANC = '"+cGet2+"' , C2_XDOCPRE = '"+cGet3+"' , C2_XDTPREV = '"+cGet4+"'    where C2_NUM = '"+_op+"' "
				//TcSqlExec(_qry)

				/*_qry := "update "+RetSqlName("SB2")+" set B2_XRES = B2_XRES + "
				_qry += "(Select Sum(D4_QUANT) from "+RetSqlName("SD4")+" SD4 WITH(NOLOCK) where D4_XRES = 0 and SD4.D_E_L_E_T_ = '' and "
				_qry += "B2_COD = D4_COD and D4_FILIAL = '"+xfilial("SD4")+"' and left(D4_OP,6) = '"+_op+"') "
				_qry += "from "+RetSqlName("SB2")+" SB2 WITH(NOLOCK) "
				_qry += "inner join "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) on SB1.D_E_L_E_T_ = '' and B1_COD = B2_COD "
				_qry += "where SB2.D_E_L_E_T_ = '' and B2_FILIAL = '"+xfilial("SB2")+"' and "
				_qry += "B2_COD in (Select D4_COD from "+RetSqlName("SD4")+" SD4 where D4_FILIAL = '"+xfilial("SD4")+"' and SD4.D_E_L_E_T_ = '' and "
				_qry += "left(D4_OP,6) = '"+_op+"' group by D4_COD) and B2_LOCAL = '"+cArmLib+"' and "
				_qry += "B1_TIPO NOT IN ('"+xTpLib+"') " 
				_qry += "AND B1_GRUPO not in ('"+xGrpLib+"') "
				_qry += "and (Select Sum(D4_QUANT) from "+RetSqlName("SD4")+" SD4 WITH(NOLOCK) where D4_FILIAL = '"+xfilial("SD4")+"' and SD4.D_E_L_E_T_ = '' and "
				_qry += "B2_COD = D4_COD and D4_XRES = 0 and left(D4_OP,6) = '"+_op+"') is not null " 

				TcSqlExec(_qry)

				_qry := "update "+RetSqlName("SD4")+" set D4_XDTLIB = '"+dtos(ddatabase)+"', D4_XRES = D4_QTDEORI, "
				_qry += "D4_XOPP = left(D4_OP,6), D4_XTIPO = (Select B1_TIPO from "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) where SB1.D_E_L_E_T_ = '' and B1_COD = D4_COD) "
				_qry += "where D4_FILIAL = '"+xfilial("SD4")+"' and D_E_L_E_T_ = '' and "
				_qry += "left(D4_OP,6) = '"+_op+"' and D4_XDTLIB = '' "
				TcSqlExec(_qry)

				_qry := "update "+RetSqlName("ZAM")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where ZAM_OP = '"+_op+"' "
				TcSqlExec(_qry)


				_qry := " UPDATE "+RetSqlName("SC2")+" SET C2_XDTRANS = '"+dtos(ddatabase)+"' WHERE C2_NUM = '"+_op+"' AND C2_FILIAL = '"+xfilial("SC2")+"' AND D_E_L_E_T_ = '' "
				TcSqlExec(_qry)
				*/

				//MsgInfo("Ordem de Produ��o liberada!","Aviso")
			EndIf
		EndIf
	Else
		cErro := ""
		While TMPSLD->(!EOF())
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+TMPSLD->D4_COD)

			cErro += " Produto com estoque insuficiente "+TMPSLD->D4_COD+" - "+alltrim(SB1->B1_DESC)+"!"+ Chr(13) + Chr(10)
			cErro += " Do produto "+TMPSLD->C2_PRODUTO+" - Quantidade solicitada: "+alltrim(str(TMPSLD->EMPENHO)) + Chr(13) + Chr(10)
			cErro += " Quantidade em Estoque: "+alltrim(str(TMPSLD->(B2_QATU - B2_QACLASS - B2_RESERVA - B2_XRES))) + Chr(13) + Chr(10) + Chr(13) + Chr(10)
			DbSelectArea("TMPSLD")
			DbSkip()
		End
		EECView(cErro)
		If Select("TMPSLD") > 0
			TMPSLD->(dbCloseArea())
		EndIf
	EndIf

Return

Static Function MONTATRAB(ddata1,ddata2,cOp)

	_QRY := "DELETE "+RetSqlName("ZAM")
	TcSqlExec(_QRY)

	_qry := "Select C2_NUM, Left(C2_PRODUTO,8) as PRODUTO, B4_DESC, Substring(C2_PRODUTO,9,3) as COR, BV_DESCRI from "+RetSqlName("SC2")+" SC2 WITH(NOLOCK) "
	_qry += "left join "+RetSqlName("SB4")+" SB4 WITH(NOLOCK) on SB4.D_E_L_E_T_ = '' and B4_COD = left(C2_PRODUTO,8) "
	_qry += "left join "+RetSqlName("SBV")+" SBV WITH(NOLOCK) on SBV.D_E_L_E_T_ = '' and BV_TABELA = B4_LINHA AND BV_CHAVE = SUBSTRING(C2_PRODUTO,9,3) "
	_qry += "where SC2.D_E_L_E_T_ = '' and C2_DATRF = '' and C2_QUJE < C2_QUANT  and C2_XRES = '' and C2_SEQPAI = '' AND C2_TPOP = 'F' "

	If !Empty(ddata1)
		_qry += " AND  C2_EMISSAO BETWEEN '"+ DTOS(ddata1)+"' AND '"+ DTOS(ddata2)+"' "
	EndIf

	If !Empty(cOp)
		_qry += " AND  C2_NUM = '"+ cOp+"' "
	EndIf

	_qry += "group by C2_NUM, Left(C2_PRODUTO,8) , B4_DESC, Substring(C2_PRODUTO,9,3), BV_DESCRI "
	_qry += "order by C2_NUM "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSC2",.T.,.T.)

	dbSelectArea("TMPSC2")
	aCposBrw := {}
	aEstrut  := {}
	If !EOF()

		While !TMPSC2->(Eof())

			RecLock("ZAM", .T.)
			ZAM->ZAM_OP      := TMPSC2->C2_NUM
			ZAM->ZAM_PRODUT  := TMPSC2->PRODUTO
			ZAM->ZAM_DESC    := TMPSC2->B4_DESC
			ZAM->ZAM_COR	 := TMPSC2->COR
			ZAM->ZAM_DESCCO	 := TMPSC2->BV_DESCRI
			ZAM->(msUnlock())

			TMPSC2->(DbSkip())
		Enddo

	EndIf

	If Select("TMPSC2") > 0
		TMPSC2->(dbCloseArea())
	EndIf

Return

USER FUNCTION FILTRO1()

	Local lOk	:= .F.
	Local aParam := {}
	Local aRetParm	:= {}
	Local ddata1
	Local ddata2

	aAdd(aParam,{1,"Data de "  ,Ctod(Space(8)),"","","","",50,.F.})
	aAdd(aParam,{1,"Data at� " ,Ctod(Space(8)),"","","","",50,.F.})
	If __cUserID $ GetMv("HP_USERSTI")
		aAdd(aParam,{1,"OP " ,Space(6),"","","","",50,.F.})
	EndIf

	If ParamBox(aParam,"Ops Pendentes",@aRetParm,{||.T.},,,,,,"U_HPCPP014",.T.,.T.)
		lOk	:= .T.
		ddata1 := aRetParm[1]
		ddata2 := aRetParm[2]
		If __cUserID $ GetMv("HP_USERSTI") .AND. !Empty(Alltrim(aRetParm[3]))
			cOp		:= Alltrim(aRetParm[3])
		Else
			cOp		:= ""
		EndIf
	Else
		lOk := .F.
	EndIf

	If lOk == .T.
		MsgRun("Selecionando Registros, Aguarde...",,{|| MONTATRAB(ddata1,ddata2,cOp)})

		cCadastro := "Libera��o de Ordem de Produ��o"
		aRotina := {;
			{ "Pesquisar" 	, "AxPesqui"					, 0, 1},;
			{ "Checar"		, "u_CHKEST(ZAM->ZAM_OP)"		, 0, 2},;
			{ "Visualizar"	, "u_VISOP(ZAM->ZAM_OP)"		, 0, 2},;
			{ "Estorna"     ,  "u_ESTOP()"                  , 0, 2}}
		//{ "Imprime OP" 	, "u_HRPCP001(ZAM->ZAM_OP)"		, 0, 3},;
			//{ "Via do Tecido" ,"U_HPREL041(ZAM->ZAM_OP)"	, 0, 3},;
			//{ "Via do Aviamento" ,"U_HPREL042(ZAM->ZAM_OP)"	, 0, 3}}

		mBrowse( 6, 1,22,75,"ZAM",,,,,,)

	Else
		MsgAlert("Cancelado pelo usuario","HOPE")
		Return .F.
	EndIf

Return
/*
User Function ESTOP()

	Pergunte("HPCP014",.T.)

	cTpLib  := AllTrim(SuperGetMV("MV_XTPNLB",.F.,"MI|PI")) // Tipo de Produtos que n�o devem ser considerados na libera��o da OP
	cGrpLib := AllTrim(SuperGetMV("MV_XGRPNLB",.F.,"MP05|MP16|MP17|MP91")) //Grupo de Produtos que n�o devem ser considerados para libera��o de OP
	_op := MV_PAR01

	DbSelectArea("SC2")
	DbSetOrder(1)
	DbSeek(xfilial("SC2")+_op)

	If SC2->C2_XRES == "T"
		If !MsgYesNo("Esta OP j� foi Transferida. A Tranfer�ncia j� foi estornada?","OP j� transferida")
			Return()
		EndIf
	EndIf

	DbSelectArea("SD4")
	DbSetOrder(2)
	DbSeek(xfilial("SD4")+_op)
	_ok := .T.
	_cLocal := "A1"

	While !EOF() .and. SubStr(SD4->D4_OP,1,6) == SubStr(_op,1,6)
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SD4->D4_COD)

		If !(SB1->B1_TIPO $ (cTpLib)) .and. SubStr(SD4->D4_COD,1,1) <> "B" .AND. !(SB1->B1_GRUPO $ (cGrpLib))
			DbSelectArea("SB2")
			DbSetOrder(1)
			DbSeek(xfilial("SB2")+SD4->D4_COD+_cLocal)

			If Found()
				RecLock("SB2",.F.)
				Replace B2_XRES	with B2_XRES - SD4->D4_XRES
				MsUnLock()

				RecLock("SD4",.F.)
				Replace D4_XRES with 0
				MsUnLock()
			ElseIf Empty(SD4->D4_XDTLIB) .AND. SD4->D4_XRES == 0
				_ok := .T.
			Else
				_ok := .F.
			EndIf
		EndIf

		DbSelectArea("SD4")
		DbSkip()
	End

	If _ok
		_qry := "update "+RetSqlName("SC2")+" set C2_XRES = '' where D_E_L_E_T_ = '' and C2_NUM = '"+SubStr(_op,1,6)+"' "
		TcSqlExec(_qry)

		_qry1 := " update " +RetSqlName("SD4")+ " set D4_XDTLIB = '' where D_E_L_E_T_ = '' and LEFT(D4_OP,6) = '"+SubStr(_op,1,6)+"' "
		TcSqlExec(_qry1)

		MsgBox("Estorno realizado com sucesso!","Aviso","INFO")
	Else
		Alert("N�o foi poss�vel realizar o estorno!")
	EndIf
Return
*/
Static Function Gatilhos(_tp)
	Local _qry

	If _tp = 1
		If !Empty(cGet3)
			oGet3:SetFocus()
		Else
			If Select("TMP") > 0
				TMP->(dbclosearea())
			EndIf
			_qry := " SELECT HC_DOC  FROM "+RetSqlName("SHC")+"  WHERE HC_DOC = '"+cGet1+"' AND D_E_L_E_T_ = '' GROUP BY HC_DOC "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMP",.T.,.T.)

			cGet1 := TMP->HC_DOC

			If Empty(TMP->HC_DOC)
				MsgAlert("Plano inexistente","Hope")
				//oGet1:SetFocus()
			EndIf
		EndIf

	elseif _tp = 2
		If !Empty(cGet3)
			oGet3:SetFocus()
		Else
			If Select("TMP") > 0
				TMP->(dbclosearea())
			EndIf

			_qry := " SELECT HC_DATA  FROM "+RetSqlName("SHC")+"  WHERE HC_DATA = '"+cGet2+"' AND  HC_DOC = '"+cGet1+"' AND D_E_L_E_T_ = '' GROUP BY HC_DATA "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMP",.T.,.T.)

			cGet2		:= TMP->HC_DATA

			If Empty(TMP->HC_DATA)
				MsgAlert("Data inexistente ao Plano","Hope")
				//oGet2:SetFocus()
			EndIf
		EndIf

	elseif _tp = 3

		If !Empty(cGet1)
			oGet1:SetFocus()
		Else

			If Select("TMP") > 0
				TMP->(dbclosearea())
			EndIf

			_qry := " SELECT C4_DOC FROM "+RetSqlName("SC4")+"  WHERE C4_DOC = '"+cGet3+"' AND D_E_L_E_T_ = '' GROUP BY C4_DOC "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMP",.T.,.T.)

			cGet3		:= TMP->C4_DOC

			If Empty(TMP->C4_DOC)
				MsgAlert("Previs�o inexistente","Hope")
				//oGet3:SetFocus()
			EndIf
		EndIf

	elseif _tp = 4
		If !Empty(cGet1)
			oGet1:SetFocus()
		Else
			If Select("TMP") > 0
				TMP->(dbclosearea())
			EndIf

			_qry := " SELECT C4_DATA FROM "+RetSqlName("SC4")+"  WHERE C4_DOC = '"+cGet3+"' AND C4_DATA = '"+cGet4+"' AND D_E_L_E_T_ = '' GROUP BY C4_DATA "

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMP",.T.,.T.)

			cGet4		:= TMP->C4_DATA

			If Empty(TMP->C4_DATA)
				MsgAlert("Data inexistente na Previs�o","Hope")
				//oGet4:SetFocus()
			EndIf
		EndIf
	EndIf
Return

User Function SalvaPla(_par01,_par02,_par03,_par04,_op)

	_qry := "update "+RetSqlName("SC2")+" set C2_XRES = 'L' ,C2_XDOCPLA = '"+Alltrim(_par01)+"', C2_XDTLANC = '"+Alltrim(_par02)+"' , C2_XDOCPRE = '"+Alltrim(_par03)+"' , C2_XDTPREV = '"+Alltrim(_par04)+"'    where C2_NUM = '"+_op+"' "
	TcSqlExec(_qry)

	// UPDATE RESERVA SB2 ARMAZ�M A1
	_qry := "update "+RetSqlName("SB2")+" set B2_XRES = B2_XRES + "
	_qry += "(Select Sum(D4_QUANT) from "+RetSqlName("SD4")+" SD4 WITH(NOLOCK) where D4_XRES = 0 and SD4.D_E_L_E_T_ = '' and "
	_qry += "B2_COD = D4_COD and D4_FILIAL = '"+xfilial("SD4")+"' and left(D4_OP,6) = '"+_op+"') "
	_qry += "from "+RetSqlName("SB2")+" SB2 WITH(NOLOCK) "
	_qry += "inner join "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) on SB1.D_E_L_E_T_ = '' and B1_COD = B2_COD "
	_qry += "where SB2.D_E_L_E_T_ = '' and B2_FILIAL = '"+xfilial("SB2")+"' and "
	_qry += "B2_COD in (Select D4_COD from "+RetSqlName("SD4")+" SD4 where D4_FILIAL = '"+xfilial("SD4")+"' and SD4.D_E_L_E_T_ = '' and "
	_qry += "left(D4_OP,6) = '"+_op+"' group by D4_COD) and B2_LOCAL = '"+cArmLib+"' and "
	_qry += "B1_TIPO NOT IN ('"+xTpLib+"') "
	_qry += "AND B1_GRUPO not in ('"+xGrpLib+"') "
	_qry += "and (Select Sum(D4_QUANT) from "+RetSqlName("SD4")+" SD4 WITH(NOLOCK) where D4_FILIAL = '"+xfilial("SD4")+"' and SD4.D_E_L_E_T_ = '' and "
	_qry += "B2_COD = D4_COD and D4_XRES = 0 and left(D4_OP,6) = '"+_op+"') is not null "
	TcSqlExec(_qry)

	// UPDATE DT LIBERA��O SD4
	_qry := "update "+RetSqlName("SD4")+" set D4_XDTLIB = '"+dtos(ddatabase)+"', "
	_qry += "D4_XOPP = left(D4_OP,6), D4_XTIPO = (Select B1_TIPO from "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) where SB1.D_E_L_E_T_ = '' and B1_COD = D4_COD) "
	_qry += "where D4_FILIAL = '"+xfilial("SD4")+"' and D_E_L_E_T_ = '' and "
	_qry += "left(D4_OP,6) = '"+_op+"' and D4_XDTLIB = '' "
	TcSqlExec(_qry)

	// UPDATE RESERVA SD4 (CONSIDERANDO MESMO CRIT�RIO SB2) PEDIDO PELA CAMILA NO DIA 10/09/2020

	_qry:= " UPDATE "+RetSqlName("SD4")+" SET D4_XRES = D4_QTDEORI 				 		  "
	_qry+= " FROM "+RetSqlName("SD4")+"  D4 (NOLOCK) 							 		  "
	_qry+= " INNER JOIN "+RetSqlName("SB1")+"  B1 (NOLOCK) ON 							  "
	_qry+= " D4.D4_COD = B1.B1_COD WHERE LEFT(D4_OP, 6) = '"+_op+"'						  "
	_qry+= " AND B1.D_E_L_E_T_ = '' AND D4.D_E_L_E_T_ = ''						 		  "
	_qry+= " AND B1.B1_TIPO NOT IN ('"+xTpLib+"') AND B1.B1_GRUPO NOT IN ('"+xGrpLib+"')  "
	TcSqlExec(_qry)

	_qry := "update "+RetSqlName("ZAM")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where ZAM_OP = '"+_op+"' "
	TcSqlExec(_qry)


	_qry := " UPDATE "+RetSqlName("SC2")+" SET C2_XDTRANS = '"+dtos(ddatabase)+"' WHERE C2_NUM = '"+_op+"' AND C2_FILIAL = '"+xfilial("SC2")+"' AND D_E_L_E_T_ = '' "
	TcSqlExec(_qry)

	MsgInfo("Ordem de Produ��o liberada!","Aviso")
Return

/*/{Protheus.doc} CheckOpe
	(Fun��o para verificar se os produtos das ops possuem opera�oes cadastradas.n)
	@type  User Function
	@author Geyson Albano
	@since 16/06/2020
/*/
User Function CheckOpe(_op)

	Local cQuery := ""
	Local cRefOp := ""
	Local cErro  := ""
	Local lOk 	 := .T.

	If Select("TMP") > 0
		TMP->(dbclosearea())
	EndIf

	cQuery := " SELECT LEFT(C2_PRODUTO,8) AS C2_PRODUTO FROM "+RetSqlName("SC2")+" C2 (NOLOCK) WHERE C2_NUM = '"+_op+"' AND C2_SEQUEN = '001' "
	cQuery += " AND D_E_L_E_T_ = '' GROUP BY LEFT(C2_PRODUTO,8) "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)

	cRefOp := Alltrim(TMP->C2_PRODUTO)

	DbSelectArea("SG2")
	DbSetOrder(7)

	If !DbSeek(xfilial("SG2")+PadR(cRefOp,26)+"01")
		cErro := ""
		While TMP->(!EOF())
			cErro += " Refer�ncia "+cRefOp+ " n�o possui opera��es cadastradas !" +Chr(13) + Chr(10)
			lOk := .F.
			DbSelectArea("TMP")
			DbSkip()
		EndDo
		EECView(cErro)
		If Select("TMP") > 0
			TMP->(dbCloseArea())
		EndIf
	EndIf
Return lOk
