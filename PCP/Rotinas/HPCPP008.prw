#INCLUDE "RWMAKE.CH" 
#INCLUDE "TBICONN.CH" 

User Function HPCPP008() 

cQuery  := "SELECT * FROM SC2010 WHERE D_E_L_E_T_ = '*' and C2_FILIAL = '0101' order by R_E_C_N_O_ desc "

cQuery := ChangeQuery(cQuery)
If Select("TMPSC2") > 0 
   	 TMPSC2->(DbCloseArea()) 
EndIf 
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSC2",.T.,.T.)

DbSelectArea("TMPSC2")
DbGoTop()
_num := 1010
WHILE !EOF()
	
						aMata650:= {{'C2_FILIAL'	,TMPSC2->C2_FILIAL			,NIL},;
									{'C2_NUM'		,STRZERO(_NUM,6)				,NIL},;
								  	{'C2_ITEM'     	,"01" 					,NIL},;
									{'C2_SEQUEN'   	,TMPSC2->C2_SEQUEN					,NIL},;
									{'C2_PRODUTO'  	,TMPSC2->C2_PRODUTO			,NIL},;
									{'C2_LOCAL'    	,TMPSC2->C2_LOCAL			,NIL},;
									{'C2_QUANT'    	,TMPSC2->C2_QUANT				,NIL},;
									{'C2_UM'       	,TMPSC2->C2_UM 			,NIL},;
									{'C2_DATPRI'   	,stod(TMPSC2->C2_DATPRI)				,NIL},;
									{'C2_DATPRF'   	,ddatabase				,NIL},;
									{'C2_OBS'		,TMPSC2->C2_NUM+TMPSC2->C2_ITEMGRD,NIL},;
									{'C2_TPOP'     	,"F" 					,NIL},;
									{'C2_GRADE'		,TMPSC2->C2_GRADE					,NIL},;
									{'C2_ITEMGRD'	,TMPSC2->C2_ITEMGRD ,NIL},;
									{'C2_EMISSAO'  	,stod(TMPSC2->C2_DATPRI)				,NIL},;
									{'AUTEXPLODE'  	,'S'					,NIL},;
									{'INDEX'  		,1						,NIL}}
						//-- Chamada da rotina automatica
						lMsErroAuto := .f.
						msExecAuto({|x,Y| Mata650(x,Y)},aMata650,3)
			
						If lMsErroAuto
							Mostraerro()
						EndIf
						_num++
						DBSELECTAREA("TMPSC2")
						DBSKIP()
END

DBCLOSEAREA()

Return