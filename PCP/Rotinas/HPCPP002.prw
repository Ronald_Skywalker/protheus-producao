#INCLUDE "RWMAKE.CH" 
#INCLUDE "TBICONN.CH" 
#Define CRLF  CHR(13)+CHR(10)


User Function HPCPP002() 
Local _z
private _qrya
private _mrp 
private _OPG := {}
private _op1 := {}
private _mrp := ""      
Private _z
Private _par01
Private cNumOP
Private cNwOP
Private _Alert := ""
Private cOPPar
_aberto := .T.

IF MsgYesNo("Aglutina Ordens de Produ��o? ","Aglutina��o de Ordens de Produ��o")

	If MsgYesNo("Deseja considerar OPs do MRP?","Confirma��o")

		While _aberto
			Pergunte("HPCP2D",.T.)
			If Alltrim(MV_PAR05) = "" .or. alltrim(MV_PAR06) = "" .or. SubStr(Alltrim(MV_PAR05),1,1) = "Z" .or. SubStr(Alltrim(MV_PAR06),1,1) = "Z"
				Alert("Par�metros muito amplos para aglutina��o! Escolha um produto por vez!")
				_aberto := .T.
			Else
				_aberto := .F.
			Endif
		End

/*
		MV_PAR01 := ctod("01/02/19")
		MV_PAR02 := ctod("01/02/19")
		MV_PAR03 := ""
		MV_PAR04 := "ZZZZZ"
		MV_PAR05 := "00023840AVL"
		MV_PAR06 := "00023840AVM"
*/		
		_qrya := "Select 'OP' as TP, C2_FILIAL, C2_SEQMRP, C2_NUM, C2_ITEM, C2_SEQUEN, C2_ITEMGRD, C2_TPOP, C2_SEQPAI, C2_QUJE, C2_GRADE, C2_PRODUTO, C2_QUANT, C2_LOCAL, C2_DATPRI, C2_DATPRF, C2_EMISSAO "
		_QRYa += "from "+RetSqlName("SC2")+" SC2 "
		_QRYa += "where SC2.D_E_L_E_T_ = '' and C2_QUJE = 0 and C2_GRADE <> 'S' and C2_SEQPAI = '' AND C2_XRES = '' and C2_TPOP = 'F' AND C2_SEQMRP <> ''  
		_QRYa += "and C2_EMISSAO >= '"+dtos(MV_PAR01)+"' and C2_EMISSAO <= '"+dtos(mV_PAR02)+"' and left(C2_PRODUTO,11) >= '"+Alltrim(MV_PAR05)+"' "
		_QRYa += "and left(C2_PRODUTO,11) <= '"+alltrim(MV_PAR06)+"' and C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD >= '"+MV_PAR03+"' "
		_QRYa += "and C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD <= '"+MV_PAR04+"' "

	Else
		While _aberto
			Pergunte("MTA740",.T.)
			If Alltrim(MV_PAR01) = "" .or. alltrim(MV_PAR02) = "" .or. SubStr(Alltrim(MV_PAR01),1,1) = "Z" .or. SubStr(Alltrim(MV_PAR02),1,1) = "Z"
				Alert("Par�metros muito amplos para aglutina��o! Escolha um produto por vez!")
				_aberto := .T.
			Else
				_aberto := .F.
			Endif
		End

		_qrya := "Select 'OP' as TP, C2_FILIAL, ltrim(rtrim(C2_OBS)) as C2_SEQMRP, C2_NUM, C2_ITEM, C2_SEQUEN, C2_ITEMGRD, C2_TPOP, C2_SEQPAI, C2_QUJE, C2_GRADE, C2_PRODUTO, C2_QUANT, C2_LOCAL, C2_DATPRI, C2_DATPRF, C2_EMISSAO "
		_QRYa += "from "+RetSqlName("SC2")+" SC2 "
		_QRYa += "where SC2.D_E_L_E_T_ = '' and C2_QUJE = 0 and C2_GRADE <> 'S' and C2_SEQPAI = '' AND C2_XRES = '' and C2_TPOP = 'F' "
		_qrya += "and C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD >= '"+MV_PAR01+"' AND C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD <= '"+MV_PAR02+"' " 
	
	Endif
	Processa({|| BkpOP()},"Backup de Ordens de Produ��o...")

	aVetor := {}
	_i := 0
	_w := 0                           
	dData:=dDataBase
	_lotem := 0

	ASORT(_opg, , , { |x,y| x[4] < y[4] } )

	_i := 1
	_len := len(_opg)

	While _i <= _len
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+_opg[_i,1])
			
		IF _opg[_i,2] >= SB1->B1_XQTMIN
			_i++
		Else
			ADel( _opg, _i )
			ASize( _opg, len(_opg)-1 )
			_len := _len - 1
		Endif
	End

	_i := 1
	_len := len(_op1)

	Processa({|| ExcOP()},"Exclus�o de Ordens de Produ��o Original...")

	For _z := 1 to 3
		Processa({|| IncOP()},"Incluindo de Ordens de Produ��o ("+str(_z)+")...")
	Next

	Processa({|| ConfOP()},"Confer�ncia de Aglutina��o...")
	
Endif
             
Return

/*
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )
  
PutSx1(cPerg,"01","Da OP ?             ","Da OP ?             ","Da OP ?             ","Mv_ch1",TAMSX3("H6_OP" 	   )[3],TAMSX3("H6_OP"     )[1],TAMSX3("H6_OP"     )[2],0,"G","","SC2","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o n�mero da Ordem de Produ��o.  ",""},{""},{""},"")
PutSx1(cPerg,"02","At� OP ?            ","At� OP ?            ","At� OP ?            ","Mv_ch2",TAMSX3("H6_OP"     )[3],TAMSX3("H6_OP"     )[1],TAMSX3("H6_OP"     )[2],0,"G","","SC2","","N","Mv_par02","","","","","","","","","","","","","","","","",{"Informe o n�mero da Ordem de Produ��o.  ",""},{""},{""},"")
PutSx1(cPerg,"03","Tamanho Lote ?      ","At� OP ?            ","At� OP ?            ","Mv_ch3",TAMSX3("H6_QTDPROD")[3],TAMSX3("H6_QTDPROD")[1],TAMSX3("H6_QTDPROD")[2],0,"G","",""   ,"","N","Mv_par03","","","","","","","","","","","","","","","","",{"Informe o tamanho do Lote de Produ��o.  ",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)
*/

Static Function BkpOP()

	TcSqlExec("Update "+RetSqlName("ZZB")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D_E_L_E_T_ = '' ")

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_QRYa),"TMPSC2",.T.,.T.)
    
	DbSelectArea("TMPSC2")
	DbGoTop()
	TMPSC2->(ProcRegua(RecCount()))
	cOPPar := TMPSC2->C2_NUM
	_PAR01 := TMPSC2->C2_NUM

	
	While !EOF() //.and. C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD >= MV' .and. C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD <= MV_PAR02
	  	IncProc("Realizando Backup das OPs...")

		If alltrim(TMPSC2->C2_SEQMRP) <> ""
			_mrp := TMPSC2->C2_SEQMRP
		Endif	

		If TMPSC2->C2_TPOP == "F" .and. alltrim(TMPSC2->C2_SEQPAI) = '' .and. TMPSC2->C2_QUJE == 0 .and. TMPSC2->C2_GRADE <> 'S'
//			If TMPSC2->TP = "OP"
//				DbSelectArea("SC2")
//				DbSetOrder(1)
//				DbGoTop()
//				DbSeek(xfilial("SC2")+TMPSC2->C2_NUM+TMPSC2->C2_ITEM+TMPSC2->C2_SEQUEN+TMPSC2->C2_ITEMGRD)
				
//				If Found()
					Reclock("ZZB",.T.)
						Replace ZZB_FILIAL		with xfilial("ZZB")
						Replace ZZB_NUM			with TMPSC2->C2_NUM
						Replace ZZB_ITEM		WITH TMPSC2->C2_ITEM
						Replace ZZB_SEQUEN		WITH TMPSC2->C2_SEQUEN
						Replace ZZB_PRODUT		WITH TMPSC2->C2_PRODUTO
						Replace ZZB_LOCAL		WITH TMPSC2->C2_LOCAL
						Replace ZZB_QUANT		WITH TMPSC2->C2_QUANT
						Replace ZZB_DATPRI		WITH STOD(TMPSC2->C2_DATPRI)
						Replace ZZB_DATPRF		WITH STOD(TMPSC2->C2_DATPRF)
						Replace ZZB_OBS			WITH TMPSC2->C2_SEQMRP
						Replace ZZB_EMISSA		WITH STOD(TMPSC2->C2_EMISSAO)
					MsUnLock()
//				Endif
//			Endif
			
			DbSelectArea("SZH")
			DbSetOrder(1)
			DbSeek(xfilial("SZH")+SubStr(TMPSC2->C2_PRODUTO,1,8))

			If Found()
			
				DbSelectArea("SB4")
				DbSetOrder(1)
				DbSeek(xfilial("SB4")+SubStr(TMPSC2->C2_PRODUTO,1,8))
				
			  	cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " WHERE "
	  			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +SB4->B4_COLUNA+"' AND D_E_L_E_T_ = '' " 
	  			cQuery += "ORDER BY R_E_C_N_O_"
	  		
	  			If Select("TMPSBV") > 0 
     	 			TMPSBV->(DbCloseArea()) 
	  			EndIf 
	  			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
				
				DbSelectArea("TMPSBV")
				DbGoTop()
				
				_it := 0
				_tam := 0
				
				While !EOF() 
					_it++
					
					If SubStr(TMPSC2->C2_PRODUTO,12,4) == alltrim(TMPSBV->BV_CHAVE)
						_tam := _it
					endif
					DbSelectArea("TMPSBV")
					DbSkip()
				End
				
				DbCloseArea()
				DbSelectArea("TMPSC2")
				
				If &("SZH->ZH_TAM"+strzero(_tam,3)) > 0
					_achou := ASCAN( _opg, {|x| Alltrim(x[1]) == AllTrim(TMPSC2->C2_PRODUTO)})
					Aadd(_op1,{C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD}) 
					If _achou == 0
	    				Aadd(_opg, {TMPSC2->C2_PRODUTO,TMPSC2->C2_QUANT,C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD,SubStr(TMPSC2->C2_PRODUTO,1,11)})
    				Else
	    				_opg[_achou,2] += TMPSC2->C2_QUANT
    				Endif
    			Else
	    			_Alert += "Produto "+SubStr(TMPSC2->C2_PRODUTO,1,8)+" n�o tem Mix cadastrado para o tamanho "+SubStr(TMPSC2->C2_PRODUTO,12,4) + CRLF
    			Endif
    		Else
    			_Alert += "Produto "+SubStr(TMPSC2->C2_PRODUTO,1,8)+" n�o tem Mix cadastrado" +CRLF
    		Endif
		Endif

		DbSelectArea("TMPSC2")
		DbSkip()
	End

	DbCloseArea()

Return

Static Function ExcOP()

Local _w 		:= 0
	ProcRegua(_len)

	While _i <= _len
	  	IncProc("Excluindo OPs...")

		DbSelectArea("SC2")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SC2")+_op1[_i,1])
	
		If Found() 
			_achou := ASCAN( _opg, {|x| Alltrim(x[1]) == AllTrim(SC2->C2_PRODUTO)})

			If _achou > 0
				If SC2->C2_TPOP == "F" .and. alltrim(SC2->C2_SEQPAI) = '' .and. SC2->C2_QUJE == 0
					_C2Area := GetArea()
					_nRecSC2:=Recno()
					A650Deleta("SC2",_nRecSC2,5,{},"ELIMOP")
					RestArea(_C2Area)
				Endif
				_i++
			Else
    			ADel( _op1, _i )
  				ASize( _op1, len(_op1)-1 )
  				_len := _len - 1
			Endif
		Else
			_i++
		Endif
	End
Return

Static Function IncOP()

Local _w 		:= 0
Local _i 		:= 0

	ProcRegua(len(_opg))

	While len(_opg) > 0
	  	IncProc("Incluindo OPs...")

		_op := {}
		
		For _w := 1 to len(_opg)
			If SubStr(_opg[_w,1],1,11) = SubStr(_opg[1,1],1,11)
    			Aadd(_op, {_opg[_w,1],_opg[_w,2]})
			Endif
		Next		

		_w := 1
		_len := len(_opg)
		While _w <= _len
			If SubStr(_opg[_w,1],1,11) = SubStr(_op[1,1],1,11)
    			ADel( _opg, _w )
  				ASize( _opg, len(_opg)-1 )
  				_len := _len - 1
  			Else
  				_w++
			Endif
		End		

		_qtd := 0
		_qtd2:= 0
		For _i := 1 to len(_op)
			_qtd += _op[_i,2]
			_qtd2+= _op[_i,2]
		Next

		_mix := {}
		_divlot := 0

		For _i := 1 to len(_op)
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+_op[_i,1])
			_loteop := SB1->B1_XQMIN
			_lotem  := SB1->B1_XQMIN
		
			DbSelectArea("SB4")
			DbSetOrder(1)
			DbSeek(xfilial("SB4")+SubStr(_op[_i,1],1,8))
		
			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " WHERE "
  			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +SB4->B4_COLUNA +"' AND D_E_L_E_T_ = '' " 
	  		cQuery += "ORDER BY R_E_C_N_O_"
	  		cQuery := ChangeQuery(cQuery)
		  	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

			DbSelectArea("TMPSBV")
  		
			_tam := 1
  		
			While !EOF() .and. alltrim(TMPSBV->BV_CHAVE) <> SubStr(_op[_i,1],12,4)
				_tam++
				DbSelectArea("TMPSBV")
				DbSkip()
			End
		
			DbCloseArea()
		
			DbSelectArea("SZH")
			DbSetOrder(1)
			DbSeek(xfilial("SZH")+SubStr(_op[_i,1],1,8))
		
			_campo := &("SZH->ZH_TAM"+strzero(_tam,3))
	
			aadd(_mix, {_op[_i,1],_loteop,_campo,_op[_i,1]})
			_divlot += _campo

		Next
		
		_tamlot := 999999

		For _i := 1 to len(_op)
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+_mix[_i,1])

			//If (_op[_i,2]/SB1->B1_LM) < _tamlot
//			If (_op[_i,2]/(_mix[_i,2]*_mix[_i,3]*((_lotem/_mix[1,2])/_divlot))) < _tamlot
			If _op[_i,2]/(NoRound((_mix[_i,2]*_mix[_i,3]*((_lotem/_mix[1,2])/_divlot))/SB1->B1_XQTMIN,0)*SB1->B1_XQTMIN) < _tamlot
//				_tamlot := NoRound(_op[_i,2]/(_mix[_i,2]*_mix[_i,3]*((_lotem/_mix[1,2])/_divlot)),0)
				_tamlot := NoRound(_op[_i,2]/(NoRound((_mix[_i,2]*_mix[_i,3]*((_lotem/_mix[1,2])/_divlot))/SB1->B1_XQTMIN,0)*SB1->B1_XQTMIN),0)

			Endif
		Next

		If Len(_mix) > 0 .and. _mix[1,2] > 0
			_nlotes := _lotem/_mix[1,2]
			_xqtd := _nlotes / _divlot

			While _qtd <> 0
				cNumOP := SubStr(_PAR01,1,6)
				cNwOP  := SubStr(_PAR01,1,6)
				_nwop := {}

				DbSelectArea("SC2")
				DbSetOrder(1)
				While DbSeek(xfilial("SC2")+cNumOP)
					cNumOP := Soma1(cNumOP)
					cNwOP  := Soma1(cNwOP)
				End

				For _i := 1 to len(_mix)
					aadd(_nwop,{cNwOP+"01001"+strzero(_i,2),cNumOP+"01001"+strzero(_i,2)})
					_achou := ASCAN( _op, {|x| Alltrim(x[1]) == AllTrim(_mix[_i,1])})

					IF _tamlot > 0
						cgrade := "S"
						_qtdop := _mix[_i,2]*_mix[_i,3]*_xqtd
					Else
						If _qtd2 <=  _mix[_i,2]
							cgrade := "S"
							_qtdop := _op[_achou,2]
						Else
							cgrade := "N"
							_qtdop := _mix[_i,2]
						Endif
					Endif

					DbSelectArea("SB1")
					DbSetOrder(1)
					DbSeek(xfilial("SB1")+_mix[_i,1])
								
					If (_qtdop/SB1->B1_XQTMIN) <> NoRound(_qtdop/SB1->B1_XQTMIN,0)
						_qtdop := NoRound(_qtdop/SB1->B1_XQTMIN,0)*SB1->B1_XQTMIN
					Endif
					

					IF _op[_achou,2] < _qtdop
						_qtdop := _op[_achou,2]
						cgrade := "N"
					Endif
		
					_op[_achou,2] := _op[_achou,2] - _qtdop 

					If _qtdop > 0		
						DbSelectArea("SB1")
						DbSetOrder(1)
						DbSeek(xfilial("SB1")+_mix[_i,1])
	
						aMata650:= {{'C2_FILIAL'	,xfilial("SC2")			,NIL},;
									{'C2_NUM'		,cNumOp					,NIL},;
								  	{'C2_ITEM'     	,"01" 					,NIL},;
									{'C2_SEQUEN'   	,"001"					,NIL},;
									{'C2_PRODUTO'  	,_mix[_i,1]				,NIL},;
									{'C2_LOCAL'    	,SB1->B1_LOCPAD			,NIL},;
									{'C2_QUANT'    	,_qtdop					,NIL},;
									{'C2_UM'       	,SB1->B1_UM  			,NIL},;
									{'C2_DATPRI'   	,ddatabase				,NIL},;
									{'C2_DATPRF'   	,ddatabase				,NIL},;
									{'C2_TPOP'     	,"F" 					,NIL},;
									{'C2_GRADE'		,cGrade					,NIL},;
									{'C2_ITEMGRD'	,IIF(CGRADE = "S", strzero(_i,2),""),NIL},;
									{'C2_OBS'		,_mrp					,NIL},;
									{'C2_EMISSAO'  	,ddatabase				,NIL},;
									{'AUTEXPLODE'  	,'N'					,NIL},;
									{'INDEX'  		,1						,NIL}}
						//-- Chamada da rotina automatica
						lMsErroAuto := .f.
						msExecAuto({|x,Y| Mata650(x,Y)},aMata650,3)
			
						If lMsErroAuto
							_qtd := _qtd - _qtdop
							cNumOP := Soma1(cNumOP)
							Mostraerro()
						Else
							_qtd := _qtd - _qtdop
							cNumOP := Soma1(cNumOP)
							DbSelectArea("SC2")
							DbSetOrder(1)
							While Dbseek(xfilial("SC2")+alltrim(cNumOP))
								cNumOP := Soma1(cNumOP)
							End
						EndIf
					Endif
		
				Next
				_tamlot--

				For _i:=1 to len(_nwop)
					DbSelectArea("SC2")
					DbSetOrder(1)
					DbSeek(xfilial("SC2")+SubStr(_nwop[_i,2],1,6))
					
					If SC2->C2_GRADE == "S"
						_qry := "Update "+RetSqlName("SC1")+" set C1_OP = '"+SubStr(_nwop[_i,1],1,6)+"'+SubString(C1_OP,7,7) where D_E_L_E_T_ = '' and C1_OP like '"+SubStr(_nwop[_i,2],1,6)+"%'"
						TcSqlExec(_qry) 
						_qry := "Update "+RetSqlName("SD4")+" set D4_OP = '"+SubStr(_nwop[_i,1],1,6)+"'+SubString(D4_OP,7,7) where D_E_L_E_T_ = '' and D4_OP like '"+SubStr(_nwop[_i,2],1,6)+"%'"
						TcSqlExec(_qry) 
						_qry := "Update "+RetSqlName("SC2")+" set C2_NUM = '"+SubStr(_nwop[_i,1],1,6)+"' where D_E_L_E_T_ = '' and C2_NUM = '"+SubStr(_nwop[_i,2],1,6)+"'"
						TcSqlExec(_qry)
					Endif 
				Next
				
				//cNwOP := Soma1(cNwOP)
			End

		Endif
	End
Return

Static Function ConfOP()

	_qry := "Select * from "+RetSqlName("ZZB")+" ZZB where ZZB.D_E_L_E_T_ = '' and "
	_qry += "(Select top 1 C2_PRODUTO from "+RetSqlName("SC2")+" SC2 where SC2.D_E_L_E_T_ = '' and C2_PRODUTO = ZZB_PRODUT and C2_OBS = '"+_mrp+"') is null "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPZZB",.T.,.T.)

	DbSelectArea("TMPZZB")
	DbGoTop()
		
	While !EOF()

		DbSelectArea("SC2")
		DbSetOrder(1)
		While DbSeek(xfilial("SC2")+cOPPar)
			cOPPar := Soma1(cOPPar)
			//cNwOP  := Soma1(cNwOP)
		End

		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+TMPZZB->ZZB_PRODUT)
	
		aMata650:= {{'C2_FILIAL'	,xfilial("SC2")			,NIL},;
					{'C2_NUM'		,cOPPar					,NIL},;
				  	{'C2_ITEM'     	,"01" 					,NIL},;
					{'C2_SEQUEN'   	,"001"					,NIL},;
					{'C2_PRODUTO'  	,SB1->B1_COD			,NIL},;
					{'C2_LOCAL'    	,SB1->B1_LOCPAD			,NIL},;
					{'C2_QUANT'    	,TMPZZB->ZZB_QUANT		,NIL},;
					{'C2_UM'       	,SB1->B1_UM  			,NIL},;
					{'C2_DATPRI'   	,ddatabase				,NIL},;
					{'C2_DATPRF'   	,ddatabase				,NIL},;
					{'C2_TPOP'     	,"F" 					,NIL},;
					{'C2_OBS'		,_mrp					,NIL},;
					{'C2_EMISSAO'  	,ddatabase				,NIL},;
					{'AUTEXPLODE'  	,'N'					,NIL},;
					{'INDEX'  		,1						,NIL}}

		lMsErroAuto := .f.
		msExecAuto({|x,Y| Mata650(x,Y)},aMata650,3)
			
		If lMsErroAuto
			cOPPar := Soma1(cOPPar)
			Mostraerro()
		Else
			cOPPar := Soma1(cOPPar)
			DbSelectArea("SC2")
			DbSetOrder(1)
			While Dbseek(xfilial("SC2")+alltrim(cOPPar))
				cOPPar := Soma1(cOPPar)
			End
		EndIf
	
		DbSelectArea("TMPZZB")
		DbSkip()
	End
	
	DbCloseArea()

/*
	_qry := "Select C2_PRODUTO, QTD, QTDZZB from ( "
	_qry += "select C2_PRODUTO, sum(C2_QUANT) as QTD, "
	_qry += "Isnull((Select Sum(ZZB_QUANT) from "+RetSqlName("ZZB")+" ZZB where ZZB.D_E_L_E_T_ = '' and ZZB_PRODUT = C2_PRODUTO),0) as QTDZZB "
	_qry += "from "+RetSqlName("SC2")+" where D_E_L_E_T_ = '' and C2_OBS = '"+_mrp+"' and C2_QUJE = 0 and C2_SEQPAI = '' AND C2_XRES = '' and C2_TPOP = 'F' " 
	_qry += "group by C2_PRODUTO) as x "
//	_qry += "where QTD-QTDZZB > 0 "

	cQuery := ChangeQuery(_qry)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC2",.T.,.T.)

	DbSelectArea("TMPSC2")
	DbGoTop()

	_ok := .t.	
	While !EOF()
		If (TMPSC2->QTD - TMPSC2->QTDZZB) > 0
//			_op := .F.
//			Alert("O produto "+TMPSC2->C2_PRODUTO+" apresentou diverg�ncias na aglutina��o!")
		Else
			_qry := "update "+RetSqlName("ZZB")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D_E_L_E_T_ = '' and ZZB_PRODUT = '"+TMPSC2->C2_PRODUTO+"' "
			TcSqlExec(_qry)
		Endif
		
		Dbskip()
	End
*/	
//	If _ok
	If alltrim(_alert) <> ""
		MsgAlert(_alert)
	Endif    

	Alert("Aglutina��o realizada!")
//	Endif
	
	DbCloseArea()
Return
