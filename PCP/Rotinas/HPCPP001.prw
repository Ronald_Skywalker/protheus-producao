#INCLUDE "TOTVS.CH"
#INCLUDE "RWMAKE.CH"
#INCLUDE "TBICONN.CH"
#Include "Topconn.ch"
#Include 'Protheus.ch'


User Function HPCPP001()

	Local aVetor := {}
	Local _i
	Local _w
	Local aParam := {}
	Local aRetParm	:= {}
	Local _execauto := .T.
	Local aPergs := {}
	Local aRet := {}
	Local aPergs2 := {}
	Local aRet2 := {}
	Local lModggf := .T.
	Local nContIT := 0

	dData:=dDataBase

	_CLOCAL:=ALLTRIM(UPPER(Getmv("MV_HENDPAD")))

	aAdd(aPergs,{1,"Da Op",Space(15),"","","SC2-02","",80,.F.}) // MV_PAR01
	aAdd(aPergs, {1,"Data fim", dData,  "", ".T.", "", ".T.", 80,  .T.})    // MV_PAR02
	aAdd(aPergs,{1,"Operacao" ,Space(2),"","","","",80,.F.})    // MV_PAR03
	aAdd(aPergs, {2, "Inclusao/Estorno", "", {"1=Inclusao", "2=Estorno"}, 090, ".T.", .F.}) // MV_PAR04

	If ParamBox(aPergs,"Par�metros...",@aRet)

		_par01 := MV_PAR01
		_par02 := MV_PAR02
		_par03 := MV_PAR03
		_par04 := Val(MV_PAR04)

		DbSelectArea("SC2")
		DbSetOrder(1)
		DbSeek(xfilial("SC2")+sUBsTR(alltrim(_PAR01),1,11))

		If SC2->C2_XRES = " "
			//Alert("Ordem de Produ�ao n�o est� liberada!")
			MessageBox("Ordem de Produ�ao n�o est� liberada!.", "TOTVS", 16)
			Return
		ElseIf SC2->C2_XRES = "L"
			//Alert("Ordem de Producao foi liberada mas ainda nao foi transferida!")
			MessageBox("Ordem de Producao foi liberada mas ainda nao foi transferida!.", "TOTVS", 16)
			Return
		EndIf
		_ret := .T.
		_aOP := {}

		While !EOF() .AND. C2_NUM+C2_ITEM+C2_SEQUEN >= alltrim(SubStr(_PAR01,1,11)) .AND. C2_NUM+C2_ITEM+C2_SEQUEN <= alltrim(SubStr(_PAR01,1,11))
			If C2_QUJE < C2_QUANT
				Aadd(_aOP, {SC2->C2_NUM, SC2->C2_ITEM, SC2->C2_SEQUEN, SC2->C2_PRODUTO, SC2->C2_ITEMGRD, ' '})
			EndIf

			DbSelectArea("SC2")
			DbSkip()
		EndDo

		DbSelectArea("SC2")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SC2")+alltrim(_PAR01))

		_temrot := .F.

		DbSelectArea("SG2")
		DbSetOrder(3)
		DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

		If !Found()
			DbSelectArea("SG2")
			DbSetOrder(8)
			DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)

			If Found()
				_temrot := .T.
			EndIf
		Else
			_temrot := .T.
		EndIf

		If _temrot
			DbSkip()
			If alltrim(SG2->G2_PRODUTO) == alltrim(SC2->C2_PRODUTO) .or. SubStr(SC2->C2_PRODUTO,1,8) == alltrim(SG2->G2_REFGRD)
				_ultimo := .F.
			Else
				_ultimo := .T.
			EndIf

			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+SC2->C2_PRODUTO)

			_parx1 := 1
			If _ultimo .AND. (SB1->B1_TIPO = "PF" .or. SB1->B1_TIPO = "KT")
				aAdd(aPergs2,{1,"Qte Baus",0,"@E 999,999,999.999999","","SC2-02","",80,.F.}) // MV_PAR01

				If ParamBox(aPergs2,"Par�metros...",@aRet2)
					_parx1 := MV_PAR01
				Else
					//Alert("Cancelado Pelo Usu�rio!!!")
					MessageBox("Cancelado Pelo Usu�rio!!!", "TOTVS", 16)
				EndIf

			EndIf

			aAdd(aParam,{1,"Cod Barra",Space(50) ,"",".T.",,".T.",80,.F.})
			For _i := 1 to _parx1
				lMsErroAuto := .F.

				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+SC2->C2_PRODUTO)

				_ean := ""
				If _ultimo .AND. (SB1->B1_TIPO = "PF" .or. SB1->B1_TIPO = "KT")
					DbSelectArea("SC2")
					DbSetOrder(1)
					DbSeek(xfilial("SC2")+_aOP[1,1]+_aOP[1,2]+_aOP[1,3]+_aOP[1,5])

					If _ultimo .AND. (SB1->B1_TIPO = "PF" .or. SB1->B1_TIPO = "KT")

						If ParamBox(aParam,"Cod Barra",@aRetParm,{||.T.},,,,,,"U_HPCPP001",.F.,.F.)
							lOk	:= .T.
							_qtd := Substr(aRetParm[1],29,len(alltrim(aRetParm[1]))-28)
							_qtd := SubStr(_qtd,1,at("+",_qtd)-1)
							_qtd := val(_qtd)
							_sblote := SubStr(aRetParm[1],1,6)
							_lote := subStr(aRetParm[1],22,6)
							_xtp := "P"
							_ean := SubStr(aRetParm[1],8,13)

						else
							_qtd := SC2->C2_QUANT
							_sblote := ""
							_lote := SC2->C2_NUM
							_xtp := "T"
							lOk := .F.
						EndIf

					Else
						_qtd := SC2->C2_QUANT
						_sblote := ""
						_lote := SC2->C2_NUM
						_xtp := "T"
					EndIf

					_qry1 := "SELECT * FROM "+RetSqlName("SH6")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND H6_XSLOTE = '"+_sblote+"' "
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry1),"TMPSBF",.T.,.T.)

					DbSelectArea("TMPSBF")
					DbGoTop()
					_prod := ""
					If EOF()

						If _lote == SC2->C2_NUM

							If alltrim(_sblote) <> ""
								_qry := "SELECT * FROM "+RetSqlName("SZS")+" SZS WITH (NOLOCK) "
								_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) on SB1.D_E_L_E_T_ = '' AND B1_COD = ZS_PRODUTO "
								_qry += "WHERE SZS.D_E_L_E_T_ = '' AND ZS_NUMEROP = '"+SubStr(_par01,1,6)+"' AND ZS_CODRECI = '"+_sblote+"' " // AND B1_COD = '"+SC2->C2_PRODUTO+"' "

								dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZS",.T.,.T.)

								DbSelectArea("TMPSZS")
								DbGoTop()
								If EOF()
									_execauto := .F.
									//Alert("Apontamento do Ba� n�o confere com as informa��es da Fac��o. A ordem n�o poder� ser concluida!")
									MessageBox("Apontamento do Ba� n�o confere com as informa��es da Fac��o. A ordem n�o poder� ser concluida.", "TOTVS", 16)
								Else
									_prod := TMPSZS->ZS_PRODUTO
									DbSelectArea("SC2")
									DbSetOrder(9)
									DbSeek(xfilial("SC2")+TMPSZS->ZS_NUMEROP+SC2->C2_ITEM+TMPSZS->ZS_PRODUTO)

									If !Found()
										_execauto := .F.
										//Alert("Apontamento do Ba� n�o confere com as informa��es da Fac��o. A ordem n�o poder� ser concluida!")
										MessageBox("Apontamento do Ba� n�o confere com as informa��es da Fac��o. A ordem n�o poder� ser concluida.", "TOTVS", 16)

									EndIf
								EndIf

								DbSelectArea("TMPSZS")
								DbCloseArea()
							EndIf

							DbSelectArea("SG2")
							DbSetOrder(3)
							DbGoTop()
							DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

							If !Found()
								DbSelectArea("SG2")
								DbSetOrder(8)
								DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
							EndIf

							DbSelectArea("SC2")
							DbSetOrder(9)
							DbGoTop()
							DbSeek(xfilial("SC2")+SubStr(_par01,1,6)+SC2->C2_ITEM+_prod)

							DbSelectArea("SH6")
							DbSetOrder(1)
							DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+SC2->C2_PRODUTO+_PAR03)

							If Found()
								While !EOF() .AND. SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN = SubStr(_par01,1,11) .AND. SC2->C2_PRODUTO = _prod
									DbSelectArea("SH6")
									DbSetOrder(1)
									DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+SC2->C2_PRODUTO+_PAR03)

									If  !Found()
										Exit
									EndIf
									DbSelectArea("SC2")
									DbSkip()
								EndDo
							EndIf

							DbSelectArea("SC2")
							DbSetOrder(9)
							DbGoTop()
							DbSeek(xfilial("SC2")+SubStr(_par01,1,6)+SC2->C2_ITEM+_prod)



							If Found() .AND. alltrim(_prod) <> "" .AND. SC2->C2_NUM == SubStr(_par01,1,6)

								If _ultimo .AND. alltrim(SC2->C2_YFORNEC) == ""
									_qry := "UPDATE "+RetSqlName("SD4")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ WHERE D4_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D4_COD LIKE 'B"+SubStr(SC2->C2_PRODUTO,2,7)+"%'"
									TcSqlExec(_qry)
								EndIf

								_hr := (_QTd/SG2->G2_LOTEPAD)*SG2->G2_TEMPAD
								If _hr > 8
									_dtini := _PAR02-(_hr/8)
									_hrini := zVal2Hora( 17-((_hr/8)-int(_hr/8)),":")
									_hrfim := "17:00"
								Else
									_dtini := _PAR02
									_hrini := zVal2Hora(17-_hr,":")
									_hrfim := "17:00"
								EndIf

								If SC2->C2_QUANT-SC2->C2_QUJE <= _QTD
									_xtp := "T"
								EndIf

								_execauto := .T.

								If SELECT("TMPSC2") > 0
									TMPSC2->(DBCLOSEAREA())
								EndIf

								_cQuery := " SELECT  C2_RECURSO FROM "+RetSqlName("SC2")+" WITH(NOLOCK)  JOIN "+RetSqlName("SB1")+" WITH(NOLOCK) ON C2_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = '' "
								_cQuery += " WHERE C2_NUM = '"+SubStr(_par01,1,6)+"' AND SC2010.D_E_L_E_T_ = '' AND B1_TIPO IN ('PI','PF') "
								_cQuery += " GROUP BY C2_RECURSO "

								dbUseArea(.T.,"TOPCONN",TcGenQry(,,_cQuery),"TMPSC2",.T.,.T.)

								If alltrim(TMPSC2->C2_RECURSO) <> "" .AND. _PAR03 = "30"
									_recurso := TMPSC2->C2_RECURSO

								Else
									If SC2->C2_STATUS = "S"
										DbSelectarea("SH8")
										DbSetOrder(1)
										DbSeek(xfilial("SH8")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+_PAR03)
										If Found()
											_recurso := SH8->H8_RECURSO
										Else
											_recurso := SG2->G2_RECURSO
										EndIf
									Else
										_recurso := SG2->G2_RECURSO
									EndIf
								EndIf

								DbSelectArea("SB1")
								DbSetOrder(1)
								DbSeek(xfilial("SB1")+SC2->C2_PRODUTO)

								If _dtini = _par02 .AND. _hrini = "17:00"
									_hrini := "16:59"
								EndIf

								If SB1->B1_RASTRO = "S"

									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
										{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
										{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
										{"H6_OPERAC" 	,_PAR03  			,NIL},;
										{"H6_RECURSO" 	,_RECURSO			,NIL},;
										{"H6_DTAPONT"  	,dData				,NIL},;
										{"H6_DATAINI" 	,_dtini  			,NIL},;
										{"H6_HORAINI"	,_hrini  			,NIL},;
										{"H6_DATAFIN"	,_PAR02				,NIL},;
										{"H6_HORAFIN"	,_hrfim  			,NIL},;
										{"H6_PT"     	,_xtp      			,NIL},;
										{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
										{"H6_QTDPROD"	,_QTD				,NIL},;
										{"H6_LOTECTL"	,"000000"			,NIL},;
										{"H6_XSLOTE"	,_sblote 			,NIL}}

								ElseIf SB1->B1_RASTRO = 'L'

									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
										{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
										{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
										{"H6_OPERAC" 	,_PAR03  			,NIL},;
										{"H6_RECURSO" 	,_RECURSO			,NIL},;
										{"H6_DTAPONT"  	,dData				,NIL},;
										{"H6_DATAINI" 	,_dtini  			,NIL},;
										{"H6_HORAINI"	,_hrini  			,NIL},;
										{"H6_DATAFIN"	,_PAR02				,NIL},;
										{"H6_HORAFIN"	,_hrfim  			,NIL},;
										{"H6_PT"     	,_xtp      			,NIL},;
										{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
										{"H6_QTDPROD"	,_QTD				,NIL},;
										{"H6_LOTECTL"	,"000000"			,NIL}}
								Else
									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
										{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
										{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
										{"H6_OPERAC" 	,_PAR03  			,NIL},;
										{"H6_RECURSO" 	,_RECURSO			,NIL},;
										{"H6_DTAPONT"  	,dData				,NIL},;
										{"H6_DATAINI" 	,_dtini  			,NIL},;
										{"H6_HORAINI"	,_hrini  			,NIL},;
										{"H6_DATAFIN"	,_PAR02				,NIL},;
										{"H6_HORAFIN"	,_hrfim  			,NIL},;
										{"H6_PT"     	,_xtp      			,NIL},;
										{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
										{"H6_QTDPROD"	,_QTD				,NIL}}

								EndIf

								lMsErroAuto := .f.
								If _PAR04 = 1
									_execauto := .T.
									If _ultimo
										// TODO N�o gravar o numero da OP quando for produ��o interna (Weskley Silva 30/05/2019)
										//_qry := "SELECT C5_NUM FROM "+RetSqlName("SC5")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND left(C5_XNUMOP,11) = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+"' AND C5_CLIENTE <> '002877' "
										_qry := "SELECT C2_YFORNEC FROM "+RetSqlName("SC2")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND C2_NUM = LEFT('"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+"',6) AND C2_SEQUEN = '001' AND C2_YFORNEC NOT IN ('002877','002878') "

										If SELECT("TMPSC2") > 0
											TMPSC2->(DbCloseArea())
										EndIf

										dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC2",.T.,.T.)

										DbSelectArea("TMPSC2")
										DbGoTop()
										If !EOF()
											_qry := "SELECT C5_NUM FROM "+RetSqlName("SC5")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND left(C5_XNUMOP,11) = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+"' AND C5_CLIENTE NOT IN ('002877','002878') "

											dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC5",.T.,.T.)

											DbSelectArea("TMPSC5")
											DbGoTop()
											If !EOF()
												_qry := "SELECT QTD - (SELECT isnull(sum(H6_QTDPROD),0) FROM "+RetSqlName("SH6")+" SH6 WITH (NOLOCK) WHERE SH6.D_E_L_E_T_ = '' AND H6_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND H6_OPERAC = '"+_PAR03+"') as SALDO FROM ( "
												_qry += "SELECT isnull(Sum(D1_QUANT),0) as QTD "
												_qry += "FROM "+RetSqlName("SD1")+" SD1 WITH (NOLOCK) WHERE SD1.D_E_L_E_T_ = '' AND D1_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"') as x "

												dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSLD",.T.,.T.)

												DbSelectArea("TMPSLD")
												DbGoTop()
												If TMPSLD->SALDO < _qtd
													MessageBox("A NF do Servico de Beneficiamento ainda n�o foi lan�ada, o que impede o apontamento desta Ordem de Produ��o!", "TOTVS", 16)

													_execauto := .F.
												EndIf
												DbSelectArea("TMPSLD")
												DbCloseArea()
											EndIf
											DbSelectArea("TMPSC5")
											DbCloseArea()
										EndIf
										DbSelectArea("TMPSC2")
										DbCloseArea()
									EndIf


									If _execauto
										MSExecAuto({|x| mata681(x)},aVetor)

										If lMsErroAuto
											_ret := .f.
											mostraerro()
										else

											If alltrim(_sblote) <> ""
												_qry := "SELECT H6_NUMLOTE,H6_PRODUTO FROM "+RetSqlName("SH6")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND H6_NUMLOTE <> H6_XSLOTE AND H6_XSLOTE = '"+_sblote+"' "
												dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSH6",.T.,.T.)

												DbSelectArea("TMPSH6")
												DbGoTop()

												If TMPSH6->(!EOF())
													_qry := "UPDATE "+RetSqlName("SDA")+" SET DA_NUMLOTE = '"+_sblote+"' WHERE D_E_L_E_T_ = '' AND DA_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' AND DA_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
													TcSqlExec(_qry)
													_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_NUMLOTE = '"+_sblote+"' WHERE D_E_L_E_T_ = '' AND D3_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' AND D3_COD = '"+TMPSH6->H6_PRODUTO+"' "
													TcSqlExec(_qry)
													DbSelectArea("SD5")
													DbSetOrder(1)
													DbSeek(xfilial("SD5")+TMPSH6->H6_NUMLOTE+TMPSH6->H6_PRODUTO)
													If Found()
														RecLock("SD5",.F.)
														Replace D5_NUMLOTE with _sblote
														MsUnLock()
													EndIf

													_qry := "UPDATE "+RetSqlName("SB8")+" SET B8_NUMLOTE = '"+_sblote+"' WHERE D_E_L_E_T_ = '' AND B8_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' AND B8_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
													TcSqlExec(_qry)
													_qry := "UPDATE "+RetSqlName("SH6")+" SET H6_NUMLOTE = '"+_sblote+"' WHERE H6_XSLOTE = '"+_sblote+"' AND H6_NUMLOTE <> H6_XSLOTE "
													TcSqlExec(_qry)
												EndIf
												TMPSH6->(DbCloseArea())
											EndIf
										EndIf
									EndIf
								Else
									DbSelectArea("SG2")
									DbSetOrder(3)
									DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

									If !Found()
										DbSelectArea("SG2")
										DbSetOrder(8)
										DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
									EndIf
									_maoobra := SG2->G2_MAOOBRA
									DbSkip()
									If alltrim(SG2->G2_PRODUTO) == alltrim(SC2->C2_PRODUTO) .or. SubStr(SC2->C2_PRODUTO,1,8) == alltrim(SG2->G2_REFGRD)
										_ultimo := .F.
									Else
										_ultimo := .T.
									EndIf

									_opfut := .F.
									If !_ultimo
										DbSelectArea("SH6")
										DbSetOrder(1)
										DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+SC2->C2_PRODUTO+SG2->G2_OPERAC)

										If Found()
											_opfut := .T.
										EndIf
									EndIf

									If !_opfut
										MSExecAuto({|x,Z| MATA681(x,Z)},aVetor,5)

										//_qry := "Delete "+RetSqlName("SD3")+" WHERE D_E_L_E_T_ = '' AND D3_COD = 'MODGGF' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_EMISSAO = '"+DTOS(dDataBase)+"' "
										_qry := "Delete "+RetSqlName("SD3")+" WHERE D_E_L_E_T_ = '' AND D3_COD = 'MODGGF' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_YOPERAC = '"+_PAR03+"' "

										TcSqlExec(_qry)
										/*
										_qry := "UPDATE "+RetSqlName("SD3")+" SET D_E_L_E_T_ = '*' WHERE D_E_L_E_T_ = '' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' "
										TcSqlExec(_qry)

										//_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_QUANT = D3_QUANT * "+str(_maoobra)+" WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "
										*/
										// TODO RETIRAR UPDATE
										_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
										_qry += "FROM "+RetSqlName("SD3")+" SD3 "
										_qry += "INNER JOIN "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' AND C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
										_qry += "INNER JOIN "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' AND H6_OP = D3_OP AND D3_OP = H6_OP AND C2_PRODUTO = H6_PRODUTO AND D3_EMISSAO = H6_DATAFIN AND D3_IDENT = H6_IDENT "
										_qry += "INNER JOIN "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' AND G2_REFGRD = left(C2_PRODUTO,8) AND G2_OPERAC = H6_OPERAC "
										//_qry += "INNER JOIN "+RetSqlName("SH1")+" SH1 on SH1.D_E_L_E_T_ = '' AND H1_CODIGO = G2_RECURSO "
										_qry += "WHERE SD3.D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "

										TcSqlExec(_qry)
										_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = '' WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = 'S' "
										TcSqlExec(_qry)

										_qry := "SELECT * FROM "+RetSqlName("SD3")+" WITH(NOLOCK) WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "
										cQuery := ChangeQuery(_qry)
										If SELECT("TMPSD3") > 0
											TMPSD3->(DbCloseArea())
										EndIf

										dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
										DbSelectArea("TMPSD3")
										DbGoTop()
										While !EOF()

											cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
											cProduto:= GetMV("MV_XPRDGGF")
											If GetMV("MV_DOCSEQ") <> cDocSeq
												PutMV("MV_DOCSEQ",cDocSeq)
											EndIf

											If _par04 == 2
												_qry := "SELECT TOP 1 H6_OPERAC FROM "+RetSqlName("SH6")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND H6_OP = '"+TMPSD3->D3_OP+"' AND H6_IDENT = '"+TMPSD3->D3_IDENT+"' "

												If SELECT("TMPH6") > 0
													TMPH6->(DbCloseArea())
												EndIf
												dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPH6",.T.,.T.)
												cOperac := TMPH6->H6_OPERAC
											EndIf

											DbSelectArea("SD3")
											RecLock("SD3",.T.)
											SD3->D3_FILIAL  := xFilial("SD3")
											SD3->D3_TM      := TMPSD3->D3_TM
											SD3->D3_COD     := cProduto
											SD3->D3_UM      := TMPSD3->D3_UM
											SD3->D3_QUANT   := TMPSD3->D3_QUANT
											SD3->D3_OP		:= TMPSD3->D3_OP
											SD3->D3_CF      := TMPSD3->D3_CF
											SD3->D3_CONTA   := TMPSD3->D3_CONTA
											SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
											SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
											SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
											SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
											SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
											SD3->D3_NUMSEQ  := cDocseq
											SD3->D3_TIPO    := TMPSD3->D3_TIPO
											SD3->D3_USUARIO := UsrRetName(__cUserID)
											SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
											SD3->D3_IDENT	:= TMPSD3->D3_IDENT
											//SD3->D3_XGGF	:= "Y"
											If _par04 == 1
												SD3->D3_YOPERAC	:= _par03
											Else
												SD3->D3_YOPERAC	:= cOperac
											EndIf
											MsUnlock()
											If _par04 == 1
												_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = 'S',D3_YOPERAC = '"+Alltrim(_par03)+"' WHERE R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
											Else
												_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = 'S',D3_YOPERAC = '"+Alltrim(cOperac)+"' WHERE R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
											EndIf
											TcSqlExec(_qry)

											DbSelectArea("TMPSD3")
											DbSkip()
										EndDo
										TMPSD3->(DbCloseArea())
									Else
										//Alert("Existe operacao posterior ja apontada!")
										MessageBox("Existe operacao posterior ja apontada!", "TOTVS", 16)
									EndIf
								EndIf

								If lMsErroAuto
									_ret := .f.
									Mostraerro()
								Else
									_qry := "SELECT DA_NUMSEQ, H6_IDENT, H6_PRODUTO, * FROM "+RetSqlName("SH6")+" SH6 WITH (NOLOCK) "
									_qry += "INNER JOIN "+RetSqlName("SDA")+" SDA WITH (NOLOCK) on SDA.D_E_L_E_T_ = '' AND DA_PRODUTO = H6_PRODUTO AND H6_LOTECTL = DA_LOTECTL AND DA_NUMLOTE = H6_NUMLOTE AND DA_DATA = H6_DTAPONT AND DA_ORIGEM = 'SD3' "
									_qry += "WHERE SH6.D_E_L_E_T_ = '' AND DA_SALDO > 0 AND H6_PRODUTO = '"+SC2->C2_PRODUTO+"' "

									If SELECT("TMPSDA") > 0
										TMPSDA->(DbCloseArea())
									EndIf
									dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSDA",.T.,.T.)

									DbSelectArea("TMPSDA")
									DbGoTop()

									While TMPSDA->(!EOF()) 
										If ALLTRIM(SC2->C2_NUM) == ALLTRIM(TMPSDA->DA_DOC)
											nContIT++
												_ACAB := {}
												_AITEM:= {}
												Aadd(_acab, {"DA_PRODUTO"		, TMPSDA->DA_PRODUTO	,NIL})
												Aadd(_acab, {"DA_QTDORI"		, TMPSDA->DA_SALDO		,NIL})
												Aadd(_acab, {"DA_LOTECTL"		, TMPSDA->DA_LOTECTL	,NIL})
												Aadd(_acab, {"DA_NUMLOTE"		, TMPSDA->DA_NUMLOTE	,NIL})
												Aadd(_acab, {"DA_LOCAL"			, TMPSDA->DA_LOCAL		,NIL})
												Aadd(_acab, {"DA_DOC"			, TMPSDA->DA_DOC		,NIL})
												Aadd(_acab, {"DA_SERIE"			, TMPSDA->DA_SERIE		,NIL})
												Aadd(_acab, {"DA_CLIFOR"		, TMPSDA->DA_CLIFOR		,NIL})
												Aadd(_acab, {"DA_LOJA"			, TMPSDA->DA_LOJA		,NIL})
												Aadd(_acab, {"DA_TIPONF"		, TMPSDA->DA_TIPONF		,NIL})
												Aadd(_acab, {"DA_ORIGEM"		, "SD3"					,NIL})
												Aadd(_acab, {"DA_NUMSEQ"		, TMPSDA->DA_NUMSEQ		,NIL})

												Aadd(_aitem,{"DB_ITEM"			, RetAsc(nContIT,GetSx3Cache("DB_ITEM","X3_TAMANHO"),.T.)				,NIL})
												Aadd(_aitem,{"DB_ESTORNO"		, "N"					,NIL})
												Aadd(_aitem,{"DB_QUANT"			, TMPSDA->DA_SALDO		,NIL})
												Aadd(_aitem,{"DB_DATA"			, ddatabase				,NIL})
												Aadd(_aitem,{"DB_LOCALIZ"		, _CLOCAL				,NIL})

												lmsErroAuto := .F.
												If _execauto
													msExecAuto({|X,Y,Z|MATA265(X,Y,Z)},_acab,{_aitem},3)
													nContIT := 0
													If lMsErroAuto      // variavel do sigaauto indicando erro na operacao
														MsgBox("Ocorreu erro durante o Enderecamento, favor verificar enderecamentos!","Aviso","INFO")
														MostraErro() //MS 14.01.04
														
													EndIf
												EndIf
										EndIf
										DbSelectArea("TMPSDA")
										DbSkip()
									EndDo

									TMPSDA->(DbCloseArea())

									DbSelectArea("SG2")
									DbSetOrder(3)
									DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

									If !Found()
										DbSelectArea("SG2")
										DbSetOrder(8)
										DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
									EndIf
									_maoobra := SG2->G2_MAOOBRA


//									_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_QUANT = D3_QUANT * "+str(_maoobra)+" WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "
									_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
									_qry += "FROM "+RetSqlName("SD3")+" SD3 "
									_qry += "INNER JOIN "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' AND C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
									_qry += "INNER JOIN "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' AND H6_OP = D3_OP AND D3_OP = H6_OP AND C2_PRODUTO = H6_PRODUTO AND D3_EMISSAO = H6_DATAFIN AND D3_IDENT = H6_IDENT "
									_qry += "INNER JOIN "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' AND G2_REFGRD = left(C2_PRODUTO,8) AND G2_OPERAC = H6_OPERAC "
									//_qry += "INNER JOIN "+RetSqlName("SH1")+" SH1 on SH1.D_E_L_E_T_ = '' AND H1_CODIGO = G2_RECURSO "
									_qry += "WHERE SD3.D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "

									TcSqlExec(_qry)

									_qry := "SELECT * FROM "+RetSqlName("SD3")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "

									If SELECT("TMPSD3") > 0
										TMPSD3->(DbCloseArea())
									EndIf

									dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
									DbSelectArea("TMPSD3")
									DbGoTop()
									While !EOF()

										cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
										cProduto:= GetMV("MV_XPRDGGF")
										If GetMV("MV_DOCSEQ") <> cDocSeq
											PutMV("MV_DOCSEQ",cDocSeq)
										EndIf

										If _par04 == 2
											_qry := "SELECT TOP 1 H6_OPERAC FROM "+RetSqlName("SH6")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND H6_OP = '"+TMPSD3->D3_OP+"' AND H6_IDENT = '"+TMPSD3->D3_IDENT+"' "

											If SELECT("TMPH6") > 0
												TMPH6->(DbCloseArea())
											EndIf
											dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPH6",.T.,.T.)
											cOperac := TMPH6->H6_OPERAC
										EndIf

										DbSelectArea("SD3")
										RecLock("SD3",.T.)
										SD3->D3_FILIAL  := xFilial("SD3")
										SD3->D3_TM      := TMPSD3->D3_TM
										SD3->D3_COD     := cProduto
										SD3->D3_UM      := TMPSD3->D3_UM
										SD3->D3_QUANT   := TMPSD3->D3_QUANT
										SD3->D3_OP		:= TMPSD3->D3_OP
										SD3->D3_CF      := TMPSD3->D3_CF
										SD3->D3_CONTA   := TMPSD3->D3_CONTA
										SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
										SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
										SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
										SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
										SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
										SD3->D3_NUMSEQ  := cDocseq
										SD3->D3_TIPO    := TMPSD3->D3_TIPO
										SD3->D3_USUARIO := UsrRetName(__cUserID)
										SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
										SD3->D3_IDENT	:= TMPSD3->D3_IDENT
										//SD3->D3_XGGF	:= "Z"
										If _par04 == 1
											SD3->D3_YOPERAC	:= _par03
										Else
											SD3->D3_YOPERAC	:= cOperac
										EndIf
										MsUnlock()
										If _par04 == 1
											_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = 'S',D3_YOPERAC = '"+Alltrim(_par03)+"' WHERE R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
										Else
											_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = 'S',D3_YOPERAC = '"+Alltrim(cOperac)+"' WHERE R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
										EndIf
										TcSqlExec(_qry)

										DbSelectArea("TMPSD3")
										DbSkip()
									EndDo
									TMPSD3->(DbCloseArea())
								EndIf
							Else
								_i := _i-1
								//Alert("O produto n�o pertence a essa Ordem de Produ��o!")
								MessageBox("O produto n�o pertence a essa Ordem de Produ��o!", "TOTVS", 16)
							EndIf
						Else
							_i := _i-1
							//Alert("O Lote n�o pertence a essa Ordem de Produ��o!")
							MessageBox("O Lote n�o pertence a essa Ordem de Produ��o", "TOTVS", 16)
						EndIf
					Else
						_i := _i-1
						//Alert("O Lote  ja apontado!")
						MessageBox("O Lote  ja apontado!.", "TOTVS", 16)
					EndIf
					DbSelectArea("TMPSBF")
					DbCloseArea()
					//Next _w
				Else
					For _w := 1 to len(_aOP)
						DbSelectArea("SC2")
						DbSetOrder(1)
						DbSeek(xfilial("SC2")+_aOP[_w,1]+_aOP[_w,2]+_aOP[_w,3]+_aOP[_w,5])

						DbSelectArea("SB1")
						DbSetOrder(1)
						DbSeek(xfilial("SB1")+_aOP[_w,4])

						_qtd := SC2->C2_QUANT
						_sblote := ""
						_lote := SC2->C2_NUM
						_xtp := "T"


						If _lote == SC2->C2_NUM
							If alltrim(_ean) <> ""
								DbSelectArea("SB1")
								DbSetOrder(5)
								DbSeek(xfilial("SB1")+SubStr(_ean,1,12))
							EndIf

							DbSelectArea("SG2")
							DbSetOrder(3)
							DbGoTop()
							DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

							If !Found()
								DbSelectArea("SG2")
								DbSetOrder(8)
								DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
							EndIf

							DbSelectArea("SC2")
							DbSetOrder(1)
							DbGoTop()
							DbSeek(xfilial("SC2")+_aOP[_w,1]+_aOP[_w,2]+_aOP[_w,3]+_aOP[_w,5])


							If Found()
								If _ultimo .AND. alltrim(SC2->C2_YFORNEC) == ""

									_qry := "UPDATE "+RetSqlName("SD4")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ WHERE D4_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D4_COD LIKE 'B"+SubStr(SC2->C2_PRODUTO,2,7)+"%'"
									TcSqlExec(_qry)
								EndIf

								_hr := (_QTd/SG2->G2_LOTEPAD)*SG2->G2_TEMPAD
								If _hr > 8
									_dtini := _PAR02-(_hr/8)
									_hrini := zVal2Hora( 17-((_hr/8)-int(_hr/8)),":")
									_hrfim := "17:00"
								Else
									_dtini := _PAR02
									_hrini := zVal2Hora(17-_hr,":")
									_hrfim := "17:00"
								EndIf

								If SC2->C2_QUANT-SC2->C2_QUJE <= _QTD
									_xtp := "T"
								EndIf

								If alltrim(SC2->C2_RECURSO) <> "" .AND. _PAR03 = "30"
									_recurso := SC2->C2_RECURSO
								Else
									If SC2->C2_STATUS = "S"
										DbSelectarea("SH8")
										DbSetOrder(1)
										DbSeek(xfilial("SH8")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+_PAR03)
										If Found()
											_recurso := SH8->H8_RECURSO
										Else
											_recurso := SG2->G2_RECURSO
										EndIf
									Else
										_recurso := SG2->G2_RECURSO
									EndIf
								EndIf

								DbSelectArea("SB1")
								DbSetOrder(1)
								DbSeek(xfilial("SB1")+SC2->C2_PRODUTO)

								If _dtini = _par02 .AND. _hrini = "17:00"
									_hrini := "16:59"
								EndIf

								If SB1->B1_RASTRO = "S"
									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
										{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
										{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
										{"H6_OPERAC" 	,_PAR03  			,NIL},;
										{"H6_RECURSO" 	,_RECURSO			,NIL},;
										{"H6_DTAPONT"  	,dData				,NIL},;
										{"H6_DATAINI" 	,_dtini  			,NIL},;
										{"H6_HORAINI"	,_hrini  			,NIL},;
										{"H6_DATAFIN"	,_PAR02				,NIL},;
										{"H6_HORAFIN"	,_hrfim  			,NIL},;
										{"H6_PT"     	,_xtp      			,NIL},;
										{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
										{"H6_QTDPROD"	,_QTD				,NIL},;
										{"H6_XSLOTE"	,_sblote 			,NIL},;
										{"H6_LOTECTL"	,"000000"			,NIL}}
								ElseIf SB1->B1_RASTRO = "L"
									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
										{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
										{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
										{"H6_OPERAC" 	,_PAR03  			,NIL},;
										{"H6_RECURSO" 	,_RECURSO			,NIL},;
										{"H6_DTAPONT"  	,dData				,NIL},;
										{"H6_DATAINI" 	,_dtini  			,NIL},;
										{"H6_HORAINI"	,_hrini  			,NIL},;
										{"H6_DATAFIN"	,_PAR02				,NIL},;
										{"H6_HORAFIN"	,_hrfim  			,NIL},;
										{"H6_PT"     	,_xtp      			,NIL},;
										{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
										{"H6_QTDPROD"	,_QTD				,NIL},;
										{"H6_LOTECTL"	,"000000"			,NIL}}

								Else
									aVetor := { {"H6_FILIAL"	,xfilial("SH6")		,NIL},;
										{"H6_OP"	  	,SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD,NIL},;
										{"H6_PRODUTO" 	,SC2->C2_PRODUTO	,NIL},;
										{"H6_OPERAC" 	,_PAR03  			,NIL},;
										{"H6_RECURSO" 	,_RECURSO			,NIL},;
										{"H6_DTAPONT"  	,dData				,NIL},;
										{"H6_DATAINI" 	,_dtini  			,NIL},;
										{"H6_HORAINI"	,_hrini  			,NIL},;
										{"H6_DATAFIN"	,_PAR02				,NIL},;
										{"H6_HORAFIN"	,_hrfim  			,NIL},;
										{"H6_PT"     	,_xtp      			,NIL},;
										{"H6_LOCAL"  	,SC2->C2_LOCAL		,NIL},;
										{"H6_QTDPROD"	,_QTD				,NIL}}

								EndIf
								lMsErroAuto := .f.
								If _PAR04 = 1
									MSExecAuto({|x| mata681(x)},aVetor)

									If alltrim(_sblote) <> ""
										_qry := "SELECT H6_NUMLOTE,H6_PRODUTO FROM "+RetSqlName("SH6")+" WITH(NOLOCK) WHERE D_E_L_E_T_ = '' AND H6_NUMLOTE <> H6_XSLOTE AND H6_XSLOTE = '"+_sblote+"' "
										dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSH6",.T.,.T.)

										DbSelectArea("TMPSH6")
										DbGoTop()

										If TMPSH6->(!EOF())
											_qry := "UPDATE "+RetSqlName("SDA")+" SET DA_NUMLOTE = '"+_sblote+"' WHERE D_E_L_E_T_ = '' AND DA_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' AND DA_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
											TcSqlExec(_qry)
											_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_NUMLOTE = '"+_sblote+"' WHERE D_E_L_E_T_ = '' AND D3_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' AND D3_COD = '"+TMPSH6->H6_PRODUTO+"' "
											TcSqlExec(_qry)
											//										_qry := "UPDATE "+RetSqlName("SD5")+" SET D5_NUMLOTE = '"+_sblote+"' WHERE D_E_L_E_T_ = '' AND D5_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' AND D5_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
											//										TcSqlExec(_qry)
											DbSelectArea("SD5")
											DbSetOrder(1)
											DbSeek(xfilial("SD5")+TMPSH6->H6_NUMLOTE+TMPSH6->H6_PRODUTO)
											If Found()
												RecLock("SD5",.F.)
												Replace D5_NUMLOTE with _sblote
												MsUnLock()
											EndIf

											_qry := "UPDATE "+RetSqlName("SB8")+" SET B8_NUMLOTE = '"+_sblote+"' WHERE D_E_L_E_T_ = '' AND B8_NUMLOTE = '"+TMPSH6->H6_NUMLOTE+"' AND B8_PRODUTO = '"+TMPSH6->H6_PRODUTO+"' "
											TcSqlExec(_qry)
											_qry := "UPDATE "+RetSqlName("SH6")+" SET H6_NUMLOTE = '"+_sblote+"' WHERE H6_XSLOTE = '"+_sblote+"' AND H6_NUMLOTE <> H6_XSLOTE "
											TcSqlExec(_qry)
										EndIf
										TMPSH6->(DbCloseArea())
									EndIf

								Else
									DbSelectArea("SG2")
									DbSetOrder(3)
									DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

									If !Found()
										DbSelectArea("SG2")
										DbSetOrder(8)
										DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
									EndIf
									_maoobra := SG2->G2_MAOOBRA
									DbSkip()
									If alltrim(SG2->G2_PRODUTO) == alltrim(SC2->C2_PRODUTO) .or. SubStr(SC2->C2_PRODUTO,1,8) == alltrim(SG2->G2_REFGRD)
										_ultimo := .F.
									Else
										_ultimo := .T.
									EndIf

									_opfut := .F.
									If !_ultimo
										DbSelectArea("SH6")
										DbSetOrder(1)
										DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+SC2->C2_PRODUTO+SG2->G2_OPERAC)

										If Found()
											_opfut := .T.
										EndIf
									EndIf

									If !_opfut
										MSExecAuto({|x,Z| MATA681(x,Z)},aVetor,5)

										//_qry := "Delete "+RetSqlName("SD3")+" WHERE D_E_L_E_T_ = '' AND D3_COD = 'MODGGF' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_EMISSAO = '"+DTOS(dDataBase)+"' "
										_qry := "Delete "+RetSqlName("SD3")+" WHERE D_E_L_E_T_ = '' AND D3_COD = 'MODGGF' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_YOPERAC = '"+_PAR03+"' "

										TcSqlExec(_qry)
										/*
										_qry := "UPDATE "+RetSqlName("SD3")+" SET D_E_L_E_T_ = '*' WHERE D_E_L_E_T_ = '' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' "
										TcSqlExec(_qry)
										*/
										//_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_QUANT = D3_QUANT * "+str(_maoobra)+" WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "
										_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
										_qry += "FROM "+RetSqlName("SD3")+" SD3 "
										_qry += "INNER JOIN "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' AND C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
										_qry += "INNER JOIN "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' AND H6_OP = D3_OP AND D3_OP = H6_OP AND C2_PRODUTO = H6_PRODUTO AND D3_EMISSAO = H6_DATAFIN AND D3_IDENT = H6_IDENT "
										_qry += "INNER JOIN "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' AND G2_REFGRD = left(C2_PRODUTO,8) AND G2_OPERAC = H6_OPERAC "
										//_qry += "INNER JOIN "+RetSqlName("SH1")+" SH1 on SH1.D_E_L_E_T_ = '' AND H1_CODIGO = G2_RECURSO "
										_qry += "WHERE SD3.D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "

										TcSqlExec(_qry)

										_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = '' WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = 'S' "
										TcSqlExec(_qry)

										_qry := "SELECT * FROM "+RetSqlName("SD3")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND D3_COD = 'MODGGF' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "

										If SELECT("TMPSD3") > 0
											TMPSD3->(DbCloseArea())
										EndIf
										dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)

										If TMPSD3->(!EOF())
											lModggf := .F.
										EndIf
											_qry := "SELECT * FROM "+RetSqlName("SD3")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "

											If SELECT("TMPSD3") > 0
												TMPSD3->(DbCloseArea())
											EndIf

											dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
											DbSelectArea("TMPSD3")
											DbGoTop()
											While !EOF()

												cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
												cProduto:= GetMV("MV_XPRDGGF")
												If GetMV("MV_DOCSEQ") <> cDocSeq
													PutMV("MV_DOCSEQ",cDocSeq)
												EndIf

												If _par04 == 2
													_qry := "SELECT TOP 1 H6_OPERAC FROM "+RetSqlName("SH6")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND H6_OP = '"+TMPSD3->D3_OP+"' AND H6_IDENT = '"+TMPSD3->D3_IDENT+"' "

													If SELECT("TMPH6") > 0
														TMPH6->(DbCloseArea())
													EndIf
													dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPH6",.T.,.T.)
													cOperac := TMPH6->H6_OPERAC
												EndIf
												If lModggf
												DbSelectArea("SD3")
												RecLock("SD3",.T.)
												SD3->D3_FILIAL  := xFilial("SD3")
												SD3->D3_TM      := TMPSD3->D3_TM
												SD3->D3_COD     := cProduto
												SD3->D3_UM      := TMPSD3->D3_UM
												SD3->D3_QUANT   := TMPSD3->D3_QUANT
												SD3->D3_OP		:= TMPSD3->D3_OP
												SD3->D3_CF      := TMPSD3->D3_CF
												SD3->D3_CONTA   := TMPSD3->D3_CONTA
												SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
												SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
												SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
												SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
												SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
												SD3->D3_NUMSEQ  := cDocseq
												SD3->D3_TIPO    := TMPSD3->D3_TIPO
												SD3->D3_USUARIO := UsrRetName(__cUserID)
												SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
												SD3->D3_IDENT	:= TMPSD3->D3_IDENT
												//SD3->D3_XGGF	:= "N"
												If _par04 == 1
													SD3->D3_YOPERAC	:= _par03
												Else
													SD3->D3_YOPERAC	:= cOperac
												EndIf
												MsUnlock()
											EndIf	
												If _par04 == 1
													_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = 'S',D3_YOPERAC = '"+Alltrim(_par03)+"' WHERE R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
												Else
													_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = 'S',D3_YOPERAC = '"+Alltrim(cOperac)+"' WHERE R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
												EndIf
												TcSqlExec(_qry)

												DbSelectArea("TMPSD3")
												DbSkip()
											EndDo
											TMPSD3->(DbCloseArea())
											lModggf := .T.
										
									Else
										//Alert("Existe operacao posterior ja apontada!")
										MessageBox("Existe operacao posterior ja apontada!", "TOTVS", 16)
									EndIf
								EndIf

								If lMsErroAuto
									_ret := .f.
									Mostraerro()
								Else
									DbSelectArea("SG2")
									DbSetOrder(3)
									DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+_PAR03)

									If !Found()
										DbSelectArea("SG2")
										DbSetOrder(8)
										DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+_PAR03)
									EndIf
									_maoobra := SG2->G2_MAOOBRA

//									_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_QUANT = D3_QUANT * "+str(_maoobra)+" WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "
									_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
									_qry += "FROM "+RetSqlName("SD3")+" SD3 "
									_qry += "INNER JOIN "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' AND C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
									_qry += "INNER JOIN "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' AND H6_OP = D3_OP AND D3_OP = H6_OP AND C2_PRODUTO = H6_PRODUTO AND D3_EMISSAO = H6_DATAFIN AND D3_IDENT = H6_IDENT "
									_qry += "INNER JOIN "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' AND G2_REFGRD = left(C2_PRODUTO,8) AND G2_OPERAC = H6_OPERAC "
									//_qry += "INNER JOIN "+RetSqlName("SH1")+" SH1 on SH1.D_E_L_E_T_ = '' AND H1_CODIGO = G2_RECURSO "
									_qry += "WHERE SD3.D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "

									TcSqlExec(_qry)

									_qry := "SELECT * FROM "+RetSqlName("SD3")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD LIKE 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "

									cQuery := ChangeQuery(_qry)
									If SELECT("TMPSD3") > 0
										TMPSD3->(DbCloseArea())
									EndIf

									dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
									DbSelectArea("TMPSD3")
									DbGoTop()
									While !EOF() 
										cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
										cProduto:= GetMV("MV_XPRDGGF")
										If GetMV("MV_DOCSEQ") <> cDocSeq
											PutMV("MV_DOCSEQ",cDocSeq)
										EndIf

										If _par04 == 2
											_qry := "SELECT TOP 1 H6_OPERAC FROM "+RetSqlName("SH6")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND H6_OP = '"+TMPSD3->D3_OP+"' AND H6_IDENT = '"+TMPSD3->D3_IDENT+"' "

											If SELECT("TMPH6") > 0
												TMPH6->(DbCloseArea())
											EndIf
											dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPH6",.T.,.T.)
											cOperac := TMPH6->H6_OPERAC
										EndIf

										DbSelectArea("SD3")
										RecLock("SD3",.T.)
										SD3->D3_FILIAL  := xFilial("SD3")
										SD3->D3_TM      := TMPSD3->D3_TM
										SD3->D3_COD     := cProduto
										SD3->D3_UM      := TMPSD3->D3_UM
										SD3->D3_QUANT   := TMPSD3->D3_QUANT
										SD3->D3_OP		:= TMPSD3->D3_OP
										SD3->D3_CF      := TMPSD3->D3_CF
										SD3->D3_CONTA   := TMPSD3->D3_CONTA
										SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
										SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
										SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
										SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
										SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
										SD3->D3_NUMSEQ  := cDocseq
										SD3->D3_TIPO    := TMPSD3->D3_TIPO
										SD3->D3_USUARIO := UsrRetName(__cUserID)
										SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
										SD3->D3_IDENT	:= TMPSD3->D3_IDENT
										//SD3->D3_XGGF	:= "X"
										If _par04 == 1
											SD3->D3_YOPERAC	:= _par03
										Else
											SD3->D3_YOPERAC	:= cOperac
										EndIf
										MsUnlock()
										If _par04 == 1
											_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = 'S',D3_YOPERAC = '"+Alltrim(_par03)+"' WHERE R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
										Else
											_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = 'S',D3_YOPERAC = '"+Alltrim(cOperac)+"' WHERE R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
										EndIf
										TcSqlExec(_qry)

										DbSelectArea("TMPSD3")
										DbSkip()
									EndDo

									TMPSD3->(DbCloseArea())
								EndIf
							Else
								_i := _i-1
								//Alert("O produto  n�o pertence a essa Ordem de Produ��o!")
								MessageBox("O produto n�o pertence a essa Ordem de Produ��o!.", "TOTVS", 16)
							EndIf
						Else
							_i := _i-1
							//Alert("O Lote  n�o pertence a essa Ordem de Produ��o!")
							MessageBox("O Lote n�o pertence a essa Ordem de Produ��o!.", "TOTVS", 16)
						EndIf
					Next _w
				EndIf
			Next _i

			If _ret
				If _PAR04 = 1
					//u_funciona()//MsgAlert("Apontamento realizado com sucesso!")
					MessageBox("Apontamento realizado com sucesso!.", "TOTVS", 48)
				Else
					//MsgAlert("Estorno realizado com sucesso!","TOTVS")
					MessageBox("Estorno realizado com sucesso!.", "TOTVS", 48)
				EndIf
			EndIf
		Else
			//Alert("Nao existe roteiro cadastrado!")
			MessageBox("N�o existe roteiro cadastrado!.", "TOTVS", 16)
		EndIf
	Else
		//Alert("Cancelado Pelo Usu�rio!!!")
		MessageBox("Cancelado Pelo Usu�rio!!!", "TOTVS", 16)
	EndIf

Return

/*
Static Function AjustaSX1(cPerg)

	Local aAreaAtu	:= GetArea()
	Local aAreaSX1	:= SX1->( GetArea() )

	PutSx1(cPerg,"01","Da OP ?             ","Da OP ?             ","Da OP ?             ","Mv_ch1",TAMSX3("H6_OP" 	   )[3],TAMSX3("H6_OP"     )[1],TAMSX3("H6_OP"     )[2],0,"G","","SC2-02","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o numero da Ordem de Producao.  ",""},{""},{""},"")
	
	PutSx1(cPerg,"02","Data Fim ?          ","Data Fim ?          ","Data Fim ?          ","Mv_ch2",TAMSX3("H6_DATAINI")[3],TAMSX3("H6_DATAINI")[1],TAMSX3("H6_DATAINI")[2],0,"G","",   "","","N","Mv_par02","","","","","","","","","","","","","","","","",{"",""},{""},{""},"")
	
	PutSx1(cPerg,"03","Operacao ?          ","Operacao ?          ","Operacao ?          ","Mv_ch3",TAMSX3("H6_OPERAC" )[3],TAMSX3("H6_OPERAC" )[1],TAMSX3("H6_OPERAC" )[2],0,"G","",   "","","N","Mv_par03","","","","","","","","","","","","","","","","",{"",""},{""},{""},"")
	PutSx1(cPerg,"04","Inclusao/Estorno ?"  ,"Inclusao/Estorno ?"  ,"Inclusao/Estorno ?"  ,"mv_ch4","N",1,0,1,"C","","","","","mv_par04","Inclusao","Inclusao","Inclusao","","Estorno","Estorno","Estorno","","","","","","","","","",{""},{"",""},{"",""}) 

	PutSx1("FPCPA1","01","Qte Baus ?         ","Qtd Baus ?          ","Qtd Baus ?          ","Mv_ch1",TAMSX3("C2_QUANT"  )[3],TAMSX3("C2_QUANT"  )[1],TAMSX3("C2_QUANT"  )[2],0,"G","","","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe a quantidade de Baus.  ",""},{""},{""},"")
	PutSx1("FPCPB1","01","C.Barra  ?         ","C.Barra  ?          ","C.Barra  ?          ","Mv_ch1","C",50,0,0,"G","","","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o codigo de barras dos Baus.  ",""},{""},{""},"")

	RestArea( aAreaSX1 )
	RestArea( aAreaAtu )

Return (cPerg) */

Static Function zVal2Hora(nValor, cSepar)

	Local cHora := ""
	Local cMinutos  := ""

	If nValor < 0
		cHora := SubStr(Time(), 1, 5)
		cHora := StrTran(cHora, ':', cSepar)
	Else
		cHora := Alltrim(Transform(nValor, "@E 99.99"))

		If Len(cHora) < 5
			cHora := Replicate('0', 5-Len(cHora)) + cHora
		EndIf

		cMinutos := SubStr(cHora, At(',', cHora)+1, 2)
		cMinutos := StrZero((Val(cMinutos)*60)/100, 2)
		cHora := SubStr(cHora, 1, At(',', cHora))+cMinutos
		cHora := StrTran(cHora, ',', cSepar)
	EndIf

Return cHora
