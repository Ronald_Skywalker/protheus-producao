#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWPrintSetup.ch"
#Include "TOTVS.CH"
#include 'parmtype.ch'
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'
#INCLUDE "RPTDEF.CH"

// Retornos poss�veis do MessageBox
 #define IDOK			    1
 #define IDCANCEL		    2
 #define IDYES			    6
 #define IDNO			    7

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  HPCPA002  �Autor  �Ronaldo Parreira      � Data �  08/10/21 ���
�������������������������������������������������������������������������͹��
���Desc.     �Lote Separa��o Tecidos.                                     ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE.                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HPCPA002()

Private lGera     := .T.
Private lMarcar   := .F.
Private cAlias    :="ZBH"               
Private cCadastro := "Lote separa��o Tecidos" 
Private oMark                                                                                                   
Private lContinua := .F.
Private cMark     := GetMark()
Private aCampos   := {}
Private aRotina := {{"Incluir","U_IncluiLote()",0,2},; 
					{"Alterar"   ,"",0,2},;
					{"Visualizar" ,"U_VisuLoteOP()",0,2},;                   
					{"Apontar"   ,"",0,2},;
					{"Filtrar"  ,"U_Filtro()",0,2},;					
	                {"Legenda"   ,"U_LegendLote()",0,2},;
                    {"Imprimir"  ,"U_HESTR001(TRB->ZM_LOTDAP)",0,2}}
                     
Private aCores := {{"SubStr(TRB->ZBH_STATUS,1,1)=='T'","DISABLE"},;
					{"SubStr(TRB->ZBH_STATUS,1,1)=='A'","BR_AMARELO"},;
					{"SubStr(TRB->ZBH_STATUS,1,1)=='N'","ENABLE"}}

Private _cIndex := ""
Private _cChave := ""
Private oFont20n    := TFont():New("Arial",,20,,.t.,,,,,.f.)	

private aColsEx := {}

U_Filtro(lGera)				                     
	
	If lGera
	    dbSelectArea("TRB")
							
		_cIndex:=Criatrab(Nil,.F.)
		_cChave:="ZBH_LOTE"
		Indregua("TRB",_cIndex,_cchave,,,"Selecionando Registros...")
		dBSETINDEX(_cIndex+ordbagext())
				
		dbSelectArea("TRB")
		dbGoTop()
		
		aCampos:={{"Data"         ,"ZBH_DTLOTE"  ,TAMSX3("ZBH_DTLOTE"  )[3],TAMSX3("ZBH_DTLOTE"  )[1],TAMSX3("ZBH_DTLOTE"  )[2],X3Picture("ZBH_DTLOTE")},;
                  {"Lote Sep."    ,"ZBH_LOTE"    ,TAMSX3("ZBH_LOTE")[3],TAMSX3("ZBH_LOTE")[1],TAMSX3("ZBH_LOTE")[2],X3Picture("ZBH_LOTE")},;		          
		          {"Qtde. OP"     ,"ZBH_QTDPRO"  ,TAMSX3("ZBH_QTDPRO")[3],TAMSX3("ZBH_QTDPRO")[1],0,"@E 999,999,999.999999"},;
				  {"Qtde. Sep"    ,"ZBH_QTDSEP"  ,TAMSX3("ZBH_QTDSEP")[3],TAMSX3("ZBH_QTDSEP")[1],0,"@E 999,999,999.999999"},;
		          {"Status"       ,"ZBH_STATUS"  ,"C",20,0,"@!"}}
		
		mBrowse(,,,,"TRB",aCampos,,,,,aCores)
		
		TRB->(DbCloseArea())
		TMP->(DbCloseArea())
	EndIf	

Return

User Function Filtro(lGera)

Local aRet  := {}
Local aPerg := {}

	aAdd(aPerg,{1,"Data de: "	,Ctod(Space(8)),"","","","",50,.F.}) 
	aAdd(aPerg,{1,"Data at�: "  ,Ctod(Space(8)),"","","","",50,.F.})
	aAdd(aPerg,{1,"Lote SEP: "  ,Space(6)      ,"","","","",50,.F.})
    
	If ParamBox(aPerg,"Par�metros...",@aRet,{||.T.},,,,,,"U_HPCPA002",.T.,.T.)

		Processa( {|| GeraDados(aRet) }, "Aguarde...", "Processando...",.F.)
	Else
		MessageBox("Cancelado Pelo Usu�rio!!!","Aviso",16)
		lGera := .F.
		return lGera
	Endif

return lGera

Static Function Geradados(aRet)
Local _astru := {}
Local cQuery := ""
Local lRet   := .F.

       	IF Select("TMP") > 0
       		TMP->(dbCloseArea())
       	Endif

       	IF Select("TRB") > 0
       		TRB->(dbCloseArea())
       	Endif

        cQuery := "SELECT ZBH_LOTE, ZBH_DTLOTE, SUM(ZBH_QTDPRO) AS ZBH_QTDPRO, SUM(ZBH_QTDSEP) AS ZBH_QTDSEP, ZBH_STATUS "
        cQuery += "FROM "+RetSqlName("ZBH")+" WHERE D_E_L_E_T_ = '' "
        cQuery += "AND ZBH_FILIAL = '"+xFilial("ZBH")+"' "
        cQuery += "AND ZBH_DTLOTE BETWEEN '"+DtoS(aRet[1])+"' AND '"+DtoS(aRet[2])+"' "

        If !Empty(aRet[3])
			cQuery += " AND ZBH_LOTE = '"+Alltrim(aRet[3])+"' "
		EndIf

        cQuery += "GROUP BY ZBH_FILIAL,ZBH_LOTE,ZBH_DTLOTE,ZBH_STATUS "
        cQuery += "ORDER BY ZBH_DTLOTE, ZBH_LOTE "
	    
		MemoWrite("HESTA005.txt",cQuery)
		
		cQuery := ChangeQuery(cQuery)
		
		
		If Select("TMP") > 0
			TMP->(DbCloseArea())
		EndIf
		
		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)
		
		//Estrutura da tabela temporaria
        AADD(_astru,{"ZBH_DTLOTE"   ,TAMSX3("ZBH_DTLOTE"  )[3],TAMSX3("ZBH_DTLOTE"  )[1],TAMSX3("ZBH_DTLOTE"  )[2]})
		AADD(_astru,{"ZBH_LOTE"     ,TAMSX3("ZBH_LOTE")[3],TAMSX3("ZBH_LOTE")[1],TAMSX3("ZBH_LOTE")[2]})
		AADD(_astru,{"ZBH_QTDPRO"   ,TAMSX3("ZBH_QTDPRO")[3],TAMSX3("ZBH_QTDPRO")[1],0                })
		AADD(_astru,{"ZBH_QTDSEP"   ,TAMSX3("ZBH_QTDSEP")[3],TAMSX3("ZBH_QTDSEP")[1],0                })
		AADD(_astru,{"ZBH_STATUS"   ,"C"                    ,20                     ,0                })
		
		cArqTrab  := CriaTrab(_astru,.T.)
		dbUseArea(.T.,,cArqTrab,"TRB")//, .F., .F. )
		
		//Atribui a tabela temporaria ao alias TRB
		Dbselectarea("TMP")
		TMP->(DbGoTop())
		If TMP->(!EOF())
		    lRet := .T.
			While TMP->(!EOF())
				DbSelectArea("TRB")        
				RecLock("TRB",.T.) 
				TRB->ZBH_LOTE   := TMP->ZBH_LOTE
				TRB->ZBH_DTLOTE := StoD(TMP->ZBH_DTLOTE)
				TRB->ZBH_QTDPRO := TMP->ZBH_QTDPRO
				TRB->ZBH_QTDSEP := TMP->ZBH_QTDSEP
				If TMP->ZBH_STATUS = "T"
					TRB->ZBH_STATUS := "T - Transferido"
				ElseIf TMP->ZBH_STATUS = "N"
					TRB->ZBH_STATUS :=	 "N - N�o Iniciado"
				ElseIf TMP->ZBH_STATUS = "A"
					TRB->ZBH_STATUS :=	 "A - Em andamento"
				EndIf
				MsUnlock()        
				TMP->(DbSkip())
			EndDo
		EndIf

Return lRet


//--------------------------------------------------------------
/*/{Protheus.doc} IncluiLote
Incluir Lote de Separa��o

@param xParam Parameter Description
@return xRet Return Description
@author e - Ronaldo Parreira
@since 15/10/2021
/*/
//--------------------------------------------------------------
User Function IncluiLote()
Local oButton1
Local oButton2
Local oButton3
Local oSay1
Local oSay2

Private oGet1
private cGeOP := SPACE(6)

Static oDlg

  DEFINE MSDIALOG oDlg TITLE "INCLUIR LOTE" FROM 000, 000  TO 500, 1000 COLORS 0, 16777215 PIXEL //Style DS_MODALFRAME  // Cria Dialog sem o bot�o de Fechar.

    @ 010, 010 SAY oSay1 PROMPT "Lote Separa��o Tecido - INCLUIR" SIZE 168, 009 OF oDlg COLORS 16711680, 16777215 FONT oFont20n PIXEL
    @ 031, 009 SAY oSay2 PROMPT "Apontar OP:" SIZE 030, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 029, 040 MSGET oGet1 VAR cGeOP SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 028, 104 BUTTON oButton1 PROMPT "Incluir" SIZE 037, 012 OF oDlg ACTION Iif(cGeOP="",oGet1:setfocus(),fMSNewGe1(cGeOP)) PIXEL
    fMSNewGe1(cGeOP)
    @ 028, 438 BUTTON oButton2 PROMPT "Gerar LOTE" SIZE 050, 012 OF oDlg aCTION LoteSep() PIXEL
    @ 234, 453 BUTTON oButton3 PROMPT "Sair" SIZE 037, 012 OF oDlg ACTION SairLote() PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------ 
Static Function fMSNewGe1(cOP)
//------------------------------------------------ 
Local nX
Local aHeaderEx := {}
Local aFieldFill := {}
Local aFields := {"ZBH_DTEMP","ZBH_GRUPO","ZBH_OP","ZBH_QTDOP","ZBH_CODOP"}
Local aFields2 := {"Data Empenho","Grupo","OP","Qtde OP","Refer�ncia OP"}
Local aAlterFields := {}
Static oMSNewGe1

// Define field properties
  DbSelectArea("SX3")
  SX3->(DbSetOrder(2))
  For nX := 1 to Len(aFields)
    If SX3->(DbSeek(aFields[nX]))
      Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
                       SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
    Endif
  Next nX

  // Define field values
  If len(aColsEx) = 0
	For nX := 1 to Len(aFields)
		If DbSeek(aFields[nX])
		Aadd(aFieldFill, "")
		Endif
	Next nX

  EndIf	 

   If !Empty(cOP) 
		
        cQry := " SELECT T.DATA_EMP, "
		cQry += "		T.GRUPO, "
		cQry += " 		T.OP, "
		cQry += " 		T.QTDE_OP, "
		cQry += " 		(SELECT RTRIM(B4.B4_COD) +' - '+ B4.B4_DESC "
		cQry += " 			FROM "+RetSqlName("SC2")+" AS C2 WITH (NOLOCK) "
		cQry += " 			INNER JOIN "+RetSqlName("SB4")+" AS B4 WITH (NOLOCK) ON (B4.B4_COD = LEFT(C2.C2_PRODUTO, 8) AND B4.D_E_L_E_T_='') "
		cQry += " 			WHERE C2.C2_NUM = T.OP "
		cQry += " 			AND C2.C2_ITEM = '01' "
		cQry += " 			AND C2.C2_SEQUEN = '001' "
		cQry += " 			AND C2.D_E_L_E_T_ = '' "
		cQry += " 			GROUP BY B4.B4_COD,B4.B4_DESC) AS REFERENCIA_OP "
		cQry += " 	FROM "
		cQry += " 	(SELECT D4.D4_DATA AS DATA_EMP, "
		cQry += " 		    B1.B1_GRUPO +' - '+ BM.BM_DESC AS GRUPO, "
		cQry += " 		    D4_XOPP AS OP, "
		cQry += " 		    SUM(D4.D4_QTDEORI) AS QTDE_OP "
		cQry += " 	FROM "+RetSqlName("SD4")+" AS D4 WITH (NOLOCK) "
		cQry += " 	INNER JOIN "+RetSqlName("SB1")+" AS B1 WITH (NOLOCK) ON (B1.B1_COD = D4.D4_COD "
		cQry += " 												AND B1.D_E_L_E_T_='') "
		cQry += " 	INNER JOIN "+RetSqlName("SBM")+" AS BM WITH (NOLOCK) ON (BM.BM_GRUPO = B1.B1_GRUPO "
		cQry += " 												AND BM.D_E_L_E_T_ = '') "
		cQry += " 	WHERE D4.D_E_L_E_T_ = '' "
		cQry += " 		AND B1.B1_GRUPO IN ('MP01','MP02') "
		cQry += " 		AND D4_XOPP <> ''"
		cQry += "       AND D4_QUANT > 0 "
		cQry += " 		AND D4_XOPP = '"+cOP+"' "
		cQry += " 	GROUP BY D4.D4_FILIAL, "
		cQry += " 				D4.D4_DATA, "
		cQry += " 				D4_XOPP, "
		cQry += " 				B1.B1_GRUPO, "
		cQry += " 				BM.BM_DESC "
		cQry += " 	) T "
    	
	    If Select("TMPRD") > 0
	        TMPRD->(DbCloseArea())
	    EndIf
	
	    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPRD",.T.,.T.)
	    DbSelectArea("TMPRD")
	    DbGoTop()

        IF TMPRD->(!EOF()) 
			DbselectArea("ZBH")
			DbSetOrder(2)
			IF DbSeek(xfilial("ZBH")+TMPRD->OP)		    
				MessageBox("OP j� incluida no lote: "+ZBH->ZBH_LOTE,"Aten��o!",16)
				oGet1:setfocus()
			Else
				aAdd(aColsEx,{StoD(TMPRD->DATA_EMP),TMPRD->GRUPO,TMPRD->OP,TMPRD->QTDE_OP,TMPRD->REFERENCIA_OP,.F.})  
				cGeOP := SPACE(6)
				oGet1:setfocus()				
			EndIf
		Else
			MessageBox("OP n�o liberada ou j� Transferida!","Aten��o!",16)
			oGet1:setfocus()
		EndIf	
       	
    EndIf

   oMSNewGe1 := MsNewGetDados():New( 048, 006, 232, 495, GD_INSERT+GD_DELETE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)  
  
Return


// FUN��O CHAMADA AO SAIR DA ROTINA DE INCLUIR LOTE.
Static Function SairLote()
	
	nRet3 := MessageBox("Deseja realmente sair?","Lote Separa��o",4)
	If nRet3 == 6
		aColsEx := {}
		oDlg:End()
	Else
		MessageBox("Cancelado pelo usu�rio","Aten��o",16)
	EndIf

Return 


//--------------------------------------------------------------
/*/{Protheus.doc} LoteSep
Seleciona as OPs para gerar um Lote de Separa��o de Tecidos

@param xParam Parameter Description
@return xRet Return Description
@author e - Ronaldo Parreira
@since 11/11/2021
/*/
//--------------------------------------------------------------
Static Function LoteSep()
Local nx   := 0
Local cOP  := ""
Local cOPs := ""

	If Len(aColsEx) > 0 //OP
		nRet3 := MessageBox("Confirma grava��o do Lote?","Confirma��o",4)
		If nRet3 == 6 //Confirma��o

			For nx := 1 to Len(aColsEx)
				cOP  := aColsEx[nx][3]
				cOPs += "'"+cOP+"',"
            Next

			cOPs := SubStr(cOPs,1,Len(cOPs)-1)

			Processa( {|| lGera := GeraLote(cOPs) }, "Aguarde...", "Processando...",.F.)

		EndIf
	Else
		MessageBox("Selecione uma OP!","Aten��o",16)	
	EndIf

Return

//--------------------------------------------------------------
/*/{Protheus.doc} GeraLote
Gerar um Lote de Separa��o de Tecidos

@param xParam Parameter Description
@return xRet Return Description
@author e - Ronaldo Parreira
@since 11/11/2021
/*/
//--------------------------------------------------------------
Static Function GeraLote(cOPs)
Local cProxLot  := ""
Private cLoteSep := ""

    cLoteSep := AllTrim(GetMV("HP_XLOTSEP")) //Numero do LOTE DE SEPARA��O
	cProxLot := SOMA1(cLoteSep,6)
	PutMV("HP_XLOTSEP",cProxLot)

	cQryLote := "SELECT T.FILIAL, "
	cQryLote +=	"T.PRODUTO, "
	cQryLote +=	"	T.DESCRICAO, "
	cQryLote +=	"	T.GRUPO, "
	cQryLote +=	"	T.UNI, "
	cQryLote +=	"	T.QTDE_PROD, "
	cQryLote +=	"	(T.QTDE_PROD * 1.05) AS QTD_PERC_PRO, "
	cQryLote +=	"	T.DATA_EMP, "
	cQryLote +=	"	T.OP, "
	cQryLote +=	"	(SELECT RTRIM(B4.B4_COD) +' - '+ B4.B4_DESC "
	cQryLote +=	"		FROM "+RetSqlName("SC2")+" AS C2 WITH (NOLOCK) "
	cQryLote +=	"		INNER JOIN "+RetSqlName("SB4")+" AS B4 WITH (NOLOCK) ON (B4.B4_COD = LEFT(C2.C2_PRODUTO, 8) AND B4.D_E_L_E_T_='') "
	cQryLote +=	"		WHERE C2.C2_NUM = T.OP "
	cQryLote +=	"		AND C2.C2_ITEM = '01' "
	cQryLote +=	"		AND C2.C2_SEQUEN = '001' "
	cQryLote +=	"		AND C2.D_E_L_E_T_ = '' "
	cQryLote +=	"		GROUP BY B4.B4_COD,B4.B4_DESC) AS REFERENCIA_OP "
	cQryLote +=	"FROM "
	cQryLote +=	"(SELECT D4.D4_FILIAL AS FILIAL, "
	cQryLote +=	"		D4.D4_DATA  AS DATA_EMP, "
	cQryLote +=	"		D4.D4_COD AS PRODUTO, "
	cQryLote +=	"		B1.B1_DESC AS DESCRICAO, "
	cQryLote +=	"		B1.B1_GRUPO +' - '+ BM.BM_DESC AS GRUPO, "
	cQryLote +=	"		B1.B1_UM AS UNI, "
	cQryLote +=	"		D4_XOPP AS OP, "
	cQryLote +=	"		SUM(D4.D4_QTDEORI) AS QTDE_PROD "
	cQryLote +=	"FROM "+RetSqlName("SD4")+" AS D4 WITH (NOLOCK) "
	cQryLote +=	"INNER JOIN "+RetSqlName("SB1")+" AS B1 WITH (NOLOCK) ON (B1.B1_COD = D4.D4_COD "
	cQryLote +=	"											         AND B1.D_E_L_E_T_='') "
	cQryLote +=	"INNER JOIN "+RetSqlName("SBM")+" AS BM WITH (NOLOCK) ON (BM.BM_GRUPO = B1.B1_GRUPO "
	cQryLote +=	"											         AND BM.D_E_L_E_T_ = '') "
	cQryLote +=	"WHERE D4.D_E_L_E_T_ = '' "
	cQryLote +=	"	AND B1.B1_GRUPO IN ('MP01','MP02') "
	cQryLote +=	"	AND D4_XOPP <> '' "
	cQryLote +=	"	AND D4_XOPP IN ("+cOPs+") "
	cQryLote +=	"GROUP BY D4.D4_FILIAL, "
	cQryLote +=	"			D4.D4_DATA, "
	cQryLote +=	"			D4.D4_COD, "
	cQryLote +=	"			B1.B1_DESC, "
	cQryLote +=	"			D4_XOPP, "
	cQryLote +=	"			B1.B1_GRUPO, "
	cQryLote +=	"			B1.B1_UM, "
	cQryLote +=	"			BM.BM_DESC "
	cQryLote +=	") T "
	cQryLote +=	"ORDER BY T.PRODUTO,T.OP "

	If Select("TMLOTE") > 0
	        TMLOTE->(DbCloseArea())
	EndIf
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrylote),"TMLOTE",.T.,.T.)
	DbSelectArea("TMLOTE")
	DbGoTop()

	While TMLOTE->(!EOF())
		DbSelectArea("ZBH")
		RecLock("ZBH",.T.)	
		ZBH->ZBH_FILIAL  := xFilial("ZBH")
		ZBH->ZBH_DTLOTE  := DDATABASE
		ZBH->ZBH_LOTE    := cLoteSep
		ZBH->ZBH_CODPRO  := TMLOTE->PRODUTO
		ZBH->ZBH_DESPRO  := AllTrim(TMLOTE->DESCRICAO)
		ZBH->ZBH_GRUPO   := TMLOTE->GRUPO
		ZBH->ZBH_UNI     := TMLOTE->UNI
		ZBH->ZBH_QTDPRO  := TMLOTE->QTDE_PROD
		ZBH->ZBH_QTPERC  := TMLOTE->QTD_PERC_PRO
		ZBH->ZBH_DTEMP   := StoD(TMLOTE->DATA_EMP)
		ZBH->ZBH_OP      := TMLOTE->OP
		ZBH->ZBH_CODOP   := AllTrim(TMLOTE->REFERENCIA_OP)
		ZBH->ZBH_STATUS  := "N"
		MsUnlock()
		TMLOTE->(DbSkip())
	EndDo
    
	MessageBox("Lote Separa��o: "+cLoteSep+" gerado com sucesso!","Aviso",64)
	aColsEx := {}
	oDlg:End()

Return

//--------------------------------------------------------------
/*/{Protheus.doc} VisuLoteOP
Description

@param xParam Parameter Description
@return xRet Return Description
@author e - Ronaldo Parreira
@since 11/11/2021
/*/                                                             
//--------------------------------------------------------------
User Function VisuLoteOP()
Local oButton1
Local oButton2
Local oSay1
Static oDlgLot

  DEFINE MSDIALOG oDlgLot TITLE "VISUALIZAR LOTE" FROM 000, 000  TO 500, 1000 COLORS 0, 16777215 PIXEL

    fMSVerLote()
    @ 014, 007 SAY oSay1 PROMPT "Lote Separa��o Tecido - VISUALIZAR" SIZE 198, 011 OF oDlgLot COLORS 16711680, 16777215 FONT oFont20n PIXEL
    @ 020, 441 BUTTON oButton1 PROMPT "Visualizar OPs" SIZE 047, 012 OF oDlgLot PIXEL
    @ 234, 454 BUTTON oButton2 PROMPT "Sair" SIZE 037, 012 OF oDlgLot ACTION oDlgLot:End() PIXEL

  ACTIVATE MSDIALOG oDlgLot CENTERED

Return

//------------------------------------------------
Static Function fMSVerLote()
//------------------------------------------------
Local nX
Local aHeaderEx := {}
Local aColsLot := {}
Local aFieldFill := {}
Local aFields := {"ZBH_CODPRO","ZBH_LOTE","ZBH_CODPRO","ZBH_UNI","ZBH_QTDPRO","ZBH_QTPERC","ZBH_QTDSEP","ZBH_STATUS","ZBH_GRUPO","ZBH_DESPRO"}
Local aFields2 := {"Legenda","Lote","Produto","Unidade","Qtde_Prod","Qtde_%","Qtde_Sep","Status","Grupo","Descri��o"}
Local aAlterFields := {}
Local cQrlyLt := ""
Private cLegenda  := ""
Static oMSVerLote

  // Define field properties
  DbSelectArea("SX3")
  SX3->(DbSetOrder(2))
  For nX := 1 to Len(aFields)
    If SX3->(DbSeek(aFields[nX]))
      Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
                       SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})			   
    Endif
  Next nX

  // Define field values
  For nX := 1 to Len(aFields)
    If DbSeek(aFields[nX])
      Aadd(aFieldFill, "")
    Endif
  Next nX
  Aadd(aFieldFill, .F.)
  Aadd(aColsLot, aFieldFill)

	
    cQrlyLt := "SELECT ZBH_LOTE, ZBH_GRUPO, ZBH_CODPRO, ZBH_UNI,
    cQrlyLt += "       SUM(ZBH_QTDPRO) AS ZBH_QTDPRO, 
    cQrlyLt += "	   SUM(ZBH_QTPERC) AS ZBH_QTPERC, 
    cQrlyLt += "	   SUM(ZBH_QTDSEP) AS ZBH_QTDSEP, 
    cQrlyLt += "	   ZBH_STATUS, ZBH_DESPRO	    
    cQrlyLt += "FROM "+RetSqlName("ZBH")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZBH_LOTE = '"+TRB->ZBH_LOTE+"'
    cQrlyLt += "GROUP BY ZBH_FILIAL,ZBH_LOTE,ZBH_GRUPO,ZBH_CODPRO,ZBH_DESPRO,ZBH_UNI,ZBH_STATUS
	
	If Select("TMPVIS") > 0
	        TMPVIS->(DbCloseArea())
	EndIf

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQrlyLt),"TMPVIS", .F., .T.)
	
	DbSelectArea("TMPVIS")
	
	If TMPVIS->(!EOF())
		While TMPVIS->(!EOF())	

			If TMPVIS->ZBH_STATUS = "T"
					cStatus  := "T - Transferido"
					cLegenda := "BR_VERMELHO"
				ElseIf TMPVIS->ZBH_STATUS = "N"
					cStatus  :=	 "N - N�o Iniciado"
					cLegenda := "BR_VERDE"
				ElseIf TMPVIS->ZBH_STATUS = "A"
					cStatus  :=	 "A - Em andamento"
					cLegenda := "BR_AMARELO"
			EndIf
		
			aAdd(aColsLot,{cLegenda,TMPVIS->ZBH_LOTE,TMPVIS->ZBH_CODPRO,TMPVIS->ZBH_UNI,TMPVIS->ZBH_QTDPRO,TMPVIS->ZBH_QTPERC,TMPVIS->ZBH_QTDSEP,cStatus,TMPVIS->ZBH_GRUPO,TMPVIS->ZBH_DESPRO,.F.})
			TMPVIS->(DbSkip())
		EndDo
	EndIf
	
	TMPVIS->(DbCloseArea())

  oMSVerLote := MsNewGetDados():New( 036, 003, 232, 496, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlgLot, aHeaderEx, aColsLot)

Return

//--------------------------------------------------------------
/*/{Protheus.doc} ConsLoteOP
Description: Consulta o lote de sepera��o gerado

@param xParam Parameter Description
@return xRet Return Description
@author e - Ronaldo Parreira
@since 11/11/2021
/*/                                                             
//--------------------------------------------------------------
Static Function ConsLoteOP()

Return





//------------------------------------------------
User Function LegendLote() // Legenda dos Lotes()
//------------------------------------------------
Local aCores := {}
                                                                   
//BR_AMARELO,BR_AZUL,BR_BRANCO,BR_CINZA,BR_LARANJA,BR_MARROM,BR_VERDE,BR_VERMELHO,BR_PINK,BR_PRETO,BR_VIOLETA

aAdd(aCores,{"BR_VERDE"   ,"Separa��o N�o Iniciada" })
aAdd(aCores,{"BR_AMARELO" ,"Separa��o em Andamento" })
aAdd(aCores,{"BR_VERMELHO","Separa��o Realizada" })

BrwLegenda("Lote de Separa��o","Legenda",aCores)

Return .T.



