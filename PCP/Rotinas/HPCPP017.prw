#INCLUDE "TOPCONN.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "MSOLE.CH"
#INCLUDE "rwmake.ch"
/*/{Protheus.doc} HPCPP017
Tela de impress�o de OP
@author Geyson Albano
@since  14/07/2021
@version P12
/*/

User Function HPCPP017()

	Local aStru     := {}
	Local cAliasQry := GetNextAlias()
	Local cOp		:= ""
	Local lOk	:= .F.
	Local aParam := {}
	Local aRetParm	:= {}
	Local aSeek	:= {}

	Private  cAliasTab := GetNextAlias()
	Private oBrowse     := Nil

	aAdd(aParam,{1,"Data de "  ,Ctod(Space(8)),"","","","",50,.F.})
	aAdd(aParam,{1,"Data at� " ,Ctod(Space(8)),"","","","",50,.F.})
	If __cUserID $ GetMv("HP_USERSTI")
		aAdd(aParam,{1,"OP " ,Space(6),"","","","",50,.F.})
	EndIf

	If ParamBox(aParam,"Filtro Impress�o OP ",@aRetParm,{||.T.},,,,,,"U_HPCPP016",.T.,.T.)
		lOk	:= .T.
		ddata1 := aRetParm[1]
		ddata2 := aRetParm[2]
		If __cUserID $ GetMv("HP_USERSTI") .AND. !Empty(Alltrim(aRetParm[3]))
			cOp		:=Alltrim(aRetParm[3])
		EndIf
	Else
		lOk := .F.
	EndIf

	If lOk

		_qry := "SELECT C2_NUM, LEFT(C2_PRODUTO,8) AS PRODUTO, B4_DESC, Substring(C2_PRODUTO,9,3) AS COR, BV_DESCRI,SC2.C2_OBS FROM "+RetSqlName("SC2")+" SC2 WITH (NOLOCK) "
		_qry += "LEFT JOIN "+RetSqlName("SB4")+" SB4 WITH (NOLOCK) on SB4.D_E_L_E_T_ = '' AND B4_COD = LEFT(C2_PRODUTO,8) "
		_qry += "LEFT JOIN "+RetSqlName("SBV")+" SBV WITH (NOLOCK) on SBV.D_E_L_E_T_ = '' AND BV_TABELA = B4_LINHA AND BV_CHAVE = SUBSTRING(C2_PRODUTO,9,3) "
		_qry += "where SC2.D_E_L_E_T_ = '' AND C2_TPOP = 'F'  AND C2_XRES <> ''   AND  C2_SEQUEN  = '001'"

		If !Empty(ddata1)
			_qry += " AND  C2_EMISSAO BETWEEN '"+ DTOS(ddata1)+"' AND '"+ DTOS(ddata2)+"' "
		EndIf
		If !Empty(cOp)
			_qry += " AND  C2_NUM = '"+cOp+"' "
		EndIf
		_qry += "group by C2_NUM, LEFT(C2_PRODUTO,8) , B4_DESC, Substring(C2_PRODUTO,9,3), BV_DESCRI,SC2.C2_OBS "
		_qry += "order by C2_NUM "


		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,_qry),cAliasQry, .F., .T.)

//Estrutura da tabela temporaria
		AADD(aStru,{"C2_OK"     ,"C", 2,0})
		AADD(aStru,{"C2_NUM"    ,"C", 6,0})
		AADD(aStru,{"C2_PRODUTO","C", 8,0})
		AADD(aStru,{"B4_DESC"   ,"C", 120,0})
		AADD(aStru,{"COR"       ,"C", 3,0})
		AADD(aStru,{"BV_DESCRI" ,"C", 55,0})
		AADD(aStru,{"C2_OBS" 	,"C", 30,0})

		If SELECT(cAliasTab) > 0
			cAliasTab->(dbCloseArea())
		Endif

		cArqTrab  := CriaTrab(aStru,.T.)

		//Criar indices
		cIndice1 := Alltrim(CriaTrab(,.F.))
		cIndice2 := cIndice1
		cIndice3 := cIndice1
		cIndice4 := cIndice1

		cIndice1 := Left(cIndice1,5) + Right(cIndice1,2) + "A"
		/*
		cIndice2 := Left(cIndice2,5) + Right(cIndice2,2) + "B"
		cIndice3 := Left(cIndice3,5) + Right(cIndice3,2) + "C"
		cIndice4 := Left(cIndice4,5) + Right(cIndice4,2) + "D"
		*/

		//Se indice existir excluir
		If File(cIndice1+OrdBagExt())
			FErase(cIndice1+OrdBagExt())
		EndIf
		/*
		If File(cIndice2+OrdBagExt())
			FErase(cIndice2+OrdBagExt())
		EndIf
		If File(cIndice3+OrdBagExt())
			FErase(cIndice3+OrdBagExt())
		EndIf
		If File(cIndice4+OrdBagExt())
			FErase(cIndice4+OrdBagExt())
		EndIf
		*/

		//A fun��o dbUseArea abre uma tabela de dados na �rea de trabalho atual ou na primeira �rea de trabalho dispon�vel
		dbUseArea(.T.,,cArqTrab,cAliasTab,Nil,.F.)

		IndRegua(cAliasTab, cIndice1, "C2_NUM"    ,,, "Indice Nome...")
		/*
		IndRegua(cAliasTab, cIndice2, "C2_PRODUTO",,, "Indice Login...")
		IndRegua(cAliasTab, cIndice3, "COR",,, "Indice E-mail...")
		IndRegua(cAliasTab, cIndice4, "C2_OBS"    ,,, "Indice ID...")
		*/
		//Fecha todos os �ndices da �rea de trabalho corrente.
		dbClearIndex()
		//Acrescenta uma ou mais ordens de determinado �ndice de ordens ativas da �rea de trabalho.
		dbSetIndex(cIndice1+OrdBagExt())
		/*
		dbSetIndex(cIndice2+OrdBagExt())
		dbSetIndex(cIndice3+OrdBagExt())
		dbSetIndex(cIndice4+OrdBagExt())
		*/
		//Popular tabela tempor�ria
		Dbselectarea(cAliasQry)
		(cAliasQry)->(DbGoTop())

		While (cAliasQry)->(!EOF())
			DbSelectArea(cAliasTab)
			RecLock(cAliasTab,.T.)
			(cAliasTab)->C2_NUM        := (cAliasQry)->C2_NUM
			(cAliasTab)->C2_PRODUTO    := (cAliasQry)->PRODUTO
			(cAliasTab)->B4_DESC       := (cAliasQry)->B4_DESC
			(cAliasTab)->COR           := (cAliasQry)->COR
			(cAliasTab)->BV_DESCRI     := (cAliasQry)->BV_DESCRI
			(cAliasTab)->C2_OBS    	   := (cAliasQry)->C2_OBS

			MsUnlock()
			(cAliasQry)->(DbSkip())

		EndDo

		//Irei criar a pesquisa que ser� apresentada na tela
		aAdd(aSeek,{"Num OP"    ,{{"","C",006,0,"C2_NUM"    ,"@!"}} } )
		/*
		aAdd(aSeek,{"Login"    ,{{"","C",006,0,"Login"    ,"@!"}} } )
		aAdd(aSeek,{"E-mail",{{"","C",100,0,"E-mail",""}} } )
		aAdd(aSeek,{"ID"    ,{{"","C",006,0,"ID"    ,"@!"}} } )
		*/
//Criando o MarkBrow
		oBrowse := FWMarkBrowse():New()
		oBrowse:SetAlias(cAliasTab)
	//	oBrowse:oBrowse:SetDBFFilter(.T.)
	//	oBrowse:oBrowse:SetUseFilter(.T.) //Habilita a utiliza��o do filtro no Browse
	//	oBrowse:oBrowse:SetFixedBrowse(.T.)
		oBrowse:SetWalkThru(.F.) //Habilita a utiliza��o da funcionalidade Walk-Thru no Browse
		oBrowse:SetAmbiente(.T.) //Habilita a utiliza��o da funcionalidade Ambiente no Browse
		oBrowse:SetTemporary() //Indica que o Browse utiliza tabela tempor�ria
		oBrowse:oBrowse:SetSeek(.T.,aSeek) //Habilita a utiliza��o da pesquisa de registros no Browse
		oBrowse:oBrowse:SetFilterDefault("") //Indica o filtro padr�o do Browse

//define AS colunas para o browse
		aColunas := {;
			{"Ordem de Prod"  ,"C2_NUM"    ,"C", 6,0,   "@!"},;
			{"Produto"        ,"C2_PRODUTO","C", 8,0,   "@!"},;
			{"Descri��o"      ,"B4_DESC"   ,"C", 120,0, "@!"},;
			{"Cor"            ,"COR","C"   , 3,0    ,   "@!"},;
			{"Desc. Cor"      ,"BV_DESCRI"   ,"C", 55,0,"@!"},;
			{"OBS"    		  ,"C2_OBS"   	 ,"C", 30,0,"@!"}}

//Seta AS colunas para o browse
		oBrowse:SetFields(aColunas)

//Setando sem�foro, descri��o e campo de mark
		oBrowse:SetSemaphore(.T.)
		oBrowse:SetDescription('Impress�o de OPs')
		oBrowse:SetFieldMark('C2_OK')


		oBrowse:AddButton("Imprimir OPs selecionadas","U_HRPCP002(oBrowse)",,2,0)
		oBrowse:AddButton("Marca/Descmarca Todos","U_MarkOP(oBrowse)",,2,0)
		oBrowse:AddButton("Estorna","u_ESTOP()",,2,0)

//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
		oBrowse:SetAllMark({ || MarkRec(),oBrowse:Refresh(.T.)})
		oBrowse:SetDoubleClick({|| Iif(Empty(Alltrim((cAliasTab)->C2_OK)),oBrowse:Mark(),"  ")  })
		//	oBrowse:SetTemporary()

//Ativando a janela
		oBrowse:Activate()
		oBrowse:oBrowse:Setfocus()
	Else
		MsgAlert("Cancelado pelo usuario","HOPE")
		Return .F.
	EndIf


Return Nil

/*/{Protheus.doc} MarkOP
Programa criado para marcar e desmarcar todos os registros
@author Geyson Albano
@since  14/07/2021
@version P12
/*/

User Function MarkOP(oBrowse)

	Local cMarca:= oBrowse:Mark()
	Local aAreaTRB:= (cAliasTab)->(GetArea())

	dbSelectArea(cAliasTab)
	(cAliasTab)->(dbGoTop())
	cMarca:= Iif(Empty(Alltrim((cAliasTab)->C2_OK)),oBrowse:Mark(),"  ")
	While !(cAliasTab)->(Eof())
		RecLock((cAliasTab),.F.)
		(cAliasTab)->C2_OK:= cMarca
		MsUnlock()
		(cAliasTab)->(dbSkip())
	EndDo

	RestArea( aAreaTRB )

Return

User Function ESTOP()

	Pergunte("HPCP014",.T.)

	cTpLib  := AllTrim(SuperGetMV("MV_XTPNLB",.F.,"MI|PI")) // Tipo de Produtos que n�o devem ser considerados na libera��o da OP
	cGrpLib := AllTrim(SuperGetMV("MV_XGRPNLB",.F.,"MP05|MP16|MP17|MP91")) //Grupo de Produtos que n�o devem ser considerados para libera��o de OP
	_op := MV_PAR01

	DbSelectArea("SC2")
	DbSetOrder(1)
	DbSeek(xfilial("SC2")+_op)

	If SC2->C2_XRES == "T"
		If !MsgYesNo("Esta OP j� foi Transferida. A Tranfer�ncia j� foi estornada?","OP j� transferida")
			Return()
		EndIf
	EndIf

	DbSelectArea("SD4")
	DbSetOrder(2)
	DbSeek(xfilial("SD4")+_op)
	_ok := .T.
	_cLocal := "A1"

	While !EOF() .and. SubStr(SD4->D4_OP,1,6) == SubStr(_op,1,6)
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SD4->D4_COD)

		If !(SB1->B1_TIPO $ (cTpLib)) .and. SubStr(SD4->D4_COD,1,1) <> "B" .AND. !(SB1->B1_GRUPO $ (cGrpLib))
			DbSelectArea("SB2")
			DbSetOrder(1)
			DbSeek(xfilial("SB2")+SD4->D4_COD+_cLocal)

			If Found()
				RecLock("SB2",.F.)
				Replace B2_XRES	with B2_XRES - SD4->D4_XRES
				MsUnLock()

				RecLock("SD4",.F.)
				Replace D4_XRES with 0
				MsUnLock()
			ElseIf Empty(SD4->D4_XDTLIB) .AND. SD4->D4_XRES == 0
				_ok := .T.
			Else
				_ok := .F.
			EndIf
		EndIf

		DbSelectArea("SD4")
		DbSkip()
	End

	If _ok
		_qry := "update "+RetSqlName("SC2")+" set C2_XRES = '' where D_E_L_E_T_ = '' and C2_NUM = '"+SubStr(_op,1,6)+"' "
		TcSqlExec(_qry)

		_qry1 := " update " +RetSqlName("SD4")+ " set D4_XDTLIB = '' where D_E_L_E_T_ = '' and LEFT(D4_OP,6) = '"+SubStr(_op,1,6)+"' "
		TcSqlExec(_qry1)

		MsgBox("Estorno realizado com sucesso!","Aviso","INFO")
	Else
		Alert("N�o foi poss�vel realizar o estorno!")
	EndIf
Return
