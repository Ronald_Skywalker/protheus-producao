#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL006 
Relatório de Logs
@author Weskley Silva
@since 01/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL007()

Private oReport

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 01 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'LOGS', 'RELATORIO LOGS ', , {|oReport| ReportPrint( oReport ), 'RELATORIO LOGS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATORIO LOGS', { 'LOGS', 'SHG'})
			
	TRCell():New( oSection1, 'FILIAL'	    	    ,'LOG', 		'FILIAL',							"@!"                        ,06)
	TRCell():New( oSection1, 'SEQ'      	        ,'LOG', 		'SEQ',	    						"@!"				        ,08)
	TRCell():New( oSection1, 'COD_PRODUTO'          ,'LOG', 		'COD_PRODUTO',              		"@!"						,20)
	TRCell():New( oSection1, 'COD_LOG'         		,'LOG', 		'COD_LOG',	              			"@!"						,03)
	TRCell():New( oSection1, 'LOG_MRP' 				,'LOG', 		'LOG_MRP' ,	        				"@!"		                ,100)
	TRCell():New( oSection1, 'DOC'		       		,'LOG', 		'DOC',          					"@!"				        ,25)
	TRCell():New( oSection1, 'ITEM' 				,'LOG', 		'ITEM' ,	    					"@!"				        ,03)
	TRCell():New( oSection1, 'ALIAS'	        	,'LOG', 		'ALIAS' ,		       				"@!"		    	        ,03)
	TRCell():New( oSection1, 'COD_ORI'			    ,'LOG', 		'COD_ORI',			   				"@!"						,20)
	
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 01 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("LOG1") > 0
		LOG1->(dbCloseArea())
	Endif
      
	
	cQuery := " SELECT HG_FILIAL AS FILIAL, " 
	cQuery += " HG_SEQMRP AS SEQ," 
	cQuery += " HG_COD AS COD_PRODUTO," 
	cQuery += " HG_CODLOG AS COD_LOG , "
	cQuery += " HG_LOGMRP AS LOG_MRP, "
	cQuery += " HG_DOC AS DOC,"
	cQuery += " HG_ITEM AS ITEM,"
	cQuery += " HG_ALIAS AS ALIAS," 
	cQuery += " HG_CODORI AS COD_ORI " 
	cQuery += " FROM "+RetSqlName('SHG')+"  WHERE HG_SEQMRP = (SELECT MAX(HG_SEQMRP) from "+RetSqlName('SHG')+ " )"
	
	
	TCQUERY cQuery NEW ALIAS LOG1

	While LOG1->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL"):SetValue(LOG1->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")

		oSection1:Cell("SEQ"):SetValue(LOG1->SEQ)
		oSection1:Cell("SEQ"):SetAlign("LEFT")
		
		oSection1:Cell("COD_PRODUTO"):SetValue(LOG1->COD_PRODUTO)
		oSection1:Cell("COD_PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_LOG"):SetValue(LOG1->COD_LOG)
		oSection1:Cell("COD_LOG"):SetAlign("LEFT")
		
		oSection1:Cell("LOG_MRP"):SetValue(LOG1->LOG_MRP)
		oSection1:Cell("LOG_MRP"):SetAlign("LEFT")
			
		oSection1:Cell("DOC"):SetValue(LOG1->DOC)
		oSection1:Cell("DOC"):SetAlign("LEFT")
				
		oSection1:Cell("ITEM"):SetValue(LOG1->ITEM)
		oSection1:Cell("ITEM"):SetAlign("LEFT")
				
		oSection1:Cell("ALIAS"):SetValue(LOG1->ALIAS)
		oSection1:Cell("ALIAS"):SetAlign("LEFT")
		
		oSection1:Cell("COD_ORI"):SetValue(LOG1->COD_ORI)
		oSection1:Cell("COD_ORI"):SetAlign("LEFT")
						
									
		oSection1:PrintLine()
		
		LOG1->(DBSKIP()) 
	enddo
	LOG1->(DBCLOSEAREA())
Return( Nil )