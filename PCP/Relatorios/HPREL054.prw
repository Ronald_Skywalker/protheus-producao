//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio Mat�ria Prima Exclusiva.
Rotina para gerar mat�ria prima exclusiva;

@author Ronaldo Pereira
@since 09 de Julho de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL054()

	Local aRet := {}
	Local aPerg := {}
	 
	aAdd(aPerg,{2,"Filtrar por: ","",{"SubCole��o", "Cor" },60,"",.T.})
	aAdd(aPerg,{1,"Quando for: " 		,Space(30),""   ,""		,""	    ,""		,0	,.F.}) 
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'MP Exclusiva'+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Base"
    Local cTable1    	:= "Mat�ria Prima Exclusiva" 

        
    //Dados tabela 1          
    cQry := " SELECT * FROM V_ESTRUTURA WITH (NOLOCK)
    cQry += " WHERE TIPO_COMP NOT IN ('BN')
    cQry += " AND COD_COMP NOT IN (
    cQry += " SELECT COD_COMP FROM V_ESTRUTURA WITH (NOLOCK) WHERE TIPO_PAI IN ('PF','KT','ME'))

    If aRet[1] == 'SubCole��o'    
    	cQry += " AND SUBCOLECAO = '"+Alltrim(aRet[2])+"' "
	ElseIf aRet[1] == 'Cor'
    	cQry += " AND SUBSTRING(CODIGO,9,3)= '"+Alltrim(aRet[2])+"' "
	Endif

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CODIGO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCRI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SITUACAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SUBCOLECAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COLECAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CICLO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MARCA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"GRUPO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_PAI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESC_PAI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO_PAI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"GRUPO_PAI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_COMP",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESC_COMP",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO_COMP",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"GRUPO_COMP",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTD",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PERDA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"UM_COMP",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_INI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_FIM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NIVEL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"REVISAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"INDICE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"LOTE_ECON",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MULTIPLO",1)
        
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;                                             
                                               CODIGO,;
                                               DESCRI,;
                                               TIPO,;
                                               SITUACAO,;
                                               SUBCOLECAO,;
                                               COLECAO,;
                                               CICLO,;
                                               MARCA,;
                                               GRUPO,;
                                               COD_PAI,;
                                               DESC_PAI,;
                                               TIPO_PAI,;
                                               GRUPO_PAI,;
                                               COD_COMP,;
                                               DESC_COMP,;
                                               TIPO_COMP,;
                                               GRUPO_COMP,;
                                               QTD,;
                                               PERDA,;
                                               UM_COMP,;
                                               DT_INI,;
                                               DT_FIM,;
                                               NIVEL,;
                                               REVISAO,;
                                               INDICE,;
                                               LOTE_ECON,;
                                               MULTIPLO;                                               
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return