#include "topconn.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWPrintSetup.ch"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'
#INCLUDE "TBICONN.CH" 
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'       
#INCLUDE "RPTDEF.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPREL042  �Autor  �Weskley Silva      � Data �  07/01/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio de ordem de abastecimento. Via do Aviamento       ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

user function HPREL042(_cOP)
	
	Private cDesc1     := "Este programa tem como objetivo imprimir relatorio "
	Private cDesc2     := "de acordo com os parametros informados pelo usuario."
	Private cDesc3 	   := ""
	Private cString    := "SC2"
	Private cPerg      := PADR("HPREL042",10)
	Private Tamanho  := "G"
	Private aOrd	   := {}
	Private aReturn  := {"Zebrado",1,"Administracao", 2, 2, 1, "",0 }
	Private wnrel    := "HPREL041"
	Private titulo   := "ORDEM DE CORTE REQUISICAO DE MATERIA PRIMA"
	Private nLin 	   := 100
	Private nCol 	   := 60
	Private nPula    := 60
	Private nfim     := 3200
	Private imprp	   := .F.
	Private cOrdemServ := " "
	Private cObs := " "
	Private cProdPri := ""
	Private cProdCab := ""
	Private cCabec := ""
	Private cRolet := ""
	Private cAlca := ""

	Private oFont08  := TFont():New("Arial",,08,,.f.,,,,,.f.)
	Private oFont09  := TFont():New("Arial",,09,,.f.,,,,,.f.)
	Private oFont09n := TFont():New("Arial",,09,,.t.,,,,,.f.)
	Private oFont10  := TFont():New("Arial",,10,,.f.,,,,,.f.)
	Private oFont10n := TFont():New("Arial",,10,,.t.,,,,,.f.)
	Private oFont11  := TFont():New("Arial",,11,,.f.,,,,,.f.)
	Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)
	Private oFont14n := TFont():New("Arial",,14,,.t.,,,,,.f.)
	Private aParam := {}
	Private aRetParm := {}
	
	cOrdemServ	:= _cOP

	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf

	cQuery := " SELECT TOP 1 C2_NUM AS NUM , C2_OBS AS OBS  FROM "+RetSqlName("SC2")+" SC2 WHERE C2_NUM = '"+cOrdemServ+"' AND C2_TPOP = 'F' AND C2_XRES <> '' AND D_E_L_E_T_ = '' "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	DbSelectArea("TMP")
	TMP->(DbGoTop())
	
	if EMPTY(TMP->NUM)
		MsgAlert("Ordem de Produ��o n�o liberada! Favor liberar.","HOPE")
		RETURN 
	ELSE	
	
	cObs := TMP->OBS
		
	aAdd(aParam,{1,"N� Paginas",2 ,"",".T.",,".T.",40,.F.})
	
	If ParamBox(aParam,"Filtro",@aRetParm,{||.T.},,,,,,"U_HPCPP014",.F.,.F.)
		lOk	:= .T.
		nResp := aRetParm[1]
	else
		lOk := .F.
	endif	
	
	wnrel:=SetPrint(cString,"HPREL042",cPerg,@Titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.T.,Tamanho)
			
	If nLastKey == 27
		Return
	Endif

	oPrn 	 := TMSPrinter():New()
	
	Processa({||ImpAvi(cOrdemServ)}, titulo, "Imprimindo, aguarde...")
		
	If( aReturn[5] == 1 ) //1-Disco, 2-Impressora
		oPrn:Preview()
	Else
		oPrn:Setup()
		oPrn:Print({1,10},nResp)
	EndIf
	
	ENDIF
	MS_FLUSH() 
Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � ImpTec   � Autor �Weskley Silva         � Data � 07/01/19 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chamada do Relatorio                                       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      �                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpAvi(cOrdemServ)

	Local aArea := GetArea()
	Local _cOrdem		:= cOrdemServ
	Local cTipoMat	:= ""
	Local cProdPri := " "
	Local _i 		:= 0
	Local _x 		:= 0

	
	aColI1      := {080,900,1580,2300}
	
	IncProc()
	
	nLin	     := 100
	nCol	     := 50
	nPula       := 50
	nPosTit     := 60
	
	oPrn:StartPage()
		
	oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)

	MSBAR('CODE128',1,23.8,cOrdemServ,oPrn,.F.,,.T.,0.042,1.7,,,,.F.)
	
	nLin += nPula
	oPrn:Box(nLin+20,50,nLin+23,nfim-450)

	nLin += nPula

	DbSelectArea("SC2")
	DbSetOrder(1)
	DbSeek(xfilial("SC2")+_cOrdem)
		
	oPrn:Say(nLin,080,"ORDEM DE AVIAMENTOS N�:",oFont10,100)
	oPrn:Say(nLin-5,550,AllTrim(_cOrdem),oFont12n,100)
	
	cObsOs		:= AllTrim(SubStr(SC2->C2_XBOS,1,100))
	Cabec1b	:= SubStr(SC2->C2_PRODUTO,1,8) + " - " + AllTrim(POSICIONE("SB4",1,xFilial("SB4")+SubStr(SC2->C2_PRODUTO,1,8),"B4_DESC"))
				
	oPrn:Say(nLin,900,"PRODUTO:",oFont10,100)
	oPrn:Say(nLin,1130,Cabec1b,oFont10n,100)
		
	dData := Date()
		
	oPrn:Say(nLin,2250,"Data de Impress�o:",oFont10,100)
	oPrn:Say(nLin,2600,Dtos(dData),oFont10n,100)

	nLin += nPula
	oPrn:Box(nLin,50,nLin+3,nfim-450)
	nLin += nPula
		
	oPrn:Say(nLin,1300,"ORIENTACAO DA ORDEM:",oFont09n,100)
	oPrn:Say(nLin,2100,cObs,oFont10,100)
				
	nLin += nPula
					
	If Select("TMP3") > 0
			TMP3->(DbCloseArea())
		EndIf
		
		cQuery := " SELECT TOP 1 C2_PRODUTO AS PRODUTO FROM "+RetSqlName("SC2")+" WITH (NOLOCK)  "
		cQuery += " JOIN "+RetSqlName("SB1")+" WITH (NOLOCK) ON B1_COD = C2_PRODUTO AND SB1010.D_E_L_E_T_ = '' "
		cQuery += " WHERE C2_NUM = '"+AllTrim(_cOrdem)+"' AND SC2010.D_E_L_E_T_ = '' AND LEFT(C2_PRODUTO,4) = 'MIRO'  "
		cQuery += " GROUP BY C2_PRODUTO "
		
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP3",.T.,.T.)
		DbSelectArea("TMP3")
		TMP3->(DbGoTop())	
		
		
		 cRolet := LEFT(TMP3->PRODUTO,4)
		
		TMP3->(DBCLOSEAREA())	
		
		
		If Select("TMP2") > 0
			TMP2->(DbCloseArea())
		EndIf
		
		cQuery := " SELECT TOP 1 C2_PRODUTO AS PRODUTO FROM "+RetSqlName("SC2")+" WITH (NOLOCK)  "
		cQuery += " JOIN "+RetSqlName("SB1")+" WITH (NOLOCK) ON B1_COD = C2_PRODUTO AND SB1010.D_E_L_E_T_ = '' "
		cQuery += " WHERE C2_NUM = '"+AllTrim(_cOrdem)+"' AND SC2010.D_E_L_E_T_ = '' AND LEFT(C2_PRODUTO,4) = 'MIAL'  "
		cQuery += " GROUP BY C2_PRODUTO "
		
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP2",.T.,.T.)
		DbSelectArea("TMP2")
		TMP2->(DbGoTop())	
		
		
		 cAlca := LEFT(TMP2->PRODUTO,4)
		
		TMP2->(DBCLOSEAREA())	
		
		If !SC2->(Eof())
		
			oPrn:Say(nLin,aColI1[1],"QUANTIDADES SOLICITADAS POR GRADE E COR",oFont09n,100)
			oPrn:Say(nLin,aColI1[3],"QUANTIDADES PRODUZIDAS",oFont09n,100)
			nLin += nPula
			oPrn:Box(nLin,50,nLin+3,nfim)
			nLin += nPula
			
			aTaman	:= VERTAM(_cOrdem)
			aCores	:= VERCOR(_cOrdem)
			
			aColI1x1 := aColI1[2]
			aColI1x2 := aColI1[4]
			
			For _i := 1 to len(aTaman)
				oPrn:Say(nLin,aColI1x1,AllTrim(aTaman[_i,3]),oFont09n,100)
				aColI1x1 += 120
				oPrn:Say(nLin,aColI1x2,AllTrim(aTaman[_i,3]),oFont09n,100)
				aColI1x2 += 120
			Next _i
			nLin += nPula
	
			For _i := 1 to Len(aCores)
				oPrn:Say(nLin,aColI1[1],aCores[_i,1] +"-"+ SubStr(aCores[_i,2],1,35),oFont09,100)
				oPrn:Say(nLin,aColI1[3],aCores[_i,1] +"-"+ SubStr(aCores[_i,2],1,35),oFont09,100)

				aColI1x1 := aColI1[2]
				aColI1x2 := aColI1[4]
				For _x := 1 to len(aTaman)
					If aTaman[_x,1] == aCores[_i,1]
						oPrn:Say(nLin,aColI1x1,Alltrim(STR(aTaman[_x,4])),oFont09,100)
						aColI1x1 += 120
						oPrn:Say(nLin,aColI1x2,Alltrim(STR(aTaman[_x,5])),oFont09,100)
						aColI1x2 += 120
					End
				next _x
				nLin += nPula
			next _i

			nLin += nPula + nPula
			
			oPrn:Box(nLin-10,50,nLin+nPula+5,nfim)
			oPrn:Say(nLin,080,cObsOs,oFont11,100)
			nLin += nPula + nPula
				
				
			IF !Empty(cRolet)
				oPrn:Say(nLin,080,"ROLETE",oFont12n,100)
				nLin += nPula + nPula + nPula
			endif
			
			
			IF !Empty(cAlca) .AND. Empty(cRolet)
				oPrn:Say(nLin,080,"ALCA",oFont12n,100)
				nLin += nPula + nPula + nPula
			ELSEIF !Empty(cAlca)
				oPrn:Say(nLin,200,"ALCA",oFont12n,100)
				nLin += nPula + nPula + nPula
			endif		
			
			aMatPrim	:= VERMTPR(cTipoMat,_cOrdem,cProdCab)
			
			
			aColItem := {050,450,1750,1900,2300,2600,2700}
			aColCab  := {"| CODIGO","| DESCRICAO MATERIA PRIMA","| TAM.","| CORES","| QTD. SOL.","| UN","| QTD. UTILIZADA"}
			For _i := 1 to len(aColCab)
				oPrn:Say(nLin,aColItem[_i],AllTrim(aColCab[_i]),oFont09,100)
			Next _i
			nLin += nPula
			oPrn:Box(nLin,50,nLin+3,nfim)
			nLin += nPula
			
			
			For _i := 1 to len(aMatPrim)
				oPrn:Say(nLin,aColItem[1],"| "+ Alltrim(aMatPrim[_i,1])								,oFont08,100)		//CODIGO
				oPrn:Say(nLin,aColItem[2],"| "+ SubStr(Alltrim(aMatPrim[_i,2]),1,80)				,oFont08,100)		//DESCRICAO
				oPrn:Say(nLin,aColItem[3],"| "+ Alltrim(aMatPrim[_i,4])								,oFont08,100)		//TAMANHO
				oPrn:Say(nLin,aColItem[4],"| "+ SubStr(Alltrim(aMatPrim[_i,6]),1,30)				,oFont08,100)		//COR
				oPrn:Say(nLin,aColItem[5],"| "+ Transform(aMatPrim[_i,8],"@E 999,999.99999")		,oFont08,100)		//QTD. SOLICITADA
				oPrn:Say(nLin,aColItem[6],"| "+ Alltrim(aMatPrim[_i,7])								,oFont08,100)		//UNIDADE
				oPrn:Say(nLin,aColItem[7],"|  "														,oFont08,100)		//QTD. UTILIZADA
					
				If nLin > 2200
					nLin	  := 100
					oPrn:EndPage()
					oPrn:StartPage()
					
					oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
					
					nLin += nPula + nPula
					oPrn:Box(nLin-30,50,nLin-30+3,nfim-530)
				
					MSBAR('CODE128',1,23.8,cOrdemServ,oPrn,.F.,,.T.,0.042,1.7,,,,.F.)

					oPrn:Box(nLin-10,50,nLin+nPula+5,nfim)
					oPrn:Say(nLin,080,cObsOs,oFont11,100)
					nLin += nPula + nPula
						
					oPrn:Say(nLin,080,"ORDEM DE AVIAMENTOS N�:",oFont10,100)
					oPrn:Say(nLin-5,600,AllTrim(_cOrdem),oFont12n,100)	
		
					Cabec1b	:= SubStr(SC2->C2_PRODUTO,1,8) + " - " + AllTrim(POSICIONE("SB4",1,xFilial("SB4")+SubStr(SC2->C2_PRODUTO,1,8),"B4_DESC"))
					oPrn:Say(nLin,900,"PRODUTO:",oFont10,100)
					oPrn:Say(nLin,1130,Cabec1b,oFont10n,100)
			
					nLin += nPula
					oPrn:Box(nLin,50,nLin+3,nfim-530)

					nLin += nPula + nPula
					For _x := 1 to len(aColCab)
						oPrn:Say(nLin,aColItem[_x],AllTrim(aColCab[_x]),oFont08,100)
					Next _x
					nLin += nPula
					oPrn:Box(nLin,50,nLin+3,nfim)

				Endif

				nLin += nPula

			Next _i
			
		endif
		
		nLin += nPula
		oPrn:Box(nLin,50,nLin+3,nfim-450)
		nLin += nPula
 		
 
	nLin += nPula
	imprp	:= .T.

	RestArea(aArea)
			
Return

Static Function VERCOR(_cOrdem)

	Local cEOL			:= +Chr(13)+Chr(10)
	Local cQuery
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}

	cQuery  := "SELECT SUBSTRING(C2_PRODUTO,9,3) AS COR, SBV1.BV_DESCRI AS DESCCOR "+cEOL
	cQuery  += "FROM "+RetSQLName("SC2")+" SC2 WITH (NOLOCK) "+cEOL
	cQuery  += "	INNER JOIN "+RetSQLName("SB4")+" SB4 WITH (NOLOCK)  ON SB4.D_E_L_E_T_<>'*' AND B4_COD=SUBSTRING(C2_PRODUTO,1,8) "+cEOL
	cQuery  += "	INNER JOIN "+RetSQLName("SBV")+" SBV1 WITH (NOLOCK) ON SBV1.D_E_L_E_T_<>'*' AND SBV1.BV_TABELA=B4_LINHA "+cEOL
	cQuery  += "				AND SBV1.BV_CHAVE=SUBSTRING(C2_PRODUTO,9,3) "+cEOL
	cQuery  += "WHERE SC2.D_E_L_E_T_<>'*' AND C2_FILIAL= '"+xFilial("SC2")+"' "+cEOL
	cQuery  += "	AND C2_SEQPAI= '' AND C2_NUM= '"+AllTrim(_cOrdem)+"' "+cEOL
	cQuery  += "GROUP BY SUBSTRING(C2_PRODUTO,9,3), SBV1.BV_DESCRI "+cEOL

	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	DbSelectArea("TMP")
	TMP->(DbGoTop())
		
	While TMP->(!EOF())
		Aadd(aFieldFill, TMP->COR)
		Aadd(aFieldFill, TMP->DESCCOR)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)

Static Function VERTAM(_cOrdem)

	Local cEOL			:= +Chr(13)+Chr(10)
	Local cQuery
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}

	cQuery  := "SELECT SUBSTRING(C2_PRODUTO,9,3) AS COR,SBV2.R_E_C_N_O_, SUBSTRING(C2_PRODUTO,12,4) AS TAM, SBV2.BV_DESCRI AS DESCTAM, "+cEOL
	cQuery  += "			C2_QUANT AS QUANT, C2_QUJE AS QENTREG "+cEOL
	cQuery  += "FROM "+RetSQLName("SC2")+" SC2 "+cEOL
	cQuery  += "	INNER JOIN "+RetSQLName("SB4")+" SB4 WITH (NOLOCK) ON SB4.D_E_L_E_T_<>'*' AND B4_COD=SUBSTRING(C2_PRODUTO,1,8) "+cEOL
	cQuery  += "	INNER JOIN "+RetSQLName("SBV")+" SBV2 WITH (NOLOCK) ON SBV2.D_E_L_E_T_<>'*' AND SBV2.BV_TABELA=B4_COLUNA AND "+cEOL
	cQuery  += "				SBV2.BV_CHAVE=SUBSTRING(C2_PRODUTO,12,4) "+cEOL
	cQuery  += "WHERE SC2.D_E_L_E_T_<>'*' AND C2_FILIAL= '"+xFilial("SC2")+"' "+cEOL
	cQuery  += "	AND C2_SEQPAI= '' AND C2_NUM= '"+AllTrim(_cOrdem)+"' "+cEOL
	cQuery  += "GROUP BY C2_PRODUTO, C2_ITEMGRD, SBV2.BV_DESCRI, C2_QUANT, C2_QUJE , SBV2.R_E_C_N_O_ "+cEOL
	cQuery  += " ORDER BY SBV2.R_E_C_N_O_ "+cEOL
	
	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	DbSelectArea("TMP")
	TMP->(DbGoTop())
		
	While TMP->(!EOF())
		Aadd(aFieldFill, TMP->COR)
		Aadd(aFieldFill, TMP->TAM)
		Aadd(aFieldFill, TMP->DESCTAM)
		Aadd(aFieldFill, TMP->QUANT)
		Aadd(aFieldFill, TMP->QENTREG)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)


Static Function VERMTPR(cTipoMat,_cOrdem,cProdCab)

	Local cEOL	     := +Chr(13)+Chr(10)
	Local cQuery
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}
	
	
	If Select("TMP1") > 0
		TMP1->(DbCloseArea())
	EndIf
	
	
	If Select("TMP2") > 0
		TMP2->(DbCloseArea())
	EndIf
	
	// TODO  INICIO - QUERY INCLUIDA POR WESKLEY (23/10/2017)
	
	cQuery := " SELECT SD4.D4_COD AS CODIGO,SB1.B1_TIPO AS TIPO, B4_DESC AS DESCRIPRO ,SUBSTRING(D4_COD,12,4) AS TAMANHO, SUM(SD4.D4_QUANT) AS QUANT, ISNULL(SBV2.BV_DESCRI,'') AS DESCTAM, "
	cQuery += " SUBSTRING(D4_COD,9,3) AS COR, SBV1.BV_DESCRI AS DESCCOR, SB1.B1_UM AS UN "
	cQuery += " FROM SD4010 SD4 "
	cQuery += " JOIN "+RetSqlName("SC2")+" SC2 WITH (NOLOCK) ON C2_FILIAL=D4_FILIAL AND D4_PRODUTO=C2_PRODUTO AND D4_OP=C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD AND SC2.D_E_L_E_T_ = '' "
	cQuery += " JOIN "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) ON SB1.B1_COD=SD4.D4_COD AND SB1.D_E_L_E_T_='' "
	cQuery += " JOIN "+RetSqlName("SBM")+" SBM WITH (NOLOCK) ON SBM.BM_GRUPO=SB1.B1_GRUPO AND SBM.D_E_L_E_T_='' "
	cQuery += " JOIN "+RetSqlName("SB4")+" SB4 WITH (NOLOCK) ON SB4.D_E_L_E_T_<>'*'  AND B4_COD=SUBSTRING(D4_COD,1,8) "
	cQuery += " JOIN "+RetSqlName("SBV")+" SBV1 WITH (NOLOCK) ON SBV1.D_E_L_E_T_<>'*' AND SBV1.BV_TABELA=B4_LINHA AND SBV1.BV_CHAVE=SUBSTRING(D4_COD,9,3) "
	cQuery += " JOIN "+RetSqlName("SBV")+" SBV2 WITH (NOLOCK) ON SBV2.D_E_L_E_T_<>'*' AND SBV2.BV_TABELA=B4_COLUNA AND SBV2.BV_CHAVE=SUBSTRING(D4_COD,12,4) "
	cQuery += " JOIN "+RetSqlName("SB1")+" SB WITH (NOLOCK) ON  SB.B1_COD = SD4.D4_PRODUTO AND SB.D_E_L_E_T_ = '' " 
	cQuery += " JOIN "+RetSqlName("SBM")+" SBM1 WITH (NOLOCK) ON SBM1.BM_GRUPO=SB.B1_GRUPO AND SBM1.D_E_L_E_T_=''  "
	cQuery += " WHERE SD4.D_E_L_E_T_ = '' "
	cQuery += " AND C2_FILIAL= '" +xFilial("SC2") +"' "
	cQuery += " AND C2_NUM = '"+AllTrim(_cOrdem)+"' "
	
	//if cProdCab = 'KT' .OR. cProdCab = 'PI'
	//	cQuery += " AND SD4.D4_OP NOT LIKE '%00100%' " 
	//ELSE 
	//	cQuery += " AND SD4.D4_OP LIKE '%00100%' "
	//ENDIF
	
	cQuery += " AND SB1.B1_TIPO <> 'PI' "
	cQuery += " AND SB.B1_GRUPO NOT IN ("+ GETMV("HP_GRPOP") + ") AND SB1.B1_GRUPO NOT IN ("+ GETMV("HP_GRPOP") + ") "
	cQuery += " GROUP BY SD4.D4_COD,B4_DESC,D4_COD,SBV2.BV_DESCRI,SBV1.BV_DESCRI,SB1.B1_UM,SB1.B1_TIPO "
	cQuery += " HAVING SUM(SD4.D4_QUANT)>0 "
	
	// TODO FIM - QUERY INCLUIDA POR WESKLEY (23/10/2017)
	
	TCQUERY cQuery NEW ALIAS TMP2
	
		
	While TMP2->(!EOF())
		Aadd(aFieldFill, TMP2->CODIGO)
		Aadd(aFieldFill, TMP2->DESCRIPRO)
		Aadd(aFieldFill, TMP2->TAMANHO)
		Aadd(aFieldFill, TMP2->DESCTAM)
		Aadd(aFieldFill, TMP2->COR)
		Aadd(aFieldFill, TMP2->DESCCOR)
		Aadd(aFieldFill, TMP2->UN)
		Aadd(aFieldFill, TMP2->QUANT)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)

