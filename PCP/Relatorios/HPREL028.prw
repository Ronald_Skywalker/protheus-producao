#include 'protheus.ch'
#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} RF013 
Relat�rio de Fases
@author Weskley Silva
@since 06/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL028()


	Private oReport
	Private cPergCont	:= 'HPREL028' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'TMP', 'RELATORIO DE FASES ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELATORIO DE FASES' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATORIO DE FASES', { 'TMP', 'SC6','SB1','SC5','SB2','SC2','ZAJ','ZAI','SBM','SB4'})
			
	TRCell():New( oSection1, 'COD'									,'TMP', 		'COD',											"@!"                        ,20)
	TRCell():New( oSection1, 'DESCR'								,'TMP', 		'DESCR',										"@!"                        ,35)
	TRCell():New( oSection1, 'COD_GRUPO'   							,'TMP', 		'COD_GRUPO',    								"@!"				        ,3)
	TRCell():New( oSection1, 'GRUPO' 	    						,'TMP', 		'GRUPO',	              						"@!"						,5)
	TRCell():New( oSection1, 'COD_SUBGRUPO'    						,'TMP', 		'COD_SUBGRUPO',        							"@!"						,10)
	TRCell():New( oSection1, 'SUBGRUPO' 	    					,'TMP', 		'SUBGRUPO',	        	    					"@!"		                ,10)
	TRCell():New( oSection1, 'REFERENCIA'							,'TMP', 		'REFERENCIA',	        						"@!"		                ,5)
	TRCell():New( oSection1, 'TIPO' 	    						,'TMP', 		'TIPO' ,	    								"@!"		                ,20)
	TRCell():New( oSection1, 'SITUACAO'								,'TMP', 		'SITUACAO' ,		        					"@!"		                ,4)
	TRCell():New( oSection1, 'COD_COR'  							,'TMP', 		'COD_COR' ,		       	 						"@!"		                ,4)
	TRCell():New( oSection1, 'COR' 									,'TMP', 		'COR' ,				    						"@!"	                    ,4)
	TRCell():New( oSection1, 'TAMANHO'								,'TMP', 		'TAMANHO',										"@!"		                ,4)
	TRCell():New( oSection1, 'SUBCOLECAO'							,'TMP', 		'SUBCOLECAO',		    						"@!"		                ,4)
	TRCell():New( oSection1, 'COLECAO'								,'TMP', 		'COLECAO',										"@!"	                    ,4)
	TRCell():New( oSection1, 'EAN'									,'TMP', 		'EAN',											"@!"						,4)
	TRCell():New( oSection1, 'UTILIZACAO_PRODUTO'					,'TMP', 		'UTILIZACAO_PRODUTO',							"@!"						,4)
	TRCell():New( oSection1, 'CICLO'								,'TMP', 		'CICLO',										"@!"						,4)
	TRCell():New( oSection1, 'MARCA'								,'TMP', 		'MARCA',										"@!"						,4)
	TRCell():New( oSection1, 'PROCESSO'								,'TMP', 		'PROCESSO',										"@!" 						,15)
	TRCell():New( oSection1, 'ESTOQUE'								,'TMP', 		'ESTOQUE',										"@!"						,15)
	TRCell():New( oSection1, 'RESERVA'								,'TMP', 		'RESERVA',										"@!"						,15)
	TRCell():New( oSection1, 'DISPONIVEL'							,'TMP', 		'DISPONIVEL',									"@!"						,15)
	TRCell():New( oSection1, 'ESTOQUE_EC'							,'TMP', 		'ESTOQUE_EC',									"@!"						,15)
	TRCell():New( oSection1, 'CRT_ATUAL'							,'TMP', 		'CRT_ATUAL',									"@!"						,15)
	TRCell():New( oSection1, 'CRTAT_APROVADA'						,'TMP', 		'CRTAT_APROVADA',						    	"@!"						,15)
	TRCell():New( oSection1, 'CT_FUTURA'							,'TMP', 		'CT_FUTURA',							    	"@!" 						,15)
	TRCell():New( oSection1, 'CTFUT_APROVADA'						,'TMP', 		'CTFUT_APROVADA',					        	"@!"						,15)
	TRCell():New( oSection1, 'CT_TOTAL'								,'TMP', 		'CT_TOTAL',							        	"@!"						,15)
	TRCell():New( oSection1, 'CTTOTAL_APROVADA'						,'TMP', 		'CTTOTAL_APROVADA',					        	"@!"						,15)
	TRCell():New( oSection1, 'SEGUNDA_BLOQUEADO'					,'TMP', 		'SEGUNDA_BLOQUEADO',							"@!"						,15)
	TRCell():New( oSection1, 'SEGUNDA_QUALIDADE'					,'TMP', 		'SEGUNDA_QUALIDADE',							"@!"						,15)
	TRCell():New( oSection1, 'ESTCART_ATUAL'						,'TMP', 		'ESTCART_ATUAL',								"@!"	    				,15)
	TRCell():New( oSection1, 'E_PROCESSO_CART_ATUAL'				,'TMP', 		'E_PROCESSO_CART_ATUAL',						"@!"						,15)
	TRCell():New( oSection1, 'QTD_CRT_ATUAL_APROVADA'				,'TMP', 		'QTD_CRT_ATUAL_APROVADA',						"@!"						,15)
	TRCell():New( oSection1, 'E_PROC_CART_ATUAL_APROVADA'			,'TMP',			'E_PROC_CART_ATUAL_APROVADA',					"@!"						,15)
	TRCell():New( oSection1, 'ET_CART_TOTAL_APROVADA' 				,'TMP', 		'ET_CART_TOTAL_APROVADA',						"@!"						,15)
	TRCell():New( oSection1, 'ET_PRO_CART_TOTAL_APROVADA'		    ,'TMP',			'ET_PRO_CART_TOTAL_APROVADA',				    "@!"						,15)
	TRCell():New( oSection1, 'DISP_RESERVA_CART_TOTAL'				,'TMP',			'DISP_RESERVA_CART_TOTAL',						"@!"						,15)
	TRCell():New( oSection1, 'ESTO_PROCESSO_CART_TOTAL'				,'TMP',			'ESTO_PROCESSO_CART_TOTAL',						"@!"						,15)
	

Return( oReport )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""
	Local cTpPed  := GETMV("HP_TPRELFA")

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("TMP") > 0
		TMP->(dbCloseArea())
	Endif
      
    cQuery := " SELECT T.B1_COD AS COD, T.B4_DESC AS DESCR, " 
	cQuery += " T.COD_GRUPO AS COD_GRUPO, " 
	cQuery += " T.GRUPO , " 
	cQuery += " T.COD_SUBGRUPO, " 
	cQuery += " T.SUBGRUPO, " 
	cQuery += " T.REFERENCIA, " 
	cQuery += " T.B1_TIPO AS TIPO, " 
	cQuery += " T.B1_YSITUAC AS SITUACAO, " 
	cQuery += " T.COD_COR AS COD_COR, " 
	cQuery += " T.COR AS COR, " 
	//cQuery += " CASE WHEN T.TAMANHO LIKE '000%' THEN REPLACE(T.TAMANHO,'000','') " 
	//cQuery += " WHEN T.TAMANHO LIKE '0%' THEN SUBSTRING(T.TAMANHO,2,4) " 
	//cQuery += " ELSE REPLACE(T.TAMANHO,'00','') END TAMANHO, " 
	cQuery += " T.TAMANHO AS TAMANHO, "  
	cQuery += " T.SUBCOLECAO, " 
	cQuery += " T.COLECAO, " 
	cQuery += " T.EAN, " 
	cQuery += " T.UTILIZACAO_PRODUTO, " 
	cQuery += " T.CICLO, " 
	cQuery += " T.MARCA, " 
	cQuery += " T.PROCESSO, " 
	cQuery += " CASE WHEN T.ESTOQUE < 0 THEN 0 ELSE T.ESTOQUE END AS ESTOQUE, " 
	cQuery += " T.RESERVA, " 
	cQuery += " T.DISPONIVEL, "
	cQuery += " T.ESTOQUE_EC, " 	 
	cQuery += " T.CRT_ATUAL, " 
	cQuery += " T.CRTAT_APROVADA, " 
	cQuery += " T.CT_FUTURA, " 
	cQuery += " T.CTFUT_APROVADA, " 
	cQuery += " T.CRT_ATUAL+T.CT_FUTURA AS CT_TOTAL, "
	cQuery += " T.CRTAT_APROVADA+T.CTFUT_APROVADA AS CTTOTAL_APROVADA , "
	cQuery += " T.SEGUNDA_BLOQUEADO, "
	cQuery += " T.SEGUNDA_QUALIDADE, "
	cQuery += " T.ESTOQUE - T.CRT_ATUAL AS 'ESTCART_ATUAL', "
	cQuery += " T.ESTOQUE + (T.PROCESSO - T.CRT_ATUAL) AS 'E_PROCESSO_CART_ATUAL', "
	cQuery += " T.ESTOQUE - T.CRTAT_APROVADA AS 'QTD_CRT_ATUAL_APROVADA', "
	cQuery += " T.ESTOQUE + (T.PROCESSO - CRTAT_APROVADA) AS 'E_PROC_CART_ATUAL_APROVADA', "
	cQuery += " T.ESTOQUE - (T.CRTAT_APROVADA+T.CTFUT_APROVADA) AS 'ET_CART_TOTAL_APROVADA', "
	cQuery += " T.ESTOQUE + T.PROCESSO - (T.CRTAT_APROVADA+T.CTFUT_APROVADA) AS 'ET_PRO_CART_TOTAL_APROVADA', "
	cQuery += " T.DISPONIVEL + T.RESERVA - (T.CRT_ATUAL+T.CT_FUTURA) AS 'DISP_RESERVA_CART_TOTAL', "
	cQuery += " T.ESTOQUE + T.PROCESSO - (T.CRT_ATUAL+T.CT_FUTURA) AS 'ESTO_PROCESSO_CART_TOTAL' "
	   
	cQuery += " FROM ( " 
	
	cQuery += "	SELECT B1_COD, "
	cQuery += " B1_TIPO, B4_DESC, "
	cQuery += " B1_YSITUAC, "
	cQuery += " ISNULL(BV_CHAVE,'-') AS COD_COR, "
	cQuery += " ISNULL(BV_DESCRI,'-') AS COR, "
	cQuery += " ISNULL(ZAJ_CICLO,'-') AS CICLO, "
	cQuery += " ISNULL(ZAH_DESCRI,'-' )AS SUBCOLECAO, "
	cQuery += " ISNULL(ZAA_DESCRI,'-') AS COLECAO, "
	cQuery += " B1_CODBAR AS EAN, "
	cQuery += " ISNULL((SELECT  ZAD_DESCRI FROM "+RetSqlName('ZAD')+"  WHERE ZAD_CODIGO = SUBSTRING(B4_YOCASIA,1,5)),'-') AS 'UTILIZACAO_PRODUTO', "
	cQuery += " ISNULL(ZAI_MARCA,'-') AS MARCA, "
	cQuery += " ISNULL(B1_GRUPO,'-') AS COD_GRUPO, "
	cQuery += " ISNULL(BM_DESC,'-') AS GRUPO, "
	cQuery += " ISNULL(ZAG_CODIGO,'-') AS COD_SUBGRUPO, "
	cQuery += " ISNULL(ZAG_DESCRI,'-') AS SUBGRUPO, "
	cQuery += " ISNULL(B4_COD,'-') AS REFERENCIA, "
	cQuery += " RIGHT(B1_COD,4) AS TAMANHO, "
	       
	cQuery += " (SELECT ISNULL(SUM(C2_QUANT-C2_QUJE-C2_PERDA),0) FROM "+RetSqlName('SC2')+" SC2010 (NOLOCK) WHERE B1_COD=C2_PRODUTO AND SC2010.D_E_L_E_T_ = '' AND C2_TPOP = 'F' AND C2_DATRF='' AND C2_XRES <> '' AND C2_FILIAL = '"+xFilial('SC2')+"' ) " 
	cQuery += "  AS PROCESSO, "

	cQuery += "	ISNULL((SELECT SUM(B2_QATU) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' AND B2_FILIAL = '"+xFilial('SB2')+"'  AND B2_LOCAL IN ('E1','E0') AND B2_COD = B1_COD  "
	cQuery += "	GROUP BY B2_COD),0) AS ESTOQUE, "

	cQuery += "	ISNULL((SELECT SUM(B2_RESERVA + B2_XRESERV) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' AND B2_FILIAL = '"+xFilial('SB2')+"' AND B2_LOCAL IN ('E1','E0') AND B2_COD = B1_COD "
	cQuery += " GROUP BY B2_COD),0) AS RESERVA, "
			
	cQuery += "	ISNULL((SELECT (SUM(B2_QATU)-SUM(B2_RESERVA + B2_XRESERV)) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' AND B2_FILIAL = '"+xFilial('SB2')+"' AND B2_LOCAL IN ('E1','E0') " 
	cQuery += "  AND B2_COD = B1_COD "
	cQuery += "	GROUP BY B2_COD),0) AS DISPONIVEL, "
	
	cQuery += "	ISNULL((SELECT SUM(B2_QATU) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' AND B2_FILIAL = '"+xFilial('SB2')+"'  AND B2_LOCAL = 'EC' AND B2_COD = B1_COD  "
	cQuery += "	GROUP BY B2_COD),0) AS ESTOQUE_EC, "	
	
	cQuery += "	ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) AS C6_QTDVEN FROM SC6010 (NOLOCK) "
	cQuery += "	JOIN SC5010 (NOLOCK) ON (C6_NUM = C5_NUM AND C6_CLI = C5_CLIENT AND C6_LOJA = C5_LOJACLI  AND SC5010.D_E_L_E_T_ = '' AND SC6010.D_E_L_E_T_ = '' AND  C5_FILIAL = '0101') "
	cQuery += "	JOIN SF4010 (NOLOCK) ON (F4_CODIGO = C6_TES AND SF4010.D_E_L_E_T_ = '') "
	cQuery += "	WHERE SC6010.D_E_L_E_T_='' AND C6_PRODUTO = B1_COD AND C6_BLQ<>'R' AND C5_TPPED NOT IN ("+cTpPed+") AND C6_LOCAL = 'E0' AND F4_ESTOQUE = 'S' AND C6_ENTREG BETWEEN '"+ DTOS(mv_par04) +"' AND '"+ DTOS(mv_par05) +"' "
	cQuery += "	GROUP BY C6_PRODUTO),0) AS CRT_ATUAL, "  
			
	cQuery += "	ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) AS C6_QTDVEN  FROM SC5010 (NOLOCK) , SC6010 (NOLOCK) " 
	cQuery += "	JOIN SF4010 (NOLOCK) ON (F4_CODIGO = C6_TES AND SF4010.D_E_L_E_T_ = '') "
	cQuery += "	WHERE C6_PRODUTO = B1_COD AND C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA AND SC5010.D_E_L_E_T_=''  "
	cQuery += "	AND SC6010.D_E_L_E_T_='' AND C6_BLQ<>'R' AND C5_XBLQ = 'L' AND C5_TPPED NOT IN ("+cTpPed+") AND C6_LOCAL = 'E0' AND F4_ESTOQUE = 'S' AND C5_FILIAL = '0101' AND C6_ENTREG BETWEEN '"+ DTOS(mv_par08) +"' AND '"+ DTOS(mv_par09) +"'  "
	cQuery += "	GROUP BY C6_PRODUTO),0) AS CRTAT_APROVADA, " 

	cQuery += "	ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) AS C6_QTDVEN FROM SC5010 (NOLOCK), SC6010 (NOLOCK) "
	cQuery += "	JOIN SF4010 (NOLOCK) ON (F4_CODIGO = C6_TES AND SF4010.D_E_L_E_T_ = '') "
	cQuery += "	WHERE C6_PRODUTO = B1_COD  "
	cQuery += "	AND C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA AND SC5010.D_E_L_E_T_='' AND SC6010.D_E_L_E_T_='' AND C5_FILIAL = '0101'  "
	cQuery += "	AND C6_ENTREG BETWEEN '"+ DTOS(mv_par06) +"' AND '"+ DTOS(mv_par07) +"'  AND C6_BLQ<>'R' AND C5_TPPED NOT IN ("+cTpPed+") AND C6_LOCAL = 'E0' AND F4_ESTOQUE = 'S' "
	cQuery += "	GROUP BY C6_PRODUTO),0) AS CT_FUTURA, "

	cQuery += "	ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) AS C6_QTDVEN FROM SC5010 (NOLOCK), SC6010 (NOLOCK) "
	cQuery += "	JOIN SF4010 (NOLOCK) ON (F4_CODIGO = C6_TES AND SF4010.D_E_L_E_T_ = '') "
	cQuery += "	WHERE C6_PRODUTO = B1_COD AND C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA AND SC5010.D_E_L_E_T_=''  "
	cQuery += "	AND SC6010.D_E_L_E_T_='' AND C6_BLQ<>'R'  AND C5_TPPED NOT IN ("+cTpPed+") AND C6_LOCAL = 'E0' AND F4_ESTOQUE = 'S' AND C5_XBLQ=  'L' AND  C6_ENTREG BETWEEN '"+ DTOS(mv_par10) +"' AND '"+ DTOS(mv_par11) +"' AND C5_FILIAL = '0101'  "
	cQuery += "	GROUP BY C6_PRODUTO),0) AS CTFUT_APROVADA, "	

	cQuery += "	ISNULL((SELECT (SUM(B2_QATU)-SUM(B2_RESERVA + B2_XRESERV)) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_='' " 
	cQuery += " AND B2_LOCAL IN ('QB') AND B2_COD = B1_COD  AND B2_FILIAL = '"+xFilial('SB2')+"' "
	cQuery += "	GROUP BY B2_COD),0) AS 'SEGUNDA_BLOQUEADO', "

	cQuery += "	ISNULL((SELECT (SUM(B2_QATU)-SUM(B2_RESERVA + B2_XRESERV)) FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE SB2010.D_E_L_E_T_=''  " 
	cQuery += " AND B2_LOCAL IN ('QL') AND B2_COD = B1_COD AND B2_FILIAL = '"+xFilial('SB2')+"'  "
	cQuery += "	GROUP BY B2_COD),0)AS 'SEGUNDA_QUALIDADE' "

	cQuery += "	FROM "+RetSqlName('SB1')+" (NOLOCK) "
	cQuery += "	LEFT JOIN "+RetSqlName('SBM')+" (NOLOCK) ON SB1010.B1_GRUPO=SBM010.BM_GRUPO AND SBM010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAJ')+" (NOLOCK) ON B1_YCICLO = ZAJ_CODIGO AND ZAJ010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAI')+" (NOLOCK) ON B1_YMARCA = ZAI_CODIGO AND ZAI010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAA')+" (NOLOCK) ON B1_YCOLECA = ZAA_CODIGO AND ZAA010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('SBV')+" (NOLOCK) ON SUBSTRING(B1_COD,9,3) = BV_CHAVE  AND BV_TABELA = 'COR' AND SBV010.D_E_L_E_T_ = ''  "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAH')+" (NOLOCK) ON B1_YSUBCOL = ZAH_CODIGO AND ZAH010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('SB4')+" (NOLOCK) ON LEFT(B1_COD,8) = B4_COD AND SB4010.D_E_L_E_T_ = '' "
	cQuery += "	LEFT JOIN "+RetSqlName('ZAG')+" ZAG (NOLOCK) ON  ZAG.ZAG_CODIGO = SB4010.B4_YSUBGRP AND ZAG.D_E_L_E_T_ = ''  "   
   
	cQuery += " WHERE SB1010.D_E_L_E_T_='' " 
	cQuery += "	AND B1_TIPO NOT IN ('MI','MP','SV','MO','MC','MB','AI') "
	cQuery += "	AND LEFT(B1_GRUPO,2) NOT IN ('AI','SV','MC','BN','MB','MI','MP') " 
	
	If !Empty(mv_par03)
       	cQuery += " AND B1_YSUBCOL = '"+ Alltrim(mv_par03) +"'  "
    EndIF
		
	cQuery += "	GROUP BY B1_COD, B1_TIPO,B4_DESC,B1_YSITUAC,ZAJ_CICLO,ZAI_MARCA,B1_GRUPO,BM_DESC,BV_CHAVE,BV_DESCRI,ZAH_DESCRI,ZAA_DESCRI,B1_YCOLECA," 
	cQuery += " B1_CODBAR,B4_YOCASIA,B4_COD,ZAG_CODIGO,ZAG_DESCRI,B1_LOCPAD "
		
	cQuery += " ) T "	
		       
	cQuery += " WHERE (T.ESTOQUE > 0 OR T.PROCESSO > 0 OR T.CRT_ATUAL > 0 OR T.CT_FUTURA > 0 OR T.CRTAT_APROVADA > 0 OR T.SEGUNDA_BLOQUEADO > 0 OR T.SEGUNDA_QUALIDADE > 0 ) " 	    
		    
	if !EMPTY(mv_par01)
   
        cQuery += " AND  T.B1_COD = '"+ Alltrim(mv_par01) +"'  "	
    endif
    
    if !EMPTY(mv_par02)
       	cQuery += " AND T.B1_TIPO = '"+ Alltrim(mv_par02) +"'   "
    endif
    
    /*if !EMPTY(mv_par03)
       	cQuery += " AND T.COLECAO = '"+ Alltrim(mv_par03) +"'  "
    ENDIF*/	    
		    
    TCQUERY cQuery NEW ALIAS "TMP"
    
   // dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	//DbSelectArea("TMP")
	//TMP->(DbGoTop())

	While TMP->(!EOF())      
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("COD"):SetValue(TMP->COD)
		oSection1:Cell("COD"):SetAlign("LEFT")
		
		oSection1:Cell("DESCR"):SetValue(TMP->DESCR)
		oSection1:Cell("DESCR"):SetAlign("LEFT")
		
		oSection1:Cell("COD_GRUPO"):SetValue(TMP->COD_GRUPO)
		oSection1:Cell("COD_GRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO"):SetValue(TMP->GRUPO)
		oSection1:Cell("GRUPO"):SetAlign("LEFT")	
		
		oSection1:Cell("COD_SUBGRUPO"):SetValue(TMP->COD_SUBGRUPO)
		oSection1:Cell("COD_SUBGRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("SUBGRUPO"):SetValue(TMP->SUBGRUPO)
		oSection1:Cell("SUBGRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(TMP->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(TMP->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("SITUACAO"):SetValue(TMP->SITUACAO)
		oSection1:Cell("SITUACAO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(TMP->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("COR"):SetValue(TMP->COR)
		oSection1:Cell("COR"):SetAlign("LEFT")

		oSection1:Cell("TAMANHO"):SetValue(TMP->TAMANHO)
		oSection1:Cell("TAMANHO"):SetAlign("LEFT")
		
		oSection1:Cell("SUBCOLECAO"):SetValue(TMP->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")

		oSection1:Cell("COLECAO"):SetValue(TMP->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("EAN"):SetValue(TMP->EAN)
		oSection1:Cell("EAN"):SetAlign("LEFT")
		
		oSection1:Cell("UTILIZACAO_PRODUTO"):SetValue(TMP->UTILIZACAO_PRODUTO)
		oSection1:Cell("UTILIZACAO_PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("CICLO"):SetValue(TMP->CICLO)
		oSection1:Cell("CICLO"):SetAlign("LEFT")
		
		oSection1:Cell("MARCA"):SetValue(TMP->MARCA)
		oSection1:Cell("MARCA"):SetAlign("LEFT")
		
		oSection1:Cell("PROCESSO"):SetValue(TMP->PROCESSO)
		oSection1:Cell("PROCESSO"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE"):SetValue(TMP->ESTOQUE)
		oSection1:Cell("ESTOQUE"):SetAlign("LEFT")
		
		oSection1:Cell("RESERVA"):SetValue(TMP->RESERVA)
		oSection1:Cell("RESERVA"):SetAlign("LEFT")
		
		oSection1:Cell("DISPONIVEL"):SetValue(TMP->DISPONIVEL)
		oSection1:Cell("DISPONIVEL"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE_EC"):SetValue(TMP->ESTOQUE_EC)
		oSection1:Cell("ESTOQUE_EC"):SetAlign("LEFT")		
		
		oSection1:Cell("CRT_ATUAL"):SetValue(TMP->CRT_ATUAL)
		oSection1:Cell("CRT_ATUAL"):SetAlign("LEFT")
		
		oSection1:Cell("CRTAT_APROVADA"):SetValue(TMP->CRTAT_APROVADA)
		oSection1:Cell("CRTAT_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("CT_FUTURA"):SetValue(TMP->CT_FUTURA)
		oSection1:Cell("CT_FUTURA"):SetAlign("LEFT")
			
		oSection1:Cell("CTFUT_APROVADA"):SetValue(TMP->CTFUT_APROVADA)
		oSection1:Cell("CTFUT_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("CT_TOTAL"):SetValue(TMP->CT_TOTAL)
		oSection1:Cell("CT_TOTAL"):SetAlign("LEFT")
		
		oSection1:Cell("CTTOTAL_APROVADA"):SetValue(TMP->CTTOTAL_APROVADA)
		oSection1:Cell("CTTOTAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("SEGUNDA_BLOQUEADO"):SetValue(TMP->SEGUNDA_BLOQUEADO)
		oSection1:Cell("SEGUNDA_BLOQUEADO"):SetAlign("LEFT")
		
		oSection1:Cell("SEGUNDA_QUALIDADE"):SetValue(TMP->SEGUNDA_QUALIDADE)
		oSection1:Cell("SEGUNDA_QUALIDADE"):SetAlign("LEFT")
		
		oSection1:Cell("ESTCART_ATUAL"):SetValue(TMP->ESTCART_ATUAL)
		oSection1:Cell("ESTCART_ATUAL"):SetAlign("LEFT")
		
		oSection1:Cell("E_PROCESSO_CART_ATUAL"):SetValue(TMP->E_PROCESSO_CART_ATUAL)
		oSection1:Cell("E_PROCESSO_CART_ATUAL"):SetAlign("LEFT")
		
		oSection1:Cell("QTD_CRT_ATUAL_APROVADA"):SetValue(TMP->QTD_CRT_ATUAL_APROVADA)
		oSection1:Cell("QTD_CRT_ATUAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("E_PROC_CART_ATUAL_APROVADA"):SetValue(TMP->E_PROC_CART_ATUAL_APROVADA)
		oSection1:Cell("E_PROC_CART_ATUAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("ET_CART_TOTAL_APROVADA"):SetValue(TMP->ET_CART_TOTAL_APROVADA)
		oSection1:Cell("ET_CART_TOTAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("ET_PRO_CART_TOTAL_APROVADA"):SetValue(TMP->ET_PRO_CART_TOTAL_APROVADA)
		oSection1:Cell("ET_PRO_CART_TOTAL_APROVADA"):SetAlign("LEFT")
		
		oSection1:Cell("DISP_RESERVA_CART_TOTAL"):SetValue(TMP->DISP_RESERVA_CART_TOTAL)
		oSection1:Cell("DISP_RESERVA_CART_TOTAL"):SetAlign("LEFT")
		
		oSection1:Cell("ESTO_PROCESSO_CART_TOTAL"):SetValue(TMP->ESTO_PROCESSO_CART_TOTAL)
		oSection1:Cell("ESTO_PROCESSO_CART_TOTAL"):SetAlign("LEFT")
				
		oSection1:PrintLine()
		
		TMP->(DBSKIP()) 
	enddo
	TMP->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Produto"		               ,""		,""		    ,"mv_ch1","C",20,0,1,"G",""	,"SB1","","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Tipo"		                   ,""		,""		    ,"mv_ch2","C",03,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Cole��o"		               ,""		,""	 	    ,"mv_ch3","C",15,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Carteira Atual de?"	           ,""		,""		    ,"mv_ch4","D",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Carteira Atual At�?"		   ,""		,""		    ,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "06","Carteira Futura de?"	       ,""		,""		    ,"mv_ch6","D",08,0,1,"G",""	,""	,"","","mv_par06"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "07","Carteira Futura At�?"	       ,""		,""		    ,"mv_ch7","D",08,0,1,"G",""	,""	,"","","mv_par07"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "08","Carteira Atual Aprovada de?"   ,""		,""		    ,"mv_ch8","D",08,0,1,"G",""	,""	,"","","mv_par08"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "09","Carteira Atual Aprovada At�?"  ,""		,""		    ,"mv_ch9","D",08,0,1,"G",""	,""	,"","","mv_par09"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "10","Carteira Futura Aprovada de?"  ,""		,""		    ,"mv_c10","D",08,0,1,"G","",""	,"","","mv_par10"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "11","Carteira Futura Aprovada At�?" ,""		,""		    ,"mv_c11","D",08,0,1,"G","",""	,"","","mv_par11"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "12","Armazem?"                      ,""		,""		    ,"mv_c12","C",03,0,1,"G","","NNR","","","mv_par12"," ","","","","","","","","","","","","","","","")
Return  