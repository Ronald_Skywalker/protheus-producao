#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL019 
Ops Previstas
@author Weskley Silva
@since 14/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL020()

Private oReport
Private cPergCont	:= 'HPREL020' 

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'OPS', 'OPS PREVISTAS', , {|oReport| ReportPrint( oReport ), 'OPS PREVISTAS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'OPS PREVISTAS', { 'OPS', 'SBV','SC2','SB4'})
			
	TRCell():New( oSection1, 'SEQ_MRP'				,'OPS', 		'SEQ_MRP',						    "@!"                        ,10)
	TRCell():New( oSection1, 'OP'					,'OPS', 		'OP',				  			    "@!"				        ,15)
	TRCell():New( oSection1, 'SEQUENCIA_OP'			,'OPS', 		'SEQUENCIA_OP',		  			    "@!"				        ,06)
	TRCell():New( oSection1, 'PRODUTO'				,'OPS', 		'PRODUTO',			  			    "@!"				        ,20)
	TRCell():New( oSection1, 'COLECAO'			    ,'OPS', 		'COLECAO',			  			    "@!"				        ,25)
	TRCell():New( oSection1, 'SUBCOLECAO'			,'OPS', 		'SUBCOLECAO',		  			    "@!"				        ,25)
	TRCell():New( oSection1, 'REFERENCIA'			,'OPS', 		'REFERENCIA',		  			    "@!"				        ,15)
	TRCell():New( oSection1, 'COD_COR'				,'OPS', 		'COD_COR',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DESC_COR'				,'OPS', 		'DESC_COR',			  			    "@!"				        ,25)
	TRCell():New( oSection1, 'TAM'					,'OPS', 		'TAM',				  			    "@!"				        ,06)
	TRCell():New( oSection1, 'QUANTIDADE'			,'OPS', 		'QUANTIDADE',		     			"@E 999,99"			        ,06)
			
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("OPS") > 0
		OPS->(dbCloseArea())
	Endif
      
    cQuery := " SELECT " 
    cQuery += " SC2.C2_SEQMRP AS SEQ_MRP, "  
    cQuery += " C2_NUM AS OP, "
    cQuery += " C2_SEQUEN AS SEQUENCIA_OP, "
    cQuery += " C2_PRODUTO AS PRODUTO, "
    cQuery += " SB4.B4_YNCOLEC AS COLECAO, "
    cQuery += " SB4.B4_YNSUBCO AS SUBCOLECAO, "
    cQuery += " LEFT(C2_PRODUTO,8) AS REFERENCIA , "
    cQuery += "	SUBSTRING(SC2.C2_PRODUTO,9,3) AS COD_COR, "
    cQuery += " SBV.BV_DESCRI AS DESC_COR, "
    cQuery += " RIGHT(SC2.C2_PRODUTO,4)  AS TAM, "
    cQuery += " C2_QUANT AS QUANTIDADE "
    cQuery += " FROM "+RetSqlName('SC2')+" SC2 "
    cQuery += " INNER JOIN "+RetSqlName('SBV')+" SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC2.C2_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
    cQuery += " INNER JOIN "+RetSqlName('SB4')+" SB4 ON SB4.B4_COD=LEFT(SC2.C2_PRODUTO,8) " 
    cQuery += " WHERE SC2.C2_TPOP='P' AND SC2.D_E_L_E_T_='' " 
	
	TCQUERY cQuery NEW ALIAS OPS

	While OPS->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("SEQ_MRP"):SetValue(OPS->SEQ_MRP)
		oSection1:Cell("SEQ_MRP"):SetAlign("LEFT")

		oSection1:Cell("OP"):SetValue(OPS->OP)
		oSection1:Cell("OP"):SetAlign("LEFT")
		
		oSection1:Cell("SEQUENCIA_OP"):SetValue(OPS->SEQUENCIA_OP)
		oSection1:Cell("SEQUENCIA_OP"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTO"):SetValue(OPS->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("COLECAO"):SetValue(OPS->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
				
		oSection1:Cell("SUBCOLECAO"):SetValue(OPS->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(OPS->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(OPS->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(OPS->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(OPS->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
		
		oSection1:Cell("QUANTIDADE"):SetValue(OPS->QUANTIDADE)
		oSection1:Cell("QUANTIDADE"):SetAlign("LEFT")
				
		oSection1:PrintLine()
		
		OPS->(DBSKIP()) 
	enddo
	OPS->(DBCLOSEAREA())
Return( Nil )