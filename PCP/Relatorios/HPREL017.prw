#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL017 
Caracteristicas dos Produtos
@author Weskley Silva
@since 13/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL017()

Private oReport
Private cPergCont	:= 'HPREL017' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'CRT', 'CARACTERISTICAS DOS PRODUTOS', , {|oReport| ReportPrint( oReport ), 'CARACTERISTICAS DOS PRODUTOS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'CARACTERISTICAS DOS PRODUTOS', { 'CRT', 'SB4','SB1','SBM','SBV'})
			
	TRCell():New( oSection1, 'TIPO'					,'CRT', 		'TIPO',							    "@!"                        ,10)
	TRCell():New( oSection1, 'COD_GRUPO'			,'CRT', 		'COD_GRUPO',	  				    "@!"				        ,10)
	TRCell():New( oSection1, 'DESC_GRUPO'    		,'CRT', 		'DESC_GRUPO',		  			    "@!"				        ,04)
	TRCell():New( oSection1, 'SKU'					,'CRT', 		'SKU',				  			    "@!"				        ,15)
	TRCell():New( oSection1, 'REFERENCIA'			,'CRT', 		'REFERENCIA',		  			    "@!"				        ,15)
	TRCell():New( oSection1, 'COD_COR'				,'CRT', 		'COD_COR',			  			    "@!"				        ,35)
	TRCell():New( oSection1, 'DESC_COR'				,'CRT', 		'DESC_COR',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'TAM'					,'CRT', 		'TAM',				  			    "@!"				        ,35)
	TRCell():New( oSection1, 'UNID_MEDIDA'			,'CRT', 		'UNID_MEDIDA',		  			    "@!"				        ,04)
	TRCell():New( oSection1, 'ARMAZEN_PADRAO'		,'CRT', 		'ARMAZEN_PADRAO',	  			    "@!"				        ,10)
	TRCell():New( oSection1, 'ENTRA_MRP'			,'CRT', 		'ENTRA_MRP',		  			    "@!"				        ,25)
	TRCell():New( oSection1, 'SITUACAO'				,'CRT', 		'SITUACAO',		     			    "@!"				        ,10)
	TRCell():New( oSection1, 'DISPWEB_B2C'			,'CRT', 		'DISPWEB_B2C',	  			   	 	"@!"  				        ,06)
	TRCell():New( oSection1, 'DISPWEB_B2B'			,'CRT', 		'DISPWEB_B2B',		  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DATA_LIBERACAO'		,'CRT', 		'DATA_LIBERACAO',	  			    "@!"				        ,06)
	TRCell():New( oSection1, 'DATA_INATIVACAO'		,'CRT', 		'DATA_INATIVACAO',	  			    "@!"				        ,35)
	TRCell():New( oSection1, 'CICLO_PRODUCAO'		,'CRT', 		'CICLO_PRODUCAO',	  			    "@!"				        ,10)
	TRCell():New( oSection1, 'MARCA'				,'CRT', 		'MARCA',			  			    "@!"				        ,08)
	TRCell():New( oSection1, 'COLECAO'				,'CRT', 		'COLECAO',			  			    "@!"				        ,06)
	TRCell():New( oSection1, 'SUBCOLECAO'			,'CRT', 		'SUBCOLECAO',		  			    "@!"				        ,06)
	TRCell():New( oSection1, 'CATEGORIA'			,'CRT', 		'CATEGORIA',		  			    "@!"				        ,08)
	TRCell():New( oSection1, 'GRUPO_COMERCIA'		,'CRT', 		'GRUPO_COMERCIA',	  			    "@!"				        ,06)
	TRCell():New( oSection1, 'SUB_GRUPO'			,'CRT', 		'SUB_GRUPO',			  			"@!"				        ,06)
		
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("CRT") > 0
		CRT->(dbCloseArea())
	Endif
      
    cQuery := " SELECT " 
    cQuery += " SB4.B4_TIPO AS TIPO," 
    cQuery += " SBM.BM_GRUPO AS COD_GRUPO, "  
    cQuery += " SBM.BM_DESC AS DESC_GRUPO, "  
    cQuery += " SB1.B1_COD AS SKU, " 
    cQuery += " SB4.B4_COD AS REFERENCIA, " 
    cQuery += " LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AS COD_COR, "
    cQuery += " SBV.BV_DESCRI AS DESC_COR, "
    cQuery += " RIGHT(SB1.B1_COD,4)  AS TAM, "
    cQuery += " SB1.B1_UM AS UNID_MEDIDA, "
    cQuery += " SB1.B1_LOCPAD AS ARMAZEN_PADRAO, "
    cQuery += " SB1.B1_MRP AS ENTRA_MRP, "
    cQuery += " SB1.B1_YSITUAC AS SITUACAO, "
    cQuery += " SB4.B4_DWEB AS DISPWEB_B2C, "
    cQuery += " SB4.B4_DWEB2B AS DISPWEB_B2B, " 
    cQuery += " RIGHT(SB1.B1_YDATLIB,2)+'/'+SUBSTRING(SB1.B1_YDATLIB,5,2)+'/'+LEFT(SB1.B1_YDATLIB,4) AS DATA_LIBERACAO, "
    cQuery += " RIGHT(SB1.B1_YDATINA,2)+'/'+SUBSTRING(SB1.B1_YDATINA,5,2)+'/'+LEFT(SB1.B1_YDATINA,4)  AS DATA_INATIVACAO, "
    cQuery += " SB4.B4_YNCICLO AS CICLO_PRODUCAO, "
    cQuery += " SB4.B4_YNOMARC AS MARCA, "
    cQuery += " SB4.B4_YNCOLEC AS COLECAO, "
    cQuery += " SB4.B4_YNSUBCO AS SUBCOLECAO, "
    cQuery += " SB4.B4_YNCATEG AS CATEGORIA, "
    cQuery += " SB4.B4_YNGPCOM AS GRUPO_COMERCIA, "
    cQuery += " SB4.B4_YNSUBGR AS SUB_GRUPO " 
    cQuery += " FROM "+RetSqlName('SB4')+" SB4 (NOLOCK) "
    cQuery += " JOIN "+RetSqlName('SB1')+" SB1 (NOLOCK) ON LEFT(SB1.B1_COD,8)=SB4.B4_COD "
    cQuery += " JOIN "+RetSqlName('SBM')+" SBM (NOLOCK) ON SBM.BM_GRUPO=SB1.B1_GRUPO "
    cQuery += " JOIN "+RetSqlName('SBV')+" SBV (NOLOCK) ON SBV.BV_CHAVE=LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AND SBV.BV_TABELA='COR' "
    cQuery += " WHERE "
    cQuery += " SB1.D_E_L_E_T_=''  "
    cQuery += " AND SB4.D_E_L_E_T_='' "
    cQuery += " AND SBM.D_E_L_E_T_='' "
    cQuery += " AND SBV.D_E_L_E_T_='' " 
	
	if !Empty(mv_par01)
		cQuery += " AND B4_TIPO = '"+Alltrim(mv_par01)+"'  " 
	else
		cQuery += " "
	endif
	
	if !Empty(mv_par02) 
		cQuery += " AND B4_COD = '"+Alltrim(mv_par02)+"' "
	else
		cQuery += " "
	endif
	
	
	TCQUERY cQuery NEW ALIAS CRT

	While CRT->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("TIPO"):SetValue(CRT->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")

		oSection1:Cell("COD_GRUPO"):SetValue(CRT->COD_GRUPO)
		oSection1:Cell("COD_GRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_GRUPO"):SetValue(CRT->DESC_GRUPO)
		oSection1:Cell("DESC_GRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(CRT->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(CRT->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
				
		oSection1:Cell("COD_COR"):SetValue(CRT->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(CRT->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(CRT->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
		
		oSection1:Cell("UNID_MEDIDA"):SetValue(CRT->UNID_MEDIDA)
		oSection1:Cell("UNID_MEDIDA"):SetAlign("LEFT")
		
		oSection1:Cell("ARMAZEN_PADRAO"):SetValue(CRT->ARMAZEN_PADRAO)
		oSection1:Cell("ARMAZEN_PADRAO"):SetAlign("LEFT")
		
		oSection1:Cell("ENTRA_MRP"):SetValue(CRT->ENTRA_MRP)
		oSection1:Cell("ENTRA_MRP"):SetAlign("LEFT")
		
		oSection1:Cell("SITUACAO"):SetValue(CRT->SITUACAO)
		oSection1:Cell("SITUACAO"):SetAlign("LEFT")
		
		oSection1:Cell("DISPWEB_B2C"):SetValue(CRT->DISPWEB_B2C)
		oSection1:Cell("DISPWEB_B2C"):SetAlign("LEFT")
		
		oSection1:Cell("DISPWEB_B2B"):SetValue(CRT->DISPWEB_B2B)
		oSection1:Cell("DISPWEB_B2B"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_LIBERACAO"):SetValue(CRT->DATA_LIBERACAO)
		oSection1:Cell("DATA_LIBERACAO"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_INATIVACAO"):SetValue(CRT->DATA_INATIVACAO)
		oSection1:Cell("DATA_INATIVACAO"):SetAlign("LEFT")
		
		oSection1:Cell("CICLO_PRODUCAO"):SetValue(CRT->CICLO_PRODUCAO)
		oSection1:Cell("CICLO_PRODUCAO"):SetAlign("LEFT")
		
		oSection1:Cell("MARCA"):SetValue(CRT->MARCA)
		oSection1:Cell("MARCA"):SetAlign("LEFT")
		
		oSection1:Cell("COLECAO"):SetValue(CRT->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("SUBCOLECAO"):SetValue(CRT->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("CATEGORIA"):SetValue(CRT->CATEGORIA)
		oSection1:Cell("CATEGORIA"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO_COMERCIA"):SetValue(CRT->GRUPO_COMERCIA)
		oSection1:Cell("GRUPO_COMERCIA"):SetAlign("LEFT")
		
		oSection1:Cell("SUB_GRUPO"):SetValue(CRT->SUB_GRUPO)
		oSection1:Cell("SUB_GRUPO"):SetAlign("LEFT")		
												
		oSection1:PrintLine()
		
		CRT->(DBSKIP()) 
	enddo
	CRT->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Tipo de Produto?"            	,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Referencia ? "	                ,""		,""		,"mv_ch2","C",15,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
Return
