//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio Boletim de Recebimento.
Rotina para acompanhamento das entradas de notas de MP;

@author Ronaldo Pereira
@since 20 de Abril de 2020
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL002()

	Local aRet := {}
	Local aPerg := {}
	 
    aAdd(aPerg,{1,"Data Emiss�o de"		,dDataBase,PesqPict("SF1", "F1_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) 
	aAdd(aPerg,{1,"Data Emiss�o At�" 	,dDataBase,PesqPict("SF1", "F1_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) 
	aAdd(aPerg,{1,"Nota Fiscal de: " 	,Space(9),""   ,""		,""	    ,""		,0	,.F.}) 
	aAdd(aPerg,{1,"Nota Fiscal at�: " 	,Space(9),""   ,""		,""	    ,""		,0	,.F.})
	aAdd(aPerg,{1,"Pedido de: " 		,Space(6),""   ,""		,""	    ,""		,0	,.F.})
	aAdd(aPerg,{1,"Pedido at�: " 		,Space(6),""   ,""		,""	    ,""		,0	,.F.}) 
	aAdd(aPerg,{1,"OP: " 		        ,Space(6),""   ,""		,""	    ,""		,0	,.F.})  

	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'BOLETIM'+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "BASE"
    Local cTable1    	:= "Boletim de Recebimento" 

        
    //Dados tabela 1          
	cQry := " SELECT "
	cQry += "   D1_FILIAL AS FILIAL, "
	cQry += "   D1_LOCAL AS 'LOCAL', "
	cQry += "   D1_DOC AS 'NOTA_FISCAL', "
	cQry += "   D1_PEDIDO AS 'NUM_PEDIDO', "
	cQry += "   '' AS OP, "
	cQry += "   B1_COD AS 'COD_PRODUTO', "
	cQry += "   B1_DESC AS 'DESCRICAO', "
	cQry += "   B1_UM AS 'UNI_MEDIDA', "
	cQry += "   CONVERT(char, CAST(SF1.F1_EMISSAO AS smalldatetime), 103) AS 'DT_EMISSAO', "
	cQry += "   LEFT(B1_COD, 8) AS 'REFERENCIA', "
	cQry += "   CONVERT(char, CAST(SF1.F1_DTDIGIT AS smalldatetime), 103) AS 'DT_RECEBIMENTO', "
	cQry += "   CASE "
	cQry += "       WHEN SF1.F1_DTLANC <> '' THEN CONVERT(CHAR, CAST(SF1.F1_DTLANC AS SMALLDATETIME),103) "
	cQry += "       ELSE ''"
	cQry += "   END 'DT_LANCAMENTO',"	
	cQry += "   BM_GRUPO AS COD_GRUPO, "
	cQry += "   SF4.F4_CODIGO AS TES, "
	cQry += "   SF4.F4_TEXTO AS DESC_TES, "
	cQry += "   BM_DESC AS DESC_GRUPO, "
	cQry += "   BV_DESCRI AS 'COR', "
	cQry += "   BV_CHAVE AS 'COD_COR', "
	cQry += "   CASE "
	cQry += "     WHEN RIGHT(SB1.B1_COD, 4) LIKE '000%' THEN REPLACE(RIGHT(SB1.B1_COD, 4), '000', '') "
	cQry += "     WHEN RIGHT(SB1.B1_COD, 4) LIKE '00%' THEN REPLACE(RIGHT(SB1.B1_COD, 4), '00', '') "
	cQry += "     WHEN RIGHT(SB1.B1_COD, 4) LIKE '0%' THEN SUBSTRING(RIGHT(SB1.B1_COD, 4), 2, 4) "
	cQry += "     ELSE REPLACE(RIGHT(SB1.B1_COD, 4), '00', '') "
	cQry += "   END TAMANHO, "
	cQry += "   B1_TIPO AS 'TIPO', "
	cQry += "   D1_QUANT AS 'QUANTIDADE', "
	cQry += "   SUM(D1_TOTAL) AS 'VALOR_NF', "
	cQry += "   D1_FORNECE AS 'COD_FORNECE', "
	cQry += "   A2_NOME AS 'NOME' "
	cQry += " FROM "+RetSqlName("SD1")+" AS SD1 WITH (NOLOCK) "
	cQry += " JOIN "+RetSqlName("SB1")+" AS SB1 WITH (NOLOCK) "
	cQry += "   ON (SD1.D1_COD = SB1.B1_COD "
	cQry += "   AND SB1.D_E_L_E_T_ = '') "
	cQry += " JOIN "+RetSqlName("SBV")+" AS SBV WITH (NOLOCK) "
	cQry += "   ON (SUBSTRING(B1_COD, 9, 3) = SBV.BV_CHAVE "
	cQry += "   AND SBV.BV_TABELA = 'COR' "
	cQry += "   AND SBV.D_E_L_E_T_ = '') "
	cQry += " JOIN "+RetSqlName("SA2")+" AS SA2 WITH (NOLOCK) "
	cQry += "   ON (D1_FORNECE = A2_COD "
	cQry += "   AND D1_LOJA = A2_LOJA "
	cQry += "   AND SA2.D_E_L_E_T_ = '') "
	cQry += " JOIN "+RetSqlName("SBM")+" AS SBM WITH (NOLOCK) "
	cQry += "   ON (BM_GRUPO = B1_GRUPO "
	cQry += "   AND SBM.D_E_L_E_T_ = '') "
	cQry += " JOIN "+RetSqlName("SF4")+" AS SF4 WITH (NOLOCK) "
	cQry += "   ON (F4_CODIGO = D1_TES "
	cQry += "   AND SF4.D_E_L_E_T_ = '') "
	cQry += " JOIN "+RetSqlName("SF1")+" AS SF1 WITH (NOLOCK) "
	cQry += "   ON (F1_DOC = D1_DOC "
	cQry += "   AND F1_FORNECE = D1_FORNECE "
	cQry += "   AND F1_LOJA = D1_LOJA "
	cQry += "   AND F1_FILIAL = D1_FILIAL "
	cQry += "   AND SF1.D_E_L_E_T_ = '') "
	cQry += " WHERE SD1.D_E_L_E_T_ = '' "
	cQry += " AND SB1.B1_TIPO = 'MP' "
	cQry += " AND SD1.D1_LOCAL IN ('A1','98') "	
	cQry += " AND F1_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"' "

    If !Empty(aRet[3])
    	cQry += " AND D1_DOC BETWEEN '"+Alltrim(aRet[3])+"' AND '"+Alltrim(aRet[4])+"' "
    EndIf

	If !Empty(aRet[5])
    	cQry += " AND D1_PEDIDO BETWEEN '"+Alltrim(aRet[5])+"' AND '"+Alltrim(aRet[6])+"' "
    EndIf

	If !Empty(aRet[7])
    	cQry += " AND D1_OP LIKE '"+Alltrim(aRet[7])+"%' "
    EndIf
	 
	cQry += " GROUP BY D1_DOC, "
	cQry += "          D1_FILIAL, "
	cQry += "          D1_LOCAL, "
	cQry += "          D1_PEDIDO, "
	cQry += "          B1_COD, "
	cQry += "          B1_DESC, "
	cQry += "          B1_UM, "
	cQry += "          F1_EMISSAO, "
	cQry += "          B1_COD, "
	cQry += "          F1_DTDIGIT, "
	cQry += "          F1_DTLANC, "
	cQry += "          BM_GRUPO, "
	cQry += "          SF4.F4_CODIGO, "
	cQry += "          SF4.F4_TEXTO, "
	cQry += "          BM_DESC, "
	cQry += "          BV_DESCRI, "
	cQry += "          BV_CHAVE, "
	cQry += "          B1_TIPO, "
	cQry += "          D1_QUANT, "
	cQry += "          D1_FORNECE, "
	cQry += "          A2_NOME "
	
	cQry += " UNION  "
	 
	cQry += " SELECT  "
	cQry += "   D1_FILIAL AS FILIAL, "
	cQry += "   D1_LOCAL AS 'LOCAL', "
	cQry += "   D1_DOC AS 'NOTA_FISCAL',  "
	cQry += "   D1_PEDIDO AS 'NUM_PEDIDO', "
	cQry += "   C2.C2_NUM AS OP, "
	cQry += "   B1_COD AS 'COD_PRODUTO', "
	cQry += "   B1_DESC AS 'DESCRICAO', "
	cQry += "   B1_UM AS 'UNI_MEDIDA', "
	cQry += "   CONVERT(char, CAST(SF1.F1_EMISSAO AS smalldatetime), 103) AS 'DT_EMISSAO', "
	cQry += "   LEFT(B1_COD, 8) AS 'REFERENCIA', "
	cQry += "   CONVERT(char, CAST(SF1.F1_DTDIGIT AS smalldatetime), 103) AS 'DT_RECEBIMENTO', "
	cQry += "   CASE "
	cQry += "       WHEN SF1.F1_DTLANC <> '' THEN CONVERT(CHAR, CAST(SF1.F1_DTLANC AS SMALLDATETIME),103) "
	cQry += "       ELSE ''"
	cQry += "   END 'DT_LANCAMENTO',"
	cQry += "   BM_GRUPO AS COD_GRUPO, "
	cQry += "   SF4.F4_CODIGO AS TES, "
	cQry += "   SF4.F4_TEXTO AS DESC_TES, "
	cQry += "   BM_DESC AS DESC_GRUPO, "
	cQry += "   BV_DESCRI AS 'COR', "
	cQry += "   BV_CHAVE AS 'COD_COR', "
	cQry += "   CASE "
	cQry += "     WHEN RIGHT(SB1.B1_COD, 4) LIKE '000%' THEN REPLACE(RIGHT(SB1.B1_COD, 4), '000', '') "
	cQry += "     WHEN RIGHT(SB1.B1_COD, 4) LIKE '00%' THEN REPLACE(RIGHT(SB1.B1_COD, 4), '00', '') "
	cQry += "     WHEN RIGHT(SB1.B1_COD, 4) LIKE '0%' THEN SUBSTRING(RIGHT(SB1.B1_COD, 4), 2, 4) "
	cQry += "     ELSE REPLACE(RIGHT(SB1.B1_COD, 4), '00', '') "
	cQry += "   END TAMANHO, "
	cQry += "   B1_TIPO AS 'TIPO', "
	cQry += "   D1_QUANT AS 'QUANTIDADE', "
	cQry += "   SUM(D1_TOTAL) AS 'VALOR_NF', "
	cQry += "   D1_FORNECE AS 'COD_FORNECE', "
	cQry += "   A2_NOME AS 'NOME' "
	cQry += " FROM "+RetSqlName("SC2")+" AS C2 WITH (NOLOCK) "
	cQry += " INNER JOIN "+RetSqlName("SB1")+" AS SB1 (NOLOCK) "
	cQry += "   ON (C2_PRODUTO = SB1.B1_COD "
	cQry += "   AND SB1.D_E_L_E_T_ = '') "
	cQry += " INNER JOIN "+RetSqlName("SBV")+" AS SBV WITH (NOLOCK) "
	cQry += "   ON (SUBSTRING(C2_PRODUTO, 9, 3) = SBV.BV_CHAVE "
	cQry += "   AND SBV.BV_TABELA = 'COR' "
	cQry += "   AND SBV.D_E_L_E_T_ = '') "
	cQry += " INNER JOIN "+RetSqlName("SD1")+" AS SD1 WITH (NOLOCK) "
	cQry += "   ON (C2_NUM + C2_ITEM + C2_SEQUEN + C2_ITEMGRD = SD1.D1_OP "
	cQry += "   AND SD1.D_E_L_E_T_ = '') "
	cQry += " INNER JOIN "+RetSqlName("SA2")+" AS SA2 WITH (NOLOCK) "
	cQry += "   ON (D1_FORNECE = A2_COD "
	cQry += "   AND D1_LOJA = A2_LOJA "
	cQry += "   AND SA2.D_E_L_E_T_ = '') "
	cQry += " INNER JOIN "+RetSqlName("SBM")+" AS SBM WITH (NOLOCK) "
	cQry += "   ON (BM_GRUPO = B1_GRUPO "
	cQry += "   AND SBM.D_E_L_E_T_ = '') "
	cQry += " INNER JOIN "+RetSqlName("SF4")+" AS SF4 WITH (NOLOCK) "
	cQry += "   ON (F4_CODIGO = D1_TES "
	cQry += "   AND SF4.D_E_L_E_T_ = '') "
	cQry += " INNER JOIN "+RetSqlName("SF1")+" AS SF1 WITH (NOLOCK) "
	cQry += "   ON (F1_DOC = D1_DOC "
	cQry += "   AND F1_FORNECE = D1_FORNECE "
	cQry += "   AND F1_LOJA = D1_LOJA "
	cQry += "   AND F1_FILIAL = D1_FILIAL "
	cQry += "   AND SF1.D_E_L_E_T_ = '') "
	cQry += " WHERE C2.D_E_L_E_T_ = '' "
	cQry += " AND SB1.B1_TIPO = 'MB' "
	//cQry += " AND SD1.D1_LOCAL IN ('A1','98') "	 
	cQry += " AND SF1.F1_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"' "

    If !Empty(aRet[3])
    	cQry += " AND D1_DOC BETWEEN '"+Alltrim(aRet[3])+"' AND '"+Alltrim(aRet[4])+"' "
    EndIf

	If !Empty(aRet[5])
    	cQry += " AND D1_PEDIDO BETWEEN '"+Alltrim(aRet[5])+"' AND '"+Alltrim(aRet[6])+"' "
    EndIf

	If !Empty(aRet[7])
    	cQry += " AND D1_OP LIKE '"+Alltrim(aRet[7])+"%' "
    EndIf
	
	 
	cQry += " GROUP BY D1_DOC, "
	cQry += "          D1_FILIAL, "
	cQry += "          D1_LOCAL, "
	cQry += "          D1_PEDIDO, "
	cQry += "          C2.C2_NUM, "
	cQry += "          B1_COD, "
	cQry += "          B1_DESC, "
	cQry += "          B1_UM, "
	cQry += "          F1_EMISSAO, "
	cQry += "          B1_COD, "
	cQry += "          F1_DTDIGIT, "
	cQry += "          F1_DTLANC, "
	cQry += "          BM_GRUPO, "
	cQry += "          SF4.F4_CODIGO, "
	cQry += "          SF4.F4_TEXTO, "
	cQry += "          BM_DESC, "
	cQry += "          BV_DESCRI, "
	cQry += "          BV_CHAVE, "
	cQry += "          B1_TIPO, "
	cQry += "          D1_QUANT, "
	cQry += "          D1_FORNECE, "
	cQry += "          A2_NOME "
                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas      
		oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)
		oFWMsExcel:AddColumn(cPlan1,cTable1,"LOCAL",1)		
		oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_EMISSAO",1)	
		oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_RECEBIMENTO",1)
		oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_LANCAMENTO",1)
		oFWMsExcel:AddColumn(cPlan1,cTable1,"NOTA_FISCAL",1)	
		oFWMsExcel:AddColumn(cPlan1,cTable1,"NUM_PEDIDO",1)   
		oFWMsExcel:AddColumn(cPlan1,cTable1,"OP",1)      		
		oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_PRODUTO",1) 
		oFWMsExcel:AddColumn(cPlan1,cTable1,"REFERENCIA",1)   
		oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCRICAO",1)    
		oFWMsExcel:AddColumn(cPlan1,cTable1,"UNI_MEDIDA",1)   
		oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_COR",1)	   
		oFWMsExcel:AddColumn(cPlan1,cTable1,"COR",1) 			
		oFWMsExcel:AddColumn(cPlan1,cTable1,"TAMANHO",1) 		
		oFWMsExcel:AddColumn(cPlan1,cTable1,"QUANTIDADE",1)	
		oFWMsExcel:AddColumn(cPlan1,cTable1,"VALOR_NF",1)	   
		oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO",1)	       
		oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_GRUPO",1)    
		oFWMsExcel:AddColumn(cPlan1,cTable1,"DESC_GRUPO",1)   
		oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_FORNECE",1)	
		oFWMsExcel:AddColumn(cPlan1,cTable1,"NOME",1)	    	
		oFWMsExcel:AddColumn(cPlan1,cTable1,"TES",1)	    	
		oFWMsExcel:AddColumn(cPlan1,cTable1,"DESC_TES",1)

                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
												QRY->FILIAL,;
												QRY->LOCAL,;
												QRY->DT_EMISSAO,;		
												QRY->DT_RECEBIMENTO,;
												QRY->DT_LANCAMENTO,;
												QRY->NOTA_FISCAL,;	
												QRY->NUM_PEDIDO,;   
												QRY->OP,;      		
												QRY->COD_PRODUTO,; 
												QRY->REFERENCIA,;   
												QRY->DESCRICAO,;    
												QRY->UNI_MEDIDA,;   
												QRY->COD_COR,;	   
												QRY->COR,; 			
												QRY->TAMANHO,; 		
												QRY->QUANTIDADE,;	
												QRY->VALOR_NF,;	   
												QRY->TIPO,;	       
												QRY->COD_GRUPO,;    
												QRY->DESC_GRUPO,;   
												QRY->COD_FORNECE,;	
												QRY->NOME,;	    	
												QRY->TES,;	    	
												QRY->DESC_TES;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return