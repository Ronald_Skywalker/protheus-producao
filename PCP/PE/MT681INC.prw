#include "rwmake.ch"

User Function MT681INC()

	Local cTipo := ""
	Local cCiclo := ""

	_area := GetArea()


	If FUNNAME() != 'HPCPP001'
		DbSelectArea("SC2")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SC2")+alltrim(SH6->H6_OP))

		DbSelectArea("SG2")
		DbSetOrder(3)
		DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+SH6->H6_OPERAC)

		If !Found()
			DbSelectArea("SG2")
			DbSetOrder(8)
			DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+SH6->H6_OPERAC)
		Endif

		If alltrim(SG2->G2_PRODUTO) == alltrim(SC2->C2_PRODUTO) .or. SubStr(SC2->C2_PRODUTO,1,8) == alltrim(SG2->G2_REFGRD)
			_ultimo := .F.
		Else
			_ultimo := .T.
		Endif

		If _ultimo .AND. alltrim(SC2->C2_YFORNEC) == ""
			_qry := "UPDATE "+RetSqlName("SD4")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ WHERE D4_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D4_COD like 'B"+SubStr(SC2->C2_PRODUTO,2,7)+"%'"
			TcSqlExec(_qry)
		Endif

		_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
		_qry += "FROM "+RetSqlName("SD3")+" SD3 "
		_qry += "INNER JOIN "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' AND C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
		_qry += "INNER JOIN "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' AND H6_OP = D3_OP AND D3_OP = H6_OP AND C2_PRODUTO = H6_PRODUTO AND D3_EMISSAO = H6_DATAFIN AND D3_IDENT = H6_IDENT "
		_qry += "INNER JOIN "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' AND G2_REFGRD = left(C2_PRODUTO,8) AND G2_OPERAC = H6_OPERAC "
		_qry += "WHERE SD3.D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD like 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "
		TcSqlExec(_qry)

		_qry := "SELECT * FROM "+RetSqlName("SD3")+" WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD like 'MOD%' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = '' "

		If SELECT("TMPSD3") > 0
			TMPSD3->(DbCloseArea())
		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
		DbSelectArea("TMPSD3")
		DbGoTop()

		While !EOF()

			cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
			cProduto:= GetMV("MV_XPRDGGF")
			If GetMV("MV_DOCSEQ") <> cDocSeq
				PutMV("MV_DOCSEQ",cDocSeq)
			EndIf

			DbSelectArea("SD3")
			RecLock("SD3",.T.)
			SD3->D3_FILIAL  := xFilial("SD3")
			SD3->D3_TM      := TMPSD3->D3_TM
			SD3->D3_COD     := cProduto
			SD3->D3_UM      := TMPSD3->D3_UM
			SD3->D3_QUANT   := TMPSD3->D3_QUANT
			SD3->D3_OP		:= TMPSD3->D3_OP
			SD3->D3_CF      := TMPSD3->D3_CF
			SD3->D3_CONTA   := TMPSD3->D3_CONTA
			SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
			SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
			SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
			SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
			SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
			SD3->D3_NUMSEQ  := cDocseq
			SD3->D3_TIPO    := TMPSD3->D3_TIPO
			SD3->D3_USUARIO := UsrRetName(__cUserID)
			SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
			SD3->D3_YOPERAC	:= SH6->H6_OPERAC
			MsUnlock()

			_qry := "UPDATE "+RetSqlName("SD3")+" SET D3_XGGF = 'S',D3_YOPERAC = '"+SH6->H6_OPERAC+"' WHERE R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
			TcSqlExec(_qry)

			DbSelectArea("TMPSD3")
			DbSkip()
		EndDo
		TMPSD3->(DbCloseArea())
	EndIf

	If SH6->H6_OPERAC = "30" .AND. LEFT(SC2->C2_RECURSO,3) = "CTH" .AND. FUNNAME() == 'HPCPP001' //  TODO CHAMADO 15594 SOLIC. ALISSON SOUZA 30-06-21 IN�CIO

		cTipo := SB4->B4_TIPO
		cCiclo := Alltrim(SB4->B4_YCICLO)


		If 	cTipo $ GetMv("HP_TPAPONT") .AND. cCiclo == "00001"

			Reclock("SD3",.F.)
			SD3->D3_COD := "MOD001115001"
			SD3->(MsUnLock())

		ElseIf cTipo $ GetMv("HP_TPAPONT") .AND. cCiclo != "00001"

			Reclock("SD3",.F.)
			SD3->D3_COD := "MOD001115050"
			SD3->(MsUnLock())

		ElseIf cTipo $ GetMv("HP_TPAPON2")

			cQuery :=" SELECT LEFT(C2_PRODUTO,8) AS PROD FROM "+RetSqlName("SC2")+" (NOLOCK) WHERE D_E_L_E_T_ = ''  "
			cQuery +=" AND C2_NUM = '"+SC2->C2_NUM+"' AND C2_ITEM = '"+SC2->C2_ITEM+"' AND C2_SEQUEN = '001' GROUP BY LEFT(C2_PRODUTO,8) "

			If SELECT("TMPSC2") > 0
				TMPSC2->(DbCloseArea())
			EndIf

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSC2",.T.,.T.)

			cRefPF := TMPSC2->PROD

			cCiclo := Alltrim(Posicione("SB4",1,xFilial("SB4")+cRefPF,"B4_YCICLO"))

			If 	cCiclo == "00001"

				Reclock("SD3",.F.)
				SD3->D3_COD := "MOD001115001"
				SD3->(MsUnLock())
			Else
				Reclock("SD3",.F.)
				SD3->D3_COD := "MOD001115050"
				SD3->(MsUnLock())
			EndIf

		EndIf

	ElseIf SH6->H6_OPERAC = "30" .AND. LEFT(SC2->C2_RECURSO,3) = "CTH" .AND. FUNNAME() != 'HPCPP001'

		_qry := "SELECT * FROM "+RetSqlName("SD3")+" WHERE D_E_L_E_T_ = '' AND D3_COD <> 'MODGGF' AND D3_COD like 'MOD%' "
		_qry += "AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_XGGF = 'S' AND D3_YOPERAC = '30' "

		If SELECT("TMPSD3") > 0
			TMPSD3->(DbCloseArea())
		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
		DbSelectArea("TMPSD3")
		DbGoTop()

		While !EOF()

			cTipo := SB4->B4_TIPO
			cCiclo := Alltrim(SB4->B4_YCICLO)


			If 	cTipo $ GetMv("HP_TPAPONT") .AND. cCiclo == "00001"

				SD3->( dbGoTo( TMPSD3->R_E_C_N_O_ ) )
				Reclock("SD3",.F.)
				SD3->D3_COD := "MOD001115001"
				SD3->(MsUnLock())

			ElseIf cTipo $ GetMv("HP_TPAPONT") .AND. cCiclo != "00001"
				SD3->( dbGoTo( TMPSD3->R_E_C_N_O_ ) )
				Reclock("SD3",.F.)
				SD3->D3_COD := "MOD001115050"
				SD3->(MsUnLock())

			ElseIf cTipo $ GetMv("HP_TPAPON2")

				cQuery :=" SELECT LEFT(C2_PRODUTO,8) AS PROD FROM "+RetSqlName("SC2")+" (NOLOCK) WHERE D_E_L_E_T_ = ''  "
				cQuery +=" AND C2_NUM = '"+SC2->C2_NUM+"' AND C2_ITEM = '"+SC2->C2_ITEM+"' AND C2_SEQUEN = '001' GROUP BY LEFT(C2_PRODUTO,8) "

				If SELECT("TMPSC2") > 0
					TMPSC2->(DbCloseArea())
				EndIf

				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSC2",.T.,.T.)

				cRefPF := TMPSC2->PROD

				cCiclo := Alltrim(Posicione("SB4",1,xFilial("SB4")+cRefPF,"B4_YCICLO"))

				If 	cCiclo == "00001"
					SD3->( dbGoTo( TMPSD3->R_E_C_N_O_ ) )
					Reclock("SD3",.F.)
					SD3->D3_COD := "MOD001115001"
					SD3->(MsUnLock())
				Else
					SD3->( dbGoTo( TMPSD3->R_E_C_N_O_ ) )
					Reclock("SD3",.F.)
					SD3->D3_COD := "MOD001115050"
					SD3->(MsUnLock())
				EndIf

			EndIf
			DbSelectArea("TMPSD3")
			DbSkip()
		EndDo
		TMPSD3->(DbCloseArea())
	Endif //  TODO CHAMADO 15594 SOLIC. ALISSON SOUZA 30-06-21 FIM

	RestArea(_area)
Return
