#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"

User Function MT680GEFTR()
	If FUNNAME() != "MATA681"
	
		_qry := " UPDATE "+RetSqlName("SD3")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ "
		_qry += " WHERE D_E_L_E_T_ = '' AND D3_COD LIKE 'MOD%' "
		_qry += " AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_YOPERAC = '"+_par03+"' "
		TcSqlExec(_qry)
	
	Else
		_par03:= SH6->H6_OPERAC
		_qtdD3 := quantD3()

		_qry := " UPDATE "+RetSqlName("SD3")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ "
		_qry += " WHERE D_E_L_E_T_ = '' AND D3_COD = 'MODGGF' "
		_qry += " AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_YOPERAC = '"+_par03+"' AND D3_QUANT = "+cvaltochar(_qtdD3)+" "
		TcSqlExec(_qry)

		_qry := " UPDATE "+RetSqlName("SD3")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ "
		_qry += " WHERE D_E_L_E_T_ = '' AND LEFT(D3_COD,3) = 'MOD' "
		_qry += " AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' AND D3_QUANT = "+cvaltochar(_qtdD3)+" "
		TcSqlExec(_qry)

	EndIf

Return

Static Function quantD3()


	_qry := "SELECT TOP 1 round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*"+cvaltochar(SH6->H6_QTDPROD)+",2) AS QTDD3 "
	_qry += "from "+RetSqlName("SD3")+" SD3 "
	_qry += "inner join "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' and C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
	_qry += "inner join "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' and '"+cvaltochar(SH6->H6_OP)+"' = D3_OP and C2_PRODUTO = '"+cvaltochar(SH6->H6_PRODUTO)+"' and D3_EMISSAO = "+DTOS(SH6->H6_DATAFIN)+"  "
	_qry += "inner join "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' and G2_REFGRD = left(C2_PRODUTO,8) and G2_OPERAC = "+cvaltochar(SH6->H6_OPERAC)+" "
	_qry += "where SD3.D_E_L_E_T_ = '' AND D3_COD = 'MODGGF' AND D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' AND D3_YOPERAC = '"+SH6->H6_OPERAC+"' "

	If Select("TMPQD3") > 0
		TMPQD3->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPQD3",.T.,.T.)
	cQuant := TMPQD3->QTDD3

Return	cQuant
