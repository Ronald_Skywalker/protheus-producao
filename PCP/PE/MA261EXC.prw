#Include 'Protheus.ch'

User Function MA261EXC()

_area := GetArea()

	_qry := "update "+RetSqlName("SC2")+" set C2_XRES = 'L' where C2_NUM+C2_ITEM+C2_SEQUEN = '"+left(SD3->D3_XOP,11)+"' "
	TcSqlExec(_qry)
    		
/*	_qry := "update "+RetSqlName("SB2")+" set B2_XRES = B2_XRES + "
	_qry += "(Select Sum(D4_QUANT) from "+RetSqlName("SD4")+" SD4 where SD4.D_E_L_E_T_ = '' and "
	_qry += "B2_COD = D4_COD and D4_FILIAL = '"+xfilial("SD4")+"' and left(D4_OP,11) = '"+left(SD3->D3_XOP,11)+"') "
 	_qry += "from "+RetSqlName("SB2")+" SB2 where SB2.D_E_L_E_T_ = '' and B2_FILIAL = '"+xfilial("SB2")+"' and "
 	_qry += "B2_COD = '"+SD3->D3_COD+"' and B2_LOCAL = 'A1' and "
    _qry += "(Select Sum(D4_QUANT) from "+RetSqlName("SD4")+" SD4 where D4_FILIAL = '"+xfilial("SD4")+"' and SD4.D_E_L_E_T_ = '' and "
    _qry += "B2_COD = D4_COD and left(D4_OP,11) = '"+left(SD3->D3_XOP,11)+"') is not null " 
	TcSqlExec(_qry)
*/

	cGrpLib := AllTrim(SuperGetMV("MV_XGRPNLB",.F.,"MP05|MP16|MP17|MP91|MP92|MP30|MP98")) //Grupo de Produtos que n�o devem ser considerados para libera��o de OP
	cTpLib  := AllTrim(SuperGetMV("MV_XTPNLB",.F.,"MI|PI|SV")) // Tipo de Produtos que n�o devem ser considerados na libera��o da OP
	cArmLib := AllTrim(SuperGetMV("MV_XARMLIB",.F.,"A1")) // Armaz�m que deve ser considerado para a Libera��o da OP
	xGrpLib := StrTran(cGrpLib,"|","','")
	xTpLib := StrTran(cTpLib,"|","','")


	cQuery := "UPDATE "+RetSqlName("SB2")+" SET B2_XRES = 0 WHERE D_E_L_E_T_ = '' AND B2_XRES <> 0 and B2_COD = '"+SD3->D3_COD+"' "
	TcSqlExec(cQuery)

	cQuery := "UPDATE SB2010 SET B2_XRES =  "
	cQuery += "(SELECT ISNULL(SUM(D4_XRES),0) from "+RetSqlName("SD4")+" SD4 "
	cQuery += "inner join "+RetSqlName("SC2")+" SC2 WITH(NOLOCK) ON C2_FILIAL = '"+xfilial("SC2")+"' and SC2.D_E_L_E_T_ = '' and C2_NUM = left(D4_OP,6) and C2_ITEM = Substring(D4_OP,7,2) and C2_SEQUEN = Substring(D4_OP,9,3) and C2_ITEMGRD = SubString(D4_OP,12,3) "
	cQuery += "inner join "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) ON SB1.D_E_L_E_T_ = '' AND B1_COD = D4_COD "
	cQuery += "where SD4.D_E_L_E_T_ = '' and C2_XRES in ('L') and Substring(D4_COD,1,1) <> 'B' and D4_FILIAL = '"+XfILIAL("SD4")+"' "
	cQuery += "AND B1_TIPO NOT IN ('"+xTpLib+"')AND SB1.B1_GRUPO NOT IN ('"+xGrpLib+"') "
	cQuery += "and D4_COD = B2_COD) "
	cQuery += "FROM "+RetSqlName("SB2")+" SB2 WHERE B2_FILIAL = '"+xfilial("SB2")+"' and SB2.D_E_L_E_T_ = '' AND B2_LOCAL = 'A1' AND B2_COD = '"+SD3->D3_COD+"' "
	
	TcSqlExec(cQuery)

	_qry := "update "+RetSqlName("SD4")+" set D4_XDTTRA = '', D4_XQTRANS = 0 "
	_qry += "where D4_FILIAL = '"+xfilial("SD4")+"' and D_E_L_E_T_ = '' and "
	_qry += "left(D4_OP,11) = '"+left(SD3->D3_XOP,11)+"' and D4_COD = '"+SD3->D3_COD+"' "
	TcSqlExec(_qry)


RestArea(_area)

Return
