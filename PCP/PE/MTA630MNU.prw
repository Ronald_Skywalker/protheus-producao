#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"

User Function MTA630MNU()

aAdd(aRotina,{ "Copia Refer�ncia", "U_HCOPREF", 0 , 2, 0, .F.})	

Return()

User Function HCOPREF

_Ref := SG2->G2_REFGRD
AjustaSX1("COPROT")
Pergunte("COPROT",.T.)

_qry := "Select * from "+RetSqlName("SG2")+" where D_E_L_E_T_ = '' and G2_REFGRD = '"+_ref+"' "

_qry := ChangeQuery(_qry)
If Select("TMPSG2") > 0 
	TMPSG2->(DbCloseArea()) 
EndIf
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSG2",.T.,.T.)
						
DbSelectArea("TMPSG2")
DbGoTop()

While !EOF()
	RecLock("SG2",.T.)
		Replace G2_FILIAL		with xfilial("SG2")
		Replace G2_CODIGO		with TMPSG2->G2_CODIGO
		Replace G2_PRODUTO		with ""
		Replace G2_OPERAC		with TMPSG2->G2_OPERAC
		Replace G2_RECURSO		with TMPSG2->G2_RECURSO
		Replace G2_FERRAM		with TMPSG2->G2_FERRAM
		Replace G2_LINHAPR		with TMPSG2->G2_LINHAPR
		Replace G2_TPALOCF		with TMPSG2->G2_TPALOCF
		Replace G2_TPLINHA		with TMPSG2->G2_TPLINHA
		Replace G2_DESCRI		with TMPSG2->G2_DESCRI
		Replace G2_MAOOBRA		with TMPSG2->G2_MAOOBRA
		Replace G2_DEPTO		with TMPSG2->G2_DEPTO
		Replace G2_SETUP		with TMPSG2->G2_SETUP
		Replace G2_FORMSTP		with TMPSG2->G2_FORMSTP
		Replace G2_LOTEPAD		with TMPSG2->G2_LOTEPAD
		Replace G2_TEMPAD		with TMPSG2->G2_TEMPAD
		Replace G2_TPOPER		with TMPSG2->G2_TPOPER
		Replace G2_TPSOBRE		with TMPSG2->G2_TPSOBRE
		Replace G2_TEMPSOB		with TMPSG2->G2_TEMPSOB
		Replace G2_TPDESD		with TMPSG2->G2_TPDESD
		Replace G2_TEMPDES		with TMPSG2->G2_TEMPDES
		Replace G2_DESPROP		with TMPSG2->G2_DESPROP
		Replace G2_CTRAB		with TMPSG2->G2_CTRAB
		Replace G2_OPE_OBR		with TMPSG2->G2_OPE_OBR
		Replace G2_SEQ_OBR		with TMPSG2->G2_SEQ_OBR
		Replace G2_LAU_OBR		with TMPSG2->G2_LAU_OBR
		Replace G2_REVIPRD		with TMPSG2->G2_REVIPRD
		Replace G2_CHAVE		with TMPSG2->G2_CHAVE
		Replace G2_ROTALT		with TMPSG2->G2_ROTALT
		Replace G2_OPERGRP		with TMPSG2->G2_OPERGRP
		Replace G2_GRSETUP		with TMPSG2->G2_GRSETUP
		Replace G2_GRUPREC		with TMPSG2->G2_GRUPREC
		Replace G2_NIVMONT		with TMPSG2->G2_NIVMONT
		Replace G2_CHAVMON		with TMPSG2->G2_CHAVMON
		Replace G2_TMAXPRO		with TMPSG2->G2_TMAXPRO
		Replace G2_TPINTER		with TMPSG2->G2_TPINTER
		Replace G2_MAXINCR		with TMPSG2->G2_MAXINCR
		Replace G2_FOLMIN		with TMPSG2->G2_FOLMIN
		Replace G2_HOROTIM		with TMPSG2->G2_HOROTIM
		Replace G2_TEMPEND		with TMPSG2->G2_TEMPEND
		Replace G2_REFGRD		with MV_PAR01
	MsUnLock()
	DbSelectArea("TMPSG2")
	DbSkip()
End

MsgBox("Roteiro de Opera��es copiado","C�pia de Roteiro de Opera��es","Info")

TMPSG2->(DbCloseArea())

Return()

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Cod Refer�ncia","","","Mv_ch1",TAMSX3("G2_REFGRD")[3],TAMSX3("G2_REFGRD")[1],TAMSX3("G2_REFGRD")[2],0,"G","","","","N","Mv_par01","","","","","","","","","","","","","","","","","","","","","","","",{"",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)