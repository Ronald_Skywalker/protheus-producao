//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio Informa��e Gerenciais Funcion�rio.
Rotina para Custos Funcionario;

@author Ronaldo Pereira
@since 29 de Janeiro de 2020
@version V.01
/*/
//_____________________________________________________________________________      


user function HPREL058()

	Local aRet     := {}
	Local aPerg    := {}
	Local cCodUsr  := AllTrim(SuperGetMV("HP_USERREL",.F.,"000138"))

	aAdd(aPerg,{1,"Data Admiss�o In�cio"		,dDataBase,PesqPict("SRA", "RA_ADMISSA"),'.T.'	,""		,'.T.'	,50	,.F.})
	aAdd(aPerg,{1,"Data Admiss�o Fim"		    ,dDataBase,PesqPict("SRA", "RA_ADMISSA"),'.T.'	,""		,'.T.'	,50	,.F.})
	
	If !(__cuserid) $ cCodUsr //OBS.: Este relat�rio n�o pode ser liberado para qualquer usu�rio, pois contem informa��es restristas!
			MessageBox("Relat�rio Exclusivo para o Setor de Informa��es Gerenciais!!","Aten��o!",48)
			return()
	EndIf
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		MessageBox("Cancelado Pelo Usu�rio!!!","",16)
	Endif
    
Return    	
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Informa��es_Gerenciais'+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Base"
    Local cTable1    	:= "FUNCIONARIOS" 

        
    //Dados tabela 1             
    cQry := " SELECT RA_FILIAL AS FILIAL, "
    cQry += "        SubString(S.RA_ADMISSA, 7, 2)+'/'+SubString(S.RA_ADMISSA, 5, 2)+'/'+SubString(S.RA_ADMISSA, 1, 4) AS DT_ADMISSAO, "    
    cQry += "        RA_MAT AS MATRICULA, "
    cQry += "        RA_NOMECMP AS NOME, "
    cQry += "        CASE(RA_SITFOLH) "
    cQry += "            WHEN '' THEN 'SITUACAO NORMAL' "
    cQry += "            WHEN 'A' THEN 'AFASTADO TEMP.' "
    cQry += "            WHEN 'D' THEN 'DEMITIDO' "
    cQry += "            WHEN 'F' THEN 'FERIAS' "
    cQry += "            WHEN 'T' THEN 'TRANSFERIDO' "
    cQry += "        END AS SITUACAO, "
    cQry += "        RA_CC AS COD_CC, "
    cQry += "        S1.CTT_DESC01 AS DESC_CC, "
    cQry += "        S2.RJ_DESC AS FUNCAO, "
    cQry += "        CASE(S.RA_VIEMRAI) "
    cQry += "            WHEN '55' THEN 'SIM' "
    cQry += "            ELSE 'N�O' "
    cQry += "        END AS APRENDIZ, "
    cQry += "        S.RA_SALARIO AS SALARIO, "
    cQry += "        SubString(RA_NASC, 7, 2)+'/'+SubString(RA_NASC, 5, 2)+'/'+SubString(RA_NASC, 1, 4) AS DATA_NASC, "
    cQry += "        RA_SEXO AS SEXO "
    cQry += " FROM "+RetSqlName('SRA')+" AS S WITH (NOLOCK) "
    cQry += " LEFT JOIN "+RetSqlName('CTT')+" AS S1 WITH (NOLOCK) ON (S1.CTT_CUSTO=S.RA_CC "
    cQry += "                         AND S1.D_E_L_E_T_='' "
    cQry += "                         AND S1.CTT_FILIAL='01') "
    cQry += " LEFT JOIN "+RetSqlName('SRJ')+" AS S2 WITH (NOLOCK) ON (S2.RJ_FUNCAO=S.RA_CODFUNC "
    cQry += "                         AND S2.D_E_L_E_T_='' "
    cQry += "                         AND S2.RJ_FILIAL='01') "
    cQry += " WHERE S.RA_FILIAL IN ('0101', "
    cQry += "                       '0103') "
    cQry += "   AND S.RA_CATEG <> '13' "
    cQry += "   AND S.D_E_L_E_T_='' "
    cQry += "   AND S.RA_SITFOLH<>'D' "
    cQry += "   AND RA_MAT NOT LIKE '99%' "
    cQry += "   AND RA_SALARIO>0 "
       
    If !Empty(aRet[1]) .AND. !Empty(aRet[2])
    	cQry += "  AND S.RA_ADMISSA BETWEEN '"+ DTOS(aRet[1])+"' AND '"+ DTOS(aRet[2])+"' "
    EndIf
  
    cQry += " ORDER BY RA_FILIAL, RA_ADMISSA "

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_ADMISSAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MATRICULA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NOME",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SITUACAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_CC",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESC_CC",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FUNCAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"APRENDIZ",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SALARIO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DATA_NASC",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SEXO",1)
        
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;                                             
                                               FILIAL,;
                                               DT_ADMISSAO,;
                                               MATRICULA,;
                                               NOME,;
                                               SITUACAO,;
                                               COD_CC,;
                                               DESC_CC,;
                                               FUNCAO,;
                                               APRENDIZ,;
                                               SALARIO,;
                                               DATA_NASC,;                                            
                                               SEXO;                                               
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
	
return