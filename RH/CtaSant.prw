#Include 'Protheus.ch'

//Protheus 12.1.25 - Hope Lingerie
//Fun��o: PosCorPa()
//Descri��o: Verifica se existe uma corre��o de adiantamento para uma compensa��o de t�tulos moeda 02
//e retorna com o valor da corre��o, realizado este desenvolvimento, pois o padr�o nas compensa��es 
//posiciona apenas na NF e n�o na PA.
//Autor: Jos� Carlos Ferrari
//Data: 26/06/2020
User Function CtaSant()		

Local cEmp		:= ""
Local cConta 	:= ""

cEmp := Substr(cFilAnt,1,2)

If cEmp == "01"
	cConta := "000013002669"

ElseIf cEmp == "02"
	cConta := "000013005001"

ElseIf cEmp == "03"
	cConta := "000013005009"

ElseIf cEmp == "04"
	cConta := "000013005000"
	
EndIf

Return cConta

//Protheus 12.1.25 - Hope Lingerie
//Fun��o: PosCorPa()
//Descri��o: Verifica se existe uma corre��o de adiantamento para uma compensa��o de t�tulos moeda 02
//e retorna com o valor da corre��o, realizado este desenvolvimento, pois o padr�o nas compensa��es 
//posiciona apenas na NF e n�o na PA.
//Autor: Jos� Carlos Ferrari
//Data: 26/06/2020
User Function AgeSant()		

Local cEmp		:= ""
Local cAgen		:= ""

cEmp := Substr(cFilAnt,1,2)

If cEmp == "01"
	cAgen  := "03984"

ElseIf cEmp == "02"
	cAgen  := "00438"

ElseIf cEmp == "03"
	cAgen  := "00438"

ElseIf cEmp == "04"
	cAgen  := "00438"
	
EndIf

Return cAgen

//Protheus 12.1.25 - Hope Lingerie
//Fun��o: PosCorPa()
//Descri��o: Verifica se existe uma corre��o de adiantamento para uma compensa��o de t�tulos moeda 02
//e retorna com o valor da corre��o, realizado este desenvolvimento, pois o padr�o nas compensa��es 
//posiciona apenas na NF e n�o na PA.
//Autor: Jos� Carlos Ferrari
//Data: 26/06/2020
User Function DgtSant()		

Local cEmp		:= ""
Local cDigit	:= "'"

cEmp := Substr(cFilAnt,1,2)

If cEmp == "01"
	cDigit := "7"

ElseIf cEmp == "02"
	cDigit := "7"

ElseIf cEmp == "03"
	cDigit := "3"

ElseIf cEmp == "04"
	cDigit := "0"
	
EndIf

Return cDigit

//Protheus 12.1.25 - Hope Lingerie
//Fun��o: PosCorPa()
//Descri��o: Verifica se existe uma corre��o de adiantamento para uma compensa��o de t�tulos moeda 02
//e retorna com o valor da corre��o, realizado este desenvolvimento, pois o padr�o nas compensa��es 
//posiciona apenas na NF e n�o na PA.
//Autor: Jos� Carlos Ferrari
//Data: 26/06/2020
User Function CnvSant()		

Local cEmp		:= ""
Local cConv		:= ""

cEmp := Substr(cFilAnt,1,2)

If cEmp == "01"
	cConv := "00333984008300421425"

ElseIf cEmp == "02"
	cConv := "00330438008303580725"

ElseIf cEmp == "03"
	cConv := "00330438008303603032"

ElseIf cEmp == "04"
	cConv := "00330438008303580733"
	
EndIf

Return cConv

