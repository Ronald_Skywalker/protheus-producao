#INCLUDE "TOTVS.CH"
#INCLUDE "TopConn.ch"

/*/

	{Protheus.doc} LOGINT
					
	Historico das transacoes de Integracao

	@author		Milton J.dos Santos	
	@since		12/06/20
	@version	1.0

/*/

User Function LOGPINT( _cTabela, _cChave )
Local oDlg
Local aArea		:= GetArea()
Local cQuery	:= ""
Local aCab		:= {}
Local cTitulo	:= "Historico da Integracao"
Local cChave	:= Space( TamSx3("ZAW_CHAVE")[1] )

Private aRegs	:= {}

Default _cTabela:= "SB1"
Default _cChave	:= SB1->B1_COD

cChave	:= PADR( _cChave, TamSx3("ZAW_CHAVE")[1])

DbSelectArea("ZAW")

aAdd( aCab, "Sequencia"		)
aAdd( aCab, "Dt.Transacao"	)
aAdd( aCab, "Hora Transacao")
aAdd( aCab, "Sit.Transacao"	)
aAdd( aCab, "Acao"			)
aAdd( aCab, "Usuario"		)

cQuery := " SELECT	ZAW_SEQ,	 				"					+ CRLF
cQuery += "			ZAW_DATA, 					"					+ CRLF
cQuery += "			ZAW_HORA, 					"					+ CRLF
cQuery += "			ZAW_STATUS,					"					+ CRLF
cQuery += "			ZAW_ACAO,	 				" 					+ CRLF
cQuery += "			ZAW_USER,					" 					+ CRLF
cQuery += "			R_E_C_N_O_ RECNO			"					+ CRLF
cQuery += " FROM " + RetSqlName("ZAW") + " ZAW  "					+ CRLF
cQuery += " WHERE	ZAW.D_E_L_E_T_ = ' '" 							+ CRLF
cQuery += " 	AND	ZAW.ZAW_FILIAL  = '" + FwxFilial("ZAW") + "'"	+ CRLF
cQuery += "		AND ZAW.ZAW_TABELA  = '" + _cTabela + "'" 			+ CRLF
cQuery += "		AND ZAW.ZAW_CHAVE   = '" + cChave  + "'" 			+ CRLF
cQuery += " ORDER BY ZAW_FILIAL, ZAW_SEQ " 							+ CRLF
Iif(Select("TZAW") > 0, TZAW->(DbCloseArea()), )
TcQuery cQuery Alias "TZAW" New
TcSetField("TZAW", "ZAW_DATA", "D")
TZAW->(dBGoTop())
If TZAW->(Eof())
	MsgAlert("Nao existem transacoes registradas deste item desde sua criacao")
Else
	While !TZAW->(Eof())
		Aadd(aRegs, {	;
						TZAW->ZAW_SEQ,	 				;
						TZAW->ZAW_DATA, 				;
						TZAW->ZAW_HORA, 				;
						RetStatus1(TZAW->ZAW_STATUS),	;
						TZAW->ZAW_ACAO,	 				;
						RetUsuario(TZAW->ZAW_USER),		;
						TZAW->RECNO		 				;
					})
		TZAW->(dbSkip())
	EndDo
	DEFINE DIALOG oDlg TITLE cTitulo FROM 170, 170 TO 580, 990 PIXEL
	oLbx := TCBrowse():New(05, 05, 255, 180,, aCab, {15, 30, 70, 70, 50, 25}, oDlg,,,,, {||},,,,,,, .F.,, .T.,, .F.,,,)
	oLbx:Align := CONTROL_ALIGN_TOP //CONTROL_ALIGN_ALLCLIENT
	oLbx:SetArray( aRegs )
	oLbx:bLine := { || aRegs[ oLbx:nAT ] }
	oLbx:SetFocus()                   // Refocus on The Browse
	oLbx:Refresh()                    // Estabiliza o Browse/Listbox
	DEFINE SBUTTON FROM 190, 250 TYPE 15 ACTION DETALHE(oLbx:nAt)  ENABLE OF oDlg
	DEFINE SBUTTON FROM 190, 350 TYPE  1 ACTION oDlg:End() ENABLE OF oDlg
	ACTIVATE MSDIALOG oDlg CENTER
EndIf
TZAW->(DbCloseArea())
RestArea(aArea)

Return

/*

	Historico de transacoes de Integracao

*/

Static Function TrataTipo(cCampo,cConteudo)

Local cRet := ""

If GetSX3Cache(cCampo, "X3_TIPO") == "N"
	cRet := AllTrim(Transform(Val(cConteudo), GetSX3Cache(TZAW->ZAW_CAMPO, "X3_PICTURE")))
Else
	cRet := cConteudo
EndIf

Return cRet

/*
	Grava Dados na ZAW para auditoria
*/

User Function GravaLog( _cFuncao, _cMetodo, _cLink, _cJson, _cResult, _cAcao, _cStatus, _cHrIni, _cHrFim, _cTabela, _cChave, _cIdPlugg, _aAlerta )
Local cSeq		 := StrZero(1,6)
Local cResult	 := ""
//Local cIdInt	 := ""
Local cAlerta	 := ""
Local nAlerta	 := 0
Local cChave	 := Space( TamSx3("ZAW_CHAVE")[1] )

Default _cFuncao := "" 
Default _cMetodo := "" 
Default _cLink	 := "" 
Default _cJson	 := "" 
Default _cResult := ""
Default _cAcao	 := ""
Default _cStatus := ""
Default _cHrIni	 := Time()
Default _cHrFim	 := Time()
Default _cTabela := ""
Default _cChave	 := Space( TamSx3("ZAW_CHAVE")[1] )
Default _aAlerta := {}

cChave	 := PADR( _cChave, TamSx3("ZAW_CHAVE")[1])

For nAlerta := 1 to Len( _aAlerta )
	cAlerta += _aAlerta[ nAlerta ] + CRLF
Next

cResult := DecodeUTF8(_cResult, "cp1252")	// Corrige erros com acentuacao

cQuery := "SELECT COUNT(*) TOTAL "								+ CRLF
cQuery += "FROM " + RetSqlName("ZAW")	 + " ZAW "				+ CRLF
cQuery += "WHERE ZAW.D_E_L_E_T_ = ' '"							+ CRLF
cQuery += "	AND	 ZAW.ZAW_FILIAL = '" + xFilial("ZAW") + "'"		+ CRLF
cQuery += "	AND	 ZAW.ZAW_TABELA = '" + _cTabela + "'" 			+ CRLF
cQuery += "	AND	 ZAW.ZAW_CHAVE  = '" + cChave   + "'" 			+ CRLF

Iif(Select("TZAW") > 0, TZAW->(DbCloseArea()), )
TcQuery cQuery Alias "TZAW" New
If !TZAW->(Eof())
	cSeq := StrZero(TZAW->TOTAL+1,6)
Endif

DbSelectArea("ZAW")
RecLock( "ZAW", .T. )
ZAW->ZAW_FILIAL := xFilial("ZAW")	// Filial logada
ZAW->ZAW_SEQ	:= cSEQ				// Numero sequencial
ZAW->ZAW_DATA  	:= DATE()			// Data de gravacao no LOG
ZAW->ZAW_HORA  	:= TIME()			// Hora de gravacao no LOG
ZAW->ZAW_TABELA	:= _cTabela			// ID da Tabela
ZAW->ZAW_CHAVE	:= _cChave			// ID da Chave de Pesquisa
ZAW->ZAW_TOKEN	:= cToken			// Token da Transa��o
ZAW->ZAW_METODO	:= _cMetodo			// Metodo: 1=Get,2=Post,3=Put,4=Delete,5=Head
ZAW->ZAW_STWS	:= _cStatus			// Status do PLUGG.TO da transacao
ZAW->ZAW_IDWS	:= _cIdPlugg		// ID do PLUGG.TO
ZAW->ZAW_FUNCAO := _cFuncao			// Funcao executada
ZAW->ZAW_ACAO	:= _cAcao			// Acao tomada
ZAW->ZAW_STATUS := _cStatus			// Comunicacao concluida com sucesso >> 1=Sim, 2=Nao 
ZAW->ZAW_USER	:= __cUserID		// Usuario logado
ZAW->ZAW_LINK	:= _cLink			// EndPoint
ZAW->ZAW_JSON	:= _cJson			// JSON enviado
ZAW->ZAW_RESULT := cResult			// Resposta do WebService
ZAW->ZAW_ALERTA := cAlerta			// Alertas internos
ZAW->ZAW_HRENV	:= _cHrIni			// Hora do envio para o WebService
ZAW->ZAW_HRRESU := _cHrFim			// Hora de resposta do WebService
MsUnlock()	

Return

Static Function RetUsuario( _cUser )
Default _cUser := "000000"
Return UsrRetName (_cUser )

Static Function RetStatus1( _cStatus )
Local cStatus := ""
Do case
	Case _cStatus == '1'
		cStatus := "Concluido"
	Case _cStatus == '2'
		cStatus := "Erro na TAG"
	Case _cStatus == '3'
		cStatus := "Erro no JSON"
	Case _cStatus == '4'
		cStatus := "Erro na Comunicacao"
EndCase
Return cStatus

Static Function RetMetodo( _cMetodo )
Local cMetodo := ""
Do case
	Case _cMetodo == '1'
		cMetodo := "GET" 
	Case _cMetodo == '2'
		cMetodo := "POST" 
	Case _cMetodo == '3'
		cMetodo := "PUT" 
	Case _cMetodo == '4'
		cMetodo := "DELETE" 
	Case _cMetodo == '5'
		cMetodo := "HEAD" 
EndCase
Return cMetodo

Static Function DETALHE( _nLinha )
Local oDlg3
Local cTitulo   := "Detalhes da Transacao"
Local cLink		:= ""	//"ENDPOINT"			// MemoRead( ZAW->ZAW_JSON		)
Local cTexto1	:= ""	//"JSON ENVIADO"		// MemoRead( ZAW->ZAW_JSON		)
Local cTexto2	:= "" 	//"JSON DE RESPOSTA"	// MemoRead( ZAW->ZAW_RESULT	)
Local cTexto3	:= "" 	//"ALERTAS INTERNOS"	// MemoRead( ZAW->ZAW_ALERTA	)
Local cStatus	:= ""

DbSelectarEA("ZAW")
DbGoTo( aRegs[_nLinha, 7] )

cLink	:= Alltrim( ZAW->ZAW_LINK	)	//"ENDPOINT"			// MemoRead( ZAW->ZAW_JSON	)
cTexto1	:= Alltrim( ZAW->ZAW_JSON	)	//"JSON ENVIADO"		// MemoRead( ZAW->ZAW_JSON	)
cTexto2	:= Alltrim( ZAW->ZAW_RESULT	)	//"JSON DE RESPOSTA"	// MemoRead( ZAW->ZAW_RESULT	)
cTexto3	:= Alltrim( ZAW->ZAW_ALERTA	)	//"JSON DE RESPOSTA"	// MemoRead( ZAW->ZAW_RESULT	)

cStatus	:= ZAW->ZAW_STATUS
cMetodo	:= ZAW->ZAW_METODO
//cTexto2 := DecodeUTF8(cTexto2, "cp1252")

DEFINE MSDIALOG oDlg3 TITLE cTitulo From 0,0 To 038,107 OF oMainWnd // 35

@ 006, 006 SAY "Sequencia:"		SIZE 035,007 PIXEL OF oDlg3
@ 004, 036 GET ZAW->ZAW_SEQ		SIZE 035,007 PIXEL OF oDlg3 WHEN .F.

@ 006, 078 SAY "Data LOG:"		SIZE 030,007 PIXEL OF oDlg3
@ 004, 107 GET ZAW->ZAW_DATA	SIZE 040,007 PIXEL OF oDlg3 WHEN .F.

@ 006, 153 SAY "Hora LOG:"		SIZE 030,007 PIXEL OF oDlg3 //inicio do get + tamanho + 6 = comeco do SAY
@ 004, 182 GET ZAW->ZAW_HORA	SIZE 020,007 PIXEL OF oDlg3 WHEN .F.

@ 006, 217 SAY "Acao:"			SIZE 020,007 PIXEL OF oDlg3
@ 004, 234 GET 	ZAW->ZAW_ACAO	SIZE 182,007 PIXEL OF oDlg3 WHEN .F. // REPLICATE ("X" , 40)

cStatus := cStatus + "=" + RetStatus1(cStatus) 
cMetodo := cMetodo + "=" + RetMetodo(cMetodo) 

@ 026, 006 SAY "Sit.Transacao:"		SIZE 035,007 PIXEL OF oDlg3
@ 024, 044 GET 	cStatus 			SIZE 080,007 PIXEL OF oDlg3 WHEN .F. //ZAW->ZAW_STATUS

@ 026, 161 SAY "Metodo:"			SIZE 035,007 PIXEL OF oDlg3
@ 024, 187 GET 	cMetodo 			SIZE 045,007 PIXEL OF oDlg3 WHEN .F. //ZAW->ZAW_STATUS

@ 026, 259 SAY "Token:"				SIZE 065,007 PIXEL OF oDlg3
@ 024, 286 GET 	ZAW->ZAW_TOKEN 		SIZE 125,007 PIXEL OF oDlg3 WHEN .F. 

cNome := UsrRetName ( ZAW->ZAW_USER )
@ 046, 006 SAY "Usuario:"			SIZE 020,007 PIXEL OF oDlg3
@ 044, 030 GET 	ZAW->ZAW_USER		SIZE 020,007 PIXEL OF oDlg3 WHEN .F. 
@ 044, 057 GET 	cNome				SIZE 040,007 PIXEL OF oDlg3 WHEN .F.	//POSICIONAR

cTempo := ELAPTIME( ZAW->ZAW_HRENV , ZAW->ZAW_HRRESU )
@ 046, 103 SAY "Hr.Envio:"			SIZE 030,007 PIXEL OF oDlg3 		// 046, 006 
@ 044, 127 GET 	ZAW->ZAW_HRENV		SIZE 030,007 PIXEL OF oDlg3 WHEN .F.// 044, 030 
@ 046, 168 SAY "Hr.Retorno:"		SIZE 030,007 PIXEL OF oDlg3 		// 046, 006 
@ 044, 198 GET 	ZAW->ZAW_HRRESU		SIZE 030,007 PIXEL OF oDlg3 WHEN .F.// 044, 030 
@ 046, 239 SAY "Demora:"			SIZE 030,007 PIXEL OF oDlg3 		// 046, 006 
@ 044, 262 GET cTempo				SIZE 030,007 PIXEL OF oDlg3 WHEN .F.// 044, 030 
@ 046, 309 SAY "Chamada:"			SIZE 065,007 PIXEL OF oDlg3
@ 044, 336 GET 	ZAW->ZAW_FUNCAO 	SIZE 045,007 PIXEL OF oDlg3 WHEN .F. 

@ 066, 006 SAY "EndPoint:"			SIZE 130,007 PIXEL OF oDlg3
@ 064, 034 GET cLink				SIZE 382,007 PIXEL OF oDlg3 WHEN !Empty(cLink)

@ 080, 006 SAY "JSON Enviado"		SIZE 130,007 PIXEL OF oDlg3 //+11

oTMultiget1 := TMultiget():New(088,06,{|u|If(Pcount()>0,cTexto1:=u,cTexto1)},oDlg3,165,105,,,,,,.T.,,,,,,.T.) //+8

@ 080, 250 SAY "JSON Resposta"		SIZE 130,007 PIXEL OF oDlg3 //+11

oTMultiget2 := TMultiget():New(088,250,{|u|If(Pcount()>0,cTexto2:=u,cTexto2)},oDlg3,165,105,,,,,,.T.,,,,,,.T.) //+8

//---------------------------------------------------------------------------------------------------------------------------------

@ 198, 006 SAY "Alertas"			SIZE 130,007 PIXEL OF oDlg3 //+11

oTMultiget3 := TMultiget():New(210,006,{|u|If(Pcount()>0,cTexto3:=u,cTexto3)},oDlg3,410,040,,,,,,.T.,,,,,,.T.) //+8

//---------------------------------------------------------------------------------------------------------------------------------

oButtonOK   := tButton():New(260,385,'Finalizar',oDlg3,{|| oDlg3:End()},030,010,,,,.T.) //+9

ACTIVATE MSDIALOG oDlg3 CENTER

RETURN

// Trata mensagem de erro recebida do WEBSERVICE WS
// Para torna-la melhores para o usuario entender

User Function TrataErro( _cMsgError )
Local oError:= Nil
Local cErro	:= 'Erros'
Local cMsg	:= ""
Local cJson	:= ""
Local _nx

cJson := DecodeUTF8(_cMsgError, "cp1252")
oError:=JsonObject():new()
oError:fromJson( cJson )

Do Case
	Case ValType(oError['erro']) <> "U"
		cErro := 'erro'
	Case ValType(oError['erros']) <> "U"
		cErro := 'erros'
	Case ValType(oError['error']) <> "U"
		cErro := 'error'
	Case ValType(oError['errors']) <> "U"
		cErro := 'errors'
	Case  ValType(oError['Errors']) <> "U"
		cErro := 'Errors'
EndCase

If	ValType(oError[cErro]) == "C"
	cMsg  += oError[cErro]
ElseIf	ValType(oError[cErro]) == "L"
	If	ValType(oError['message']  ) == "C"
		cMsg  += oError['message'] + CRLF
	Endif
ElseIf ValType(oError[cErro]) == "A"
	For _nx := 1 to Len (oError[cErro])
		If	ValType(oError[cErro][_nx]) == "C"
			cMsg  += oError[cErro][_nx] + CRLF
		Else
			If	ValType(oError[cErro][_nx]['message']  ) == "C"
				cMsg  += oError[cErro][_nx]['message'] + CRLF
			Endif
		Endif
	Next
Endif
If Empty( cMsg )
	cMsg  := cJson
Else
	cMsg := "Erro: " + CRLF + "- " + cMsg
Endif

Return Iif( Empty( cMsg) , cJson, cMsg )
