#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#include "TOTVS.CH"

// Retornos poss�veis do MessageBox
#Define IDOK			    1
#Define IDCANCEL		    2
#Define IDYES			    6
#Define IDNO			    7
#Define CRLF  CHR(13)+CHR(10)


/*
�����������������������������������������������������������������������������
?????????????????????????????????????????????????????????????????????????????
??������������������������������������������������������������������������???
???Programa  ?HUPDTEMPO  ?Autor  ? Geyson Albano     ? Data ?  12/05/21   ???
??������������������������������������������������������������������������???
???Desc.     ?Rotina para atualizar tempos nas telas customizada e padr�o ???
??? com base no arquivo .csv (ref+cor+tempo)                              ???
??������������������������������������������������������������������������???
?????????????????????????????????????????????????????????????????????????????
�����������������������������������������������������������������������������
*/

User Function HUPDTEMPO()

	Local aPergs    := {}
	Private cArq       := ""
	Private cArqMacro  := "XLS2DBF.XLA"
	Private cTemp      := GetTempPath() //pega caminho do temp do client
	Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema
	Private aResps     := {}
	Private aArquivos  := {}
	Private nRet       := 0
	Private aTitle     := {}
	Private aLog       := {}
	Private lAtutmp    := .F.

	Aadd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.T.,""})

	If ParamBox(aPergs,"Parametros", @aResps)

		Aadd(aArquivos, AllTrim(aResps[1]))

		cArq := AllTrim(aResps[1])

		Processa({|| ProcMov()})

	EndIF
Return

Static Function ProcMov()

	Local nFile		:= 0
	Local cFile		:= AllTrim(aResps[1])
	Local cLinha  	:= ""
	Local nLintit	:= 1
	Local nLin 		:= 0
	Local aDados  	:= {}
	Local nHandle 	:= 0


	nFile := fOpen(cFile,0,0) //fOpen(cFile, FO_READ + FO_DENYWRITE)

	If nFile == -1
		If !Empty(cFile)
			MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")
		EndIf
		Return
	EndIf

	nHandle := Ft_Fuse(cFile)
	Ft_FGoTop()
	nLinTot := FT_FLastRec()-1
	ProcRegua(nLinTot)

	While nLinTit > 0 .AND. !Ft_FEof()
		Ft_FSkip()
		nLinTit--
	EndDo

	Do While !Ft_FEof()
		IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))
		nLin++
		cLinha := Ft_FReadLn()

		If Empty(AllTrim(StrTran(cLinha,',','')))
			Ft_FSkip()
			Loop
		EndIf

		cLinha := StrTran(cLinha,'"',"'")
		cLinha := '{"'+cLinha+','+alltrim(str(nLin+1))+'"}'
		cLinha := StrTran(cLinha,';',',')
		cLinha := StrTran(cLinha,',','","')

		Aadd(aDados, &cLinha)
		FT_FSkip()
	EndDo

	FT_FUse()

	//ProcRegua(Len(aDados))
	ProcRegua(0)
	nRet:= MessageBox("Deseja realmente importar o arquivo:" +Upper(cFile)+ " ?","Confirma��o",4)

	If nRet == 6
		Exploarq(aDados)
	Else
		MessageBox("Cancelado pelo usu�rio !","Cancelado",16)
	EndIf

Return

Static function Exploarq(pMatriz)
	Local nXT           := 0
	Local cRef          := ""
	Local cCor          := ""
	Local cTempo        := ""
	Private pMatrizped  := pMatriz
	Private lMsErroAuto := .F.

	For nXT:=1 to len(pMatrizped)

		cRef := Alltrim(pMatrizped[nXT][1])
		cCor := Alltrim(pMatrizped[nXT][2])
		cTempo := Alltrim(pMatrizped[nXT][3])
		xcoluna := POSICIONE("SB4",1,XFilial("SB4")+cRef,"B4_COLUNA")

		_qry := "UPDATE "+RetSqlName("SG1")+" SET G1_QUANT = '"+cTempo+"' "
		_qry += "FROM "+RetSqlName("SG1")+" SG1 "
		_qry += "	INNER JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = G1_COMP "
		If !Empty(cCor)
			_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(G1_COD,11) = '"+cRef+cCor+"' AND B1_TIPO = 'BN' "
		Else
			_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(G1_COD,8) = '"+cRef+"' AND B1_TIPO = 'BN' "
		EndIf

		nStatus := TcSqlExec(_qry)

		If (nStatus < 0)
			MsgBox(TCSQLError(), "Erro no comando", "Stop")
		Else
			lAtutmp := .T.
		EndIf

		If !Empty(cCor) .AND. lAtutmp

			DbSelectArea("SG1")
			DbSetOrder(1)
			DbGoTop()
			DbSeek(xfilial("SG1")+ALLTRIM(cRef)+cCor)

			While !EOF() .AND. SubStr(SG1->G1_COD,1,11) == ALLTRIM(cRef)+cCor
				DbSelectArea("SB1")
				DbSetOrder(1)
				Dbseek(xfilial("SB1")+SG1->G1_COMP)

				If SB1->B1_TIPO = "BN"

					DbSelectArea("SB4")
					DbSetOrder(1)
					Dbseek(xfilial("SB4")+SubStr(SG1->G1_COD,1,8))

					_wtam := rettam3(xcoluna,SubStr(SG1->G1_COD,12,4))
					_campo:= "ZF_TAM"+_wtam
					_campo2:= "ZB1_TAM"+_wtam // TABELA DE BKP ESTRUTURA

					DbSelectarea("SG5")
					DbSetOrder(1)
					DbSeek(xfilial("SG5")+alltrim(SB4->B4_COD))

					If Found()
						_cod := SG5->G5_PRODUTO
						While !EOF() .AND. SG5->G5_PRODUTO == _cod
							DbSelectArea("SZF")
							DbSetOrder(2)
							DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG5->G5_REVISAO+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))

							If Found() .AND. &_campo <> SG1->G1_QUANT
								RecLock("SZF",.F.)
								Replace &_campo			with SG1->G1_QUANT
								MsUnLock()
							EndIf
							DbSelectArea("SG5")
							DbSkip()
						EndDo
					Else
						DbSelectArea("SZF")
						DbSetOrder(2)
						DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))

						If Found() .AND. &_campo <> SG1->G1_QUANT
							RecLock("SZF",.F.)
							Replace &_campo			with SG1->G1_QUANT
							MsUnLock()
						EndIf

					EndIf
					// ATUALIZACAO TABELA BKP INICIO
					If Found()
						_cod := SG5->G5_PRODUTO
						While !EOF() .AND. SG5->G5_PRODUTO == _cod
							DbSelectArea("ZB1")
							DbSetOrder(2)
							DbSeek(xfilial("ZB1")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
							If Found() .AND. &_campo2 <> SG1->G1_QUANT
								RecLock("ZB1",.F.)
								Replace &_campo2			with SG1->G1_QUANT
								MsUnLock()
							EndIf
							DbSelectArea("SG5")
							DbSkip()
						EndDo
					Else
						DbSelectArea("ZB1")
						DbSetOrder(2)
						DbSeek(xfilial("ZB1")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))

						If Found() .AND. &_campo2 <> SG1->G1_QUANT
							RecLock("ZB1",.F.)
							Replace &_campo2			with SG1->G1_QUANT
							MsUnLock()
						EndIf

					EndIf
					//ATUALIZACAO TABELA BKP FIM
				EndIf
				DbSelectArea("SG1")
				DbSkip()
			EndDo
		
        ElseIf Empty(cCor) .AND. lAtutmp

			DbSelectArea("SG1")
			DbSetOrder(1)
			DbGoTop()
			DbSeek(xfilial("SG1")+ALLTRIM(cRef))

			While !EOF() .AND. SubStr(SG1->G1_COD,1,8) == ALLTRIM(cRef)
				DbSelectArea("SB1")
				DbSetOrder(1)
				Dbseek(xfilial("SB1")+SG1->G1_COMP)

				If SB1->B1_TIPO = "BN"

					DbSelectArea("SB4")
					DbSetOrder(1)
					Dbseek(xfilial("SB4")+SubStr(SG1->G1_COD,1,8))

					_wtam := rettam3(xcoluna,SubStr(SG1->G1_COD,12,4))
					_campo:= "ZF_TAM"+_wtam
					_campo2:= "ZB1_TAM"+_wtam // TABELA DE BKP ESTRUTURA

					DbSelectarea("SG5")
					DbSetOrder(1)
					DbSeek(xfilial("SG5")+alltrim(SB4->B4_COD))

					If Found()
						_cod := SG5->G5_PRODUTO
						While !EOF() .AND. SG5->G5_PRODUTO == _cod
							DbSelectArea("SZF")
							DbSetOrder(2)
							DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG5->G5_REVISAO+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))

							If Found() .AND. &_campo <> SG1->G1_QUANT
								RecLock("SZF",.F.)
								Replace &_campo			with SG1->G1_QUANT
								MsUnLock()
							EndIf
							DbSelectArea("SG5")
							DbSkip()
						EndDo
					Else
						DbSelectArea("SZF")
						DbSetOrder(2)
						DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))

						If Found() .AND. &_campo <> SG1->G1_QUANT
							RecLock("SZF",.F.)
							Replace &_campo			with SG1->G1_QUANT
							MsUnLock()
						EndIf

					EndIf
					// ATUALIZACAO TABELA BKP INICIO
					If Found()
						_cod := SG5->G5_PRODUTO
						While !EOF() .AND. SG5->G5_PRODUTO == _cod
							DbSelectArea("ZB1")
							DbSetOrder(2)
							DbSeek(xfilial("ZB1")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
							If Found() .AND. &_campo2 <> SG1->G1_QUANT
								RecLock("ZB1",.F.)
								Replace &_campo2			with SG1->G1_QUANT
								MsUnLock()
							EndIf
							DbSelectArea("SG5")
							DbSkip()
						EndDo
					Else
						DbSelectArea("ZB1")
						DbSetOrder(2)
						DbSeek(xfilial("ZB1")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))

						If Found() .AND. &_campo2 <> SG1->G1_QUANT
							RecLock("ZB1",.F.)
							Replace &_campo2			with SG1->G1_QUANT
							MsUnLock()
						EndIf

					EndIf
					//ATUALIZACAO TABELA BKP FIM
				EndIf
				DbSelectArea("SG1")
				DbSkip()
			EndDo
		EndIf
	Next
	
    If lAtutmp
		MessageBox("Tempos atualizados com sucesso ! ","HUPDTEMPO",0)
    Else
        MessageBox("Tempos n�o atualizados favor, verificar o arquivo e tentar novamente ! ","HUPDTEMPO",16)
	EndIf

Return

Static Function rettam3(xcoluna,_tam)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
	_BVTAM := ""

	cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
	cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' "
	cQuery += "ORDER BY R_E_C_N_O_"

	cQuery := ChangeQuery(cQuery)
	If SELECT("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	DbSelectArea("TMPSBV")
	DbGoTop()
	_xtam := 0
	_achou := .F.
	While !EOF() .AND. !_achou
		_xtam++
		If alltrim(TMPSBV->BV_CHAVE) == alltrim(_tam)
			_achou := .T.
		EndIf
		DbSkip()
	EndDo

	DbCloseArea()
	_BVTAM := StrZero(_xtam,3)
Return(_BVTAM)
