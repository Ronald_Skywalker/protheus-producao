#include "rwmake.ch"
#include "topconn.ch"
/*/
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������Ŀ��
���Funcao    � HCADX43	         � Autor � Roberto Le�o   � Data �  30/03/2017��
����������������������������������������������������������������������������Ĵ��
���Descricao � Manutencao da tabela 43 N�o Conformidades					 ���
�����������������������������������������������������������������������������ٱ�
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
/*/

User Function HCADX43()
cCadastro := "N�o Conformidades"
aAutoCab    := {}
aAutoItens  := {}
PRIVATE aRotina := { { "" ,  "AxPesqui"  , 0 , 1},;  // "Pesquisar"
       				  { "",   "C160Visual", 0 , 2},;  // "Visualizar"
					  { "",   "C160Inclui", 0 , 3},;  // "Incluir"
					  { "",   "C160Altera", 0 , 4},;  // "Alterar"
					  { "",   "C160Deleta", 0 , 5} }  // "Excluir"
DbSelectArea("SX5")           
DbSetOrder(1)  


If !DbSeek(xFilial("SX5")+"43",.F.)
   MsgAlert(xFilial("SX5"))
   MsgAlert("Nao foi possivel localizar a tabela 43 no cadastro de tabelas (SX5) !")
Else   
   c160altera("SX5",,3)
Endif   
return       