#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'
#Include 'TopConn.ch'
#INCLUDE "TBICONN.CH" 
#include "TOTVS.CH"

/*

	Rotina para Baixa de Titulo Direto via Banco

*/


User Function CTOBOR()

Local aRet := {}
Local aParamBox := {}
Local aFilTam := TamSX3("E1_FILIAL")
Local vTotalBor := 0
Local vBanco    := ""
Local vAgencia   := ""
Local vConta     := ""
Local vNomeCli   := ""
Local vFilorig   := ""
Local cData  := dDatabase


Private cCadastro := "Baixa Bordero"

//	aAdd(aParamBox,{1,"Filial",Space(aFilTam[01]),"","","SM0","",0,.F.}) // Tipo caractere
	aAdd(aParamBox,{1,"Bordero",Space(6),"","","","",0,.F.}) // Tipo caractere   
	aAdd(aParamBox,{1,"Data Baixa"  ,DTOS(cData),"","","","",50,.F.}) // Tipo data
//	aAdd(aParamBox,{1,"Data Baixa " , dDataBase , ,'.T.',"" ,'.T.', TamSX3("E1_BAIXA")[1], .F.})
    aAdd(aParamBox,{1,"Lote"  ,Space(8),"","","","",50,.F.}) // Tipo data

	If ParamBox(aParamBox,"Par�metros...",@aRet)
	
			cSQL := "SELECT * FROM "+ RETSQLNAME("SE1")+" WHERE D_E_L_E_T_='' AND E1_NUMBOR='"+aRet[1]+"' AND E1_SALDO > 0" //AND E1_FILORIG='"+aRet[1]+"'*/
			IF SELECT("QSE1")>0
				QSE1->(dbCloseArea()) 
			Endif
			TCQUERY cSQL NEW ALIAS QSE1	
	 
			IF (QSE1->(Eof()))
			
				aLERT("Nenhum Registro Encontrado!!!")
				RETURN
			ENDIF
	 
	//BEGIN TRANSACTION
			
	 
			While !QSE1->(Eof())
				
				nRecno := QSE1->R_E_C_N_O_
				
				
			cSQL2 := "SELECT MAX(E5_SEQ) E5_SEQ FROM "+ RETSQLNAME("SE5")+" WHERE D_E_L_E_T_='' AND E5_NUMERO='"+QSE1->E1_NUM+"' AND E5_PREFIXO='"+QSE1->E1_PREFIXO+"' AND E5_FILIAL='"+QSE1->E1_FILIAL+"' AND E5_PARCELA='"+QSE1->E1_PARCELA+"'"
			IF SELECT("QSE5")>0
				QSE5->(dbCloseArea()) 
			Endif
			TCQUERY cSQL2 NEW ALIAS QSE5
			
			cSEQ := SOMA1(QSE5->E5_SEQ)					
	 
		
				//BAIXA SE1
				DbSelectArea("SE1")
				SE1->(dbGoTo(nRecno))
	 
					RecLock("SE1", .F.)
						SE1->E1_VALLIQ = QSE1->E1_SALDO
						SE1->E1_SALDO = 0
						SE1->E1_BAIXA = aRet[2]
						SE1->E1_STATUS = 'B'
					MsUnLock() 
				
				
				//BAIXA SE5
				DbSelectArea("SE5")
	 
					RecLock("SE5", .T.)
						SE5->E5_FILIAL 	= xFilial("SE5")
						SE5->E5_DATA	= aRet[2]
						SE5->E5_TIPO	= QSE1->E1_TIPO
						SE5->E5_MOEDA	= '01'
						SE5->E5_VALOR	= QSE1->E1_SALDO
						vTotalBor += QSE1->E1_SALDO
						SE5->E5_NATUREZ = QSE1->E1_NATUREZ
						SE5->E5_BANCO	= QSE1->E1_PORTADO
						vBanco    :=  QSE1->E1_PORTADO
						SE5->E5_AGENCIA = QSE1->E1_AGEDEP
						vAgencia := QSE1->E1_AGEDEP
						SE5->E5_CONTA   = QSE1->E1_CONTA
						vConta  := QSE1->E1_CONTA
						SE5->E5_DOCUMEN = aRet[1]
						SE5->E5_VENCTO	= stod(QSE1->E1_VENCTO)
						SE5->E5_RECPAG 	= 'R'
						SE5->E5_BENEF 	= QSE1->E1_NOMCLI
						SE5->E5_HISTOR 	= 'Valor recebido s/Titulo'  
						SE5->E5_LOTE 	= aRet[3]  
						SE5->E5_TIPODOC = 'BA'
						SE5->E5_LA      = 'N'
						SE5->E5_VLMOED2 = QSE1->E1_SALDO
						SE5->E5_PREFIXO = QSE1->E1_PREFIXO
						SE5->E5_NUMERO	= QSE1->E1_NUM
						SE5->E5_PARCELA = QSE1->E1_PARCELA
						SE5->E5_CLIFOR	= QSE1->E1_CLIENTE
						SE5->E5_LOJA	= QSE1->E1_LOJA
						SE5->E5_DTDIGIT = aRet[2]
						SE5->E5_MOTBX	= 'NOR'
						SE5->E5_SEQ		= cSEQ
						SE5->E5_DTDISPO = aRet[2]
						SE5->E5_ARQCNAB = 'FINA110'
						SE5->E5_FILORIG = QSE1->E1_FILORIG //aRet[1]
						vFilorig  := QSE1->E1_FILORIG
						SE5->E5_SITCOB	= QSE1->E1_SITUACA
						SE5->E5_CLIENTE	= QSE1->E1_CLIENTE
						SE5->E5_ORIGEM	= 'FINA110'
						SE5->E5_TPDESC	= 'I'
				
					MsUnLock()
		
				QSE1->(dbSkip())
	 
			End Do
				    DbSelectArea("SE5")
					RecLock("SE5", .T.)
						SE5->E5_FILIAL 	= xFilial("SE5")
						SE5->E5_DATA	= aRet[2]
						SE5->E5_MOEDA	= '01'
						SE5->E5_VALOR	= vTotalBor
						SE5->E5_NATUREZ = 'NATMOVR'
						SE5->E5_BANCO	= vBanco
						SE5->E5_AGENCIA = vAgencia
						SE5->E5_CONTA   = vConta
						SE5->E5_RECPAG 	= 'R'
						SE5->E5_BENEF 	= vNomeCli
						SE5->E5_HISTOR 	= 'Baixa Automatica /'+ aRet[3]  
						SE5->E5_LOTE 	= aRet[3]  
						SE5->E5_TIPODOC = 'VL'
						SE5->E5_DTDIGIT = aRet[2]
						SE5->E5_RATEIO  = 'N'
						SE5->E5_DTDISPO = aRet[2]
						SE5->E5_FILORIG = vFilorig
						SE5->E5_ORIGEM	= 'FINA110'
						SE5->E5_TPDESC	= 'I'			
				
					MsUnLock()


	// END TRANSACTION
	
		MSGINFO( 'Baixa Realizada Com Sucesso!! Favor verifique no extrato!!!', 'Baixa Realizada' )

	Else
	
		Alert("Cancelado Pelo Usu�rio!!!")
		
	Endif

Return