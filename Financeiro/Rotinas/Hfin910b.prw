#Include "Protheus.ch"
#Include "ApWizard.ch"
#include "fileio.ch"


//Header do Arquivo de Conciliacao do Concil
#Define HD_TPREG 1			//Tipo de Registro
#Define HD_DTARQ 2			//Data do Arquivo
#Define HD_HRARQ 3			//Hora do Arquivo
#Define HD_DTINI 4			//Data Inicial do Periodo
#Define HD_DTFIM 5			//Data Final do Periodo
#Define HD_VSARQ 6			//Versao do Arquivo
#Define HD_CODRD 7			//Codigo de Identificacao de Rede
#Define HD_SQARQ 8			//Numero sequencial do arquivo
#Define HD_SQREG 9			//Numero sequencial do registro
#Define HD_TOTRG 9			//Total de Campos do Heade

//Detalhes do Arquivo de Conciliacao do Concil - Cr�dito
#Define DT_TPREG 01			//V01 - Tipo de Registro
#Define DT_CODRP 02			//V02 - C�digo ERP
#Define DT_FILIA 03			//V03 - C�digo da Filial
#Define DT_ESTAB 04			//V04 - Estabelecimento
#Define DT_TPLAN 05         //V05 - Tipo de Lan�amento
#Define DT_DESCR 06			//V06 - Descri��o
#Define DT_NPARC 07			//V07 - Numero da Parcela
#Define DT_ADIQU 08			//V08 - Adiquirente
#Define DT_BANDE 09			//V09 - Bandeira
#Define DT_TPTRA 10			//V10 - Tipo Transa��o
#Define DT_TRAID 11 		//V11 - Tracking ID
#Define DT_NSUST 12			//V12 - NSU
#Define DT_AUTOR 13			//V13 - C�digo de Autoriza��o
#Define DT_DTPAG 14			//V14 - Data Pagamento
#Define DT_VLPAG 15			//V15 - Valor Pago
#Define DT_PTAXA 16			//V16 - Porcentagem da taxa de Servi�o
#Define DT_STATU 17			//V17 - Status
#Define DT_REFR1 18			//V18 - Referencia de Retorno 1
#Define DT_REFR2 19			//V19 - Referencia de Retorno 2
#Define DT_NRANT 20			//V20 - Numero da opera��o de antecipa��o (Para pagto antecipado)
#Define DT_VLTXS 21			//V16 - Valor da Taxa de antecipa��o (Para pagto antecipado)
#Define DT_CDBCO 22			//V19 - C�digo do Banco
#Define DT_CDAGE 23 		//V23 - C�digo da Agencia
#Define DT_CDCC  24			//V24 - Numero da Conta Corrente
#Define DT_TID   25			//V25 - Identificador de Transa��o ( TRANSACTION ID )
#Define DT_TERMI 26			//V26 - N�mero do Terminal que Efetuou a Transa��o
#Define DT_DTVEN 27			//V27 - Data da Venda
#Define DT_MASCR 28			//V28 - Mascara do cart�o  -  Numero mascarado do Cart�o
#Define DT_NRROP 29			//V29 - N�mero do Resumo de Opera��o
#Define DT_NRROA 30			//V30 - N�mero do Resumo de Opera��o de	Ajuste


//Detalhes do Arquivo de Conciliacao do Consil - Venda
#Define V_DT_TPREG 01			//V01 - Tipo de Registro
#Define V_DT_CODRP 02			//V02 - C�digo ERP
#Define V_DT_FILIA 03			//V03 - C�digo da Filial
#Define V_DT_ESTAB 04			//V04 - Estabelecimento
#Define V_DT_TPLAN 05         //V05 - Tipo de Lan�amento
#Define V_DT_DESCR 06			//V06 - Descri��o
#Define V_DT_NPARC 07			//V07 - Numero da Parcela
#Define V_DT_ADIQU 08			//V08 - Adiquirente
#Define V_DT_BANDE 09			//V09 - Bandeira
#Define V_DT_TPTRA 10			//V10 - Tipo Transa��o
#Define V_DT_TRAID 11 		//V11 - Tracking ID
#Define V_DT_NSUST 12			//V12 - NSU
#Define V_DT_AUTOR 13			//V13 - C�digo de Autoriza��o
#Define V_DT_DTPAG 14			//V14 - Data Pagamento
#Define V_DT_VLPAG 15			//V15 - Valor Pago
#Define V_DT_PTAXA 16			//V16 - Porcentagem da taxa de Servi�o
#Define V_DT_STATU 17			//V17 - Status
#Define V_DT_REFR1 18			//V18 - Referencia de Retorno 1
#Define V_DT_REFR2 19			//V19 - Referencia de Retorno 2
#Define V_DT_NRANT 20			//V20 - Numero da opera��o de antecipa��o (Para pagto antecipado)
#Define V_DT_VLTXS 21			//V16 - Valor da Taxa de antecipa��o (Para pagto antecipado)
#Define V_DT_CDBCO 22			//V19 - C�digo do Banco
#Define V_DT_CDAGE 23 		    //V23 - C�digo da Agencia
#Define V_DT_CDCC  24			//V24 - Numero da Conta Corrente
#Define V_DT_TID   25			//V25 - Identificador de Transa��o ( TRANSACTION ID )
#Define V_DT_TERMI 26			//V26 - N�mero do Terminal que Efetuou a Transa��o
#Define V_DT_DTVEN 27			//V27 - Data da Venda
#Define V_DT_MASCR 28			//V28 - Mascara do cart�o  -  Numero mascarado do Cart�o
#Define V_DT_NRROP 29			//V29 - N�mero do Resumo de Opera��o
#Define V_DT_NRROA 30			//V30 - N�mero do Resumo de Opera��o de	Ajuste


//Trailer do Arquivo de Conciliacao do Concil
#Define TR_TPREG 1			//Tipo de Registro
#Define TR_QTREG 2			//Quantidade de registro
#Define TR_DTGER 3			//Data gera��o do Arquivo


Static nTamEmis 	:= TamSX3("ZAK_EMISSA")[1]
Static nTamParc 	:= TamSX3("ZAK_PARCELA")[1]
//Static nTamTEF 		:= TamSX3("E1_NSUTEF")[1]
//Static nTamTPE1 	:= TamSX3("E1_TIPO")[1]
//Static nFIFParc		:= TamSX3("FIF_PARCEL")[1]
//Static nTamNSU		:= TamSX3("FIF_NSUTEF")[1]
//Static nTamDtVnd	:= TamSX3("FIF_DTTEF")[1]

//---------------------------------------------------------------------------------------------------------------

//Detalhes do Arquivo de Conciliacao do Concil  - Cr�dito
Static nPosSqReg 	:= 0 		//Seq. do Registro no Arquivo
Static nPosToReg 	:= 0		//Total de Campos do Trailer

//Detalhes do Arquivo de Conciliacao do Concil - Venda
Static nVPosSqReg 	:= 0		//Seq. do Registro no Arquivo
Static nVPosToReg 	:= 0		//Total de Campos do Trailer

Static cVerCon		:= ""
Static oParamFil    := Nil		//Objeto do tipo LJCHasheTable com as filiais cadastradas nos parametros
Static aCompSA1		:= {}
Static aCompSA6		:= {}

Static lFin910Fil	:= (ExistBlock("FIN910FIL")) //PE para sele��o de filiais.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �FINA910B  � Autor � Rafael Rosa da Silva  � Data �05/08/2009���
�������������������������������������������������������������������������Ĵ��
���Locacao   � CSA              �Contato � 								  ���
�������������������������������������������������������������������������Ĵ��
���Descricao �Rotina que importa os Arquivos do concil          		  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Aplicacao �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Analista Resp.�  Data  � Bops � Manutencao Efetuada                    ���
�������������������������������������������������������������������������Ĵ��
���              �  /  /  �      �                                        ���
���              �  /  /  �      �                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFIN910B()

Local oWizard	:= Nil		//Objeto matriz
Local oCamArq	:= Nil		//Objeto do caminho do arquivo
Local cCamArq	:= ""		//variavel do caminho do arquivo
Local lOk		:= .F.		//Variavel que verifica se o procedimento foi executado com um Finalizar

Private lEnd 	:= .T.
Private aRegSE1 := {}


DEFINE WIZARD oWizard TITLE "Concilia��o CONCIL " HEADER "Wizard utilizado para importacao de arquivos de concilia��o Concil";			//"STR0002 Concilia��o TEF"	### STR0003 "Wizard utilizado para importacao de arquivos de concilia��o TEF"
MESSAGE "";
TEXT "Esta rotina tem por objetivo importar os arquivos de concilia��o CONCIL" ;
PANEL NEXT {|| .T. } FINISH {|| .T. };

// Painel da selecao do arquivo
CREATE PANEL oWizard HEADER "Dados concilia��o"	MESSAGE "Selecione o arquivo de integra��o de concilia��o do SITEF" ;
PANEL BACK {|| .T. } NEXT {|| A910ExtArq(cCamArq) } FINISH {|| .T. } EXEC {|| .T. }

@ C(005),C(005) Say "Arquivo" 			  Size C(051),C(008) COLOR CLR_BLACK PIXEL OF oWizard:oMPanel[2]				//"Arquivo"
@ C(004),C(055) MsGet oCamArq Var cCamArq Size C(105),C(009) COLOR CLR_BLACK PIXEL OF oWizard:oMPanel[2]

@ C(004),C(162) Button "Procurar" Size C(037),C(009) Action A910BscArq(@cCamArq,@oCamArq) PIXEL OF oWizard:oMPanel[2]	//"&Procurar"

// Painel da importacao do arquivo e finalizacao do processo
CREATE PANEL oWizard HEADER "Finalizar" MESSAGE "Para confirmar a importa��o do arquivo de concilia��o do Concil clique em Finalizar ou clique em Cancelar para sair da rotina" ;
PANEL BACK {|| .T. } FINISH {|| lOk := .T. } EXEC {|| .T.}

ACTIVATE WIZARD oWizard CENTERED

If lOk
	//Ponto de Entrada para substituir a importacao do arquivo padrao de Conciliacao do SITEF
	If (ExistBlock("F910PROC"))
		Processa({|| U_F910PROC(cCamArq) },"Processando...")				//"Processando..."
	Else
		Processa({|lEnd| A910VldArq(cCamArq,@lEnd) },"Processando...", ,@lEnd)				//"Processando..."
	EndIf
EndIf

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �A910VldArq�Autor  �Rafael Rosa da Silva� Data �  08/11/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function A910ExtArq(cCamArq)

Local lRet := .T.

If Empty(cCamArq)
	MsgInfo("Para continuar � necessario a pesquisa do arquivo")							//"Para continuar � necessario a pesquisa do arquivo"
	lRet := .F.
ElseIf !File(cCamArq)
	MsgInfo("Arquivo " + cCamArq + " nao encontrado!")		//"Arquivo "	### " nao encontrado!"
	lRet := .F.
EndIf

Return lRet
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �A910BscArq�Autor  �Rafael Rosa da Silva� Data �  08/11/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function A910BscArq(cCamArq,oCamArq)

Local cType := OemToAnsi("Arquivos CSV") + "(*.csv) |*.csv|"													//"Arquivos CSV"

cCamArq := Upper(Alltrim(cGetFile(cType ,"Selecione o Arquivo",0,,.F.,GETF_LOCALHARD + GETF_NETWORKDRIVE)))			//"Selecione o Arquivo"
oCamArq:Refresh()

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �A910GrvArq�Autor  �Rafael Rosa da Silva� Data �  08/11/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function A910VldArq(cCamArq, lEnd)

Local lRet		:= .T.		//Variavel de controle do retorno
Local cLinha	:= ""		//variavel de leitura da linha
Local nSeq		:= 1		//Variavel que verifica se a sequencia do arquivo esta correta
Local aLinha	:= {}		//Array contendo todos os Registros ja desmembrados
Local lTemHead	:= .F.		//Variavel que verifica se existe o registro Header
Local lTemVnd	:= .F.		//Variavel que verifica se existe o registro Venda
Local lIncVnd	:= .T.		//Variavel que verifica se inclue ou nao registro Venda
Local lFirst	:= .T.		//Variavel que estancia a existencia de Detalhes da Venda somente uma vez
Local lTemRod	:= .F.		//Variavel que verifica se existe o registro Trailer
Local aDados	:= {}		//Variavel que guarda as informacoes de campos e os valores que deverao ser gravados nele
Local aLog		:= {}		//Array contendo LOG DE execu��o do arquivo em CSV
Local nRegExist	:= 0		//Verifica se foi escolhida uma opcao para registros ja existentes no tratamento do Detalhes do Arquivo
Local VDT_NUM   := ""
Local VDT_FILIA	:= ""
Local VDT_ESTAB := ""
Local VDT_TPLAN := ""
Local VDT_DESCR := ""
Local VDT_NPARC := ""
Local VDT_ADIQU := ""
Local VDT_BANDE := ""
Local VDT_TPTRA := ""
Local VDT_TRAID := ""
Local VDT_NSUST := ""
Local VDT_AUTOR := ""
Local VDT_DTPAG := ctod(Space(08))
Local VDT_VLPAG := 0 //""""
Local VDT_PTAXA := 0 //""""
Local VDT_STATU := ""
Local VDT_REFR1 := ""
Local VDT_REFR2 := ""
Local VDT_NRANT := ""
Local VDT_VLTXS := 0 //""""
Local VDT_CDBCO := ""
Local VDT_CDAGE := ""
Local VDT_CDCC  := ""
Local VDT_TID   := ""
Local VDT_TERMI := ""
Local VDT_DTVEN := ctod(Space(08))
Local VDT_MASCR := ""
Local VDT_NRROP := ""
Local VDT_NRROA := ""
Local LOG_ERRO  := "Erro"
Local LOG_INFO  := "Informativo"
Local aCampos		:= {}
Local aTam     	:= {}
Local cArqTrab		:=	""
Local cIndTmp  	:=	""
Local cNome			:= ""
Local cChave		:=	""
Local cArqReg100	:=	"TMP100"
Local aParc			:= {}
Local nI			:= 0
Local nEx1			:= 0
Local nEx2			:= 0
Local cFilFif    	:= ""
Local cChaveTmp     := ""
Local cAliasSitef		:= GetNextAlias()         		//Variavel que recebe o proximo Alias disponivel
Private cSeqFIF     := ""		//Sequencial da tabela FIF
Private aRegSE1       := {}
Private ABAIXA        := {}
Private nTamCODERP  := TamSX3("ZAK_NUM")[1]
Private nTamFILIAL  := TamSX3("ZAK_FILIAL")[1]
Private nTamSTATUS  := TamSX3("ZAK_STATUS")[1]
Private nTamNUMZ    := TamSX3("ZAK_NUM")[1]
Private nTamCODCLI	:= TamSX3("ZAK_CODCLI")[1]
Private nTamNOMECL  := TamSX3("ZAK_NOMECL")[1]
Private nTamEMISSA  := TamSX3("ZAK_EMISSA")[1]
Private nTamVENCTO  := TamSX3("ZAK_VENCTO")[1]
Private nTamPAGTO   := TamSX3("ZAK_PAGTO")[1]
Private nTamVLINI   := TamSX3("ZAK_VLINI")[1]
Private nTamVLLIQ   := TamSX3("ZAK_VLLIQ")[1]
Private nTamVLPAGO  := TamSX3("ZAK_VLPAGO")[1]
Private nTamTPPAGO  := TamSX3("ZAK_TPPAGO")[1]
Private nTamCARTAO  := TamSX3("ZAK_CARTAO")[1]
Private nTamNSUCAR  := TamSX3("ZAK_NSUCAR")[1]
Private nTamAUTCAR  := TamSX3("ZAK_AUTCAR")[1]
Private nTamPTXACC  := TamSX3("ZAK_PTXACC")[1]
Private nTamVTXACC  := TamSX3("ZAK_VTXACC")[1]
Private nTamFILORI  := TamSX3("ZAK_FILORI")[1]
Private nTamPREFIX  := TamSX3("ZAK_PREFIX")[1]
Private nTamPARCEL  := TamSX3("ZAK_PARCEL")[1]
Private nTamTPCRED  := TamSX3("ZAK_TPCRED")[1]
Private nTamMOEDA   := TamSX3("ZAK_MOEDA")[1]
Private nTamVMOEDA  := TamSX3("ZAK_VMOEDA")[1]
Private nTamACRDCR  := TamSX3("ZAK_ACRDCR")[1]
Private nTamBANCOT  := TamSX3("ZAK_BANCOT")[1]
Private nTamDTCHEQ  := TamSX3("ZAK_DTCHEQ")[1]
Private nTamNCHEQU  := TamSX3("ZAK_NCHEQU")[1]
Private nTamDOCORI  := TamSX3("ZAK_DOCORI")[1]
Private nTamTPCOBR  := TamSX3("ZAK_TPCOBR")[1]
Private nTamPREVIS  := TamSX3("ZAK_PREVIS")[1]
Private nTamEFETUA  := TamSX3("ZAK_EFETUA")[1]
Private nTamHISTOR  := TamSX3("ZAK_HISTOR")[1]
Private nTamFILBAI  := TamSX3("ZAK_FILBAI")[1]
Private nTamCLASSF  := TamSX3("ZAK_CLASSF")[1]
Private nTamPLANOC  := TamSX3("ZAK_PLANOC")[1]
Private nTamPREFIX  := TamSX3("ZAK_PREFIX")[1]
Private CON_CDBCO   := "CON"
Private CON_CDAGE   := "CONCI"
Private CON_CDCC    := "CONCIL"

/*======================================\
|Estrutura do Array aLog				|
|---------------------------------------|
|aLog[n][1] -> Linha da Ocorrencia		|
|aLog[n][2] -> Tipo da Ocorrencia		|
|aLog[n][3] -> Descricao da Ocorrencia	|
|aLog[n][4] -> Numero do T�tulo      	|
|aLog[n][5] ->
|aLog[n][6] ->
|aLog[n][7] ->
|aLog[n][8] ->
|aLog[n][9] ->
|aLog[n][10] ->
\======================================*/
ConoutR("Conciliador Concil - HFIN910B - A910VldArq - INICIO IMPORTANDO ARQUIVO - " + DToC(Date()) + " - Hora: " + TIME())
//If !LockByName( "HFIN910B"+cEmpAnt, .F. , .F. )
//	MsgStop("Esta rotina est� sendo utilizada por outro usu�rio. Tente novamente mais tarde.","HFIN910B" )//"Esta rotina est� sendo utilizada por outro usu�rio. Tente novamente mais tarde."
//	Return
//EndIf

//Carrega as filiais cadastradas no parametro MV_EMPTEF
//A910CarFil()

dbSelectArea("ZAK")
//	dbSetOrder(5)	//ZAK_FILIAL+ZAK_NUM+ZAK_STATUS
ZAK->(DbOrderNickName("ZAKNUMSTAT")) //ZAKNUMSTAT   filial + numero + status

nHdlFile := FT_FUse(cCamArq)
nRecCount := FT_FLASTREC()
fClose(nHdlFile)
FT_FUSE()

//se arquivo tiver mais de 2 mil registros realiza o commit e atualiza��o de tela a cada 1000
If nRecCount > 2000
	nFator := 1000
Else
	nFator := 1
EndIf
nLin:=0
ProcRegua(nRecCount/nFator )
nTam := 1000
//=====================================================================================
nHdlFile := FT_FUse(cCamArq)
if nHdlFile = -1
	MsgStop(" Problemas na abertura do Arquivo")
	return
endif
aRet      := {}
FT_FGoTop()
//BeginTran()
While !FT_FEOF()
	cLine := FT_FReadLn()
	if Len(alltrim(cLine))== 0
		FT_FSKIP()
		Loop
	Endif
	nRecno := FT_FRecno()
	cLinha := StrTran(cLine,'"',"'")
	cLinha := '{"'+cLinha+'"}'
	//adiciona o cLinha no array trocando o delimitador ; por , para ser reconhecido como elementos de um array
	cLinha := StrTran(cLinha,';','","')
	aAdd(aRet, &cLinha)
	IncProc("Processando..." + "(" + AllTrim(Str(nSeq)) + "/" + AllTrim(Str(nRecCount /*FT_FLASTREC()*/)) + ")")			//"Processando..."
	//caso usuario aborte pressionando botao cancelar
	If lEnd .And. Aviso( "Atencao","Abortar Processamento ?", {"Sim","Nao"} ) == 1
		//	DisarmTransaction()
		MsUnLockAll()
		Return
	Else
		lEnd := .F.
	EndIf
	If Alltrim(aRet[1][1]) == "C01"						//Cabe�alho do arquivo
		lTemHead := .T.
	ElseIf Alltrim(aRet[1][1]) == "C02"					//Detalhes do arquivo
		if Len(Alltrim(aRet[1][05])) ==  3
			nEx1 := 0
			nEx2 := 0
		Else
			nEx1 := 1
			nEx2 := 1
		Endif
		nPos := 0
		nPos := at('/',aRet[1][12])
		IF nPos <> 0
			nEx2 := nEx2 +1
		else
			nEx2 := nEx2
		Endif
		VDT_NUM    := aRet[1][02]
		VDT_FILIA  := aRet[1][03] //Alltrim(aLinha[DT_FILIA]) +Space(nTamFILIAL - Len(Alltrim(aLinha[DT_FILIA])) )
		VDT_ESTAB  := aRet[1][04] //Alltrim(aLinha[DT_ESTAB]) +Space(20         - Len(Alltrim(aLinha[DT_ESTAB])) )
		VDT_TPLAN  := aRet[1][05-nEx1] //Alltrim(aLinha[DT_TPLAN]) +Space(05         - Len(Alltrim(aLinha[DT_TPLAN])) )
		VDT_DESCR  := aRet[1][06-nEx1] //Alltrim(aLinha[DT_DESCR]) +Space(20         - Len(Alltrim(aLinha[DT_DESCR])) )
		VDT_NPARC  := aRet[1][07-nEx1] //Alltrim(aLinha[DT_NPARC]) +Space(nTamPARCEL - Len(Alltrim(aLinha[DT_NPARC])) )
		VDT_ADIQU  := aRet[1][08-nEx1] //Alltrim(aLinha[DT_ADIQU]) +Space(20         - Len(Alltrim(aLinha[DT_ADIQU])) )
		VDT_BANDE  := aRet[1][09-nEx1] //Alltrim(aLinha[DT_BANDE]) +Space(20		  - Len(Alltrim(aLinha[DT_BANDE])) )
		VDT_TPTRA  := aRet[1][10-nEx2] //Alltrim(aLinha[DT_TPTRA]) +Space(20		  - Len(Alltrim(aLinha[DT_TPTRA])) )
		VDT_TRAID  := aRet[1][11-nEx2] //Alltrim(aLinha[DT_TRAID]) +Space(20		  - Len(Alltrim(aLinha[DT_TRAID])) )
		VDT_NSUST  := aRet[1][12-nEx2] //Alltrim(aLinha[DT_NSUST]) +Space(nTamNSUCAR - Len(Alltrim(aLinha[DT_NSUST])) )
		VDT_AUTOR  := aRet[1][13-nEx2] //Alltrim(aLinha[DT_AUTOR]) +Space(nTamAUTCAR - Len(Alltrim(aLinha[DT_AUTOR])) )
		VDT_DTPAG  := aRet[1][14-nEx2] //Alltrim(aLinha[DT_DTPAG]) +Space(nTamPAGTO  - Len(Alltrim(aLinha[DT_DTPAG])) )
		VDT_VLPAG  := aRet[1][15-nEx2] //Alltrim(aLinha[DT_VLPAG]) +Space(nTamVLPAGO - Len(Alltrim(aLinha[DT_VLPAG])) )
		VDT_PTAXA  := aRet[1][16-nEx2] //Alltrim(aLinha[DT_PTAXA]) +Space(nTamPTXACC - Len(Alltrim(aLinha[DT_PTAXA])) )
		VDT_STATU  := aRet[1][17-nEx2] //Alltrim(aLinha[DT_STATU]) +Space(nTamSTATUS - Len(Alltrim(aLinha[DT_STATU])) )
		VDT_REFR1  := aRet[1][18-nEx2] //Alltrim(aLinha[DT_REFR1]) +Space(30		  - Len(Alltrim(aLinha[DT_REFR1])) )
		VDT_REFR2  := aRet[1][19-nEx2] //Alltrim(aLinha[DT_REFR2]) +Space(30  	      - Len(Alltrim(aLinha[DT_REFR2])) )
		VDT_NRANT  := aRet[1][20-nEx2] //Alltrim(aLinha[DT_NRANT]) +Space(20         - Len(Alltrim(aLinha[DT_NRANT])) )
		VDT_VLTXS  := aRet[1][21-nEx2] //Alltrim(aLinha[DT_VLTXS]) +Space(14  	      - Len(Alltrim(aLinha[DT_VLTXS])) )
		VDT_CDBCO  := aRet[1][22-nEx2] //Alltrim(aLinha[DT_CDBCO]) +Space(nTamBANCOT - Len(Alltrim(aLinha[DT_CDBCO])) )
		VDT_CDAGE  := aRet[1][23-nEx2] //Alltrim(aLinha[DT_CDAGE]) +Space(03		  - Len(Alltrim(aLinha[DT_CDAGE])) )
		VDT_CDCC   := aRet[1][24-nEx2] //Alltrim(aLinha[DT_CDCC])  +Space(10    	  - Len(Alltrim(aLinha[DT_CDCC ])) )
		VDT_TID    := aRet[1][25-nEx2] //Alltrim(aLinha[DT_TID])   +Space(10		  - Len(Alltrim(aLinha[DT_TID  ])) )
		VDT_TERMI  := aRet[1][26-nEx2] //Alltrim(aLinha[DT_TERMI]) +Space(20		  - Len(Alltrim(aLinha[DT_TERMI])) )
		VDT_DTVEN  := aRet[1][27-nEx2] //Alltrim(aLinha[DT_DTVEN]) +Space(nTamVENCTO - Len(Alltrim(aLinha[DT_DTVEN])) )
		VDT_MASCR  := aRet[1][28-nEx2] //Alltrim(aLinha[DT_MASCR]) +Space(15		  - Len(Alltrim(aLinha[DT_MASCR])) )
		VDT_NRROP  := aRet[1][29-nEx2] //Alltrim(aLinha[DT_NRROP]) +Space(20		  - Len(Alltrim(aLinha[DT_NRROP])) )
		VDT_NRROA  := aRet[1][30-nEx2] //Alltrim(aLinha[DT_NRROA]) +Space(20	      - Len(Alltrim(aLinha[DT_NRROA])) )
		//inicia transacao  -- somente na leitura do arquivo texto eh permitido abortar
		//                     transacao existe pq em algum momento ele deleta registro na tabela FIF
		
		xnTamanho  := at("/",VDT_NUM)
		if xnTamanho == 0
			v1         := strzero(val(VDT_NUM),9)
			v2         := "/"
		Else
			v1         := strzero(val(substr(VDT_NUM,1,xnTamanho-1)),9)
			v2         := substr(VDT_NUM,xnTamanho,5)
		endif
		cParcTot := Substr(alltrim(VDT_NPARC),2,2)
		cParcela := Substr(alltrim(VDT_NPARC),1,1)
		v3 := "/"+PgaParc(cParcTot,cParcela)
		if v3 <> v2
			VDT_NUM    := v1+v3
		Else
			VDT_NUM    := v1+v2
		Endif
		//		ZAK->(DbOrderNickName("ZAKNUMSTAT")) //ZAKNUMSTAT   filial + numero + status
		DBSELECTAREA("ZAK")
		ZAK->(DbSetOrder(1))   //ZAK_FILIAL, ZAK_NUM, ZAK_STATUS, R_E_C_N_O_, D_E_L_E_T_
		ZAK->(DbGotop())
		VDT_NUM := PADR(Alltrim(VDT_NUM),nTamNUMZ)
		IF ZAK->(Dbseek(xFilial("ZAK")+VDT_NUM),.F.)
			mSgStop("N�o Achei o registro "+VDT_NUM)
			aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,LOG_ERRO,"Detalhes -> Registro N�o Econtrado ( "+VDT_NUM+" ) Verifique -> ",VDT_NUM,VDT_NSUST,0,VDT_PTAXA,0,VDT_VLPAG,0 })
		Else
			VDT_NSUST:= STRZERO(val(VDT_NSUST),6)
			cValor:= VDT_PTAXA
			npos := At(',',Alltrim(cValor))
			IF npos <> 0
				cValor := substr(Alltrim(cValor),1,npos-1)+substr(Alltrim(cValor),npos+1,5)
			Endif
			nValor := val(cValor)/100
			VDT_PTAXA:= nValor
			cValor:= VDT_VLPAG
			npos := At(',',Alltrim(cValor))
			if nPos <> 0
				cValor := substr(Alltrim(cValor),1,npos-1)+substr(Alltrim(cValor),npos+1,5)
			endif
			nValor := val(cValor)/100
			VDT_VLPAG:= nValor
			
			lBaixar := .f.
			xNatureza := POSICIONE("SA1",1,xFilial("SA1")+ZAK->ZAK_CODCLI+ZAK->ZAK_LOJA,"A1_NATUREZ")
			If ALLTRIM(ZAK->ZAK_NUM) ==  ALLTRIM(VDT_NUM)
				Do case
					Case alltrim(VDT_TPLAN) == 'CANC'    //  cancelamentp de venda
						If nRegExist == 0 .AND. val(ZAK->ZAK_STATUS) <= 2 .AND. ALLTRIM(VDT_NSUST) == ALLTRIM(ZAK->ZAK_NSUCAR)
							ZAK->(RecLock("ZAK",.F.))
							ZAK->ZAK_STATUS := '5'
							ZAK->( MsUnLock() )
							aAdd(aLog,{"Linha -> "+ AllTrim(Str(nSeq)) ,LOG_ERRO,"Detalhes - Cancelamento Verifique -> ",VDT_NUM,VDT_NSUST,ZAK->ZAK_NSUCAR,VDT_PTAXA,ZAK->ZAK_PTXACC,VDT_VLPAG,ZAK->ZAK_VLINI  })
						Endif
					Case alltrim(VDT_TPLAN) == 'PCV'  //   comprovante
						If nRegExist == 0 .AND. val(ZAK->ZAK_STATUS) <= 3  .AND. ALLTRIM(VDT_NSUST) == STRZERO(VAL(ZAK->ZAK_NSUCAR),6)
							ZAK->(RecLock("ZAK",.F.))
							ZAK->ZAK_PAGTO  := ctod(VDT_DTPAG)
							ZAK->ZAK_VLPAGO := VDT_VLPAG
							ZAK->ZAK_BANCOT := VDT_CDBCO
							ZAK->ZAK_AGENCI := VDT_CDAGE
							ZAK->ZAK_CONTAC := VDT_CDCC
							lCont:= .f.
							If VDT_VLPAG+ZAK->ZAK_VTXACC == ZAK->ZAK_VLINI
								lCont:= .t.
							Else
								If ((VDT_VLPAG+ZAK->ZAK_VTXACC)-ZAK->ZAK_VLINI) <= 0.49 .and. ((VDT_VLPAG+ZAK->ZAK_VTXACC)-ZAK->ZAK_VLINI) >= -0.49
									lCont:= .t.
								Endif
							Endif
							If lCont
								cRegist:=transform(ZAK->ZAK_REGIST, "@E 9999999")
								If Select(cAliasSitef) > 0
									(cAliasSitef)->(DbCloseArea())
								Endif
								ZAK->ZAK_STATUS := '4'
								cQry := "SELECT   E1_BAIXA, R_E_C_N_O_ AS REGISTRO "
								cQry += "FROM " + AllTrim(RetSqlName("SE1")) + " SE1 WHERE R_E_C_N_O_ =  '" +cRegist + "' "
								cQry := ChangeQuery(cQry)
								dbUseArea(.T.,"TOPCONN",TCGenQry(,,cQry),cAliasSitef,.F.,.T.)
								If !(cAliasSitef)->(Eof())
									If Len(alltrim((cAliasSitef)->E1_BAIXA)) == 0
										nPoscc  := len(alltrim(VDT_CDCC))
										nxPos:=AT('/',ZAK->ZAK_NUM)
										//                   '            2     DANIEL                           3                  4           5             6
										AADD(AregSe1,{ZAK->ZAK_PREFIX,substr(ZAK->ZAK_NUM,1,nxPos-1),substr(ZAK->ZAK_NUM,nxPos+1,2),'NF ',ZAK->ZAK_CODCLI,ZAK->ZAK_LOJA,;
										"NOR",VDT_CDBCO,VDT_CDAGE,SUBSTR(ALLTRIM(VDT_CDCC),1,nPoscc-1),ZAK->ZAK_PAGTO,ZAK->ZAK_PAGTO,"Conciliador CONCIL", 0 , 0, 0 , 0 , 0 ,ZAK->ZAK_VLPAGO,xNatureza})
										//7       8        9                 10                               11         12             13          14  15  16  17  18      19            20
										//     		 					  AADD(aRegSE1,{ZAK->ZAK_PREFIX,substr(ZAK->ZAK_NUM,1,nxPos-1),substr(ZAK->ZAK_NUM,nxPos+1,2),'NF ',ZAK->ZAK_CODCLI,ZAK->ZAK_LOJA,;
										//	     		    			  "NOR",CON_CDBCO,CON_CDAGE,CON_CDCC,ZAK->ZAK_PAGTO,ZAK->ZAK_PAGTO,"Taxa C.Credito - CONCIL ",0,0,0,0,0,ZAK->ZAK_VTXACC,xNatureza})
										LOG_INFO := HFIN910BX(aRegSE1)
										if  LOG_INFO == "N�o baixado-Verifique"
											aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,LOG_INFO,"Detalhes.  Erro na Baixa Verifique -> ", VDT_NUM,VDT_NSUST,ZAK->ZAK_NSUCAR,VDT_PTAXA,ZAK->ZAK_PTXACC,VDT_VLPAG,ZAK->ZAK_VLINI  })
										else
											aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,LOG_INFO,"Detalhes.  Foi encontrado e Baixado Normal -> ", VDT_NUM,VDT_NSUST,ZAK->ZAK_NSUCAR,VDT_PTAXA,ZAK->ZAK_PTXACC,VDT_VLPAG,ZAK->ZAK_VLINI  })
										endif
										LOG_INFO  := "Informativo"
										aRegSE1 := {}
									Else
										aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,"Baixado Anteriormente" ,"Detalhes  o Titulo j� esta baixado  -> ", VDT_NUM,VDT_NSUST,ZAK->ZAK_NSUCAR,VDT_PTAXA,ZAK->ZAK_PTXACC,VDT_VLPAG,ZAK->ZAK_VLINI })
									endif
								endif
							Else
								ZAK->ZAK_STATUS := '3'
								aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,LOG_INFO,"Detalhes.  Foi encontrado, Verificar Valores  -> ", VDT_NUM,VDT_NSUST,ZAK->ZAK_NSUCAR,VDT_PTAXA,ZAK->ZAK_PTXACC,VDT_VLPAG,ZAK->ZAK_VLINI})
							Endif
							ZAK->( MsUnLock() )
						Else
							If ALLTRIM(VDT_NSUST) == STRZERO(VAL(ZAK->ZAK_NSUCAR),6) //STRZERO(val(VDT_NSUST),6)
								cStatus:=ZAK->ZAK_STATUS
								do case
									case ZAK->ZAK_STATUS == '1'
										cStatus := "N�o Processado"
									case ZAK->ZAK_STATUS == '2'
										cStatus := 	"Em Processo      "
									case ZAK->ZAK_STATUS == '3'              //
										LOG_ERRO := "Valores Divergentes"
										cStatus  := "Divergente"
									case ZAK->ZAK_STATUS == '4'              //
										LOG_ERRO := "Baixado Anteriormente"
										cStatus  := "Conciliado Normal"
									case ZAK->ZAK_STATUS == '5'              //
										LOG_ERRO := "Cancelado - Verifique"
										cStatus  :=	"Cancelado"
									case ZAK->ZAK_STATUS == '6'               //
										LOG_ERRO := "N�o Processado - Verifique"
										cStatus  := "Ant. Nao Processada"
									case ZAK->ZAK_STATUS == '7'               //
										LOG_ERRO := "Antecipado - Verifique"
										cStatus  :=	"Antecipado"
								Endcase
								aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,LOG_ERRO,"Detalhes  o Status ("+cStatus+")  J� esta diferente de em processo -> ", VDT_NUM,VDT_NSUST,ZAK->ZAK_NSUCAR,VDT_PTAXA,ZAK->ZAK_PTXACC,VDT_VLPAG,ZAK->ZAK_VLINI })
								cStatus := ""
							Else
								aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,LOG_ERRO,"Detalhes a Autoriza��o ("+ALLTRIM(VDT_NSUST)+") diferente Verifiqueo -> ", VDT_NUM,VDT_NSUST,ZAK->ZAK_NSUCAR,VDT_PTAXA,ZAK->ZAK_PTXACC,VDT_VLPAG,ZAK->ZAK_VLINI })
							Endif
						Endif
					OTHERWISE
						aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,LOG_ERRO,"Detalhes -> Tipo de Lan�amento ( "+VDT_DESCR+" ) est� diferente de Cancelamento ou Comprovante -> ",VDT_NUM,VDT_NSUST,0,VDT_PTAXA,0,VDT_VLPAG,0 })
				Endcase
			Else
				If ALLTRIM(VDT_NUM) == '000000000/'  .AND. (alltrim(VDT_TPLAN) == 'PCV' .or. alltrim(VDT_TPLAN) == 'CANC' ) //   comprovante
					ZAK->(RecLock("ZAK",.T.))
					ZAK->ZAK_FILIAL :=  xFilial("ZAK")
					ZAK->ZAK_NUM    :=  'AVE'+strZero(Recno(),6)
					ZAK->ZAK_PAGTO  := ctod(VDT_DTPAG)
					ZAK->ZAK_VLPAGO := VDT_VLPAG
					ZAK->ZAK_BANCOT := VDT_CDBCO
					ZAK->ZAK_AGENCI := VDT_CDAGE
					ZAK->ZAK_CONTAC := VDT_CDCC
					ZAK->ZAK_CARTAO := VDT_TRAID
					ZAK->ZAK_NSUCAR := VDT_NSUST
					ZAK->ZAK_AUTCAR := VDT_AUTOR
					ZAK->ZAK_VTXACC := VDT_PTAXA
					ZAK->ZAK_TPPAGO := VDT_TPTRA
					ZAK->ZAK_STATUS := '9'
					ZAK->( MsUnLock() )
					aAdd(aLog,{"Linha -> "+ AllTrim(Str(nSeq)) ,LOG_ERRO,"Detalhes - Omnichannel ou um Cancelamento Verifique -> ",VDT_NUM,VDT_NSUST,ZAK->ZAK_NSUCAR,VDT_PTAXA,ZAK->ZAK_PTXACC,VDT_VLPAG,ZAK->ZAK_VLINI  })
				Else
					aAdd(aLog,{	"Linha-> "+AllTrim(Str(nSeq)) ,LOG_ERRO,"Detalhes n�o encontrado correspondente -> ",VDT_NUM,VDT_NSUST,0,VDT_PTAXA,0,VDT_VLPAG,0 })
				Endif
				lTemRod := .F.
				lIncVnd := .F.
			endif
		endif
		//		if lBaixar
		//		   HFIN910BX(aRegSE1)
		//		   aRegSE1 := {}
		//		   lBaixar := .f.
		//		Endif
		//Soma um no sequenciador do arquivo
		nSeq++
	Endif
	FT_FSKIP()
	aRet      := {}
Enddo
//EndTran()		//termino transacao
MsUnlockAll()   //libera todos os registros lockados
//nSeq
//limpa array
fClose( nHdlFile)
UnlockByName("HFIN910B"+cEmpAnt, .F., .F. )
A910GrvLog(aLog)
Return lRet



/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �A910GrvArq�Autor  �Rafael Rosa da Silva� Data �  08/12/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao que efetua a gravacao dos registrosn na tabela FIF	  ���
���          �(Conciliacao do SITEF)									  ���
�������������������������������������������������������������������������͹��
���Uso       �															  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFIN910BX()
Local aArea   := GetArea()
Local aTitInd := {}
Local nTamPref 	:= 3  //TamSX3("E1_PREFIXO")[3]
Local nTamNume 	:= 9  //TamSX3("E1_NUM")[1]
Local nTamTipo 	:= 3  //TamSX3("E1_TIPO")[1]
Local nTamParc 	:= 3  //TamSX3("E1_PARCELA")[1]
Local nTamBanc 	:= 3  //TamSX3("E8_BANCO")[1]
Local nTamAgec 	:= 5  //TamSX3("E8_AGENCIA")[1]
Local nTamCont 	:= 10 //TamSX3("E8_CONTA")[1]
Local cRet      := space(21)    
Local T			:= 0

CONOUTR("Conciliador Concil - HFIN910B - Hfin910bx - INICIO baixa de t�tulos - " + DToC(Date()) + " - Hora: " + TIME())
For T := 1 to Len(aRegSE1)
	aTitInd	:=	{{"E1_PREFIXO"		,PADR(aRegSE1[T][01],nTamPref) ,NiL}                                                                                                                                     ,;
	{"E1_NUM"			,PADR(aRegSE1[T][02],nTamNume) ,NiL},;
	{"E1_PARCELA"		,PADR(aRegSE1[T][03],nTamParc) ,NiL},;
	{"E1_TIPO"			,PADR(aRegSE1[T][04],nTamTipo) ,NiL},;
	{"E1_CLIENTE"		,aRegSE1[T][05] 			   ,NiL},;
	{"E1_LOJA"			,aRegSE1[T][06] 			   ,NiL},;
	{"E1_NATUREZA"		,aRegSE1[T][20] 			   ,NiL},;
	{"AUTMOTBX"			,aRegSE1[T][07] 			   ,NiL},;
	{"AUTBANCO"			,PadR(aRegSE1[T][08],nTamBanc) ,Nil},;
	{"AUTAGENCIA"		,PadR(aRegSE1[T][09],nTamAgec) ,Nil},;
	{"AUTCONTA"	 		,PadR(aRegSE1[T][10],nTamCont) ,Nil},;
	{"AUTDTBAIXA"		,aRegSE1[T][11] 			   ,NiL},;
	{"AUTDTCREDITO"		,aRegSE1[T][12] 			   ,NiL},;
	{"AUTHIST"			,aRegSE1[T][13] 			   ,NiL},; //"Conciliador SITEF"
	{"AUTDESCONT"		,aRegSE1[T][14]    			   ,NiL},; //Valores de desconto
	{"AUTACRESC"		,aRegSE1[T][15] 			   ,NiL},; //Valores de acrescimo - deve estar cadastrado no titulo previamente
	{"AUTDECRESC"		,aRegSE1[T][16] 			   ,NiL},; //Valore de decrescimo - deve estar cadastrado no titulo previamente
	{"AUTMULTA"			,aRegSE1[T][17] 			   ,NiL},; //Valores de multa
	{"AUTJUROS"			,aRegSE1[T][18] 			   ,NiL},; //Valores de Juros
	{"AUTVALREC"		,aRegSE1[T][19] 			   ,NiL}}  //Valor recebido
	       
	cCli := aRegSE1[T][05]
	cLoj := aRegSE1[T][06] 
	cPref:= PADR(aRegSE1[T][01],nTamPref)
	cNum := PADR(aRegSE1[T][02],nTamNume)
	cParc:= PADR(aRegSE1[T][03],nTamParc)
	cTip := PADR(aRegSE1[T][04],nTamTipo)
	 
	//Chama rotina que altera o vencimento real para nao cobrar juros                                           		
	//fAltVenc(cCli, cLoj, cPref, cNum, cParc, cTip)
	
	//Efetua baixa individual
	aTitulosBX := aTitInd                    	
	lMsErroAuto := .f.
	MSExecAuto({|x, y| FINA070(x, y)}, aTitulosBX, 3)
	                                    	
	If lMsErroAuto
		MostraErro()
		cRet := "N�o baixado-Verifique"
	Else
		cRet := "Baixado  com  sucesso"
	Endif
	aTitInd :={}
Next T

	CONOUTR("Conciliador Concil - FINA910B - A910GrvArq - FIM GRAVANDO ARQUIVO - " + DToC(Date()) + " - Hora: " + TIME())

Return(cRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �A910GrvLog�Autor  �Rafael Rosa da Silva� Data �  08/12/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao que efetua a gravacao dos Logs						  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �															  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function A910GrvLog(aLog)

Local cType := "Arquivos LOG"+ "(*.CSV) |*.CSV|"			//"Arquivos LOG"	### "(*.log) |*.log|"
Local cDir	:= cGetFile(cType ,"Selecione o diretorio para grava��o do LOG",0,,.F.,GETF_LOCALHARD + GETF_NETWORKDRIVE + GETF_RETDIRECTORY)			//
Local nHdl	:= 0                           //Handle do arquivo
Local cDados:= ""                          //Descri��o da Linha
Local nI	:= 0                           //Variavel contadora de log
Local cLin	:= ""                          //Variavel da linha do log
Local cEOL	:= CHR(13)+CHR(10)            //Final de Linha
Local nFator := If(Len(alog) > 2000, 1000, 1)

//Incluo o nome do arquivo no caminho ja selecionado pelo usuario
cDir := Upper(Alltrim(cDir)) + "LOG_FINA910_" + dTos(dDataBase) + StrTran(Time(),":","") + ".CSV"

If (nHdl := FCreate(cDir)) == -1
	MsgInfo("O arquivo de nome " + cDir + " nao pode ser executado! Verifique os parametros.")			//	###
	Return
EndIf

cDados	:= "Linha da Ocorrencia;Tipo da Ocorrencia;Descricao da Ocorrencia;Nr. T�tulo; AUTORI ;ZAK_AUTORI; TAXA; ZAK_TAXA;VL PGTO;ZAK_VLLIQ"
cLin	:= Space(Len(cDados)) + cEOL
cLin	:= Stuff(cLin,01,Len(cDados),cDados)

If FWrite(nHdl,cLin,Len(cLin)) != Len(cLin)
	If Aviso("Atencao","Ocorreu um erro na gravacao do arquivo. Continua?",{"Sim","N�o"}) == 2		//	### 	###
		FClose(nHdl)
		Return
	EndIf
EndIf

CONOUTR("Conciliador Concil - FINA910B - A910GrvLog - INICIO GRAVANDO LOG - " + DToC(Date()) + " - Hora: " + TIME())

ProcRegua(Len(aLog)/nFator)

For nI := 1 to Len(aLog)
	
	If nI%nFator == 0
		IncProc("Gravando os Log's..." + "(" + AllTrim(Str(nI)) + "/" + AllTrim(Str(Len(aLog))) + ")")			//
	EndIf
	//IF type(aLog[nI][9]) == 'N'
	cDados	:= aLog[nI][1] + ';' + aLog[nI][2] + ';' + aLog[nI][3] + ';' + aLog[nI][4] + ';' +  transform(aLog[nI][5],"@e 999999") + ';' +  transform(aLog[nI][6],"@e 999999") + ';' +  transform(aLog[nI][7],"@e 99999.99") + ';' + transform(aLog[nI][8],"@e 99999.99") + ';' + Transform(aLog[nI][9],"@e 99999.99") + ';' + Transform(aLog[nI][10],"@e 99999.99") //+';'
	//eLSE
	//   cDados	:= aLog[nI][1] + ';' + aLog[nI][2] + ';' + aLog[nI][3] + ';' + aLog[nI][4] + ';' + aLog[nI][5] + ';' + aLog[nI][6] + ';' + aLog[nI][7] + ';' + transform(aLog[nI][8],"@e 999.99") + ';' + aLog[nI][9] + ';' + Transform(aLog[nI][10],"@e 999.99") //+';'
	//eNDIF
	cLin	:= Space( Len(cDados) ) + cEOL
	cLin	:= Stuff(cLin,01,Len(cDados),cDados)
	
	If FWrite(nHdl,cLin,Len(cLin)) != Len(cLin)
		If Aviso(3026,"Ocorreu um erro na gravacao do arquivo. Continua?",{ "Sim","N�o"}) == 2		//	### 	###
			FClose(nHdl)
			Return
		EndIf
	EndIf
Next nI

FClose(nHdl)

CONOUTR("Conciliador TEF - FINA910B - A910GrvLog - FIM GRAVANDO LOG - " + DToC(Date()) + " - Hora: " + TIME())

Return

static Function PgaParc(cParT,cParP) //06 5
Local aArea  := GetArea()
Local cRet   := ""
Local _cParc := "ABCDEFGHIJKLMNOPQR"
Local nLocal := val(cParP)
If cParP == '1'
	if cParT ='01'
		cRet := ''
	else
		cRet := Substr(_cParc,nLocal,1)   //'A'
	Endif
Else
	cRet := Substr(_cParc,nLocal,1)
Endif
RestArea(aArea)
Return(cRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �fAltVenc  �RXM					     � Data �  01/10/18   ���
�������������������������������������������������������������������������͹��
���Desc.     �Rotina que altera o vencimento real para nao cobrar juros   ���
���          �Pontos de entrada nao manipularam a variavel de juros       ���
�������������������������������������������������������������������������͹��
���Uso       �															  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function fAltVenc(cCli, cLoj, cPref, cNum, cParc, cTip)

Local aAreaSE1:= SE1->(GetArea())

dbSelectArea("SE1")
dbSetOrder(2) // E1_FILIAL, E1_CLIENTE, E1_LOJA, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, R_E_C_N_O_, D_E_L_E_T_
IF dbSeek(XFILIAL("SE1")+cCli+cLoj+cPref+cNum+cParc+cTip)
	RecLock("SE1",.F.)
	SE1->E1_VENCREA:= DataValida(SE1->E1_VENCREA+3, .T.) 
	SE1->(MsUnlock())
Endif

RestArea(aAreaSE1)
Return
