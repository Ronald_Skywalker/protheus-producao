#INCLUDE "PROTHEUS.CH"
/*
Le arquivo CSV (planilha com dados de titulos a pagar
Conforme seguencia abaixo:
PREFIXO	NUMERO PARCELA TIPO	NATUREZA PORTADOR FORNECEDOR LOJA NOME_FORNECEDOR EMISSAO VENCIMENTO VALOR CODIGO_DE_BARRAS	HISTORICO
PREFIXO	NUMERO DOC	PARCELA	TIPO	NATUREZA	PORTADOR	AGENCIA	CODIGO CLIENTE	LOJA	NOME	EMISSAO	VENCIENTO	VALOR	CENTRO CUSTO	EMISSAO	HISTORICO

1	             2	       3	      4	      5      	6	          7	            8	     9	10	       11	   12      	13	      14        	15	     16	           17	        18	       19	   20	      21	       22	      23	      24	      25	   26	       27	       28	          29	           30	  31	   32	      33	        34	       35	    36	       37	        38	        39	       40	        41	        42	        43	       44	      45	         46
E1_PREFIXO	N_DOCUMENTO	CHECK NF	CONTA	E1_NUM	E1_PARCELA	CODIGO_PROTHEUS	E1_NATUREZA	CPF	CNPJ	E1_LOJA	BLABAL	E1_EMISSAO	E1_VENCTO	E1_VENCREA	E1_VALOR	E1_NUMBCO	E1_HIST	E1_STUACA	E1_SALDO	E1_DESCONT	E1_MULTA	E1_JUROS	CUSTAS	E1_NUMNOTA	E1_VALLIQ	E1_IDCNAB	CODIGO_MILLENIUM	E1_XCOD	E1_XNSU	E1_XAUTNSU	E1_XCANAL	E1_XCANALD	E1_XEMP	E1_XDESEMP	E1_XGRUPO	E1_XGRUPON	E1_XCODDIV	E1_XNONDIV	E1_XMICRRE	E1_XMICRDE	E1_XCODREG	E1_XDESREG	E1_XFORREC	TIPO_GERADOR	EVENTO


*/
USER FUNCTION IMPZAR()  //U_IMPSE1()
IF MsgYesno("Confirma importa��o dA SED  NATUREZA ? " )
	Processa({|| u_IMPZARA()})

Endif
Return

USER FUNCTION IMPZARA()
Local aSE1      := {}
Local aCrateio  := {}
Local aErros    := {}
Local lSim      := .T.
Local nlin      := 0
Local aRet      := {} //
Local cTexto    := ""
Local cxTexto   := ""
Local NPARC     := 0
Local i			:= 0

PRIVATE lMsHelpAuto := .F.
PRIVATE lMsErroAuto := .F.


nVisibilidade := Nil													// Exibe todos os diret�rios do servidor e do cliente
//		nVisibilidade := GETF_ONLYSERVER										// Somente exibe o diret�rio do servidor
//		nVisibilidade := GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE	// Somente exibe o diret�rio do cliente

cArquivo := cGetFile("Todos os Arquivos ()*.*)     | *.* | ","Selecione um arquivo",0,,.T.,nVisibilidade, .T.)
nHandle := FT_FUse(cArquivo)
if nHandle = -1
	MsgStop(" Problemas na abertura do Arquivo")
	return
endif
nLinTot := FT_FLastRec()
Procregua(nLinTot)
FT_FGoTop()
While !FT_FEOF()
	cLine := FT_FReadLn()
	if Len(alltrim(cLine))== 0
		FT_FSKIP()
		Loop
	Endif
	nRecno := FT_FRecno()
	cLinha := StrTran(cLine,'"',"'")
	cLinha := '{"'+cLinha+'"}'
	//adiciona o cLinha no array trocando o delimitador ; por , para ser reconhecido como elementos de um array
	cLinha := StrTran(cLinha,';','","')
	aAdd(aRet, &cLinha)
	nlin++
	IncProc("Processando  Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)) )
	If nlin >= 2
	   cNatureza:= Padr(Alltrim(aRet[1][3]),10)
	   dbSelectArea("SED")
	   SED->(dbSetOrder(1))
	   If SED->(dbSeek(xFilial("SED")+cNatureza))
 	      SED->(RecLock("SED",.F.))
          SED->ED_CONTA   := Alltrim(aRet[1][1])
          SED->(MsUnLock())
       Endif
	Endif
	// Pula para pr�xima linha
	FT_FSKIP()
	aRet      := {}
Enddo
cxTexto:=""
for i := 1 to len(aErros)
	cxTexto+= aErros[i][1]+"  "+CHR(13)+CHR(10)
Next i
aRet      := {}
lSim      := .T.
cTexto    := cxTexto
If len(aErros) >0
	cTexto := "Log de Importa��o, Dados n�o Importados. "+CHR(13)+CHR(10)
	for i := 1 to len(aErros)
		cTexto+= aErros[i][1]+"  "+CHR(13)+CHR(10)
	Next i
	__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)
Endif
if len(alltrim(cTexto)) == 0
	MsgInfo("Fim da Importa��o -  com sucesso        "+CHR(13)+CHR(10)+cTexto)

Else
	MsgInfo("Fim da Importa��o -  Verifique erros   "+CHR(13)+CHR(10)+cTexto)
Endif
Return()


static FUNCTION NoAcento(cString)
Local cChar  := ""
Local nX     := 0
Local nY     := 0
Local cVogal := "aeiouAEIOU"
Local cAgudo := "�����"+"�����"
Local cCircu := "�����"+"�����"
Local cTrema := "�����"+"�����"
Local cCrase := "�����"+"�����"
Local cTio   := "����"
Local cCecid := "��"
Local cMaior := "&lt;"
Local cMenor := "&gt;"
Local nx	:= 0

For nX:= 1 To Len(cString)
	cChar:=SubStr(cString, nX, 1)
	IF cChar$cAgudo+cCircu+cTrema+cCecid+cTio+cCrase
		nY:= At(cChar,cAgudo)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr(cVogal,nY,1))
		EndIf
		nY:= At(cChar,cCircu)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr(cVogal,nY,1))
		EndIf
		nY:= At(cChar,cTrema)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr(cVogal,nY,1))
		EndIf
		nY:= At(cChar,cCrase)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr(cVogal,nY,1))
		EndIf
		nY:= At(cChar,cTio)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr("aoAO",nY,1))
		EndIf
		nY:= At(cChar,cCecid)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr("cC",nY,1))
		EndIf
	Endif
Next

If cMaior$ cString
	cString := strTran( cString, cMaior, "" )
EndIf
If cMenor$ cString
	cString := strTran( cString, cMenor, "" )
EndIf

cString := StrTran( cString, CRLF, " " )

For nX:=1 To Len(cString)
	cChar:=SubStr(cString, nX, 1)
	If (Asc(cChar) < 32 .Or. Asc(cChar) > 123) .and. !cChar $ '|'
		cString:=StrTran(cString,cChar,".")
	Endif
Next nX
Return cString

