#Include 'Protheus.ch'

User Function HPREFZFK()
Local cRet  := ''
Local cPerg := 'HPRFCARTAO'  
IF MsgYesno("1- Confirma Atualiza玢o das FKs do contas a receber? " )

		Processa({|| u_refazfk()})
ENDIF
Return


User Function refazfk()
Local cIdMov    := '' //FWUUIDV4()
Local cIdDoc	:= '' //FINGRVFK7("SE1", cChaveTit)
Local cChaveTit := '' 
Private cPergCont	:= 'HPRFCARTAO' 
************************
*Monta pergunte do Log *
************************
AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif
nLin   := 0 	
nLinTot:= 700	
Procregua(nLinTot)
dbSelectArea("SE5") 
SE5->(dbSetOrder(1)) // E5_FILIAL+DTOS(E5_DATA)+E5_BANCO+E5_AGENCIA+E5_CONTA+E5_NUMCHEQ                                                                                                 
SE5->(DBSEEK(xFilial("SE1")+DTOS(MV_PAR01))) 
WHILE SE5->(!EOF()) .and. DTOS(E5_DATA) >= DTOS(MV_PAR01) .and. DTOS(E5_DATA) <= DTOS(MV_PAR02)
   IF SE5->E5_BANCO = '341' .AND. SE5->E5_CONTA =  '14677' .AND. SE5->E5_TIPODOC = 'VL' 
      IF SE5->E5_RECPAG == 'R'
         dbSelectArea('SE1')
         SE1->(dbSetOrder(1)) // E1_FILIAL, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, R_E_C_N_O_, D_E_L_E_T_
         IF SE1->(dbSeek(xFilial("SE1")+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO))
            cChaveTit := xFilial("SE1")+"|"+SE1->E1_PREFIXO+"|"+SE1->E1_NUM+"|"+SE1->E1_PARCELA+"|"+SE1->E1_TIPO+"|"+SE1->E1_CLIENTE+"|"+SE1->E1_LOJA
            cIdMov    := FWUUIDV4()
	        cIdDoc	   := FINGRVFK7("SE1", cChaveTit)
            IF !EMPTY(SE5->E5_IDORIG)
               cIdMov2:= SE5->E5_IDORIG
            Else
               cIdMov2:= cIdMov 
               SE5->(RecLock("SE5",.F.))
       	       SE5->E5_IDORIG == cIdMov2
       	       SE5->(MsUnLock())
		    Endif
            //ATUALIZAR FK1  -> Baixas a receber	Registrar as baixas de t韙ulos a receber
            dbSelectArea("FK1")
            FK1->(dbSetOrder(1)) // FK1_FILIAL+FK1_IDFK1 
            IF FK1->(dbSeek(xFilial("FK1")+cIdMov2))
               FK1->(RecLock("FK1",.F.))
            ELSE   
               FK1->(RecLock("FK1",.T.))
            ENDIF
            FK1->FK1_FILIAL := xFilial("FK1")
            FK1->FK1_IDFK1  := cIdMov2 
            FK1->FK1_DATA   := SE5->E5_DATA
            FK1->FK1_VALOR  := SE5->E5_VALOR
            FK1->FK1_MOEDA  := SE5->E5_MOEDA
            FK1->FK1_NATURE := SE5->E5_NATUREZ
            FK1->FK1_VENCTO := SE5->E5_VENCTO
            FK1->FK1_RECPAG := SE5->E5_RECPAG 
            FK1->FK1_TPDOC  := SE5->E5_TIPO  
            FK1->FK1_HISTOR := SE5->E5_HISTOR
            FK1->FK1_VLMOE2 := SE5->E5_VLMOED2
            FK1->FK1_LOTE   := SE5->E5_LOTE
            FK1->FK1_MOTBX  := SE5->E5_MOTBX
            FK1->FK1_ORDREC := ''
            FK1->FK1_FILORI := SE1->E1_MSFIL
            FK1->FK1_ARCNAB := SE5->E5_ARQCNAB
            FK1->FK1_CNABOC := SE5->E5_CNABOC
            FK1->FK1_TXMOED := SE5->E5_TXMOEDA
            FK1->FK1_SITCOB := SE5->E5_SITCOB
            FK1->FK1_SERREC := SE5->E5_SERREC
            FK1->FK1_MULNAT := SE5->E5_MULTNAT
            FK1->FK1_AUTBCO := SE5->E5_AUTBCO
            FK1->FK1_CCUSTO := SE5->E5_CCUSTO
            FK1->FK1_ORIGEM := SE5->E5_ORIGEM
            FK1->FK1_SEQ    := SE5->E5_SEQ  
            FK1->FK1_DIACTB := '01'
            FK1->FK1_NODIA  := ''
            FK1->FK1_LA     := "N"
            FK1->FK1_IDDOC  := cIdDoc 
            FK1->FK1_DOC    := SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO+SE5->E5_CLIENTE+SE5->E5_LOJA
            FK1->FK1_IDCOMP := ""
            FK1->FK1_IDPROC := ""
            FK1->(MsUnLock())
         ENDIF 
         //ATUALIXAR FK5  -> Movimentos banc醨ios	Registra as movimenta珲es banc醨ias de t韙ulos a receber, a pagar e individuais 
         dbSelectArea("FK5")
         FK5->(dbSetOrder(1)) // FK5_FILIAL+FK5_IDMOV 
         IF FK5->(dbSeek(xFilial("FK5")+cIdMov2))
            FK5->(RecLock("FK5",.F.))
         ELSE   
            FK5->(RecLock("FK5",.T.))
         ENDIF
         FK5->FK5_FILIAL := xFilial("FK5")
         FK5->FK5_IDMOV  := cIdMov2 
         FK5->FK5_DATA   := SE5->E5_DATA
         FK5->FK5_VALOR  := SE5->E5_VALOR
         FK5->FK5_MOEDA  := SE5->E5_MOEDA
         FK5->FK5_NATURE := SE5->E5_NATUREZ
         FK5->FK5_RECPAG := SE5->E5_RECPAG
         FK5->FK5_TPDOC  := SE5->E5_TIPODOC
         FK5->FK5_FILORI := SE1->E1_MSFIL
         FK5->FK5_ORIGEM := SE5->E5_ORIGEM
         FK5->FK5_BANCO  := SE5->E5_BANCO
         FK5->FK5_AGENCI := SE5->E5_AGENCIA
         FK5->FK5_CONTA  := SE5->E5_CONTA
         FK5->FK5_NUMCH  := SE5->E5_NUMCHEQ
         FK5->FK5_DOC    := SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO+SE5->E5_CLIENTE+SE5->E5_LOJA
         FK5->FK5_HISTOR := SE5->E5_HISTOR
         FK5->FK5_VLMOE2 := SE5->E5_VLMOED2
         FK5->FK5_DTCONC := iif(SE5->E5_RECONC == 'x',SE5->E5_DATA,CTOD(SPACE(8)) )
         FK5->FK5_DTDISP := SE5->E5_DTDISPO
         FK5->FK5_MODSPB := SE5->E5_MODSPB
         FK5->FK5_SEQCON := SE5->E5_SEQCON
         //FK5->FK5_TERCEI := SE5->E5_TERCEI
         FK5->FK5_TPMOV  := '1'
         FK5->FK5_OK     := SE5->E5_OK
         //FK5->FK5_STATUS := SE5->E5_STATUS
         FK5->FK5_RATEIO := SE5->E5_RATEIO
         FK5->FK5_SEQ    := SE5->E5_SEQ
         //FK5->FK5_PROTRA := SE5->E5_PROTRA
         FK5->FK5_CCUSTO := SE5->E5_CCUSTO
         FK5->FK5_NUMBOR := SE1->E1_NUMBOR
         FK5->FK5_LA     := SE5->E5_LA
         FK5->FK5_TXMOED := SE5->E5_TXMOEDA
         FK5->FK5_ORDREC := SE5->E5_ORDREC
         FK5->FK5_LOTE   := SE5->E5_LOTE
         FK5->FK5_IDDOC  := cIdDoc
         FK5->(MsUnLock())
       //ATUALIZAR FK7  -> Tabela auxiliar	Nessa tabela s鉶 armazenadas as chaves de amarra玢o entre as tabelas SE1 e SE2 com as tabelas FK1 e FK2
         dbSelectArea("FK7")  //    , 9ff66e48c9a0443880167e45a31e2000, 0
         FK7->(dbSetOrder(1)) // FK7_FILIAL+FK7_IDDOC 
         IF FK7->(dbSeek(xFilial("FK7")+cIdDoc))
            FK7->(RecLock("FK7",.F.))
         ELSE   
            FK7->(RecLock("FK7",.T.))
         ENDIF   
         FK7->FK7_FILIAL := xFilial("FK7")
         FK7->FK7_IDDOC  := cIdDoc
         FK7->FK7_ALIAS  := 'SE1'
         FK7->FK7_CHAVE  := cChaveTit
         FK7->(MsUnLock())       
	     nLin++	
         IncProc("Processando  Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)) )
      endif
      /*
      If SE5->E5_RECPAG == 'P'
         dbSelectArea('SE2')
         SE2->(dbSetOrder(1)) // E1_FILIAL, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, R_E_C_N_O_, D_E_L_E_T_
         IF SE2->(dbSeek(xFilial("SE2")+SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO))
            cChaveTit := xFilial("SE2")+"|"+SE2->E2_PREFIXO+"|"+SE2->E2_NUM+"|"+SE2->E2_PARCELA+"|"+SE2->E2_TIPO+"|"+SE2->E1_CLIENTE+"|"+SE2->E1_LOJA
            cIdMov    := FWUUIDV4()
	        cIdDoc	   := FINGRVFK7("SE2", cChaveTit)
            IF !EMPTY(SE5->E5_IDORIG)
               cIdMov2:= SE5->E5_IDORIG
            Else
               cIdMov2:= cIdMov 
               SE5->(RecLock("SE5",.F.))
       	       SE5->E5_IDORIG == cIdMov2
       	       SE5->(MsUnLock())
		    Endif
            //ATUALIZAR FK1  -> Baixas a receber	Registrar as baixas de t韙ulos a receber
            dbSelectArea("FK2")
            FK2->(dbSetOrder(1)) // FK1_FILIAL+FK1_IDFK1 
            IF FK2->(dbSeek(xFilial("FK2")+cIdMov2))
               FK2->(RecLock("FK2",.F.))
            ELSE   
               FK2->(RecLock("FK2",.T.))
            ENDIF
            FK2->FK2_FILIAL := xFilial("FK1")
            FK2->FK2_IDFK1  := cIdMov2 
            FK2->FK2_DATA   := SE5->E5_DATA
            FK2->FK2_VALOR  := SE5->E5_VALOR
            FK2->FK2_MOEDA  := SE5->E5_MOEDA
            FK2->FK2_NATURE := SE5->E5_NATUREZ
            FK2->FK2_VENCTO := SE5->E5_VENCTO
            FK2->FK2_RECPAG := SE5->E5_RECPAG 
            FK2->FK2_TPDOC  := SE5->E5_TIPO  
            FK2->FK2_HISTOR := SE5->E5_HISTOR
            FK2->FK2_VLMOE2 := SE5->E5_VLMOED2
            FK2->FK2_LOTE   := SE5->E5_LOTE
            FK2->FK2_MOTBX  := SE5->E5_MOTBX
            FK2->FK2_ORDREC := ''
            FK2->FK2_FILORI := SE1->E1_MSFIL
            FK2->FK2_ARCNAB := SE5->E5_ARQCNAB
            FK2->FK2_CNABOC := SE5->E5_CNABOC
            FK2->FK2_TXMOED := SE5->E5_TXMOEDA
            FK2->FK2_SITCOB := SE5->E5_SITCOB
            FK2->FK2_SERREC := SE5->E5_SERREC
            FK2->FK2_MULNAT := SE5->E5_MULTNAT
            FK2->FK2_AUTBCO := SE5->E5_AUTBCO
            FK2->FK2_CCUSTO := SE5->E5_CCUSTO
            FK2->FK2_ORIGEM := SE5->E5_ORIGEM
            FK2->FK2_SEQ    := SE5->E5_SEQ  
            FK2->FK2_DIACTB := '01'
            FK2->FK2_NODIA  := ''
            FK2->FK2_LA     := "N"
            FK2->FK2_IDDOC  := cIdDoc 
            FK2->FK2_DOC    := SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO+SE5->E5_CLIENTE+SE5->E5_LOJA
            FK2->FK2_IDCOMP := ""
            FK2->FK2_IDPROC := ""
            FK2->(MsUnLock())
         ENDIF          
         //ATUALIXAR FK5  -> Movimentos banc醨ios	Registra as movimenta珲es banc醨ias de t韙ulos a receber, a pagar e individuais 
         dbSelectArea("FK5")
         FK5->(dbSetOrder(1)) // FK5_FILIAL+FK5_IDMOV 
         IF FK5->(dbSeek(xFilial("FK5")+cIdMov2))
            FK5->(RecLock("FK5",.F.))
         ELSE   
            FK5->(RecLock("FK5",.T.))
         ENDIF
         FK5->FK5_FILIAL := xFilial("FK1")
         FK5->FK5_IDMOV  := cIdMov2 
         FK5->FK5_DATA   := SE5->E5_DATA
         FK5->FK5_VALOR  := SE5->E5_VALOR
         FK5->FK5_MOEDA  := SE5->E5_MOEDA
         FK5->FK5_NATURE := SE5->E5_NATUREZ
         FK5->FK5_RECPAG := SE5->E5_RECPAG
         FK5->FK5_TPDOC  := SE5->E5_TIPODOC
         FK5->FK5_FILORI := SE1->E1_MSFIL
         FK5->FK5_ORIGEM := SE5->E5_ORIGEM
         FK5->FK5_BANCO  := SE5->E5_BANCO
         FK5->FK5_AGENCI := SE5->E5_AGENCIA
         FK5->FK5_CONTA  := SE5->E5_CONTA
         FK5->FK5_NUMCH  := SE5->E5_NUMCHEQ
         FK5->FK5_DOC    := SE5->E5_PREFIXO+SE5->E5_NUMERO+SE5->E5_PARCELA+SE5->E5_TIPO+SE5->E5_CLIENTE+SE5->E5_LOJA
         FK5->FK5_HISTOR := SE5->E5_HISTOR
         FK5->FK5_VLMOE2 := SE5->E5_VLMOED2
         FK5->FK5_DTCONC := iif(SE5->E5_RECONC == 'x',SE5->E5_DATA,CTOD(SPACE(8)) )
         FK5->FK5_DTDISP := SE5->E5_DTDISPO
         FK5->FK5_MODSPB := SE5->E5_MODSPB
         FK5->FK5_SEQCON := SE5->E5_SEQCON
         //FK5->FK5_TERCEI := SE5->E5_TERCEI
         FK5->FK5_TPMOV  := '1'
         FK5->FK5_OK     := SE5->E5_OK
         //FK5->FK5_STATUS := SE5->E5_STATUS
         FK5->FK5_RATEIO := SE5->E5_RATEIO
         FK5->FK5_SEQ    := SE5->E5_SEQ
         //FK5->FK5_PROTRA := SE5->E5_PROTRA
         FK5->FK5_CCUSTO := SE5->E5_CCUSTO
         FK5->FK5_NUMBOR := SE1->E1_NUMBOR
         FK5->FK5_LA     := SE5->E5_LA
         FK5->FK5_TXMOED := SE5->E5_TXMOEDA
         FK5->FK5_ORDREC := SE5->E5_ORDREC
         FK5->FK5_LOTE   := SE5->E5_LOTE
         FK5->FK5_IDDOC  := cIdDoc
         FK5->(MsUnLock())
       //ATUALIZAR FK7  -> Tabela auxiliar	Nessa tabela s鉶 armazenadas as chaves de amarra玢o entre as tabelas SE1 e SE2 com as tabelas FK1 e FK2
         dbSelectArea("FK7")  //    , 9ff66e48c9a0443880167e45a31e2000, 0
         FK7->(dbSetOrder(1)) // FK7_FILIAL+FK7_IDDOC 
         IF FK7->(dbSeek(xFilial("FK7")+cIdDoc))
            FK7->(RecLock("FK7",.F.))
         ELSE   
            FK7->(RecLock("FK7",.T.))
         ENDIF   
         FK7->FK7_FILIAL := xFilial("FK7")
         FK7->FK7_IDDOC  := cIdDoc
         FK7->FK7_ALIAS  := 'SE1'
         FK7->FK7_CHAVE  := cChaveTit
         FK7->(MsUnLock())       
		 nLin++	
         IncProc("Processando  Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)) )
      ENDIF
      */  
   ENDIF   
   SE5->(dbSkip())
Enddo    
Return( Nil )


/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Verifica/cria SX1 a partir de matriz para verificacao          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � Especifico para Clientes Microsiga                             潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Static Function AjustaSX1(cPerg)

Local _sAlias	:= Alias()
Local aCposSX1	:= {}
Local aPergs	:= {}
Local nX 		:= 0
Local lAltera	:= .F.
Local nCondicao
Local cKey		:= ""
Local nJ		:= 0

aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

	Aadd(aPergs,{"Dt Emiss.de? ","","","mv_ch1","D",08,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Dt Emiss.ate?","","","mv_ch2","D",08,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})

dbSelectArea("SX1")
dbSetOrder(1)
For nX:=1 to Len(aPergs)
	lAltera := .F.
	If MsSeek(cPerg+Right(aPergs[nX][11], 2))
		If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
			aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
			lAltera := .T.
		Endif
	Endif
	
	If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
		lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
	Endif
	
	If ! Found() .Or. lAltera
		RecLock("SX1",If(lAltera, .F., .T.))
		Replace X1_GRUPO with cPerg
		Replace X1_ORDEM with Right(aPergs[nX][11], 2)
		For nj:=1 to Len(aCposSX1)
			If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
				Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
			Endif
		Next nj
		MsUnlock()
		cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."
		
		
		If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
			aHelpSpa := aPergs[nx][Len(aPergs[nx])]
		Else
			aHelpSpa := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
			aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
		Else
			aHelpEng := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
			aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
		Else
			aHelpPor := {}
		Endif
		
		PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
	Endif
Next


