#Include 'Protheus.ch'
#Include 'FwMVCDef.ch'
#INCLUDE "TBICONN.CH"
  
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA001  �Autor  �Bruno Parreira      � Data �  08/11/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa para geracao dos pedidos de venda de distribuicao  ���
���          �de material do departamento de marketing                    ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function MVCAPRO1()
Local _astru   := {}
Local _afields := {}     
Local _carq    := ""         
Local cQuery   := "" 
Local xFornec := ""
Local xLoja   := ""
Local xVecto  := ctod(space(8))
Local nValor  := 0
Local xItens  := ""
Local xRazao  := ""
Local cFiltraSCR
Local ca097Usr 	:= __cuserid//'000091'//RetCodUsr()
Private cDescXPTO := 'Total Selecionado: '
Private lMarcar  := .F.
Private oDlg
Private oMark
Private cPerg     := "HFATA001"
Private aRotina   := {} 
Private lContinua := .F.
Private cAtuar     := ""
Private cMark     := GetMark() 
Private aScreens  := getScreenRes()
Private cTotValPed:= "R$ 0,00"
                        
//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"     ,"C",  2,0})
AADD(_astru,{"CR_NUM"    ,"C",  9,0})
AADD(_astru,{"C7_FORNEC" ,"C",  6,0})
AADD(_astru,{"C7_LOJA"   ,"C",  4,0})
AADD(_astru,{"A2_NOMEFOR","C", 15,0})
AADD(_astru,{"CR_EMISSAO","D",  8,0})
AADD(_astru,{"CR_TOTAL"  ,"N", 16,6})
AADD(_astru,{"C7_ITENS"  ,"C",100,6})
AADD(_astru,{"CR_STATUS" ,"C",  1,0})
AADD(_astru,{"CR_REG"    ,"N", 08,0})
AADD(_astru,{"C7_REG"    ,"N", 08,0})

cArqTrab  := CriaTrab(_astru)
If Select("XTRB") > 0
	XTRB->(DbCloseArea())
EndIf

dbUseArea( .T.,, cArqTrab, "XTRB", .F., .F. )

//-------------------------------------------------------------------
// Verifica se o usuario possui direito de liberacao.           
//-------------------------------------------------------------------
dbSelectArea("SAK")
dbSetOrder(2)
//If  cUserName <> "ROBSON.MELO" .and. !MsSeek(xFilial("SAK")+RetCodUsr())
If  !MsSeek(xFilial("SAK")+RetCodUsr())
	Help(" ",1,"A097APROV") //  Usu�rio n�o esta cadastrado como aprovador. O  acesso  e  a utilizacao desta rotina e destinada apenas aos usu�rios envolvidos no processo de aprova��o de Pedido Compras definido pelos grupos de aprova��o.
	//dbSelectArea("SCR")
	//dbSetOrder(1)
	Return
Else
	If Pergunte("HPXP001",.T.) //Foi necessario incluir esse grupo de perguntas para nao modificar o padrao.
		dDtIni:= MV_PAR02
		dDtfIM:= MV_PAR03
		//-------------------------------------------------------------------
		// Controle de Aprovacao : CR_STATUS                
		// 01 - Bloqueado p/ sistema (aguardando outros niveis) 
		// 02 - Aguardando Liberacao do usuario                 
		// 03 - Pedido Liberado pelo usuario                    
		// 04 - Pedido Bloqueado pelo usuario                   
		// 05 - Pedido Liberado por outro usuario               
		//-------------------------------------------------------------------
		dbSelectArea("SCR")
		dbSetOrder(1)   
		SCR->(dbGoTop())      
 		If cFiltraSCR == NIL
 		    cFiltraSCR  := 'CR_FILIAL=="'+xFilial("SCR")+'"'+'.And.CR_USER=="'+ca097Usr+'"
   	    EndIf		
   	    
   		Do Case
			Case mv_par01 == 1
				cFiltraSCR += ' .And.(Alltrim(CR_STATUS)=="02".OR.Alltrim(CR_STATUS)=="05")' 
			Case mv_par01 == 2
				cFiltraSCR += ' .And.(Alltrim(CR_STATUS)=="03".OR.Alltrim(CR_STATUS)=="05")'
			Case mv_par01 == 3
				cFiltraSCR += ''
			OtherWise
				cFiltraSCR += ' .And.(Alltrim(CR_STATUS)=="01".OR.Alltrim(CR_STATUS)=="04")'
		EndCase
		//Inserido filtro de data a pedido do Marcilio
		cFiltraSCR += ' .And. (DTOS(CR_EMISSAO)>= "'+DTOS(dDtIni)+'" .AND. DTOS(CR_EMISSAO)<= "'+DTOS(dDtfIM)+'")'
   Else
     Return()
   EndIf
EndIf


Dbselectarea("SCR")
//SetFilterDefault(cFiltraSCR)
//SCR->(DbSetFilter({||&cFiltraSCR},cFiltraSCR))
SET FILTER TO &cFiltraSCR
SCR->(DbGoTop())
n:=0
While SCR->(!EOF())
   If SCR->CR_TIPO == "PC"       
      xFornec := ""
      xLoja   := ""
      xVecto  := ctod(space(8))
      nValor  := 0
      xItens  := ""
      xRazao  := ""
      xCR_reg := SCR->(RECNO())
      dbSelectArea("SC7")
      SC7->(dbSetOrder(1))
      if SC7->(dbSeek(xFilial("SC7")+AllTrim(SCR->CR_NUM)))

		  aItem:= {}

	      WHILE SC7->(!eof()) .AND. ALLTRIM(SC7->C7_NUM) == alltrim(SCR->CR_NUM)     

			Aadd(aItem,{SC7->C7_ITEM, SC7->C7_DESCRI})

	         xFornec := SC7->C7_FORNECE
	         xLoja   := SC7->C7_LOJA
	         dbSelectArea("SA2")
	         SA2->(dbSetOrder(1))
	         SA2->(dbgotop())
	         if SA2->(dbSeek(xFilial("SA2")+xFornec+xLoja))  
	     //       MsgStop("Achei o Cliente caracas!" )
	         Else
	      //       MsgStop("N�o achei!")
	         Endif        
	         xRazao  := SA2->A2_NOME
	         //dbSelectArea("SC7")         
	         nValor  := nValor+SC7->C7_TOTAL
	         If xItens  == ""
	            xItens  := SC7->C7_DESCRI
	         Else
	            xItens  := xItens+" - "+SC7->C7_DESCRI
	         Endif
	         XC7_REG := SC7->(RECNO())        
	         SC7->(DBSKIP())
	      Enddo
		  DbSelectArea("XTRB")        
		  XTRB->(RecLock("XTRB",.T.)) 
		  XTRB->CR_NUM     := SCR->CR_NUM             
		  XTRB->C7_FORNEC  := xFornec 
		  XTRB->C7_LOJA    := xLoja
		  XTRB->A2_NOMEFOR := xRazao 
		  XTRB->CR_EMISSAO := SCR->CR_EMISSAO 
		  XTRB->CR_TOTAL   := SCR->CR_TOTAL
		  XTRB->C7_ITENS   := xItens
		  XTRB->CR_STATUS  := SCR->CR_STATUS 
		  XTRB->CR_REG     := xCR_reg
		  XTRB->C7_REG     := xC7_reg
		  XTRB->(MsUnlock())        
	  Endif	  
	Endif
	SCR->(DbSkip())
//	n++
//	If n >= 20
//	   Exit
//	Endif   
Enddo
    
//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('XTRB')        

//define as colunas para o browse
aColunas := {;
{"Documento"       ,"CR_NUM"     ,"C",  9,0,"@!"},;
{"Cod.Fornec"      ,"C7_FORNEC"  ,"C",  6,0,"@!"},;
{"Lj Fornec"       ,"C7_LOJA"    ,"C",  4,0,"@!"},;
{"Nome Fornec"     ,"A2_NOMEFOR" ,"C", 30,0,"@!"},;
{"Emissao"         ,"CR_EMISSAO" ,"D", 08,0,""  },;
{"Valor"           ,"CR_TOTAL"   ,"N", 16,2,"@E 99,999,999.99"},;
{"Produto/Servi�o" ,"C7_ITENS"   ,"C",100,0,"@!"},;
{"Status"          ,"CR_STATUS"  ,"C",  1,0,"@!"},;
{"RegistroSCR"     ,"CR_REG"     ,"N", 10,0,"@!"},;
{"RegistroSC7"     ,"C7_REG"     ,"N", 10,0,"@!"}}

Static oDlg

If aScreens[2] >= 800		//resolu��o da tela
	DEFINE MSDIALOG oDlg TITLE "Aprova��o de Compras" FROM 000, 000  TO 705, 1400 COLORS 0, 16777215 PIXEL
	_nTam		:= 74
Else
	DEFINE MSDIALOG oDlg TITLE "Aprova��o de Compras" FROM 000, 000  TO 590, 1300 COLORS 0, 16777215 PIXEL
	_nTam		:= 66
End

oMark:SetOwner(oDlg)

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.F.)

oMark:SetDescription(cDescXPTO)
oMark:SetFieldMark('MK_OK')
                              
oMark:AddButton("Aprovar ","U_MVCAPRMCK(oMark,1)",,2,0)
oMark:AddButton("Reprovar ","U_MVCAPRMCK(oMark,2)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_MVCAPRINVE(oMark:Mark(),)",,2,0)

//u_TSayHtml()
//Indica o Code-Block executado ap�s a marca��o ou desmarca��o do registro
oMark:SetAfterMark({|| fMVCCONMK()})
oMark:SetDoubleClick({|| fMVCCONMK() })
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || fMVCCONMK()})
oMark:SetAllMark({ || MarkRec()})
oMark:SetTemporary()

//Ativando a janela
oMark:Activate()

oDlg:lMaximized := .T. //Maximiza a janela


//@ 004,200 Say "TESTANDO"               SIZE _nTam, 010 OF oDlg  COLORS 0, 16777215 PIXEL 
@ 004,090 MSGET oGet1 	VAR cTotValPed SIZE _nTam, 010 OF oDlg  COLORS 0, 16777215 PIXEL 
//When .F. 

ACTIVATE MSDIALOG oDlg CENTERED

If lContinua	
   If cAtuar = 'APR'	
      //MsgStop("Irei aprovar!")
      XTRB->(DbGoTop())
      While XTRB->(!Eof())

        If !Empty(XTRB->MK_OK) //Se diferente de vazio, foi marcado  

			cQuery:= "UPDATE "+ RetSqlName("SC7") +"  SET C7_APROV = '"+SAK->AK_COD+"' ,C7_CONAPRO = 'L' WHERE C7_NUM  = '"+ALLTRIM(XTRB->CR_NUM)+"'  AND  D_E_L_E_T_  = ''"
            TcSQLExec(cQuery)

			aAreaScr:= SCR->(GetArea())

			dbSelectArea("SCR")
			SCR->(dbSetOrder(1))
			SCR->(dbGoTop())

			If SCR->(dbSeek( xFilial("SCR") + "PC" + alltrim(XTRB->CR_NUM)))

				While SCR->(!EOF()) .and. Alltrim(SCR->CR_NUM) == Alltrim(XTRB->CR_NUM)

					RecLock("SCR",.F.) 
					SCR->CR_STATUS := '03'              
					SCR->CR_DATALIB := dDatabase              
					SCR->CR_USERLIB := SAK->AK_USER              
					SCR->CR_LIBAPRO := SAK->AK_COD              
					SCR->CR_VALLIB  := XTRB->CR_TOTAL              
					SCR->CR_TIPOLIM := SAK->AK_TIPO              
					MsUnlock()

					SCR->(dbSkip())
				Enddo

			Endif 

			RestArea(aAreaScr)

			U_HPWFMAIL(.T., Alltrim(XTRB->CR_NUM), SC7->C7_USER, aItem)
            //A097ProcLib(XTRB->CR_REG,2)
         Endif
         XTRB->( dbSkip() )
		 
      EndDo
   EndIf
   If cAtuar = 'REP'
      MsgStop('Irei Reprovar!')
      If XTRB->CR_STATUS $ "02"
	     A097ProcLib(XTRB->CR_REG,6)
      Else
	     Help("N�o � Poss�vel, Verifique! ",1,"A097BLOQ")  //N�o � possivel bloquear o documento selecionado.  
      EndIf   
         
   EndIf

EndIf 
//Limpar o arquivo tempor�rio
If !Empty(cArqTrab)
    Ferase(cArqTrab+GetDBExtension())
    Ferase(cArqTrab+OrdBagExt())
    cArqTrab := ""
Endif
 if !Empty('XTRB')
    XTRB->(DbCloseArea())
Endif
Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01NOTA�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria array contendo os itens das NFs selecionados.          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function MVCAPRNOTA(oMark)
Local aRet := {} 
Local aAreaTRB := GetArea()                 

XTRB->(DbGoTop())
While XTRB->(!Eof())
    If !Empty(XTRB->MK_OK) //Se diferente de vazio, foi marcado
        aAdd(aRet,{XTRB->D1_COD,XTRB->D1_QUANT,XTRB->D1_VUNIT,TRB->D1_DOC,XTRB->D1_SERIE,TRB->D1_ITEM})
    Endif
    XTRB->( dbSkip() )
EndDo

RestArea(aAreaTRB)

Return aRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01SLCN�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria array contendo os canais selecionados.                 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

Static Function MVCAPRSLCN(oMark)
Local aRet := {} 
Local aAreaTRB := GetArea()                 

XTRB->(DbGoTop())
While XTRB->(!Eof())
    If !Empty(XTRB->MK_OK) //Se diferente de vazio, foi marcado
        aAdd(aRet,{XTRB->ZA3_CODIGO,XTRB->ZA3_DESCRICAO})
    Endif
    XTRB->( dbSkip() )
EndDo

RestArea(aAreaTRB)

Return aRet      

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01SLCL�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria array contendo os clientes selecionados.               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function MVCAPRSLCL(oMark)
Local aRet := {} 
Local aAreaTRB := GetArea()                 

XTRB->(DbGoTop())
While XTRB->(!Eof())
    If !Empty(XTRB->MK_OK) //Se diferente de vazio, foi marcado
        aAdd(aRet,{XTRB->A1_COD,TRB->A1_LOJA})
    Endif
    XTRB->( dbSkip() )
EndDo

RestArea(aAreaTRB)

Return aRet 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01INVE�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Marca ou desmarca todos os itens da FWMarkBrowse.           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
              
User Function MVCAPRINVE(cMarca)
Local aAreaTRB  := XTRB->(GetArea())
Local nValMk  := 0

lMarcar := !lMarcar 
 
dbSelectArea("XTRB")
XTRB->(dbGoTop())
While !XTRB->(Eof())
    RecLock("XTRB",.F.)
    XTRB->MK_OK := IIf(lMarcar,cMarca,'  ')
    If XTRB->MK_OK == cMarca
    	nValMk+= XTRB->CR_TOTAL
    Endif
    MsUnlock()
    XTRB->(dbSkip())
EndDo

RestArea( aAreaTRB )

//Chama rotina que atualiza valores selecionados.
cTotValPed:= "R$ "+transform(nValMk,"@E 999,999,999.99")
oDlg:Refresh()
Return 
//cTotValPed//.T.
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01PROX�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Funcao para controlar a transicao de uma tela para outra.   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

User Function MVCAPRMCK(oMark,nTela)
Local nCont := 0 
Local aAreaTRB := GetArea()                 

XTRB->(DbGoTop())
While XTRB->(!Eof())
    If !Empty(XTRB->MK_OK) //Se diferente de vazio, foi marcado
        nCont++
    Endif
    XTRB->( dbSkip() )
EndDo
   	
If nCont == 0
    Alert("Selecione pelo menos um registro.")
    RestArea(aAreaTRB)
    lContinua := .F.
    Return
Endif

If nTela = 1
	If !MsgYesNo("Confirma a Aprova��o dos pedidos de Compras selecionados?","Confirma��o")
		RestArea(aAreaTRB)
    	lContinua := .F.
    	Return
    Else
        cAtuar := 'APR'	
    EndIf
EndIf
If nTela = 2
	If !MsgYesNo("Confirma a Reprova��o dos pedidos de Compras selecionados?","Confirma��o")
		RestArea(aAreaTRB)
    	lContinua := .F.
    	Return
    Else
        cAtuar := 'REP'	
    EndIf
EndIf

CloseBrowse()
oMark:DeActivate()

lContinua := .T.       

RestArea(aAreaTRB)

Return  

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01CANA�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta a tela para selecao dos canais de venda.              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFAT01CANA(oMark)
Local _astru   := {}
Local _afields := {}     
Local _carq    := ""         
Local cQuery   := ""

lContinua := .F.

lMarcar := .F.   

cQuery := "select ZA3_CODIGO,ZA3_DESCRI "
cQuery += CRLF + "from "+RetSqlName("ZA3")+" ZA3 "
cQuery += CRLF + "where ZA3.D_E_L_E_T_ = '' " 
cQuery += CRLF + "and ZA3_FILIAL = '"+xFilial("ZA3")+"' "
cQuery += CRLF + "order by ZA3_CODIGO "
    
MemoWrite("HFATA001_2.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)       
                         
//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"     ,"C", 2,0})
AADD(_astru,{"ZA3_CODIGO","C", 3,0})
AADD(_astru,{"ZA3_DESCRI","C",30,0})

cArqTrab  := CriaTrab(_astru)
dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )

Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		DbSelectArea("TRB")        
		RecLock("TRB",.T.) 
		TRB->ZA3_CODIGO := TMP->ZA3_CODIGO                
		TRB->ZA3_DESCRI := TMP->ZA3_DESCRI
		MsUnlock()        
		TMP->(DbSkip())
	EndDo
EndIf	
     
//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('TRB')        

//define as colunas para o browse
aColunas := {;
{"Codigo"   ,"ZA3_CODIGO","C",3 ,0,"@!"},;
{"Descricao","ZA3_DESCRI","C",30,0,"@!"}}

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.T.)
oMark:SetDescription('Seleciona Canais')    
oMark:SetFieldMark('MK_OK')
                              
oMark:AddButton("Selecionar Clientes","U_HFAT01PROX(oMark,2)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_HFAT01INVE(oMark:Mark())",,2,0)
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || HFAT01INVE(oMark:Mark(),lMarcar := !lMarcar ),oMark:Refresh(.T.)})

oMark:SetTemporary()
   
//Ativando a janela
oMark:Activate() 
                                                    
Return Nil 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01CLIE�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta a tela para selecao dos clientes.                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFAT01CLIE(oMark,aCanais)
Local _astru   := {}
Local _afields := {}     
Local _carq    := ""         
Local cQuery   := ""  
Local cCanais  := ""
Local nx := 0

lContinua := .F. 

lMarcar := .F.

For nx := 1 to Len(aCanais)
	cCanais += "'"+aCanais[nx][1]+"',"
Next                                   

cCanais := SubStr(cCanais,1,Len(cCanais)-1)

cQuery := "select A1_COD,A1_LOJA,A1_CGC,A1_NOME,A1_NREDUZ "
cQuery += CRLF + "from "+RetSqlName("SA1")+" SA1 "
cQuery += CRLF + "where SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "and A1_FILIAL = '"+xFilial("SA1")+"' "
cQuery += CRLF + "and A1_XCANAL IN ("+cCanais+") "
cQuery += CRLF + "and A1_MSBLQL <> '1' "
cQuery += CRLF + "order by A1_COD,A1_LOJA "
    
MemoWrite("HFATA001_3.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)       
                         
//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"     ,"C", 2,0})
AADD(_astru,{"A1_COD"    ,"C", 6,0})
AADD(_astru,{"A1_LOJA"   ,"C", 4,0})
AADD(_astru,{"A1_CGC"    ,"C",18,0})
AADD(_astru,{"A1_NOME"   ,"C",40,0}) 
AADD(_astru,{"A1_NREDUZ" ,"C",40,0})

cArqTrab  := CriaTrab(_astru)
dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )

Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		DbSelectArea("TRB")        
		RecLock("TRB",.T.) 
		TRB->A1_COD    := TMP->A1_COD                
		TRB->A1_LOJA   := TMP->A1_LOJA 
		TRB->A1_CGC    := TMP->A1_CGC                
		TRB->A1_NOME   := TMP->A1_NOME
		TRB->A1_NREDUZ := TMP->A1_NREDUZ 
		MsUnlock()        
		TMP->(DbSkip())
	EndDo
EndIf	
     
//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('TRB')        

//define as colunas para o browse
aColunas := {;
{"Cliente"      ,"A1_COD"   ,"C",6 ,0,"@!"},; 
{"Loja"         ,"A1_LOJA"  ,"C",4 ,0,"@!"},;
{"CNPJ"         ,"A1_CGC"   ,"C",18,0,"@!"},;
{"Razao Social" ,"A1_NOME"  ,"C",40,0,"@!"},;
{"Nome Fantasia","A1_NREDUZ","C",40,0,"@!"}}

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.T.)
oMark:SetDescription('Seleciona Clientes')    
oMark:SetFieldMark('MK_OK')
                              
oMark:AddButton("Aprova Pedidos","U_MVCAPRPROX(oMark,3)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_MVCAPRINVE(oMark:Mark())",,2,0)
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || HFAT01INVE(oMark:Mark(),lMarcar := !lMarcar ),oMark:Refresh(.T.)})

oMark:SetTemporary()
   
//Ativando a janela
oMark:Activate() 
                                                    
Return Nil 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01CLIE�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Gera PVs para os clientes selecionados.                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function MVCAPRPEDI(oMark,aItensNF,aClientes)
Local nCont     := 0 
Local nx,ny     := 0               
Local cPrd      := ""
Local aAux      := {}
Local nSoma     := 0
Local nQuant    := 0
Local nVunit    := 0
Local cNroPed   := "" 
Local cNumPVde  := "" 
Local cNumPVate := ""
Local cNatureza := "" 
Local cTES      := ""
Local cUM       := ""
Local aCabPV    := {}
Local aItemPV   := {}
Local aErro     := {}
Local aGerados  := {}
Local nErro     := 0
Local nGerados  := 0   
Local i
PRIVATE lMsErroAuto := .F.
     
//Agrupa os itens selecionados somando as qtdes de produtos comuns e ja divide pela qtde de clientes selecionados.

aSort(aItensNF)
cPrd  := aItensNF[1][1]    //Cod. Produto

nCont := Len(aClientes)

For nx := 1 to Len(aItensNF)
	nQuant := aItensNF[nx][2] //Qtde. do Item
	If cPrd <> aItensNF[nx][1]
		aAdd(aAux,{cPrd,nSoma/nCont,nVunit})
		nSoma := nQuant
	Else
	    nSoma += nQuant
	EndIf
	cPrd   := aItensNF[nx][1]
	nVunit := aItensNF[nx][3]	 //Valor Unitario do Item
Next 
aAdd(aAux,{cPrd,nSoma/nCont,nVunit}) 

ProcRegua(Len(aClientes))

If !Empty(mv_par14)
	cNatureza := mv_par14
Else
	cNatureza := "21314305" //Natureza de Material Promocional Mkt
EndIf 

cNumPVde := GetMV("MV_XNMPVMK")	

For nx := 1 to Len(aClientes)
    IncProc()  
     
    DbSelectArea("SC5")
    
    cNroPed := GetMV("MV_XNMPVMK")
    
    aCabec := {}
    aadd(aCabec,{"C5_NUM"   ,cNroPed,Nil})
	aadd(aCabec,{"C5_TIPO"   ,"N",Nil})
	aadd(aCabec,{"C5_CLIENTE",aClientes[nx][1],Nil})		
	aadd(aCabec,{"C5_LOJACLI",aClientes[nx][2],Nil})		
	aadd(aCabec,{"C5_LOJAENT",aClientes[nx][2],Nil})		
	aadd(aCabec,{"C5_POLCOM" ,MV_PAR15,Nil})
	aadd(aCabec,{"C5_TPPED"  ,MV_PAR17,Nil})
	aadd(aCabec,{"C5_CONDPAG",MV_PAR16,Nil})
	aadd(aCabec,{"C5_NATUREZ",cNatureza,Nil})
	    
	aItens := {}
	
    For ny := 1 to Len(aAux)
    	
    	DbSelectArea("SB1")
    	DbSeek(xFilial("SB1")+aAux[ny][1])
    	
    	DbSelectArea("SF4")
    	If DbSeek(xFilial("SF4")+IF(!EMPTY(MV_PAR13),MV_PAR13,"501"))
    		cTES := SF4->F4_CODIGO
    	Else
    		cTES := "501"
    	EndIf

		aLinha := {}
		aadd(aLinha,{"C6_ITEM",StrZero(nY,2),Nil})			
		aadd(aLinha,{"C6_PRODUTO",SB1->B1_COD,Nil})			
		aadd(aLinha,{"C6_QTDVEN",aAux[ny][2],Nil})			
		aadd(aLinha,{"C6_PRCVEN",aAux[ny][3],Nil})			
		aadd(aLinha,{"C6_PRUNIT",aAux[ny][3],Nil})			
		aadd(aLinha,{"C6_VALOR",ROUND(aAux[ny][2]*aAux[ny][3],2),Nil})			
		aadd(aLinha,{"C6_TES",SF4->F4_CODIGO,Nil})
		aadd(aLinha,{"C6_NUM",cNroPed,Nil})
		aadd(aItens,aLinha)		
    Next
    
    BEGIN TRANSACTION

	MATA410(aCabec,aItens,3)
		
	If lMsErroAuto
		MostraErro()
		nErro++
	Else 
		nGerados++
		cNumPVate := cNroPed
		cNroPed := SOMA1(SubStr(cNroPed,3,4))
		PutMV("MV_XNMPVMK","MK"+cNroPed)	
	EndIf
	
	END TRANSACTION	
        
Next  

If nErro > 0
	MsgInfo("Encontrado erro na gera��o de "+StrZero(nErro,4)+" pedidos.","Aviso")
EndIf

If nGerados > 0	
	MsgInfo("Foram gerados "+StrZero(nGerados,4)+" pedidos com sucesso. N�: "+cNumPVde+" ao "+cNumPVAte,"Aviso")
EndIf

CloseBrowse()	

Return 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )
  
PutSx1(cPerg,"01","Da Nota Fiscal ?    ","Da Nota Fiscal ?    ","Da Nota Fiscal ?    ","Mv_ch1",TAMSX3("D1_DOC"    )[3],TAMSX3("D1_DOC"    )[1],TAMSX3("D1_DOC"    )[2],0,"G","","SF1XSR","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o numero da nota fiscal.        ",""},{""},{""},"")
PutSx1(cPerg,"02","Da S�rie ?          ","Da S�rie ?          ","Da S�rie ?          ","Mv_ch2",TAMSX3("D1_SERIE"  )[3],TAMSX3("D1_SERIE"  )[1],TAMSX3("D1_SERIE"  )[2],0,"G","",      "","","N","Mv_par02","","","","","","","","","","","","","","","","",{"Informe a s�rie da nota fiscal.         ",""},{""},{""},"")
PutSx1(cPerg,"03","At� Nota Fiscal ?   ","At� Nota Fiscal ?   ","At� Nota Fiscal ?   ","Mv_ch3",TAMSX3("D1_DOC"    )[3],TAMSX3("D1_DOC"    )[1],TAMSX3("D1_DOC"    )[2],0,"G","","SF1XSR","","N","Mv_par03","","","","","","","","","","","","","","","","",{"Informe o numero da nota fiscal.        ",""},{""},{""},"")
PutSx1(cPerg,"04","At� S�rie ?         ","At� S�rie ?         ","At� S�rie ?         ","Mv_ch4",TAMSX3("D1_SERIE"  )[3],TAMSX3("D1_SERIE"  )[1],TAMSX3("D1_SERIE"  )[2],0,"G","",      "","","N","Mv_par04","","","","","","","","","","","","","","","","",{"Informe a s�rie da nota fiscal.         ",""},{""},{""},"")
PutSx1(cPerg,"05","De Fornecedor ?     ","De Fornecedor ?     ","De Fornecedor ?     ","Mv_ch5",TAMSX3("D1_FORNECE")[3],TAMSX3("D1_FORNECE")[1],TAMSX3("D1_FORNECE")[2],0,"G","",  "SA2A","","N","Mv_par05","","","","","","","","","","","","","","","","",{"Informe o c�digo do fornecedor.         ",""},{""},{""},"")
PutSx1(cPerg,"06","De Loja ?           ","De Loja ?           ","De Loja ?           ","Mv_ch6",TAMSX3("D1_LOJA"   )[3],TAMSX3("D1_LOJA"   )[1],TAMSX3("D1_LOJA"   )[2],0,"G","",      "","","N","Mv_par06","","","","","","","","","","","","","","","","",{"Informe a loja do fornecedor.           ",""},{""},{""},"") 
PutSx1(cPerg,"07","At� Fornecedor ?    ","At� Fornecedor ?    ","At� Fornecedor ?    ","Mv_ch7",TAMSX3("D1_FORNECE")[3],TAMSX3("D1_FORNECE")[1],TAMSX3("D1_FORNECE")[2],0,"G","",  "SA2A","","N","Mv_par07","","","","","","","","","","","","","","","","",{"Informe o c�digo do fornecedor.         ",""},{""},{""},"")
PutSx1(cPerg,"08","At� Loja ?          ","At� Loja ?          ","At� Loja ?          ","Mv_ch8",TAMSX3("D1_LOJA"   )[3],TAMSX3("D1_LOJA"   )[1],TAMSX3("D1_LOJA"   )[2],0,"G","",      "","","N","Mv_par08","","","","","","","","","","","","","","","","",{"Informe a loja do fornecedor.           ",""},{""},{""},"") 
PutSx1(cPerg,"09","Do Produto ?        ","Do Produto ?        ","Do Produto ?        ","Mv_ch9",TAMSX3("D1_COD"    )[3],TAMSX3("D1_COD"    )[1],TAMSX3("D1_COD"    )[2],0,"G","",   "SB1","","N","Mv_par09","","","","","","","","","","","","","","","","",{"Informe o c�digo do produto.            ",""},{""},{""},"")
PutSx1(cPerg,"10","At� Produto ?       ","At� Produto ?       ","At� Produto ?       ","Mv_chA",TAMSX3("D1_COD"    )[3],TAMSX3("D1_COD"    )[1],TAMSX3("D1_COD"    )[2],0,"G","",   "SB1","","N","Mv_par10","","","","","","","","","","","","","","","","",{"Informe o c�digo do produto.            ",""},{""},{""},"")
PutSx1(cPerg,"11","Da Emiss�o ?        ","Da Emiss�o ?        ","Da Emiss�o ?        ","Mv_chB",TAMSX3("D1_EMISSAO")[3],TAMSX3("D1_EMISSAO")[1],TAMSX3("D1_EMISSAO")[2],0,"G","",      "","","N","Mv_par11","","","","","","","","","","","","","","","","",{"Informe a data de emiss�o.              ",""},{""},{""},"")
PutSx1(cPerg,"12","At� Emiss�o ?       ","At� Emiss�o ?       ","At� Emiss�o ?       ","Mv_chC",TAMSX3("D1_EMISSAO")[3],TAMSX3("D1_EMISSAO")[1],TAMSX3("D1_EMISSAO")[2],0,"G","",      "","","N","Mv_par12","","","","","","","","","","","","","","","","",{"Informe a data de emiss�o.              ",""},{""},{""},"")
PutSx1(cPerg,"13","TES ?               ","TES ?               ","TES ?               ","Mv_chD",TAMSX3("D1_TES"    )[3],TAMSX3("D1_TES"    )[1],TAMSX3("D1_TES"    )[2],0,"G","",   "SF4","","N","Mv_par13","","","","","","","","","","","","","","","","",{"Informe a TES dos pedidos de venda.     ",""},{""},{""},"")
PutSx1(cPerg,"14","Natureza ?          ","Natureza ?          ","Natureza ?          ","Mv_chE",TAMSX3("ED_CODIGO" )[3],TAMSX3("ED_CODIGO" )[1],TAMSX3("ED_CODIGO" )[2],0,"G","",   "SED","","N","Mv_par14","","","","","","","","","","","","","","","","",{"Informe a natureza dos pedidos de venda.",""},{""},{""},"")
PutSx1(cPerg,"15","Politica Comercial ?","Politica Comercial ?","Politica Comercial ?","Mv_chF",TAMSX3("C5_POLCOM" )[3],TAMSX3("C5_POLCOM" )[1],TAMSX3("C5_POLCOM" )[2],0,"G","",   "SZ2","","N","Mv_par15","","","","","","","","","","","","","","","","",{"Informe a Pol. Com. dos pedidos de venda",""},{""},{""},"")
PutSx1(cPerg,"16","Cond. Pgto. ?       ","Cond. Pgto. ?       ","Cond. Pgto. ?       ","Mv_chG",TAMSX3("C5_CONDPAG")[3],TAMSX3("C5_CONDPAG")[1],TAMSX3("C5_CONDPAG")[2],0,"G","", "SZ3_2","","N","Mv_par16","","","","","","","","","","","","","","","","",{"Informe a Cond Pag. dos pedidos de venda",""},{""},{""},"")
PutSx1(cPerg,"17","Tipo Pedido ?       ","Tipo Pedido ?       ","Tipo Pedido ?       ","Mv_chH",TAMSX3("C5_TPPED"  )[3],TAMSX3("C5_TPPED"  )[1],TAMSX3("C5_TPPED"  )[2],0,"G","", "SZ4_2","","N","Mv_par17","","","","","","","","","","","","","","","","",{"Informe o Tipo dos pedidos de venda.    ",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function MarkRec()

Local cMarca:= oMark:Mark()
Local aAreaTRB:= XTRB->(GetArea())
Local nValMk  := 0
//Local cDescXPTO:= 'Aprova��o de Compras - '

dbSelectArea("XTRB")
XTRB->(dbGoTop())
While !XTRB->(Eof())
    If XTRB->MK_OK == cMarca
    	nValMk+= XTRB->CR_TOTAL
    Endif
    XTRB->(dbSkip())
EndDo

cTotValPed:= "R$ "+transform(nValMk,"@E 999,999,999.99")// cValtoChar(nValMk)

nValMk := transform(nValMk,"@E 999,999,999.99") 

//cFik:= Alltrim(cValtochar(nValMk))
//oMark:SetDescription(cDescXPTO + "Total selecionado: R$ " + Alltrim(cValtochar(nValMk)))

//XTRB->(DbGoTop())
//XTRB->(RecLock("XTRB",.f.))
//XTRB->TOTSLECT:= nValMk
//XTRB->(MsUnlock())
//oMark:Refresh(.f.)


//MsgInfo("Total selecionado: R$ " + Alltrim(cValtochar(nValMk)))
//SysRefresh()
RestArea( aAreaTRB )

//cRet:= cDescXPTO + "Total selecionado: R$ " + Alltrim(cValtochar(nValMk))
//'"'+cRet+'"'

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �fMVCCONMK �Autor  �R.Melo	             � Data � 04/12/2018  ���
�������������������������������������������������������������������������͹��
���Desc      �Totaliza titulos selecionados para aprovacao.               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function fMVCCONMK()
Local cMarca:= oMark:Mark()
Local aAreaTRB:= XTRB->(GetArea())
Local nValMk  := 0
//Local cDescXPTO:= 'Aprova��o de Compras - '

dbSelectArea("XTRB")
XTRB->(dbGoTop())
While !XTRB->(Eof())
    If XTRB->MK_OK == cMarca
    	nValMk+= XTRB->CR_TOTAL
    Endif
    XTRB->(dbSkip())
EndDo

cTotValPed:= "R$ "+transform(nValMk,"@E 999,999,999.99")// cValtoChar(nValMk)

nValMk := transform(nValMk,"@E 999,999,999.99") 

//cFik:= Alltrim(cValtochar(nValMk))
//oMark:SetDescription(cDescXPTO + "Total selecionado: R$ " + Alltrim(cValtochar(nValMk)))

//XTRB->(DbGoTop())
//XTRB->(RecLock("XTRB",.f.))
//XTRB->TOTSLECT:= nValMk
//XTRB->(MsUnlock())
//oMark:Refresh(.f.)


//MsgInfo("Total selecionado: R$ " + Alltrim(cValtochar(nValMk)))
//SysRefresh()
RestArea( aAreaTRB )

//cRet:= cDescXPTO + "Total selecionado: R$ " + Alltrim(cValtochar(nValMk))
//'"'+cRet+'"'
Return 


User Function HPWFMAIL(lAprovado, cNum, cSC7User, aItens)

Local nx       := 0
Local cEmailCC := SuperGetMv("MV_XMAILWF",.F.,"")

CONOUT("LOGWF: Envia email de aprovacao: "+cNum)

oProcAprov := TWFProcess():New("000001","Pedido de Compras - Aprovacao")

If lAprovado 
	oProcAprov:NewTask("Pedido","\workflow\html\wfw120p2.html")
	oProcAprov:cSubject := "Pedido de Compra N� "+cNum+" aprovado."
	CONOUT("LOGWF: Aprovado. Pedido: "+cNum)
Else
	oProcAprov:NewTask("Pedido","\workflow\html\wfw120p3.html")
	oProcAprov:cSubject := "Pedido de Compra N� "+cNum+" reprovado."
	CONOUT("LOGWF: Reprovado. Pedido: "+cNum)
EndIf	

oProcAprov:cTo		:= UsrRetMail(cSC7User)
oProcAprov:cCC		:= cEmailCC 
oProcAprov:ohtml:valbyname("Num",cNum)
oProcAprov:ohtml:valbyname("Emissao",DtoC(Posicione("SC7",1,xFilial("SC7")+cNum,"C7_EMISSAO")))
oProcAprov:ohtml:valbyname("Req",UsrRetName(cSC7User))

For nx := 1 to Len(aItens)
	AAdd((oProcAprov:oHTML:ValByName("it.item")),aItens[nx,1])
	AAdd((oProcAprov:oHTML:ValByName("it.produto")),aItens[nx,2])
Next

//oProcAprov:ohtml:valbyname("Motivo",cObserv)

oProcAprov:start()	
oProcAprov:Free()

Return
