/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MVCAULA1  �Autor  �Fab. Soft ADVPL     � Data �  05/27/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa exemplo da utiliza��o das classes de MVC           ���
���          �Cadastro Basico                                             ���
�������������������������������������������������������������������������͹��
���Uso       � Aula de MVC                                                ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
#Include "Totvs.ch"
#Include "FwMvcDef.ch"
#INCLUDE "RWMAKE.CH"
#Include "ApWizard.ch"
#include "fileio.ch" 

User Function  PRJ003()
Private oBrowse 	:= FwMBrowse():New()				//Variavel de Browse

//Alias do Browse
oBrowse:SetAlias('ZAV')
//Descri��o da Parte Superior Esquerda do Browse
oBrowse:SetDescripton("Fundo de Marketing ")

//Legendas do Browse
oBrowse:AddLegend( "ZAV_STATUS=='1'", "GREEN" , "Pendente"       )
oBrowse:AddLegend( "ZAV_STATUS=='2'", "RED"   , "Titulo Gerado " )
oBrowse:AddLegend( "ZAV_STATUS=='3'", "YELLOW", "Boleto emitido" )
oBrowse:AddLegend( "ZAV_STATUS=='4'", "BLUE"  , "E-mail Enviado" )
oBrowse:AddLegend( "ZAV_STATUS=='5'", "CANCEL", "Cancelado"      )



//Habilita os Bot�es Ambiente e WalkThru
oBrowse:SetWalkThru(.T.)

//Desabilita os Detalhes da parte inferior do Browse
//oBrowse:DisableDetails()

// Habilita Filtro Padrao
//oBrowse:SetFilterDefault("ZAV_STATUS <> '5' ")


//Ativa o Browse
oBrowse:Activate()

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MVCAULA1  �Autor  �ADVPL BIALE - FAB. SOFTWARE� Data �  05/27/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao para Menu do Browse                                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Aula MVC                                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function MenuDef()
Local aMenu :=	{}

ADD OPTION aMenu TITLE 'Pesquisar'      ACTION 'PesqBrw'   	OPERATION 1 ACCESS 0
ADD Option aMenu Title 'Imp Arq CSV'    Action 'U_MVCFD001'	Operation 3 Access 0
ADD OPTION aMenu TITLE 'Gerar Titulo'   ACTION 'U_MVCFD002' OPERATION 4 ACCESS 0
ADD OPTION aMenu TITLE 'NT dbto/Boleto' ACTION 'U_MVCFD003' OPERATION 4 ACCESS 0
ADD OPTION aMenu TITLE 'Enviar E-mail'  ACTION 'U_MVCFD004' OPERATION 4 ACCESS 0
ADD OPTION aMenu TITLE 'Comp.Cliente'   ACTION 'U_COMPCLI1' OPERATION 4 ACCESS 0


Return(aMenu)


/*
�����������������������������������������������������������������������������������
�����������������������������������������������������������������������������������
�������������������������������������������������������������������������������ͻ��
���Programa  �PRJ001  �Autor  �ADVPL BIALE - FAB. SOFTWARE� Data �  05/27/12    ���
�������������������������������������������������������������������������������͹��
���Desc.     �Funcao de Modelo de Dados.                                        ���
���          �Onde � definido a estrutura de dados                              ���
���          �Regra	 de Negocio.                                                ���
�������������������������������������������������������������������������������͹��
���Uso       � Aula de MVC                                                      ���
�������������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������������
�����������������������������������������������������������������������������������
*/

Static Function ModelDef()
Local oStruct	:=	FWFormStruct(1,"ZAV") //Retorna a Estrutura do Alias passado como Parametro (1=Model,2=View)
Local oModel

//Instancia do Objeto de Modelo de Dados
oModel	:=	MpFormModel():New('MDPRJ003',/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)
oModel:SetPrimaryKey( { RECNO() } )
//Adiciona um modelo de Formulario de Cadastro Similar � Enchoice ou Msmget
oModel:AddFields('ID_FLD_PRJ003', /*cOwner*/, oStruct, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )

//Adiciona Descricao do Modelo de Dados
oModel:SetDescription( 'Modelo de Dados de Fundo de Marketing' )

//Adiciona Descricao do Componente do Modelo de Dados
oModel:GetModel( 'ID_FLD_PRJ003' ):SetDescription( 'Formulario de Dados para Fundo de Marketing' )


Return(oModel)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PRJ001  �Autor  �ADVPL BIALE - FAB. SOFTWARE� Data �  05/27/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao de Visualizacao.                                     ���
���          �Onde � definido a visualizacao da Regra de Negocio.         ���
�������������������������������������������������������������������������͹��
���Uso       � Aula de MVC                                                ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function ViewDef()
Local oStruct	:=	FWFormStruct(1,"ZAV") 	//Retorna a Estrutura do Alias passado
                                            // como Parametro (1=Model,2=View)
Local oModel	:=	FwLoadModel('PRJ003')	//Retorna o Objeto do Modelo de Dados
Local oView		:=	FwFormView():New()      //Instancia do Objeto de Visualiza��o

//Define o Modelo sobre qual a Visualizacao sera utilizada
oView:SetModel(oModel)

//Vincula o Objeto visual de Cadastro com o modelo
oView:AddField( 'ID_VIEW_PRJ003', oStruct, 'ID_FLD_PRJ003')

//Define o Preenchimento da Janela
oView:CreateHorizontalBox( 'ID_HBOX' , 100 )


// Relaciona o ID da View com o "box" para exibicao
oView:SetOwnerView( 'ID_VIEW_PRJ003', 'ID_HBOX' )
oModel: SetPrimaryKey( {  RECNO() } )
Return(oView)

//oModel: SetPrimaryKey( { "ZAV_FILIAL", RECNO() } )



USER FUNCTION COMPCLI1()
local cVldAlt := ".T." 
local cVldExc := ".T." 
local cAlias  := "ZB4"
chkFile(cAlias)
dbSelectArea(cAlias)
dbSetOrder(1)
axCadastro(cAlias, "Cadastro de ComplementoS de Clientes", cVldExc, cVldAlt)

Return


USER FUNCTION ACOMPCLI()
dbselectarea("ZAV")
ZAV->(Dbgotop("ZAV"))
while ZAV->(!eof())
    dbselectarea("ZB4") 
    ZB4->(DBSETORDER(1))                //ZB4_FILIAL, ZB4_CODIGO, ZB4_LOJA, R_E_C_N_O_, D_E_L_E_T_
    ZB4->(DbSeek(xfilial("ZB4")+ZAV->ZAV_CODIGO+ZAV->ZAV_LOJA))
    IF ZB4->(!EOF())
       ZB4->(RecLock("ZB4",.F.))
       ZB4->ZB4_EMAILF     := ZAV->ZAV_EMAIL
       ZB4->(MsUnLock())
    ELSE
       ZB4->(RecLock("ZB4",.T.))
       ZB4->ZB4_FILIAL := xFilial("ZB4") 
       ZB4->ZB4_CODIGO := ZAV->ZAV_CODIGO
       ZB4->ZB4_LOJA   := ZAV->ZAV_LOJA
       ZB4->ZB4_NOME   := ZAV->ZAV_NOMECLI
       ZB4->ZB4_EMAILF := ZAV->ZAV_EMAIL
       //ZB4->ZB4_UDTAEV := ZAV->ZAV_
       //ZB4->ZB4_MESBOL := ZAV->ZAV_
       //ZB4->ZB4_DIRXML := ZAV->ZAV_
       //ZB4->ZB4_TPCLI  := ZAV->ZAV_
       ZB4->(MsUnLock())
    Endif 
    dbselectarea("ZAV")
    ZAV->(DBSKIP())
 ENDDO
 Return()
    