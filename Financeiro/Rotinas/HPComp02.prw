#Include 'Protheus.ch'
#Include 'FwMVCDef.ch'
#INCLUDE "TBICONN.CH"
#include "TOTVS.CH"
#INCLUDE "topconn.ch"
#include "rwmake.ch"
#INCLUDE "Ap5Mail.ch"
#Define DS_MODALFRAME   128

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA001  �Autor  �Bruno Parreira      � Data �  08/11/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa para Compensa��o de titulos referenta ao B2C      ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HPComp02(nReg)
	Local _astru   := {}
	Local _afields := {}
	Local _carq    := ""
	Local cQuery   := ""
	Local xFornec := ""
	Local xLoja   := ""
	Local xVecto  := ctod(space(8))
	Local nValor  := 0
	Local xItens  := ""
	Local xRazao  := ""
	Local cFiltraSE1
	Local ca097User 	:= RetCodUsr()
	Local aSize   := MsAdvSize()
	Local oFont1  := TFont():New("Verdana",,012,,.T.,,,,,.F.,.F.)
	Local oFont2  := TFont():New("Verdana",,012,,.F.,,,,,.F.,.F.)
	Local aColunas := {	{"E1_OK"     		,,""       		},;
		{"E1_NUM"   	,,"Titulo"    ,"@!" 	},;
		{"E1_PARCELA"   ,,"Parcela"   ,"@!" 	},;
		{"E1_PREFIXO"   ,,"Prefixo"   ,"@!" 	},;
		{"E1_CLIENTE"  	,,"Cliente"	  ,"@!"		},;
		{"E1_LOJA" 		,,"Loja"	  ,"@!"		},;
		{"E1_NOMCLI" 	,,"Nome"	  ,"@!"		},;
		{"E1_EMISSAO" 	,,"Emiss�o"	  ,"@D"		},;
		{"E1_VENCREA" 	,,"Vencimento","@D"		},;
		{"E1_VALOR" 	,,"Valor"	  ,"@E 999,999.99"  },;
		{"E1_VLTXCC"    ,,"Taxa"	  ,"@E 999,999.99"  },;
		{"E1_SALDO"     ,,"Saldo"	  ,"@E 999,999.99"  },;
		{"E1_XFORREC"   ,,"TpVenda"	  ,"@!"	    },;
		{"E1_XTPPAG"    ,,"Tipo Pagto","@!"		},;
		{"E1_TIPO"      ,,"Tipo T�t. ","@!"		}}
	Private cliSE1    := ""
	Private lojSE1    := ""
	Private preSE1    := ""
	Private numSE1    := ""
	Private parSE1    := ""
	Private tipSE1    := ""
	Private cli_RA    := ""
	Private loj_RA    := ""
	Private pre_RA    := ""
	Private num_RA    := ""
	Private par_RA    := ""
	Private tip_RA    := ""
	Private lMarcar   := .F.
	Private oMark
	Private cPerg     := "HPCOMP0002"
	Private aRotina   := {}
	Private lContinua := .F.
	Private atuar     := ""
	Private cMark     := GetMark()
	Private lInverte  := .F.
	Private oChk
	Private lChkSel   := .F.
	Private lRefresh  := .T.
	Private oDlgT
	Private onVlrCom
	Private onVlrSld
	Private onVlrMar
	Private onVlrfIL
	Private onVlrSob
	Private nVlrCom   := 0
	Private nVlrSld   := 0
	Private nVlrMar   := 0
	Private nVlrfIL   := 0
	Private nVlrSob   := 0
	Private aRecSE1   := {}
	Private nRecnoRA  := RECNO()
	If SE1->E1_TIPO <> 'RA '
		MsgStop("T�tulo posicionado n�o � um (RA) Recebimento Antecipado")
		Return()
	Else
		cli_RA   := SE1->E1_CLIENTE
		loj_RA   := SE1->E1_LOJA
		pre_RA   := SE1->E1_PREFIXO
		num_RA   := SE1->E1_NUM
		par_RA   := SE1->E1_PARCELA
		tip_RA   := SE1->E1_TIPO
		nVlrCom  := SE1->E1_SALDO
		nVlrSld  := SE1->E1_SALDO
		nVlrSob  := SE1->E1_SALDO
		cMensa   := "Confirma a compensa��o do T�tulo No. "+SE1->E1_NUM+ "  "+ CHR(13)+CHR(10)
		cMensa   += "                          Valor de R$ "+ Transform(SE1->E1_VALOR, '@E 999,999,999.99'  )+""+CHR(13)+CHR(10)
		cMensa   += "                          Saldo de R$ "+ Transform(nVlrSld, '@E 999,999,999.99'  )+""+CHR(13)+CHR(10)
		If MsgYesNo(cMensa,"Confirma��o")

			If Select("XTRB") > 0
				XTRB->(DbCloseArea())
			EndIf

			//Estrutura da tabela temporaria
			AADD(_astru,{"E1_OK"     ,"C",  2,0})
			AADD(_astru,{"E1_PREFIXO","C",  3,0})
			AADD(_astru,{"E1_NUM"    ,"C",  9,0})
			AADD(_astru,{"E1_PARCELA","C",  3,0})
			AADD(_astru,{"E1_CLIENTE","C",  6,0})
			AADD(_astru,{"E1_LOJA"   ,"C",  4,0})
			AADD(_astru,{"E1_NOMCLI" ,"C", 15,0})
			AADD(_astru,{"E1_EMISSAO","D",  8,0})
			AADD(_astru,{"E1_VENCREA","D",  8,0})
			AADD(_astru,{"E1_VALOR"  ,"N", 16,2})
			AADD(_astru,{"E1_VLTXCC" ,"N", 16,2})
			AADD(_astru,{"E1_SALDO"  ,"N", 16,2})
			AADD(_astru,{"E1_XFORREC","C", 15,0})
			AADD(_astru,{"E1_XTPPAG" ,"C", 15,0})
			AADD(_astru,{"E1_RECNO"  ,"N", 15,0})
			AADD(_astru,{"E1_TIPO"   ,"C",  3,0})
			cArqTrab  := CriaTrab(_astru)
			dbUseArea( .T.,, cArqTrab, "XTRB", .F., .F. )

			//Atribui a tabela temporaria ao alias TRB

			//-------------------------------------------------------------------
			// Verifica se o usuario possui direito de liberacao.
			//-------------------------------------------------------------------
			AjustaSX1(cPerg)
			Pergunte(cPerg, .T.)

			cQuery := "SELECT '' AS E1_OK,E1_NUM,E1_CLIENTE,E1_LOJA,E1_NOMCLI,E1_EMISSAO,E1_VENCREA,E1_VALOR,E1_VLTXCC,E1_SALDO,E1_XFORREC,E1_XTPPAG,"
			cQuery += " E1_PREFIXO, E1_PARCELA, E1_TIPO,  R_E_C_N_O_ AS E1_RECNO, E1_DECRESC, E1_VALJUR  "
			cQuery += "FROM "+RetSqlName("SE1")+" E1 WITH (NOLOCK) WHERE SUBSTRING(E1_XFORREC,1,20) = '"+MV_PAR01+"' AND E1_EMISSAO BETWEEN '"+DtOS(MV_PAR02)+"' AND '"+DtOS(MV_PAR03)+"' "
			//	cQuery += "AND E1.D_E_L_E_T_= ''  AND  E1_SALDO <> 0  ORDER BY E1_NUM "
			cQuery += "AND E1.D_E_L_E_T_= ''  AND  E1_SALDO <> 0  ORDER BY E1_EMISSAO,E1_NUM "  //Marcio
			If Select("YTRB") > 0
				YTRB->(DbCloseArea())
			EndIf
			//cQuery := ChangeQuery(cQuery)
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"YTRB",.T.,.T.)


			DbSelectArea("YTRB")
			YTRB->(DbGotop())
			While YTRB->(!EOF())
				DbSelectArea("XTRB")
				XTRB->(RecLock("XTRB",.T.))
				XTRB->E1_NUM     := YTRB->E1_NUM
				XTRB->E1_CLIENTE := YTRB->E1_CLIENTE
				XTRB->E1_LOJA    := YTRB->E1_LOJA
				XTRB->E1_NOMCLI  := YTRB->E1_NOMCLI
				XTRB->E1_EMISSAO := CTOD(SUBSTR(YTRB->E1_EMISSAO,7,2)+'/'+SUBSTR(YTRB->E1_EMISSAO,5,2)+'/'+SUBSTR(YTRB->E1_EMISSAO,3,2))
				XTRB->E1_VENCREA := CTOD(SUBSTR(YTRB->E1_VENCREA,7,2)+'/'+SUBSTR(YTRB->E1_VENCREA,5,2)+'/'+SUBSTR(YTRB->E1_VENCREA,3,2))
				XTRB->E1_VALOR   := YTRB->E1_VALOR
				XTRB->E1_VLTXCC  := YTRB->E1_VLTXCC
				XTRB->E1_SALDO   := YTRB->E1_SALDO - YTRB->E1_DECRESC
				XTRB->E1_XFORREC := YTRB->E1_XFORREC
				XTRB->E1_XTPPAG  := YTRB->E1_XTPPAG
				XTRB->E1_PREFIXO := YTRB->E1_PREFIXO
				XTRB->E1_PARCELA := YTRB->E1_PARCELA
				XTRB->E1_TIPO    := YTRB->E1_TIPO
				XTRB->E1_RECNO   := YTRB->E1_RECNO
				nVlrfIL := ROUND(nVlrfIL+XTRB->E1_SALDO,2)
				XTRB->(MsUnlock())
				YTRB->(DbSkip())
			Enddo
			DbSelectArea("XTRB")
			XTRB->(DbGotop())

			DEFINE MSDIALOG oDlgT TITLE "HOPE - Compensa��o SHOPLINE" FROM aSize[7],100 To aSize[6],aSize[5] COLORS 0, 16777215 PIXEL Style DS_MODALFRAME
			//		DEFINE MSDIALOG oDlgT TITLE "HOPE - Compensa��o SHOPLINE" FROM aSize[7],0 To aSize[6],aSize[5] COLORS 0, 16777215 PIXEL Style DS_MODALFRAME
			cMarca    := GetMark()
			@ 005, 005 SAY oSay2 PROMPT "Lista de t�tulos a serem compensados ." SIZE 242, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
			oMark := MsSelect():New("XTRB","E1_OK",,aColunas,lInverte,cMarca,{ 015, 003, 242, 650})
			oMark:bAval:= {||(CSINT04E(cMarca),oMark:oBrowse:Refresh())}
			oMark:oBrowse:Refresh(.F.)
			oMark:oBrowse:lHasMark    := .T.
			oMark:oBrowse:lCanAllMark := .T.
			oMark:oBrowse:bAllMark := {|| U_CSINT04F(cMarca),oMark:oBrowse:Refresh()}
			@ 250, 005 BUTTON oButton1 PROMPT "Compensar" 	SIZE 032, 013 OF oDlgT ACTION U_CSINT04G(cMarca,1) PIXEL
			@ 250, 040 BUTTON oButton1 PROMPT "Fechar" 	  	SIZE 035, 013 OF oDlgT ACTION U_CSINT04G(cMarca,5) PIXEL
			@ 250, 200 Say oSay3 prompt " Valor Total da RA a Ser Compensado:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
			@ 260, 200 Say oSay5 prompt " Saldo Total da RA a Ser Compensado:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
			@ 270, 200 Say oSay7 prompt " Total Marcado para compensar......:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
			@ 280, 200 Say oSay7 prompt " Saldo a Compensar ................:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
			@ 250,315 MSGET onVlrCom VAR nVlrCom When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"
			@ 260,315 MSGET onVlrSld VAR nVlrSld When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"
			@ 270,315 MSGET onVlrMar VAR nVlrMar When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"
			@ 280,315 MSGET onVlrSob VAR nVlrSob When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"

			ACTIVATE MSDIALOG oDlgT CENTERED

			if !Empty('XTRB')
				XTRB->(DbCloseArea())
			Endif
		Endif
	Endif
Return Nil


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � CSINT04E � Autor � Edgar Benuto Frastrone� Data �21/07/2016���
�������������������������������������������������������������������������Ĵ��
���Locacao   � Comsystem�Contato� edgar.frastrone@comsystem.com.br        ���
�������������������������������������������������������������������������Ĵ��
���Descricao �Rotina que marca e desmarca Browse						  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Aplicacao �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Alianca Francesa		                                      ���
�������������������������������������������������������������������������Ĵ��
���Analista Resp.�  Data  � Bops � Manutencao Efetuada                    ���
�������������������������������������������������������������������������Ĵ��
���Edgar B.F.    �21/07/16�      �Desenvolvimento da Rotina.              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                               
Static Function CSINT04E(cMarca)

	If Alltrim(XTRB->E1_OK) == ""
		If nVlrSob == 0
			MsgStop("Valor a Compensar j� Utilizado,Verifique","Aten��o" )
		Else
			nVlrSob := 0
			XTRB->(RecLock( "XTRB", .F. ))
			XTRB->E1_OK   := cMarca
			XTRB->(msUnlock())
			nVlrMar :=	nVlrMar+XTRB->E1_SALDO
			nVlrSob :=  nVlrSld-nVlrMar
			//msgStop("Diminuindo saldo Pois foi Marcado Aqui")
		Endif
	Else
		nVlrSob := 0
		XTRB->(RecLock( "XTRB", .F. ))
		XTRB->E1_OK   := Space(2)
		XTRB->(msUnlock())
		nVlrMar :=	nVlrMar-XTRB->E1_SALDO
		nVlrSob :=  nVlrSld-nVlrMar
		//msgStop("Somando saldo Pois foi Marcado Aqui")
	EndIf
	onVlrMar:Refresh()
	OnVlrSob:Refresh()
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � CSINT04F � Autor � Edgar Benuto Frastrone� Data �21/07/2016���
�������������������������������������������������������������������������Ĵ��
���Locacao   � Comsystem�Contato� edgar.frastrone@comsystem.com.br        ���
�������������������������������������������������������������������������Ĵ��
���Descricao �Rotina que marca e desmarca Browse total.					  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Aplicacao �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Alianca Francesa		                                      ���
�������������������������������������������������������������������������Ĵ��
���Analista Resp.�  Data  � Bops � Manutencao Efetuada                    ���
�������������������������������������������������������������������������Ĵ��
���Edgar B.F.    �21/07/16�      �Desenvolvimento da Rotina.              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                               
User Function CSINT04F(cMarca)

	dbSelectArea("XTRB")
	XTRB->(dbGoTop())
	While XTRB->(!Eof())
		If Alltrim(XTRB->E1_OK) == ""
			If nVlrSob == 0
				MsgStop("Valor a Compensar j� Utilizado,Verifique","Aten��o" )
			Else
				RecLock( "TIT", .F. )
				XTRB->E1_OK   := cMarca
				XTRB->(msUnlock())
				nVlrSob := 0
				nVlrMar :=	nVlrMar+XTRB->E1_VALOR
				nVlrSob :=  nVlrSld-nVlrMar
				//msgStop("Diminuindo saldo Pois foi Marcado")
			Endif
		Else
			XTRB->(RecLock( "XTRB", .F. ))
			XTRB->E1_OK   := Space(2)
			XTRB->(msUnlock())
			nVlrSob := 0
			nVlrMar :=	nVlrMar-XTRB->E1_VALOR
			nVlrSob :=  nVlrSld-nVlrMar
			//msgStop("Somonado o saldo Pois foi desMarcado")
		EndIf

		XTRB->(DBSKIP())
	End
	onVlrMar:Refresh()
	OnVlrSob:Refresh()
	XTRB->(dbGoTop())
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return(Nil)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � CSINT04F � Autor � Edgar Benuto Frastrone� Data �21/07/2016���
�������������������������������������������������������������������������Ĵ��
���Locacao   � Comsystem�Contato� edgar.frastrone@comsystem.com.br        ���
�������������������������������������������������������������������������Ĵ��
���Descricao �Rotina que marca e desmarca Browse total.					  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Aplicacao �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Alianca Francesa		                                      ���
�������������������������������������������������������������������������Ĵ��
���Analista Resp.�  Data  � Bops � Manutencao Efetuada                    ���
�������������������������������������������������������������������������Ĵ��
���Edgar B.F.    �21/07/16�      �Desenvolvimento da Rotina.              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                               
User Function CSINT04G(cMarca,nOpc)

	If(nOpc == 5)
		oDlgT:End()
	ElseIf(nOpc == 1)
		Processa( {|| CSINT04H(cMarca) }, "Aten��o", "Iniciando a Compensa��o", .F.)
		oDlgT:End()
	EndIf

Return(Nil)
Static Function CSINT04H(cMarca)
	Local cQry_ := ""
	Local Reg_  := ""
	Private nSaldoComp:= 0
	Private nSldCompen:= nVlrMar
	dbSelectArea("XTRB")
	XTRB->(dbGoTop())
	While XTRB->(!Eof())
		If Alltrim(XTRB->E1_OK) <> ""
			Reg_ := STRZERO(XTRB->E1_RECNO,10)
			cQry_ := "UPDATE SE1010 SET E1_VALJUR = 0 WHERE R_E_C_N_O_ = "+Reg_+ " "
			TcSqlExec(cQry_)//TcSqlExec(cQuery) //Marcio
			Aadd(aRecSE1,{STRZERO(XTRB->E1_RECNO,10)} )
			nSaldoComp:= nSaldoComp+XTRB->E1_SALDO
			cliSE1 := XTRB->E1_CLIENTE
			lojSE1 := XTRB->E1_LOJA
			preSE1 := XTRB->E1_PREFIXO
			numSE1 := XTRB->E1_NUM
			parSE1 := XTRB->E1_PARCELA
			tipSE1 := XTRB->E1_TIPO
			lRet   := U_COMPCR(nSaldoComp)
			if lRet
				nSldCompen := nSaldoComp
			Endif
		Endif
		XTRB->(DBSKIP())
	End
	onVlrMar:Refresh()
	OnVlrSob:Refresh()
	XTRB->(dbGoTop())
	lRet := U_COMPCR(nSaldoComp)
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return(Nil)


User FUNCTION COMPCR()
	Local lRetOK := .T.
	Local aArea := GetArea()
	Local nTaxaCM := 0
	Local aTxMoeda := {}
	Local cHPNATECOM := SuperGetMV("HP_NATECOM",.T.,"111160001")  //Marcio
	Private nRecnoNDF
	Private nRecnoE1
	dbSelectArea("SE1")
	dbSetOrder(2) // E1_FILIAL, E1_CLIENTE, E1_LOJA, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, R_E_C_N_O_, D_E_L_E_T_
	IF dbSeek(XFILIAL("SE1")+cli_RA+loj_RA+pre_RA+num_RA+par_RA+tip_RA)
		nRecnoRA := RECNO()
		IF dbSeek(XFILIAL("SE1")+cliSE1+lojSE1+preSE1+numSE1+parSE1+tipSE1)
			nRecnoE1 := RECNO()
			PERGUNTE("AFI340",.F.)
			lContabiliza := MV_PAR11 == 1
			lAglutina := MV_PAR08 == 1
			lDigita := MV_PAR09 == 1
			nTaxaCM := RecMoeda(dDataBase,SE1->E1_MOEDA)
			aAdd(aTxMoeda, {1, 1} )
			aAdd(aTxMoeda, {2, nTaxaCM} )
			SE1->(dbSetOrder(1)) //E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_FORNECE+E1_LOJA
			aRecRA := { nRecnoRA }
			aRecSE1 := { nRecnoE1 }
			/*-- Marcio --*/
			If AllTrim(SE1->E1_NATUREZ) $ AllTrim(cHPNATECOM)
				dDtBxComp := SE1->E1_VENCREA
			Else
				dDtBxComp := dDataBase
			Endif
			Conout('LOG HPCOMP02 - MARCIO ->')
			Conout('Dados SE1: ' +SE1->E1_PREFIXO + "/" + SE1->E1_NUM + "/" + SE1->E1_PARCELA + " - data venc. " + DTOC(SE1->E1_VENCREA))
			Conout('Database: ' + DTOC(dDataBase))
			Conout('dDtBxComp: ' + DTOC(dDtBxComp))
			Conout('Nat.Parametro: ' + cHPNATECOM)
			Conout('Nat.Titolo: ' + AllTrim(SE1->E1_NATUREZ))
			/*----*/
			//	If !MaIntBxCR(3, aRecSE1, ,aRecRA, ,{lContabiliza,lAglutina,lDigita,.F.,.F.,.F.}, , , ,nSaldoComp, dDatabase, , , , , ,.t. ,  )
			//	If !MaIntBxCR(3, aRecSE1, ,aRecRA, ,{lContabiliza,lAglutina,lDigita,.F.,.F.,.F.}, , , ,nSaldoComp, dDtBxComp, , , , , ,.t. ,  ) //Marcio
			If !MaIntBxCR(3, aRecSE1, ,aRecRA, ,{lContabiliza,lAglutina,lDigita,.F.,.F.,.F.}, , , , ,nSaldoComp, , , , , ,.t.)
				Help("XAFCMPAD",1,"HELP","XAFCMPAD","N�o foi poss�vel a compensa��o"+CRLF+" do titulo do adiantamento",1,0)
				lRet := .F.
			ENDIF
		ENDIF
	ENDIF
	RestArea(aArea)
Return lRetOK



/*
//___________________________________________________________________________________________________________________________________________________________
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01NOTA�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria array contendo os itens das NFs selecionados.          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function MVCAPRNOTA(oMark)
	Local aRet := {}
	Local aAreaTRB := GetArea()

	XTRB->(DbGoTop())
	While XTRB->(!Eof())
		If !Empty(XTRB->E1_OK) //Se diferente de vazio, foi marcado
			aAdd(aRet,{XTRB->D1_COD,XTRB->D1_QUANT,XTRB->D1_VUNIT,TRB->D1_DOC,XTRB->D1_SERIE,TRB->D1_ITEM})
		Endif
		XTRB->( dbSkip() )
	EndDo

	RestArea(aAreaTRB)

Return aRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01SLCN�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria array contendo os canais selecionados.                 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

Static Function MVCAPRSLCN(oMark)
	Local aRet := {}
	Local aAreaTRB := GetArea()

	XTRB->(DbGoTop())
	While XTRB->(!Eof())
		If !Empty(XTRB->E1_OK) //Se diferente de vazio, foi marcado
			aAdd(aRet,{XTRB->ZA3_CODIGO,XTRB->ZA3_DESCRICAO})
		Endif
		XTRB->( dbSkip() )
	EndDo

	RestArea(aAreaTRB)

Return aRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01SLCL�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria array contendo os clientes selecionados.               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function MVCAPRSLCL(oMark)
	Local aRet := {}
	Local aAreaTRB := GetArea()

	XTRB->(DbGoTop())
	While XTRB->(!Eof())
		If !Empty(XTRB->E1_OK) //Se diferente de vazio, foi marcado
			aAdd(aRet,{XTRB->A1_COD,TRB->A1_LOJA})
		Endif
		XTRB->( dbSkip() )
	EndDo

	RestArea(aAreaTRB)

Return aRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01INVE�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Marca ou desmarca todos os itens da FWMarkBrowse.           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HPMARDES(cMarca)
	Local aAreaTRB  := XTRB->(GetArea())

	lMarcar := !lMarcar

	dbSelectArea("XTRB")
	XTRB->(dbGoTop())
	While !XTRB->(Eof())
		RecLock("XTRB",.F.)
		XTRB->E1_OK := IIf(lMarcar,cMarca,'  ')
		MsUnlock()
		XTRB->(dbSkip())
	EndDo

	RestArea( aAreaTRB )

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01PROX�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Funcao para controlar a transicao de uma tela para outra.   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

User Function HPCOMPO2A(oMark,nTela)
	Local nCont := 0
	Local aAreaTRB := GetArea()

	XTRB->(DbGoTop())
	While XTRB->(!Eof())
		If !Empty(XTRB->E1_OK) //Se diferente de vazio, foi marcado
			nCont++
		Endif
		XTRB->( dbSkip() )
	EndDo

	If nCont == 0
		Alert("Selecione pelo menos um registro.")
		RestArea(aAreaTRB)
		lContinua := .F.
		Return
	Endif

	If nTela = 1
		If !MsgYesNo("Confirma a Compensa��o dos T�tulos selecionados?","Confirma��o")
			RestArea(aAreaTRB)
			lContinua := .F.
			Return
		Else
			atuar := 'APR'
		EndIf
	EndIf
	If nTela = 2
		//If !MsgYesNo("Confirma a Reprova��o dos pedidos de Compras selecionados?","Confirma��o")
		RestArea(aAreaTRB)
		lContinua := .F.
	Endif

	CloseBrowse()
	oMark:DeActivate()

	lContinua := .T.

	RestArea(aAreaTRB)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01CANA�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta a tela para selecao dos canais de venda.              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFAT01CANA(oMark)
	Local _astru   := {}
	Local _afields := {}
	Local _carq    := ""
	Local cQuery   := ""

	lContinua := .F.

	lMarcar := .F.

	cQuery := "select ZA3_CODIGO,ZA3_DESCRI "
	cQuery += CRLF + "from "+RetSqlName("ZA3")+" ZA3 "
	cQuery += CRLF + "where ZA3.D_E_L_E_T_ = '' "
	cQuery += CRLF + "and ZA3_FILIAL = '"+xFilial("ZA3")+"' "
	cQuery += CRLF + "order by ZA3_CODIGO "

	MemoWrite("HFATA001_2.txt",cQuery)

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)

	//Estrutura da tabela temporaria
	AADD(_astru,{"E1_OK"     ,"C", 2,0})
	AADD(_astru,{"ZA3_CODIGO","C", 3,0})
	AADD(_astru,{"ZA3_DESCRI","C",30,0})

	cArqTrab  := CriaTrab(_astru)
	dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )

	Dbselectarea("TMP")
	TMP->(DbGoTop())
	If TMP->(!EOF())
		While TMP->(!EOF())
			DbSelectArea("TRB")
			RecLock("TRB",.T.)
			TRB->ZA3_CODIGO := TMP->ZA3_CODIGO
			TRB->ZA3_DESCRI := TMP->ZA3_DESCRI
			MsUnlock()
			TMP->(DbSkip())
		EndDo
	EndIf

	//Criando o MarkBrow
	oMark := FWMarkBrowse():New()
	oMark:SetAlias('TRB')

	//define as colunas para o browse
	aColunas := {;
		{"Codigo"   ,"ZA3_CODIGO","C",3 ,0,"@!"},;
		{"Descricao","ZA3_DESCRI","C",30,0,"@!"}}

	//seta as colunas para o browse
	oMark:SetFields(aColunas)

	//Setando sem�foro, descri��o e campo de mark
	oMark:SetSemaphore(.T.)
	oMark:SetDescription('Seleciona Canais')
	oMark:SetFieldMark('E1_OK')

	oMark:AddButton("Selecionar Clientes","U_HFAT01PROX(oMark,2)",,2,0)
	oMark:AddButton("Marca/Descmarca Todos","U_HFAT01INVE(oMark:Mark())",,2,0)

	//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
	//oMark:SetAllMark({ || HFAT01INVE(oMark:Mark(),lMarcar := !lMarcar ),oMark:Refresh(.T.)})

	oMark:SetTemporary()

	//Ativando a janela
	oMark:Activate()

Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01CLIE�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta a tela para selecao dos clientes.                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFAT01CLIE(oMark,aCanais)
	Local _astru   := {}
	Local _afields := {}
	Local _carq    := ""
	Local cQuery   := ""
	Local cCanais  := ""
	Local nx := 0

	lContinua := .F.

	lMarcar := .F.

	For nx := 1 to Len(aCanais)
		cCanais += "'"+aCanais[nx][1]+"',"
	Next

	cCanais := SubStr(cCanais,1,Len(cCanais)-1)

	cQuery := "select A1_COD,A1_LOJA,A1_CGC,A1_NOME,A1_NREDUZ "
	cQuery += CRLF + "from "+RetSqlName("SA1")+" SA1 "
	cQuery += CRLF + "where SA1.D_E_L_E_T_ = '' "
	cQuery += CRLF + "and A1_FILIAL = '"+xFilial("SA1")+"' "
	cQuery += CRLF + "and A1_XCANAL IN ("+cCanais+") "
	cQuery += CRLF + "and A1_MSBLQL <> '1' "
	cQuery += CRLF + "order by A1_COD,A1_LOJA "

	MemoWrite("HFATA001_3.txt",cQuery)

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)

	//Estrutura da tabela temporaria
	AADD(_astru,{"E1_OK"     ,"C", 2,0})
	AADD(_astru,{"A1_COD"    ,"C", 6,0})
	AADD(_astru,{"A1_LOJA"   ,"C", 4,0})
	AADD(_astru,{"A1_CGC"    ,"C",18,0})
	AADD(_astru,{"A1_NOME"   ,"C",40,0})
	AADD(_astru,{"A1_NREDUZ" ,"C",40,0})

	cArqTrab  := CriaTrab(_astru)
	dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )

	Dbselectarea("TMP")
	TMP->(DbGoTop())
	If TMP->(!EOF())
		While TMP->(!EOF())
			DbSelectArea("TRB")
			RecLock("TRB",.T.)
			TRB->A1_COD    := TMP->A1_COD
			TRB->A1_LOJA   := TMP->A1_LOJA
			TRB->A1_CGC    := TMP->A1_CGC
			TRB->A1_NOME   := TMP->A1_NOME
			TRB->A1_NREDUZ := TMP->A1_NREDUZ
			MsUnlock()
			TMP->(DbSkip())
		EndDo
	EndIf

	//Criando o MarkBrow
	oMark := FWMarkBrowse():New()
	oMark:SetAlias('TRB')

	//define as colunas para o browse
	aColunas := {;
		{"Cliente"      ,"A1_COD"   ,"C",6 ,0,"@!"},;
		{"Loja"         ,"A1_LOJA"  ,"C",4 ,0,"@!"},;
		{"CNPJ"         ,"A1_CGC"   ,"C",18,0,"@!"},;
		{"Razao Social" ,"A1_NOME"  ,"C",40,0,"@!"},;
		{"Nome Fantasia","A1_NREDUZ","C",40,0,"@!"}}

	//seta as colunas para o browse
	oMark:SetFields(aColunas)

	//Setando sem�foro, descri��o e campo de mark
	oMark:SetSemaphore(.T.)
	oMark:SetDescription('Seleciona Clientes')
	oMark:SetFieldMark('E1_OK')

	oMark:AddButton("Aprova Pedidos","U_MVCAPRPROX(oMark,3)",,2,0)
	oMark:AddButton("Marca/Descmarca Todos","U_MVCAPRINVE(oMark:Mark())",,2,0)

	//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
	//oMark:SetAllMark({ || HFAT01INVE(oMark:Mark(),lMarcar := !lMarcar ),oMark:Refresh(.T.)})

	oMark:SetTemporary()

	//Ativando a janela
	oMark:Activate()

Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01CLIE�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Gera PVs para os clientes selecionados.                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function MVCAPRPEDI(oMark,aItensNF,aClientes)
	Local nCont     := 0
	Local nx,ny     := 0
	Local cPrd      := ""
	Local aAux      := {}
	Local nSoma     := 0
	Local nQuant    := 0
	Local nVunit    := 0
	Local cNroPed   := ""
	Local cNumPVde  := ""
	Local cNumPVate := ""
	Local cNatureza := ""
	Local cTES      := ""
	Local cUM       := ""
	Local aCabPV    := {}
	Local aItemPV   := {}
	Local aErro     := {}
	Local aGerados  := {}
	Local nErro     := 0
	Local nGerados  := 0
	Local i
	PRIVATE lMsErroAuto := .F.

	//Agrupa os itens selecionados somando as qtdes de produtos comuns e ja divide pela qtde de clientes selecionados.

	aSort(aItensNF)
	cPrd  := aItensNF[1][1]    //Cod. Produto

	nCont := Len(aClientes)

	For nx := 1 to Len(aItensNF)
		nQuant := aItensNF[nx][2] //Qtde. do Item
		If cPrd <> aItensNF[nx][1]
			aAdd(aAux,{cPrd,nSoma/nCont,nVunit})
			nSoma := nQuant
		Else
			nSoma += nQuant
		EndIf
		cPrd   := aItensNF[nx][1]
		nVunit := aItensNF[nx][3]	 //Valor Unitario do Item
	Next
	aAdd(aAux,{cPrd,nSoma/nCont,nVunit})

	ProcRegua(Len(aClientes))

	If !Empty(mv_par14)
		cNatureza := mv_par14
	Else
		cNatureza := "21314305" //Natureza de Material Promocional Mkt
	EndIf

	cNumPVde := GetMV("MV_XNMPVMK")

	For nx := 1 to Len(aClientes)
		IncProc()

		DbSelectArea("SC5")

		cNroPed := GetMV("MV_XNMPVMK")

		aCabec := {}
		aadd(aCabec,{"C5_NUM"   ,cNroPed,Nil})
		aadd(aCabec,{"C5_TIPO"   ,"N",Nil})
		aadd(aCabec,{"C5_CLIENTE",aClientes[nx][1],Nil})
		aadd(aCabec,{"C5_LOJACLI",aClientes[nx][2],Nil})
		aadd(aCabec,{"C5_LOJAENT",aClientes[nx][2],Nil})
		aadd(aCabec,{"C5_POLCOM" ,MV_PAR15,Nil})
		aadd(aCabec,{"C5_TPPED"  ,MV_PAR17,Nil})
		aadd(aCabec,{"C5_CONDPAG",MV_PAR16,Nil})
		aadd(aCabec,{"C5_NATUREZ",cNatureza,Nil})

		aItens := {}

		For ny := 1 to Len(aAux)

			DbSelectArea("SB1")
			DbSeek(xFilial("SB1")+aAux[ny][1])

			DbSelectArea("SF4")
			If DbSeek(xFilial("SF4")+IF(!EMPTY(MV_PAR13),MV_PAR13,"501"))
				cTES := SF4->F4_CODIGO
			Else
				cTES := "501"
			EndIf

			aLinha := {}
			aadd(aLinha,{"C6_ITEM",StrZero(nY,2),Nil})
			aadd(aLinha,{"C6_PRODUTO",SB1->B1_COD,Nil})
			aadd(aLinha,{"C6_QTDVEN",aAux[ny][2],Nil})
			aadd(aLinha,{"C6_PRCVEN",aAux[ny][3],Nil})
			aadd(aLinha,{"C6_PRUNIT",aAux[ny][3],Nil})
			aadd(aLinha,{"C6_VALOR",ROUND(aAux[ny][2]*aAux[ny][3],2),Nil})
			aadd(aLinha,{"C6_TES",SF4->F4_CODIGO,Nil})
			aadd(aLinha,{"C6_NUM",cNroPed,Nil})
			aadd(aItens,aLinha)
		Next

		BEGIN TRANSACTION

			MATA410(aCabec,aItens,3)

			If lMsErroAuto
				MostraErro()
				nErro++
			Else
				nGerados++
				cNumPVate := cNroPed
				cNroPed := SOMA1(SubStr(cNroPed,3,4))
				PutMV("MV_XNMPVMK","MK"+cNroPed)
			EndIf

		END TRANSACTION

	Next

	If nErro > 0
		MsgInfo("Encontrado erro na gera��o de "+StrZero(nErro,4)+" pedidos.","Aviso")
	EndIf

	If nGerados > 0
		MsgInfo("Foram gerados "+StrZero(nGerados,4)+" pedidos com sucesso. N�: "+cNumPVde+" ao "+cNumPVAte,"Aviso")
	EndIf

	CloseBrowse()

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
/*/
	���������������������������������������������������������������������������������
	�����������������������������������������������������������������������������Ŀ��
	���Fun��o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 ���
	�����������������������������������������������������������������������������Ĵ��
	���Descri��o � Verifica/cria SX1 a partir de matriz parverificacao          ���
	�����������������������������������������������������������������������������Ĵ��
	���Uso       � Especifico para Clientes Microsiga                             ���
	������������������������������������������������������������������������������ٱ�
	���������������������������������������������������������������������������������
	����������������������������������������������������������������������������������
/*/
Static Function AjustaSX1(cPerg)

	Local _sAlias	:= Alias()
	Local aCposSX1	:= {}
	Local aPergs	:= {}
	Local nX 		:= 0
	Local lAltera	:= .F.
	Local nCondicao
	Local cKey		:= ""
	Local nJ		:= 0

	aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
		"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
		"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
		"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
		"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
		"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
		"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
		"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

	//	Aadd(aPergs,{"Ate Cliente","","","mv_ch8","C",6,0,0,"G","","MV_PAR08","","","","ZZZZZZ","","","","","","","","","","","","","","","","","","","","","SE1","","","",""})
	Aadd(aPergs,{"Tipo Pgto.?      ","","","mv_ch1","C",20,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data Emiss�o de? ","","","mv_ch2","D",08,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data Emiss�o ate?","","","mv_ch3","D",08,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	dbSelectArea("SX1")
	dbSetOrder(1)
	For nX:=1 to Len(aPergs)
		lAltera := .F.
		If MsSeek(cPerg+Right(aPergs[nX][11], 2))
			If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
					Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
				aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
				lAltera := .T.
			Endif
		Endif

		If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
			lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
		Endif

		If ! Found() .Or. lAltera
			RecLock("SX1",If(lAltera, .F., .T.))
			Replace X1_GRUPO with cPerg
			Replace X1_ORDEM with Right(aPergs[nX][11], 2)
			For nj:=1 to Len(aCposSX1)
				If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
						FieldPos(AllTrim(aCposSX1[nJ])) > 0
					Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
				Endif
			Next nj
			MsUnlock()
			cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."


			If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
				aHelpSpa := aPergs[nx][Len(aPergs[nx])]
			Else
				aHelpSpa := {}
			Endif

			If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
				aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
			Else
				aHelpEng := {}
			Endif

			If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
				aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
			Else
				aHelpPor := {}
			Endif

			PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
		Endif
	Next
Return()


