#Include 'Protheus.ch'

/// -> Retorna o Tipo de Pagamento Conforme o Modelo do Bordero Criado

User Function TipPag()
Local aArea := GetArea()
Local cTipCob := ''

If SEA->EA_MODELO == '01'	// Credito em Conta
	cTipCob := 'CC '
ElseIf SEA->EA_MODELO == '02'	// Cheque
	cTipCob := 'CH '
ElseIf SEA->EA_MODELO == '03'	// DOC
	cTipCob := 'DOC'
ElseIf SEA->EA_MODELO == '30'	// Boleto
	cTipCob := 'COB'
ElseIf SEA->EA_MODELO == '41'	// TED
	cTipCob := 'TED'
Endif

RestArea(aArea)
Return cTipCob
	

/// -> Caso Pagamento For TED,DOC ou CC Retorna Agencia do Fornecedor	

User Function BcoPag()
Local aArea := GetArea()
Local cBcoPag := '000'

If SEA->EA_MODELO == '30'	// Boleto
	cBcoPag := Left(SE2->E2_LINDIG,3)
Else
	cBcoPag := SA2->A2_BANCO
Endif

RestArea(aArea)
Return cBcoPag

/// -> Caso Pagamento For TED,DOC ou CC Retorna Agencia do Fornecedor	

User Function AgePag()
Local aArea := GetArea()
Local cAgePag := '0000000'

If SEA->EA_MODELO <> '30'	// Boleto
	cAgePag := StrZero(Val(AllTrim(SA2->A2_AGENCIA)+SA2->A2_DVAGE),7)
	cAgePag := Iif(Empty(cAgePag),'0000000',cAgePag)
Endif

RestArea(aArea)
Return cAgePag


/// -> Caso Pagamento For TED,DOC ou CC Retorna Conta do Fornecedor

User Function ConPag()
Local aArea := GetArea()
Local cConPag := '0000000000'

If SEA->EA_MODELO <> '30'	// Boleto
	cConPag := StrZero(Val(SA2->A2_NUMCON),9)+SA2->A2_DVCTA
Endif

RestArea(aArea)
Return cConPag

/// -> Em Caso de Boleto Pega Valor da Linha Digitavel e Monta CNAB

User Function CbarPag()
Local aArea := GetArea()
Local cBarPag := Space(44)

If SEA->EA_MODELO == '30'	// Boleto
	// 23791787066000003854103001401300369320000302178
//	   12345678901234567890123456789012345678901234567890
//         12345 1234567890 1234567890 
	            //1         2         3         4
	            
	
	cBarPag := ''
	cBarPag += Left(AllTrim(SE2->E2_LINDIG),3)	// Banco	   237
	cBarPag += SubStr(AllTrim(SE2->E2_LINDIG),4,1)	// Moeda   9
	cBarPag += SubStr(AllTrim(SE2->E2_LINDIG),33,1)	// Dac     3
	cBarPag += SubStr(AllTrim(SE2->E2_LINDIG),34,4)	// Fator de Vencimento 6932
	cBarPag += SubStr(AllTrim(SE2->E2_LINDIG),38,10)// Valor do Compromisso  0000302178
	cBarPag += SubStr(AllTrim(SE2->E2_LINDIG),5,5)// Campo Livre   17870       
	cBarPag += SubStr(AllTrim(SE2->E2_LINDIG),11,10)// Campo Livre  6000003854
	cBarPag += SubStr(AllTrim(SE2->E2_LINDIG),22,10)// Campo Livre  0300140130
Endif

RestArea(aArea)
Return cBarPag



/// -> Converte Linha Digitavel para Codigo de Barras

User Function LinCb(cLinDig)
Local aArea := GetArea()
Local cBarPag := Space(44)

cBarPag := ''
cBarPag += Left(AllTrim(cLinDig),3)	// Banco
cBarPag += SubStr(AllTrim(cLinDig),4,1)	// Moeda
cBarPag += SubStr(AllTrim(cLinDig),33,1)	// Dac
cBarPag += SubStr(AllTrim(cLinDig),34,14)	// Fator de Vencimento e Valor
cBarPag += SubStr(AllTrim(cLinDig),5,5)// Campo Livre
cBarPag += SubStr(AllTrim(cLinDig),11,10)// Campo Livre
cBarPag += SubStr(AllTrim(cLinDig),22,10)// Campo Livre

//Alert(cBarPag)

RestArea(aArea)
Return cBarPag
