#Include "RwMake.ch"
#Include "Protheus.ch"
#INCLUDE "TBICONN.CH"
#Include "topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HopCom01  �Autor  �Marcio MEdeiros Jr  � Data �  07/07/18   ���
�������������������������������������������������������������������������͹��
���Desc.     �											                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������  
COLOCAR HISTORICO NO E2 - OK
TRATAR DEBITO DE FRETE ABATENDO COMISSAO - OK
TRATAR DEBITO JUROS DE NEGOCIACAO
CRESITOS DOS DOIS ESTORNANDO
BUSCAR NATUREZA DO FORNECEDOR
*/
User Function HopCom01

Private aRotina		:= {}
Private cMarca 		:= GetMark()
Private lMsErroAuto := .F.
Private lHaErros	:= .F.
aRet	:= {}
aParam  := {}
aAdd(aParam, { 	1,;							//Tipo do param
"Vededor de:",;				//Descricao
Space(6),;					//Inicializador padr�o
"@!",;						//Picture
".T.",;						//Validacao
"SA3",;						//Consulta F3
".T.",;						//When
6,;							//Tamanho do get
.F. })						//Obrigatorio?

aAdd(aParam, { 	1,;							//Tipo do param
"Vededor at�:",;			//Descricao
"ZZZZZZ",;					//Inicializador padr�o
"@!",;						//Picture
".T.",;						//Validacao
"SA3",;						//Consulta F3
".T.",;						//When
6,;							//Tamanho do get
.F. })						//Obrigatorio?

aAdd(aParam, { 	1,;							//Tipo do param
"Data de:",;				//Descricao
dDatabase,;					//Inicializador padr�o
"99/99/9999",;				//Picture
".T.",;						//Validacao
"",;						//Consulta F3
".T.",;						//When
50,;						//Tamanho do get
.F. })						//Obrigatorio?

aAdd(aParam, { 	1,;							//Tipo do param
"Data at�:",;				//Descricao
dDatabase,;					//Inicializador padr�o
"99/99/9999",;				//Picture
".T.",;						//Validacao
"",;						//Consulta F3
".T.",;						//When
50,;						//Tamanho do get
.F. })						//Obrigatorio?

aRet    := Array(Len(aParam))
cTitulo := "Parametros de consulta"
If ParamBox( aParam,@cTitulo,@aRet,,,.T.)
	VarInfo('',aRet)
	Processa( {|| HopLeSE3(aRet) }, "Aguarde...", "Filtrando informa��es...",.F.)
Endif

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HopLeSE3   �Autor  �Marcio MEdeiros Jr  � Data �  07/07/18  ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava pedidos de venda					                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function HopLeSE3(aRet)

cQRY := "SELECT * FROM " + RetSQLName("SE3")
cQRY += " WHERE E3_FILIAL = '" + xFilial("SE3") + "'"
cQRY += "   AND E3_VEND BETWEEN '"+aRet[1]+"' AND '"+aRet[2]+"'"
cQRY += "   AND E3_VENCTO BETWEEN '"+DTOS(aRet[3])+"' AND '"+DTOS(aRet[4])+"'"
cQRY += "   AND E3_DATA = ''"
cQRY += "   AND E3_XORIGEM IN ('3','4','7','8')"
cQRY += "   AND D_E_L_E_T_ = ''"
cQRY += " ORDER BY E3_FILIAL,E3_VEND,E3_VENCTO,E3_NUM,E3_SERIE"

If Select("QRY") > 0
	QRY->(DbCloseArea())
Endif

TCQUERY cQRY NEW ALIAS "QRY"

aEval(SE3->(dbStruct()), {|x| If(x[2] <> "C", TcSetField("QRY",x[1],x[2],x[3],x[4]),Nil)})

If QRY->(!EoF())
	ProcInfo()
Else
	Alert("N�o h� dados")
Endif

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ProcInfo   �Autor  �Marcio MEdeiros Jr  � Data �  23/01/17  ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava pedidos de venda					                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ProcInfo(aTabela)

lInverte  := .F.

aAdd(aRotina,{"Efetivar"   ,"U_HOPEFET()",0,2,0,.F.})
aAdd(aRotina,{"Gerar Excel","U_HOPEXCEL()",0,2,0,.F.})

aCores := {}
aAdd(aCores,{'TRB->INTEG=="I"'	 ,'ENABLE'	})
aAdd(aCores,{'TRB->INTEG=="E"'	 ,'DISABLE'	})
aAdd(aCores,{'TRB->INTEG==" ".And.TRB->STATUS=="4"'	 ,'BR_AMARELO'	})
aAdd(aCores,{'TRB->INTEG==" ".And.TRB->STATUS=="3"'	 ,'BR_LARANJA'  })

aCampos  := {}
aAdd(aCampos, {"OK"       ,"",""		   		,					  })
aAdd(aCampos, {"STATUS"   ,"","Tipo Registro",					  })
aAdd(aCampos, {"VENDEDOR" ,"","Vendedor"  	,					  })
aAdd(aCampos, {"NOTA"     ,"","Nota Fiscal"	,					  })
aAdd(aCampos, {"SERIE"    ,"","S�rie"   		,					  })
aAdd(aCampos, {"PARCELA"  ,"","Parcela"		,					  })
aAdd(aCampos, {"VENCTO"   ,"","Vencimento"	,					  })
aAdd(aCampos, {"BASE"     ,"","Base Comis."	,X3Picture("E3_BASE") })
aAdd(aCampos, {"PERCENT"  ,"","Perc Comis."	,X3Picture("E3_PORC") })
aAdd(aCampos, {"COMISSAO" ,"","Valor Comis."	,X3Picture("E3_COMIS")})
aAdd(aCampos, {"PEDIDO"   ,"","Pedido"		,					  }) 
aAdd(aCampos, {"HISTORICO","","Historico"	,					  })
aAdd(aCampos, {"INTEG"    ,"","Status Integ.",					  })
aAdd(aCampos, {"MSG"  	  ,"","Msg.integ."	,					  })

aStruTRB := {}
aAdd(aStruTRB,{"OK"       ,"C",2                      ,0						})
aAdd(aStruTRB,{"STATUS"   ,"C",1    					 ,0						})
aAdd(aStruTRB,{"VENDEDOR" ,"C",TamSX3("E3_VEND")[1]   ,0						})
aAdd(aStruTRB,{"NOTA"     ,"C",TamSX3("E3_NUM")[1]    ,0						})
aAdd(aStruTRB,{"SERIE"    ,"C",TamSX3("E3_SERIE")[1]  ,0						})
aAdd(aStruTRB,{"PARCELA"  ,"C",TamSX3("E3_PARCELA")[1],0						})
aAdd(aStruTRB,{"VENCTO"   ,"D",8 					 ,0						})
aAdd(aStruTRB,{"BASE"     ,"N",TamSX3("E3_BASE")[1]   ,TamSX3("E3_BASE")[2]	})
aAdd(aStruTRB,{"PERCENT"  ,"N",TamSX3("E3_PORC")[1]   ,TamSX3("E3_PORC")[2]	})
aAdd(aStruTRB,{"COMISSAO" ,"N",TamSX3("E3_COMIS")[1]  ,TamSX3("E3_COMIS")[2]	})
aAdd(aStruTRB,{"PEDIDO"   ,"C",TamSX3("E3_PEDIDO")[1] ,0						})
aAdd(aStruTRB,{"HISTORICO","C",40						 ,0						})
aAdd(aStruTRB,{"INTEG"    ,"C",1    					 ,0						})
aAdd(aStruTRB,{"MSG"  	  ,"C",50  					 ,0						})

cArqTRB := CriaTrab( aStruTRB, .T. )

If Select("TRB") > 0
	TRB->(DbCloseArea())
Endif

DbUseArea(.T.,"DBFCDX",cArqTRB,"TRB",.F.,.F.)

_cIndex := Criatrab(Nil,.F.)
_cChave := 'VENDEDOR+STATUS'
Indregua("TRB",_cIndex,_cChave,,,"Ordenando registros selecionados...")
DbSetIndex(_cIndex+ordbagext())
Dbselectarea("TRB")
SysRefresh()

QRY->(DbGoTop())
While QRY->(!EoF())
	Reclock("TRB",.T.)
	TRB->OK 	  	:= cMarca
	TRB->VENDEDOR 	:= QRY->E3_VEND
	TRB->NOTA		:= QRY->E3_NUM
	TRB->SERIE		:= QRY->E3_SERIE
	TRB->PARCELA	:= QRY->E3_PARCELA
	TRB->VENCTO 	:= QRY->E3_VENCTO
	TRB->BASE		:= QRY->E3_BASE
	TRB->PERCENT	:= QRY->E3_PORC
	TRB->COMISSAO	:= Iif(QRY->E3_COMIS<0,QRY->E3_COMIS*(-1),QRY->E3_COMIS)
	TRB->HISTORICO  := QRY->E3_XORIGEM
	TRB->PEDIDO		:= QRY->E3_PEDIDO
	TRB->STATUS		:= QRY->E3_XORIGEM
	MsUnlock()
	QRY->(DbSkip())
EndDo

MarkBrowse( "TRB", "OK",,aCampos,lInverte, cMarca,"u_RecMarkAll()",.T.,,,"u_RecClick()",,,, aCores )

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HOPEFET    �Autor  �Marcio MEdeiros Jr  � Data �  23/01/17  ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava pedidos de venda					                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HOPEFET
MsgRun( "Gerando titulos, aguarde. . .","Comiss�o", {|| ProcCP() } )
Return

Static Function ProcCP

Local cHist := ""

aReg 		:= {}
aReport		:= {}

TRB->(DbGoTop())

DbSelectArea("SA3")
SA3->(DbSetOrder(1)) //A3_FILIAL, A3_COD, R_E_C_N_O_, D_E_L_E_T_

DbSelectArea("SA2")
SA2->(DbSetOrder(3)) //A2_FILIAL, A2_CGC, R_E_C_N_O_, D_E_L_E_T_

DbSelectArea("SE3")
SE3->(DbSetOrder(2)) //E3_FILIAL, E3_VEND, E3_PREFIXO, E3_NUM, E3_PARCELA, E3_SEQ, R_E_C_N_O_, D_E_L_E_T_

DbSelectArea("SE2")
SE2->(DbSetOrder(1)) //E2_FILIAL, E2_PREFIXO, E2_NUM, E2_PARCELA, E2_TIPO, E2_FORNECE, E2_LOJA, R_E_C_N_O_, D_E_L_E_T_

If Alltrim(TRB->STATUS) == "3"
	cHist := "DEB INADIMPLENCIA"
ElseIf  Alltrim(TRB->STATUS) == "4"	
	cHist := "CRED INADIMPLENCIA"
ElseIf Alltrim(TRB->STATUS) == "7"
	cHist := "OUTROS DEBITOS"
ElseIf Alltrim(TRB->STATUS) == "8"
	cHist := "OUTROS CREDITOS"
EndIf

cRegAtu := ""
While TRB->(!EoF())
	If !Empty(AllTrim(TRB->OK))
		
		SE3->(DbSetOrder(2)) //E3_FILIAL, E3_VEND, E3_PREFIXO, E3_NUM, E3_PARCELA, E3_SEQ, R_E_C_N_O_, D_E_L_E_T_
		SE3->( DbSeek( xFilial("SE3") + TRB->VENDEDOR + TRB->SERIE + TRB->NOTA + TRB->PARCELA ) )
		
		cRegAnt := cRegAtu
		cRegAtu := TRB->VENDEDOR + TRB->STATUS
		
		SA3->( DbSeek( xFilial("SA3") + TRB->VENDEDOR ) )
		SA2->(DbSetOrder(3)) //A2_FILIAL, A2_CGC, R_E_C_N_O_, D_E_L_E_T_
		If SA2->( DbSeek( xFilial("SA2") + SA3->A3_CGC ) )
			
		/*	If cRegAnt <> cRegAtu
				aNFVend := NFVend(TRB->STATUS,SA2->A2_COD,SA2->A2_LOJA)
				
			//	aNFVend:
			//	[1]QRYSE2->E2_FILIAL
			//	[2]QRYSE2->E2_NUM
			//	[3]QRYSE2->E2_PREFIXO
			//	[4]QRYSE2->E2_PARCELA
			//	[5]QRYSE2->E2_SALDO
			//	[6]nSldUti
				
			Endif    */

			If TRB->STATUS == "3" .OR. TRB->STATUS = "7" 
				cTipo   := "NDF"//AB-"
				cNat    := SuperGetMV("HP_NATABAT",.T.,"21532035")//"000002"

				aReg := { { "E2_FILIAL" , xFilial("SE2")    , NIL },;
						{ "E2_PREFIXO"  , TRB->SERIE   		, NIL },;//"NDF"
						{ "E2_NUM"      , TRB->NOTA	        , NIL },;
						{ "E2_TIPO"     , cTipo             , NIL },;  
						{ "E2_PARCELA"  , TRB->PARCELA      , NIL },;
						{ "E2_NATUREZ"  , cNat              , NIL },;  
						{ "E2_FORNECE"  , SA2->A2_COD       , NIL },;
						{ "E2_LOJA"  	, SA2->A2_LOJA      , NIL },;
						{ "E2_EMISSAO"  , dDatabase			, NIL },;
						{ "E2_VENCTO"   , dDatabase			, NIL },;
						{ "E2_VENCREA"  , dDatabase			, NIL },;
						{ "E2_HIST"     , cHist   			, NIL },; 
						{ "E2_VALOR"    , TRB->COMISSAO     , NIL } }
					
				MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aReg,, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
					
				If lMsErroAuto
					//	aAdd(aReport,{cCliAnt,cLojAnt,"Erro ao gerar titulo","E"})
					//	MostraErro(cDirLog,cArqLog)
					MostraErro()
					RecLock("TRB",.F.)
					TRB->OK		:= ""
					TRB->INTEG  := "E"
					TRB->MSG	:= "Erro na gera��o do CP. Consulte log de integra��o"
					MsUnlock()
					lHaErros 	:= .T.
				Else
						// 	aAdd(aReport,{cCliAnt,cLojAnt,"Pedido criado " + SC5->C5_NUM,"I"})
					Begin Transaction
					RecLock("TRB",.F.)
					TRB->OK		:= ""
					TRB->INTEG  := "I"
					TRB->MSG	:= "Gerado CP NDF " + TRB->SERIE + "/" + TRB->NOTA
					MsUnlock()
					/*
					RecLock("SE3",.F.)
					SE3->E3_DATA := dDataBase
					MsUnlock()
					*/
						
					cQuery := "UPDATE " + RetSqlName("SE3") + " SET E3_DATA = '" + DTOS(dDataBase) +"' "
					cQuery += "WHERE E3_VEND = '" + TRB->VENDEDOR + "' AND E3_SERIE = '" + TRB->SERIE + "' "
					cQuery += "AND E3_NUM = '" + TRB->NOTA + "' AND E3_PARCELA = '" + TRB->PARCELA	+ "' "
					cQuery += "AND E3_XORIGEM = '" + TRB->STATUS +  "' AND D_E_L_E_T_ = '' "
						
					If TcSqlExec(cQuery) > 0
						MsgAlert("Erro na atualiza��o do SE3!!")
					Endif
					
					End Transaction 
				Endif
				/*	
					nTotSld := 0
					aEval(aNFVend,{ |x| nTotSld += (x[5] - x[6]) } )
					
					If nTotSld >= TRB->COMISSAO
						
						nNF 	  := 1
						nTRBCOMIS := TRB->COMISSAO
						
						For nNF := 1 to Len(aNFVend)
							If nTRBCOMIS > 0
								If aNFVend[nNF][5] > aNFVend[nNF][6]
									nDif := aNFVend[nNF][5] - aNFVend[nNF][6]
									If nTRBCOMIS <= nDif
										aNFVend[nNF][6] += nTRBCOMIS
										nTRBCOMIS		:= 0
										nSubtrai		:= 0
									Else
										aNFVend[nNF][6] += nDif
										nTRBCOMIS       -= nDif
										nSubtrai		:= nDif
									Endif
									
									cParc  := ""
									nOpRot := 3
									
									If SE2->( DbSeek( xFilial("SE2") + aNFVend[nNF][3] + aNFVend[nNF][2] + aNFVend[nNF][4] + cTipo + SA2->A2_COD + SA2->A2_LOJA ) )  //E2_FILIAL, E2_PREFIXO, E2_NUM, E2_PARCELA, E2_TIPO, E2_FORNECE, E2_LOJA, R_E_C_N_O_, D_E_L_E_T_
										nOpRot := 4
										aReg := { { "E2_FILIAL"   , xFilial("SE2")    		 	, NIL },;
										{ "E2_PREFIXO"  , aNFVend[nNF][3]     	 	, NIL },;
										{ "E2_NUM"      , aNFVend[nNF][2]     	 	, NIL },;
										{ "E2_PARCELA"  , aNFVend[nNF][4]    		 	, NIL },;
										{ "E2_TIPO"     , cTipo             		 	, NIL },;
										{ "E2_VALOR"    , SE2->E2_VALOR + (TRB->COMISSAO - nSubtrai)	, NIL } }
									Else
										aReg := { { "E2_FILIAL"   , xFilial("SE2")    		 	, NIL },;
										{ "E2_PREFIXO"  , aNFVend[nNF][3]     	 	, NIL },;
										{ "E2_NUM"      , aNFVend[nNF][2]     	 	, NIL },;
										{ "E2_PARCELA"  , aNFVend[nNF][4]    		 	, NIL },;
										{ "E2_TIPO"     , cTipo             		 	, NIL },;
										{ "E2_MULTNAT"  , '2'             		 	, NIL },;
										{ "E2_NATUREZ"  , cNat              		 	, NIL },;
										{ "E2_FORNECE"  , SA2->A2_COD       		 	, NIL },;
										{ "E2_LOJA"  	  , SA2->A2_LOJA       		 	, NIL },;
										{ "E2_EMISSAO"  , dDatabase				 	, NIL },;
										{ "E2_VENCTO"   , dDatabase				 	, NIL },;
										{ "E2_VENCREA"  , dDatabase				 	, NIL },;
										{ "E2_VALOR"    , (TRB->COMISSAO - nSubtrai)	, NIL } }
									Endif
									
									
									
									MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aReg,, nOpRot)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
									
									If lMsErroAuto
										//	aAdd(aReport,{cCliAnt,cLojAnt,"Erro ao gerar titulo","E"})
										//	MostraErro(cDirLog,cArqLog)
										RecLock("TRB",.F.)
										TRB->OK		:= ""
										TRB->INTEG  := "E"
										TRB->MSG	:= "Erro na gera��o do CP. Consulte log de integra��o"
										MsUnlock()
										lHaErros 	:= .T.
									Else
										// 	aAdd(aReport,{cCliAnt,cLojAnt,"Pedido criado " + SC5->C5_NUM,"I"})
										Begin Transaction
										RecLock("TRB",.F.)
										TRB->OK		:= ""
										TRB->INTEG  := "I"
										TRB->MSG	:= "Gerado CP AB- " + aNFVend[nNF][3] + "/" + aNFVend[nNF][2]
										MsUnlock()
										
									//	RecLock("SE3",.F.)
									//	SE3->E3_DATA := dDataBase
									//	MsUnlock()
										
										cQuery := "UPDATE " + RetSqlName("SE3") + " SET E3_DATA = '" + DTOS(dDataBase) +"' "
										cQuery += "WHERE E3_VEND = '" + TRB->VENDEDOR + "' AND E3_SERIE = '" + TRB->SERIE + "' "
										cQuery += "AND E3_NUM = '" + TRB->NOTA + "' AND E3_PARCELA = '" + TRB->PARCELA	+ "' "
										cQuery += "AND D_E_L_E_T_ = ''"
										
										If TcSqlExec(cQuery) > 0
											MsgAlert("Erro na atualiza��o do SE3!!")
										Endif
										
										End Transaction
									Endif
									
								Endif
							Else
								Exit
							Endif
						Next       */	
			Else
				cTipo := "NCF"
				cNat  := SuperGetMV("HP_NATNFC",.T.,"21532035")//"000001"
				
				aReg := { { "E2_FILIAL"   , xFilial("SE2")    , NIL },;
				{ "E2_PREFIXO"  , TRB->SERIE       		, NIL },; //"NCF"
				{ "E2_NUM"      , TRB->NOTA	        	, NIL },;
				{ "E2_TIPO"     , cTipo             	, NIL },;  
				{ "E2_PARCELA"  , TRB->PARCELA          , NIL },;
				{ "E2_NATUREZ"  , cNat              	, NIL },;
				{ "E2_FORNECE"  , SA2->A2_COD       	, NIL },;
				{ "E2_LOJA"  	  , SA2->A2_LOJA       	, NIL },;
				{ "E2_EMISSAO"  , dDatabase				, NIL },;
				{ "E2_VENCTO"   , dDatabase				, NIL },;
				{ "E2_VENCREA"  , dDatabase				, NIL },;    
				{ "E2_HIST"     , cHist       			, NIL },; 				
				{ "E2_VALOR"    , TRB->COMISSAO     	, NIL } }
					
				MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aReg,, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
					
				If lMsErroAuto
					//	aAdd(aReport,{cCliAnt,cLojAnt,"Erro ao gerar titulo","E"})
					//	MostraErro(cDirLog,cArqLog)
					MostraErro()
					RecLock("TRB",.F.)
					TRB->OK		:= ""
					TRB->INTEG  := "E"
					TRB->MSG	:= "Erro na gera��o do CP. Consulte log de integra��o"
					MsUnlock()
					lHaErros 	:= .T.
				Else
					// 	aAdd(aReport,{cCliAnt,cLojAnt,"Pedido criado " + SC5->C5_NUM,"I"})
					Begin Transaction
					RecLock("TRB",.F.)
					TRB->OK		:= ""
					TRB->INTEG  := "I"
					TRB->MSG	:= "Gerado CP NCF " + TRB->SERIE + "/" + TRB->NOTA
					MsUnlock()
					/*
					RecLock("SE3",.F.)
					SE3->E3_DATA := dDataBase
					MsUnlock()
					*/
						
					cQuery := "UPDATE " + RetSqlName("SE3") + " SET E3_DATA = '" + DTOS(dDataBase) +"' "
					cQuery += "WHERE E3_VEND = '" + TRB->VENDEDOR + "' AND E3_SERIE = '" + TRB->SERIE + "' "
					cQuery += "AND E3_NUM = '" + TRB->NOTA + "' AND E3_PARCELA = '" + TRB->PARCELA	+ "' "
					cQuery += "AND E3_XORIGEM = '" + TRB->STATUS +  "' AND D_E_L_E_T_ = '' "
					
					If TcSqlExec(cQuery) > 0
						MsgAlert("Erro na atualiza��o do SE3!!")
					Endif
					
					End Transaction
				Endif
			Endif
		Else
			RecLock("TRB",.F.)
			TRB->OK		:= ""
			TRB->INTEG  := "E"
			TRB->MSG	:= "O vendedor n�o est� cadastrado como fornecedor"
			MsUnlock()
		Endif
	Endif
	lMsErroAuto := .F.
	TRB->(DbSkip())
EndDo

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �NFVend     �Autor  �Marcio Medeiros Jr  � Data �  08/07/18  ���
�������������������������������������������������������������������������͹��
���Desc.     �        										              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function NFVend(cTRBSTATUS,cA2COD,cA2LOJA)

Local aSE2 := {}

cQRYSE2 := "SELECT E2_FILIAL,E2_NUM,E2_PREFIXO,E2_PARCELA,E2_TIPO,E2_FORNECE,E2_LOJA,E2_EMISSAO,E2_SALDO FROM " + RetSQLName("SE2")
cQRYSE2 += " WHERE E2_FILIAL = '"+xFilial("SE2")+"'"
cQRYSE2 += "   AND E2_FORNECE = '"+cA2COD+"'"
cQRYSE2 += "   AND E2_LOJA = '"+cA2LOJA+"'"
cQRYSE2 += "   AND E2_TIPO = 'NF'"
If cTRBSTATUS == "D"
	cQRYSE2 += "   AND E2_SALDO > 0 "
Endif
cQRYSE2 += "   AND D_E_L_E_T_ = ''"
cQRYSE2 += " ORDER BY E2_EMISSAO DESC"

If Select("QRYSE2") > 0
	QRYSE2->(DbCloseArea())
Endif

TCQUERY cQRYSE2 NEW ALIAS "QRYSE2"

TCSetField("QRYSE2","E2_SALDO","N",TamSX3("E2_SALDO")[1],TamSX3("E2_SALDO")[2])

While QRYSE2->(!EoF())
	aAdd(aSE2,{QRYSE2->E2_FILIAL,QRYSE2->E2_NUM,QRYSE2->E2_PREFIXO,QRYSE2->E2_PARCELA,QRYSE2->E2_SALDO,0})
	QRYSE2->(DbSkip())
EndDo
VarInfo('',aSE2)
Return aSE2

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RecClick   �Autor  �Marcio MEdeiros Jr  � Data �  23/01/17  ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao para marcar/ desmarcar um item                       ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function RecClick()

If Empty(AllTrim(TRB->INTEG))
	RecLock("TRB",.F.)
	TRB->OK := IIf( TRB->OK == cMarca, " ", cMarca )
	MsUnlock()
Endif
Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RecMarkAll�Autor  �Marcio MEdeiros Jr  � Data �  23/01/17  ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao para marcar/desmarcar todos os itens.                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function RecMarkAll()
Local aArea	:= GetArea()

TRB->( DbGoTop() )
While TRB->( !Eof() )
	If Empty(AllTrim(TRB->INTEG))
		RecLock("TRB",.F.)
		TRB->OK := IIf( TRB->OK == cMarca, " ", cMarca )
		MsUnlock()
	Endif
	TRB->( DbSkip() )
EndDo

RestArea( aArea )
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HOPEXCEL   �Autor  �Marcio Medeiros Jr  � Data �  08/07/18  ���
�������������������������������������������������������������������������͹��
���Desc.     �        										              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HOPEXCEL

Local oExcel := FWMSExcel():New()
cPathTmp := cGetFile('','Onde quer salvar?',0,,.F.,GETF_LOCALHARD+ GETF_RETDIRECTORY+GETF_NETWORKDRIVE)

If !Empty(cPathTmp)
	cArqPesq := cPathTmp + "Comis_Hope_"+DTOS(date())+"-"+StrTran(time(),":","")+".xml"
	
	If File(cArqPesq)
		FErase(cArqPesq)
	EndIf
	
	TRB->(DbGoTop())
	If TRB->(!EoF())
		oExcel:AddWorkSheet("Pagto.Comissao")
		oExcel:AddTable("Pagto.Comissao","Conferencia")
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Vendedor",1,1)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Nota",1,1)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Serie",1,1)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Parcela",1,1)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Vencimento",2,4)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Base C�lculo",3,2)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Perc.Comiss�o",3,2)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Valor Comiss�o",3,2)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Pedido",1,1)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Tipo Registro",1,1)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Status Integr.",1,1)
		oExcel:AddColumn("Pagto.Comissao","Conferencia","Msg. Integra��o",1,1)
		
		While TRB->(!EoF())
			oExcel:AddRow("Pagto.Comissao","Conferencia",{TRB->VENDEDOR,;
			TRB->NOTA,;
			TRB->SERIE,;
			TRB->PARCELA,;
			TRB->VENCTO,;
			TRB->BASE,;
			TRB->PERCENT,;
			TRB->COMISSAO,;
			TRB->PEDIDO,;
			Iif(TRB->STATUS=="4","Cr�dito Inadimpl�ncia","D�bito Inadimpl�ncia"),;
			TRB->INTEG,;
			TRB->MSG})
			TRB->(DbSkip())
		EndDo
		
		oExcel:Activate()
		oExcel:GetXMLFile(cArqPesq)
		oExcel:DeActivate()
		oExcelApp:= MsExcel():New()
		oExcelApp:WorkBooks:Open(cArqPesq)
		oExcelApp:SetVisible(.T.)
	Endif
Endif
Return
