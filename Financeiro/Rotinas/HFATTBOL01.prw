
#INCLUDE "RWMAKE.CH"
#include "colors.ch"
#INCLUDE "AP5MAIL.CH"
#INCLUDE "PROTHEUS.CH"
#include "tbiconn.ch"
#include "topconn.ch"
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"

user function HFATTBOL01()
	Local aArea := GetArea()
//IF MsgYesno("Confirma gera��o de T�tulos de Fundo de Marketing? " )
	Processa({|| u_fdmbol()})
//Endif
	RestArea(aArea)
Return

USER FUNCTION fdmbol()
	Local _astru     := {}
	Local _afields   := {}
	Local _carq      := ""
	Local _cTpPag    := 'BOLETO IMPRESSO   '
	Local cQuery     := ""
	Local xFornec    := ""
	Local xLoja      := ""
	Local xVecto     := ctod(space(8))
	Local nValor     := 0
	Local xItens     := ""
	Local xRazao     := ""
	Local cFiltraSCR
	Local ca097User  := RetCodUsr()
	Local Fazer      := .f.
	Local aRet       := {}
	Local nlin       := 0
	Local oFont1  := TFont():New("Verdana",,012,,.T.,,,,,.F.,.F.)
	Local oFont2  := TFont():New("Verdana",,012,,.F.,,,,,.F.,.F.)
	Local aColunas := {{"MK_OK"       ,,""       	           },;
		{"E1_PREFIXO"  ,,"Prefixo"       ,"@!" },;
		{"E1_NUM"      ,,"Numero T�tulo" ,"@!" },;
		{"E1_CLIENTE"  ,,"Cod.Cliente"   ,"@!" },;
		{"E1_LOJA"     ,,"Lj Cliente"    ,"@!" },;
		{"E1_EMISSAO"   ,," Emissao    " ,"@D" },;
		{"E1_NOMCLI"   ,,"Nome Cliente"  ,"@!" },;
		{"E1_VALOR"    ,,"Valor Liq."    ,"@E 99,999,999.99"},;
		{"E1_VENCREA"   ,,"Vencimento"    ,"@D" },;
		{"E1_XTPPAG"   ,,"Tipo Pagto"    ,"@!" },;
		{"E1_XCANALD"  ,,"Canal "        ,"@!" },;
        {"E1_NUMBCO"  ,,"Numero bancario","@!" }}

	Private aHeader := {}
	Private aCOLS := {}
	Private aGets := {}
	Private aTela := {}
	Private aREG := {}
	Private cCadastro := "Registros de Titulos de Fundo de Marketing "
	Private aRotina := {}
	Private oCliente
	Private oTotal
	Private cCliente := ""
	Private nTotal := 0
	Private bCampo := {|nField| FieldName(nField) }
	Private aSize := {}
	Private aInfo := {}
	Private aObj := {}
	Private aPObj := {}
	Private aPGet := {}
	Private cGet1 := Space(25)
	Private cGet2 := Space(25)
	Private lContinua := .F.
	Private atuar     := ""
	Private cMark     := GetMark()
	Private lInverte  := .F.
	Private oChk
	Private lChkSel   := .F.
	Private lRefresh  := .T.
	Private oDlgT
	Private onVlrCom
	Private onVlrSld
	Private onVlrMar
	Private onVlrfIL
	Private onVlrSob
	Private nVlrCom   := 0
	Private nVlrSld   := 0
	Private nVlrMar   := 0
	Private nVlrfIL   := 0
	Private nVlrSob   := 0
	Private lMarcar  := .F.
	Private oMark3
	Private cPerg     := "HP_BOLETO0"
	Private aRotina   := {}
	Private lContinua := .F.
	Private atuar     := ""
	Private cMark2    := GetMark()
    Private aGerBol   := {} 
// Retorna a �rea �til das janelas Protheus
	aSize := MsAdvSize()
// Ser� utilizado tr�s �reas na janela
// 1� - Enchoice, sendo 80 pontos pixel
// 2� - MsGetDados, o que sobrar em pontos pixel � para este objeto
// 3� - Rodap� que � a pr�pria janela, sendo 15 pontos pixel
	AADD( aObj, { 100, 080, .T., .F. })
	AADD( aObj, { 100, 100, .T., .T. })
	AADD( aObj, { 100, 015, .T., .F. })
	aInfo := { aSize[1], aSize[2], aSize[3], aSize[4], 3, 3 }
	aPObj := MsObjSize( aInfo, aObj )
// C�lculo autom�tico de dimens�es dos objetos MSGET
	aPGet := MsObjGetPos( (aSize[3] - aSize[1]), 315, { {004, 024, 240, 270} } )
	If Select("XTRB") > 0
		DbSelectArea("XTRB")
		XTRB->(DbCloseArea())
	EndIf

    CriaSX1(cPerg)
    Pergunte(cPerg,.f.)
    MV_PAR01 := DDATABASE
    MV_PAR02 := DDATABASE
    MV_PAR0322 := 'BOLETO IMPRESSO     '
	If !Pergunte(cPerg,.T.)
		Return
	EndIf

	cQuery := "SELECT SPACE(2) AS MK_OK, E1_PREFIXO,E1_NUM,E1_CLIENTE,E1_LOJA ,E1_EMISSAO, E1_NOMCLI ,E1_VALOR,E1_VENCREA ,E1_XTPPAG,E1_XCANALD, E1_NUMBCO "
	cQuery += CRLF + "FROM "+RetSqlName("SE1")+" WITH (NOLOCK) WHERE E1_EMISSAO  BETWEEN  '"+DTOS(MV_PAR01)+"' AND '"+DTOS(MV_PAR02)+"' " 
	cQuery += CRLF + "AND E1_XTPPAG = '"+ALLTRIM(MV_PAR03)+"' AND E1_NUM BETWEEN  '"+ALLTRIM(MV_PAR04)+"' AND '"+ALLTRIM(MV_PAR05)+"' AND (E1_PARCELA = ' ' OR E1_PARCELA = 'A' )  AND D_E_L_E_T_ <> '*'  order by E1_PREFIXO+E1_NUM "
 
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRB", .F., .T.)
	XTRB->(dbgotop())

	If XTRB->(Eof())
		MsgStop("Nenhum registro encontrado para EMISSAO., Verifique")
		Return()
	Endif
	If Select("XMKT") > 0
		DbSelectArea("XMKT")
		XMKT->(DbCloseArea())
	EndIf

//Estrutura da tabela temporaria
	AADD(_astru,{"MK_OK"      ,"C", 02,0})
	AADD(_astru,{"E1_PREFIXO" ,"C", 03,0})
	AADD(_astru,{"E1_NUM"     ,"C", 09,0})
	AADD(_astru,{"E1_CLIENTE" ,"C", 06,0})
	AADD(_astru,{"E1_LOJA"    ,"C", 04,0})
	AADD(_astru,{"E1_EMISSAO" ,"D", 08,0})
	AADD(_astru,{"E1_NOMCLI"  ,"C", 30,0})
	AADD(_astru,{"E1_VALOR"   ,"N", 16,2})
	AADD(_astru,{"E1_VENCREA" ,"D", 08,2})
	AADD(_astru,{"E1_XTPPAG"  ,"C", 15,0})
	AADD(_astru,{"E1_XCANALD"  ,"C", 15,0})
	AADD(_astru,{"E1_NUMBCO"  ,"C", 10,0})

	cArqTrab  := CriaTrab(_astru)
	dbUseArea( .T.,, cArqTrab, "XMKT", .F., .F. )

	While XTRB->(!EOF())
		DbSelectArea("XMKT")
		XMKT->(RecLock("XMKT",.T.))
		XMKT->E1_PREFIXO := XTRB->E1_PREFIXO
       	XMKT->E1_NUM     := XTRB->E1_NUM
		XMKT->E1_CLIENTE := XTRB->E1_CLIENTE
		XMKT->E1_LOJA    := XTRB->E1_LOJA
		XMKT->E1_EMISSAO := ctod(substr(XTRB->E1_EMISSAO,7,2)+'/'+substr(XTRB->E1_EMISSAO,5,2)+'/'+substr(XTRB->E1_EMISSAO,3,2))
		XMKT->E1_NOMCLI  := XTRB->E1_NOMCLI
		XMKT->E1_VALOR   := XTRB->E1_VALOR 
		XMKT->E1_VENCREA := ctod(substr(XTRB->E1_VENCREA,7,2)+'/'+substr(XTRB->E1_VENCREA,5,2)+'/'+substr(XTRB->E1_VENCREA,3,2))
		XMKT->E1_XTPPAG  := XTRB->E1_XTPPAG
		XMKT->E1_XCANALD := XTRB->E1_XCANALD
		XMKT->E1_NUMBCO  := XTRB->E1_NUMBCO
		XMKT->(MsUnlock())
		XTRB->(DbSkip())
	Enddo

	DbSelectArea("XMKT")
	XMKT->(DbGotop())
	DEFINE MSDIALOG oDlgT TITLE "HOPE - Emissao de Boleto Banc�rio " FROM aSize[7],100 To aSize[6],aSize[5] COLORS 0, 16777215 PIXEL Style DS_MODALFRAME
	cMarca    := GetMark()
	@ 005, 005 SAY oSay2 PROMPT "Lista dos Titulos para emiss�o de Boleto Banc�rio." SIZE 242, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
	oMark := MsSelect():New("XMKT","MK_OK",,aColunas,lInverte,cMarca,{ 015, 003, 242, 570})
	oMark:bAval:= {||(HPFDK009E(cMarca),oMark:oBrowse:Refresh())}
	oMark:oBrowse:Refresh(.F.)
	oMark:oBrowse:lHasMark    := .T.
	oMark:oBrowse:lCanAllMark := .T.
	oMark:oBrowse:bAllMark := {|| U_HPFDK009F(cMarca),oMark:oBrowse:Refresh()}
	@ 250, 005 BUTTON oButton1 PROMPT "  Emitir   " SIZE 055, 013 OF oDlgT ACTION lretG:= HPFDK009H(cMarca,1) PIXEL
	@ 250, 085 BUTTON oButton1 PROMPT "  Fechar   " SIZE 035, 013 OF oDlgT ACTION LretG:= HPFDK009H(cMarca,5) PIXEL
	@ 270, 190 Say oSay7 prompt " Quantida de Boletos Marcado :" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
	@ 270,315 MSGET onVlrMar VAR nVlrMar When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"

	ACTIVATE MSDIALOG oDlgT CENTERED
	if !Empty('XMKT')
		XMKT->(DbCloseArea())
	Endif
Return()

Static Function HPFDK009H(cMarca,xopc)
	Private nSaldoComp:= 0	
	Private cBoleto  := Alltrim(MV_PAR03)
	
	If xOpc <> 5
		dbSelectArea("XMKT")
		XMKT->(dbGoTop())
		xFez:=''
		While XMKT->(!Eof())
			If Alltrim(XMKT->MK_OK) <> ""
				IF xOpc == 1
				    if xFez <> XMKT->E1_PREFIXO+XMKT->E1_NUM+XMKT->E1_CLIENTE+XMKT->E1_LOJA
				       Aadd(aGerBol,{ XMKT->E1_PREFIXO,XMKT->E1_NUM,XMKT->E1_CLIENTE,XMKT->E1_LOJA})
					   //U_BOLITAU1(XMKT->E1_PREFIXO,XMKT->E1_NUM,XMKT->E1_CLIENTE,XMKT->E1_LOJA)
					   //U_BOLITAU1(aGerBol)
	     	   	       xFez:=XMKT->E1_PREFIXO+XMKT->E1_NUM+XMKT->E1_CLIENTE+XMKT->E1_LOJA
					Endif     
				Endif
			Endif
			XMKT->(DBSKIP())
		End

		IF MsgYesno("Confirma a gera��o do "+cBoleto+IIF(cBoleto == "BOLETO IMPRESSO"," - BANCO ITAU"," - BANCO DO BRASIL") )
			If cBoleto == "BOLETO IMPRESSO"											 
				U_BOLITAU1(aGerBol)  //Gera boleto do Itau
			Else 
				//Alterado por UYRANDE RIBEIRO - 17/12/3030
				U_BOLBRASIL(aGerBol)  
			EndIf
		EndIf  

		msgStop("Processo Finalizado!")

	Endif
	onVlrMar:Refresh()
	XMKT->(dbGoTop())
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
	oDlgT:End()
Return(Nil)


Static Function HPFDK009E(cMarca)
	If Alltrim(XMKT->MK_OK) == ""
		XMKT->(RecLock( "XMKT", .F. ))
		XMKT->MK_OK   := cMarca
		XMKT->(msUnlock())
		nVlrMar++
	Else
		nVlrSob := 0
		XMKT->(RecLock( "XMKT", .F. ))
		XMKT->MK_OK   := Space(2)
		XMKT->(msUnlock())
		nVlrMar--
	EndIf
	onVlrMar:Refresh()
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return

User Function HPFDK009F(cMarca)
	XMKT->(dbGoTop())
	While XMKT->(!Eof())
		If Alltrim(XMKT->MK_OK) == ""
			XMKT->(RecLock( "XMKT", .F. ))
			XMKT->MK_OK   := cMarca
			XMKT->(msUnlock())
			nVlrMar++ 
		//msgStop("Diminuindo saldo Pois foi Marcado")
		Else
			XMKT->(RecLock( "XMKT", .F. ))
			XMKT->MK_OK   := Space(2)
			XMKT->(msUnlock())
			nVlrMar-- 
		//msgStop("Somonado o saldo Pois foi desMarcado")
		EndIf
		XMKT->(DBSKIP())
	End
	onVlrMar:Refresh()
	XMKT->(dbGoTop())
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return(Nil)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



Static Function CriaSX1(cPerg)

Local _sAlias 	:= Alias()
Local aRegs 	:= {}
Local i,j

dbSelectArea("SX1")
dbSetOrder(1)

cPerg := PADR(cPerg,10)
aAdd(aRegs,{cPerg,"01","De Emissao     ?","","","mv_ch1","D",08,0,0,"G","","MV_PAR01","","","","15/04/18","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"02","Ate Emissao    ?","","","mv_ch2","D",08,0,0,"G","","MV_PAR02","","","","15/04/18","","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"03","Tipo Bol       ?","","","mv_ch3","C",15,0,1,"G","","MV_PAR03","","","",""        ,"","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"04","do  N�mero     ?","","","mv_ch4","C",09,0,0,"G","","MV_PAR04","","","",""        ,"","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aRegs,{cPerg,"05","At� N�mero     ?","","","mv_ch5","C",09,0,0,"G","","MV_PAR05","","","",""        ,"","","","","","","","","","","","","","","","","","","","","","","","",""})

For i:=1 to Len(aRegs)
   If !dbSeek(cPerg+aRegs[i,2])
      RecLock("SX1",.T.)
      For j:=1 to FCount()
        If j <= Len(aRegs[i])
           FieldPut(j,aRegs[i,j])
        Endif
      Next
      MsUnlock()
   Endif
Next
Return

