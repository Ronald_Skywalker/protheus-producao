#include 'protheus.ch'
#include 'parmtype.ch'
#Include "Totvs.ch"
#Include "FwMvcDef.ch"
#INCLUDE "RWMAKE.CH"
#Include "ApWizard.ch"
#include "fileio.ch" 

user function HPCompe01()
	Local aLegenda 		:= {{"E1_SALDO == E1_VALOR" , "BR_VERDE"},;
	{"E1_SALDO <= E1_VALOR .AND. E1_SALDO > 0" , "BR_AMARELO"},;
	{"E1_SALDO == 0       " , "BR_VERMELHO"}}

	Private cCadastro 	:= "Compensa��o E-Commerce - HOPE"
	Private cString 	:= "SE1"
	Private aRotina 	:= {{"Pesquisar"	 ,"AxPesqui"	 ,0,1},;
	{"Compensar","U_HPComp02"   ,0,4},;
	{"Extornar","U_HPComp03"   ,0,4},;
	{"Boleto Ita�","U_HPGerbol"   ,0,4},;
	{"Legenda"  ,"U_HPCOMPLEG"  ,0,5}}

	dbSelectArea(cString)
	dbSetOrder(1)

	mBrowse(6,1,22,75,cString,,,,,,aLegenda)

Return

User Function HPCOMPLEG()

	BrwLegenda("Compensa��o E-Commerce","Legenda",{{"BR_VERDE"	,"N�o Processado"	 },;
	{"BR_AMARELO"	,"Processado Parcial"},;
	{"BR_VERMELHO"		,"Processado Total"}})
Return



