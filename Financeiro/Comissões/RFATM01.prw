#include "protheus.ch"
#include "topconn.ch"
/******************************************************************************
* Programa: RFATM01           09/2019            Totalit Solutions            *
* Funcionalidade: Programa para calcular as comissoes de vendas               *
* Parametros: cTipo = F - Faturamento                                         *
*                     E - Exclusao de notas de saida                          *
*                     D - Devolucao de vendas                                 *
*                     R - Exclusao de notas de devolucao de vendas            *
******************************************************************************/
User Function RFATM01(cTipo)
Local nCount := 0
Local nPercCom := 0
Local nPos := 0
Local cCriter := ""
Local cCond := ""
Local cTabela := ""
Local nZZGPev := 0
Local aPercCom := {}
Local aComis := {}
Local aAreaAnt := GetArea()
Local aAreaDA0 := DA0->(GetArea())
Local aAreaSA1 := SA1->(GetArea())
Local aAreaSA3 := SA3->(GetArea())
Local aAreaSB1 := SB1->(GetArea())
Local aAreaSC5 := SC5->(GetArea())
Local aAreaSD1 := SD1->(GetArea())
Local aAreaSD2 := SD2->(GetArea())
Local aAreaSE1 := SE1->(GetArea())
Local aAreaSE3 := SE3->(GetArea())
Local aAreaSE4 := SE4->(GetArea())
Local aAreaSF2 := SF2->(GetArea())
Local aAreaSZ1 := SZ1->(GetArea())
Local aAreaZAI := ZAI->(GetArea())
Local aAreaZZG := ZZG->(GetArea())
Local i := 0
Private cVend  := ""

DA0->(dBSetOrder(1))  // DA0_FILIAL+DA0_CODTAB
SA1->(dBSetOrder(1))  // A1_FILIAL+A1_COD+A1_LOJA
SA3->(dBSetOrder(1))  // A3_FILIAL+A3_COD
SB1->(dBSetOrder(1))  // B1_FILIAL+B1_COD
SC5->(dBSetOrder(1))  // C5_FILIAL+C5_NUM
SD1->(dBSetOrder(1))  // D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
SD2->(dBSetOrder(3))  // D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
SE1->(dBSetOrder(1))  // E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
SE3->(dBOrderNickName("VENDMARCA"))  // E3_FILIAL+E3_VEND+E3_XMARCA+E3_PREFIXO+E3_NUM+E3_TIPO+E3_CODCLI+E3_LOJA
SE4->(dBSetOrder(1))  // E4_FILIAL+E4_CODIGO
SZ1->(dBSetOrder(1))  // Z1_FILIAL+Z1_CODIGO+Z1_DESC
ZAI->(dBSetOrder(1))  // ZAI_FILIAL+ZAI_CODIGO
ZZG->(dBSetOrder(1))  // ZZG_FILIAL+ZZG_CHVSD2+ZZG_VEND

//cBlq := POSICIONE("SA3",1,xFilial("SA3")+Alltrim(SC5->C5_VEND1),"A3_MSBLQL")



	BEGIN TRANSACTION

	If cTipo $ "FE" .and. sd2->(MSSeek(xFilial("SD2")+SF2->(F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA)))
		cCond := 'SD2->(!EOF() .AND. D2_FILIAL = XFILIAL("SD2") .AND. D2_DOC = SF2->F2_DOC .AND. '
		cCond += 'D2_SERIE = SF2->F2_SERIE .AND. D2_CLIENTE = SF2->F2_CLIENTE .AND. D2_LOJA = SF2->F2_LOJA)'
		cTabela := "SD2"
	ElseIf cTipo $ "DR" .and. sd1->(MSSeek(xFilial("SD1")+SF1->(F1_DOC+F1_SERIE+F1_FORNECE+F1_LOJA)))
		cCond := 'SD1->(!EOF() .AND. D1_FILIAL = XFILIAL("SD1") .AND. D1_DOC = SF1->F1_DOC .AND. '
		cCond += 'D1_SERIE = SF1->F1_SERIE .AND. D1_FORNECE = SF1->F1_FORNECE .AND. D1_LOJA = SF1->F1_LOJA)'
		cTabela := "SD1"
	Endif

	While &cCond
		// So calcular comissoes para tipos de pedidos que estejam configurados para gerar e para 
		// notas em que o vendedor tenha percentual maior que zero
		If If(cTipo $ "DR",SF2->(!MSSeek(xFilial("SF2")+SD1->(D1_NFORI+D1_SERIORI+D1_FORNECE+D1_LOJA))) .or. ;
			SD2->(!MSSeek(xFilial("SD2")+SD1->(D1_NFORI+D1_SERIORI+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEMORI))),.F.) .or. ;
			Empty(SF2->(F2_VEND1+F2_VEND2+F2_VEND3+F2_VEND4+F2_VEND5)) .or. Empty(SF2->F2_DUPL) .or. ;
			SC5->(!MSSeek(xFilial("SC5")+SD2->D2_PEDIDO)) .or. ;
			SZ1->(!MSSeek(xFilial("SZ1")+SC5->C5_TPPED) .or. Z1_COMIS <> "S") .or. ;
			SA3->(!MSSeek(xFilial("SA3")+SF2->F2_VEND1) .or. A3_COMIS = 0) .or. ;
			DA0->(!MSSeek(xFilial("DA0")+SC5->C5_TABELA)) .or. ;
			SA1->(!MSSeek(xFilial("SA1")+SF2->F2_CLIENTE+SF2->F2_LOJA)) .or. ;
			SE4->(!MSSeek(xFilial("SE4")+SC5->C5_CONDPAG))

			(cTabela)->(dBSkip())
			Loop
		Endif

		If cTipo = "R"
			SE1->(MSSeek(xFilial("SE1")+sf1->(f1_serie+f1_doc)))
			While SE1->(!Eof() .and. E1_FILIAL = xFilial("SE1") .and. E1_PREFIXO = SF1->F1_SERIE .AND. E1_NUM = SF1->F1_DOC .AND. E1_TIPO <> "NCC")
				SE1->(dBSkip())
			Enddo
		Else
			SE1->(MSSeek(xFilial("SE1")+SF2->(f2_serie+f2_doc)))
			While SE1->(!Eof() .and. E1_FILIAL = xFilial("SE1") .and. E1_PREFIXO = SF2->F2_SERIE .AND. E1_NUM = SF2->F2_DOC .AND. E1_TIPO <> "NF")
				SE1->(dBSkip())
			Enddo
		Endif

		For nCount := 1 to 5
			cVend := "F2_VEND"+Str(nCount,1)
			If Empty(SF2->&cVend)
				Loop
			Endif

			SA3->(MSSeek(xFilial("SA3")+SF2->&CVEND))
			SB1->(MSSeek(xFilial("SB1")+SD2->D2_COD))
			ZAI->(MSSeek(xFilial("ZAI")+SB1->B1_YMARCA))

			// Se estiver calculando a partir do faturamento, analisa todas as regras para encontrar o percentual de comissao
			// Se estiver tratando de nota ja emitida, buscar na ZZG qual foi o criterio e o percentual utilizado
			// Caso o campo F2_VEND4 esteja preenchido a comissao para os representantes (F2_VEND1 e F2_VEND4) sera dividida e obedecera
			// a regra do pedido. Para os demais campos e situacoes, a comissao utilizada no calculo sera a menor entre o cadastro do vendedor,
			// do cliente, da tabela de precos, da marca, da condicao de pagamento, do produto e do digitado no pedido
			
			If SA3->A3_MSBLQL == "2"
			
				If cTipo = "F"
					// Caso tenha sido registrado o segundo vendedor
					If (nCount = 1 .or. nCount = 4) .and. !Empty(SF2->F2_VEND4)
						nPercCom := &("SC5->C5_COMIS"+Str(nCount,1))
						cCriter := "1" // COMISSAO COMPARTILHADA
					Else
						// Se for gerente buscar o percentual definido no cadastro do vendedor/representante
		/*				If nCount = 2 .and. !Empty(SF2->f2_vend1)
							nPercCom := Posicione("SA3",1,xFilial("SA3")+SF2->f2_vend1,"SA3->A3_COMIS2")
						Else
							nPercCom := SA3->a3_comis
						Endif */
						nPercCom := sc5->&("C5_COMIS"+Str(nCount,1))
						cCriter := "8"  // PEDIDO
				
		//				If !Empty(SA3->&("A3_COMIS"+Str(nCount,1))) .and. SA3->&("A3_COMIS"+Str(nCount,1)) < nPercCom 
							// Se for gerente buscar o percentual definido no campo de % gerente - Alteracao em 09/03/2020 por solicitacao de Roseli
		//					If nCount = 2
		//						nPercCom := SA3->a3_comis2
		//					Else
		//						nPercCom := SA3->a3_comis
		//					Endif
		//					cCriter := "2"  // CADASTRO VENDEDOR
		//				Endif
						If !Empty(SA1->&("A1_XCOMIS"+Str(nCount,1))) .and. SA1->&("A1_XCOMIS"+Str(nCount,1)) < nPercCom
							nPercCom := SA1->&("A1_XCOMIS"+Str(nCount,1))
							cCriter := "3"  // CADASTRO CLIENTE
						Endif
						If !Empty(DA0->&("DA0_XCOMI"+Str(nCount,1))) .and. DA0->&("DA0_XCOMI"+Str(nCount,1)) < nPercCom
							nPercCom := DA0->&("DA0_XCOMI"+Str(nCount,1))
							cCriter := "4"  // TABELA DE PRECOS
						Endif
						If !Empty(ZAI->&("ZAI_COMIS"+Str(nCount,1))) .and. ZAI->&("ZAI_COMIS"+Str(nCount,1)) < nPercCom
							nPercCom := ZAI->&("ZAI_COMIS"+Str(nCount,1))
							cCriter := "5"  // CADASTRO MARCA
						Endif
						If !Empty(SE4->&("E4_XCOMIS"+Str(nCount,1))) .and. SE4->&("E4_XCOMIS"+Str(nCount,1)) < nPercCom
							nPercCom := SE4->&("E4_XCOMIS"+Str(nCount,1))
							cCriter := "6"  // CONDICAO PAGAMENTO
						Endif
						If !Empty(SB1->&("B1_XCOMIS"+Str(nCount,1))) .and. SB1->&("B1_XCOMIS"+Str(nCount,1)) < nPercCom
							nPercCom := SB1->&("B1_XCOMIS"+Str(nCount,1))
							cCriter := "7"  // CADASTRO PRODUTO
						Endif	
					Endif
				Elseif ZZG->(MSSeek(xFilial("ZZG")+PadR(SD2->(D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM),TAMSX3("ZZG_CHVSD2")[1])+SF2->&(CVEND)))
					nPercCom := If(ZZG->ZZG_CRITER $ "18",ZZG->ZZG_PPEDID,;
								If(ZZG->ZZG_CRITER = "2",ZZG->ZZG_PVEND,;
								If(ZZG->ZZG_CRITER = "3",ZZG->ZZG_PCLIEN,;
								If(ZZG->ZZG_CRITER = "4",ZZG->ZZG_PTBPRC,;
								If(ZZG->ZZG_CRITER = "5",ZZG->ZZG_PMARCA,;
								If(ZZG->ZZG_CRITER = "6",ZZG->ZZG_PCDPAG,ZZG->ZZG_PPROD))))))
				Endif

				// array aComis: {vendedor,marca,base,percentual,comissao,dia vencto comis,fora o mes}
				//                  1        2     3       4        5           6              7
				nPos := aScan(aComis,{|x| x[1] = SF2->&CVEND .AND. X[2] = ZAI->ZAI_CODIGO})
				If nPos = 0
					aAdd(aComis,{SF2->&CVEND,ZAI->ZAI_CODIGO,0,0,0,SA3->A3_DIA,SA3->A3_DDD})
					nPos := Len(aComis)
				Endif

				// Identificar como ser� o calculo da base da comissao
				cValBase := If(cTipo="D","SD1->D1_TOTAL","SD2->D2_TOTAL")
				If SA3->A3_ICM = "N"
					cValBase += If(cTipo="D","-SD1->D1_VALICM","-SD2->D2_VALICM")
				Endif
				If SA3->A3_ICMSRET = "S"
					cValBase += If(cTipo="D","+SD1->D1_ICMSRET","+SD2->D2_ICMSRET")
				Endif
				If SA3->A3_ISS = "S"
					cValBase += If(cTipo="D","+SD1->D1_VALISS","+SD2->D2_VALISS")
				Endif
				If SA3->A3_IPI = "S"
					cValBase += If(cTipo="D","+SD1->D1_VALIPI","+SD2->D2_VALIPI")
				Endif
				If SA3->A3_FRETE = "S"
					cValBase += If(cTipo="D","+SD1->D1_VALFRE","+SD2->D2_VALFRE")
				Endif

				aComis[nPos,3] += &cValBase
		//		aComis[nPos,4] += nPercCom
				aComis[nPos,5] += If(nPercCom = 0 .or. &(cValBase) = 0,0,&(cValBase)*nPercCom/100)

				If cTipo = "F"
					
					If nCount == 1 .AND. !Empty(SF2->F2_VEND1)
						nZZGPev := SA3->A3_COMIS

					ElseIf nCount == 2 .AND. !Empty(SF2->F2_VEND2)
						nZZGPev := Posicione("SA3",1,xFilial("SA3")+SF2->F2_VEND2,"SA3->A3_COMIS2")

					EndIf
					
					// Gravar na tabela de memoria de calculo o detalhe por item
					DbSelectArea("ZZG")
					DbSetOrder(1) //ZZG_FILIAL+ZZG_CHVSD2+ZZG_VEND
					If !DbSeek(xFilial("ZZG")+PadR(SD2->D2_FILIAL+SD2->D2_DOC+SD2->D2_SERIE+SD2->D2_CLIENTE+SD2->D2_LOJA+SD2->D2_COD+SD2->D2_ITEM,50,"")+SF2->&cVend)
						RecLock("ZZG",.T.)
						ZZG->ZZG_FILIAL := xFilial("ZZG")
						ZZG->ZZG_CHVSD2 := SD2->(D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM)
						ZZG->ZZG_VEND   := SF2->&cVend
						//ZZG->zzg_pvend  := If(nCount = 2 .and. !Empty(SF2->f2_vend2),Posicione("SA3",1,xFilial("SA3")+SF2->f2_vend1,"SA3->A3_COMIS2"),SA3->a3_comis)
						ZZG->ZZG_PVEND  := nZZGPev
						ZZG->ZZG_PCLIEN := SA1->&("A1_XCOMIS"+Str(nCount,1))
						ZZG->ZZG_PTBPRC := DA0->&("DA0_XCOMI"+Str(nCount,1))
						ZZG->ZZG_PPROD  := SB1->&("B1_XCOMIS"+Str(nCount,1))
						ZZG->ZZG_PMARCA := ZAI->&("ZAI_COMIS"+Str(nCount,1))
						ZZG->ZZG_PPEDID := SC5->&("C5_COMIS"+Str(nCount,1))
						ZZG->ZZG_PCDPAG := SE4->&("E4_XCOMIS"+Str(nCount,1))
						ZZG->ZZG_CRITER := cCriter
						ZZG->(MsUnlock())
					EndIf
				ElseIf cTipo = "E" .and. ZZG->(MSSeek(xFilial("ZZG")+PadR(SD2->(D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM),TamSX3("ZZG_CHVSD2")[1])+SF2->&(cVend)))
					RecLock("ZZG",.F.)
					ZZG->(dBDelete())
					ZZG->(MsUnlock())
				Endif
			EndIf	
		Next

		(cTabela)->(dBSkip())
	Enddo

	// Tratar SE3
	For i := 1 to Len(aComis)
		// Se for comissao gerada no faturamento gravar a SE3 positiva
		// Se for exclusao de nota cuja comissao ja foi paga gravar a SE3 negativa
		If cTipo $ "FD" .or. ;
			(cTipo $ "ER" .and. SE3->(MSSeek(xFilial("SE3")+aComis[i,1]+aComis[i,2]+SE1->(E1_PREFIXO+E1_NUM+E1_TIPO+E1_CLIENTE+E1_LOJA)) .AND. ;
			!Empty(SE3->E3_DATA)))

			cBlq := POSICIONE("SA3",1,xFilial("SA3")+Alltrim(aComis[i,1]),"A3_MSBLQL")
			
			If cBlq == "2"
				If Round(aComis[i,5]/aComis[i,3]*100,2) > 0 //SC5->&("C5_COMIS"+Str(i,1)) <> 0 

					RecLock("SE3",.T.)
					SE3->E3_FILIAL  := xFilial("SE3")
					SE3->E3_VEND    := aComis[i,1]
					SE3->E3_XMARCA  := aComis[i,2]
					SE3->E3_XORIGEM := If(cTipo="F","1",If(cTipo="E","2","5"))  // 1-Vendas, 2-Canc. vendas, 5-Devolucao
					SE3->E3_XSITUAC := "S"	// Considerar para o fechamento
					SE3->E3_EMISSAO := If(cTipo $ "FE",SE1->E1_EMISSAO,dDEmissao)
					SE3->E3_NUM     := If(cTipo $ "FE",SE1->E1_NUM,cNFiscal)
					SE3->E3_SERIE   := If(cTipo $ "FE",SE1->E1_PREFIXO,cSerie)
					SE3->E3_CODCLI  := SE1->E1_CLIENTE
					SE3->E3_LOJA    := SE1->E1_LOJA
					SE3->E3_PREFIXO := If(cTipo $ "FE",SE1->E1_PREFIXO,cSerie)
					SE3->E3_TIPO    := If(cTipo $ "FE","NF","NCC")
					SE3->E3_BAIEMI  := "E"
					SE3->E3_PEDIDO  := SE1->E1_PEDIDO
					SE3->E3_ORIGEM  := If(cTipo="D","D","F")  // Devolucao ou Faturamento
					SE3->E3_VENCTO	:= U_RFATM011(aComis[i,6],aComis[i,7])
					SE3->E3_MOEDA   := Strzero(SE1->E1_MOEDA,2)
					SE3->E3_SDOC    := If(cTipo $ "FE",SE1->E1_PREFIXO,cSerie)
					SE3->E3_BASE    := If(cTipo $ "FR",aComis[i,3],-aComis[i,3])
					SE3->E3_PORC    := Round(aComis[i,5]/aComis[i,3]*100,2)
					SE3->E3_COMIS   := If(cTipo $ "FR",aComis[i,5],-aComis[i,5])
					SE3->(MsUnlock())
				
				EndIf
			EndIf
		// Se for exclusao de nota cuja comissao nao foi paga excluir a SE3
		ElseIf cTipo $ "ER" .and. SE3->(MSSeek(xFilial("SE3")+aComis[i,1]+aComis[i,2]+SE1->(E1_PREFIXO+E1_NUM+E1_TIPO+E1_CLIENTE+E1_LOJA)) .AND. ;
			Empty(SE3->E3_DATA))

			RecLock("SE3",.F.)
			SE3->(dBDelete())
			SE3->(Msunlock())
		Endif
	Next

	END TRANSACTION

RestArea(aAreaDA0)
RestArea(aAreaSA1)
RestArea(aAreaSA3)
RestArea(aAreaSB1)
RestArea(aAreaSC5)
RestArea(aAreaSD1)
RestArea(aAreaSD2)
RestArea(aAreaSE1)
RestArea(aAreaSE3)
RestArea(aAreaSE4)
RestArea(aAreaSF2)
RestArea(aAreaSZ1)
RestArea(aAreaZAI)
RestArea(aAreaZZG)
RestArea(aAreaAnt)

Return Nil
/*---------------------------------------------------------------------------*/
// Logica copiada do FINA440 para definir a data de vencimento da comissao   //
/*---------------------------------------------------------------------------*/
User Function RFATM011(cDia,cDDD)
Local dVencto := SE1->e1_emissao
Local nDia    := If(!Empty(cDia) .and. Type("cDia")="N",Val(cDia),0)
Local nMes,nAno

If nDia > 0
	dVencto := Ctod(StrZero(nDia,2)+"/"+StrZero(Month(SE1->e1_EMISSAO),2)+"/"+StrZero(year(SE1->E1_EMISSAO),4),"ddmmyy")

	While Empty(dVencto)
		nDia -= 1
		dVencto := ctod(StrZero(nDia,2)+"/"+StrZero(Month(SE1->E1_EMISSAO),2)+"/"+StrZero(year(SE1->E1_EMISSAO),4),"ddmmyy")
	EndDo
EndIf

if cDDD == "F" .or. dVencto < SE1->E1_EMISSAO		//Fora o mes
	nDia := If(!Empty(cDia) .and. Type("cDia")="N",Val(cDia),0)
	nMes := Month(dVencto)+1
	nAno := year (dVencto)
	If nMes == 13
		nMes := 01
		nAno++
	Endif
	nDia	  := StrZero(nDia,2)
	nMes	  := StrZero(nMes,2)
	nAno	  := substr(lTrim(str(nAno)),3,2)
	dVencto := CtoD(nDia+"/"+nMes+"/"+nAno,"ddmmyy")
Else
	nDia	  := StrZero(day(dVencto),2)
	nMes	  := StrZero(Month(dVencto),2)
	nAno	  := substr(lTrim(str(Year(dVencto))),3,2)
Endif

While Empty(dVencto)
	nDia := If(Valtype(nDia)=="C",Val(nDia),nDia)
	nDia -= 1
	dVencto := CtoD(StrZero(nDia,2)+"/"+nMes+"/"+nAno,"ddmmyy")
	If !Empty(dVencto)
		If dVencto < SE1->E1_EMISSAO
			dVencto += 2
		EndIf
	EndIf
Enddo

Return dVencto
/*---------------------------------------------------------------------------*/
// Funcao para criar creditos por inadimplencia para os titulos que tiveram  //
// lancamentos de debitos e foram pagos                                      //
/*---------------------------------------------------------------------------*/
User Function RFATM012()
Local aAreaAnt := GetArea()
Local aAreaSA3 := SA3->(GetArea())
Local aAreaSE3 := SE3->(GetArea())
Local nValBase := 0
Local nValCom  := 0
Local nPorc    := 0
Local cQuery   := ""
Local nReg	   := 0
Local cNewAlias  := GetNextAlias()
Local cCred 	 := GetNextAlias()

// So serao considerados os titulos gerados de notas fiscais de saida 
If SE1->e1_tipo <> "NF"
	Return Nil
Endif

SA3->(dBSetOrder(1)) // A3_FILIAL+A3_COD
SE3->(dBSetOrder(3)) // E3_FILIAL+E3_VEND+E3_CODCLI+E3_LOJA+E3_PREFIXO+E3_NUM+E3_PARCELA+E3_TIPO+E3_SEQ     

cQuery := "SELECT TOP 1 R_E_C_N_O_ FROM "
cQuery += RetSqlName("SE3")+" WITH (NOLOCK) WHERE "
cQuery += "E3_FILIAL = '"+xFilial("SE3")+"' AND E3_PREFIXO = '"+SE1->E1_PREFIXO+"' AND E3_NUM = '"+SE1->E1_NUM+"' AND E3_PARCELA = '"+SE1->E1_PARCELA+"' AND "
cQuery += "E3_CODCLI = '"+SE1->E1_CLIENTE+"' AND E3_LOJA = '"+SE1->E1_LOJA+"' AND E3_XORIGEM IN ('4') AND D_E_L_E_T_ = ' ' "
TcQuery cQuery Alias (cCred) New

nReg := (cCred)->R_E_C_N_O_ 
(cCred)->(dBCloseArea())

cQuery := "SELECT E3_VEND,E3_XMARCA,SUM(E3_COMIS) nTotal FROM "
cQuery += RetSqlName("SE3")+" WITH (NOLOCK) WHERE "
cQuery += "E3_FILIAL = '"+xFilial("SE3")+"' AND E3_PREFIXO = '"+SE1->E1_PREFIXO+"' AND E3_NUM = '"+SE1->E1_NUM+"' AND E3_PARCELA = '"+SE1->E1_PARCELA+"' AND "
cQuery += "E3_CODCLI = '"+SE1->E1_CLIENTE+"' AND E3_LOJA = '"+SE1->E1_LOJA+"' AND E3_XORIGEM IN ('3','4') AND D_E_L_E_T_ = ' ' "
cQuery += "GROUP BY E3_VEND,E3_XMARCA "
cQuery += "ORDER BY E3_VEND,E3_XMARCA"
TcQuery cQuery Alias (cNewAlias) New

BEGIN TRANSACTION

(cNewAlias)->(dBGotop())
While (cNewAlias)->(!Eof())
	If nReg == 0 .OR. nReg == Nil 
		If (cNewAlias)->nTotal < 0 .and. ;
			SA3->(MSSeek(xFilial("SA3")+(cNewAlias)->E3_VEND)) .and. ;
			SE3->(MSSeek(xFilial("SE3")+(cNewAlias)->E3_VEND+SE1->(E1_CLIENTE+E1_LOJA+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO)))

			nValBase := Abs(SE3->E3_BASE)
			nValCom  := Abs(SE3->E3_COMIS)
			nPorc    := SE3->E3_PORC
	
			If SA3->A3_MSBLQL == "2"

				RecLock("SE3",.T.)
				SE3->E3_FILIAL  := xFilial("SE3")
				SE3->E3_VEND    := (cNewAlias)->E3_VEND
				SE3->E3_XMARCA  := (cNewAlias)->E3_XMARCA
				SE3->E3_XORIGEM := "4"  // credito por inadimplencia
				SE3->E3_XSITUAC := "S"	// Considerar para o fechamento
				SE3->E3_EMISSAO := dDataBase
				SE3->E3_NUM     := SE1->E1_NUM
				SE3->E3_SERIE   := SE1->E1_PREFIXO
				SE3->E3_CODCLI  := SE1->E1_CLIENTE
				SE3->E3_LOJA    := SE1->E1_LOJA
				SE3->E3_PREFIXO := SE1->E1_PREFIXO
				SE3->E3_PARCELA := SE1->E1_PARCELA
				SE3->E3_TIPO    := SE1->E1_TIPO
				SE3->E3_BAIEMI  := "E"
				SE3->E3_PEDIDO  := SE1->E1_PEDIDO
				SE3->E3_ORIGEM  := "F"
				SE3->E3_VENCTO	:= dDataBase
				SE3->E3_MOEDA   := Strzero(SE1->E1_MOEDA,2)
				SE3->E3_SDOC    := SE1->E1_PREFIXO
				SE3->E3_BASE    := nValBase
				SE3->E3_PORC    := nPorc    //If(nValBase > 0 .and. nValCom > 0, nValCom/nValBase*100, 0)
				SE3->E3_COMIS   := nValCom
				SE3->(MsUnlock())
			EndIf
		Endif 

		// Contabilizar o credito por inadimplencia (sera feito pela contabilizacao do fechamento)
	//	U_RFATM021("C")

		(cNewAlias)->(dBSkip())
	EndIf

	(cNewAlias)->(dBSkip())

Enddo

END TRANSACTION

(cNewAlias)->(dBCloseArea())

RestArea(aAreaSA3)
RestArea(aAreaSE3)
RestArea(aAreaAnt)

Return Nil
