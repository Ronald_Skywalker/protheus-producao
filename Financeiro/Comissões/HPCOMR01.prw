#INCLUDE "Protheus.ch"
#include "Topconn.ch"
/*----------------------------------------------------------------------------*
* Extrato de comissoes                                                        *
* Autor: TOTALIT SOLUTIONS                 10/2019                            *
*-----------------------------------------------------------------------------*/
User Function HPCOMR01()
Local oReport
Private cNewAlias := GetNextAlias()

If xParamet()
	oReport := ReportDef()
	oReport:PrintDialog()
Endif

Return Nil
/*---------------------------------------------------------------------------*/
Static Function ReportDef()

Local oReport,oVend,oMarca,oOrig,oComis

//oReport := TReport():New(cReport,cTitle,uParam,bAction,cDescription,lLandscape,uTotalText,lTotalInLine,cPageTText,lPageTInLine,lTPageBreak,nColSpace)
oReport := TReport():New("HPCOMR01","Extrato de comiss�es",,{|oReport| PrintReport(oReport)},;
								 "Este relat�rio ir� imprimir o extrato de comiss�es de acordo com par�metros solicitados.",.T.,,.F.)
//oReport:lDisableOrientation := .T.  // Orienta��o (Retrato/Paisagem) nao podera ser modificada

/*oCliente := TRSection():New(oParent,cTitle,uTable,aOrder,lLoadCells,lLoadOrder,uTotalText,lTotalInLine,lHeaderPage,lHeaderBreak,lPageBreak,;
										lLineBreak,nLeftMargin,lLineStyle,nColSpace,lAutoSize,cCharSeparator,nLinesBefore,nCols,nClrBack,nClrFore,nPercentage)*/
oVend := TRSection():New(oReport,"Vendedor",{"SA3"},,,,"Total",.F.,,,.T.,,,.T.,5,.T.)
TRCell():New(oVend,"A3_COD","SA3")
TRCell():New(oVend,"A3_NOME","SA3")

//oMarca := TRSection():New(oVend,"Marca",{},,,,"Total",.F.,,,.F.,,,.F.,,.T.)
//TRCell():New(oOrig,"ZAI_MARCA","ZAI","")

oOrig := TRSection():New(oVend,"Origem",{},,,,"Total",.F.,.F.,.T.,,,,.F.,,.T.)
TRCell():New(oOrig,"E3_XORIGEM","SE3","")

oComis := TRSection():New(oOrig,"Comiss�es",{"SE3","SC5","SA1"},,,,"Total",.F.,.F.,,,,,,,.T.)

//TRCell:New(oParent,cName,cAlias,cTitle,cPicture,nSize,lPixel,bBlock,cAlign,lLineBreak,cHeaderAlign,lCellBreak,nColSpace,lAutoSize,nClrBack,nClrFore,lBold)
TRCell():New(oComis,"C5_XCODREG","SC5","Regiao")
TRCell():New(oComis,"E3_PEDIDO" ,"SE3","Pedido")
TRCell():New(oComis,"E3_NUM"    ,"SE3","Nota")
TRCell():New(oComis,"E3_PARCELA","SE3","Parc")
TRCell():New(oComis,"ZAI_MARCA" ,"ZAI","Marca")
TRCell():New(oComis,"E3_EMISSAO","SE3","Emissao")
TRCell():New(oComis,"E3_CODCLI" ,"SE3","Cliente")
TRCell():New(oComis,"A1_NOME"   ,"SA1","Razao Social")
TRCell():New(oComis,"E3_BASE"   ,"SE3","Base","@E 999,999.99",,,,,,"RIGHT")
TRCell():New(oComis,"E3_PORC"   ,"SE3","%","@E 999.99",,,,,,"RIGHT")
TRCell():New(oComis,"E3_COMIS"  ,"SE3","Comissao","@E 99,999.99",,,,,,"RIGHT")
TRCell():New(oComis,"E3_DATA"   ,"SE3","Pagto")
TRCell():New(oComis,"E3_VENCTO" ,"SE3","Vencto")

//TRFunction():New(<oCell>,<cName>,<cFunction>,<oBreak>,<cTitle>,<cPicture>,<uFormula>,<lEndSection>,<lEndReport>,<lEndPage>,<oParent>,<bCondition>,
//                 <lDisable>,<bCanPrint>) 
TRFunction():New(oComis:Cell("E3_BASE") ,NIL,"SUM"  ,,,"@E 99,999,999.99",,.T.,.F.)
TRFunction():New(oComis:Cell("E3_COMIS"),NIL,"SUM"  ,,,"@E 99,999,999.99",,.T.,.F.)

/*oCliente := TRSection():New(oParent,cTitle,uTable,aOrder,lLoadCells,lLoadOrder,uTotalText,lTotalInLine,lHeaderPage,lHeaderBreak,lPageBreak,;
										lLineBreak,nLeftMargin,lLineStyle,nColSpace,lAutoSize,cCharSeparator,nLinesBefore,nCols,nClrBack,nClrFore,nPercentage)*/

Return (oReport)
/*---------------------------------------------------------------------------*/
Static Function PrintReport(oReport)
Local cNewAlias := GetNextAlias()
Local cVend,cOrigem,cCNPJ,cMarca
Local nTotVenda := 0
Local nTotDevol := 0
Local nTotAjCre := 0
Local nTotAjDeb := 0
Local nComBase  := 0
Local nComPag   := 0

sa2->(dBSetOrder(3))  // A2_FILIAL+A2_CGC
sed->(dBSetOrder(1))  // ED_FILIAL+ED_CODIGO

oReport:Section(1):BeginQuery()
BeginSql Alias cNewAlias
	SELECT SE3.*,SA1.*,SC5.*,SA3.*,ZAI_CODIGO,ZAI_MARCA   
	FROM %table:SE3% SE3 INNER JOIN %table:SA1% SA1 ON 
	A1_FILIAL = %xFilial:SA1% AND A1_COD = E3_CODCLI AND A1_LOJA = E3_LOJA AND SA1.%notdel% 
	INNER JOIN %table:SA3% SA3 ON 
	A3_FILIAL = %xFilial:SA3% AND A3_COD = E3_VEND AND SA3.%notdel% 
	LEFT OUTER JOIN %table:SC5% SC5 ON 
	C5_FILIAL = %xFilial:SC5% AND C5_NUM = E3_PEDIDO AND SC5.%notdel%
	LEFT OUTER JOIN %table:ZAI% ZAI ON
	ZAI_FILIAL = %xFilial:ZAI% AND ZAI_CODIGO = E3_XMARCA AND ZAI.%notdel%
	WHERE E3_FILIAL = %xFilial:SE3% AND E3_VEND BETWEEN %Exp:mv_par01% AND %Exp:mv_par02% AND 
	E3_VENCTO BETWEEN %Exp:dtos(mv_par03)% AND %Exp:dtos(mv_par04)% AND 
	E3_DATA BETWEEN %Exp:dtos(mv_par05)% AND %Exp:dtos(mv_par06)% AND 
	SE3.%notdel% 
	ORDER BY E3_VEND,ZAI_CODIGO,E3_XORIGEM
EndSql
oReport:Section(1):EndQuery()
oReport:Section(1):Section(1):SetParentQuery()
oReport:Section(1):Section(1):Section(1):SetParentQuery()

oReport:SetMeter((cNewAlias)->(RecCount()))

While !oReport:Cancel() .and. (cNewAlias)->(!Eof())
	oReport:Section(1):Init()
	oReport:Section(1):PrintLine()

	If mv_par07 = 1
		oReport:Section(1):Section(1):Init()
	Endif

	cVend := (cNewAlias)->e3_vend
	cCNPJ := (cNewAlias)->a3_cgc
	While !oReport:Cancel() .and. (cNewAlias)->(!Eof() .and. e3_vend = cVend)
		nTotVenda := 0
		nTotDevol := 0
		nTotAjCre := 0
		nTotAjDeb := 0
		nComBase  := 0
		nComPag   := 0
		cMarca := (cNewAlias)->zai_marca

		While !oReport:Cancel() .and. (cNewAlias)->(!Eof() .and. e3_vend = cVend .and. zai_marca = cMarca)
			cOrigem := (cNewAlias)->e3_xorigem
	
			If mv_par07 = 1
				oReport:Section(1):Section(1):PrintLine()
				oReport:ThinLine()
				oReport:Section(1):Section(1):Section(1):Init()
			Endif
	
			While !oReport:Cancel() .and. (cNewAlias)->(!Eof() .and. e3_vend = cVend .and. e3_xorigem = cOrigem)
				oReport:IncMeter()
		
				If mv_par07 = 1
					oReport:Section(1):Section(1):Section(1):PrintLine()
				Endif
	
				// So vai para o resumo as comissoes pendentes 
//				If Empty((cNewAlias)->e3_data)
					If (cNewAlias)->e3_xorigem = "1"
						nTotVenda += (cNewAlias)->e3_base
						nComBase  += (cNewAlias)->e3_comis
						nComPag   += (cNewAlias)->e3_comis
					ElseIf (cNewAlias)->e3_xorigem = "5"
						nTotDevol += (cNewAlias)->e3_base
						nComBase  += (cNewAlias)->e3_comis
						nComPag   += (cNewAlias)->e3_comis
					ElseIf (cNewAlias)->e3_comis > 0
						nTotAjCre += (cNewAlias)->e3_comis
						nComPag   += (cNewAlias)->e3_comis
					Else
						nTotAjDeb += Abs((cNewAlias)->e3_comis)
						nComPag   += (cNewAlias)->e3_comis
					Endif
//				Endif 
		
				(cNewAlias)->(dBSkip())
			Enddo
			If mv_par07 = 1
				oReport:Section(1):Section(1):Section(1):SetTotalText( "Total de "+fOrigem(cOrigem) )
		
				oReport:Section(1):Section(1):Section(1):Finish()
				If !oReport:Cancel() .and. (cNewAlias)->(!Eof() .and. e3_vend = cVend)
					oReport:ThinLine()
				Endif
			Endif
		Enddo
		If mv_par07 = 1
			oReport:Section(1):Section(1):Finish()
		Endif
	
		// Verificar se o representante tem IRRF
		If !Empty(cCNPJ) .and. sa2->(MSSeek(xFilial("SA2")+cCNPJ) .and. a2_calcirf $ " 1" .and. !Empty(a2_naturez)) .and. ;
			sed->(MSSeek(xFilial("SED")+sa2->a2_naturez) .and. ed_calcirf = "S")
	
			nPercIRRF := sed->ed_percirf
		Endif

//		oReport:ThinLine()
		oReport:PrintText("RESUMO DE COMISS�ES A PAGAR MARCA "+Alltrim(cMarca),oReport:Row(),150)
		oReport:SkipLine()
		oReport:SkipLine()
		oReport:PrintText("VALOR TOTAL DE VENDAS: R$ "+Alltrim(Transform(nTotVenda,"@E 99,999,999.99")),oReport:Row(),10)
		oReport:SkipLine()
		oReport:PrintText("VALOR TOTAL DE DEVOLU��ES DE VENDAS: R$ "+Alltrim(Transform(nTotDevol,"@E 99,999,999.99")),oReport:Row(),10)
		oReport:SkipLine()
		oReport:PrintText("VALOR TOTAL DE AJUSTES A CR�DITO: R$ "+Alltrim(Transform(nTotAjCre,"@E 99,999,999.99")),oReport:Row(),10)
		oReport:SkipLine()
		oReport:PrintText("VALOR TOTAL DE AJUSTES A D�BITO: R$ "+Alltrim(Transform(nTotAjDeb,"@E 99,999,999.99")),oReport:Row(),10)
		oReport:SkipLine()
		oReport:PrintText("VALOR BASE COMISS�O: R$ "+Alltrim(Transform(nComBase,"@E 999,999.99"))+". UTILIZAR ESTE VALOR PARA EMISS�O DA NOTA",oReport:Row(),10)
		oReport:SkipLine()
	//	oReport:PrintText("IRRF "+Alltrim(Transform(nPercIRRF,"@E 999.99"))+"%  R$ "+Alltrim(Transform(nTotCom*nPercIRRF/100,"@E 999,999.99")),oReport:Row(),10)
	//	oReport:SkipLine()
	//	oReport:PrintText("TOTAL L�QUIDO A PAGAR: R$ "+Alltrim(Transform(nTotCom+nTotExt-nTotCom*nPercIRRF/100,"@E 999,999.99")),oReport:Row(),10)
		oReport:PrintText("TOTAL COMISS�O A PAGAR: R$ "+Alltrim(Transform(nComPag,"@E 999,999.99")),oReport:Row(),10)
		oReport:SkipLine()
	//	oReport:ThinLine()

		If mv_par07 = 1 .and. !oReport:Cancel() .and. (cNewAlias)->(!Eof() .and. e3_vend = cVend) 
			oReport:Section(1):Section(1):Init()
		Endif
	Enddo

	oReport:Section(1):Finish()
Enddo

Return Nil
/*---------------------------------------------------------------------------*/
Static Function xParamet()
Local aRet := {}
Local aParamBox := {}
Local lRet := .F.
Local aTipos := {"Anal�tico","Sint�tico"}

aParamBox := {{1,"Do vendedor"      ,Space(TamSX3("E3_VEND")[1]),""    ,"","SA3","",0,.F.},;
			  {1,"Ao vendedor"      ,Space(TamSX3("E3_VEND")[1]),""    ,"","SA3","",0,.F.},;
			  {1,"Do vencimento"    ,Ctod(Space(08))            ,""    ,"",""   ,"",0,.F.},;
			  {1,"Ao vencimento"    ,Ctod(Space(08))            ,""    ,"",""   ,"",0,.F.},;
			  {1,"Do pagamento"     ,Ctod(Space(08))            ,""    ,"",""   ,"",0,.F.},;
			  {1,"Ao pagamento"     ,Ctod(Space(08))            ,""    ,"",""   ,"",0,.F.},;
			  {2,"Tipo do relat�rio",1                          ,aTipos,50,""   ,.F.}}

lRet := ParamBox(aParamBox,"Par�metros para o extrato de comiss�es",@aRet)

Return lRet
/*---------------------------------------------------------------------------*/
Static Function fOrigem(cOrigem)
Local cTexto := ""
Local aCombo := {}
Local i := 0

aCombo := RetSX3Box(GetSX3Cache("E3_XORIGEM","X3_CBOX"),,,1)
For i := 1 to Len(aCombo)
	If aCombo[i,2] = cOrigem
		cTexto := aCombo[i,3]
	Endif
Next

Return cTexto
