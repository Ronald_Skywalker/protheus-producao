#INCLUDE "RWMAKE.CH"

/*
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
����������������������������������������������������������������������������ͻ��
���Programa  |  CNABENV     �Autor �Daniel R. Melo     ?Data ?19/10/2017   ��?
����������������������������������������������������������������������������͹��
���Desc.     ?PROGRAMA PARA VERIFICACAO E CONFIGURACAO DA GERACAO DO CNAB   ��?
����������������������������������������������������������������������������͹��
���Uso       ?Rotina incluida no X1_VALID do X1_GRUPO = AFI150.01           ��?
��?         ?Atualiza parametros do proprio AFI150                         ��?
��?         | Filtro SX1 (X1_GRUPO+X1_ORDEM $ 'AFI150    01')               ��?
����������������������������������������������������������������������������͹��
���Revisoes  | 19/10/2017 - Daniel R. Melo (Actual Trend)                    ��?
����������������������������������������������������������������������������ͼ��
��������������������������������������������������������������������������������
��������������������������������������������������������������������������������
*/

User Function CNABENV(nPar)
	Local cEnv		:= ALLTRIM(UPPER(GetEnvServer()))
	Local cArqParam	:= "            "
	Local cDate		:= alltrim(str(day(date()))) + alltrim(str(month(date()))) + SubStr(alltrim(str(year(date()))),3,2)
	Local cRet      := nPar

	Return()

	SEA->(dbSetOrder(1))

Return(.T.)


//*****************************************************
// Fun��o que gera e retorna o nosso numero.
//*****************************************************
User Function xGerNNum() //_cBanco, _cAgencia, _cConta, _cSubCta
	Local _cNumero:= ""
	Local _lAbort := .F.

	If Len(ALLTRIM(SE1->E1_NUMBCO)) <> 0
		_cNumero := SE1->E1_NUMBCO
	Else   
		dbSelectArea("SEE")
		//SEE->( dbSeek(xfilial("SEE")+_cBanco+_cAgencia+_cConta+_cSubCta) )
		If int(Val(EE_FAXFIM))-Int(Val(EE_FAXATU)) == 0
			Alert("A faixa de gera��o do nosso n�mero se esgotou. Fa�a o ajuste pelo cadastro dos paramentros do banco.")
			_lAbort := .T.
		Endif
		IF ! _lAbort
			// Trava o SEE, soma 1 na faixa atual e pega para o nosso nro.
			SEE->(RecLock("SEE",.f.))
			SEE->EE_FAXATU := alltrim(str(val(SEE->EE_FAXATU)+1))
			SEE->(MsUnlock())
			_cNumero:=Alltrim(SEE->EE_FAXATU)
			if SEE->EE_CODIGO == '341' 
		        IF 	SEE->EE_CODCART == '109'
				_cNumero:=STRZERO(VAL(SEE->EE_FAXATU),8)
				Endif
				IF 	SEE->EE_CODCART == '112'
				_cNumero:= SPACE(08) //STRZERO(VAL(SEE->EE_FAXATU),8)
				Endif
			Elseif SEE->EE_CODIGO == '001'
			     IF alltrim(SEE->EE_CODCART) == '17' 
			        _cNumero:=STRZERO(VAL(SEE->EE_FAXATU),10)                           
                 Endif
                 IF alltrim(SEE->EE_CODCART) == '11'
				    _cNumero:= SPACE(10) //STRZERO(VAL(SEE->EE_FAXATU),8)
				 Endif 
			eNDIF
			if len(allTrim(_cNumero)) <> 0 
			   SE1->(RecLock("SE1",.f.))
			   SE1->E1_NUMBCO := _cNumero
			   SEE->(MsUnlock())
			Endif   
		Endif
	Endif
Return(_cNumero)
             
User Function xGrvNNum() //_cBanco, _cAgencia, _cConta, _cSubCta
	Local _cNumero:= ""
	Local _lAbort := .F.

	If Len(ALLTRIM(SE1->E1_NUMBCO)) <> 0
		SE1->(RecLock("SE1",.f.))
		SE1->E1_NUMBCO := cNossnum 
		SE1->(MsUnlock())
    Endif 
Return()

/*
������������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������ͻ��
���Programa  �XCALC_J    �Autor  �DANIEL R. MELO      ?Data ?08/10/2014  ��?
��������������������������������������������������������������������������͹��
���Desc.     ?CALCULO DO JUROS, PARA O CNAB                               ��?
��������������������������������������������������������������������������͹��
���Uso       ?BEAUTY                                                      ��?
��������������������������������������������������������������������������ͼ��
������������������������������������������������������������������������������
������������������������������������������������������������������������������
*/
User Function XCALC_J()
nValorTit     :=SE1->E1_VALOR
nPrecentJuros := 5.00   //SEE->EE_XJUROS 
xJuros   := ROUND((nValorTit)*nPrecentJuros,2)
xJuros   := ROUND((xJuros/30),2)
Return (xJuros)

