#Include "RwMake.ch"
#Include "Protheus.ch"   
#INCLUDE "TBICONN.CH"      
#Include "topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPRevCnab  �Autor  �Marcio MEdeiros Jr  � Data �  20/10/18  ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava pedidos de venda					                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HPRevCnab

Private cMarca 		:= GetMark()  
Private aRotina		:= {}      
Private oMark
      
//1. Acionar perguntas para selecionar o arquivo de retorno e selecionar tudo que foi baixado pelo arquivo em questao
cArquivo := cGetFile( "Arq.  | *.RET" , 'Selecione arquivo', 1, 'C:\', .T., ( GETF_LOCALHARD + GETF_LOCALFLOPPY ),.F.)

If !Empty(cArquivo)
	Processa( {|| AbreRET(cArquivo) }, "Lendo o arquivo, aguarde. . ." ) 
Else
	Alert("Nenhum arquivo selecionado.")
Endif

Return

//2. Criar uma interface com MarkBrowse com os registros para se reverter (inclusive as taxas)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AbreRET    �Autor  �Marcio MEdeiros Jr  � Data �  20/10/18  ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava pedidos de venda					                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AbreRET(cArquivo)

Local oSize
Local oInverte
Private lInvSel := .F.

nHdl 	 := FT_FUSE(cArquivo)

If nHdl < 0
	Alert("N�o foi possivel abrir o arquivo, verifique se ele est� em uso por outro programa.")
	Return
Endif

If FT_FLASTREC() < 3
	Alert("Arquivo vazio ou sem registros para serem processados.")
	Return
Endif

cCabec   := FT_FREADLN() 
cIdBanco := SubStr(cCabec,77,3)
cAgencia := SubStr(cCabec,27,4)
cNomeBco := SubStr(cCabec,80,15)

If cIdBanco <> "001" .And. cIdBanco <> "341"
	Alert("Esse layout est� preparado para trabalhar com os bancos Itau e Brasil, caso necessite acrescentar outro banco, contate o administrador.")
	Return
Endif

If cIdBanco == "001"
	cDigAg := SubStr(cCabec,31,1)
	cConta := SubStr(cCabec,34,6)
	cDigCt := SubStr(cCabec,40,1) 
	dDtArq := CTOD(SubStr(cCabec,95,2) + "/" + SubStr(cCabec,97,2) + "/" + SubStr(cCabec,99,2))
Else
	cDigAg := ""          
	cConta := SubStr(cCabec,33,5)
	cDigCt := SubStr(cCabec,32,1) 
	dDtArq := CTOD(SubStr(cCabec,95,2) + "/" + SubStr(cCabec,97,2) + "/" + SubStr(cCabec,99,2))
Endif

lInverte  := .F.

aAdd(aRotina,{"Processar","u_ProcRev()" ,0,2,0,.F.})
aAdd(aRotina,{"Legenda"  ,"U_RCLegen()" ,0,2,0,.F.})   
aAdd(aRotina,{"Localizar","U_RCSearch()",0,2,0,.F.})

aCores := {}          
/*
aAdd(aCores,{'TRB->VALARQ == 0 .Or. Empty(AllTrim(TRB->NUM))','BR_PRETO'	})
aAdd(aCores,{'SubStr(TRB->STATUS,1,1)=="A"','ENABLE'	}) 
aAdd(aCores,{'SubStr(TRB->STATUS,1,1)=="B"','DISABLE'	})
aAdd(aCores,{'SubStr(TRB->STATUS,1,1)=="P"','BR_AZUL'	})
*/
aAdd(aCores,{'TRB->PROCESS == "N"','BR_PRETO'	})
aAdd(aCores,{'TRB->PROCESS == "S"','BR_AMARELO'	}) 
aAdd(aCores,{'TRB->PROCESS == "R"','ENABLE'		})
aAdd(aCores,{'TRB->PROCESS == "E"','DISABLE'	})
                                                                                                         
aCampos  := {}
aAdd(aCampos, {"OK"     ,"",""		   			,					})
aAdd(aCampos, {"STATUS" ,"","Status Titulo"		,					}) 
aAdd(aCampos, {"OCORREN","","Cod.Ocorr."		,					})	
aAdd(aCampos, {"DESCOCO","","Desc.Ocorrencia"	,					})
aAdd(aCampos, {"DTOCRED","","Data Cr�dito"		,					})
aAdd(aCampos, {"PREFIXO","","Prefixo"  			,					}) 
aAdd(aCampos, {"NUM"	,"","Titulo"  			,					}) 
aAdd(aCampos, {"PARCELA","","Parcela"  			,					}) 
aAdd(aCampos, {"CLIENTE","","Cliente"			,					}) 
aAdd(aCampos, {"LOJA"   ,"","Loja"	   			,					}) 
aAdd(aCampos, {"NOME"	,"","Raz�o Social"		,					}) 
aAdd(aCampos, {"VALTIT" ,"","Valor Titulo"		,"@E 999,999,999.99"}) 
aAdd(aCampos, {"VALARQ" ,"","Valor Arquivo"		,"@E 999,999,999.99"}) 
aAdd(aCampos, {"VALIOF" ,"","IOF"				,"@E 999,999,999.99"}) 
aAdd(aCampos, {"VALMUL" ,"","Multa"				,"@E 999,999,999.99"}) 
aAdd(aCampos, {"VALJUR" ,"","Juros"				,"@E 999,999,999.99"}) 
aAdd(aCampos, {"VENCREA","","Vencto Real"		,					})		
aAdd(aCampos, {"RESULT" ,"","Resultado do Proc.",					}) 
//aAdd(aCampos, {"PROCESS","","Process"			,					}) 

aStruTRB := {}
aAdd(aStruTRB,{"OK"     ,"C",2                      ,0						})    
aAdd(aStruTRB,{"STATUS"	,"C",7						,0						})  
aAdd(aStruTRB,{"OCORREN","C",2						,0						})  
aAdd(aStruTRB,{"DTOCRED","D",8						,0						})
aAdd(aStruTRB,{"DESCOCO","C",TamSX3("EB_DESCRI")[1]	,0						})
aAdd(aStruTRB,{"PREFIXO","C",TamSX3("E1_PREFIXO")[1],0						}) 
aAdd(aStruTRB,{"NUM"   	,"C",TamSX3("E1_NUM")[1]   	,0						}) 
aAdd(aStruTRB,{"PARCELA","C",TamSX3("E1_PARCELA")[1],0						})
aAdd(aStruTRB,{"CLIENTE","C",TamSX3("A1_COD")[1] 	,0						})	
aAdd(aStruTRB,{"LOJA"  	,"C",TamSX3("A1_LOJA")[1] 	,0 						})	
aAdd(aStruTRB,{"NOME" 	,"C",TamSX3("A1_NOME")[1] 	,0						}) 
aAdd(aStruTRB,{"VALTIT" ,"N",TamSX3("E1_VALOR")[1] 	,TamSX3("E1_VALOR")[2]	})	
aAdd(aStruTRB,{"VALARQ" ,"N",TamSX3("E1_VALOR")[1]	,TamSX3("E1_VALOR")[2]	})	
aAdd(aStruTRB,{"VALIOF" ,"N",TamSX3("E1_VALOR")[1]	,TamSX3("E1_VALOR")[2]	})	
aAdd(aStruTRB,{"VALMUL" ,"N",TamSX3("E1_VALOR")[1]	,TamSX3("E1_VALOR")[2]	})	
aAdd(aStruTRB,{"VALJUR" ,"N",TamSX3("E1_VALOR")[1]	,TamSX3("E1_VALOR")[2]	})	
aAdd(aStruTRB,{"VENCREA","D",TamSX3("E1_VENCREA")[1],0						}) 
aAdd(aStruTRB,{"RESULT" ,"C",250					,0						})
  
aAdd(aStruTRB,{"BANCO" 	,"C",TamSX3("A6_COD")[1]	,0						})
aAdd(aStruTRB,{"AGENCIA","C",TamSX3("A6_AGENCIA")[1],0						})  
aAdd(aStruTRB,{"DVAGE"	,"C",TamSX3("A6_DVAGE")[1]	,0						})
aAdd(aStruTRB,{"CONTA" 	,"C",TamSX3("A6_NUMCON")[1]	,0						})
aAdd(aStruTRB,{"DVCTA"	,"C",TamSX3("A6_DVCTA")[1]	,0						}) 
aAdd(aStruTRB,{"PROCESS","C",1						,0						}) 


cArqTRB := CriaTrab( aStruTRB, .T. )
	
If Select("TRB") > 0
	TRB->(DbCloseArea())
Endif			        

DbUseArea(.T.,"DBFCDX",cArqTRB,"TRB",.F.,.F.)  

_cIndex:=Criatrab(Nil,.F.)
_cChave:='PREFIXO+NUM+PARCELA+CLIENTE+LOJA+OCORREN'
Indregua("TRB",_cIndex,_cChave,,,"Ordenando registros selecionados...")
DbSetIndex(_cIndex+ordbagext())
Dbselectarea("TRB")
SysRefresh()    

DbSelectArea("SE1")     
DbSelectArea("SE5")
DbSelectArea("SEB")
DbSelectArea("SA1")

nTotReg := 0
nTotOk  := 0
nTotBlq := 0
   
While !FT_FEOF()
	cLinha	 := FT_FREADLN() 
    cIdLinha := SubStr(cLinha,1,1) 
    If cIdLinha == "1" // Detalhes     
    
        //Validacao de processamento:
        //	1. Se encontrar o titulo 							- OK
        // 	2. Se o valor do arquivo for > zero					- OK
        //	3. Se o valor do arquivo for >= ao valor do saldo	- OK
        //	4. Banco nao estiver conciliado	   
    
    	SE1->(DbSetOrder(16)) //E1_FILIAL + E1_IDCNAB
    	cIdCNAB := SubStr(cLinha,38,10)
    	cCodOco := SubStr(cLinha,109,2)
    	nValLiq := Val(SubStr(cLinha,254,13))/100
    	
		cE1PREFIXO	:= ""
		cE1NUM   	:= ""
		cE1PARCELA	:= ""
		cE1CLIENTE	:= ""
		cE1TIPO		:= ""
		cE1LOJA  	:= ""
		cE1NOMCLI 	:= ""
		nE1VALOR 	:= 0      
		nE1SALDO	:= 0
		dE1VENCREA	:= STOD("") 
		cProcess	:= "N"
       	cResult		:= "Titulo n�o encontrado!"
       	
    	If SE1->(DbSeek( xFilial("SE1") + cIdCNAB )) 

	        cProcess	:= "S" 
	        cResult		:= ""

			cE1PREFIXO	:= SE1->E1_PREFIXO
			cE1NUM   	:= SE1->E1_NUM
			cE1PARCELA	:= SE1->E1_PARCELA
			cE1TIPO		:= SE1->E1_TIPO
			cE1CLIENTE	:= SE1->E1_CLIENTE
			cE1LOJA  	:= SE1->E1_LOJA
			cE1NOMCLI 	:= SE1->E1_NOMCLI
			nE1VALOR 	:= SE1->E1_VALOR
			dE1VENCREA	:= SE1->E1_VENCREA
			nE1SALDO	:= SE1->E1_SALDO
        
	        If SE1->E1_SALDO > 0 .And. SE1->E1_VALOR <> SE1->E1_SALDO  
	        	cStatus := "Parcial"
	        ElseIf SE1->E1_SALDO == 0
	        	cStatus 	:= "Baixado" 
	        Else
	        	cStatus := "Aberto"
		        cProcess	:= "N" 
		        cResult		:= "Titulo aberto, n�o h� baixa a ser cancelada."
	        Endif
				        
	  		If nValLiq == 0
            	cProcess	:= "N" 
            	cResult		:= "Valor do arquivo igual a zero"	
        	Endif
	    Endif

		cDescOco := ""
    	SEB->(DbSetOrder(1))
    	If SEB->( DbSeek( xFilial("SEB") + cIdBanco + PADR(cCodOco,3) + "R" ) )
    		cDescOco := SEB->EB_DESCRI	
    	Endif
    	
    	If cIdBanco == "001"
    		nValIOF := Val(SubStr(cLinha,182,7))/100  
    		nValJur	:= Val(SubStr(cLinha,267,6))/100
    		nValMul	:= 0 
    		If SubStr(cLinha,176,6) <> "000000"
    			dDtCred := CTOD(SubStr(cLinha,176,2) + "/" + SubStr(cLinha,178,2) + "/" + SubStr(cLinha,180,2))      
    		Else
    		    dDtCred := dDtArq
    		Endif

    		If cCodOco $ "96/98" //Custas de cart�rio 
    			nValLiq := Val(SubStr(cLinha,189,13))/100
		  		If nValLiq > 0 .And. cProcess == "S"
	            	cProcess	:= "S" 
	            	cResult		:= ""	
	        	Endif
    		ElseIf cCodOco $ "12/14/28"
    			nValLiq	:= nValIOF 
    			nValIOF := 0        
		  		If nValLiq > 0 .And. cProcess == "S"
	            	cProcess	:= "S" 
	            	cResult		:= ""	
	        	Endif
    		Endif
    	Else 
    		nValIOF := Val(SubStr(cLinha,215,13))/100
    		nValJur	:= Val(SubStr(cLinha,267,13))/100   
    		nValMul	:= Val(SubStr(cLinha,280,13))/100                                                       
    		If !Empty(AllTrim(SubStr(cLinha,296,6)))
    			dDtCred := CTOD(SubStr(cLinha,296,2) + "/" + SubStr(cLinha,298,2) + "/" + SubStr(cLinha,300,2)) 
    		Else
    		    dDtCred := dDtArq
    		Endif
    		If cCodOco == "35" //Custas de cart�rio 
    			nValLiq := Val(SubStr(cLinha,176,13))/100 
		  		If nValLiq > 0 .And. cProcess == "S"
	            	cProcess	:= "S" 
	            	cResult		:= ""	
	        	Endif
    		Endif
    	Endif

        SE5->(DbSetOrder(3))	//E5_FILIAL+E5_BANCO+E5_AGENCIA+E5_CONTA+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+DtoS(E5_DATA)
		If SE5->( DbSeek(xFilial("SE5") + cIdBanco + PADR(cAgencia,5) + PADR(cConta,10) + cE1PREFIXO + cE1NUM + cE1PARCELA + cE1TIPO + DTOS(dDtCred)) )  
        	If !Empty(AllTrim(SE5->E5_RECONC))
            	cProcess	:= "N" 
            	cResult		:= "Baixa ja reconciliada. Cancele a concilia��o bancaria."	        	
        	Endif
    	Else
    		If !Empty(cE1NUM)
	           	cProcess	:= "N" 
	           	cResult		:= "Baixa n�o localizada ou ja revertida."	
           	Endif       		
    	Endif
    	
    	RecLock("TRB",.T.)
			TRB->OK			:= If(cProcess == "S",cMarca,"")//If(SubStr(cStatus,1,1)<>"A" .And. nValLiq > 0 .And. !Empty(AllTrim(SE1->E1_NUM)) ,cMarca,"")    
			TRB->STATUS		:= cStatus
			TRB->PREFIXO	:= cE1PREFIXO	//SE1->E1_PREFIXO
			TRB->NUM   		:= cE1NUM		//SE1->E1_NUM
			TRB->PARCELA	:= cE1PARCELA	//SE1->E1_PARCELA
			TRB->CLIENTE	:= cE1CLIENTE	//SE1->E1_CLIENTE
			TRB->LOJA  		:= cE1LOJA		//SE1->E1_LOJA
			TRB->NOME 		:= cE1NOMCLI	//SE1->E1_NOMCLI
			TRB->VALTIT 	:= nE1VALOR		//SE1->E1_VALOR
			TRB->VALARQ 	:= nValLiq
			TRB->VALIOF 	:= nValIOF
			TRB->VALMUL 	:= nValMul
			TRB->VALJUR 	:= nValJur
			TRB->VENCREA	:= dE1VENCREA	//SE1->E1_VENCREA
			TRB->OCORREN	:= cCodOco
			TRB->DESCOCO	:= cDescOco
			TRB->RESULT    	:= cResult		//""    
			TRB->DTOCRED    := dDtCred         
			TRB->BANCO		:= cIdBanco
			TRB->AGENCIA	:= cAgencia
			TRB->DVAGE		:= cDigAg
			TRB->CONTA		:= cConta
			TRB->DVCTA		:= cDigCt   
			TRB->PROCESS	:= cProcess
    	MsUnlock()
	    nTotReg ++
	    If cProcess == "S"
	    	nTotOk ++
	    Else
	    	nTotBlq ++
	    Endif
    Endif

	FT_FSKIP()
EndDo
FT_FUSE() 
TRB->(DbGoTop())
//MarkBrowse( "TRB", "OK",,aCampos,lInverte, cMarca,"u_RMAll()",.T.,,,"u_RClick()",,,, aCores )	
bOK 	  := {||u_ProcRev()}
bCancel   := {||oDlg:End()}  
aBotAdc   := { { "HISTORIC", { || U_RCLegen() }, "Legenda" },;
			   { "HISTORIC", { || U_RCSearch() }, "Localizar" } }  
oSize := FwDefSize():New( .T. )

oSize:AddObject( "ENCHOICE" , 100, 090, .T., .T. ) // Adiciona enchoice   
oSize:AddObject( "CABECALHO", 010, 010, .T., .T. ) // Adiciona Folder       

oSize:lProp 	:= .T. // Proporcional    
oSize:Process()

oFontR := TFont():New('Arial',,-18,,.T.)     
oFont  := TFont():New('Arial',,-12,,.T.)

Define MSDialog oDlg Title "Revers�o de baixa - CNAB" From oSize:aWindSize[1],oSize:aWindSize[2] To oSize:aWindSize[3],oSize:aWindSize[4] of oMainWnd Pixel   
	
	oMark := MSSelect():New("TRB","OK","",aCampos,@lInverte,@cMarca,{oSize:GetDimension("ENCHOICE","LININI"),oSize:GetDimension("ENCHOICE","COLINI"),oSize:GetDimension("ENCHOICE","LINEND"),oSize:GetDimension("ENCHOICE","COLEND")},,,oDlg,,aCores) 
	oMark:bMark := {|| RClick()}  
	oMark:oBrowse:lHasMark    :=.T.
	oMark:oBrowse:lCanAllMark :=.T.
	oMark:oBrowse:bAllMark    := {|| RMAll()}
    
	tSay():New(oSize:GetDimension("CABECALHO","LININI") ,oSize:GetDimension("CABECALHO","COLINI")+10,{|| "Resumo" },oDlg,,oFontR,,,,.T.,,,110,20)

	tSay():New(oSize:GetDimension("CABECALHO","LININI")+03 ,oSize:GetDimension("CABECALHO","COLINI")+120,{|| "Aquivo: " },oDlg,,oFont,,,,.T.,,,210,120)
	tSay():New(oSize:GetDimension("CABECALHO","LININI")+03 ,oSize:GetDimension("CABECALHO","COLINI")+150,{|| cArquivo },oDlg,,,,,,.T.,,,210,20)

	tSay():New(oSize:GetDimension("CABECALHO","LININI")+03 ,oSize:GetDimension("CABECALHO","COLINI")+420,{|| "Banco: " },oDlg,,oFont,,,,.T.,,,110,20)
	tSay():New(oSize:GetDimension("CABECALHO","LININI")+13 ,oSize:GetDimension("CABECALHO","COLINI")+420,{|| "Agencia: " },oDlg,,oFont,,,,.T.,,,110,20) 
	tSay():New(oSize:GetDimension("CABECALHO","LININI")+23 ,oSize:GetDimension("CABECALHO","COLINI")+420,{|| "Conta: " },oDlg,,oFont,,,,.T.,,,110,20)

	tSay():New(oSize:GetDimension("CABECALHO","LININI")+03 ,oSize:GetDimension("CABECALHO","COLINI")+450,{|| cIdBanco + " " + cNomeBco},oDlg,,,,,,.T.,,,210,20)
	tSay():New(oSize:GetDimension("CABECALHO","LININI")+13 ,oSize:GetDimension("CABECALHO","COLINI")+450,{|| cAgencia + "-" + cDigAg },oDlg,,,,,,.T.,,,110,20) 
	tSay():New(oSize:GetDimension("CABECALHO","LININI")+23 ,oSize:GetDimension("CABECALHO","COLINI")+450,{|| cConta + "-" + cDigCt },oDlg,,,,,,.T.,,,110,20)

	tSay():New(oSize:GetDimension("CABECALHO","LININI")+03 ,oSize:GetDimension("CABECALHO","COLINI")+620,{|| "Total Registros: " },oDlg,,oFont,,,,.T.,,,110,20)
	tSay():New(oSize:GetDimension("CABECALHO","LININI")+13 ,oSize:GetDimension("CABECALHO","COLINI")+620,{|| "Registros Aptos: " },oDlg,,oFont,,,,.T.,,,110,20) 
	tSay():New(oSize:GetDimension("CABECALHO","LININI")+23 ,oSize:GetDimension("CABECALHO","COLINI")+620,{|| "Registros Bloq.: " },oDlg,,oFont,,,,.T.,,,110,20)

	tSay():New(oSize:GetDimension("CABECALHO","LININI")+03 ,oSize:GetDimension("CABECALHO","COLINI")+670,{|| nTotReg },oDlg,,,,,,.T.,,,110,20)
	tSay():New(oSize:GetDimension("CABECALHO","LININI")+13 ,oSize:GetDimension("CABECALHO","COLINI")+670,{|| nTotOk },oDlg,,,,,,.T.,,,110,20) 
	tSay():New(oSize:GetDimension("CABECALHO","LININI")+23 ,oSize:GetDimension("CABECALHO","COLINI")+670,{|| nTotBlq },oDlg,,,,,,.T.,,,110,20)

	
ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg, bOK, bCancel,,aBotAdc,,,,,.F.) CENTERED 

Return

//3. Criar um log para registrar tudo o que foi revertido

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ProcRev    �Autor  �Marcio MEdeiros Jr  � Data �  20/10/18  ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava pedidos de venda					                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function ProcRev    
Processa( {|| EfetRev()},"Executando revers�o. . ." ) 
Return

Static Function EfetRev    
        
DbSelectArea("SE1")
SE1->(DbSetOrder(1))

DbSelectArea("SE5")

TRB->(DbGoTop())
While TRB->(!EoF())
	If !Empty(AllTrim(TRB->OK)) 
		
		If SE1->(DbSeek( xFilial("SE1") + TRB->PREFIXO + TRB->NUM + TRB->PARCELA ))
        	                                                                              
	        SE5->(DbSetOrder(3))	//E5_FILIAL+E5_BANCO+E5_AGENCIA+E5_CONTA+E5_PREFIXO+E5_NUMERO+E5_PARCELA+E5_TIPO+DtoS(E5_DATA)
			If SE5->( DbSeek(xFilial("SE5") + TRB->BANCO + TRB->AGENCIA + TRB->CONTA + TRB->PREFIXO + TRB->NUM + TRB->PARCELA + SE1->E1_TIPO + DTOS(TRB->DTOCRED)) )  
	        	
	        	While SE5->(!EoF()) .And. SE5->E5_FILIAL + SE5->E5_BANCO + SE5->E5_AGENCIA + SE5->E5_CONTA + SE5->E5_PREFIXO + SE5->E5_NUMERO + SE5->E5_PARCELA + SE5->E5_TIPO + DtoS(SE5->E5_DATA) ==;
	        	 						  xFilial("SE5") + TRB->BANCO    + TRB->AGENCIA    + TRB->CONTA    + TRB->PREFIXO    + TRB->NUM       + TRB->PARCELA    + SE1->E1_TIPO + DTOS(TRB->DTOCRED)
	        	    //Comparar o valor do arquivo com o valor d SE5 para ver qual a baixa deve ser considerada
	        		
	        		If SE5->E5_VALOR == TRB->VALARQ  
	        			If TRB->OCORREN <> "06"
		        			Begin Transaction
		        			Reclock("SE5",.F.)
		        				DbDelete()
		        			MsUnlock()      
		        			
						    Reclock("TRB",.F.)
						    TRB->OK 		:= ""
						    TRB->PROCESS 	:= "R"
						    TRB->RESULT 	:= "Revertido com sucesso"
						    MsUnlock()	        			   
						    
		        			End Transaction    
		        		Endif
	        			Exit	
	        		Endif
	        		SE5->(DbSkip())
	        	EndDo
	        	
			Endif
			
			If TRB->OCORREN == "06"
				_aCabec      := {}
		  		Aadd(_aCabec, {"E1_PREFIXO"    	, SE1->E1_PREFIXO   , nil})
		    	Aadd(_aCabec, {"E1_NUM"        	, SE1->E1_NUM       , nil})
		     	Aadd(_aCabec, {"E1_PARCELA"    	, SE1->E1_PARCELA   , nil})
		      	Aadd(_aCabec, {"E1_TIPO"		, SE1->E1_TIPO      , nil})
		       	Aadd(_aCabec, {"E1_CLIENTE"    	, SE1->E1_CLIENTE   , nil})
		        Aadd(_aCabec, {"E1_LOJA"       	, SE1->E1_LOJA		, nil})
		        
		        lMsErroAuto := .F.
		        
			//	MSExecAuto({|x, y| FINA070(x,y)}, _aCabec, 5) //Cancelamento baixa
				MSExecAuto({|x, y| FINA070(x,y)}, _aCabec, 6) //Exclus�o baixa
				
				cErroRev := ""
				
				If  lMsErroAuto
					cErroRev 		:= MOSTRAERRO() // Sempre que o micro comeca a apitar esta ocorrendo um erro desta forma          
				    Reclock("TRB",.F.)
				    TRB->PROCESS 	:= "E"
				    TRB->RESULT 	:= cErroRev//"Erro na revers�o"
				    MsUnlock()
				Else
				    Reclock("TRB",.F.)
				    TRB->OK    		:= ""                        
				    TRB->PROCESS 	:= "R"
				    TRB->RESULT 	:= "Revertido com sucesso"
				    MsUnlock()
				Endif
			Endif
		Else
		    Reclock("TRB",.F.)
		    TRB->PROCESS 	:= "E"
		    TRB->RESULT 	:= "Titulo n�o encontrado"
		    MsUnlock()					
		Endif
	Endif
	TRB->(DbSkip())
EndDo
TRB->(DbGoTop())  
oMark:oBrowse:Refresh()
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RecClick   �Autor  �Marcio MEdeiros Jr  � Data �  23/01/17  ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao para marcar/ desmarcar um item                       ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RClick()
If TRB->PROCESS == "S" //SubStr(TRB->STATUS,1,1)<>"A" .And. TRB->VALARQ > 0 .And. !Empty(AllTrim(SE1->E1_NUM))  
	If Marked("OK")	
		RecLock("TRB",.F.)
			TRB->OK := cMarca
		MsUnlock()
	Else
		RecLock("TRB",.F.)
			TRB->OK := ""
		MsUnlock()	
	Endif
Else
	RecLock("TRB",.F.)
		TRB->OK := ""
	MsUnlock()	
Endif
oMark:oBrowse:Refresh()
Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RecMarkAll�Autor  �Marcio MEdeiros Jr  � Data �  23/01/17  ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao para marcar/desmarcar todos os itens.                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function RMAll(lMarkAll)
Local aArea	:= GetArea()

TRB->( DbGoTop() )
While TRB->( !Eof() )
           
If TRB->PROCESS == "S" //SubStr(TRB->STATUS,1,1)<>"A" .And. TRB->VALARQ > 0 .And. !Empty(AllTrim(SE1->E1_NUM))  
	If Empty(TRB->OK)
		RecLock("TRB",.F.)
			TRB->OK := cMarca
		MsUnlock()
	Else
		RecLock("TRB",.F.)
			TRB->OK := ""
		MsUnlock()	
	Endif
Else
	RecLock("TRB",.F.)
		TRB->OK := ""
	MsUnlock()	
Endif

	TRB->( DbSkip() )
EndDo
oMark:oBrowse:Refresh()
RestArea( aArea )
Return                                         

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RCLegen    �Autor  �Marcio MEdeiros Jr  � Data �  20/10/18  ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava pedidos de venda					                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function RCLegen 

Local aCores := {}                                                              

aAdd(aCores,{'BR_PRETO','Erro ao carregar registro'	})
aAdd(aCores,{'BR_AMARELO','Apto a ser processado'	}) 
aAdd(aCores,{'ENABLE','Estornado com sucesso'		})
aAdd(aCores,{'DISABLE','Erro ao tentar estornar'	})
BrwLegenda("Revers�o de baixa por CNAB","Legenda",aCores)
Return                                                                                     

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RCSearch   �Autor  �Marcio MEdeiros Jr  � Data �  20/10/18  ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava pedidos de venda					                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function RCSearch 

Local olDlgPesq	:= Nil
Local olBtnC	:= Nil
Local olBtnS	:= Nil
Local cDoc		:= Space(09)
Local cPref		:= Space(03) 
Local cParc		:= Space(03)
		
DEFINE MSDIALOG olDlgPesq TITLE "Pesquisa" FROM 0,0 TO 170,305 PIXEL OF GetWndDefault()

@ 010,008 SAY "Prefixo: " PIXEL OF olDlgPesq
@ 010,050 MSGET cPref SIZE 10,10 PIXEL OF olDlgPesq 
@ 025,008 SAY "N� Titulo: " PIXEL OF olDlgPesq
@ 025,050 MSGET cDoc SIZE 30,10 PIXEL OF olDlgPesq
@ 040,008 SAY "Parcela: " PIXEL OF olDlgPesq
@ 040,050 MSGET cParc SIZE 10,10 PIXEL OF olDlgPesq
			
@ 060,035 BUTTON olBtnC PROMPT "&Pesquisar" SIZE 38,11 PIXEL ACTION (FPesq(cPref,cDoc,cParc),olDlgPesq:End())
@ 060,077 BUTTON olBtnS PROMPT "&Sair" SIZE 38,11 PIXEL ACTION olDlgPesq:End()
		
ACTIVATE DIALOG olDlgPesq CENTERED
Return 

Static Function fPesq(cPref,cDoc,cParc)  
If !Empty(cPref+cDoc+cParc)
	DbSelectArea("TRB")
	TRB->(DbSetOrder(1))
	If TRB->( DbSeek( cPref+cDoc+cParc, .T. ) ) 
		oMark:oBrowse:Refresh()
	Endif   
Endif
Return