#Include "PROTHEUS.CH"
#INCLUDE "FWPRINTSETUP.CH"
#Include "RPTDEF.CH"
#Include "TOPCONN.CH"
#Include "TBICONN.CH"

//Constantes
#Define NXTLINE		Chr(13)+Chr(10)

/*/{Protheus.doc} HPFTREL01
Relatorio de Contas a Receber por Vendedor/Macro-Regi�o
@type  User Function
@author R. Melo
@since 04/05/2021
@version version 1.0
/*/
User Function HPFTREL01()

	Local aArea   := GetArea()
	Local oReport
	Local lEmail  := .F.
	Local cPara   := ""
	Private cPerg := ""
	
	//Defini��es da pergunta
	cPerg := "HPFTREL01"   
	/*-------------------------------*
	 | Macro Regi�o?  = MV_PAR01  |
	 | Representante? = MV_PAR02  |
	 | Emiss�o de?    = MV_PAR03  |
	 | Emissao ate?   = MV_PAR04  | 
	 | Vencto de?     = MV_PAR05  |
	 | Vencto ate?    = MV_PAR06  |                     
	 *--------------------------*/

	Pergunte(cPerg, .T.)
	
	//Cria as defini��es do relat�rio
	oReport := fReportDef()
	
	//Ser� enviado por e-Mail?
	If lEmail
		oReport:nRemoteType := NO_REMOTE
		oReport:cEmail := cPara
		oReport:nDevice := 1 
		oReport:SetPreview(.F.)
		oReport:Print(.F., "", .T.)
	Else
		oReport:PrintDialog()
	EndIf
	
	RestArea(aArea)
Return
	
/*-------------------------------------------------------------------------------*
 | Func:  fReportDef                                                             |
 | Desc:  Fun��o que monta a defini��o do relat�rio                              |
 *-------------------------------------------------------------------------------*/
	
Static Function fReportDef()
	Local oReport
	Local oSectDad := Nil
	Local oBreak   := Nil
	Local oFunTot1 := Nil
	
	//Cria��o do componente de impress�o
	oReport := TReport():New(	"RecMacro",;	   				//Nome do Relat�rio
								"Cta Receber Macro Reg",;		//T�tulo
								cPerg,;							//Pergunte ... Se eu defino a pergunta aqui, ser� impresso uma p�gina com os par�metros, conforme privil�gio 101
								{|oReport| fRepPrint(oReport)},;//Bloco de c�digo que ser� executado na confirma��o da impress�o
								)	  							//Descri��o
	oReport:SetTotalInLine(.F.)
	oReport:lParamPage := .T.
	oReport:oPage:SetPaperSize(9) //Folha A4
	oReport:SetPortrait()
	
	//Criando a se��o de dados
	oSectDad := TRSection():New(	oReport,;			//Objeto TReport que a se��o pertence
									"Totais",;	   		//Descri��o da se��o
									{"QRY_AUX"})		//Tabelas utilizadas, a primeira ser� considerada como principal da se��o
	oSectDad:SetTotalInLine(.F.)  //Define se os totalizadores ser�o impressos em linha ou coluna. .F.=Coluna; .T.=Linha
	
	//Colunas do relat�rio
	TRCell():New(oSectDad, "E1_CLIENTE", "QRY_AUX", "Cod.Cli",    /*cPicture*/, 8, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	TRCell():New(oSectDad, "E1_NOMCLI" , "QRY_AUX", "Nome Cliente",    /*cPicture*/, 30, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	TRCell():New(oSectDad, "E1_NUM"    , "QRY_AUX", "T�tulo",      /*cPicture*/, 11, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	TRCell():New(oSectDad, "E1_PARCELA", "QRY_AUX", "Parcela",     /*cPicture*/, 3, /*lPixel*/,/*{|| code-block de impressao }*/,'CENTER',/*lLineBreak*/,'CENTER' ,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	TRCell():New(oSectDad, "E5_TIPODOC", "QRY_AUX", "Tipo",       /*cPicture*/, 3, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	TRCell():New(oSectDad, "E1_VALOR"  , "QRY_AUX", "Valor",       /*cPicture*/, 16, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	TRCell():New(oSectDad, "E5_HISTOR" , "QRY_AUX", "Hist�rico",       /*cPicture*/, 30, /*lPixel*/,/*{|| code-block de impressao }*/,'CENTER',/*lLineBreak*/,'CENTER',/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	
	TRCell():New(oSectDad, "E1_EMISSAO", "QRY_AUX", "Emiss�o",      /*cPicture*/, 10, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	TRCell():New(oSectDad, "E1_VENCREA", "QRY_AUX", "Vencimento",   /*cPicture*/, 10, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	TRCell():New(oSectDad, "E5_DATA"   , "QRY_AUX", "Dt.Pgto",/*cPicture*/, 8, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	TRCell():New(oSectDad, "E5_VALOR"  , "QRY_AUX", "Valor Pago",   /*cPicture*/, 16, /*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign */,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/,/*lBold*/)
	
	//Definindo a quebra
	oBreak := TRBreak():New(oSectDad,{|| QRY_AUX->ZA4_REPRE },{|| "Total Por Vendedor" })
	oSectDad:SetHeaderBreak(.T.)
	
	//Totalizadores
	oFunTot1 := TRFunction():New(oSectDad:Cell("E5_VALOR"),,"SUM",oBreak,,/*cPicture*/)
	oFunTot1:SetEndReport(.F.)

Return oReport
	
/*-------------------------------------------------------------------------------*
 | Func:  fRepPrint                                                              |
 | Desc:  Fun��o que imprime o relat�rio                                         |
 *-------------------------------------------------------------------------------*/
	
Static Function fRepPrint(oReport)
	Local aArea    := GetArea()
	Local cQryAux  := ""
	Local oSectDad := Nil
	Local nAtual   := 0
	Local nTotal   := 0
		
	//Pegando as se��es do relat�rio
	oSectDad := oReport:Section(1)

	//Montando consulta de dados
	cQryAux := ""
	cQryAux += "SELECT E1_CLIENTE, E1_NOMCLI, E1_NUM, E1_PARCELA, E1_EMISSAO, E1_VENCTO,E1_VENCREA," + NXTLINE
	cQryAux += " E5_HISTOR, E5_DATA, E5_VALOR, E5_VALOR, E1_VALOR, ZA4_REPRE, E5_TIPODOC "     		 + NXTLINE
	cQryAux += "FROM SE1010 SE1 " 														  			 + NXTLINE

	cQryAux += " LEFT JOIN SE5010 SE5 ON E1_PREFIXO = E5_PREFIXO " 				        		     + NXTLINE
	cQryAux += " AND E5_NUMERO = E1_NUM  " 															 + NXTLINE
	cQryAux += " AND E1_PARCELA = E5_PARCELA  " 									  				 + NXTLINE
	cQryAux += " AND E5_TIPO = E1_TIPO  " 										  					 + NXTLINE
	cQryAux += " AND E5_CLIFOR = E1_CLIENTE  " 									  					 + NXTLINE
	cQryAux += " AND E5_LOJA = E1_LOJA " 															 + NXTLINE
	cQryAux += " AND SE5.D_E_L_E_T_ = ' ' " 									     				 + NXTLINE
	cQryAux += " AND  E5_TIPODOC IN ('NF','VL','RA')  " 									     	 + NXTLINE
	cQryAux += " INNER JOIN SA1010 SA1 ON A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA" 	 			 + NXTLINE
	cQryAux += " AND SA1.D_E_L_E_T_ = ' '      "                                  					 + NXTLINE
	cQryAux += " INNER JOIN ZA4010 ZA4 ON A1_XMICRRE = ZA4_CODMAC " 					 			 + NXTLINE
	cQryAux += " AND  ZA4.D_E_L_E_T_ = ' '" 					     				  				 + NXTLINE

	If MV_PAR07 == 1	
		cQryAux += " AND ZA4_CODMAC = '"+MV_PAR01+"' " 									 			 + NXTLINE
	Else
		cQryAux += " AND ZA4_REPRE  = '"+MV_PAR02+"' " 					       	          			 + NXTLINE
		cQryAux += " INNER JOIN SA3010 SA3 ON A3_COD = ZA4_REPRE" 							  		 + NXTLINE
		cQryAux += " AND  SA3.D_E_L_E_T_ = ' '" 					     				 			 + NXTLINE
	Endif
	
	cQryAux += "WHERE  SE1.D_E_L_E_T_ = ' '      " 									     			 + NXTLINE
	cQryAux += " AND  E1_TIPO IN ('NF','RA')    " 									      			 + NXTLINE
	cQryAux += " AND E1_EMISSAO BETWEEN '"+DTOS(MV_PAR03)+"' AND '"+DTOS(MV_PAR04)+"' "  			 + NXTLINE
	cQryAux += " AND E1_VENCREA BETWEEN '"+DTOS(MV_PAR05)+"' AND '"+DTOS(MV_PAR06)+"' "  			 + NXTLINE
	cQryAux += "ORDER BY E1_NOMCLI, E1_NUM, E1_PARCELA"		
	cQryAux := ChangeQuery(cQryAux)
	
	//Executando consulta e setando o total da r�gua
	TCQuery cQryAux New Alias "QRY_AUX"
	Count to nTotal
	oReport:SetMeter(nTotal)

	TCSetField("QRY_AUX", "E1_EMISSAO", "D")
	TCSetField("QRY_AUX", "E1_VENCTO ", "D")
	TCSetField("QRY_AUX", "E1_VENCREA", "D")
	TCSetField("QRY_AUX", "E5_DATA"   , "D")
	
	//Enquanto houver dados
	oSectDad:Init()

	QRY_AUX->(DbGoTop())

	While ! QRY_AUX->(Eof())
		//Incrementando a r�gua
		nAtual++
		oReport:SetMsgPrint("Imprimindo registro "+cValToChar(nAtual)+" de "+cValToChar(nTotal)+"...")
		oReport:IncMeter()
		
		//Imprimindo a linha atual
		oSectDad:PrintLine()
		
		QRY_AUX->(DbSkip())

	EndDo

	oSectDad:Finish()
	QRY_AUX->(DbCloseArea())
	
	RestArea(aArea)
Return
