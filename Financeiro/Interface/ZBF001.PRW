#Include "Protheus.ch"
#Include "TopConn.ch"
#Include "ApWizard.ch"

/*/{Protheus.doc} ZBF001

	Wizard de inclusao de devolucao

	@author Milton J.dos Santos
	@since  03/05/2021
	@version 

	Caminho: FINANCEIRO > Atualizacoes > Especificos Hope > Creditos do Cliente

/*/

User Function ZBF001
Local oWizard                                                                           
Local oPanel
Local nTam

Local nTamDocE	:= TamSX3("F1_DOC")		[1]
Local nTamSerE	:= TamSX3("F1_SERIE")	[1]
Local nTamForE	:= TamSX3("F1_FORNECE")	[1]
Local nTamLojE	:= TamSX3("F1_LOJA")	[1]

Local nTamDocS	:= TamSX3("F2_DOC")		[1]
Local nTamSerS	:= TamSX3("F2_SERIE")	[1]
Local nTamForS	:= TamSX3("F2_CLIENTE")	[1]
Local nTamLojS	:= TamSX3("F2_LOJA")	[1]

Local oOrigem
Local aOrigem	:= {}
Local aParIni	:= NIL
Local aParNFE	:= {	{1,"Nota Fiscal",nTamDocE ,"@9"	,"","SF1"	,If(aParIni==NIL,".T.",".F."),0,.F.},;	// Nota Fiscal Entrada
						{1,"Serie" 		,nTamSerE ,""	,"",		,If(aParIni==NIL,".T.",".F."),0,.F.},;	// Serie
						{1,"Fornecedor"	,nTamForE ,"@!"	,"","SA2"	,If(aParIni==NIL,".T.",".F."),0,.F.},;	// Fornecedor
						{1,"Loja"		,nTamLojE ,""	,"",		,If(aParIni==NIL,".T.",".F."),0,.F.} }	// Loja

Local aParNFS	:= {	{1,"Nota Fiscal",nTamDocS ,"@9"	,"","SF2DEV",If(aParIni==NIL,".T.",".F."),0,.F.},;	// Nota Fiscal Saida
						{1,"Serie" 		,nTamSerS ,""	,"",		,If(aParIni==NIL,".T.",".F."),0,.F.},;	// Serie
						{1,"Cliente"	,nTamForS ,"@!"	,"","SA1"	,If(aParIni==NIL,".T.",".F."),0,.F.},;	// Cliente
						{1,"Loja"		,nTamLojS ,""	,"",		,If(aParIni==NIL,".T.",".F."),0,.F.} }	// Loja

//Local aRetNFE	:= {Space(nTamDocE),Space(nTamSerE),Space(nTamForE),Space(nTamLojE)}
Local aRetNFS	:= {Space(nTamDocS),Space(nTamSerS),Space(nTamForS),Space(nTamLojS)}
Local aOpcao	:= {"1-Estorno","2-Troca","3-Cupom"}
Local aMotivo	:= {"1-Arrependimento"		,;
					"2-Defeito"				,;
					"3-Ficou Grande"		,;
					"4-Ficou Pequeno"		,;
					"5-Nao Vestiu bem"		,;
					"6-Pedido divergente"	,;
					"7-Outros motivos"		}

Local aParImp	:= {{2,"Motivo"	,1,aMotivo	, 100,'.T.',.T.},;
                    {2,"Op��o"	,2,aOpcao	,  50,'.T.',.T.}} 

Local aRetImp	:= {Space(06)}
Local aParam	:= {} 

Local nx		:= 1

Private cCondSF1:= ' 1234567890'  // variavel utilizada na consulta sxb CBW, favor nao remover esta linha
Private oLbx
Private aLbx	:= {{.f., Space(Tamsx3("B1_COD")[1]),Space(20),Space(10),Space(10),Space(10),Space(10),Space(20),,,''}}
Private aSvPar	:= {}
Private cOpcSel	:= ""  // variavel disponivel para infomar a opcao de origem selecionada
Private aRetImp2:= {Space(26),Space(16)}
Private cNota   := Padr(SF2->F2_DOC,TamSx3("F2_DOC")[1])
Private cSerie  := Padr(SF2->F2_SERIE,TamSx3("F2_SERIE")[1])
Private cCliFor := Padr(MV_PAR03,TamSx3("F1_FORNECE")[1])
Private cLoja   := Padr(MV_PAR04,TamSx3("F1_LOJA")[1])
Private dDataEm	:= CTOD("")
Private aOpc	:= aOpcao
Private	cPedWEB := Padr(SC5->C5_XPEDWEB,TamSx3("C5_XPEDWEB")[1])
Private dDtPed  := SC5->C5_EMISSAO
Private	cPedido := Padr(SC5->C5_NUM,TamSx3("C5_NUM")[1])
Private	cMotivo := "1"
Private nTipo	:= 0	// 1-NF DEVOLUCAO e 2=NF VENDA 

DEFAULT nOrigem := 1
/*
aParam:={	{"Nota Fiscal (Venda)"		,aParNFS,aRetNFS,{|| AWzVNFSA(1)}},;	// "Nota Fiscal (Venda)"
 	        {"Nota Fiscal (Devolu��o)"	,aParNFE,aRetNFE,{|| AWzVNFSA(2)}}}		// "Nota Fiscal (Devolu��o)"
*/
aParam:={	{"Nota Fiscal (Venda)"		,aParNFS,aRetNFS,{|| AWzVNFSA(1)}}}	// "Nota Fiscal (Venda)"

// Carrega parametros vindo da funcao pai
If aParIni <> NIL  
	For nX := 1 to Len(aParIni)              
		nTam := Len( aParam[nOrigem,3,nX ] )
		aParam[nOrigem,3,nX ] := Padr(aParIni[nX],nTam )
	Next             
EndIf 

For nx:= 1 to Len(aParam)                       
	aAdd(aOrigem,aParam[nX,1])
Next

DEFINE WIZARD oWizard TITLE "Credito de Devolucao" ;		  // "Credito de Devolucao"
        HEADER "Rotina de Inclusao de Credito de Devolucao" ; // "Rotina de Inclusao de Credito de Devolucao."
        MESSAGE "";
        TEXT "Esta rotina tem por objetivo realizar a Inclusao Credito de Devolucao conforme as opcoes disponives a seguir." ; 
        NEXT {|| .T.} ;
		FINISH {|| .T. } ;
        PANEL

//	Primeira Etapa
    CREATE PANEL oWizard ;
        HEADER "Informe a origem das informa��es para credito de devolucao" ; // "Informe a origem das informa��es para credito de devolucao"
        MESSAGE "" ;
        BACK {|| .T. } ;
 	    NEXT {|| nc:= 0,aeval(aParam,{|| &("oP"+str(++nc,1)):Hide()} ),&("oP"+str(nOrigem,1)+":Show()"),cOpcSel:= aParam[nOrigem,1],A11WZIniPar(nOrigem,aParIni,aParam) ,.T. } ;
        FINISH {|| .F. } ;
        PANEL
   
    oPanel := oWizard:GetPanel(2)  
   
    oOrigem := TRadMenu():New(30,10,aOrigem,BSetGet(nOrigem),oPanel,,,,,,,,100,8,,,,.T.)
    If aParIni <> NIL
		oOrigem:Disable()
	EndIf	   
	
//	Segunda Etapa
    CREATE PANEL oWizard ;
        HEADER "Preencha as solicita��es abaixo para a sele��o do produto" ; // Preencha as solicita��es abaixo para a sele��o do produto"
        MESSAGE "" ;
        BACK {|| .T. } ;
        NEXT {|| Eval(aParam[nOrigem,4]) } ;
        FINISH {|| .F. } ;
        PANEL                                  

    oPanel := oWizard:GetPanel(3)    
   
    For nx:= 1 to Len(aParam)
  		&("oP"+str(nx,1)) := TPanel():New( 028, 072, ,oPanel, , , , , , 120, 20, .F.,.T. )
		&("oP"+str(nx,1)):align:= CONTROL_ALIGN_ALLCLIENT                                             
        
		Do Case
			Case nx == 1
				ParamBox(aParNFS,"Par�metros...",aParam[nX,3],,,,,,&("oP"+str(nx,1)))			
			Case nx == 2
				ParamBox(aParNFE,"Par�metros...",aParam[nX,3],,,,,,&("oP"+str(nx,1)))		 //"Par�metros..."
		EndCase
		&("oP"+str(nx,1)):Hide()
	Next

//	Terceira Etapa
    CREATE PANEL oWizard ;
          HEADER "Parametriza��o por produto" ;					// Parametriza��o por produto
          MESSAGE "Marque os produtos que deseja dar entrada" ; // Marque os produtos que deseja dar entrada
          BACK {|| .T. } ;
          NEXT {|| aRetImp  := {Space(6)},VldaLbx()} ;
          FINISH {|| .T. } ;
          PANEL
    oPanel := oWizard:GetPanel(4)       
    ListBoxMar(oPanel)
                        
//	Quarta Etapa
    CREATE PANEL oWizard ;
          HEADER "Parametriza��o do Credito" 	;	// "Parametriza��o do Credito"
          MESSAGE "Informe a opcao do cliente e o motivo da Devolu��o";	// "Informe a opcao do cliente e o motivo da Devolu��o"
          BACK {|| .T. } ;
          NEXT {|| .T. } ;
          FINISH {|| Conclui( nOrigem )  } ;
          PANEL
    oPanel := oWizard:GetPanel(5)       
    ParamBox(aParImp,"Par�metros...",aRetImp2,,,,,,oPanel)	// "Par�metros..."
/*
//	Etapa Final
    CREATE PANEL oWizard ;
          HEADER "Inclus�o Finalizada" ;					// "Inclus�o Finalizada"
          MESSAGE "" ;
          NEXT {|| .T. } ;
          FINISH {|| .T.  } ;
          PANEL
*/   
ACTIVATE WIZARD oWizard CENTERED

Return                                           

Static Function A11WZIniPar(nOrigem, aParIni,aParam)
Local nX
If aParIni <> NIL
	For nx := 1 to Len(aParIni)
		&( "MV_PAR" + StrZero( nX, 2, 0 ) ) := aParIni[ nX ]
	Next
EndIf
         
For nx := 1 to Len(aParam[nOrigem,3])                                    
	&( "MV_PAR" + StrZero( nX, 2, 0 ) ) := aParam[nOrigem,3,nX ]
Next                       

Return .t.                                     

Static Function AWzVNFSA(_nTipo)
Local nQE
Local nQVol
Local nResto               
Local oOk	:= LoadBitmap( GetResources(), "LBOK" )  // CHECKED    //LBOK  //LBTIK
Local oNo	:= LoadBitmap( GetResources(), "LBNO" )  // UNCHECKED  //LBNO   
Local nT	:= TamSx3("D3_QUANT")[1]
Local nD	:= TamSx3("D3_QUANT")[2] 
Local nPrazo:= SuperGetMV("FS_PRAZODV",.F.,180)

nTipo := _nTipo

If _nTipo == 1
    cNota   := Padr(MV_PAR01,TamSx3("D2_DOC")[1])
    cSerie  := Padr(MV_PAR02,TamSx3("D2_SERIE")[1])
	cCliFor	:= Padr(MV_PAR03,TamSx3("D2_CLIENTE")[1])
	cLoja  	:= Padr(MV_PAR04,TamSx3("D2_LOJA")[1])   
	If Empty(cNota+cSerie+cCliFor+cLoja)
		MsgAlert(" Necessario informar a nota e o Cliente. ") //" Necessario informar a nota e o Cliente. "
		Return .F.
	EndIf
	SF2->(DbSetOrder(1))
	If ! SF2->(DbSeek(xFilial('SF2')+cNota+cSerie+cCliFor+cLoja))
		MsgAlert(" Nota fiscal n�o encontrada. ") //" Nota fiscal n�o encontrada. "
		Return .F.
	EndIf       
	dDataEm := SF2->F2_EMISSAO
	nCorrido:= Date() - dDataEm	
	If nCorrido > nPrazo
		MsgAlert("Prazo para devolu��o expirado ! O limite sao " + Str(nPrazo,3) + " dias. E ja se passaram " + Transf(nCorrido,"@E 999,999") + ' dias') //" Nota fiscal n�o encontrada. "
		Return .F.
	ENDIF
	aLbx:={}
	SD2->(DbSetOrder(3))
	SD2->(dbSeek(xFilial('SD2')+cNota+cSerie+cCliFor+cLoja)	)
	While SD2->(!EOF() .and. SD2->( D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA ) == xFilial('SD2')+cNota+cSerie+cCliFor+cLoja)
				
		SB1->(dbSeek(xFilial('SB1') + SD2->D2_COD))

		If ! CBImpEti(SB1->B1_COD)
			SD2->(dbSkip()	)
			Loop
		EndIf 
		nDisp   := SD2->( D2_QUANT  - D2_QTDEDEV )
		nDesc	:= SD2->( D2_DESCON / D2_QUANT )
		cPedido := SD2->D2_PEDIDO
		If nDisp > 0
			aAdd(aLbx,{	.F.,									;	// Marca��o
						SD2->D2_COD,							;	// Produto
						Transf(nDisp,			"@E 999.9999" ),;	// Qtde Disponivel
						Transf(SD2->D2_PRUNIT,	"@E 99,999.99"),;	// Preco Unit.
						Transf(nDesc,			"@E 99,999.99"),;	// Valor Desconto
						Transf(SD2->D2_PRCVEN,	"@E 99,999.99"),;	// Preco Venda
						Transf(SD2->D2_TOTAL,	"@E 99,999.99"),;	// Valor Total
						Transf(SD2->D2_QUANT,	"@E 999.9999" ),;	// Qtde Vendida
						"SD2",									;	// Preco Tabela
						SD2->(Recno()),							;	// Registro
						cPedido	}) 									// Nr Pedido
		Endif
		SD2->(dbSkip()	)
	End     
	DbSelectArea("SC5") 
	DbSetOrder(1)
	IF DBSEEK( xFilial("SC5") + cPedido )
        If ALLTRIM(SC5->C5_ORIGEM) == 'B2C'	//'VTEX'
			cPedWeb := SC5->C5_XPEDWEB
			dDtPed  := SC5->C5_EMISSAO 
		else
			MsgAlert("Inclusao bloqueada. Pedido nao e B2C") // Somente para B2C
			Return .F.
		ENDIF
	ENDIF
else
	    //cNota   := Padr(MV_PAR01,TamSx3("D1_DOC")[1])
    //cSerie  := Padr(MV_PAR02,TamSx3("D1_SERIE")[1])
	cCliFor	:= Padr(MV_PAR03,TamSx3("D1_FORNECE")[1])
	cLoja  	:= Padr(MV_PAR04,TamSx3("D1_LOJA")[1])   
	If Empty(cNota+cSerie+cCliFor+cLoja)
		MsgAlert(" Necessario informar a nota e o fornecedor. ") //" Necessario informar a nota e o fornecedor. "
		Return .F.
	EndIf
	SF1->(DbSetOrder(1))
	If ! SF1->(DbSeek(xFilial('SF1')+cNota+cSerie+cCliFor+cLoja))
		MsgAlert(" Nota fiscal n�o encontrada. ") //" Nota fiscal n�o encontrada. "
		Return .F.
	EndIf       

	aLbx:={}
	SD1->(DbSetOrder(1))
	SD1->(dbSeek(xFilial('SD1')+cNota+cSerie+cCliFor+cLoja)	)
	While SD1->(!EOF()  .and. SD1->( D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA ) == xFilial('SD1')+cNota+cSerie+cCliFor+cLoja)
				
		SB1->(dbSeek(xFilial('SB1') + SD1->D1_COD))

		If ! CBImpEti(SB1->B1_COD)
			SD1->(dbSkip()	)
			Loop
		EndIf 
		nQE     := CBQEmbI()
		nQE	    := If(Empty(nQE),1,nQE)
		nQVol   := Int(SD1->D1_QUANT/nQE)
		nResto  := SD1->D1_QUANT%nQE
		If nResto >0
			nQVol++
		EndIf
		aAdd(aLbx,{.f.,SD1->D1_COD,Str(SD1->D1_QUANT,nT,nD),Str(nQe,nT,nD),Str(nResto,nT,nD),Str(nQVol,nT,nD),SD1->D1_LOTECTL,Space(20),"SD1",SD1->(Recno()),"1"})  

		SD1->(dbSkip()	)
	End     
Endif	
oLbx:SetArray( aLbx )
oLbx:bLine := {|| {Iif(aLbx[oLbx:nAt,1],oOk,oNo),aLbx[oLbx:nAt,2],aLbx[oLbx:nAt,3],aLbx[oLbx:nAt,4],aLbx[oLbx:nAt,5],aLbx[oLbx:nAt,6],aLbx[oLbx:nAt,7],aLbx[oLbx:nAt,8],aLbx[oLbx:nAt,11]}}
oLbx:Refresh()

Return .t.

Static Function AWzVPR()
Local cProduto	:= Padr(MV_PAR01,Tamsx3("B1_COD")[1])
Local oOk		:= LoadBitmap( GetResources(), "LBOK" )   //CHECKED    //LBOK  //LBTIK
Local oNo		:= LoadBitmap( GetResources(), "LBNO" ) //UNCHECKED  //LBNO      
Local nT		:= TamSx3("D3_QUANT")[1]
Local nD		:= TamSx3("D3_QUANT")[2] 

If Empty(cProduto)
  	MsgAlert(" Necessario informar o codigo do produto. ") //" Necessario informar o codigo do produto. "
  	Return .F.
EndIf

SB1->(DbSetOrder(1))
If ! SB1->(DbSeek(xFilial('SB1')+cProduto))
  	MsgAlert(" Produto n�o encontrado ") //" Produto n�o encontrado "
  	Return .F.
EndIf    

If ! CBImpEti(SB1->B1_COD)
  	MsgAlert(" Este Produto est� configurado para nao permitir devolucao ") //" Este Produto est� configurado para nao permitir devolucao"
  	Return .F.
EndIf 
aLbx:={{	.f., SB1->B1_COD,Space(10),Str(CBQEmbI(),nT,nD),Str(0,nT,nD),Str(0,nT,nD),Space(10),Space(20),"SB1",SB1->(Recno()),''}}
oLbx:SetArray( aLbx )
oLbx:bLine := {|| {Iif(aLbx[oLbx:nAt,1],oOk,oNo),aLbx[oLbx:nAt,2],aLbx[oLbx:nAt,3],aLbx[oLbx:nAt,4],aLbx[oLbx:nAt,5],aLbx[oLbx:nAt,6],aLbx[oLbx:nAt,7],aLbx[oLbx:nAt,8]}}
oLbx:Refresh()
Return .t.

Static Function AWzVOP()
Local cOp	:= Padr(MV_PAR01,13) 
Local oOk	:= LoadBitmap( GetResources(), "LBOK" )   //CHECKED    //LBOK  //LBTIK
Local oNo	:= LoadBitmap( GetResources(), "LBNO" ) //UNCHECKED  //LBNO
Local nQtde
Local nQE
Local nQVol
Local nResto                                            
Local nT	:= TamSx3("D3_QUANT")[1]
Local nD	:= TamSx3("D3_QUANT")[2] 

If Empty(cOP)
  	MsgAlert(" Necessario informar o codigo do ordem de produ��o. ") //" Necessario informar o codigo do ordem de produ��o. "
  	Return .F.
EndIf

SC2->(DbSetOrder(1))
If ! SC2->(DbSeek(xFilial('SC2')+cOP))
  	MsgAlert(" Ordem de Produ��o n�o encontrado ") //" Ordem de Produ��o n�o encontrado "
 	Return .F.
EndIf               
SB1->(DbSetOrder(1))
SB1->(DbSeek(xFilial("SB1")+SC2->C2_PRODUTO))
If ! CBImpEti(SB1->B1_COD)
  	MsgAlert(" Este Produto est� configurado para nao permitir devolucao") //" Este Produto est� configurado para nao permitir devolucao"
  	Return .F.
EndIf 
                                                        
nQtde	:= SC2->(C2_QUANT-C2_QUJE)
nQE		:= CBQEmbI()
nQE		:= If(Empty(nQE),1,nQE)
nQVol	:= Int(nQtde/nQE)
nResto  :=nQtde%nQE                                               
If nResto >0
   nQVol++
EndIf

aLbx:={{	.f., SB1->B1_COD,Str(nQtde,nT,nD),Str(nQE,nT,nD),Str(nResto,nT,nD),Str(nQVol,nT,nD),Space(10),Space(20),"SC2",SC2->(Recno())}}
oLbx:SetArray( aLbx )
oLbx:bLine := {|| {Iif(aLbx[oLbx:nAt,1],oOk,oNo),aLbx[oLbx:nAt,2],aLbx[oLbx:nAt,3],aLbx[oLbx:nAt,4],aLbx[oLbx:nAt,5],aLbx[oLbx:nAt,6],aLbx[oLbx:nAt,7],aLbx[oLbx:nAt,8]}}
oLbx:Refresh()

Return .t.

Static Function ListBoxMar(oDlg)
Local oChk1
Local oChk2
Local lChk1 := .F.
Local lChk2 := .F.
Local oOk	:= LoadBitmap( GetResources(), "LBOK" ) //CHECKED     //LBOK  //LBTIK
Local oNo	:= LoadBitmap( GetResources(), "LBNO" )	// UNCHECKED  //LBNO
Local oP
Local lAlter := .T.   
//Local lAltPe := .T. 
  
@ 10,10 LISTBOX oLbx FIELDS HEADER " ", "Produto", "Qtde Disponivel","Preco Unit.","Valor Desc.","Preco Venda", "Valor Total", "Qtde Vendida", "Pedido"  SIZE 230,095 OF oDlg PIXEL ;  
        ON dblClick(aLbx[oLbx:nAt,1] := !aLbx[oLbx:nAt,1])

oLbx:SetArray( aLbx )
oLbx:bLine	:= {|| {Iif(aLbx[oLbx:nAt,1],oOk,oNo),aLbx[oLbx:nAt,2],aLbx[oLbx:nAt,3],aLbx[oLbx:nAt,4],aLbx[oLbx:nAt,5],aLbx[oLbx:nAt,6],aLbx[oLbx:nAt,7],aLbx[oLbx:nAt,8],aLbx[oLbx:nAt,11]}}
oLbx:align	:= CONTROL_ALIGN_ALLCLIENT

oP := TPanel():New( 028, 072, ,oDlg, , , , , , 120, 20, .F.,.T. )
oP:align:= CONTROL_ALIGN_BOTTOM

@ 5,010  BUTTON "Alterar"	 SIZE 55,11 ACTION FormProd(1) WHEN lAlter OF oP PIXEL //"Alterar"
@ 5,080  BUTTON "Copiar"	 SIZE 55,11 ACTION FormProd(2) OF oP PIXEL //"Copiar"
@ 5,160 CHECKBOX oChk1 VAR lChk1 PROMPT "Marca/Desmarca Todos" SIZE 70,7 	PIXEL OF oP ON CLICK( aEval( aLbx, {|x| x[1] := lChk1 } ),oLbx:Refresh() ) //"Marca/Desmarca Todos"
@ 5,230 CHECKBOX oChk2 VAR lChk2 PROMPT "Inverter a sele��o" 	SIZE 70,7 	PIXEL OF oP ON CLICK( aEval( aLbx, {|x| x[1] := !x[1] } ), oLbx:Refresh() ) //"Inverter a sele��o"

Return
            
Static Function FormProd( nOpcao )
Local oOk		:= LoadBitmap( GetResources(), "LBOK" ) //CHECKED    //LBOK  //LBTIK
Local oNo		:= LoadBitmap( GetResources(), "LBNO" ) //UNCHECKED  //LBNO
Local aRet		:= {}
Local aParamBox := {}  
Local cProduto	:= aLbx[oLbx:nAt,2]
Local nQtde		:= Val(aLbx[oLbx:nAt,3]) 
Local nQEmb		:= Val(aLbx[oLbx:nAt,4]) 
Local cQtde		:= aLbx[oLbx:nAt,3]
Local cQEmb		:= aLbx[oLbx:nAt,4]

Local nQVol		:= 0
Local nResto	:= 0
Local cLote		:= aLbx[oLbx:nAt,7]
Local cNumSer	:= aLbx[oLbx:nAt,8]   
Local nAt		:= oLbx:nAt  

Local nMv
Local aMvPar	:= {}
//Local lRastro 	:= .T. // Rastro(cProduto)
Local lEndere 	:= .F. // Localiza(cProduto) 
Local nT		:= TamSx3("D3_QUANT")[1]
Local nD		:= TamSx3("D3_QUANT")[2] 

For nMv := 1 To 40
     aAdd( aMvPar, &( "MV_PAR" + StrZero( nMv, 2, 0 ) ) )
Next nMv                     
 
aParamBox :={     	{1,"Produto"	      ,cProduto,"",""                          ,""	,".F.",0,.F.},; //"Produto"
					{1,"Quantidade"	      ,cQtde   ,"",'PesqPict("SD3","D3_QUANT")',""	,".T.",0,.F.},; //"Quantidade"
					{1,"Qtd por Embalagem",cQEmb   ,"",'PesqPict("SD3","D3_QUANT")',""	,".T.",0,.t.},;  //"Qtd por Embalagem"
					{1,"Lote"	          ,cLote   ,"",""                          ,""	,".F.",0,.F.},; //"Lote"
					{1,"Serie"	          ,cNumSer ,"",""                          ,""	,If(lEndere,".T.",".F."),0,.F.}} //"Serie"

If ! ParamBox(aParamBox,If(nopcao == 1,"Alterar","Copiar"),@aRet,,,,,,,,.f.)    //"Alterar","Copiar" 
	For nMv := 1 To Len( aMvPar )
  	  &( "MV_PAR" + StrZero( nMv, 2, 0 ) ) := aMvPar[ nMv ]
	Next nMv
	oLbx:SetArray( aLbx )
	oLbx:bLine := {|| {Iif(aLbx[oLbx:nAt,1],oOk,oNo),aLbx[oLbx:nAt,2],aLbx[oLbx:nAt,3],aLbx[oLbx:nAt,4],aLbx[oLbx:nAt,5],aLbx[oLbx:nAt,6],aLbx[oLbx:nAt,7],aLbx[oLbx:nAt,8]}}
	oLbx:Refresh()
	Return
EndIf

nQtde 	:= val(aRet[2])  
If Empty(nQtde)  
	If nOpcao == 2
		MsgAlert("Para a copia a quantidade n�o pode estar em branco!") //"Para a copia a quantidade n�o pode estar em branco!"
	EndIf
	If MsgYesNo("Quantidade informada igual a zero, deseja excluir esta linha?") //"Quantidade informada igual a zero, deseja excluir esta linha?"
	   aDel(aLbx,nAt)
	   aSize(aLbx,len(albx)-1)
   EndIf
Else
	nQEmb	:= val(aRet[3])
	cLote 	:= aRet[4]
	cNumSer	:= aRet[5]

	nQVol	:= Int(nQtde/nQEmb)  
	nResto	:= nQtde%nQEmb
	If nResto >0
	   nQVol++
	EndIf
	
	If nOpcao == 2
		aAdd(aLbx,aClone(aLbx[nAt]))
		nAt := Len(aLbx)
	EndIf  
	aLbx[nAt,3] := str(nQtde,nT,nD)
	aLbx[nAt,4] := str(nQEmb,nT,nD) 
	aLbx[nAt,5] := str(nResto,nT,nD) 
	aLbx[nAt,6] := str(nQVol,nT,nD) 
	
	aLbx[nAt,7] := cLote
	aLbx[nAt,8] := cNumSer  
	
EndIf

oLbx:SetArray( aLbx )
oLbx:bLine := {|| {Iif(aLbx[oLbx:nAt,1],oOk,oNo),aLbx[oLbx:nAt,2],aLbx[oLbx:nAt,3],aLbx[oLbx:nAt,4],aLbx[oLbx:nAt,5],aLbx[oLbx:nAt,6],aLbx[oLbx:nAt,7],aLbx[oLbx:nAt,8]}}
oLbx:Refresh()

For nMv := 1 To Len( aMvPar )
    &( "MV_PAR" + StrZero( nMv, 2, 0 ) ) := aMvPar[ nMv ]
Next nMv
Return .t.          

/*
	Programa para Validar a parametrizacao por produto
*/

Static Function VldaLbx()
Local nx
Local nMv
//Local lACDI11VL := .T.

SB1->(DbSetOrder(1))
For nX := 1 to Len(aLbx)   
	If aLbx[nx,1] .and. ! Empty(aLbx[nX,3])
		exit
	EndIf	
Next
If nX > len(aLbx)
	MsgAlert("Necessario marcar pelo menos um item com quantidade para incluir !") //"Necessario marcar pelo menos um item com quantidade para incluir !"
	Return .f.
EndIf      

//����������������������������������������������Ŀ
//� Ponto de Entrada para validacoes especificas �
//������������������������������������������������

aSvPar := {}
For nMv := 1 To 40
     aAdd( aSvPar, &( "MV_PAR" + StrZero( nMv, 2, 0 ) ) )
Next nMv                     

Return .t.

Static Function Conclui( _nOption ) 
//Local cLocImp  := MV_PAR01
//Local cTamanho := aRetImp2[02]
Local nX 
Local cProduto
Local nQtde
Local nQE   
Local nQVol
Local nResto
Local nValor	:= 0
Local nFrete	:= 0
Local nMv

/*
If nOption == 0 
	Return .f.
EndIf
If ! MsgYesNo("Confirma a Inclusao das devolucoes","Aviso")		// "Confirma a Inclusao de devolucoes"###"Aviso"
	Return .f.
EndIf
*/
For nMv := 1 To Len( aSvPar )
    &( "MV_PAR" + StrZero( nMv, 2, 0 ) ) := aSvPar[ nMv ]
Next nMv

For nX := 1 to Len(aLbx)   
	If aLbx[nx,1]
		cProduto:= aLbx[nx,2]
		nQtde	:= val(aLbx[nx,3])
		nQE		:= val(aLbx[nx,4])
		nResto	:= val(aLbx[nx,5])
		nQVol 	:= val(aLbx[nx,6])
	Endif
NEXT
nValor	:= 0
aItens	:= {}
For nX	:= 1 to Len(aLbx)   
	If aLbx[nx,1]
		DbSelectArea(aLbx[nx,9])
		DbGoto( aLbx[nx,10] )
		If aLbx[nx,9] == "SD1"
			aAdd(aItens,{ D1_DOC, D1_SERIE, D1_EMISSAO, D1_ITEM, D1_COD, (D1_QUANT - D1_QTDEDEV), round(D1_VUNIT,2), round(D1_TOTAL,2),D1_FORNECE, D1_LOJA })  
			nValor += round(SD1->D1_TOTAL,2)
		Else
			aAdd(aItens,{ D2_DOC, D2_SERIE, D2_EMISSAO, D2_ITEM, D2_COD, (D2_QUANT - D2_QTDEDEV), round(D2_PRCVEN,2), round(D2_TOTAL,2),D2_CLIENTE, D2_LOJA })  
			nValor += round(SD2->D2_TOTAL,2)
		Endif
	EndIf 
Next 
If nValor == 0
	MsgStop( "Devolucao zerada!" )
	Return( .F. )
Endif

If nTipo == 1	// Nota fiscal de venda
	nFrete := SF2->F2_FRETE
ELSE			// Nota fiscal de devolucao
	nFrete := SF1->F1_FRETE
ENDIF
cNomCli := POSICIONE("SA1",1, xFilial("SA1") + cCliFor + cLoja,"A1_NOME")

cCodZBF := ZBFNum()
cFilZBF := xFilial("ZBF")
DbSelectArea("ZBF")
ZBF->(DbSetOrder(1))  //ZBG_FILIAL, ZBG_NFENTR, ZBG_SERIE, ZBG_CODCLI, ZBG_LOJCLI, R_E_C_N_O_, D_E_L_E_T_
ZBF->(Reclock("ZBF",.T.))
ZBF->ZBF_FILIAL := cFilZBF
ZBF->ZBF_COD    := cCodZBF    
ZBF->ZBF_DATA	:= dDataBase		
ZBF->ZBF_DOCORI	:= cNota
ZBF->ZBF_SERIE	:= cSerie
ZBF->ZBF_DTEMIS	:= dDataEm
ZBF->ZBF_PEDIDO	:= cPedido
ZBF->ZBF_PEDWEB	:= cPedWeb
ZBF->ZBF_CLIORI	:= cCliFor
ZBF->ZBF_LOJORI	:= cLoja
ZBF->ZBF_NOMORI	:= cNomCli 
ZBF->ZBF_UF		:= SA1->A1_EST
ZBF->ZBF_VALOR	:= nValor
ZBF->ZBF_FRETE	:= nFrete
ZBF->ZBF_MOTIVO	:= aRetImp2[1]
ZBF->ZBF_OPCAO	:= aRetImp2[2]
ZBF->ZBF_VLREST	:= 0		// Valor da ESTORNO
ZBF->ZBF_VLRTRO	:= 0		// Valor da TROCA
ZBF->ZBF_VLRCUP	:= 0		// Valor do CUPOM
ZBF->ZBF_MOTDEV	:= ""
ZBF->ZBF_DTECOM := dDtPed
ZBF->ZBF_DTFISC	:= CtoD( " " )
ZBF->ZBF_NFDEV	:= ""
ZBF->ZBF_DTDEV	:= CtoD( " " )
ZBF->ZBF_PRAFIS	:= ""
ZBF->ZBF_PENDEN	:= "1"
ZBF->ZBF_NFTROC	:= ""
ZBF->ZBF_DTTROC	:= CtoD( " " )
ZBF->ZBF_ORIGEM	:= "2"
ZBF->ZBF_TIPO	:= "1"
ZBF->ZBF_CANAL	:= IIF(SA1->A1_PESSOA == "F","B2C","B2B")
ZBF->ZBF_STATUS	:= "1"
ZBF->ZBF_PAGDOR	:= "1"
ZBF->ZBF_TPTRA	:= "1"
ZBF->(MsUnlock()) 

ConfirmSX8()

DbSelectArea("ZBG")
ZBG->(DbSetOrder(1)) 
For nX := 1 to Len(aItens)   
	cCODCOR 		 := SUBSTR(ALLTRIM(aItens[nX][5]),9,3)
	cCODTAM 		 := SUBSTR(ALLTRIM(aItens[nX][5]),12,4)
	ZBG->(Reclock("ZBG",.T.))
	ZBG->ZBG_FILIAL  := cFilZBF    
	ZBG->ZBG_COD     := cCodZBF    
	ZBG->ZBG_ITEM    := aItens[nX][ 4]     
	ZBG->ZBG_SKU     := aItens[nX][ 5] 
	ZBG->ZBG_NFORI	 := aItens[nX][ 1]
	ZBG->ZBG_SERORI  := aItens[nX][ 2]
	ZBG->ZBG_ITEORI  := aItens[nX][ 4]
	ZBG->ZBG_QUANT   := aItens[nX][ 6]
	ZBG->ZBG_VLUNIT  := aItens[nX][ 7]
	ZBG->ZBG_VLTOT   := aItens[nX][ 8]
	ZBG->ZBG_CODCLI  := aItens[nX][ 9]
	ZBG->ZBG_LOJCLI  := aItens[nX][10]
	ZBG->ZBG_DESCR   := Posicione("SB1",1,xFilial("SB1")+aItens[nX][5],"B1_DESC")  
	ZBG->ZBG_COR     := Posicione("SBV",1,xFilial("SBV")+'COR'+cCODCOR,"BV_DESCRI")
	ZBG->ZBG_TAMANH  := Posicione("SBV",1,xFilial("SBV")+'TAM'+cCODTAM,"BV_DESCRI") 
	ZBG->ZBG_REFER   := Substr(ALLTRIM(aItens[nX][5]),1,8)
	ZBG->ZBG_OPCAO	 := Substr(aRetImp2[2],1,1)
	If	   Substr(aRetImp2[2],1,1) == "1"	// "1-Estorno"
		ZBG->ZBG_QTDEST  := aItens[nX][ 6]
	ElseIf Substr(aRetImp2[2],1,1) == "2"	// "2-Troca"
		ZBG->ZBG_QTDTRO  := aItens[nX][ 6]
	ElseIf Substr(aRetImp2[2],1,1) == "3"	// "3-Cupom"	
		ZBG->ZBG_QTDCUP  := aItens[nX][ 6]
	Endif
	ZBG->(MsUnlock()) 
Next
Return .T.                             

User Function CargaZBG
Local cQuery := ""
Local aItens := {}
Local NI	 := 0

	DbSelectArea("ZBG")
	DbSetOrder(1)
	If DbSeek( xFilial("ZBG") + ZBF->ZBF_COD )
		MsgStop("Itens ja existem !")
		RETURN
	Endif
	cQuery  := "SELECT D2_DOC,D2_SERIE, D2_EMISSAO, D2_ITEM, D2_COD, D2_QUANT, D2_PRCVEN, D2_TOTAL, D2_CLIENTE, D2_LOJA  " + CRLF
	cQuery  += " FROM " + RetSQLName("SD2") + " SD2 WITH (NOLOCK)  "	 + CRLF
	cQuery  += " WHERE SD2.D2_FILIAL = '"  + xFilial("SD2")   + "' "	 + CRLF
	cQuery  += "   AND SD2.D2_DOC = '"	 + ZBF->ZBF_DOCORI 	  + "' "	 + CRLF
	cQuery  += "   AND SD2.D2_CLIENTE = '" + ZBF->ZBF_CLIORI  + "' "	 + CRLF
	cQuery  += "   AND SD2.D_E_L_E_T_ = '' " + CRLF

	IF Select("TRB") > 0
		TRB->(dbCloseArea())
	Endif  

	TCQUERY cQuery NEW ALIAS TRB 

	TRB->(DBGOTOP())

	If TRB->(!EOF())
		aItens := {}
		TRB->( DBGOTOP() )
		WHILE TRB->(!EOF())
			aAdd(aItens,{TRB->D2_DOC,TRB->D2_SERIE, TRB->D2_EMISSAO, TRB->D2_ITEM, TRB->D2_COD, TRB->D2_QUANT, round(TRB->D2_PRCVEN,2), round(TRB->D2_TOTAL,2),TRB->D2_CLIENTE, TRB->D2_LOJA })  
			TRB->(dbskip()) 
		Enddo  
		If Len( aItens ) > 0 
			dbSelectArea("ZBG")
			ZBG->(DbSetOrder(1))  //ZBG_FILIAL, ZBG_NFENTR, ZBG_SERIE, ZBG_CODCLI, ZBG_LOJCLI, R_E_C_N_O_, D_E_L_E_T_
			FOR NI := 1 TO LEN(aLbx)
				ZBG->(Dbgotop())
				If ZBG->(DbSeek(xFilial("ZBG")+padr(aItens[NI][1],9)+aItens[NI][2]+aItens[NI][9]+aItens[NI][10]+aItens[NI][4],.t. ))
					ZBG->(Reclock("ZBG",.F.))
				ELSE
					ZBG->(Reclock("ZBG",.T.))
					ZBG->ZBG_FILIAL  := ZBF->ZBF_FILIAL    
					ZBG->ZBG_COD     := ZBF->ZBF_COD    
				Endif
				ZBG->ZBG_ITEM    := aItens[NI][4]    
				ZBG->ZBG_SKU     := aItens[NI][5] 
//				ZBG->ZBG_NFENTR  := aItens[NI][1]
//				ZBG->ZBG_SERIE   := aItens[NI][2]
				ZBG->ZBG_QUANT   := aItens[NI][6]
				ZBG->ZBG_VLUNIT  := aItens[NI][7]
				ZBG->ZBG_VLTOT   := aItens[NI][8]
				ZBG->ZBG_CODCLI  := aItens[NI][9]
				ZBG->ZBG_LOJCLI  := aItens[NI][10]
				cCODCOR := SUBSTR(ALLTRIM(aItens[NI][5]),9,3)
				cCODTAM := SUBSTR(ALLTRIM(aItens[NI][5]),12,4)
				ZBG->ZBG_DESCR   := Posicione("SB1",1,xFilial("SB1")+aItens[NI][5],"B1_DESC")  
				ZBG->ZBG_COR     := Posicione("SBV",1,xFilial("SBV")+'COR'+cCODCOR,"BV_DESCRI")
				ZBG->ZBG_TAMANH  := Posicione("SBV",1,xFilial("SBV")+'TAM'+cCODTAM,"BV_DESCRI") 
				ZBG->ZBG_REFER   := SUBSTR(ALLTRIM(aItens[NI][5]),1,8)
				ZBG->(MsUnlock()) 
			NEXT NI
		Endif
	Endif     
RETURN

Static Function ZBFNum()
Local cProxNum := ""

cProxNum := GETSX8NUM("ZBF","ZBF_COD")
DbSelectArea("ZBF")
DbSetOrder(1)
Do While .T.
	If ZBF->( DbSeek( xFilial("ZBF")  + cProxNum ) )
		ConfirmSX8()
		cProxNum := GetSXeNum("ZBF","ZBF_COD")
	Else
		Exit
	Endif
Enddo

Return(cProxNum)
