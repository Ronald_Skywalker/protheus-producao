#Include "Protheus.ch"
#Include "RESTFUL.ch"
#Include "tbiconn.ch"
#Include "aarray.ch"
#Include "json.ch"

/*/{Protheus.doc} GetCli

	Disponibilizar informacoes para HOTSITE

	@author 	Milton J. dos Santos
	@since		09/03/2021
	@version	1.0
	@type function
/*/
#Define Desc_Rest 	"Serviço REST para disponibilizar a Consulta Cliente"
#Define Desc_Get  	"Retorna o Faturamento - Clientes" 

User Function GetCli()

Return

WSRESTFUL rGetCli DESCRIPTION Desc_Rest

WSDATA CNPJ 	As String
WSDATA EMISSAO 	As String

WSMETHOD GET  DESCRIPTION Desc_Get  WSSYNTAX "/rGetCli || /rGetCli/{}"

END WSRESTFUL

/*
***********************************************************************************************
* {Protheus.doc}  GET                                                                         *
* @author Milton J.dos Santos                                                                 *  
* Processa as informações e retorna o json                                                    *
* @since 	09/03/2021                                                                        *
* @version undefined                                                                          *
* @param oSelf, object, Objeto contendo dados da requisição efetuada pelo cliente, tais como: *
*    - Parâmetros querystring (parâmetros informado via URL)                                  *
*    - Objeto JSON caso o requisição seja efetuada via Request Post                           *
*    - Header da requisição                                                                   *
*    - entre outras ...                                                                       *
* @type Method                                                                                *
***********************************************************************************************
*/

WSMETHOD GET WSRECEIVE CNPJ,EMISSAO WSSERVICE rGetCli
	
Local cJsonRet
Local cAgora
Local lRet 		:= .F.		
Local cCNPJ		:= ""
Local cDirSQL	:= "\Query"
Local cArqSQL 	:= "" 		
Local uAlias	:= GetNextAlias()
Local bWhile	:= {|| .NOT. (  (uAlias)->(eof()))}
Local bSkip		:= {|| (uAlias)->(dbSkip())}
Local ENTER		:= Chr(13) + Chr(10)
Local cCodPrd	:= ""
Local cMarca	:= ""

//	cJSonRec	:= Self:GetContent() // Pega a string do JSON
	cJsonRet 	:= ""	
	
//	oObj		:= NIL	 

// Deserializa a string JSON

//	FWJsonDeserialize(cJsonRec, @oObj)

//	cCNPJ		:= '31742337805'
//	cEmissao	:= '20210101'

	cCNPJ		:= Self:CNPJ
	cEmissao	:= Self:EMISSAO

	cEmissao	:= DtoS( CtoD (cEmissao) )

	If cCNPJ == 'ALL'
		cCNPJ := " "
	Endif

	If cCNPJ == NIL .or. Empty( cCNPJ )
		cMsg	:= "Conteudo invalido !"
	ElseIf Valtype( cCNPJ ) <> "C"
		cMsg	:= "Erro no formato do parametro mes e ano recebido --> " +  Valtype( cCNPJ )
	ElseIf Empty( cCNPJ )
		cMsg	:= "Faltou informar o C.N.P.J. !"
	Else
		lRet	:= .T.

		cAgora 	:= "_D_" + AllTrim( DtOS( Date() ) ) 
		cAgora	+= "_H_" + SubStr(Time(),1,2 )
		cAgora	+= "_M_" + SubStr(Time(),4,2 )

		ConOut("Requisicao de Consulta Cliente : " + cAgora )
	
//		Pesquisa CADASTRO DE CLIENTES

		cQuery := "SELECT	A1_CGC			[CNPJ],		" 			+ ENTER
		cQuery += "			A1_NOME			[NOME],		" 			+ ENTER
		cQuery += "			FT_FILIAL		[FILIAL],	" 			+ ENTER
		cQuery += "			A1_DDD			[DDD],		" 			+ ENTER
		cQuery += "			A1_TEL			[TELEFONE],	" 			+ ENTER
		cQuery += "			A1_END			[ENDERECO],	" 			+ ENTER
		cQuery += "			A1_EMAIL		[EMAIL],	" 			+ ENTER
		cQuery += "			A1_CEP		    [CEP],		" 			+ ENTER
		cQuery += "			A1_BAIRRO	    [BAIRRO],	" 			+ ENTER
		cQuery += "			A1_EST		    [ESTADO],	" 			+ ENTER
		cQuery += "			A1_MUN		    [CIDADE],	" 			+ ENTER
		cQuery += "			FT_SERIE		[SERIE],	" 			+ ENTER
		cQuery += "			FT_NFISCAL		[NFISCAL],	" 			+ ENTER
		cQuery += "			FT_NFORI		[NFORI],	" 			+ ENTER
		cQuery += "			A1_COD  		[CODCLI],	" 			+ ENTER
		cQuery += "			A1_LOJA 		[LOJACLI],	" 			+ ENTER
		cQuery += "			FT_TIPO 		[TIPO],	" 				+ ENTER
		cQuery += "			FT_EMISSAO		[EMISSAO],	" 			+ ENTER
		cQuery += "			SUM(FT_VALCONT) [TOTAL]  	" 			+ ENTER
		cQuery += " FROM " + RetSqlNAME("SFT") + " SFT "			+ ENTER
		cQuery += " INNER JOIN " + RetSqlNAME("SA1") + " SA1 "		+ ENTER
		cQuery += " 	ON  SA1.D_E_L_E_T_ = ' ' "					+ ENTER
		cQuery += " 	AND	SA1.A1_COD = SFT.FT_CLIEFOR "			+ ENTER
		cQuery += " 	AND SA1.A1_LOJA = SFT.FT_LOJA "				+ ENTER
		cQuery += " 	AND SA1.A1_CGC = '" + cCNPJ + "' "			+ ENTER
		cQuery += " WHERE	SFT.D_E_L_E_T_ = ' ' "					+ ENTER
		cQuery += " 	AND SFT.FT_EMISSAO >= '" + cEmissao + "' "	+ ENTER
		cQuery += " 	AND SFT.FT_CLIEFOR IN ( SELECT A1_COD FROM " + RetSqlNAME("SA1") + " WHERE D_E_L_E_T_ = ' ' AND A1_CGC = '" + cCNPJ + "')"
		cQuery += " GROUP BY A1_CGC, FT_FILIAL, A1_COD,A1_LOJA, A1_NOME, A1_DDD, A1_TEL, A1_EMAIL,	FT_SERIE, FT_NFISCAL, FT_EMISSAO, A1_END, FT_TIPO, FT_NFORI, A1_CEP, A1_BAIRRO, A1_EST, A1_MUN "	+ ENTER	
		If .not. Empty( cDirSQL )

//			Cria o diretório para salvar os arquivos de SQL
			If !ExistDir( cDirSQL )
				MakeDir( cDirSQL )
			EndIf
			cArqSQL 	:= "GetCli"	+ cAgora + ".SQL"
			MemoWrite( cDirSQL + '/' + cArqSQL , cQuery)

		Endif
		MPSysOpenQuery( cQuery, uAlias )
	
		dbSelectArea(uAlias)
	
		IF ( eval(bWhile))

			

			nX	:= 1
			cJsonRet  := '{ "CNPJ":"'		+ AllTrim( (uAlias)->CNPJ		)
			cJsonRet  += '","NOME":"'		+ AllTrim( (uAlias)->NOME		)
			cJsonRet  += '","DDD":"'		+ AllTrim( (uAlias)->DDD		)
			cJsonRet  += '","TELEFONE":"'	+ AllTrim( (uAlias)->TELEFONE	)
			cJsonRet  += '","ENDERECO":"'	+ AllTrim( (uAlias)->ENDERECO	)
			cJsonRet  += '","CEP":"'		+ AllTrim( (uAlias)->CEP	)
			cJsonRet  += '","BAIRRO":"'		+ AllTrim( (uAlias)->BAIRRO	)
			cJsonRet  += '","ESTADO":"'		+ AllTrim( (uAlias)->ESTADO	)
			cJsonRet  += '","CIDADE":"'		+ AllTrim( (uAlias)->CIDADE	)
			cJsonRet  += '","EMAIL":"'		+ AllTrim( (uAlias)->EMAIL		)
			cJsonRet  += '", "NOTAS":[ ' 
			While ( eval(bWhile) )
				If nX > 1
					cJsonRet  +=' , '
				EndIf	

				cCodPrd:= Posicione("SD2",3,(uAlias)->FILIAL+(uAlias)->NFISCAL+(uAlias)->SERIE+(uAlias)->CODCLI+(uAlias)->LOJACLI,"D2_COD")			
				cMarca:= Posicione("SB1",1,XFILIAL("SB1")+cCodPrd,"B1_XDESCMA")

				cJsonRet  += '{'
				cJsonRet  += '  "SERIE":"'		+ AllTrim( (uAlias)->SERIE		)
				cJsonRet  += '","NFISCAL":"'	+ AllTrim( (uAlias)->NFISCAL	)
				cJsonRet  += '","NFORI":"'		+ AllTrim( (uAlias)->NFORI	)
				cJsonRet  += '","TIPO":"'		+ AllTrim( (uAlias)->TIPO	)
				cJsonRet  += '","MARCA":"'		+ AllTrim( cMarca	)
				cJsonRet  += '","EMISSAO":"'	+ DtoC(StoD((uAlias)->EMISSAO)	)
				cJsonRet  += '","TOTAL":'		+ Transform( (uAlias)->TOTAL,"@ 999999999.99") 
				cJsonRet  += '}'

				eval(bSkip)
				nX:= nX+1
			Enddo
			cJsonRet  += ']'	
			cJsonRet  += '}'

		Else
			cMsg	:= OEMtoANSI("CNPJ sem movimentacao : ") + cCNPJ + " "
			cJsonRet:= '{"msg":"Erro : ' + cMsg + '"}'
		Endif
//		Fecha a tabela
		(uAlias)->(dbCloseArea())

		::SetResponse( cJsonRet )

	Endif
	If .not. lRet

		ConOut( cMsg  )
		SetRestFault(400, cMsg )

	Endif

Return( lRet )
