#Include "Rwmake.ch"
#Include "Protheus.ch"
#INCLUDE "TBICONN.CH"
#Include "topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FA050DEL  �Autor  �Marcio Medeiros Junior� Data �  07/13/18 ���
�������������������������������������������������������������������������͹��
���Desc.     � Totalit                                                    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function FA050DEL
Local lRet     := .T.
Local cQuery   := ""
Local aAreaAnt := GetArea()
Local aAreaSA2 := sa2->(GetArea())
Local aAreaSA3 := sa3->(GetArea())
Local aAreaSE3 := se3->(GetArea())
Local cNewAlias := GetNextAlias()

// Ajustes para o novo tratamento de comissoes - TOTALIT 11/2019
// Verificar se houve lancamento de comissao negativa para o titulo a pagar (NDF) com origem 7-outros debitos

sa2->(dBSetOrder(1))  // A2_FILIAL+A2_COD+A2_LOJA
sa3->(dBSetOrder(3))  // A3_FILIAL+A3_CGC

If se2->e2_tipo = "NDF" .and. sa2->(MSSeek(xFilial("SA2")+se2->e2_fornece+se2->e2_loja)) .and. ;
	sa3->(MSSeek(xFilial("SA3")+sa2->a2_cgc))

	cQuery := "SELECT E3_DATA FROM "+RetSqlName("SE3")+" WHERE "
	cQuery += "E3_FILIAL = '"+xFilial("SE3")+"' AND E3_PREFIXO = '"+se2->e2_prefixo+"' AND E3_NUM = '"+se2->e2_num+"' AND "
	cQuery += "E3_PARCELA = '"+se2->e2_parcela+"' AND E3_TIPO = '"+se2->e2_tipo+"' AND E3_XORIGEM = '7' AND "
	cQuery += "E3_CODCLI = '"+se2->e2_fornece+"' AND E3_LOJA = '"+se2->e2_loja+"' AND D_E_L_E_T_ = ' '"
	TcQuery cQuery Alias (cNewAlias) New 
	TcSetField(cNewAlias,"E3_DATA","D")
	
	While (cNewAlias)->(!Eof())
		// Se a comissao ainda nao foi paga, exclui o debito. Se foi paga tem que criar um credito para compensar
		If Empty((cNewAlias)->e3_data)
			RecLock("SE3",.F.)
			se3->(dBDelete())
			se3->(MsUnlock())
		Else
			RecLock("SE3",.T.)
			se3->e3_filial  := xFilial("SE3")
			se3->e3_vend    := sa3->a3_cod
			se3->e3_xmarca  := ""
			se3->e3_xorigem := "8"  // Outros creditos
			se3->e3_xsituac := "S"	// Considerar para o fechamento
			se3->e3_emissao := dDataBase
			se3->e3_num     := se2->e2_num
			se3->e3_serie   := se2->e2_prefixo
			se3->e3_codcli  := se2->e2_fornece
			se3->e3_loja    := se2->e2_loja
			se3->e3_prefixo := se2->e2_prefixo
			se3->e3_tipo    := se2->e2_tipo
			se3->e3_baiemi  := "E"
			se3->e3_pedido  := ""
			se3->e3_origem  := "E"
			se3->e3_vencto	:= U_RFATM011(sa3->a3_dia,sa3->a3_ddd)
			se3->e3_moeda   := Strzero(se2->e2_moeda,2)
			se3->e3_sdoc    := se2->e2_prefixo
			se3->e3_base    := se2->e2_valor
			se3->e3_porc    := 100
			se3->e3_comis   := se2->e2_valor
			se3->(MsUnlock())
		Endif
	
		(cNewAlias)->(dBSkip())
	Enddo
	
	(cNewAlias)->(dBCloseArea())
Endif

/*
DbSelectArea("SA2")
SA2->(DbSetOrder(1))
SA2->( DbSeek( xFilial("SA2") + SE2->E2_FORNECE + SE2->E2_LOJA ) )

DbSelectArea("SA3")
SA3->(DbSetOrder(3))
If SA3->( DbSeek( xFilial("SA3") + SA2->A2_CGC ) )
	cQuery := "UPDATE " + RetSqlName("SE3") + " SET E3_DATA = '' "
	cQuery += "WHERE E3_VEND = '" + SA3->A3_COD + "' AND E3_SERIE = '" + SE2->E2_PREFIXO + "' "
	cQuery += "AND E3_NUM = '" + SE2->E2_NUM + "' AND E3_PARCELA = '" + SE2->E2_PARCELA	+ "' "
	cQuery += "AND D_E_L_E_T_ = ''"
					
	If TcSqlExec(cQuery) > 0
		MsgAlert("Erro na atualiza��o do SE3!!")
		lRet := .F.
	Endif   
	
Endif
*/

RestArea(aAreaSA2)
RestArea(aAreaSA3)
RestArea(aAreaSE3)
RestArea(aAreaAnt)

Return lRet