#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"  
#INCLUDE "RWMAKE.CH"  

user function Fa330Vld()  
Local lRet     := .t.  
Local xcQuery  := ""  
Local NR       := 0    
Local cDHIni   := Time()

FOR NR := 1 TO LEN(aTitulos) 
   If aTitulos[NR][4] == 'NCC'  .AND.  aTitulos[NR][8]    
      xcQuery  := "SELECT E1_XSTATUS FROM "+RetSQLName("SE1")+ " WITH (NOLOCK) WHERE E1_PREFIXO = '"+aTitulos[NR][1]+"' AND E1_NUM = '"+aTitulos[NR][2]+"' AND E1_PARCELA = '"+aTitulos[NR][3]+"' AND D_E_L_E_T_  = '' " 
	  If Select("TBSE1") > 0
		 TBSE1->(DBCloseArea())                    
	  EndIf
	  TCQuery xcQuery New Alias "TBSE1"
	  If !TBSE1->(Eof())
	     MsgStop('o status � '+TBSE1->E1_XSTATUS)
		 IF ALLTRIM(TBSE1->E1_XSTATUS) == "Cr�d.Conc." .OR. ALLTRIM(TBSE1->E1_XSTATUS) == "Proc.Finan" 
            lRet := .t.    
         else
            msgStop("Aten��o...  T�tulo NCC n�o Liberado!","Verifique! ")
            lRet := .f.
            return(lRet)
         Endif   
	  Endif	
   Endif
Next NR 
conout("FA330VLD - Inicio: " + cDHIni + " - Fim: " + Time() )
return(lRet)
