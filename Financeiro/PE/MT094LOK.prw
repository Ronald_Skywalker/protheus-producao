#include 'protheus.ch'
#include 'parmtype.ch'
#Include "Totvs.ch"
#Include "FwMvcDef.ch"   
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#Include "Xmlxfun.ch"
#INCLUDE "bitmap.ch"
#INCLUDE "ap5mail.ch"
#INCLUDE "shell.ch"

//
//Fun��o para redirecionamento de Libera��o de Documentos 
//Espec�fico HOPE
//Execu��o - Daniel Pereira de Souza  em 20/10/2017
//
user function MT094LOK()
Local lRet := .F.
           
        // AtuSZ0()  
         //U_mvcaprov()
         U_MVCAPRO1()
  

Return lRet	


Static Function atuSZ0()
Local xFornec := ""
Local xLoja   := ""
Local xVecto  := ctod(space(8))
Local nValor  := 0
Local xItens  := ""
Local xRazao  := ""
Local cQuery  := "DELETE FROM "+RetSqlName("SZ0")
Local nLog := TcSQLExec(cQuery)
If nLog < 0
   MsgStop("N�o foi poss�vel atualizar base de dados, verifica com o Dpto. TI ")
   Return()
EndIf
dbselectarea("SCR")
N:=0
WHILE SCR->(!EOF())
   If SCR->CR_TIPO == "PC"       
      xFornec := ""
      xLoja   := ""
      xVecto  := ctod(space(8))
      nValor  := 0
      xItens  := ""
      xRazao  := ""
      dbSelectArea("SC7")
      SC7->(dbSetOrder(1))
      if SC7->(dbSeek(xFilial("SC7")+AllTrim(SCR->CR_NUM)))
	      WHILE SC7->(!eof()) .AND. ALLTRIM(SC7->C7_NUM) == alltrim(SCR->CR_NUM)       
	         xFornec := SC7->C7_FORNECE
	         xLoja   := SC7->C7_LOJA
	         dbSelectArea("SA2")
	         SA2->(dbSetOrder(1))
	         SA2->(dbgotop())
	         if SA2->(dbSeek(xFilial("SA2")+xFornec+xLoja))  
	            MsgStop("Achei o Cliente caracas!" )
	         Else
	             MsgStop("N�o achei!")
	         Endif        
	         xRazao  := SA2->A2_NOME
	         //dbSelectArea("SC7")         
	         nValor  := nValor+SC7->C7_TOTAL
	         If xItens  == ""
	            xItens  := SC7->C7_DESCRI
	         Else
	            xItens  := xItens+" - "+SC7->C7_DESCRI
	         Endif        
	         SC7->(DBSKIP())
	      Enddo
	      
	      dbselectarea("SZ0")
	      SZ0->(RecLock("SZ0",.T.))
	      SZ0->Z0_FILIAL     := xFilial("SZ0")
	      SZ0->Z0_DOCUMENT   := SCR->CR_NUM 
	      SZ0->Z0_FORNECE    := xFornec  
	      SZ0->Z0_LOJA       := xLoja 
	      SZ0->Z0_RSOCIAL    := xRazao 
	      SZ0->Z0_VENCTO     := xVecto
	      SZ0->Z0_VALOR      := nValor
	      SZ0->Z0_ITENS      := xItens
	      SZ0->Z0_TIPO       := substr(SCR->CR_TIPO,1,1)  
	      SZ0->Z0_OK         :=  ""
	      SZ0->(MsUnLock())
	      N++
	  EndIf
   Endif                 
   //dbSelectArea("SCR")
   SCR->(DBSKIP())
   IF N >= 11
     EXIT //SCR->(DBGOTOP())
   Endif  
Enddo
Return()    
