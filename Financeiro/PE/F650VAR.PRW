#INCLUDE "RWMAKE.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "PROTHEUS.CH"
/*
+----------------------------------------------------------------------------+
!                         FICHA TECNICA DO PROGRAMA                          !
+----------------------------------------------------------------------------+
!   DADOS DO PROGRAMA                                                        !
+------------------+---------------------------------------------------------+
!Tipo              ! Ponto de Entrada                                        !
+------------------+---------------------------------------------------------+
!Modulo            ! FINANCEIRO                                              !
+------------------+---------------------------------------------------------+
!Nome              ! F650VAR                                                 ! 
+------------------+---------------------------------------------------------+
!Descricao         ! P.E. Substituir pequisa Cnab(Contas a Receber)          !
+------------------+---------------------------------------------------------+
!Autor             ! Luiz Fernando Berti                                     !
+------------------+---------------------------------------------------------+
!Data de Criacao   ! 11/07/2012                                              !
+------------------+---------------------------------------------------------+
*/
User Function F650VAR() // U_F650VAR()
Local aValores := PARAMIXB
Local cNmTt    := aValores[01][01]//Id.Cnab
Local cNsNum   := aValores[01][04]//Nosso Numero
Local xBuffer  := aValores[01][14]//Linha Inteira
Local cBanco   := MV_PAR06
Local lAchou   := .f.       
Local nPos     := 0
/*----------------------------------------------------------
|	PROTHEUS PADRAO											|
-----------------------------------------------------------*/
//msgStop("PAssei aqui!.... Daniel  Nosso Numero " +cNsNum+" Id Cnab "+cNmTt  )
SE1->(dbSetOrder(19)) // IdCnab
If SE1->(DbSeek(Substr(cNmTt,1,10)))
	lAchou  := .t.
	cFilAnt	:= SE1->E1_FILIAL
	If !Empty( xFilial("CT2") )//verifica se a contabilizacao eh exclusiva...
		mv_par11 := 2  //Desligo contabilizacao on-line  
		
	Endif
Else
	lAchou := .f.
	DBSelectArea("SE1")
	DBGoTop()
	cNumTit:= ""
Endif
If !lAchou
	//Busca por IdCnab
	SE1->(dbSetOrder(16)) // Filial+IdCnab
	If !SE1->(DbSeek(xFilial("SE1")+Substr(cNmTt,1,10)))
		DBSelectArea("SE1")
		DBGoTop()
		cNumTit:= ""
		lAchou := .f.
	Else
		lAchou := .t.
	EndIf
Endif
/*----------------------------------------------------------
|	BUSCA POR NOSSO NUMERO									|
------------------------------------------------------------*/
If !lAchou  // !SE1->(Found())	.And. !Empty(cNsNum)
	cQuery := "SELECT R_E_C_N_O_ AS E1_RECNO FROM "+RetSQLName("SE1")
	cQuery += " WHERE  "
	cQuery += " E1_FILIAL = '"+xFilial("SE1")+"' AND "
	cQuery += " E1_NUMBCO = '"+cNsNum+"' AND "
	cQuery += " D_E_L_E_T_ <> '*' "
	If Select("TBSA1") <> 0
		DBSelectArea("TBSA1")
		DBCloseArea()
	EndIf
	TCQuery cQuery New Alias "TBSA1"
	If !TBSA1->(Eof())
		SE1->(dbGoTo(TBSA1->E1_RECNO))
		lAchou := .t.
	Else
		nPos := At('/',cNmTt)
		if npos <> 0
			cxNumero := substr(cNmTt,1,npos-1)
			cxParcel := substr(cNmTt,npos+1,3)
		Else
			cxNumero := cNmTt
			cxParcel := "   "
		Endif
		cxNumero := StrZero(VAL(CxNumero),9)
		cxPrefix:= "MIL"
	    cQuery := "SELECT R_E_C_N_O_ AS E1_RECNO FROM "+RetSQLName("SE1")
	    cQuery += " WHERE  "
	    cQuery += " E1_FILIAL = '"+xFilial("SE1")+"' AND "
	    cQuery += " E1_NUM = '"+cxNumero+"' AND "
	    cQuery += " E1_PARCELA = '"+cxParcel+"' AND "
	    cQuery += " D_E_L_E_T_ <> '*' "
	    If Select("TBSA1") <> 0
	       DBSelectArea("TBSA1")
	  	   DBCloseArea()
	    EndIf
	    TCQuery cQuery New Alias "TBSA1"
	    If !TBSA1->(Eof())
	 	   SE1->(dbGoTo(TBSA1->E1_RECNO))
		   lachou := .t.
		EndIf
	EndIf
	If Select("TBSA1") <> 0
		DBSelectArea("TBSA1")
		DBCloseArea()
	EndIf
	DBSelectArea("SE1")
EndIf
Return() // (lAchou)
