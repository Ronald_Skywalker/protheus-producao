#include "protheus.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} FA070CA2()
PE na Exclus�o da Baixa de Titulos

@author Sergio S. Fuzinaka
@since Set/2019
/*/
//-------------------------------------------------------------------
User Function FA070CA2()

    If ParamIxb[1] = 5 .OR. ParamIxb[1] = 6  // Cancelamento da Baixa|Exclusao da Baixa
        If SE1->E1_VLTXCC > 0
            RecLock("SE1",.F.)
            SE1->E1_SDDECRE := ( SE1->E1_VLTXCC + SE1->E1_XVLPIX + SE1->E1_DECRESC )
            MsUnlock()
        Endif
    Endif

Return
