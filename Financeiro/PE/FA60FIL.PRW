#include "rwmake.ch"     

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �FA60FIL �Autor  �Daniel Pereira de Souza � Data � 18/04/05  ���
�������������������������������������������������������������������������͹��
���Ponto de Entrada Para Filtrar no Border� somente os Titulos de Parcelas ��
��� sem x                                                                  ��
��                                                                         ��
�������������������������������������������������������������������������͹��
���Uso       � FINA060                                                    ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

User Function FA60FIL()

Local cRet    := ""
Local cPerg   := 'FILBORDERO'
Local cResp   := ""
Local aParam := {}
Local aRetParm	:= {}
Local aArea   := GetArea()
Local aAreaSX1 := SX1->(GetArea())

//Faz backup da vari�vel MV_PAR01 
xBkpPar01:= MV_PAR01

aAdd(aParam,{1,"Filtrar por?"  ,Space(20),"","","","",50,.F.})  

If ParamBox(aParam,"Filtro",@aRetParm,{||.T.},,,,,,"U_FA60FIL",.F.,.F.)
		lOk	:= .T.
		cResp := aRetParm[1]
    
      If !Empty(Alltrim(cResp))   
        cRet += '!(alltrim(SE1->E1_XTPPAG) <> ALLTRIM("'+cResp+'"))'    
      Else
        cRet += '!(alltrim(SE1->E1_XTPPAG) <> "")'
      Endif  

Else
		lOk := .F.
endif	

RestArea(aAreaSX1)
RestArea(aArea)


MV_PAR01:= xBkpPar01

Return cRet




/*
Local cRet    := ""
Local cPerg   := 'FILBORDERO'
Local cResp   := ""
If !Pergunte(cPerg, .T.)
   Return .T.
Endif         
cResp := MV_PAR01  //E1_XTPPAG =BOLETO E-MAIL       
If len(alltrim(cResp))<> 0      
  cRet += '!(alltrim(SE1->E1_XTPPAG) <> ALLTRIM("'+cResp+'"))'               
  Return( cRet )   
Else
  cRet += '!(alltrim(SE1->E1_XTPPAG) <> "")'
  Return(cRet)
   
Endif
   