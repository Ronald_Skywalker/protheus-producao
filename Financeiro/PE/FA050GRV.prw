#include "protheus.ch"
#include "topconn.ch"
/****************************************************************************
* FA050GRV - P.E. utilizado apos a gravacao de todos os dados (na inclus�o  *
*            do t�tulo) e antes da sua contabiliza��o                       *
*            Criar debito de comissao para o representante quando for titulo*
*            do tipo NDF                                                    *
*            TOTALIT SOLUTIONS   11/2019                                    *
****************************************************************************/
User Function FA050GRV()
Local aAreaAnt  := GetArea()
Local aAreaSA2  := sa2->(GetArea())
Local aAreaSA3  := sa3->(GetArea())

If se2->e2_tipo <> "NDF"
	Return Nil
Endif

sa2->(dBSetOrder(1))  // A2_FILIAL+A2_COD+A2_LOJA
sa3->(dBSetOrder(3))  // A3_FILIAL+A3_CGC

If sa2->(MSSeek(xFilial("SA2")+se2->e2_fornece+se2->e2_loja)) .and. ;
	sa3->(MSSeek(xFilial("SA3")+sa2->a2_cgc))

	RecLock("SE3",.T.)
	se3->e3_filial  := xFilial("SE3")
	se3->e3_vend    := sa3->a3_cod
	se3->e3_xmarca  := ""
	se3->e3_xorigem := "7"  // Outros debitos
	se3->e3_xsituac := "S"	// Considerar para o fechamento
	se3->e3_emissao := dDataBase
	se3->e3_num     := se2->e2_num
	se3->e3_serie   := se2->e2_prefixo
	se3->e3_codcli  := se2->e2_fornece
	se3->e3_loja    := se2->e2_loja
	se3->e3_prefixo := se2->e2_prefixo
	se3->e3_tipo    := se2->e2_tipo
	se3->e3_baiemi  := "E"
	se3->e3_pedido  := ""
	se3->e3_origem  := "E"
	se3->e3_vencto	:= U_RFATM011(sa3->a3_dia,sa3->a3_ddd)
	se3->e3_moeda   := Strzero(se2->e2_moeda,2)
	se3->e3_sdoc    := se2->e2_prefixo
	se3->e3_base    := -se2->e2_valor
	se3->e3_porc    := 100
	se3->e3_comis   := -se2->e2_valor
	se3->(MsUnlock())
Endif

RestArea(aAreaSA2)
RestArea(aAreaSA3)
RestArea(aAreaAnt)

Return Nil