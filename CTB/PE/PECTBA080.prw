#include "Protheus.ch"
#include "FwMVCDef.ch"

User Function CTBA080()

Local lRet		:= .T.
Local aParam	:= PARAMIXB
Local oObj 		:= aParam[1]
Local cModel	:= aParam[2]
Local cIdUsr	:= RetCodUsr()
Local cUsrList	:= GetNewPar("MV_USERLP","000723")

If aParam <> NIL .And. oObj:GetOperation() <> 1 .And. !( cIdUsr $ cUsrList )
	
//	Alert(oObj:GetOperation())
	Alert("Usu�rio n�o permitido para realizar esta opera��o!! (MV_USERLP)")
	lRet := .F.

Endif

Return lRet
