#include "protheus.ch"
#include "rwmake.ch"
#include "totvs.ch"

              

/////////////////////////////////////////////////////////////
//
//   Tela para cadastro consulta do de para do Centro 
//   de Custo
//
//   Ricardo Henrique de Mello Lima
//   Hope
//
//   Tabela ZA0 
//
////////////////////////////////////////////////////////////

User Function CTBA002()   


Private cTitulo := oemtoansi("De / Para Centro de Custo")

axcadastro("ZA6",cTitulo)


Return  