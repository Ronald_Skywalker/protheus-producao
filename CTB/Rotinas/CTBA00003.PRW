#INCLUDE "PROTHEUS.CH"
#INCLUDE "RWMAKE.CH"


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CTB0003   �Autor  �Cristiano Lima      � Data �  22/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �  alterar os parametros de bloqueio dos modulos de compras  ���
���          �  faturamento, financeiro e contabilidade                   ���
�������������������������������������������������������������������������͹��
���Uso       � Hope Lingerie                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/


USER FUNCTION blok() 

local oDlg
local oFin
local oComp
local oFatu


local oGfin
local oGcomp
local oGfatu



local dGfin := getmv("MV_DATAFIN")
local dGcomp := getmv("MV_ULMES")
local dGfatu := getmv("MV_DATAFIS")
//Static



  DEFINE MSDIALOG oDlg TITLE "Bloqueio dos Modulos" FROM 000, 000  TO 250, 400 COLORS 0, 16777215 PIXEL

    @ 028, 121 MSGET oGfin VAR dGfin SIZE 062, 015 OF oDlg COLORS 0, 16777215 PIXEL
    @ 048, 121 MSGET oGcomp VAR dGcomp SIZE 062, 015 OF oDlg COLORS 0, 16777215 PIXEL
    @ 068, 121 MSGET oGfatu VAR dGfatu SIZE 062, 015 OF oDlg COLORS 0, 16777215 PIXEL


    @ 028, 035 BUTTON ofin PROMPT "Financeiro" SIZE 062, 015 OF oDlg PIXEL ACTION (putmv("MV_DATAFIN",dgfin))
    @ 048, 035 BUTTON oComp PROMPT "Compras" SIZE 062, 015 OF oDlg PIXEL ACTION(putmv("MV_ULMES",dGcomp))
    @ 068, 035 BUTTON oFatu PROMPT "Fat.\L. Fiscal" SIZE 062, 015 OF oDlg PIXEL ACTION(putmv("MV_DATAFIS",dGfatu))
    @ 110, 080 BUTTON oFisc PROMPT "Fechar" SIZE 062, 015 OF oDlg PIXEL ACTION (Odlg:End(),.F.)
  ACTIVATE MSDIALOG oDlg  
  
  

 
Return