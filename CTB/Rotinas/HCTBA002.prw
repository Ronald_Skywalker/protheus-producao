#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#include "fileio.ch"

User Function HCTBA002 //U_HCTBA002()
        Local aPergs    := {}
        Local lConv      := .F.

        Private cArq       := ""
		Private cTemp      := GetTempPath() //pega caminho do temp do client
		Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema
        Private aResps     := {}
        Private aArquivos  := {}

        aAdd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.F.,""})


        If ParamBox(aPergs,"Parametros", @aResps)

	      	aAdd(aArquivos, AllTrim(aResps[1]))
			cArq := AllTrim(aResps[1])
       		Processa({|| ProcMovs()})
		Endif
Return

Static Function ProcMovs()
        Local cError    	 := ""
        Local cWarning      := ""
        Local nFile         := 0
        Local aLinhas   	 := {}
        Local cFile     	 := AllTrim(aResps[1])
        Local nI            := 0
		Local cLinha  := ""
		Local nLintit    := 1
		Local nLin 		 := 0
		Local nTotLin := 0
		Local aDados  := {}
		Local nHandle := 0

    	nFile := fOpen(cFile, FO_READ + FO_DENYWRITE)

        If nFile == -1
			If !Empty(cFile)
            	MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")
            EndIf
            Return
        EndIf

		nHandle := Ft_Fuse(cFile)
		Ft_FGoTop()
		nLinTot := FT_FLastRec()-1
		ProcRegua(nLinTot)

		While nLinTit > 0 .AND. !Ft_FEof()
			Ft_FSkip()
			nLinTit--
		EndDo

		Do While !Ft_FEof()
			IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))
			nLin++
			cLinha := Ft_FReadLn()
			If Empty(AllTrim(StrTran(cLinha,',','')))
				Ft_FSkip()
			    Loop
			EndIf
			cLinha := StrTran(cLinha,','," ")
			cLinha := StrTran(cLinha,';',",")
			cLinha := StrTran(cLinha,'"',"'")
			cLinha := '{"'+cLinha+'"}'
			cLinha := StrTran(cLinha,',','","')
			aAdd(aDados, &cLinha)

			FT_FSkip()
		EndDo

		FT_FUse()

       	_doc := aDados[1,2]
       	_data := stod(SubStr(aDados[1,7],1,4)+SubStr(aDados[1,7],6,2)+SubStr(aDados[1,7],9,2))
		nReg := 1
		_xitem := '  1'  
		DBSELECTAREA("CT2")
		Begin Transaction
		 For nI := 1 To Len(aDados)
		    cData := alltrim(aDados[nI][2])
		   	_data := ctod(cData)
			IncProc("Gravando Linha "+AllTrim(Str(nI))+" de "+AllTrim(Str(nLinTot)))
			aItens := {}

			cValor := aDados[nI,10]
			npos2 := At(',',Alltrim(cValor))
			if npos2 == 0
				nValor := nValor := val(cValor)
			Else
				nInt   := substr(Alltrim(cValor),1,npos2-1)
				nCent  := substr(Alltrim(cValor),npos2+1,15)
				if substr(nCent,2,1) ==" "
					nCent:= nCent+'0'
				Endif
				cValor := nInt+nCent
				nValor := val(cValor)/100
			Endif
            nValor := val(cValor)/100
			CT2->(RecLock("CT2",.T.))
			Replace CT2->CT2_FILIAL		with  '0'+alltrim(aDados[nI][1])  //xfilial("CT2")
			Replace CT2->CT2_DATA		with _data
    		Replace CT2->CT2_LOTE		with '00'+alltrim(aDados[nI][3])
			Replace CT2->CT2_SBLOTE		with '001' //alltrim(aDados[nI][3])
			Replace CT2->CT2_DOC		with STRZERO(Val(aDados[nI,5]),6)
		    Replace CT2->CT2_LINHA	  	with _xitem
		    Replace CT2->CT2_MOEDLC  	with '01'
		    Replace CT2->CT2_DC   		with "3"
		    Replace CT2->CT2_DEBITO  	with ALLTRIM(aDados[nI,8])
		    Replace CT2->CT2_CREDIT  	with ALLTRIM(aDados[nI,9])
		    Replace CT2->CT2_CCD	 	with ALLTRIM(aDados[nI,13])
		    Replace CT2->CT2_CCC  		with ALLTRIM(aDados[nI,14])
		    Replace CT2->CT2_VALOR  	with nValor
		    Replace CT2->CT2_HIST   	with ALLTRIM(aDados[nI,12])
		    Replace CT2->CT2_ITEMD      with ALLTRIM(aDados[nI,15])
		    Replace CT2->CT2_ITEMC      with ALLTRIM(aDados[nI,16])
		    Replace CT2->CT2_EMPORI		with "01"
		    Replace CT2->CT2_FILORI		with "0"+alltrim(aDados[nI][1])//"0101"
		    Replace CT2->CT2_TPSALD		with "1"
		    Replace CT2->CT2_SEQUEN     with "0000051306"
		    Replace CT2->CT2_MANUAL		with "2"
            Replace CT2->CT2_ORIGEM     with aDados[nI,20]
		    Replace CT2->CT2_ROTINA		with "GPEM110"
		    Replace CT2->CT2_AGLUT		with "1"
            Replace CT2->CT2_LP         with aDados[nI,20]
		    Replace CT2->CT2_SEQHIS		with "001"
		    Replace CT2->CT2_SEQLAN		with _xitem
		    Replace CT2->CT2_SEGOFI      with "0101000089"
		    Replace CT2->CT2_DTCV3       with _data
		    Replace CT2->CT2_NODIA       with "0101000089"
		    Replace CT2->CT2_DIACTB      with "01"
			MsUnLock()
			_xitem = soma1(_xitem,3,.T.,.F.)
		Next nI

        End Transaction
Return

//				               {'CT2_ORIGEM' 	,'MSEXECAUT'				,NIL},;
//				               {'CT2_HP'   		,''   						,NIL},;
