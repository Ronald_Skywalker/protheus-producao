#include "rwmake.ch"
#include "Protheus.ch"
#INCLUDE "Ap5Mail.ch"
#INCLUDE "Topconn.ch"
#INCLUDE "VKEY.CH"
#INCLUDE "colors.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � PCONA01  � Autor � Jos� Carlos Ferrari   � Data � 20/07/20� ��
�������������������������������������������������������������������������Ĵ��
���Descri��o � Controla as datas de fechamentos                           ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Especifico                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function PCONA01()
Local oDlg
Local _cCodUsr	:= GetMv("MV_XUSFECH")
Local _cUsr		:= RetCodUsr()

Private oFont1  := TFont():New( "TIMES NEW ROMAN",12.5,18,,.T.,,,,,.F.)
Private oFont2  := TFont():New( "ARIAL",12.5,12,,.T.,,,,,.F.)
Private oFont3  := TFont():New( "ARIAL",10.5,10,,.T.,,,,,.F.)
Private oFonte  := TFont():New( "TIMES NEW ROMAN",18.5,25,,.T.,,,,,.F.)

Private dDtEst, nDescLin
Private oDesFunc, oDescLin 

If _cUsr $ _cCodUsr
	dDtEst   := GETMV("MV_ULMES")  
	dDtFin   := GETMV("MV_DATAFIN")
	dDtFis   := GETMV("MV_DATAFIS")
	dDtMov   := GETMV("MV_DBLQMOV")

	nOpca:=0

	While .T.
		
		DEFINE MSDIALOG oDlg TITLE OemToAnsi("Controla as datas de Fechamento") From 9,0 To 28,80 OF oMainWnd
		
		@ 10, 10 SAY oSay PROMPT "Estoque: (MV_ULMES)"  PIXEL OF oDlg SIZE 60,14 COLOR CLR_HBLUE 
		
		@  7, 117 MSGET oDtEst VAR dDtEst   When .T.  SIZE 80,14 FONT oFont2 PIXEL OF oDlg  COLOR CLR_HBLUE CENTER
		
		@ 35, 10 SAY oSay PROMPT "Financeiro: (MV_DATAFIN)"  PIXEL OF oDlg SIZE 60,14 COLOR CLR_HBLUE
		
		@ 32, 117 MSGET oDtFin  VAR dDtFin  When .T. SIZE 80,14 FONT oFont2 PIXEL OF oDlg  COLOR CLR_HBLUE  CENTER
			
		@ 60, 10 SAY oSay PROMPT "Fiscal: (MV_DATAFIS)"  PIXEL OF oDlg SIZE 60,14 COLOR CLR_HBLUE 
		
		@ 57, 117 MSGET oDtFis  VAR dDtFis  When .T. SIZE 80,14 FONT oFont2 PIXEL OF oDlg  COLOR CLR_HBLUE  CENTER

		@ 85, 10 SAY oSay PROMPT "Estoque: (MV_DBLQMOV)"  PIXEL OF oDlg SIZE 60,14 COLOR CLR_HBLUE 
		
		@ 83, 117 MSGET oDtFis  VAR dDtMov  When .T. SIZE 80,14 FONT oFont2 PIXEL OF oDlg  COLOR CLR_HBLUE  CENTER

		@ 115,170 Button "&Encerrar"      Size 60,15 Action {nOpca := 1,oDlg:End()} of oDlg Pixel  //FONT oFonte
		
		@ 115,100 Button "&Confirmar"     Size 60,15 Action {nOpca := 2,oDlg:End()} of oDlg Pixel  //FONT oFonte
		
		ACTIVATE MSDIALOG oDlg Centered
		
		IF nOpca == 2
			
			PCONA01Grava()
			Exit 
			
		Else
			MsUnlockAll( )
			Exit
		End
		
	End
Else
	MsgAlert("Usu�rio sem permiss�o para acessar a rotina!!")

EndIf

Return

Static Function PCONA01Grava()

PutMV( "MV_ULMES", dDtEst )
PutMV( "MV_DBLQMOV", dDtMov )
PutMV( "MV_DATAFIN", dDtFin )
PutMV( "MV_DATAFIS", dDtFis )

MsgInfo("Atualizacao efetuada com sucesso.")

Return
