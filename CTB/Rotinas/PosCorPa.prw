#Include 'Protheus.ch'

//Protheus 12.1.25 - Hope Lingerie
//Fun��o: PosCorPa()
//Descri��o: Verifica se existe uma corre��o de adiantamento para uma compensa��o de t�tulos moeda 02
//e retorna com o valor da corre��o, realizado este desenvolvimento, pois o padr�o nas compensa��es 
//posiciona apenas na NF e n�o na PA.
//Autor: Jos� Carlos Ferrari
//Data: 26/06/2020
User Function PosCorPa(cNum, cClifor, cLoja, cData, cPa)		

Local cQuery 	:= ""
Local nValor 	:= 0.00

cNum 	:= Alltrim(cNum)
cPaPar	:= Substr(cPa,13,3) 
cPa  	:= Substr(cPa,4,9)
cData 	:= DTOS(cData)

IF SELECT("TMP") > 0
	TMP->(DbCloseArea())
ENDIF

cQuery := "SELECT * FROM "+RetSqlName("SE5")+" WHERE D_E_L_E_T_ = '' "
cQuery += "AND E5_FILIAL ='"+xFilial("SE5")+"' AND E5_NUMERO = '"+cPa+"'"
cQuery += "AND E5_PARCELA = '"+cPaPar+"'"
cQuery += "AND SUBSTRING(E5_DOCUMEN,4,9) ='"+cNum+"' AND E5_CLIFOR = '"+cClifor+"'"
cQuery += "AND E5_LOJA ='"+cLoja+"' AND E5_DATA = '"+cData+"'"
cQuery += "AND E5_HISTOR = 'CORREC.DE ADIANTAMENTO'"

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.F.,.T.)

If TMP->E5_VALOR
	nValor := TMP->E5_VALOR

EndIf

Return nValor

//Protheus 12.1.25 - Hope Lingerie
//Fun��o: ConNatPa()
//Descri��o: Posiciona na natureza financeira do PA, para contabilizar com a conta da natureza do mesmo
//e n�o buscar a conta da natureza da NF
//Autor: Jos� Carlos Ferrari
//Data: 13/10/2020
User Function ConNatPa(cNum, cClifor, cLoja, cData, cPa)		

Local cQuery 	:= ""
Local cConta 	:= ""
Local cNaturez	:= ""

cNum 	:= Alltrim(cNum)
cPaPar	:= Substr(cPa,13,3) 
cPa  	:= Substr(cPa,4,9)
cData 	:= DTOS(cData)

IF SELECT("TMP") > 0
	TMP->(DbCloseArea())
ENDIF

cQuery := "SELECT E5_NATUREZ FROM "+RetSqlName("SE5")+" WHERE D_E_L_E_T_ = '' "
cQuery += "AND E5_FILIAL ='"+xFilial("SE5")+"' AND E5_NUMERO = '"+cPa+"'"
cQuery += "AND E5_PARCELA = '"+cPaPar+"'"
cQuery += "AND SUBSTRING(E5_DOCUMEN,4,9) ='"+cNum+"' AND E5_CLIFOR = '"+cClifor+"'"
cQuery += "AND E5_LOJA ='"+cLoja+"' AND E5_DATA = '"+cData+"'"

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.F.,.T.)

cNaturez := TMP->E5_NATUREZ

cConta := Posicione("SED", 1, xFilial("SED")+cNaturez, "ED_CONTA")

Return cConta
