#include "rwmake.ch"
#include "Protheus.ch"
#INCLUDE "Ap5Mail.ch"
#INCLUDE "Topconn.ch"
#INCLUDE "VKEY.CH"
#INCLUDE "colors.CH"

/*---------------------------------------------------------------------*
| Func:  HMVEXCNF                                                      |
| Autor: Robson Melo                                       			   |
| Data:  27/07/2021                                                    |
| Desc:  Grava par�metro com limite de exclus�o de notas               |
*---------------------------------------------------------------------*/

User Function HMVEXCNF()
Local oDlg
Local cMvUser := SuperGetMv("HP_SPEDUSR",.F.,"000677,000182")  
Local cCodUsr := RetCodUsr()

Private oFont1  := TFont():New( "TIMES NEW ROMAN",12.5,18,,.T.,,,,,.F.)
Private oFont2  := TFont():New( "ARIAL",12.5,12,,.T.,,,,,.F.)
Private oFont3  := TFont():New( "ARIAL",10.5,10,,.T.,,,,,.F.)
Private oFonte  := TFont():New( "TIMES NEW ROMAN",18.5,25,,.T.,,,,,.F.)

Private cMvSped
Private oDesFunc 

If cCodUsr $ cMvUser

	cMvSped  := PADL(GETMV("MV_SPEDEXC "),3,'0')  

	nOpca:=0

	While .T.
		
		DEFINE MSDIALOG oDlg TITLE OemToAnsi("Controla horas limite de exclus�o de Nota") From 9,0 To 28,80 OF oMainWnd
		

		@ 35, 10 SAY oSay PROMPT "Limite: "  PIXEL OF oDlg SIZE 60,14 COLOR CLR_HBLUE FONT oFont2
		
		@ 32, 117 MSGET oDtFin  VAR cMvSped  When .T. SIZE 80,14 PIXEL OF oDlg  COLOR CLR_HBLUE  CENTER

		@ 115,170 Button "&Fechar"      Size 60,15 Action {nOpca := 1,oDlg:End()} of oDlg Pixel  
		
		@ 115,100 Button "&Confirmar"     Size 60,15 Action {nOpca := 2,oDlg:End()} of oDlg Pixel  
		
		ACTIVATE MSDIALOG oDlg Centered
		
		IF nOpca == 2
			
			GravaMv()
			Exit 
			
		Else
			MsUnlockAll( )
			Exit
		End
		
	End
Else
	MsgAlert("Usu�rio sem permiss�o para acessar a rotina!!")

EndIf

Return

/*---------------------------------------------------------------------*
| Func:  GravaMv                                                       |
| Autor: Robson Melo                                       			   |
| Data:  27/07/2021                                                    |
| Desc:  Grava par�metro com limite de exclus�o de notas               |
*---------------------------------------------------------------------*/
Static Function GravaMv()

PutMV( "MV_SPEDEXC", Val(cMvSped) )

MsgInfo("Atualizacao efetuada com sucesso.")

Return
