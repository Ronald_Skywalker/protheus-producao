#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL030 
Performance de atendimento por SKU
@author Ronaldo Pereira
@since 10/10/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/'

user function HPREL030()

Local aRet := {}
Local aPerg := {}
Local nx := 0

Local CMVPAR07
Private cTipos := ""
Private oReport
Private cPergCont	:= 'HPREL030' 
	

//AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif


If !Empty(Mv_Par07)
	For nx := 1 to len(Mv_Par07) step 3
		cAux := SubStr(Mv_Par07,nx,3)
		If cAux <> '***'
			cTipos += "'"+cAux+"',"
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)
	

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()

return 

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Ronaldo Pereira
@since 10 de Outubro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'PFF', 'PERFORMANCE DE ATENDIMENTO POR SKU', , {|oReport| ReportPrint( oReport ), 'PERFORMANCE DE ATENDIMENTO POR SKU' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'PERFORMANCE DE ATENDIMENTO POR SKU', { 'PFF', 'SC6','SC5','SA1','SZ1','SZ2','SB1','ZAH','SF2'})
			
	TRCell():New( oSection1, 'EMISSAO'			    ,'PFF', 		'EMISSAO',							"@!"                        ,15)
	TRCell():New( oSection1, 'PEDIDO'			    ,'PFF', 		'PEDIDO',		  					"@!"				        ,06)
	TRCell():New( oSection1, 'CLIENTE'				,'PFF', 		'CLIENTE',		   					"@!"						,60)	
	TRCell():New( oSection1, 'PRODUTO'		 	    ,'PFF', 		'PRODUTO',		          			"@!"						,15)
	TRCell():New( oSection1, 'QTDE_VENDA'	   	    ,'PFF', 		'QTDE_VENDA',      	   			    "@E 999,99"					,10)
	TRCell():New( oSection1, 'QTDE_ENTREGUE'		,'PFF', 		'QTDE_ENTREGUE',			   		"@E 999,99"					,10)
	TRCell():New( oSection1, 'QTDE_PERDA'			,'PFF', 		'QTDE_PERDA',				   		"@E 999,99"					,10)
	TRCell():New( oSection1, 'SALDO'				,'PFF', 		'SALDO',			   				"@E 999,99"					,10)
	TRCell():New( oSection1, 'VALOR'				,'PFF', 		'VALOR',							"@E 999,9999.99"			,10)
	TRCell():New( oSection1, 'SUBCOLECAO' 			,'PFF', 	    'SUBCOLECAO',                  		"@!"		    	        ,35)
	TRCell():New( oSection1, 'SITUACAO_PROD'		,'PFF', 		'SITUACAO_PROD',      	   			"@!"						,06)	
	TRCell():New( oSection1, 'TIPO_PEDIDO'			,'PFF', 		'TIPO_PEDIDO',			   			"@!"						,30)
	TRCell():New( oSection1, 'POLITICA'		        ,'PFF', 		'POLITICA',							"@!"						,30)
	
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Ronaldo Pereira
@since 10 de Outubro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("PFF") > 0
		PFF->(dbCloseArea())
	Endif    
    
	cQuery := " SELECT R.EMISSAO, "
	cQuery += "        R.PEDIDO, "
	cQuery += "        R.CLIENTE, "
	cQuery += "        R.PRODUTO, "
	cQuery += "        R.QTDE_VENDA, "
	cQuery += "        R.QTDE_ENTREGUE, "
	cQuery += "        R.QTDE_PERDA, "
	cQuery += "        ((R.QTDE_VENDA - R.QTDE_ENTREGUE)- R.QTDE_PERDA) AS SALDO, "
	cQuery += "        R.VALOR, "
	cQuery += "        R.SUBCOLECAO, "
	cQuery += "        R.SITUACAO_PROD, "
	cQuery += "        R.TIPO_PEDIDO, "
	cQuery += "        R.POLITICA "
	cQuery += " FROM "
	cQuery += "   (SELECT CONVERT(CHAR,CAST(C5_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
	cQuery += "           C6.C6_NUM AS PEDIDO, "
	cQuery += "           A1_COD + ' - ' + A1_NOME AS CLIENTE, "
	cQuery += "           C6.C6_PRODUTO AS PRODUTO, "
	cQuery += "           SUM(C6.C6_QTDVEN) AS QTDE_VENDA, "
	cQuery += "           SUM(C6.C6_QTDENT) AS QTDE_ENTREGUE, "
	cQuery += "           ISNULL( "
	cQuery += "                    (SELECT SUM(SC6.C6_QTDVEN) - SUM(SC6.C6_QTDENT) "
	cQuery += "                     FROM "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) "
	cQuery += "                     WHERE SC6.D_E_L_E_T_ = '' "
	cQuery += "                       AND SC6.C6_NUM = C6.C6_NUM "
	cQuery += "                       AND SC6.C6_PRODUTO = C6.C6_PRODUTO "
	cQuery += "                       AND SC6.C6_QTDVEN <> SC6.C6_QTDENT "
	cQuery += "                       AND SC6.C6_BLQ = 'R' ),0)AS QTDE_PERDA, "
	cQuery += "           (SUM(C6.C6_QTDVEN) * C6.C6_VALOR) AS VALOR, "
	cQuery += "           ISNULL(ZA.ZAH_DESCRI, '-')AS SUBCOLECAO, "
	cQuery += "           B1.B1_YSITUAC AS SITUACAO_PROD, "
	cQuery += "           Z1.Z1_CODIGO + ' - ' + Z1.Z1_DESC AS TIPO_PEDIDO, "
	cQuery += "           Z2.Z2_CODIGO + ' - ' + Z2.Z2_DESC AS POLITICA "
	cQuery += "    FROM "+RetSqlName("SC6")+" C6 WITH (NOLOCK) "
	cQuery += "    INNER JOIN "+RetSqlName("SC5")+" C5 WITH (NOLOCK) ON (C5.C5_NUM = C6.C6_NUM "
	cQuery += "                                           AND C5.C5_FILIAL = C6.C6_FILIAL "
	cQuery += "                                           AND C5.C5_CLIENTE = C6.C6_CLI "
	cQuery += "                                           AND C5.D_E_L_E_T_ = '') "
	cQuery += "    INNER JOIN "+RetSqlName("SA1")+" A1 WITH (NOLOCK) ON (A1.A1_COD = C5.C5_CLIENTE "
	cQuery += "                                           AND A1.D_E_L_E_T_ = '') "
	cQuery += "    INNER JOIN "+RetSqlName("SZ1")+" Z1 WITH (NOLOCK) ON (Z1.Z1_CODIGO = C5.C5_TPPED "
	cQuery += "                                           AND Z1.D_E_L_E_T_ = '') "
	cQuery += "    LEFT JOIN "+RetSqlName("SZ2")+" Z2 WITH (NOLOCK) ON (Z2.Z2_CODIGO = C5.C5_POLCOM "
 	cQuery += "                                         AND Z2.D_E_L_E_T_ = '') "
	cQuery += "    INNER JOIN "+RetSqlName("SB1")+" B1 WITH (NOLOCK) ON (B1.B1_COD = C6.C6_PRODUTO "
	cQuery += "                                           AND B1.D_E_L_E_T_ <> '*') "
	cQuery += "    LEFT JOIN "+RetSqlName("ZAH")+" ZA WITH (NOLOCK) ON (B1_YSUBCOL = ZA.ZAH_CODIGO "
	cQuery += "                                          AND ZA.D_E_L_E_T_ = '') "
	cQuery += "    WHERE C6.D_E_L_E_T_ = '' AND C6.C6_FILIAL = '0101' AND C6.C6_LOCAL = 'E0' "
	cQuery += "      AND C5_POLCOM NOT IN ('099', "
	cQuery += "                            '018', "
	cQuery += "                            '352', "
	cQuery += "                            '353', "
	cQuery += "                            '350') "
                           
    If !Empty(cTipos)
    	cQuery += " AND C5.C5_TPPED IN ("+cTipos+") "
	EndIf
  
   if !Empty(mv_par03)
		cQuery += " AND C6.C6_PRODUTO BETWEEN '"+Alltrim(mv_par03)+"' AND '"+Alltrim(mv_par04)+"'   "
	endif
	if !Empty(mv_par05)
		cQuery += " AND A1.A1_COD BETWEEN '"+Alltrim(mv_par05)+"' AND '"+Alltrim(mv_par06)+"'  "
	endif
    
    
	cQuery += "      AND C6.C6_NUM IN "
	cQuery += "        (SELECT SC6.C6_NUM "
	cQuery += "         FROM "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) "
	cQuery += "         INNER JOIN "+RetSqlName("SF2")+" F2 WITH (NOLOCK) ON (F2.D_E_L_E_T_ = '' "
	cQuery += "                                                AND F2.F2_FILIAL = SC6.C6_FILIAL "
	cQuery += "                                                AND F2.F2_DOC = SC6.C6_NOTA "
	cQuery += "                                                AND F2.F2_CLIENTE = SC6.C6_CLI "
	cQuery += "                                                AND F2.F2_DAUTNFE BETWEEN '"+ DTOS(mv_par01) +"' AND '"+ DTOS(mv_par02) +"') "
	cQuery += "         WHERE SC6.D_E_L_E_T_ = '' AND SC6.C6_FILIAL = '0101' AND SC6.C6_LOCAL = 'E0' "
	cQuery += "         GROUP BY SC6.C6_NUM) "
	cQuery += "    GROUP BY C5.C5_EMISSAO, "
	cQuery += "             C6.C6_NUM, "
	cQuery += "             A1_COD + ' - ' + A1_NOME, "
	cQuery += "             C6.C6_PRODUTO, "
	cQuery += "             C6.C6_VALOR, "
	cQuery += "             ZA.ZAH_DESCRI, "
	cQuery += "             B1.B1_YSITUAC, "
	cQuery += "             Z1.Z1_CODIGO + ' - ' + Z1.Z1_DESC, "
	cQuery += "             Z2.Z2_CODIGO + ' - ' + Z2.Z2_DESC) R "
	
	TCQUERY cQuery NEW ALIAS PFF

	While PFF->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("EMISSAO"):SetValue(PFF->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")

		oSection1:Cell("PEDIDO"):SetValue(PFF->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")		
				
		oSection1:Cell("CLIENTE"):SetValue(PFF->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTO"):SetValue(PFF->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_VENDA"):SetValue(PFF->QTDE_VENDA)
		oSection1:Cell("QTDE_VENDA"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_ENTREGUE"):SetValue(PFF->QTDE_ENTREGUE)
		oSection1:Cell("QTDE_ENTREGUE"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PERDA"):SetValue(PFF->QTDE_PERDA)
		oSection1:Cell("QTDE_PERDA"):SetAlign("LEFT")
						
		oSection1:Cell("SALDO"):SetValue(PFF->SALDO)
		oSection1:Cell("SALDO"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR"):SetValue(PFF->VALOR)
		oSection1:Cell("VALOR"):SetAlign("LEFT")
				
		oSection1:Cell("SUBCOLECAO"):SetValue(PFF->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")		
				
		oSection1:Cell("SITUACAO_PROD"):SetValue(PFF->SITUACAO_PROD)
		oSection1:Cell("SITUACAO_PROD"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO_PEDIDO"):SetValue(PFF->TIPO_PEDIDO)
		oSection1:Cell("TIPO_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("POLITICA"):SetValue(PFF->POLITICA)
		oSection1:Cell("POLITICA"):SetAlign("LEFT")
		
		
		oSection1:PrintLine()
		
		PFF->(DBSKIP()) 
	enddo
	PFF->(DBCLOSEAREA())
Return( Nil )
