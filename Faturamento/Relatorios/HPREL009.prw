#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL006 
Relatório PERFORMANCE 
@author Weskley Silva
@since 06/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL009()

Local nx := 0

Private oReport
Private cPergCont	:= 'HPREL009' 
Private cTipos := " "

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

cMvPar05:= AllTrim(mv_par05) 

If !Empty(cMvPar05)
	For nx := 1 to len(cMvPar05) step 3
		cAux := SubStr(cMvPar05,nx,3)
		If cAux <> '***'
			cTipos += "'"+cAux+"',"
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 03 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'PER', 'PERFORMANCE DE ATENDIMENTO POR PEDIDO ', , {|oReport| ReportPrint( oReport ), 'PERFORMANCE DE ATENDIMENTO POR PEDIDO' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'PERFORMANCE DE ATENDIMENTO POR PEDIDO', { 'PER', 'SC6','SC5','SA1','SZ1','SZ2','SF2'})
			
	TRCell():New( oSection1, 'EMISSAO'	    	    ,'PER', 		'EMISSAO',							"@!"                        ,15)
	TRCell():New( oSection1, 'PEDIDO' 		        ,'PER', 		'PEDIDO',              				"@!"						,06)
	TRCell():New( oSection1, 'CLIENTE'				,'PER', 		'CLIENTE',			   				"@!"						,60)
	TRCell():New( oSection1, 'QTDE_PEDIDO'         	,'PER', 		'QTDE_PEDIDO',             			"@E 999,99"					,06)
	TRCell():New( oSection1, 'QTDE_ENTREGUE'       	,'PER', 		'QTDE_ENTREGUE',       				"@E 999,99"		        	,06)
	TRCell():New( oSection1, 'QTDE_PERDA' 			,'PER', 		'QTDE_PERDA' ,	    			    "@E 999,99"					,06)
	TRCell():New( oSection1, 'SALDO'	        	,'PER', 		'SALDO' 	,		       			"@E 999,999.99"	   	        ,08)
	TRCell():New( oSection1, 'VALOR_TOTAL'			,'PER', 		'VALOR_TOTAL',			   			"@E 999,999.99"				,08)	
	TRCell():New( oSection1, 'TIPO_PEDIDO'			,'PER', 		'TIPO_PEDIDO',			   			"@!"						,40)
	TRCell():New( oSection1, 'POLITICA'			    ,'PER', 		'POLITICA',			   			    "@!"						,35)
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	
 
	IF Select("PER") > 0
		PER->(dbCloseArea())
	Endif		
	
    cQuery := " SELECT R.C5_EMISSAO AS EMISSAO, "
    cQuery += "        R.PEDIDO, "
    cQuery += "        R.CLIENTE, "
    cQuery += "        R.QTDE_PEDIDO, "
    cQuery += "        R.QTDE_ENTREGUE, "
    cQuery += "        R.QTDE_PERDA, "
    cQuery += "        ((R.QTDE_PEDIDO - R.QTDE_ENTREGUE) - R.QTDE_PERDA) AS SALDO, "
    cQuery += "        R.VALOR_TOTAL, "
    cQuery += "        R.TIPO_PEDIDO, "
    cQuery += "        R.POLITICA "
    cQuery += " FROM "
    cQuery += "   (SELECT C5_ORIGEM, "
    cQuery += "           CONVERT(CHAR,CAST(C5_EMISSAO AS SMALLDATETIME), 103) AS C5_EMISSAO, "
    cQuery += "          C5_NUM AS PEDIDO, "
    cQuery += "          A1_COD + ' - ' + A1_NOME AS CLIENTE, "
    cQuery += "     (SELECT SUM(SC6.C6_QTDVEN) "
    cQuery += "       FROM "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) "
    cQuery += "       WHERE SC6.D_E_L_E_T_ = '' "
    cQuery += "         AND SC6.C6_NUM = C5.C5_NUM)AS QTDE_PEDIDO, "
    cQuery += "           ISNULL( "
    cQuery += "                   (SELECT SUM(SC6.C6_QTDENT) "
    cQuery += "                     FROM "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) "
    cQuery += "                     WHERE SC6.D_E_L_E_T_ = '' "
    cQuery += "                       AND SC6.C6_NUM = C5.C5_NUM "
    cQuery += "                       AND SC6.C6_NOTA <> '' ),0)AS QTDE_ENTREGUE, "
    cQuery += "           ISNULL( "
    cQuery += "                    (SELECT SUM(SC6.C6_QTDVEN - SC6.C6_QTDENT) "
    cQuery += "                     FROM "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) "
    cQuery += "                     WHERE SC6.D_E_L_E_T_ = '' "
    cQuery += "                       AND SC6.C6_NUM = C5.C5_NUM "
    cQuery += "                       AND SC6.C6_BLQ = 'R' ),0)AS QTDE_PERDA, "
    cQuery += "           SUM(C6.C6_VALOR) AS VALOR_TOTAL, "
    cQuery += "           C5_POLCOM + ' - ' + Z2_DESC AS POLITICA, "
    cQuery += "           Z1_CODIGO + ' - ' + Z1_DESC AS TIPO_PEDIDO "
    cQuery += "    FROM "+RetSqlName("SC6")+" C6 WITH (NOLOCK) "
    cQuery += "    INNER JOIN "+RetSqlName("SC5")+" C5 WITH (NOLOCK) ON (C5_NUM = C6_NUM "
    cQuery += "                                          AND C5.D_E_L_E_T_ = '') "
    cQuery += "    INNER JOIN "+RetSqlName("SA1")+" A1 WITH (NOLOCK) ON (A1.A1_COD = C5.C5_CLIENTE "
    cQuery += "                                           AND A1.D_E_L_E_T_ = '') "
    cQuery += "    INNER JOIN "+RetSqlName("SZ1")+" Z1 WITH (NOLOCK) ON (Z1_CODIGO = C5_TPPED "
    cQuery += "                                           AND Z1.D_E_L_E_T_ = '') "
    cQuery += "    LEFT JOIN "+RetSqlName("SZ2")+" Z2 WITH (NOLOCK) ON (Z2_CODIGO = C5_POLCOM "
    cQuery += "                                          AND Z2.D_E_L_E_T_ = '') "
    cQuery += "    WHERE C6.D_E_L_E_T_ = '' AND C6.C6_FILIAL = '0101' AND C6.C6_LOCAL = 'E0' "
    cQuery += "      AND C5_POLCOM NOT IN ('099', "
    cQuery += "                            '018', "
    cQuery += "                            '352', "
    cQuery += "                            '353', "
    cQuery += "                            '350') "
	 
	If !Empty(cTipos)
	cQuery += CRLF + "AND C5.C5_TPPED IN ("+cTipos+") "
	EndIf
	
	if !Empty(mv_par03)
		cQuery += " AND C5.C5_NUM BETWEEN '"+Alltrim(mv_par03)+"' AND '"+Alltrim(mv_par04)+"' "
	endif

    cQuery += "      AND C6.C6_NUM IN "
    cQuery += "        (SELECT SC6.C6_NUM "
    cQuery += "         FROM "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) "
    cQuery += "        INNER JOIN "+RetSqlName("SF2")+" F2 WITH (NOLOCK) ON (F2.D_E_L_E_T_ = '' "
    cQuery += "                                                AND F2.F2_FILIAL = SC6.C6_FILIAL "
    cQuery += "                                                AND F2.F2_DOC = SC6.C6_NOTA "
    cQuery += "                                                AND F2.F2_CLIENTE = SC6.C6_CLI "
    cQuery += "                                                AND F2.F2_DAUTNFE BETWEEN '"+ DTOS(mv_par01) +"' AND '"+ DTOS(mv_par02) +"') "
    cQuery += "         WHERE SC6.D_E_L_E_T_ = '' AND SC6.C6_FILIAL = '0101' AND SC6.C6_LOCAL = 'E0' "
    cQuery += "         GROUP BY SC6.C6_NUM) "
    cQuery += "    GROUP BY C5_ORIGEM, "
    cQuery += "             C5_EMISSAO, "
    cQuery += "             C5_NUM, "
    cQuery += "             A1_COD + ' - ' + A1_NOME, "
    cQuery += "             C5_POLCOM + ' - ' + Z2_DESC, "
    cQuery += "             Z1_CODIGO + ' - ' + Z1_DESC) R  "
	
	
	TCQUERY cQuery NEW ALIAS PER

	While PER->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("EMISSAO"):SetValue(PER->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")
		
		oSection1:Cell("PEDIDO"):SetValue(PER->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")
				
		oSection1:Cell("CLIENTE"):SetValue(PER->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PEDIDO"):SetValue(PER->QTDE_PEDIDO)
		oSection1:Cell("QTDE_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_ENTREGUE"):SetValue(PER->QTDE_ENTREGUE)
		oSection1:Cell("QTDE_ENTREGUE"):SetAlign("LEFT")
				
		oSection1:Cell("QTDE_PERDA"):SetValue(PER->QTDE_PERDA)
		oSection1:Cell("QTDE_PERDA"):SetAlign("LEFT")
				
		oSection1:Cell("SALDO"):SetValue(PER->SALDO)
		oSection1:Cell("SALDO"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR_TOTAL"):SetValue(PER->VALOR_TOTAL)
		oSection1:Cell("VALOR_TOTAL"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO_PEDIDO"):SetValue(PER->TIPO_PEDIDO)
		oSection1:Cell("TIPO_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("POLITICA"):SetValue(PER->POLITICA)
		oSection1:Cell("POLITICA"):SetAlign("LEFT")
									
		oSection1:PrintLine()
		
		PER->(DBSKIP()) 
	enddo
	PER->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 06 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Dt Inicial"		        ,""		,""		,"mv_ch1","D",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Dt Final"			    ,""		,""		,"mv_ch2","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Pedido de Venda de ?"   ,""		,""		,"mv_ch3","C",06,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Pedido de Venda Ate ?"  ,""		,""		,"mv_ch4","C",06,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Tipo dos Pedidos     ","","","Mv_ch5","C",99,0,0,"G","U_HF002TP()","","","N","Mv_par05","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione os tipos de pedido.",""},{""},{""},"")
Return