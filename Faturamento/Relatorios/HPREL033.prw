#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'

/*/{Protheus.doc} HPREL033 
Relatorio de Confer�ncia
@author Ronaldo Pereira
@since 26/07/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL033()

Private oReport
Private cPergCont	:= 'HPREL033' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Ronaldo Pereira
@since 26 de Julho de 2018
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1

	oReport := TReport():New( 'TMP', 'RELAT�RIO DE CONFER�NCIA', , {|oReport| ReportPrint( oReport ), 'RELAT�RIO DE CONFER�NCIA' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELAT�RIO DE CONFER�NCIA', {'TMP','SZP','SZR','SZJ','SC5'})
			
	TRCell():New( oSection1, 'PEDIDO'	    	    ,'TMP', 		'PEDIDO',							"@!"                        ,10)
	TRCell():New( oSection1, 'DATA_EMISSAO'			,'TMP', 		'DATA_EMISSAO',       				"@!"		                ,15)
	TRCell():New( oSection1, 'DATA_LIBERACAO'		,'TMP', 		'DATA_LIBERACAO',       			"@!"		                ,15)
	TRCell():New( oSection1, 'COD_CLIENTE' 		    ,'TMP', 		'COD_CLIENTE',              		"@!"						,08)
	TRCell():New( oSection1, 'CLIENTE' 		        ,'TMP', 		'CLIENTE',              			"@!"						,60)
	TRCell():New( oSection1, 'CANAL'         		,'TMP', 		'CANAL',			           		"@!"						,10)
	TRCell():New( oSection1, 'DAP'         			,'TMP', 		'DAP',			           			"@!"						,10)
	TRCell():New( oSection1, 'DATA_PRE'				,'TMP', 		'DATA_PRE',       					"@!"		                ,15)
	TRCell():New( oSection1, 'NUM_PREFAT'	        ,'TMP', 		'NUM_PREFAT',	   			        "@!"						,10)
	TRCell():New( oSection1, 'QTDE_PRE'      		,'TMP', 		'QTDE_PRE',      					"@E 999,99"				    ,15)
	TRCell():New( oSection1, 'QTDE_CONF' 			,'TMP', 	    'QTDE_CONF',     					"@E 999,99"		    	    ,15)
	TRCell():New( oSection1, 'COD_USUARIO'			,'TMP', 		'COD_USUARIO',			   			"@!"						,15)	
	TRCell():New( oSection1, 'CONFERENTE'     		,'PEC', 		'CONFERENTE',	       				"@!"						,30)
	TRCell():New( oSection1, 'DATA_CONF_INI'		,'TMP', 		'DATA_CONF_INI',		   			"@!"						,15)
	TRCell():New( oSection1, 'DATA_CONF_FIM'		,'TMP', 		'DATA_CONF_FIM',		   			"@!"						,15)
	TRCell():New( oSection1, 'HORA_INI'				,'TMP', 		'HORA_INI',			   				"@!"						,15)
	TRCell():New( oSection1, 'HORA_FIM'				,'TMP', 		'HORA_FIM',			   				"@!"						,15)
    TRCell():New( oSection1, 'STATUS_CONF'			,'TMP', 		'STATUS_CONF',				   		"@!"						,15)
	
Return( oReport )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("TMP") > 0
		TMP->(dbCloseArea())
	Endif
	
	cQuery := " SELECT ZP_PEDIDO AS PEDIDO, C5_CLIENT AS COD_CLIENTE, C5_XNOMCLI AS CLIENTE, C5_XCANALD AS CANAL, ZJ_LOTDAP AS DAP, CONVERT(CHAR, CAST(ZJ_DATA AS SMALLDATETIME), 103) AS DATA_PRE, ZP_NUMPF AS NUM_PREFAT, "
    cQuery += " CONVERT(CHAR, CAST(C5_EMISSAO AS SMALLDATETIME), 103) AS DATA_EMISSAO, (SELECT CONVERT(CHAR, CAST(MAX(ZZ1_DTLIB)AS SMALLDATETIME), 103) FROM ZZ1010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZZ1_FILIAL = ZP_FILIAL AND ZZ1_PEDIDO = ZP_PEDIDO GROUP BY ZZ1_PEDIDO,ZZ1_FILIAL) AS DATA_LIBERACAO, "
    cQuery += " (SELECT SUM(ZJ_QTDLIB) FROM "+RetSqlName("SZJ")+" (NOLOCK) AS SZJ WHERE SZJ.ZJ_NUMPF = ZP.ZP_NUMPF AND SZJ.D_E_L_E_T_ = '') AS QTDE_PRE, "
    cQuery += " (SELECT SUM(ZJ_QTDSEP) FROM "+RetSqlName("SZJ")+" (NOLOCK) AS SZJ WHERE SZJ.ZJ_NUMPF = ZP.ZP_NUMPF AND SZJ.D_E_L_E_T_ = '') AS QTDE_CONF, "
	cQuery += " ZP_USR AS COD_USUARIO, ZP_NOMEUSR AS CONFERENTE, CONVERT(CHAR, CAST(MIN(ZP_DATA) AS SMALLDATETIME), 103) AS DATA_CONF_INI, CONVERT(CHAR, CAST(MAX(ZP_DATA) AS SMALLDATETIME), 103) AS DATA_CONF_FIM, MIN(ZP_HORA) AS HORA_INI, MAX(ZP_HORA) AS HORA_FIM, "
	cQuery += " CASE WHEN ZJ_CONF = 'L' THEN 'EM ANDAMENTO' WHEN ZJ_CONF = 'S' THEN 'CONFERIDO' ELSE 'N�O INICIADO' END AS STATUS_CONF "
	cQuery += " FROM "+RetSqlName("SZP")+" (NOLOCK) ZP "
    //cQuery += " INNER JOIN "+RetSqlName("SZR")+" (NOLOCK) ZR ON (ZR_NUMPF = ZP_NUMPF AND ZR.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SZJ")+" (NOLOCK) ZJ ON (ZJ_NUMPF = ZP_NUMPF AND ZJ.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SC5")+" (NOLOCK) C5 ON (C5_FILIAL = ZJ_FILIAL AND C5_NUM = ZJ_PEDIDO AND C5.D_E_L_E_T_ = '') "
    cQuery += " WHERE ZP.D_E_L_E_T_ = '' AND C5_TPPED NOT IN ('016') "
    cQuery += " AND ZP_DATA BETWEEN '"+ DTOS(mv_par01) +"'  AND '"+ DTOS(mv_par02) + "' " 

	IF !EMPTY(mv_par03)	
		cQuery += " AND ZP_NUMPF BETWEEN  '"+mv_par03+"' AND '"+mv_par04+"' "	
	ENDIF
    
    IF !EMPTY(mv_par05)
    	cQuery += " AND ZP_PEDIDO = '"+Alltrim(mv_par05)+"' "
    ENDIF
       
    cQuery += " GROUP BY ZP_PEDIDO,C5_EMISSAO,C5_CLIENT,C5_XNOMCLI,C5_XCANALD,ZP_NUMPF, ZP_USR, ZP_NOMEUSR,ZP_DATA, ZJ_CONF, ZJ_LOTDAP, ZJ_DATA, ZP_FILIAL ORDER BY ZP_DATA, ZP_PEDIDO "
	
       	
	TCQUERY cQuery NEW ALIAS TMP

	While TMP->(!EOF())
	    
	    cNomeConf := UsrRetName(TMP->COD_USUARIO)

		If Empty(Alltrim(cNomeConf))
			cNomeConf := Alltrim(TMP->CONFERENTE)
		EndIf	
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("PEDIDO"):SetValue(TMP->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_EMISSAO"):SetValue(TMP->DATA_EMISSAO)
		oSection1:Cell("DATA_EMISSAO"):SetAlign("LEFT")		
		
		oSection1:Cell("DATA_LIBERACAO"):SetValue(TMP->DATA_LIBERACAO)
		oSection1:Cell("DATA_LIBERACAO"):SetAlign("LEFT")		
		
		oSection1:Cell("COD_CLIENTE"):SetValue(TMP->COD_CLIENTE)
		oSection1:Cell("COD_CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("CLIENTE"):SetValue(TMP->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("CANAL"):SetValue(TMP->CANAL)
		oSection1:Cell("CANAL"):SetAlign("LEFT")
				
		oSection1:Cell("DAP"):SetValue(TMP->DAP)
		oSection1:Cell("DAP"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_PRE"):SetValue(TMP->DATA_PRE)
		oSection1:Cell("DATA_PRE"):SetAlign("LEFT")
		
		oSection1:Cell("NUM_PREFAT"):SetValue(TMP->NUM_PREFAT)
		oSection1:Cell("NUM_PREFAT"):SetAlign("LEFT")
				
		oSection1:Cell("QTDE_PRE"):SetValue(TMP->QTDE_PRE)
		oSection1:Cell("QTDE_PRE"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_CONF"):SetValue(TMP->QTDE_CONF)
		oSection1:Cell("QTDE_CONF"):SetAlign("LEFT")
				
		oSection1:Cell("COD_USUARIO"):SetValue(TMP->COD_USUARIO)
		oSection1:Cell("COD_USUARIO"):SetAlign("LEFT")
		
		oSection1:Cell("CONFERENTE"):SetValue(cNomeConf)
		oSection1:Cell("CONFERENTE"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_CONF_INI"):SetValue(TMP->DATA_CONF_INI)
		oSection1:Cell("DATA_CONF_INI"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_CONF_FIM"):SetValue(TMP->DATA_CONF_FIM)
		oSection1:Cell("DATA_CONF_FIM"):SetAlign("LEFT")		
		
		oSection1:Cell("HORA_INI"):SetValue(TMP->HORA_INI)
		oSection1:Cell("HORA_INI"):SetAlign("LEFT")
		
		oSection1:Cell("HORA_FIM"):SetValue(TMP->HORA_FIM)
		oSection1:Cell("HORA_FIM"):SetAlign("LEFT")
		
		oSection1:Cell("STATUS_CONF"):SetValue(TMP->STATUS_CONF)
		oSection1:Cell("STATUS_CONF"):SetAlign("LEFT")
										
		oSection1:PrintLine()
		
		TMP->(DBSKIP()) 
	enddo
	TMP->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Ronaldo Pereira
@since 26 de Julho de 2018
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Data de ?"		        		,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Data ate ?"                     ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Num Prefat. de ?"		        ,""		,""		,"mv_ch3","C",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Num Prefat. ate ?"		        ,""		,""		,"mv_ch4","C",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Pedido ?"		                ,""		,""		,"mv_ch5","D",06,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
Return
	