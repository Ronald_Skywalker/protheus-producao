//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de Faturamento Expedi��o.
Rotina para gerar todos os pedidos faturados da Expei��o num per�odo espec�fico;

@author Ronaldo Pereira
@since 12 de Novembro de 2021
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL062()

	Local aRet := {}
	Local aPerg := {}
	
	aAdd(aPerg,{1,"Data Faturamento de"		,dDataBase,PesqPict("SF2", "F2_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) 
	aAdd(aPerg,{1,"Data Faturamento at�" 	,dDataBase,PesqPict("SF2", "F2_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) 
	aAdd(aPerg,{1,"Pedido Venda: " 		    ,Space(6) ,""            ,""		    ,""	    ,""		,0	    ,.F.}) 
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Faturamento '+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "FAT"
    Local cTable1    	:= "Faturamento Expedi��o" 
    Local cPolitica     := GETMV("HP_RPOLFAT")

    //HP_RPOLFAT:
    //099 - POLITICA BENEFICIAMENTO  
    //344 - POL�TICA TRANSFER�NCIA                                      
    //345 - POL�TICA REMESSA COMODATO   
    //347 - POL�CITA DE SUCATA   
    //348 - POL�TICA REVENDA PARA FAC��O                                
    //349 - VENDA DE ATIVO IMOBILIZADO                                  
    //354 - REMESSA PARA CONSERTO                                       
    //366 - OUTRAS SA�DAS  

        
    //Dados tabela 1          
    cQry := " SELECT T.EMISSAO, "
    cQry += "        T.PEDIDO, " 
    cQry += "        T.PRE_FAT, "
    cQry += "        T.CLIENTE, "
    cQry += "        T.CANAL_VENDA, "
    cQry += "        T.POLITICA_COMERCIAL, "
    cQry += "        T.TIPO_PEDIDO, "
    cQry += "        T.MARCA, "
    cQry += "        T.SUBCOLECAO, "
    cQry += "        T.SITUACAO_PROD, "
    cQry += "        T.ITEM, "
    cQry += "        T.PRODUTO, " 
    cQry += "        T.QTD_VENDA, " 
    cQry += "        T.QTD_ENTREGUE, "
    cQry += "        ISNULL((T.QTD_VENDA - T.QTD_ENTREGUE)- T.PERDA,0) AS SALDO, "
    cQry += "        T.PERDA, "
    cQry += "        ROUND(((T.QTD_ENTREGUE/T.QTD_VENDA) * 100),2) AS PERC_ATEND, "
    cQry += "        T.PRECO, "
    cQry += "        T.VALOR, "
    cQry += "        T.NOTA_FISCAL, " 
    cQry += "        T.DATA_FATURA "
    cQry += " FROM ( "
    cQry += " SELECT CONVERT(CHAR, CAST(C5.C5_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
    cQry += "        C6.C6_NUM AS PEDIDO,  "
    cQry += "        ISNULL(C9.C9_XNUMPF,'') AS PRE_FAT, "
    cQry += "        C5.C5_CLIENTE +' - '+ C5.C5_XNOMCLI AS CLIENTE, "
    cQry += "        C5.C5_XCANALD AS CANAL_VENDA, "
    cQry += "        C5.C5_POLCOM +' - '+ C5.C5_XDESCPO AS POLITICA_COMERCIAL, "
    cQry += "        C5.C5_TPPED + ' - ' + Z1.Z1_DESC AS TIPO_PEDIDO, "
    cQry += "        B1.B1_XDESCMA AS MARCA, "
    cQry += "        ZA.ZAH_CODIGO + ' - ' + ISNULL(ZA.ZAH_DESCRI, '') AS SUBCOLECAO, "
    cQry += "        B1.B1_YSITUAC AS SITUACAO_PROD, "
    cQry += "        C6.C6_ITEM AS ITEM, "
    cQry += "        C6.C6_PRODUTO AS PRODUTO, "
    cQry += "        C6.C6_QTDVEN AS QTD_VENDA, " 
    cQry += "        C6.C6_QTDENT AS QTD_ENTREGUE, "

    cQry += "        ISNULL((SELECT (C6_QTDVEN - C6_QTDENT) FROM "+RetSqlName("SC6")+" AS SC6 WITH (NOLOCK) "
    cQry += "            WHERE D_E_L_E_T_ = '' AND SC6.C6_FILIAL = C6.C6_FILIAL " 
    cQry += "                                  AND SC6.C6_NUM = C6.C6_NUM " 
    cQry += "                                  AND SC6.C6_ITEM = C6.C6_ITEM "
    cQry += "                                  AND SC6.C6_PRODUTO = C6.C6_PRODUTO " 
    cQry += "                                  AND SC6.C6_BLQ = 'R' "
    cQry += "        ),0) AS PERDA, "

    cQry += "        C6.C6_PRCVEN AS PRECO, "
    cQry += "        C6.C6_VALOR AS VALOR, "
    cQry += "        ISNULL(D2.D2_DOC,'') AS NOTA_FISCAL, " 
    cQry += "        ISNULL(CONVERT(CHAR, CAST(D2.D2_EMISSAO AS SMALLDATETIME), 103),'') AS DATA_FATURA "
    cQry += " FROM "+RetSqlName("SC6")+" AS C6 WITH (NOLOCK) " 
    cQry += " INNER JOIN "+RetSqlName("SC5")+" AS C5 WITH (NOLOCK) ON (C5.C5_FILIAL = C6.C6_FILIAL "
    cQry += "                                                      AND C5.C5_NUM = C6.C6_NUM "
    cQry += "                                                      AND C5.C5_CLIENTE = C6.C6_CLI "
    cQry += "                                                      AND C5.D_E_L_E_T_ = '') "
    cQry += " INNER JOIN "+RetSqlName("SZ1")+" AS Z1 WITH (NOLOCK) ON (Z1.Z1_CODIGO = C5.C5_TPPED " 
    cQry += "                                                      AND Z1.D_E_L_E_T_ = '') "
    cQry += " INNER JOIN "+RetSqlName("SA1")+" AS A1 WITH (NOLOCK) ON (C5.C5_CLIENTE = A1.A1_COD " 
    cQry += "                                                      AND C5.C5_LOJACLI = A1.A1_LOJA " 
    cQry += "                                                      AND A1.D_E_L_E_T_ = '') "   
    cQry += " LEFT  JOIN "+RetSqlName("ZA4")+" AS A4 WITH (NOLOCK) ON (A1.A1_XMICRRE = A4.ZA4_CODMAC " 
    cQry += "                                                      AND A4.D_E_L_E_T_ = '') "
    cQry += " INNER JOIN "+RetSqlName("SB1")+" AS B1 WITH (NOLOCK) ON (B1.B1_COD =  C6.C6_PRODUTO " 
    cQry += "                                                      AND B1.D_E_L_E_T_ = '') " 
    cQry += " LEFT  JOIN "+RetSqlName("ZAH")+" AS ZA WITH (NOLOCK) ON (ZA.ZAH_CODIGO = B1.B1_YSUBCOL "
    cQry += "                                                     AND ZA.D_E_L_E_T_ = '') "
    cQry += " LEFT JOIN  "+RetSqlName("SD2")+" AS D2 WITH (NOLOCK) ON (C6.C6_FILIAL = D2.D2_FILIAL "
    cQry += "                                                      AND C6.C6_NUM = D2_PEDIDO "
    cQry += "                                                      AND C6.C6_CLI = D2.D2_CLIENTE "
    cQry += "                                                      AND C6.C6_ITEM = D2.D2_ITEMPV "
    cQry += "                                                      AND C6.C6_PRODUTO = D2.D2_COD "
    cQry += "                                                      AND D2.D_E_L_E_T_ = '') "
    cQry += " LEFT  JOIN "+RetSqlName("SC9")+" AS C9 WITH (NOLOCK) ON (C9.C9_FILIAL = D2.D2_FILIAL "
    cQry += "                                                      AND C9.C9_PEDIDO = D2_PEDIDO "
    cQry += "                                                      AND C9.C9_CLIENTE = D2.D2_CLIENTE "
    cQry += "                                                      AND C9.C9_ITEM = D2.D2_ITEMPV "
    cQry += "                                                      AND C9.C9_PRODUTO = D2.D2_COD "
    cQry += "                                                      AND C9.C9_NFISCAL = D2.D2_DOC "
    cQry += "                                                      AND C9.D_E_L_E_T_ = '') "
    cQry += " WHERE C6.D_E_L_E_T_ = '' AND C5.C5_POLCOM NOT IN ("+cPolitica+") "                                      
    cQry += " AND C6.C6_NUM IN (SELECT SC6.C6_NUM FROM "+RetSqlName("SC6")+" AS SC6 WITH (NOLOCK) " 
    cQry += "                   INNER JOIN "+RetSqlName("SF2")+" AS F2 WITH (NOLOCK) ON (F2.D_E_L_E_T_ = '' "
    cQry += "                                                       AND F2.F2_FILIAL = SC6.C6_FILIAL " 
    cQry += "                                                       AND F2.F2_DOC = SC6.C6_NOTA " 
    cQry += "                                                       AND F2.F2_CLIENTE = SC6.C6_CLI "
    cQry += "                                                       AND F2.F2_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"') "
    cQry += "                   WHERE SC6.D_E_L_E_T_ = '' AND SC6.C6_FILIAL = '0101' AND SC6.C6_LOCAL = 'E0' " 
                                If !Empty(aRet[3])
                                    cQry += " AND C6.C6_NUM = 'RELMBT' "
                                EndIf
    cQry += "                   GROUP BY SC6.C6_NUM) "

    cQry += " ) T "
    cQry += " ORDER BY T.PEDIDO, " 
    cQry += "          T.PRE_FAT, "
    cQry += "          T.DATA_FATURA "
                             
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas   
        oFWMsExcel:AddColumn(cPlan1,cTable1,"EMISSAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PEDIDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRE_FAT",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CLIENTE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CANAL_VENDA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"POLITICA_COMERCIAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO_PEDIDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MARCA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SUBCOLECAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SITUACAO_PROD",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ITEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRODUTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTD_VENDA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTD_ENTREGUE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SALDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PERDA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PERC_ATEND",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRECO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"VALOR",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NOTA_FISCAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DATA_FATURA",1)
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                                QRY->EMISSAO,;
                                                QRY->PEDIDO,;
                                                QRY->PRE_FAT,;
                                                QRY->CLIENTE,;
                                                QRY->CANAL_VENDA,;
                                                QRY->POLITICA_COMERCIAL,;
                                                QRY->TIPO_PEDIDO,;
                                                QRY->MARCA,;
                                                QRY->SUBCOLECAO,;
                                                QRY->SITUACAO_PROD,;
                                                QRY->ITEM,;
                                                QRY->PRODUTO,;
                                                QRY->QTD_VENDA,;
                                                QRY->QTD_ENTREGUE,;
                                                QRY->SALDO,;
                                                QRY->PERDA,;
                                                QRY->PERC_ATEND,;
                                                QRY->PRECO,;
                                                QRY->VALOR,;
                                                QRY->NOTA_FISCAL,;
                                                QRY->DATA_FATURA;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return
