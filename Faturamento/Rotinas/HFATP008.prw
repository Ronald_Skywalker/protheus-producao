#Include "PROTHEUS.CH"

User Function HFATP008()                        
	local cVldAlt := ".T." 
	local cVldExc := ".T." 
	local cAlias
	
	cAlias := "SZA"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	private cCadastro := "Cadastro de Macro Regi�o"
	aRotina := {;
		{ "Pesquisar" , "AxPesqui", 0, 1},;
		{ "Visualizar", "u_MacReg(4)", 0, 2},;
		{ "Incluir"   , "u_MacReg(1)", 0, 3},;
		{ "Alterar"   , "u_MacReg(2)", 0, 4},;
		{ "Exlcuir"   , "u_MacReg(3)", 0, 5};
		}
	dbSelectArea(cAlias)
	mBrowse( 6, 1, 22, 75, cAlias)
	
return

User Function MacReg(_tp)


Local oButton1
Local oButton2
Local oGet1
Local oGet2
Local oGet3
Local oGet4
Local oSay1
Local oSay2
Local oSay3
Local oSay4
Private oFolder1
Private cGet1 := Space(6)
Private cGet2 := Space(40)
Private cGet3 := Space(6)
Private cGet4 := Space(6)
Private cGet5 := Space(6)
Private cGet6 := Space(6)

Static oDlg

If _tp <> 1
	cGet1 := SZA->ZA_CODIGO
	cGet2 := SZA->ZA_DESC
	cGet3 := SZA->ZA_REGION
	cGet4 := SZA->ZA_REPRES
	cGet5 := SZA->ZA_SUPERV
	cGet6 := SZA->ZA_GERENT
Endif
  DEFINE MSDIALOG oDlg TITLE "Macro Regi�o" FROM 000, 000  TO 500, 900 COLORS 0, 16777215 PIXEL

    @ 011, 015 SAY oSay1 PROMPT "C�digo" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	If _tp <> 1
	    @ 011, 049 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	Else
	    @ 011, 049 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
	Endif
    @ 012, 130 SAY oSay2 PROMPT "Descri��o" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 010, 166 MSGET oGet2 VAR cGet2 Picture "@!" SIZE 259, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 033, 014 SAY oSay3 PROMPT "Regional" SIZE 052, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 030, 076 MSGET oGet3 VAR cGet3 Picture "@!" SIZE 073, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 031, 193 SAY oSay4 PROMPT "Representante" SIZE 056, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 031, 269 MSGET oGet4 VAR cGet4 Picture "@!" SIZE 071, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 031, 193 SAY oSay5 PROMPT "Supervidor" SIZE 056, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 031, 269 MSGET oGet5 VAR cGet5 Picture "@!" SIZE 071, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 031, 193 SAY oSay6 PROMPT "Gerente" SIZE 056, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 031, 269 MSGET oGet6 VAR cGet6 Picture "@!" SIZE 071, 010 OF oDlg COLORS 0, 16777215 PIXEL

    @ 049, 002 FOLDER oFolder1 SIZE 426, 176 OF oDlg ITEMS "Regi�es Vinculadas" COLORS 0, 16777215 PIXEL
    fMSNewGe1(_tp)
    @ 233, 388 BUTTON oButton1 PROMPT "Salvar" SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
    @ 233, 344 BUTTON oButton2 PROMPT "Cancelar" SIZE 037, 012 ACTION oDlg:End()  OF oDlg PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------ 
Static Function fMSNewGe1(_tp)
//------------------------------------------------ 
Local nX
Local aFieldFill := {}
Local aFields := {"ZB_REGION","ZB_DESC"}
Local aAlterFields := {"ZB_REGION"}
Private aHeader1 := {}
Private aCols1 := {}

Static oMSNewGe1

  // Define field properties
  DbSelectArea("SX3")
  SX3->(DbSetOrder(2))
  For nX := 1 to Len(aFields)
    If SX3->(DbSeek(aFields[nX]))
      Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
                       SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
    Endif
  Next nX

  // Define field values
  If _tp <> 1
  	DbSelectArea("SZB")
  	DbSetOrder(1)
  	DbSeek(xfilial("SZB")+SZA->ZA_CODIGO)

	If Found()  	
  		While !EOF() .and. SZB->ZB_MACREG == SZA->ZA_CODIGO
	  		Aadd(aCols1,{SZB->ZB_REGION,SZB->ZB_DESC,.F.})  
  			DbSkip()
  		End
  	Else
	  	For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
  			SX3->(DbSetOrder(2))
    		If DbSeek(aFields[nX])
      		Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
    		Endif
  		Next nX
  		Aadd(aFieldFill, .F.)
  		Aadd(aCols1, aFieldFill)
  	Endif
  Else
  	For nX := 1 to Len(aFields)
		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
    	If DbSeek(aFields[nX])
      	Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
    	Endif
  	Next nX
  	Aadd(aFieldFill, .F.)
  	Aadd(aCols1, aFieldFill)
  Endif
  
  oMSNewGe1 := MsNewGetDados():New( 000, 000, 161, 421, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[1], aHeader1, aCols1)

Return

Static Function Save(_tp)

Local I

DbSelectArea("SZA")
DbSetOrder(1)
DbSeek(xfilial("SZA")+cGet1)

If !Found()
	RecLock("SZA",.T.)
	Replace ZA_FILIAL	with xfilial("SZ2")
	Replace ZA_CODIGO	with cGet1
Else
	RecLock("SZ2",.F.)
Endif
	Replace ZA_DESC		with cGet2
	Replace ZA_REGION	with cGet3
	Replace ZA_REPRES	with cGet4
	Replace ZA_SUPERV	with cGet5
	Replace ZA_GERENT	with cGet6
MsUnLock()

If _tp == 1
	For i := 1 to Len(OMSNEWGE1:ACOLS)
		If OMSNEWGE1:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE1:ACOLS[i,1]) <> ""
			RecLock("SZB",.T.)
				Replace ZB_FILIAL		with xfilial("SZB")
				Replace ZB_MACREG		with cGet1
				Replace ZB_REGION		with OMSNEWGE1:ACOLS[i,1]
				Replace ZB_DESC			with OMSNEWGE1:ACOLS[i,2]			
			MsUnLock()	
		Endif
	Next
ElseIf _tp == 2
	For i := 1 to Len(OMSNEWGE1:ACOLS)
		If OMSNEWGE1:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE1:ACOLS[i,1]) <> ""
			DbSelectArea("SZB")
			DbSetOrder(1)
			DbSeek(xfilial("SZB")+cGet1+OMSNEWGE1:ACOLS[i,1])
			
			If !Found()
				RecLock("SZB",.T.)
					Replace ZB_FILIAL		with xfilial("SZB")
					Replace ZB_MACREG		with cGet1
					Replace ZB_REGION		with OMSNEWGE1:ACOLS[i,1]
					Replace ZB_DESC			with OMSNEWGE1:ACOLS[i,2]			
				MsUnLock()
			Endif	
		Else
			DbSelectArea("SZB")
			DbSetOrder(1)
			DbSeek(xfilial("SZB")+cGet1+OMSNEWGE1:ACOLS[i,1])
			
			If Found()
				RecLock("SZB",.F.)
					DbDelete()			
				MsUnLock()
			Endif	
		Endif
	Next
ElseIf _tp == 3
	DbSelectArea("SZA")
	DbSetOrder(1)
	DbSeek(xfilial("SZA")+cGet1)

	If Found()
		RecLock("SZA",.F.)
			DbDelete()
		MsUnLock()
	Endif
	
Endif

Return