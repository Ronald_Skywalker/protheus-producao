#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

//Importar direto na tabela

User Function HIMPPEDANT()


	Processa( {|| U_SC5CSVant(.T.) }, "Aguarde...", "Processando...",.F.) //.T. Importa .F. Simula

Return

User Function SC5CSVant(lImporta)
	Local nPos   := 0
	Local cLinha := ""
	Local cCab   := ""
	Local nIni   := 0
	Local i      := 0
	Local x      := 0
	Local aCSV   := {}
	Local nCols  := 0
	Local cQuery := ""
    Local qtd    := 0

	//cQuery := "SELECT TOP 10 * FROM MOBILE.dbo.HISTORICO_PEDIDO "

	cQuery := "SELECT ID_PEDIDO as IDPEDIDO,	CNPJ_CLI AS CNPJ ,EMISSAO AS EMISSAO, ENTREGA	AS ENTREGA, "  
	cQuery += "COD_POL AS POLITICA , 	COD_PRAZ AS CODPRAZ ,	TAB_PRECO	AS TABPRECO ,CONDPAGTO	AS CONDPAGTO , DESCONTO	AS DESCONTO , "
	cQuery += "REPRES	AS REPRES , GERENTE	AS GERENTE , SUPERVISOR AS SUPERVISOR, 	OBS	AS OBSERV , PEDIDO_SIGA	AS PEDSIGA , DT_IMPORTACAO	AS DTAIMP , NFISCAL AS NFISCAL, " 
	cQuery += "SERIE_NF AS SERIE , EMIS_NF AS DTAEMIS,	INSTRUCAO AS INSTRU , 	COMPLETO	AS COMPL , PED_MILL AS PEDIMIL , 	TP_PEDIDO AS TPOPED "
	cQuery += "FROM MOBILE.dbo.HISTORICO_PEDIDO "


	//cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRB", .T., .T.)

	TcSetField("XTRB","EMISSAO","D",08,0)
	TcSetField("XTRB","ENTREGA","D",08,0)

	cNumReg := "000000"

	cLog := "Pedidos abaixo importados com sucesso na SC5: SIM/NAO "
	cLog += CRLF + "REG/PROTHEUS / PEDIDO_MILLENIUM   Importado"

	cEFIL := "Erro Filial n�o encontrada:"
	cEDA0 := "Tabelas de pre�o n�o encontradas:"
	cESA1 := "Cliente n�o encontrado:"
	cESA4 := "Transportadora n�o encontrada:"
	cESE4 := "Condicao de Pagamento n�o encontrada:"
	cESA3 := "Vendedor n�o encontrado:"                                                      
	cESZ2 := "Politica comercial nao encontrada:"
	cESZ4 := "Tipo de Pedido Nao encontrado:"
	cESZ6 := "Prazo nao encontrado:"

	lEFIL := .F.
	lEDA0 := .F.
	lESA1 := .F.
	lESA4 := .F.
	lESE4 := .F.
	lESA3 := .F.
	lESZ2 := .F.
	lESZ4 := .F.
	lESZ6 := .F.

	//IDPEDIDO	CNPJ	EMISSAO	ENTREGA	POLITICA	CODPRAZ	TABPRECO	CONDPAGTO	DESCONTO	REPRES	GERENTE	SUPERVISOR	OBSERV	PEDSIGA	
	//DTAIMP	NFISCAL	SERIE	DTAEMIS	INSTRU	COMPL	PEDIMIL	TPOPED
	xv:= 0
	XTRB->(DbGoTop())
	lFaz := .F.

	While XTRB->(!EOF())
		cFil := "0101" 	
		xv++
		IF ALLTRIM(XTRB->PEDIMIL)    == 'B2B-1750050' .and.  !lFaz 
			xv := 10
			lFaz := .t.
		Else
			if !lFaz
				xv := 0
			Endif   
		Endif

		if xv >= 11
			If !Empty(XTRB->CNPJ)
				DbSelectArea("SA1")
				DbSetOrder(3)
				If DbSeek(xFilial("SA1")+PADR(XTRB->CNPJ,14))
					cCliente := SA1->A1_COD
					cLoja    := SA1->A1_LOJA
					cCanal   := SA1->A1_XCANAL 
					cDCanal  := SA1->A1_XCANALD
					cEmp     := SA1->A1_XEMP 
					cDEmp    := SA1->A1_XDESEMP
					cGrupo   := SA1->A1_XGRUPO
					cDGrupo  := SA1->A1_XGRUPON
					cDiv     := SA1->A1_XCODDIV
					cDDiv    := SA1->A1_XNONDIV 
					cMacro   := SA1->A1_XMICRRE
					cDMacro  := SA1->A1_XMICRDE
					cReg     := SA1->A1_XCODREG 
					cDReg    := SA1->A1_XDESREG
					cNomecli := SA1->A1_NOME
				else
					cESA1 += CRLF + "CNPJ Cliente : "+XTRB->CNPJ+" Pedido: "+XTRB->PEDIMIL+" Erro: Cliente n�o encontrado CNPJ: "+XTRB->CNPJ
					lESA1 := .T.                                                    
					XTRB->(DbSkip())
					Loop	

				Endif	
			Else
				cESA1 += CRLF + "CNPJ Cliente "+XTRB->CNPJ+" Pedido: "+XTRB->PEDIMIL+" Erro: CNPJ Inexistente?: "+XTRB->CNPJ
				lESA1 := .T.
				xTRB->(DbSkip())
				Loop	
			EndIf
			cCond   := XTRB->CONDPAGTO
			cTransp := ""
			cVend   := XTRB->REPRES
			nVComi  := 0
			cNVend  := ""
			cGer    := XTRB->GERENTE	
			nGComi  := 0
			cNGer   := XTRB->GERENTE
			cPolCom := XTRB->POLITICA
			cTpPed  := XTRB->TPOPED
			cPrazo  := XTRB->CODPRAZ
			cTabela := XTRB->TABPRECO
			//IDPEDIDO	CNPJ	EMISSAO	ENTREGA	POLITICA	CODPRAZ	TABPRECO	CONDPAGTO	DESCONTO	REPRES	GERENTE	SUPERVISOR	OBSERV	PEDSIGA	
			//DTAIMP	NFISCAL	SERIE	DTAEMIS	INSTRU	COMPL	PEDIMIL	TPOPED
            IF Select("XPED") > 0
		       XPED->(dbCloseArea())
	        Endif
			xCquery := " SELECT * FROM SC5010 WITH (NOLOCK) WHERE C5_XPEDMIL = '"+XTRB->PEDIMIL+"' AND D_E_L_E_T_ = '' "                              
			DbUseArea( .T., 'TOPCONN', TCGENQRY(,,xCquery),"Xped", .F., .T.)
			DbSelectArea("XPED")
			XPED->(DbGoTop())
			IF XPED->(EOF() )
				cNumPed := "I"+GetMV("MV_XPEDIMP")
				DbSelectArea("SC5")	
				RecLock("SC5",.T.)
				SC5->C5_FILIAL  := '0101' 
				SC5->C5_NUM     := cNumPed 
				SC5->C5_TIPO    := "N"                    //20160317
				SC5->C5_EMISSAO := XTRB->EMISSAO //ctod(Substr(XTRB->EMISSAO,7,2)+'/'+Substr(XTRB->EMISSAO,5,2)+'/'+Substr(XTRB->EMISSAO,3,2))   
				SC5->C5_FECENT  := XTRB->ENTREGA //ctod(Substr(XTRB->ENTREGA,7,2)+'/'+Substr(XTRB->ENTREGA,5,2)+'/'+Substr(XTRB->ENTREGA,3,2)) //StoD(XTRB->ENTREGA)
				SC5->C5_TABELA  := cTabela
				SC5->C5_CLIENTE := cCliente
				SC5->C5_LOJACLI := cLoja
				SC5->C5_CLIENT  := cCliente
				SC5->C5_LOJAENT := cLoja
				SC5->C5_NOTA    := XTRB->NFISCAL 
				SC5->C5_SERIE   := str(XTRB->SERIE)  //'2'
				SC5->C5_XCANAL  := cCanal
				SC5->C5_XCANALD := cDCanal
				SC5->C5_XEMP    := cEmp
				SC5->C5_XDESEMP := cDEmp
				SC5->C5_XGRUPO  := cGrupo
				SC5->C5_XGRUPON := cDGrupo
				SC5->C5_XCODDIV := cDiv
				SC5->C5_XNONDIV := cDDiv
				SC5->C5_XMICRRE := cMacro
				SC5->C5_XMICRDE := cDMacro
				SC5->C5_XCODREG := cReg
				SC5->C5_XDESREG := cDReg
				SC5->C5_TRANSP  := cTransp
				SC5->C5_CONDPAG := cCond
				SC5->C5_XDESCOD := AllTrim(Posicione("SE4",1,xFilial('SE4')+PADR(cCond,3),"E4_DESCRI"))
				SC5->C5_VEND1   := cVend
				SC5->C5_COMIS1  := nVComi
				SC5->C5_NOMVEND := cNVend
				SC5->C5_VEND2   := cGer
				SC5->C5_COMIS2  := nGComi
				SC5->C5_NOMGERE := cNGer
				SC5->C5_POLCOM  := cPolCom
				SC5->C5_PRAZO   := cPrazo
				SC5->C5_TPPED   := XTRB->TPOPED
				//SC5->C5_XPEDRAK := TRB->PEDRAK
				SC5->C5_XPEDWEB := XTRB->PEDIMIL
				SC5->C5_XPEDMIL := XTRB->PEDIMIL
				SC5->C5_XBLQ    := 'B'
				SC5->C5_XINTEGR := "S"
				//		SC5->C5_OBS     := XTRB->OBSERV+' '+XTRB->INSTRU+' '+XTRB->COMPL
				SC5->C5_XOBSINT := XTRB->OBSERV+' '+XTRB->COMPL
				SC5->C5_XCDINST := XTRB->INSTRU
				SC5->C5_MSBLQL  := '2'
				SC5->C5_XDESCPO := 'POLITICA ANTIGA MILLENIUM" 
				SC5->C5_XNOMCLI := cNomecli
				SC5->C5_ORIGEM  := 'IMPOR ANTIGA' 
				SC5->C5_TPCARGA := '2'
				MsUnlock()
				U_SC6CSVant(XTRB->PEDIMIL,cNumPed)
				cProx := SOMA1(GetMV("MV_XPEDIMP"),5)    //XPEDIMP
				PutMV("MV_XPEDIMP",cProx)	
				cNumReg := SOMA1(cNumReg,6)
				cLog += CRLF + cNumReg + " - " + xTRB->CNPJ+" - "+xTRB->PEDIMIL
			ELSE
				cLog += CRLF + XPED->C5_NUM + " - " + xTRB->CNPJ+" - "+xTRB->PEDIMIL
			endif
			
		endif   	                                                  	                                                                                                                                                                                                                                                              	 
        
		XTRB->(DbSkip())
	EndDo

	XTRB->(DbCloseArea())

	MemoWrite("\log_import\SC5\_IMPORT_"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cLog)

	IF(lEFIL,MemoWrite("\log_import\SC5\FIL"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEFIL),)
	IF(lEDA0,MemoWrite("\log_import\SC5\DA0"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEDA0),)
	IF(lESA1,MemoWrite("\log_import\SC5\SA1"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESA1),)
	IF(lESA4,MemoWrite("\log_import\SC5\SA4"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESA4),)
	IF(lESE4,MemoWrite("\log_import\SC5\SE4"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESE4),)
	IF(lESA3,MemoWrite("\log_import\SC5\SA3"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESA3),)
	IF(lESZ2,MemoWrite("\log_import\SC5\SZ2"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESZ2),)
	IF(lESZ4,MemoWrite("\log_import\SC5\SZ4"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESZ4),)
	IF(lESZ6,MemoWrite("\log_import\SC5\SZ6"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESZ6),)

	MsgInfo("Feito SC5","Feito")

Return


User Function SC6CSVant(CPED_MILL,cNumPed)
	Local nPos   := 0
	Local cLinha := ""
	Local cCab   := ""
	Local nIni   := 0
	Local i      := 0
	Local x      := 0
	Local aCSV   := {}
	Local nCols  := 0
	Local cTes   := ""

	IF Select("XTRC") > 0
		XTRC->(dbCloseArea())
	Endif

	cTpOper := AllTrim(SuperGetMV("MV_XRTOV2C",.F.,"01")) //Tipo de Operacao de venda para pedidos de venda
	cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking

	cQuery := " SELECT CNPJ_FIL as CNPJ_FIL, PED_MIL as PED_MIL,COD_BAR	as COD_BAR ,COD_PRO  as COD_PRO, NF_FATU as NF_FATU ,QTDE as QTDE, QTDENT as QTDENT, " 
	cQuery += " SALDO as SALDO,PRECUNI as PRECUNI FROM MOBILE.dbo.HISTORICO_ITEM_PEDIDO WHERE PED_MIL ='"+XTRB->PEDIMIL+"' "

	//CNPJ_FIL	PED_MIL	COD_BAR	COD_PRO  	NF_FATU 	QTDE 	QTDENT 	SALDO 	PRECUNI

	//cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRC", .F., .T.)

	DbSelectArea("XTRC")

	ProcRegua(155000)

	cNumReg := "000000"

	cLog := "Pedidos abaixo importados com sucesso na SC6:"
	cLog += CRLF + "CNPJ_FILIAL / PEDIDO_MILLENIUM  / PRODUTO "

	cNumAnt := ""

	cEFIL := "Filial nao encontrada:"
	cESC5 := "Pedido Millenium nao encontrado: "
	cESB1 := "Produto nao encontrado: "
	cETES := "TES nao encontrada: "

	lEFIL := .F.
	lESC5 := .F.
	lESB1 := .F.
	lETES := .F.

	XTRC->(DbGoTop())

	nCont := 0
	cItem := "00"
	While XTRC->(!EOF())
		IncProc()
		MemoWrite("\log_import\SC6\_IMPORT_"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cLog)
		cLog := "Pedidos abaixo importados com sucesso na SC5:"
		cLog += CRLF + "CNPJ_FILIAL / PEDIDO_MILLENIUM"

		IF(lEFIL,MemoWrite("\log_import\SC6\FIL"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEFIL),)
		IF(lESC5,MemoWrite("\log_import\SC6\SC5"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESC5),)
		IF(lESB1,MemoWrite("\log_import\SC6\SB1"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESB1),)
		IF(lETES,MemoWrite("\log_import\SC6\TES"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cETES),)

		lEFIL := .F.
		lESC5 := .F.
		lESB1 := .F.
		lETES := .F.

		cEFIL := "Filial nao encontrada:"
		cESC5 := "Pedido Millenium nao encontrado: "
		cESB1 := "Produto nao encontrado: "
		cETES := "TES nao encontrada: "
		nCont := 0
		cFil := "0101" 	
		cNumPed := SC5->C5_NUM
		cCli    := SC5->C5_CLIENTE
		cLoj    := SC5->C5_LOJACLI
		If !Empty(XTRC->COD_BAR)
			DbSelectArea("SB1")
			DbSetOrder(5)
			//CNPJ_FIL	PED_MIL	COD_BAR	COD_PRO  	NF_FATU 	QTDE 	QTDENT 	SALDO 	PRECUNI
			If DbSeek(xFilial("SB1")+XTRC->COD_BAR)
				cProd := SB1->B1_COD
				cDesc := SB1->B1_DESC
				cUM   := SB1->B1_UM
			Else
				XTRC->(DbSkip())
				loop	
			EndIf
		Else
			XTRC->(DbSkip())
			Loop
		EndIf
		cTpOper:= "01"
		cTES  := MaTesInt(2,cTpOper,cCli,cLoj,"C",cProd,)
		If Empty(cTes)
			cETES += CRLF + "Fil:"+cFil+" Ped: "+cNumPed+" Prod: "+cProd+" Cli:"+cCli+"/"+cLoj
			lETES := .T.
			cTes := "999"
		EndIf
		DbSelectArea("SF4")
		DbSetOrder(1)
		If DbSeek(xFilial("SF4")+cTes)
			cCf := SF4->F4_CF
		Else
			cCf  := "5101"
		EndIf

		//	CNPJ_FIL	PED_MIL	COD_BAR	COD_PRO  	NF_FATU 	QTDE 	QTDENT 	SALDO 	PRECUNI
		//03007414000130	01829534	7891306143002	00034850_19_G	000507355	0	3	-3	15,11
		cItem := SOMA1(cItem,2)	
		cNumAnt := SC5->C5_NUM
		nQtdVen := XTRC->QTDENT
		nPrcVen := XTRC->PRECUNI
		nQtdEnt := XTRC->QTDE
		//CNPJ_FIL	PED_MIL	COD_BAR	COD_PRO  	NF_FATU 	QTDE 	QTDENT 	SALDO 	PRECUNI
		DbSelectArea("SC6")
		RecLock("SC6",.T.)
		SC6->C6_FILIAL  := cFil 
		SC6->C6_NUM     := cNumPed
		SC6->C6_ITEM    := cItem
		SC6->C6_PRODUTO := cProd
		SC6->C6_DESCRI  := cDesc
		SC6->C6_UM      := cUM
		SC6->C6_QTDVEN  := nQtdVen
		SC6->C6_QTDENT  := nQtdVen
		SC6->C6_PRCVEN  := nPrcVen
		SC6->C6_PRUNIT  := nPrcVen
		SC6->C6_VALOR   := Round(nQtdVen*nPrcVen,2)
		SC6->C6_TES     := cTES
		SC6->C6_LOCAL   := cAmzPik
		SC6->C6_QTDENT  := nQtdEnt
		SC6->C6_NOTA    := Right(XTRC->NF_FATU,9)
		SC6->C6_CF      := cCf
		SC6->C6_CLI     := cCli
		SC6->C6_LOJA    := cLoj
		SC6->C6_ENTREG  := XTRB->ENTREGA
		SC6->C6_UNSVEN  := nQtdVen
		SC6->C6_GRADE   := 'S'
		SC6->C6_ITEMGRD := '001'
		SC6->C6_CLASFIS := '000'
		SC6->C6_TPOP    := 'F'
		SC6->C6_SUGENTR := XTRB->ENTREGA
		SC6->C6_DTFIMNT := XTRB->ENTREGA
		SC6->C6_RATEIO  := '2'
		SC6->C6_TPPROD  := '1'
		SC6->C6_XNOTLIB := ''
		SC6->C6_DATCPL  := XTRB->ENTREGA
		SC6->C6_DATAEMB := XTRB->ENTREGA
		SC6->C6_INTROT  := ''
		SC6->C6_XINTEGR := "S" 
		MsUnlock()
		cNumReg := SOMA1(cNumReg,6)
		cLog += CRLF + cNumReg + XTRC->CNPJ_FIL+" - "+XTRC->PED_MIL+" - "+XTRC->COD_BAR
		nCont++
		XTRC->(DbSkip())
	EndDo

	MemoWrite("\log_import\SC6\_IMPORT_"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cLog)

	IF(lEFIL,MemoWrite("\log_import\SC6\FIL"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEFIL),)
	IF(lESC5,MemoWrite("\log_import\SC6\SC5"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESC5),)
	IF(lESB1,MemoWrite("\log_import\SC6\SB1"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESB1),)
	IF(lETES,MemoWrite("\log_import\SC6\TES"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cETES),)

	//MsgInfo("Feito SC6","Feito")

Return

User Function XINSRECNO()
	Local cQuery := ""

	cQuery := "select * from P12_HOM2.dbo.IMPITENS order by PEDMIL "

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"ITENS", .F., .T.)

	DbSelectArea("ITENS")

	While !EOF()

		ITENS->(DbSkip())
	EndDo

Return

/*
//Importar para um array

User Function HIMPPEDMIL()
Local aCabec := {}
Local aItens := {}
Local nx     := 0

PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101' TABLES 'SC5,SC6' MODULO 'FAT'

U_GETCSV("SC5",@aCabec,"D:\CABEC.csv",.t.) //.T. quando a primeira linha contem cabecalho 

U_GETCSV("SC6",@aItens,"D:\ITENS.csv",.t.) 

If Len(aCabec) > 0
For nx := 1 to Len(aCabex)


Next
EndIf

//cDebug := 0

RESET ENVIRONMENT

Return

User Function GETCSV(aMatriz,cArquivo,lUsaCab)
Local nPos   := 0
Local cLinha := ""
Local cCab   := ""
Local nIni   := 0
Local i      := 0
Local x      := 0
Local aCSV   := {}
Local nCols  := 0

Default lUsaCab := .F.

aMatriz := {}
If (!File(cArquivo))
Return
EndIf

// Abrindo o arquivo
FT_FUse(cArquivo)
FT_FGoTop()

// Capturando as linhas do arquivo
While (!FT_FEof())
If (Empty(cCab))
cCab := FT_FREADLN()
EndIf
If (lUsaCab)
AADD(aCSV,FT_FREADLN())
ElseIf (!lUsaCab) .and. (i > 0)
AADD(aCSV,FT_FREADLN())
EndIf
i++
FT_FSkip()
EndDo

FT_FUSE()

// Pegando o numero de colunas com base no cabecalho
For i := 1 to Len(cCab)
nPos := At(";",cCab)
If (nPos > 0)
nCols+= 1
cCab := SubStr(cCab,nPos+1,Len(cCab)-nPos)
EndIf
Next

// Definindo o tamanho da Matriz que recebera os dados
aMatriz := Array(Len(aCSV),nCols+1)

// Carregando os dados
For i := 1 to Len(aCSV)
cLinha := aCSV[i]
For x := 1 to nCols+1
nPos := At(";",cLinha)
If ( nPos > 0 )
aMatriz[i,x] := AllTrim(SubStr(cLinha,1,nPos-1))
cLinha := SubStr(cLinha,nPos+1,Len(cLinha)-nPos)
Else
aMatriz[i,x] := AllTrim(cLinha)
cLinha := ""
EndIf
Next x
Next i

Return*/