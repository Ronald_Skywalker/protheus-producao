#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

//Importar direto na tabela

User Function HIMPPEDMIL()

//PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101' TABLES 'SC5,SC6' MODULO 'FAT'

//Processa( {|| U_SC5GETCSV(.F.) }, "Aguarde...", "Processando...",.F.) //.T. Importa .F. Simula 

Processa( {|| U_SC6GETCSV(.T.) }, "Aguarde...", "Processando...",.F.) //.T. Importa .F. Simula

//RESET ENVIRONMENT

Return

User Function SC5GETCSV(lImporta)
Local nPos   := 0
Local cLinha := ""
Local cCab   := ""
Local nIni   := 0
Local i      := 0
Local x      := 0
Local aCSV   := {}
Local nCols  := 0
Local cQuery := ""
Default lUsaCab := .F.

cQuery := "select * from P12_HOM2.dbo.IMPCABEC"

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)

DbSelectArea("TRB")

cNumReg := "000000"

cLog := "Pedidos abaixo importados com sucesso na SC5:"
cLog += CRLF + "CNPJ_FILIAL / PEDIDO_MILLENIUM"

cEFIL := "Erro Filial n�o encontrada:"
cEDA0 := "Tabelas de pre�o n�o encontradas:"
cESA1 := "Cliente n�o encontrado:"
cESA4 := "Transportadora n�o encontrada:"
cESE4 := "Condicao de Pagamento n�o encontrada:"
cESA3 := "Vendedor n�o encontrado:"
cESZ2 := "Politica comercial nao encontrada:"
cESZ4 := "Tipo de Pedido Nao encontrado:"
cESZ6 := "Prazo nao encontrado:"

lEFIL := .F.
lEDA0 := .F.
lESA1 := .F.
lESA4 := .F.
lESE4 := .F.
lESA3 := .F.
lESZ2 := .F.
lESZ4 := .F.
lESZ6 := .F.

TRB->(DbGoTop())

While TRB->(!EOF())
	If TRB->CNPJFIL = "03007414000130"
		cFil := "0101" 	
	ElseIf TRB->CNPJFIL = "03007414000300"
		cFil := "0102"
	Else
		cEFIL += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: CNPJ n�o econtrado."
		lEFIL := .T.
		TRB->(DbSkip())
		Loop
	EndIf
	
	DbSelectArea("DA0")
	DbOrderNickName("TABMIL")
	If DbSeek(xFilial("DA0")+TRB->TABPRC)
		cTabela := DA0->DA0_CODTAB
	Else
		cEDA0 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Tabela de pre�o n�o encontrada: "+TRB->TABPRC
		lEDA0 := .T.
		cTabela := ""
		//TRB->(DbSkip())
		//Loop
	EndIf
	
	If !Empty(TRB->CNPJCLI)
		DbSelectArea("SA1")
		DbSetOrder(3)
		If DbSeek(xFilial("SA1")+TRB->CNPJCLI)
			cCliente := SA1->A1_COD
			cLoja    := SA1->A1_LOJA
			cCanal   := SA1->A1_XCANAL 
			cDCanal  := SA1->A1_XCANALD
			cEmp     := SA1->A1_XEMP 
			cDEmp    := SA1->A1_XDESEMP
			cGrupo   := SA1->A1_XGRUPO
			cDGrupo  := SA1->A1_XGRUPON
			cDiv     := SA1->A1_XCODDIV
			cDDiv    := SA1->A1_XNONDIV 
			cMacro   := SA1->A1_XMICRRE
			cDMacro  := SA1->A1_XMICRDE
			cReg     := SA1->A1_XCODREG 
			cDReg    := SA1->A1_XDESREG
		Else
			//cESA1 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Cliente n�o encontrado CNPJ: "+TRB->CNPJCLI
			//lESA1 := .T.
			//TRB->(DbSkip())
			//Loop
			DbSelectArea("SA1")
			DbOrderNickName("SA1XCOD")
			If DbSeek(xFilial("SA1")+TRB->CLIMIL)
				cCliente := SA1->A1_COD
				cLoja    := SA1->A1_LOJA
				cCanal   := SA1->A1_XCANAL 
				cDCanal  := SA1->A1_XCANALD
				cEmp     := SA1->A1_XEMP 
				cDEmp    := SA1->A1_XDESEMP
				cGrupo   := SA1->A1_XGRUPO
				cDGrupo  := SA1->A1_XGRUPON
				cDiv     := SA1->A1_XCODDIV
				cDDiv    := SA1->A1_XNONDIV 
				cMacro   := SA1->A1_XMICRRE
				cDMacro  := SA1->A1_XMICRDE
				cReg     := SA1->A1_XCODREG 
				cDReg    := SA1->A1_XDESREG
			Else
				cESA1 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Cliente n�o encontrado CNPJ: "+TRB->CNPJCLI+" XCOD : "+TRB->CLIMIL
				lESA1 := .T.
				TRB->(DbSkip())
				Loop	
			EndIf	
		EndIf
	Else
		DbSelectArea("SA1")
		DbOrderNickName("SA1XCOD")
		If DbSeek(xFilial("SA1")+TRB->CLIMIL)
			cCliente := SA1->A1_COD
			cLoja    := SA1->A1_LOJA
			cCanal   := SA1->A1_XCANAL 
			cDCanal  := SA1->A1_XCANALD
			cEmp     := SA1->A1_XEMP 
			cDEmp    := SA1->A1_XDESEMP
			cGrupo   := SA1->A1_XGRUPO
			cDGrupo  := SA1->A1_XGRUPON
			cDiv     := SA1->A1_XCODDIV
			cDDiv    := SA1->A1_XNONDIV 
			cMacro   := SA1->A1_XMICRRE
			cDMacro  := SA1->A1_XMICRDE
			cReg     := SA1->A1_XCODREG 
			cDReg    := SA1->A1_XDESREG
		Else
			cESA1 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Cliente n�o encontrado XCOD: "+TRB->CLIMIL
			lESA1 := .T.
			TRB->(DbSkip())
			Loop	
		EndIf	
	EndIf
	
	If !Empty(TRB->CNPJTRA)
		DbSelectArea("SA4")
		DbSetOrder(3)
		If DbSeek(xFilial("SA4")+TRB->CNPJTRA)
			cTransp := SA4->A4_COD
		Else
			cESA4 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Transportadora n�o encontrada: "+TRB->CNPJTRA
			lESA4 := .T.
			cTransp := ""
			//TRB->(DbSkip())
			//Loop
		EndIf
	Else
		cTransp := ""
	EndIf
	
	DbSelectArea("SE4")
	DbSetOrder(1)
	If DbSeek(xFilial("SE4")+TRB->CONDPAG)
		cCond  := SE4->E4_CODIGO
		cDCond := SE4->E4_DESCRI
	Else
		cESE4 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Condicao de pagamento n�o encontrada: "+TRB->CONDPAG
		lESE4 := .T.
		TRB->(DbSkip())
		Loop
	EndIf
	
	If !Empty(TRB->CNPJVEN)
		DbSelectArea("SA3")
		DbSetOrder(3)
		If DbSeek(xFilial("SA3")+TRB->CNPJVEN)
			cVend  := SA3->A3_COD
			nVComi := SA3->A3_COMIS
			cNVend := SA3->A3_NOME
		Else
			cESA3 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Vendedor n�o econtrado: "+TRB->CNPJVEN
			lESA3 := .T.
			TRB->(DbSkip())
			Loop
		EndIf
	Else
		cVend  := ""
		nVComi := 0
		cNVend := ""
	EndIf
	
	If !Empty(TRB->CNPJGER)
		DbSelectArea("SA3")
		DbSetOrder(3)
		If DbSeek(xFilial("SA3")+TRB->CNPJGER)
			cGer   := SA3->A3_COD
			nGComi := SA3->A3_COMIS2
			cNGer  := SA3->A3_NOME
		Else
			cESA3 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Gerente n�o econtrado: "+TRB->CNPJGER
			lESA3 := .T.
			TRB->(DbSkip())
			Loop
		EndIf
	Else
		cGer   := ""
		nGComi := 0
		cNGer  := ""
	EndIf
	
	DbSelectArea("SZ2")
	DbSetOrder(1)
	If DbSeek(xFilial("SZ2")+TRB->POLCOM)
		cPolCom := SZ2->Z2_CODIGO
	Else
		cESZ2 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Politica comercial nao encontrada: "+TRB->POLCOM
		lESZ2 := .T.
		TRB->(DbSkip())
		Loop
	EndIf
	
	DbSelectArea("SZ4")
	DbSetOrder(4)
	If DbSeek(xFilial("SZ4")+cPolCom+TRB->TPPED)
		cTpPed := SZ4->Z4_TPPED
	Else
		cESZ4 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Tipo de Pedido nao encontrado: "+cPolCom+TRB->TPPED
		lESZ4 := .T.
		TRB->(DbSkip())
		Loop
	EndIf
	
	DbSelectArea("SZ6")
	DbSetOrder(1)
	If DbSeek(xFilial("SZ6")+cPolCom+TRB->PRAZO)
		cPrazo := SZ6->Z6_PRAZO
	Else
		cESZ6 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Erro: Prazo nao encontrado: "+cPolCom+TRB->PRAZO
		lESZ6 := .T.
		TRB->(DbSkip())
		Loop
	EndIf
	
	//Ordem dos campos no CSV
	//   1          2       3       4      5          6          7          8        9        10          11     12    13    14    15      16      17
	//CNPJ_FILIAL,PEDMIL,EMISSAO,ENTREGA,TABELA,CNPJ_CLIENTE,CNPJ_TRANSP,CONDPAG,CNPJ_VEND,CNPJ_GERENTE,POLCOM,PRAZO,TPPED,PEDRAK,CLIMIL,APROVADO,OBS
	If lImporta
		cNumPed := "I"+GetMV("MV_XPEDIMP")
		
		DbSelectArea("SC5")
		RecLock("SC5",.T.)
		SC5->C5_FILIAL  := cFil 
		SC5->C5_NUM     := cNumPed 
		SC5->C5_TIPO    := "N"
		SC5->C5_EMISSAO := StoD(TRB->DTEMIS) 
		SC5->C5_FECENT  := StoD(TRB->DTENT)
		SC5->C5_TABELA  := cTabela
		SC5->C5_CLIENTE := cCliente
		SC5->C5_LOJACLI := cLoja
		SC5->C5_CLIENT  := cCliente
		SC5->C5_LOJAENT := cLoja
		SC5->C5_XCANAL  := cCanal
		SC5->C5_XCANALD := cDCanal
		SC5->C5_XEMP    := cEmp
		SC5->C5_XDESEMP := cDEmp
		SC5->C5_XGRUPO  := cGrupo
		SC5->C5_XGRUPON := cDGrupo
		SC5->C5_XCODDIV := cDiv
		SC5->C5_XNONDIV := cDDiv
		SC5->C5_XMICRRE := cMacro
		SC5->C5_XMICRDE := cDMacro
		SC5->C5_XCODREG := cReg
		SC5->C5_XDESREG := cDReg
		SC5->C5_TRANSP  := cTransp
		SC5->C5_CONDPAG := cCond
		SC5->C5_XDESCOD := cDCond
		SC5->C5_VEND1   := cVend
		SC5->C5_COMIS1  := nVComi
		SC5->C5_NOMVEND := cNVend
		SC5->C5_VEND2   := cGer
		SC5->C5_COMIS2  := nGComi
		SC5->C5_NOMGERE := cNGer
		SC5->C5_POLCOM  := cPolCom
		SC5->C5_PRAZO   := cPrazo
		SC5->C5_TPPED   := cTpPed
		SC5->C5_XPEDRAK := TRB->PEDRAK
		SC5->C5_XPEDWEB := If(!Empty(TRB->PEDRAK),TRB->PEDMIL,"")
		SC5->C5_XPEDMIL := TRB->PEDMIL
		SC5->C5_XBLQ    := TRB->STATUS
		SC5->C5_XINTEGR := "S"
		MsUnlock()
		
		cProx := SOMA1(GetMV("MV_XPEDIMP"),5)
		PutMV("MV_XPEDIMP",cProx)	
	EndIf
	
	cNumReg := SOMA1(cNumReg,6)
	
	cLog += CRLF + cNumReg + " - " + TRB->CNPJFIL+" - "+TRB->PEDMIL

	TRB->(DbSkip())
EndDo

TRB->(DbCloseArea())

MemoWrite("\log_import\SC5\_IMPORT_"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cLog)

IF(lEFIL,MemoWrite("\log_import\SC5\FIL"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEFIL),)
IF(lEDA0,MemoWrite("\log_import\SC5\DA0"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEDA0),)
IF(lESA1,MemoWrite("\log_import\SC5\SA1"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESA1),)
IF(lESA4,MemoWrite("\log_import\SC5\SA4"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESA4),)
IF(lESE4,MemoWrite("\log_import\SC5\SE4"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESE4),)
IF(lESA3,MemoWrite("\log_import\SC5\SA3"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESA3),)
IF(lESZ2,MemoWrite("\log_import\SC5\SZ2"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESZ2),)
IF(lESZ4,MemoWrite("\log_import\SC5\SZ4"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESZ4),)
IF(lESZ6,MemoWrite("\log_import\SC5\SZ6"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESZ6),)

MsgInfo("Feito SC5","Feito")

Return

User Function SC6GETCSV(lImporta)
Local nPos   := 0
Local cLinha := ""
Local cCab   := ""
Local nIni   := 0
Local i      := 0
Local x      := 0
Local aCSV   := {}
Local nCols  := 0
Local cTes   := ""

Default lUsaCab := .F.

cTpOper := AllTrim(SuperGetMV("MV_XRTOV2C",.F.,"01")) //Tipo de Operacao de venda para pedidos de venda
cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking

cQuery := "select * from P12_HOM2.dbo.IMPITENS where IMPORTADO is null order by RECNO"

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)

DbSelectArea("TRB")

ProcRegua(155000)

cNumReg := "000000"

cLog := "Pedidos abaixo importados com sucesso na SC6:"
cLog += CRLF + "CNPJ_FILIAL / PEDIDO_MILLENIUM  / PRODUTO "

cNumAnt := ""

cEFIL := "Filial nao encontrada:"
cESC5 := "Pedido Millenium nao encontrado: "
cESB1 := "Produto nao encontrado: "
cETES := "TES nao encontrada: "

lEFIL := .F.
lESC5 := .F.
lESB1 := .F.
lETES := .F.

TRB->(DbGoTop())

nCont := 0

While TRB->(!EOF())
	IncProc()
	If nCont > 10000
		MemoWrite("\log_import\SC6\_IMPORT_"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cLog)
		cLog := "Pedidos abaixo importados com sucesso na SC5:"
		cLog += CRLF + "CNPJ_FILIAL / PEDIDO_MILLENIUM"
		
		IF(lEFIL,MemoWrite("\log_import\SC6\FIL"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEFIL),)
		IF(lESC5,MemoWrite("\log_import\SC6\SC5"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESC5),)
		IF(lESB1,MemoWrite("\log_import\SC6\SB1"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESB1),)
		IF(lETES,MemoWrite("\log_import\SC6\TES"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cETES),)
		
		lEFIL := .F.
		lESC5 := .F.
		lESB1 := .F.
		lETES := .F.
		
		cEFIL := "Filial nao encontrada:"
		cESC5 := "Pedido Millenium nao encontrado: "
		cESB1 := "Produto nao encontrado: "
		cETES := "TES nao encontrada: "
		nCont := 0
	EndIf
			
	If TRB->CNPJFIL = "03007414000130"
		cFil := "0101" 	
	ElseIf TRB->CNPJFIL = "03007414000300"
		cFil := "0102"
	Else
		cEFIL += CRLF + "CNPJ Filial: "+cFil+" Pedido: "+TRB->PEDMIL+" Produto: "+TRB->PRODMIL+" Erro: CNPJ n�o econtrado."
		lEFIL := .T.
		TRB->(DbSkip())
		Loop
	EndIf
	
	DbSelectArea("SC5")
	DbOrderNickName("PEDMIL")
	If DbSeek(cFil+TRB->PEDMIL)
		cNumPed := SC5->C5_NUM
		cCli    := SC5->C5_CLIENTE
		cLoj    := SC5->C5_LOJACLI
	Else
		cESC5 += CRLF + "CNPJ Filial: "+cFil+" Pedido: "+TRB->PEDMIL+" Produto: "+TRB->PRODMIL+" Erro: Pedido n�o encontrado na SC5."
		lESC5 := .T.
		TRB->(DbSkip())
		Loop
	EndIf
	
	If !Empty(TRB->CODBAR)
		DbSelectArea("SB1")
		DbSetOrder(5)
		If DbSeek(xFilial("SB1")+TRB->CODBAR)
			cProd := SB1->B1_COD
			cDesc := SB1->B1_DESC
			cUM   := SB1->B1_UM
		Else
			//cESB1 += CRLF + "CNPJ Filial: "+TRB->CNPJFIL+" Pedido: "+TRB->PEDMIL+" Produto: "+TRB->PRODMIL+" Erro: EAN n�o encontrado:"+TRB->CODBAR
			//lESB1 := .T.
			//TRB->(DbSkip())
			//Loop
			DbSelectArea("SB1")
			DbOrderNickName("YFORMAT")
			If DbSeek(xFilial("SB1")+TRB->PRODMIL)
				cProd := SB1->B1_COD
				cDesc := SB1->B1_DESC
				cUM   := SB1->B1_UM
			Else
				cESB1 += CRLF + "Fil: "+cFil+" Ped: "+AllTrim(TRB->PEDMIL)+" Prod: "+AllTrim(TRB->PRODMIL)+" EAN "+AllTrim(TRB->CODBAR)+" Erro: Codigo Produto Millenium e EAN n�o encontrados:"+TRB->PRODMIL
				lESB1 := .T.
				TRB->(DbSkip())
				Loop
			EndIf
		EndIf
	Else
		DbSelectArea("SB1")
		DbOrderNickName("YFORMAT")
		If DbSeek(xFilial("SB1")+TRB->PRODMIL)
			cProd := SB1->B1_COD
			cDesc := SB1->B1_DESC
			cUM   := SB1->B1_UM
		Else
			cESB1 += CRLF + "Fil: "+cFil+" Ped: "+AllTrim(TRB->PEDMIL)+" Prod: "+AllTrim(TRB->PRODMIL)+" EAN "+AllTrim(TRB->CODBAR)+" Erro: Codigo Produto Millenium n�o encontrado:"+TRB->PRODMIL
			lESB1 := .T.
			TRB->(DbSkip())
			Loop
		EndIf
	EndIf
	
	cTES  := MaTesInt(2,cTpOper,cCli,cLoj,"C",cProd,)
	If Empty(cTes)
		cETES += CRLF + "Fil:"+cFil+" Ped: "+cNumPed+" Prod: "+cProd+" Cli:"+cCli+"/"+cLoj
		lETES := .T.
		cTes := "999"
		//TRB->(DbSkip())
		//Loop
	EndIf
	
	//Ordem dos campos no CSV
	//     1               2               3                    4                        5               6             7                8              9  
	//CNPJ_FILIAL,PEDIDO_MILLENIUM,CODIGO_BARRAS_EAN,CODIGO_PRODUTO_MILLENIUM,NF_FATURAMENTO_PARCIAL,QTDE_PEDIDA,QTDE_ENTREGUE,SALDO_A_ENTREGAR,PRECO_UNITARIO
	If lImporta

		_lValid	:= U_HFATP20A(cProd,"PEDIDO",.F.)
		
		If _lValid
		
			If cNumPed <> cNumAnt
				cItem := "01"
			Else
				cItem := SOMA1(cItem,2)	
			EndIf
			cNumAnt := SC5->C5_NUM
			
			nQtdVen := TRB->QTDVEN
			nPrcVen := TRB->PRCVEN
			nQtdEnt := TRB->QTDENT
			
			DbSelectArea("SC6")
			RecLock("SC6",.T.)
				SC6->C6_FILIAL  := cFil 
				SC6->C6_NUM     := cNumPed
				SC6->C6_ITEM    := cItem
				SC6->C6_PRODUTO := cProd
				SC6->C6_DESCRI  := cDesc
				SC6->C6_UM      := cUM
				SC6->C6_QTDVEN  := nQtdVen
				SC6->C6_PRCVEN  := nPrcVen
				SC6->C6_PRUNIT  := nPrcVen
				SC6->C6_VALOR   := Round(nQtdVen*nPrcVen,2)
				SC6->C6_TES     := cTES
				SC6->C6_LOCAL   := cAmzPik
				SC6->C6_QTDENT  := nQtdEnt
				SC6->C6_NOTA    := Right(TRB->NFPARC,9)
				SC6->C6_XINTEGR := "S" 
			MsUnlock()
		End
		
		TCSQLEXEC("UPDATE P12_HOM2.dbo.IMPITENS SET IMPORTADO = 'S' WHERE RECNO = "+AllTrim(Str(TRB->RECNO)))
	EndIf
	
	cNumReg := SOMA1(cNumReg,6)
	cLog += CRLF + cNumReg + TRB->CNPJFIL+" - "+TRB->PEDMIL+" - "+TRB->PRODMIL
	
	nCont++
	
	TRB->(DbSkip())
EndDo

MemoWrite("\log_import\SC6\_IMPORT_"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cLog)

IF(lEFIL,MemoWrite("\log_import\SC6\FIL"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEFIL),)
IF(lESC5,MemoWrite("\log_import\SC6\SC5"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESC5),)
IF(lESB1,MemoWrite("\log_import\SC6\SB1"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESB1),)
IF(lETES,MemoWrite("\log_import\SC6\TES"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cETES),)

MsgInfo("Feito SC6","Feito")

Return

User Function INSRECNO()
Local cQuery := ""

cQuery := "select * from P12_HOM2.dbo.IMPITENS order by PEDMIL "

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"ITENS", .F., .T.)

DbSelectArea("ITENS")

While !EOF()
	
	ITENS->(DbSkip())
EndDo

Return

/*
//Importar para um array

User Function HIMPPEDMIL()
Local aCabec := {}
Local aItens := {}
Local nx     := 0

PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101' TABLES 'SC5,SC6' MODULO 'FAT'

U_GETCSV("SC5",@aCabec,"D:\CABEC.csv",.t.) //.T. quando a primeira linha contem cabecalho 

U_GETCSV("SC6",@aItens,"D:\ITENS.csv",.t.) 

If Len(aCabec) > 0
	For nx := 1 to Len(aCabex)
		
	
	Next
EndIf

//cDebug := 0

RESET ENVIRONMENT

Return

User Function GETCSV(aMatriz,cArquivo,lUsaCab)
Local nPos   := 0
Local cLinha := ""
Local cCab   := ""
Local nIni   := 0
Local i      := 0
Local x      := 0
Local aCSV   := {}
Local nCols  := 0

Default lUsaCab := .F.

aMatriz := {}
If (!File(cArquivo))
	Return
EndIf

// Abrindo o arquivo
FT_FUse(cArquivo)
FT_FGoTop()

// Capturando as linhas do arquivo
While (!FT_FEof())
	If (Empty(cCab))
		cCab := FT_FREADLN()
	EndIf
	If (lUsaCab)
		AADD(aCSV,FT_FREADLN())
	ElseIf (!lUsaCab) .and. (i > 0)
		AADD(aCSV,FT_FREADLN())
	EndIf
	i++
	FT_FSkip()
EndDo

FT_FUSE()

// Pegando o numero de colunas com base no cabecalho
For i := 1 to Len(cCab)
	nPos := At(";",cCab)
	If (nPos > 0)
		nCols+= 1
		cCab := SubStr(cCab,nPos+1,Len(cCab)-nPos)
	EndIf
Next

// Definindo o tamanho da Matriz que recebera os dados
aMatriz := Array(Len(aCSV),nCols+1)

// Carregando os dados
For i := 1 to Len(aCSV)
	cLinha := aCSV[i]
	For x := 1 to nCols+1
		nPos := At(";",cLinha)
		If ( nPos > 0 )
			aMatriz[i,x] := AllTrim(SubStr(cLinha,1,nPos-1))
			cLinha := SubStr(cLinha,nPos+1,Len(cLinha)-nPos)
		Else
			aMatriz[i,x] := AllTrim(cLinha)
       		cLinha := ""
		EndIf
	Next x
Next i

Return*/