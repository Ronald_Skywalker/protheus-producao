// #########################################################################################
// Projeto: Hope
// Modulo : Faturamento
// Fonte  : hFATP003
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 09/10/16 | Actual Trend      | Cadastro de Prazos de Entrega 
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

user function HFATP006
	local cVldAlt := ".T." 
	local cVldExc := ".T." 
	
	local cAlias
	
	cAlias := "ZA2"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	axCadastro(cAlias, "Cadastro de Grupo de Regionais", cVldExc, cVldAlt)
	
return
