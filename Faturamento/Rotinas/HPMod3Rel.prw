#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"  
#INCLUDE "RWMAKE.CH"  

/*/{Protheus.doc} RF013 
Relat髍io de Fases
@author Weskley Silva
@since 06/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function Mod3Rel()

	Private oReport
	Private cPergCont	:= 'HPFINDEV1A' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf
	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'RDV', 'RELA敲O DE DEVOLU钦ES POR NF ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELA敲O DE DEVOLU钦ES POR NF' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELA敲O DE DEVOLU钦ES POR NF ', { 'RDV', 'ZAX','ZAZ','SA1'})
			
	TRCell():New( oSection1, 'COD_CLIENTE'            ,'RDV','COD_CLIENTE',          "@!" 			 ,06)
	TRCell():New( oSection1, 'LOJ_CLIENTE'            ,'RDV','LOJ_CLIENTE',          "@!" 			 ,04)
	TRCell():New( oSection1, 'RAZ肙 SOCIAL'           ,'RDV','RAZ肙 SOCIAL',         "@!" 			 ,30)
	TRCell():New( oSection1, 'NOTA FISCAL' 		      ,'RDV','NOTA FISCAL',          "@!"              ,09)
	TRCell():New( oSection1, 'DATA REGISTRO'		  ,'RDV','DATA REGISTRO',	       "@D"              ,08)
	TRCell():New( oSection1, 'VALOR NF'               ,'RDV','VALOR NF',             "@E 99,999,999.99",13)
	TRCell():New( oSection1, 'MACRO REGIAO'           ,'RDV','MACRO REGIAO',         "@!" 			 ,15)
	TRCell():New( oSection1, 'MOTIVO'                 ,'RDV','MOTIVO',               "@!" 			 ,15)
	TRCell():New( oSection1, 'STATUS'                 ,'RDV','STATUS',               "@!" 			 ,15)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""
    LocaL lApaga := .f.
	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	                    
   
	IF Select("xREL") > 0
		xREL->(dbCloseArea())
	Endif             

	cQuery := " SELECT ZAX_CODCLI,ZAX_LOJCLI,ZAX_CLINFE,ZAX_NFENTR,ZAX_DTAREG,ZAX_VALOR,ZAX_MREGIA,ZAX_MOTIVO,ZAX_STATUS, "
	cQuery += " CASE ZAX_STATUS 
	cQuery += "         WHEN '01'  THEN 'N鉶 Processado        '
	cQuery += "         WHEN '02'  THEN 'Em Processo Fiscal    '
	cQuery += " 	    WHEN '03'  THEN 'Em Processo Qualidade '
	cQuery += " 		WHEN '04'  THEN 'Em Processo Comercial '
	cQuery += " 		WHEN '05'  THEN 'Em Processo Financeiro'
	cQuery += " 		WHEN '06'  THEN 'Cr閐ito Concedido     '
	cQuery += "         ELSE 'Reprovado' END AS CSTATUS 
	cQuery += " FROM "+RetSqlName("ZAX")+" with (NOLOCK)"
	cQuery += " WHERE ZAX_DTAREG  BETWEEN '"+DtoS(MV_PAR05)+"' AND '"+DtoS(MV_PAR06)+"' AND ZAX_CODCLI  BETWEEN '"+MV_PAR01+"' "  
    cQuery += " AND '"+MV_PAR02+"' AND ZAX_NFENTR BETWEEN '"+MV_PAR03+"'  AND '"+MV_PAR04+"' AND D_E_L_E_T_ = '' "
    cQuery += " ORDER BY ZAX_CODCLI "
  
    TCQUERY cQuery NEW ALIAS xREL 
    xREL->(DBGOTOP())
    
  While xREL->(!EOF())      
	
	  IF oReport:Cancel()
		 Exit
	  EndIf
	
	  oReport:IncMeter()
	  oReport:IncMeter()

		oSection1:Cell("COD_CLIENTE"):SetValue(xREL->ZAX_CODCLI)            
		oSection1:Cell("COD_CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("LOJ_CLIENTE"):SetValue(xREL->ZAX_LOJCLI)            
		oSection1:Cell("LOJ_CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("RAZ肙 SOCIAL"):SetValue(xREL->ZAX_CLINFE)           
		oSection1:Cell("RAZ肙 SOCIAL"):SetAlign("LEFT")
		
		oSection1:Cell("NOTA FISCAL"):SetValue(xREL->ZAX_NFENTR) 		      
		oSection1:Cell("NOTA FISCAL"):SetAlign("LEFT")
		
		oSection1:Cell("DATA REGISTRO"):SetValue(CTOD(SUBSTR(xREL->ZAX_DTAREG,7,2)+'/'+SUBSTR(xREL->ZAX_DTAREG,5,2)+'/'+SUBSTR(xREL->ZAX_DTAREG,3,2))) // xREL->ZAS_DATA)		  
		oSection1:Cell("DATA REGISTRO"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR NF"):SetValue(xREL->ZAX_VALOR)              
		oSection1:Cell("VALOR NF"):SetAlign("LEFT")
		             
		oSection1:Cell("MACRO REGIAO"):SetValue(xREL->ZAX_MREGIA)        
		oSection1:Cell("MACRO REGIAO"):SetAlign("LEFT")
		
		oSection1:Cell("MOTIVO"):SetValue(xREL->ZAX_MOTIVO)                
		oSection1:Cell("MOTIVO"):SetAlign("LEFT")

		oSection1:Cell("STATUS"):SetValue(xREL->CSTATUS)                
		oSection1:Cell("STATUS"):SetAlign("LEFT")

	
		oSection1:PrintLine()
		
		xREL->(DBSKIP()) 
	enddo
	xREL->(DBCLOSEAREA())
Return( Nil )


/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Verifica/cria SX1 a partir de matriz para verificacao          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � Especifico para Clientes Microsiga                             潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Static Function AjustaSX1(cPerg)

Local _sAlias	:= Alias()
Local aCposSX1	:= {}
Local aPergs	:= {}
Local nX 		:= 0
Local lAltera	:= .F.
Local nCondicao
Local cKey		:= ""
Local nJ		:= 0

aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

//	Aadd(aPergs,{"Ate Cliente","","","mv_ch8","C",6,0,0,"G","","MV_PAR08","","","","ZZZZZZ","","","","","","","","","","","","","","","","","","","","","SE1","","","",""})
	Aadd(aPergs,{"Cliente de ?","","","mv_ch1","C",6,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Cliente At�?","","","mv_ch2","C",6,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Nota Inicial","","","mv_ch3","C",9,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Nota Final"  ,"","","mv_ch4","C",9,0,0,"G","","mv_par04","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data de? "   ,"","","mv_ch5","D",8,0,0,"G","","mv_par05","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data ate? "  ,"","","mv_ch6","D",8,0,0,"G","","mv_par06","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
dbSelectArea("SX1")
dbSetOrder(1)
For nX:=1 to Len(aPergs)
	lAltera := .F.
	If MsSeek(cPerg+Right(aPergs[nX][11], 2))
		If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
			aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
			lAltera := .T.
		Endif
	Endif
	
	If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
		lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
	Endif
	
	If ! Found() .Or. lAltera
		RecLock("SX1",If(lAltera, .F., .T.))
		Replace X1_GRUPO with cPerg
		Replace X1_ORDEM with Right(aPergs[nX][11], 2)
		For nj:=1 to Len(aCposSX1)
			If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
				Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
			Endif
		Next nj
		MsUnlock()
		cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."
		
		
		If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
			aHelpSpa := aPergs[nx][Len(aPergs[nx])]
		Else
			aHelpSpa := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
			aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
		Else
			aHelpEng := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
			aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
		Else
			aHelpPor := {}
		Endif
		
		PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
	Endif
Next
