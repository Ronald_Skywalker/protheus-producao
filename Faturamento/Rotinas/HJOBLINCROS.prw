#INCLUDE "PROTHEUS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"

#DEFINE nC5_XIDLINC	1

#DEFINE nC5_NUM     2
#DEFINE nC5_EMISSAO	3
#DEFINE nC5_FRETE	6
#DEFINE nC5_PESOL	7
#DEFINE nC5_PBRUTO	8
	
#DEFINE nC6_QTDVEN	9
#DEFINE nC6_VALOR	10

#DEFINE nA1_NOME    11
#DEFINE nA1_END  	12
#DEFINE nA1_BAIRRO	13
#DEFINE nA1_EST		14
#DEFINE nA1_CEP	    15
#DEFINE nA1_CGC     16

#DEFINE nB1_COD		17
#DEFINE nB1_DESC	18
#DEFINE nB1_PESO	19

STATIC cIdEmp		:= "01"
STATIC cIdFil		:= "0101"

STATIC cMailDest	:= ""
STATIC cAssunto		:= "Log de Integra��o Protheus x Lincros via WS Rest ( B2C )"
STATIC aErrorLog	:= {}
STATIC cMsg			:= ""

STATIC cUrl			:= ""
STATIC cToken		:= ""
STATIC cUser		:= ""
STATIC cPassword	:= ""

STATIC cCriterio    := ""
STATIC nPesoCubado	:= 0
STATIC aHeader		:= { 'Content-Type: application/json' }
STATIC cMarcador	:= ""

STATIC cDtHrIni		:= ""
STATIC cDtHrFim		:= ""
STATIC nPedEnv		:= 0
STATIC nPedEnvG		:= 0
STATIC nPedCons		:= 0
STATIC nPedConsG	:= 0

/*/
------------------------------------------------------------------
{Protheus.doc} HJobLincros
Job de Integra��o Protheus x Lincros ( B2C )

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
User Function JobB2C()
	//Local aEmpHope:= {}
	//Local nX:= 0
	//Private cIdEmp		:= "01"
	//Private cIdFil		:= "0101"

	RpcClearEnv()
	RpcSetType( 3 )
	RpcSetEnv( cIdEmp, cIdFil )
	
	//aEmpHope:=  Separa(SuperGetMv("HP_FILLINC",.F.,"0101/0102/0103/0104"),"/")  //FWLoadSM0()
	
	//For nX:= 1 to Len(aEmpHope)

		//cIdEmp := "01"
		//cIdFil := aEmpHope[nX]

		u_postB2c()
		u_getB2C()
		
	//Next nX
Return

/*/
------------------------------------------------------------------
{Protheus.doc} sndAuth()
Realiza a Autenticacao com a Lincros

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function sndAuth()

	Local lRet			:= .T.
	Local oRestClient	:= FWRest():New( cUrl )
	
    oRestClient:setPath( "/auth/login/" + cUser + "/" + cPassword ) 
	
    If !oRestClient:Post( aHeader )
		lRet := .F.
		cMsg := "Falha de Autenticacao: " + Rtrim( oRestClient:GetLastError() )
	ElseIf "Usuario ou senha inválidos" $ oRestClient:GetResult()
		lRet := .F.
		cMsg := "Falha de Autenticacao: " + Rtrim( oRestClient:GetResult() )
	Endif

	// Envio do alerta atrav�s do email
	If !lRet
		AADD( aErrorLog, cMsg )
		msgPrint( cMsg )
	Endif

Return( lRet )

/*/
------------------------------------------------------------------
{Protheus.doc} postB2C()
Envio Sincrono de Pedidos B2C para TranspoFrete

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
User Function postB2C()

	Local nX			:= 0
	Local aPedidos      := {}
	Local cPedido		:= ""
	Local oRestClient   := NIl
	Local lWsB2C		:= .F.
	
	RpcClearEnv()
	RpcSetType( 3 )
	RpcSetEnv( cIdEmp, cIdFil )
	// Configuracao de ambiente

	aEmpHope:=  Separa(SuperGetMv("HP_FILLINC",.F.,"0101/0102/0103/0104"),"/")  //FWLoadSM0()
	
	For nX:= 1 to Len(aEmpHope)

		cFilJob := aEmpHope[nX]

		RpcClearEnv()
		RpcSetType( 3 )
		RpcSetEnv( cIdEmp, cFilJob )
			
		// Declaracao das variaveis staticas
		cMailDest	:= Alltrim( Lower( GETMV("HP_MAILTF",.T.,"sergio.fuzinaka@hopelingerie.com.br") ) )
		cDtHrIni 	:= DToC( Date() ) + " " + Time()
		aErrorLog	:= {}
		nPedEnv		:= 0
		nPedEnvG	:= 0
		nPedCons	:= 0
		nPedConsG	:= 0
		cCriterio	:= GetNewPar( "MV_CRITCAL", "VALOR" )
		nPesoCubado	:= GetNewPar( "MV_PCUBADO", 0.68 )
		cMarcador	:= GetNewPar( "MV_MARCB2C", '["B2C"]' )
		lWsB2C		:= GetNewPar( "HP_WSB2C"  , .F. )

		BEGIN SEQUENCE

			// Habilita/Desabilita Integracao TranspoFrete x Protheus (B2C)
			If !lWsB2C
				cMsg := "[DESABILITADO] Par�metro 'HP_WSB2C' de Integracao Lincros x Protheus (B2C)!"
				msgPrint( cMsg )
				BREAK
			Endif

			// Configuracao
			If Upper( AllTrim( GetEnvServer() ) ) $ Alltrim( Upper( GETMV("HP_ENVPRD",.T.,"HOPE|PRODUCAO|PRD_CE|JOBLINCROS|COMPILA_SERGIO") ) )
				cUrl    	:= Alltrim( GetNewPar( "URL_PTPFRE", "https://ws.transpofrete.com.br/api" ) )
				cToken  	:= Alltrim( GetNewPar( "TOK_PTPFRE", "8ED00CA2AC573DD811FCF4C650429452" ) )
				cUser		:= Alltrim( GetNewPar( "MV_USRPRD", "hope.arquivos1" ) )
				cPassword	:= Alltrim( GetNewPar( "MV_PASWPRD", "hope.arquivos1" ) )
			Else
				cUrl    	:= Alltrim( GetNewPar( "URL_HTPFRE", "https://homolog.transpofrete.com.br/api" ) )
				cToken  	:= Alltrim( GetNewPar( "TOK_HTPFRE", "F86CF9A2B64889D78663D6464BE7DC3F" ) )
				cUser		:= Alltrim( GetNewPar( "MV_USRHOM", "hope.ws" ) )
				cPassword	:= Alltrim( GetNewPar( "MV_PASWHOM", "Hh123456hH" ) )
			Endif

			// Iniciando o job de envio
			msgPrint( "Iniciando o envio dos pedidos para a TranspoFrete ... Empresa:" +  cIdEmp + ' Filial: '+ cFilJob )

			// Realiza a Autenticacao com a Lincros
			If sndAuth()

				// Retorna os Pedidos para serem enviados para TranspoFrete
				aPedidos    := RetPedidos() 

				If Len( aPedidos ) > 0

					For nX := 1 To Len( aPedidos )

						nPedEnv++

						cPedido	:= aPedidos[nX][1][nC5_NUM]

						// Retorna o Pedido para serem enviado para TranspoFrete
						cJson	:= RetJsonSync( aPedidos[nX] )

						oRestClient := FWRest():New( cUrl )
						
						oRestClient:setPath( "/prenota/criarSync?token=" + cToken ) 
						oRestClient:SetPostParams( AllTrim( cJson ) )
					
						If oRestClient:Post( aHeader )
							RetSync( cPedido, oRestClient:GetResult() )
						Else
							cMsg := "Falha no Envio Sincrono: " + oRestClient:GetLastError()
							AADD( aErrorLog, cMsg )
							msgPrint( cMsg )
						Endif
					
					Next

				Else

					Sleep( 60000 )
				
				Endif

			Else

				AADD( aErrorLog, "Ambiente: " + AllTrim( GetEnvServer() ) )
				AADD( aErrorLog, "URL: " + cUrl )
				AADD( aErrorLog, "Token: " + cToken )
				AADD( aErrorLog, "Usuario: " + cUser )
				AADD( aErrorLog, "Senha: " + cPassword )

				Sleep( 60000 )

			Endif
			
			// Finalizando o job de envio
			msgPrint( "Finalizando o envio dos pedidos para a TranspoFrete ..." )

		END SEQUENCE

		cDtHrFim := DToC( Date() ) + " " + Time()

		If Len( aErrorLog ) > 0
			U_SendMail( cMailDest, cAssunto, retBody( aErrorLog ) )
		Endif

		delClassIntf()
	Next nX
Return

/*/
------------------------------------------------------------------
{Protheus.doc} RetSync()
Retorno do Envio Sincrono de Pedidos para TranspoFrete

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function RetSync( cPedido, cResponse )

	Local cMsg		:= ""
	Local lAltera	:= .F.

	Private oJson	:= NIL
			
	FWJsonDeserialize( cResponse, @oJson )

	If Type( "oJson:STATUS" ) <> "U" .And. Rtrim( Upper( oJson:STATUS ) ) == "CONFIRMADO"

		updTabPed( "1", cPedido )
	
	Else
	
		If Type( "oJson:STATUS" ) <> "U"

			cMsg := "Pedido [" + cPedido + "] - Status: " + oJson:STATUS
	
			If Type( "oJson:MENSAGEM" ) <> "U" .And. !Empty( oJson:MENSAGEM )
	
				cMsg += " - Mensagem: " + oJson:MENSAGEM

				If "ERRO" $ oJson:STATUS
					lAltera := .T.
				Endif
	
			Endif
	
		Else

			lAltera	:= .T.
			cMsg 	:= "Pedido [" + cPedido + "] - Retorno inv�lido"
	
		Endif

		If !Empty( cMsg )
			AADD( aErrorLog, cMsg )
			msgPrint( cMsg )
		Endif

		// Altera o status do pedido
		If lAltera
			StatusChange( cPedido )
		Endif
	
	Endif

Return

/*/
------------------------------------------------------------------
{Protheus.doc} getB2C()
Retorno das Transportadoras da TranspoFrete

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
User Function getB2C(cFilJob)

	Local nX        	:= 0
    Local aPedidos  	:= {}
    Local cPedido   	:= ""
	Local cIdLincros	:= ""
    Local aTransp   	:= {}
	Local cDataEmis		:= ""
	Local lWsB2C		:= .F.

	RpcClearEnv()
	RpcSetType( 3 )
	RpcSetEnv( cIdEmp, cIdFil )

	aEmpHope:=  Separa(SuperGetMv("HP_FILLINC",.F.,"0101/0102/0103/0104"),"/")  //FWLoadSM0()
	
	For nX:= 1 to Len(aEmpHope)

		cFilJob := aEmpHope[nX]

		RpcClearEnv()
		RpcSetType( 3 )
		RpcSetEnv( cIdEmp, cFilJob )

		// Declaracao das variaveis staticas
		cMailDest	:= Alltrim( Lower( GETMV("HP_MAILTF",.T.,"sergio.fuzinaka@hopelingerie.com.br") ) )
		cDtHrIni 	:= DToC( Date() ) + " " + Time()
		aErrorLog	:= {}
		nPedEnv		:= 0
		nPedEnvG	:= 0
		nPedCons	:= 0
		nPedConsG	:= 0
		cCriterio	:= GetNewPar( "MV_CRITCAL", "VALOR" )
		nPesoCubado	:= GetNewPar( "MV_PCUBADO", 0.68 )
		cMarcador	:= GetNewPar( "MV_MARCB2C", '["B2C"]' )
		lWsB2C		:= GetNewPar( "HP_WSB2C"  , .F. )

		BEGIN SEQUENCE

			// Habilita/Desabilita Integracao TranspoFrete x Protheus (B2C)
			If !lWsB2C
				cMsg := "[DESABILITADO] Par�metro 'HP_WSB2C' de Integracao Lincros x Protheus (B2C)!"
				msgPrint( cMsg )
				BREAK
			Endif
			
			// Configuracao
			If Upper( AllTrim( GetEnvServer() ) ) $ Alltrim( Upper( GETMV("HP_ENVPRD",.T.,"HOPE|PRODUCAO|PRD_CE|JOBLINCROS|COMPILA_SERGIO") ) )
				cUrl    	:= Alltrim( GetNewPar( "URL_PTPFRE", "https://ws.transpofrete.com.br/api" ) )
				cToken  	:= Alltrim( GetNewPar( "TOK_PTPFRE", "8ED00CA2AC573DD811FCF4C650429452" ) )
				cUser		:= Alltrim( GetNewPar( "MV_USRPRD", "hope.arquivos1" ) )
				cPassword	:= Alltrim( GetNewPar( "MV_PASWPRD", "hope.arquivos1" ) )
			Else
				cUrl    	:= Alltrim( GetNewPar( "URL_HTPFRE", "https://homolog.transpofrete.com.br/api" ) )
				cToken  	:= Alltrim( GetNewPar( "TOK_HTPFRE", "F86CF9A2B64889D78663D6464BE7DC3F" ) )
				cUser		:= Alltrim( GetNewPar( "MV_USRHOM", "hope.ws" ) )
				cPassword	:= Alltrim( GetNewPar( "MV_PASWHOM", "Hh123456hH" ) )
			Endif

			// Iniciando o job de consulta
			msgPrint( "Iniciando a consulta dos pedidos na TranspoFrete ... Empresa:" +  cIdEmp + ' Filial: '+ cFilJob )

			// Realiza a Autenticacao com a Lincros
			If sndAuth()

				// Retorna os Pedidos para serem consultados na TranspoFrete
				aPedidos	:= RetPedCons()

				If Len( aPedidos ) > 0

					For nX := 1 To Len( aPedidos )

						nPedCons++

						cMsg		:= ""
						cPedido 	:= aPedidos[nX,1]
						cIdLincros	:= Alltrim( aPedidos[nX,2] )
						cDataEmis	:= Substr(aPedidos[nX,3],7,2) + "-" + Substr(aPedidos[nX,3],5,2) + "-" + Substr(aPedidos[nX,3],1,4) + " 00:00"

						If !Empty( cPedido ) 
						
							If !Empty( cIdLincros )

								aTransp := getTransp( cIdLincros, cPedido, cDataEmis )

								If Len( aTransp ) > 0
									updTabPed( "2", cPedido, aTransp )
								Endif

							Else

								cMsg := "Pedido: [" + cPedido + "] - ID Lincros Vazio no retorno da Transportadora"
								AADD( aErrorLog, cMsg )
								msgPrint( cMsg )

							Endif

						Else

							cMsg := "Pedido Vazio no retorno da Transportadora"
							AADD( aErrorLog, cMsg )
							msgPrint( cMsg )

						Endif

					Next

				Else

					Sleep( 60000 )
									
				Endif

			Else

				AADD( aErrorLog, "Ambiente: " + AllTrim( GetEnvServer() ) )
				AADD( aErrorLog, "URL: " + cUrl )
				AADD( aErrorLog, "Token: " + cToken )
				AADD( aErrorLog, "Usuario: " + cUser )
				AADD( aErrorLog, "Senha: " + cPassword )

				Sleep( 60000 )

			Endif

			// Finalizando o job de consulta
			msgPrint( "Finalizando a consulta dos pedidos na TranspoFrete ..." )

		END SEQUENCE

		cDtHrFim := DToC( Date() ) + " " + Time()

		If Len( aErrorLog ) > 0
			U_SendMail( cMailDest, cAssunto, retBody( aErrorLog ) )
		Endif

		delClassIntf()

	Next nX

Return

/*/
------------------------------------------------------------------
{Protheus.doc} getTransp()
Retorna a Transportadora selecionada

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function getTransp( cIdLincros, cPedido, cDataEmis )

	Local cJson     := RetJsonCons( cIdLincros, cDataEmis )
    Local aTransp	:= {}

	oRestClient := FWRest():New( cUrl )

	oRestClient:setPath( "/prenota/selecao/simulacao?token=" + cToken ) 
	oRestClient:SetPostParams( AllTrim( cJson ) )

	If oRestClient:Post( aHeader )
		aTransp := selTransp( cIdLincros, cPedido, oRestClient:GetResult() )
	Else
		cMsg := "Falha no envio da Simulacao do Pedido/Id Lincros [" + cPedido + "-" + cIdLincros + "]: " + oRestClient:GetLastError()
		AADD( aErrorLog, cMsg )
		msgPrint( cMsg )
	Endif
	
Return( aTransp )

/*/
------------------------------------------------------------------
{Protheus.doc} selTransp()
Retorna Leiaute Sincrono do Json dos Pedidos

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function selTransp( cIdLincros, cPedido, cResponse )

	Local nX		:= 0
	Local cX		:= ""
	Local aTransp	:= {}

	Private oJson	:= Nil

	FWJsonDeserialize( cResponse, @oJson )
		
	If Type( "oJson:TRECHOS" ) <> "U" .And. Len( oJson:TRECHOS ) > 0
		
		For nX := 1 To Len( oJson:TRECHOS ) 
		
			cX := cValToChar( nX )

			// Transportadora de Despacho
			If ( Type( "oJson:TRECHOS[" + cX + "]:SEQUENCIACARGA" ) <> "U" .And. oJson:TRECHOS[nX]:SEQUENCIACARGA == "1" ) .And. ;
				( Type( "oJson:TRECHOS[" + cX + "]:CNPJTRANSPORTADORA" ) <> "U" .And. !Empty( oJson:TRECHOS[nX]:CNPJTRANSPORTADORA ) )

				AADD( aTransp , Rtrim( oJson:TRECHOS[nX]:CNPJTRANSPORTADORA ) )
			
			// Transportadora de Redespacho
			ElseIf ( Type( "oJson:TRECHOS[" + cX + "]:SEQUENCIACARGA" ) <> "U" .And. oJson:TRECHOS[nX]:SEQUENCIACARGA == "2" ) .And. ;
				( Type( "oJson:TRECHOS[" + cX + "]:CNPJTRANSPORTADORA" ) <> "U" .And. !Empty( oJson:TRECHOS[nX]:CNPJTRANSPORTADORA ) )

				AADD( aTransp , Rtrim( oJson:TRECHOS[nX]:CNPJTRANSPORTADORA ) )

			// Frete n�o Calculado
			ElseIf ( Type( "oJson:TRECHOS[" + cX + "]:STATUS" ) <> "U" ) .Or. Type( "oJson:STATUS" ) <> "U"

				cMsg := "Id Lincros/Pedido: [" + cIdLincros + "-" + cPedido + "] Status: "

				If Type( "oJson:STATUS" ) <> "U" .And. !Empty( oJson:STATUS )
					cMsg += Rtrim( oJson:STATUS )
				Endif

				If Type( "oJson:TRECHOS[" + cX + "]:STATUS" ) <> "U" .And. !Empty( oJson:TRECHOS[nX]:STATUS )
					cMsg +=  " | " + Rtrim( oJson:TRECHOS[nX]:STATUS )
				Endif

				AADD( aErrorLog, cMsg )
		 		msgPrint( cMsg )

				// Altera o status do pedido
				StatusChange( cPedido )

			Endif
			
		Next

	ElseIf Type( "oJson:STATUS" ) <> "U" .And. "ERRO" $ Upper( oJson:STATUS )

		cMsg := "Id Lincros/Pedido: [" + cIdLincros + "-" + cPedido + "] "
		cMsg += IIf( Type( "oJson:MENSAGEM" ) <> "U", Alltrim( oJson:MENSAGEM ), "" )
		AADD( aErrorLog, cMsg )
		msgPrint( cMsg )

	Else

		cMsg := "Id Lincros/Pedido: [" + cIdLincros + "-" + cPedido + "] Falha na estrutua de retorno ou retorno vazio - Response: " + cResponse
		AADD( aErrorLog, cMsg )
		msgPrint( cMsg )

	Endif

Return( aTransp )

/*/
------------------------------------------------------------------
{Protheus.doc} RetJsonSync()
Retorna Leiaute Sincrono do Json dos Pedidos

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function RetJsonSync( aPedidos )

    Local nX        	:= 0
    Local cJson    		:= ""
	Local cModalFrete	:= "0"
	Local nTotValor     := 0
    Local nTotItens     := 0
	Local nTotPeso		:= 0
	Local cDataEmis		:= ""
	Local cNrPedido		:= ""

	// Emitente
	Local cCnpjE		:= ""
	Local cNomeE		:= ""
	Local cCepE			:= ""

	// Destinatario
	Local cCnpjD		:= ""
	Local cNomeD		:= ""
	Local cEnderecoD	:= ""
	Local cBairroD		:= ""
	Local cUfD			:= ""
	Local cCepD			:= ""

	cNrPedido   := Alltrim( aPedidos[1][nC5_XIDLINC] )

	// Emitente
	cCnpjE      := Rtrim( SM0->M0_CGC )
	cNomeE      := Rtrim( SM0->M0_NOME )
	cCepE       := Rtrim( SM0->M0_CEPENT )

	// Destinatario
	cCnpjD      := Rtrim( aPedidos[1][nA1_CGC] )
	cNomeD      := Rtrim( aPedidos[1][nA1_NOME] )
	cEnderecoD	:= Rtrim( aPedidos[1][nA1_END] )
	cBairroD	:= Rtrim( aPedidos[1][nA1_BAIRRO] )
	cUfD		:= Rtrim( aPedidos[1][nA1_EST] )
	cCepD		:= Rtrim( aPedidos[1][nA1_CEP] )

	cDataEmis   := Substr(aPedidos[1][nC5_EMISSAO],7,2) + "-" + Substr(aPedidos[1][nC5_EMISSAO],5,2) + "-" + Substr(aPedidos[1][nC5_EMISSAO],1,4) + " 00:00"

	// Modalidade do Frete ( Frete > 0, FOB, CIF )
	If aPedidos[1][nC5_FRETE] > 0
		cModalFrete := "1"	// FOB - Pago pelo Destinatario
	Else
		cModalFrete := "0"	// CIF - Pago pelo Emitente
	Endif

	cJSon += '{'
	cJson += '"numero":"' + cNrPedido + '",'
	cJSon += '"serie":"1",' //Fixo

	cJSon += '"emitente":'
	cJSon += '{'
	cJSon += '"cnpj":"' + cCnpjE + '",'
	cJSon += '"nome":"' + cNomeE + '",'
	cJSon += '"cep":"' + retOrigem( cUfD ) + '"'
	cJSon += '},'

	cJSon += '"destinatario":'
	cJSon += '{'
	cJSon += '"cnpj":"' + cCnpjD + '",'
	cJSon += '"nome":"' + cNomeD + '",'
	cJSon += '"cep":"' + cCepD + '"'
	cJSon += '},'
	
	cJSon += '"itens":'
	cJSon += '['

    For nX := 1 To Len( aPedidos )

		nTotItens++
		nTotValor	+= aPedidos[nX][nC6_VALOR]
		nTotPeso	+= aPedidos[nX][nB1_PESO]
		
		cJSon += '{'
		cJSon += '"produto":"' + Rtrim( aPedidos[nX][nB1_COD] ) + '",'
		cJSon += '"nomeProduto":"' + Rtrim( aPedidos[nX][nB1_DESC] ) + '",'
		cJSon += '"peso":' + Alltrim( Str( aPedidos[nX][nB1_PESO], 11, 4 ) ) + ','
		cJSon += '"cubagem":0,'
		cJSon += '"quantidade":' + Alltrim( Str( aPedidos[nX][nC6_QTDVEN], 16, 4 ) ) + ','
		cJSon += '"volumes":0,'     // Fixo		
		cJSon += '"valor":' + Alltrim( Str( aPedidos[nX][nC6_VALOR], 12, 2 ) )
        cJson += '}' + IIf( nX < Len( aPedidos ), ',', '' )

    Next

	cJSon += '],'

	cJSon += '"enderecoRetirada":'
	cJSon += '{'
	cJSon += '"logradouro":"' + cEnderecoD + '",'
	cJSon += '"numero":"",'
	cJSon += '"complemento":"",' 
	cJSon += '"bairro":"' + cBairroD + '",'
	cJSon += '"uf":"' + cUfD + '",'
	cJSon += '"cep":"' + cCepD + '"'
	cJson += '},'

	cJSon += '"enderecoEntrega":'
	cJSon += '{'
	cJSon += '"logradouro":"' + cEnderecoD + '",'
	cJSon += '"numero":"",'
	cJSon += '"complemento":"",' 
	cJSon += '"bairro":"' + cBairroD + '",'
	cJSon += '"uf":"' + cUfD + '",'
	cJSon += '"cep":"' + cCepD + '"'
	cJson += '},'

	cJson += '"modalidade":' + cModalFrete + ','
	cJson += '"tipoOperacao":1,'    // Fixo: 1-Saida
	cJson += '"dataEmissao":"' + cDataEmis + '",'
	//cJson += '"peso":' + Alltrim( Str( nTotPeso, 11, 4 ) ) + ','    // Peso Bruto dos itens do pedido
	//cJson += '"pesoLiquido":' + Alltrim( Str( nTotPeso, 11, 4 ) ) + ','    // Peso Liquido dos itens da Pre-Nota
	cJson += '"pesoCubado":' + Alltrim( Str( nPesoCubado, 11, 4 ) ) + ','  // Total dos pesos cubados dos itens da Pre-Nota
	cJson += '"volumes":1,'	// Valor Fixo
	//cJson += '"quantidadeItens":' + Alltrim( Str( nTotItens, 16, 4 ) ) + ','
	cJson += '"valor":' + Alltrim( Str( nTotValor, 12, 2 ) ) + ','   // Valor da Pre-Nota

	If !Empty( cMarcador )
		cJson += '"marcadores":' + cMarcador + ','
	Endif

	cJson += '"valorProdutos":' + Alltrim( Str( nTotValor, 12, 2 ) )	// Valor total dos Produtos
    cJson += '}'

Return( cJson )

/*/
------------------------------------------------------------------
{Protheus.doc} RetJsonCons()
Retorna Json dos Pedidos para ser consultados na TranspoFrete

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function RetJsonCons( cPedido, cDataEmis )

    Local cJson    := ""

    If !Empty( cPedido )

        cJson := '{'
        cJson += '"cnpjEmissorPreNotaFiscal":"' + Rtrim( SM0->M0_CGC ) + '",'
        cJson += '"numeroPreNotaFiscal":"' + Alltrim( cPedido ) + '",'
        cJson += '"seriePreNotaFiscal":"1",'
        cJson += '"criterioSimulacao":'
        cJson += '{'
		cJson += '"dataEmissao":"' + cDataEmis + '",'
        
		If !Empty( cMarcador )
			cJson += '"marcadores":' + cMarcador + ','
		Endif
		
        cJson += '"criterioCalculo":"' + cCriterio + '"'
//        cJson += '"criterioCalculoNaoAtendido":"' + cCriterio + '",'
//        cJson += '"simulacaoRedespacho":true'
        cJson += '}'
        cJson += '}'

    Endif

Return( cJson )

/*/
------------------------------------------------------------------
{Protheus.doc} RetPedidos
Retorna os Pedidos para serem enviados para TranspoFrete

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function RetPedidos()

	Local cAlias    := GetNextAlias()
	Local cPedido   := ""
	Local aAux      := {}
	Local aPedidos  := {}
	Local aRetorno  := {}
	
	BEGINSQL ALIAS cAlias
	
	    SELECT  C5.C5_XIDLINC,
				C5.C5_NUM, 
	            C5.C5_EMISSAO, 
	            C5.C5_CLIENTE, 
	            C5.C5_LOJACLI, 
				C5.C5_FRETE,
	            C5.C5_PESOL, 
	            C5.C5_PBRUTO,
	            C6.C6_QTDVEN, 
	            C6.C6_VALOR,
	            A1.A1_CGC, 
	            A1.A1_NOME, 
	            A1.A1_END, 
	            A1.A1_BAIRRO, 
	            A1.A1_EST, 
	            A1.A1_CEP,  
	            B1.B1_COD, 
	            B1.B1_DESC, 
	            B1.B1_PESO 
	    FROM 
	        %table:SC5% C5 
	        INNER JOIN %table:SC6% C6 ON C5.C5_FILIAL = C6.C6_FILIAL AND C5.C5_NUM = C6.C6_NUM AND C6.%notDel%
	        INNER JOIN %table:SA1% A1 ON C5.C5_CLIENTE = A1.A1_COD AND C5.C5_LOJACLI = A1.A1_LOJA AND A1.%notDel%
	        INNER JOIN %table:SB1% B1 ON C6.C6_PRODUTO = B1.B1_COD AND B1.%notDel%
	    WHERE
	        C5.C5_FILIAL = %xFilial:SC5% AND
	        C5.C5_XLIDOTF = 'S' AND
	        C5.C5_ORIGEM = 'B2C' AND
			C5.C5_XIDLINC <> '' AND
			C5.C5_TRANSP = '' AND
			C5.C5_REDESP = '' AND
			C5.C5_NOTA = '' AND
	        C5.C5_EMISSAO >= '20170101' AND
	        C5.%notDel%
		ORDER BY C5.C5_NUM
	
	ENDSQL

	If (cAlias)->(!Eof())
	
		cPedido := (cAlias)->C5_NUM
		
	    While (cAlias)->(!Eof())

	        If (cAlias)->C5_NUM <> cPedido
	            
	            AADD( aRetorno, aPedidos )
	            
	            aPedidos:= {}
	            
	            cPedido := (cAlias)->C5_NUM 

	        Endif

	        aAux	:= {}	

   	        AADD( aAux, (cAlias)->C5_XIDLINC )  // 01

			AADD( aAux, (cAlias)->C5_NUM )      // 02
	        AADD( aAux, (cAlias)->C5_EMISSAO )  // 03
	        AADD( aAux, (cAlias)->C5_CLIENTE )  // 04
	        AADD( aAux, (cAlias)->C5_LOJACLI )  // 05
	        AADD( aAux, (cAlias)->C5_FRETE )  	// 06
	        AADD( aAux, (cAlias)->C5_PESOL )    // 07
	        AADD( aAux, (cAlias)->C5_PBRUTO )   // 08
	
	        AADD( aAux, (cAlias)->C6_QTDVEN )   // 09
	        AADD( aAux, (cAlias)->C6_VALOR )    // 10
	
	        AADD( aAux, (cAlias)->A1_NOME )     // 11
	        AADD( aAux, (cAlias)->A1_END ) 		// 12
	        AADD( aAux, (cAlias)->A1_BAIRRO )  	// 13
	        AADD( aAux, (cAlias)->A1_EST )     	// 14
	        AADD( aAux, (cAlias)->A1_CEP )     	// 15
	        AADD( aAux, (cAlias)->A1_CGC )      // 16
	
	        AADD( aAux, (cAlias)->B1_COD )      // 17
	        AADD( aAux, (cAlias)->B1_DESC )     // 18
	        AADD( aAux, (cAlias)->B1_PESO)      // 19
	
	        AADD( aPedidos, aAux )
	
	        (cAlias)->( dbSkip() )
	
	    Enddo
	
	    If Len( aPedidos ) > 0
	        AADD( aRetorno, aPedidos )
	    Endif
	
	Endif

	(cAlias)->( dbCloseArea() )

Return( aRetorno )

/*/
------------------------------------------------------------------
{Protheus.doc} RetPedCons
Retorna os Pedidos para serem consultados na TranspoFrete

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function RetPedCons( )

    Local cAlias    := GetNextAlias()
    Local aRetorno  := {}

    BEGINSQL ALIAS cAlias

        SELECT C5.C5_NUM, C5.C5_XIDLINC, C5.C5_EMISSAO
        FROM %table:SC5% C5 
        WHERE
            C5.C5_FILIAL = %xFilial:SC5% AND
            C5.C5_XLIDOTF = '1' AND
            C5.C5_ORIGEM = 'B2C' AND
			C5.C5_XIDLINC <> '' AND
			C5.C5_TRANSP = '' AND
			C5.C5_REDESP = '' AND
			C5.C5_NOTA = '' AND
            C5.C5_EMISSAO >= '20170101' AND
            C5.%notDel%
		ORDER BY C5.C5_NUM

    ENDSQL

    While (cAlias)->(!Eof())

        AADD( aRetorno, { (cAlias)->C5_NUM, (cAlias)->C5_XIDLINC, (cAlias)->C5_EMISSAO } )

        (cAlias)->( dbSkip() )

    Enddo

	(cAlias)->( dbCloseArea() )

Return( aRetorno )

/*/
------------------------------------------------------------------
{Protheus.doc} updTabPed
Atualiza tabela de Pedidos

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function updTabPed( cStatus, cPedido, aTransp )

	Local nX			:= 0
	Local cCodTransp	:= ""
	Local cMsg			:= ""

	Default cStatus		:= ""	// 1-Enviado - 2-Retornado
	Default cPedido		:= ""
	Default aTransp		:= {}

	If cStatus $ "1|2" .And. !Empty( cPedido )
	
		dbSelectArea( "SC5" )
		dbSetOrder( 1 )
		If dbSeek( xFilial( "SC5" ) + cPedido )
			
			RecLock( "SC5", .F. )

				If cStatus == "1"

					nPedEnvG++

					SC5->C5_XLIDOTF	:= cStatus
				
				Else

					For nX := 1 To Len( aTransp )
					
						cMsg		:= ""
						cCodTransp	:= retCodTransp( aTransp[nX] )
						
						If !Empty( cCodTransp ) 

							nPedConsG++

							SC5->C5_XLIDOTF	:= cStatus
							SC5->C5_REDESP	:= cCodTransp	// Transportadora de Redespacho
							Exit
							
						Else

							cMsg := "Pedido: [" + cPedido + "] - Transportadora n�o cadastrada: [" + aTransp[nX] + "]"
							AADD( aErrorLog, cMsg )
							msgPrint( cMsg )
						
						Endif

					Next

				Endif

			MsUnlock()

		Endif
	
	Endif

Return

/*/
------------------------------------------------------------------
{Protheus.doc} retCodTransp
Retorna o Codigo da Transportadora

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function retCodTransp( cTransp )

Local aArea		:= ""
Local cCodigo	:= ""

If !Empty( cTransp )

	aArea := GetArea()

	dbSelectArea( "SA4" )
	dbSetOrder(3)
	If dbSeek( xFilial( "SA4" ) + cTransp )
		cCodigo := SA4->A4_COD
	Endif

	RestArea( aArea )

Endif

Return( cCodigo )

/*/
------------------------------------------------------------------
{Protheus.doc} retOrigem
Retorna o CEP Origem

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function retOrigem( cUf )

Local cCep		:= ""

Default cUf		:= ""

// HP_UFNORT - CORREIOS - CE;AL;PE;AP;PI;PA;MA;RN;RR;PB;SE
// HP_UFREGI - TAM      - DF;AC;ES;PR;GO;BA;AM;MG;RS;MS;RO;RJ;SC;MT;TO;SP

If !Empty( cUf ) .And. Alltrim( Upper( cUf ) ) $ GetMv( "HP_UFNORT" )
 	cCep := "61942005"
Else
 	//	cCep := "07190100" // Aeroporto de Guarulhos
	cCep := "05001100"
Endif

Return( cCep )

/*/
------------------------------------------------------------------
{Protheus.doc} retBody
Retorna o Texto do Body do Email

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function retBody( aErrorLog )

Local cHtml	:= ""
Local nX	:= 0

cHtml := '<!DOCTYPE HTML>'
cHtml += '<html lang="pt-br">'
cHtml += '<head>'
cHtml += '<meta charset="utf-8">'
cHtml += '</head>'
cHtml += '<body>'

cHtml += '<h3>Log de Processamento:</h3>'
cHtml += '<ul>'
For nX := 1 To Len( aErrorLog )
	cHtml += '<li>' + aErrorLog[nX] + '</li>'
Next
cHtml += '</ul>'

cHtml += '<h3>Outras Informa��es:</h3>'
cHtml += '<b>Fonte:</b> HJOBLINCROS'
cHtml += '<br><b>Data/Hora In�cio:</b> ' + cDtHrIni
cHtml += '<br><b>Data/Hora Final:</b> ' + cDtHrFim

If ( nPedEnv + nPedEnvG ) > 0
	cHtml += '<br><b>Pedidos Enviados/Gravados:</b> ' + Rtrim( Str( nPedEnv ) ) + " / " + Rtrim( Str( nPedEnvG ) )
Endif

If ( nPedCons + nPedConsG ) > 0
	cHtml += '<br><b>Pedidos Consultados/Gravados:</b> ' + Rtrim( Str( nPedCons ) ) + " / " + Rtrim( Str( nPedConsG ) )
Endif

cHtml += '</body>'
cHtml += '</html>'

Return( cHtml )

/*/
------------------------------------------------------------------
{Protheus.doc} StatusChange
Altera para status de erro do pedido.

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function StatusChange( cPedido )

	Local aArea		:= GetArea()
	Local aAreaSC5	:= SC5->( GetArea() )

	dbSelectArea( "SC5" )
	dbSetOrder( 1 )
	If dbSeek( xFilial( "SC5" ) + cPedido )
		RecLock( "SC5", .F. )
			SC5->C5_XLIDOTF = 'X'
		MsUnlock()
	Endif

	RestArea( aAreaSC5 )
	RestArea( aArea )

Return

/*/
------------------------------------------------------------------
{Protheus.doc} msgPrint
Printa mensagem no console.log

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function msgPrint( cMsg )
Return( ConOut( "[JOB][B2C][" + Dtoc( Date() ) + "-" + Time() + "][" + cMsg + "]" ) )


/*/
------------------------------------------------------------------
{Protheus.doc} HPATLINC
Ativa/desativa Job Lincros

@author Robson Melo
@since Jul/2021
@version 1.00
------------------------------------------------------------------
/*/
User Function HPATLINC()

Local aArea    := GetArea()
Local lStAtu := Nil
Local lConsNov := Nil
Local nOption   := 0
Local aButton  := {}
Local cMsgTela  := ""
Local cMvLincros := "HP_WSB2C"
	
lStAtu := GetNewPar(cMvLincros, .F.)
	
//Adiciona os bot�es
aAdd(aButton, Iif(lStAtu,  "Manter Habilitado",   "Habilitar"))   //Op��o 1
aAdd(aButton, Iif(!lStAtu, "Manter Desabilitado", "Desabilitar")) //Op��o 2
aAdd(aButton, "Cancelar")                                           //Op��o 3
	
//Mostra o aviso e pega o bot�o
cMsgTela := "Atualmente o par�metro de integra��o Linctos est� " + Iif(lStAtu, "HABILITADO", "DESABILITADO") + "." + CRLF
cMsgTela += "Deseja alterar?"
nOption := Aviso("Aten��o", cMsgTela, aButton, 2)
	
//Definindo a op��o nova
If nOption == 1
	lConsNov := .T.
ElseIf nOption == 2
	lConsNov := .F.
EndIf
	
//Se n�o for nulo
If lConsNov != Nil
	//Se o conte�do novo for diferente do atual
	If lConsNov != lStAtu
		PutMV(cMvLincros, lConsNov)
			
		Final("Aten��o", "A tela ser� fechada e deve ser aberta novamente!")
	EndIf
EndIf
	
RestArea(aArea)

Return
