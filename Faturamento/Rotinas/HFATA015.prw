#INCLUDE "RWMAKE.CH"
#INCLUDE "TBICODE.CH"
#INCLUDE "FONT.CH"

// Retornos poss�veis do MessageBox
#Define IDOK			    1
#Define IDCANCEL		    2
#Define IDYES			    6
#Define IDNO			    7
#Define CRLF  CHR(13)+CHR(10)

STATIC cPathPed    	:= "\Log\API\HFATA015\Pedido\"

User Function HFATA015()

	Local aPergs    := {}
	Private cArq       := ""
	Private cArqMacro  := "XLS2DBF.XLA"
	Private cTemp      := GetTempPath() //pega caminho do temp do client
	Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema
	Private aResps     := {}
	Private aArquivos  := {}
	Private nRet       := 0
	Private aTitle     := {}
	Private aLog     := {}

	aAdd(aTitle, "Os seguintes pedidos foram criados a partir dessa planilha" )
	aAdd(aLog,{} )
	aAdd(aLog[Len(aLog)], Padr("Pedido",12) + Padr("Cliente",12))

	Aadd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.T.,""})

	If ParamBox(aPergs,"Parametros", @aResps)

		Aadd(aArquivos, AllTrim(aResps[1]))

		cArq := AllTrim(aResps[1])

		Processa({|| ProcMov()})

	EndIF
Return

Static Function ProcMov()

	Local nFile		:= 0
	Local cFile		:= AllTrim(aResps[1])
	Local cLinha  	:= ""
	Local nLintit	:= 1
	Local nLin 		:= 0
	Local aDados  	:= {}
	Local nHandle 	:= 0


	nFile := fOpen(cFile,0,0) 
	If nFile == -1
		If !Empty(cFile)
			MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")
		EndIf
		Return
	EndIf

	nHandle := Ft_Fuse(cFile)
	Ft_FGoTop()
	nLinTot := FT_FLastRec()-1
	ProcRegua(nLinTot)

	While nLinTit > 0 .AND. !Ft_FEof()
		Ft_FSkip()
		nLinTit--
	EndDo

	Do While !Ft_FEof()
		IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))
		nLin++
		cLinha := Ft_FReadLn()

		If Empty(AllTrim(StrTran(cLinha,',','')))
			Ft_FSkip()
			Loop
		EndIf

		cLinha := StrTran(cLinha,'"',"'")
		cLinha := '{"'+cLinha+','+alltrim(str(nLin+1))+'"}'
		cLinha := StrTran(cLinha,';',',')
		cLinha := StrTran(cLinha,',','","')

		Aadd(aDados, &cLinha)
		FT_FSkip()
	EndDo

	FT_FUse()

	ProcRegua(0)
	nRet:= MessageBox("Deseja realmente importar o arquivo:" +Upper(cFile)+ " ?","Confirma��o",4)

	If nRet == 6
		U_ImpFis(aDados)
	Else
		MessageBox("Cancelado pelo usu�rio !","Cancelado",16)
	EndIf

Return

//ROTINA DE INCLUSAO AUTOMATICA DE PEDIDOS
User Function ImpFis(pMatriz)

//pMatriz --> matriz contendo os dados do arq csv

	Local nXT           := 0
	Local aLinha        := {}
	Local nContIT		:= 0
	Local cArqPedErro 	:= "Pedido_Error_HFATA015_"
	Private pMatrizped  := pMatriz
	Private lMsErroAuto := .F.
	Private cTipoped    := ""
	Private cTabPed     := Alltrim(MV_PAR02)
	Private cCliente    := ""
	Private cLjcli      := ""
	Private cLjEnt      := ""
	Private cCondPag    := ""
	Private cProd       := ""
	Private cQtd        := ""
	Private cPrcVenda   := ""
	Private cPrcUni     := ""
	Private cValor      := ""
	Private cTES        := ""
	Private cLocal      := ""
	Private cTpped      := ""
	Private cTppra      := ""
	Private cPolCom     := ""
	Private dEmiss      := ""
	Private lOK         := .F.
	Private cPedCli     := ""
	Private nRet2       := 0

	aCabec := {}   // monta SC5
	aItens := {}   // monta SC6

	For nXT:=1 to len(pMatrizped)

		cCliente      := pMatrizped[nXT][1]
		cLjcli        := "0001"
		cLjEnt        := "0001"
		cCondPag      := pMatrizped[nXT][5]
		cTpped        := Strzero(Val(pMatrizped[nXT][3]),3)
		cPolCom       := Strzero(Val(pMatrizped[nXT][2]),3)

		cProd         := pMatrizped[nXT][6]
		cQtd          := Val(pMatrizped[nXT][7])
		cPrcVenda     := Val(pMatrizped[nXT][8])
		cPrcUni       := Val(pMatrizped[nXT][8])
		cValor        := Round(cQtd * cPrcVenda,2)
		dEmiss        := Ctod(pMatrizped[nXT][4])
		cLocal		  := pMatrizped[nXT][9]	
		cTes		  := pMatrizped[nXT][10]	

		aLinha := {}

		nContIT++

		Aadd(aLinha,{"C6_ITEM"   ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.) ,Nil})
		Aadd(aLinha,{"C6_PRODUTO",cProd                   	  ,Nil})
		Aadd(aLinha,{"C6_QTDVEN" ,cQtd                        ,Nil})
		Aadd(aLinha,{"C6_PRCVEN" ,Round(cPrcUni,3)            ,Nil})
		Aadd(aLinha,{"C6_PRUNIT" ,Round(cPrcUni,3)            ,Nil})
		Aadd(aLinha,{"C6_VALOR" ,cValor                       ,Nil})
		Aadd(aLinha,{"C6_TES"    ,cTES                        ,Nil})
		Aadd(aLinha,{"C6_LOCAL" ,cLocal                       ,Nil})

		Aadd(aItens,aLinha)

		If nXT == len(pMatrizped)
			lOk := .T.
		ElseIf  cCliente != pMatrizped[nXT+1][1]
			lOk := .T.
		EndIF

		If lOK
			aCabec := {}   // monta SC5

			Aadd(aCabec,{"C5_POLCOM" ,cPolCom                  ,Nil})
			Aadd(aCabec,{"C5_EMISSAO",dEmiss                   ,Nil})
			Aadd(aCabec,{"C5_CLIENTE",Padr(cCliente,6)         ,Nil})
			Aadd(aCabec,{"C5_LOJACLI",StrZero(val(cLjcli),4)   ,Nil})
			Aadd(aCabec,{"C5_TIPO"   ,"N"                      ,Nil})
			Aadd(aCabec,{"C5_TPPED"  ,cTpped                   ,Nil})
			Aadd(aCabec,{"C5_LOJAENT",StrZero(val(cLjEnt),4)   ,Nil})
			Aadd(aCabec,{"C5_CONDPAG",cCondPag                 ,Nil})
			Aadd(aCabec,{"C5_ORIGEM" ,"HFATA015"	           ,Nil})

			//* Inclusao Utilizando MsExecAuto
			If lOk 
				MsExecAuto({|x, y, z| MATA410(x, y, z)}, aCabec, aItens,3)
			EndIf

			If !lMsErroAuto //.AND. STOD(dDtent) > Date()
				aAdd(aLog[Len(aLog)], Padr(SC5->C5_NUM,12) + Padr(cCliente,12))

				Logs("Pedido: " + SC5->C5_NUM)
				Logs("Cliente: " + cCliente)
				Logs("==============================================================")

				aItens := {}   // monta SC6
				lOK := .F.
				nContIT := 0
			Else
				// Se erro , avisa, e sai do FOR
				Messagebox("Erro na inclusao!, favor verificar linha:" +cvaltochar(nXT+1)+ " do arquivo ! ","Erro" ,16)
				cError	:= MostraErro( cPathPed, cArqPedErro )
				consoleLog( "Pedido HFATA015 [] - Erro: " + cError )
				aCabec := {}   // monta SC5
				aItens := {}   // monta SC6
				lOK := .F.
				nXT := len(pMatrizped)
				nContIT := 0
				Loop
			EndIf
		EndIf

	Next nXT
	nRet2 := MessageBox("Deseja imprimir o LOG dos pedidos gerados ?","LOG",4)
	If nRet2 == 6
		fMakeLog( aLog , aTitle , NIL  , .T. , "HFATA015" , NIL , NIL, NIL, NIL, .F. )
	Endif
Return(.T.)

/*
* Gera��o do arquivo de log.
* @Author Geyson Albano
* @Data 10/06/2020
*/
Static Function Logs(cTexto)
	Local cCodLog	:= SubStr(DtoS(Date()),1,6)
	Local cPasta	:= "\log\HFATA015"
	Local FC_NORMAL := 0

	ConOut(DtoC(Date(),"dd/mm/yy")+"|"+Time()+"|"+cUserName+"| "+cTexto)

	FWMakeDir(cPasta)
	If File(cPasta+"\"+cCodLog+".txt")
		nHdl := FOpen(cPasta+"\"+cCodLog+".txt" , 2 + 64 )
	Else
		nHdl := FCreate(cPasta+"\"+cCodLog+".txt",FC_NORMAL)
	EndIf
	FSeek(nHdl, 0, 2)
	FWrite(nHdl, DtoC(Date(),"dd/mm/yy")+"|"+Time()+"|"+cUserName+"| "+cTexto+CRLF )

	FClose(nHdl)
Return

/*/
------------------------------------------------------------------
{Protheus.doc} ConsoleLog
Gera log de processamento
@author Geyson Albano
@since Nov/2021
@version 1.00
------------------------------------------------------------------
/*/
Static Function ConsoleLog( cMsg )
Return( ConOut( "[HFATA015][" + Dtoc( Date() ) + "-" + Time() + "] " + cMsg ) )
