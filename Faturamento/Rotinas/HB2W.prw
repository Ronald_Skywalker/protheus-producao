#INCLUDE "PROTHEUS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"

#DEFINE nPCNPJ 		1	
#DEFINE nPNome 		2
#DEFINE nPEMail		3
#DEFINE nPTelef	    4
#DEFINE nPEnder		5
#DEFINE nPNumero	6
#DEFINE nPCompl     7	
#DEFINE nPBairro	8
#DEFINE nPCidade	9
#DEFINE nPUF 		10	
#DEFINE nPPais		11
#DEFINE nPCEP  		12

STATIC cIdEmp		:= "01"
STATIC cIdFil		:= "0101"

STATIC cMailTI		:= "sergio.fuzinaka@hopelingerie.com.br;anderson.almeida@hopelingerie.com.br;cleonilson.ti@hopelingerie.com.br;geyson.albano@hopelingerie.com.br;jose.ferrari@hopelingerie.com.br;robson.vieira@hopelingerie.com.br;ronaldo.ti@hopelingerie.com.br"
STATIC cMailDest	:= "nilton.lira@bonjourlingerie.com.br"

STATIC cAssunto		:= "Log de Integra��o Protheus x B2W"
STATIC aErrorLog	:= {}
STATIC aErrorCli	:= {}
STATIC cDtHrIni 	:= ""
STATIC cLog 	   	:= ""

/*/
------------------------------------------------------------------
{Protheus.doc} JobB2W
Job de Integra��o Protheus x B2W

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
User Function JobB2W()

	// Configuracao de ambiente
	RpcClearEnv()
	RpcSetType( 3 )
	RpcSetEnv( cIdEmp, cIdFil )

	//--------------------------------------
	// Rotina de Integracao Protheus x B2W
	//--------------------------------------
	intB2W()

	If Len( aErrorLog ) > 0 .And. !Empty( cMailTI )
		U_SendMail( cMailTI, cAssunto, getBody( aErrorLog ) )
	Endif

	If Len( aErrorCli ) > 0 .And. !Empty( cMailDest )
		U_SendMail( cMailDest, cAssunto, getBody( aErrorCli ) )
	Endif

	delClassIntf()

	Sleep( 60000 )

Return

/*/
------------------------------------------------------------------
{Protheus.doc} JobNfB2W
Job de Integra��o das NF na B2W

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
User Function JobNfB2W()

	// Configuracao de ambiente
	RpcClearEnv()
	RpcSetType( 3 )
	RpcSetEnv( cIdEmp, cIdFil )

	//--------------------------------------
	// Rotina de Integracao NF na B2W
	//--------------------------------------
	intNfB2W()

	delClassIntf()

	Sleep( 60000 )

Return

/*/
------------------------------------------------------------------
{Protheus.doc} intB2W()
Rotina de Integra��o Protheus x B2W

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function intB2W()

	Local aOrder		:= {}
	Local cResponse		:= ""
	Local cEmail  	    := ""
	Local cKey			:= ""
	Local cToken   		:= ""
	Local nAcFalha		:= 0
	Local cCodCli		:= ""	
	Local cLojCli		:= ""
	Local nCont			:= 0
	Local aTipos		:= {}
	Local cMsg			:= ""

	Private cUrl   	    := ""
	Private aHeader     := {}
    Private cCodeB2W	:= ""
	Private cPedB2W		:= ""
	Private oRestClient := Nil
	Private oJson		:= Nil
	Private cDtTime 	:= ""

	// Configuracao de Email
	cMailTI		:= Alltrim( Lower( GetMv( "B2W_MAILTI", .T., cMailTI ) ) )
	cMailDest	:= Alltrim( Lower( GetMv( "B2W_MAILDE", .T., cMailDest ) ) )

	// Configuracao
	cDtHrIni 	:= DToC( Date() ) + " " + Time()
	cUrl    	:= Alltrim( GetNewPar( "B2W_URL", "https://api.skyhub.com.br" ) )
	cEmail  	:= Alltrim( GetNewPar( "B2W_EMAIL", "nilton.lira@bonjourlingerie.com.br" ) )
	cKey		:= Alltrim( GetNewPar( "B2W_KEY", "rfrYjxfbyeE2UeQzy9Q6" ) )
	cToken  	:= Alltrim( GetNewPar( "B2W_TOKEN", "xk21bPa9jQ" ) )

	AADD( aHeader, 'Accept: application/json' )
	AADD( aHeader, 'Content-Type: application/json' )
	AADD( aHeader, 'X-User-Email: ' + cEmail )
	AADD( aHeader, 'X-Api-Key: ' + cKey )
	AADD( aHeader, 'X-Accountmanager-Key: ' + cToken )

	// Iniciando consulta a fila de pedidos
	msgPrint( "Iniciando consulta a fila de pedidos na SkyHub ..." )

	oRestClient:=FWRest():New( cUrl )
	oRestClient:setPath( "/queues/orders" ) 

	While nCont <= 100

		nCont++

		cCodB2W	:= ""
		cPedB2W := ""
				
		If oRestClient:Get( aHeader )

			cResponse := oRestClient:GetResult()

			FWJsonDeserialize( cResponse, @oJson )

			If Type( "oJson" ) <> "U" .And. Type( "oJson:Import_Info:Remote_Code" ) <> "U" .And. Type( "oJson:Code" ) <> "U"

				// Nome do Canal + Codigo B2W
				cCodB2W	:= Alltrim( oJson:Code )

				// Pedido B2W
				cPedB2W	:= Alltrim( oJson:Import_Info:Remote_Code )

				// Pedido com pagamento confirmado
				If Alltrim( oJson:Status:Type ) $ "APPROVED"
					
					// Valida Cliente
					If getCustomer( @cCodCli, @cLojCli )

						aOrder	:= getOrders( cCodCli, cLojCli )

						If postOrders( aOrder[1], aOrder[2] )

							// Deleta o pedido da fila
							delQueue( cUrl, aHeader )

						Endif

					Else

						nAcFalha++

					Endif

				Else
				
					If Alltrim( oJson:Status:Type ) $ "CANCELED/SHIPMENT_EXCEPTION/PAYMENT_OVERDUE"

						cMsg := "Pedido Web B2W [" + cPedB2W + "][" + Alltrim( oJson:Status:Type ) + "] "

						If "CANCELED" $ Alltrim( Upper( oJson:Status:Type ) )
							
							cMsg += "� o status que o pedido recebe quando � cancelado na B2W."
						
						ElseIf "SHIPMENT_EXCEPTION" $ Alltrim( Upper( oJson:Status:Type ) )
							
							cMsg += "� o status que o pedido recebe quando por algum motivo a entrega n�o foi realizada pela B2W."
						
						ElseIf "PAYMENT_OVERDUE" $ Alltrim( Upper( oJson:Status:Type ) )
							
							cMsg += "� o status que o pedido recebe ao ter o boleto com a data de pagamento vencido."
						
						Endif

						AADD( aTipos, cMsg )
						msgPrint( cMsg )

					Endif

					// Deleta o pedido da fila
					delQueue( cUrl, aHeader )

				Endif

			Else

				nAcFalha++

				cMsg := "Falha no parse: " + oRestClient:GetLastError()
				AADD( aErrorLog, cMsg )
				msgPrint( cMsg )

			Endif

		Else

			// 204 (NoContent) - Fila Vazia
			If "204" $ oRestClient:GetLastError()
				Exit
			Endif

			nAcFalha++

			cMsg := "Falha na consulta da fila de pedidos: " + oRestClient:GetLastError()
			AADD( aErrorLog, cMsg )
			msgPrint( cMsg )

		Endif

		If nAcFalha > 50
			Exit
		Endif

	Enddo

	// Tipos n�o implementados na integra��o
	If Len( aTipos  ) > 0 .And. !Empty( cMailTI )
		U_SendMail( cMailTI, cAssunto, getBody( aTipos ) )
	Endif

Return

/*/
------------------------------------------------------------------
{Protheus.doc} getOrders
Retorna os Pedidos

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function getOrders( cCodCli, cLojCli )

	Local aCabec	:= {}
	Local aItem 	:= {}
	Local aItens	:= {}
	Local nX 		:= 0
	Local dEmissao	:= dDataBase
	Local dEntrega	:= dDataBase
	Local nDesconto	:= 0
	Local nFrete	:= 0

	// Data emissao
	If Type( "oJson:Placed_At" ) <> "U" .And. !Empty( oJson:Placed_At )
		dEmissao	:= SToD( Substr( StrTran( Alltrim( oJson:Placed_At ), "-", "" ), 1, 8 ) )
	Endif 

	// Data previsao de entrega
	If Type( "oJson:Estimated_Delivery" ) <> "U" .And. !Empty( oJson:Estimated_Delivery )
		dEntrega	:= SToD( Substr( StrTran( Alltrim( oJson:Estimated_Delivery ), "-", "" ), 1, 8 ) )
	Endif 

	// Desconto
	If Type( "oJson:Discount" ) <> "U"
		nDesconto	:= oJson:Discount
	Endif 

	// Frete
	If Type( "oJson:Shipping_Cost" ) <> "U"
		nFrete		:= oJson:Shipping_Cost
	Endif 

	// Dados do Cabecalho
	AADD( aCabec, cCodB2W	)		// 01-C5_XCODB2W
	AADD( aCabec, cPedB2W	)		// 02-C5_XPEDRAK
	AADD( aCabec, dEmissao 	)		// 03-C5_EMISSAO
	AADD( aCabec, cCodCli	)		// 04-C5_CLIENTE
	AADD( aCabec, cLojCli	)		// 05-C5_LOJACLI
	AADD( aCabec, dEntrega	)		// 06-C5_FECENT
	AADD( aCabec, nDesconto	)		// 07-C5_DESCONT
	AADD( aCabec, nFrete	)		// 08-C5_FRETE
	
	// Dados dos Itens
	For nX := 1 To Len( oJson:Items )

		aItem := {}

		AADD( aItem, oJson:Items[nX]:ID				)	// 01-SKU
		AADD( aItem, oJson:Items[nX]:Qty 			)	// 02-Quantidade
		AADD( aItem, oJson:Items[nX]:Original_Price	)	// 03-Preco

		AADD( aItens, aItem )

	Next

Return( { aCabec, aItens } )

/*/
------------------------------------------------------------------
{Protheus.doc} getCustomer
Verifica se o cliente esta valido

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function getCustomer( cCodCli, cLojCli )

	Local lRet 			:= .T.

	Local aCustomer		:= {}
	
	Local cCNPJ			:= ""
	Local cNome			:= ""
	Local cEMail		:= ""
	Local cTelefone		:= ""
	Local cEndereco		:= ""
	Local cNumero		:= ""
	Local cComplemento	:= ""
	Local cBairro		:= ""
	Local cCidade		:= ""
	Local cUF 			:= ""
	Local cPais			:= ""
	Local cCEP  		:= ""
	Local cMsg			:= ""

	Private oCustomer	:= NIL
	Private oEndEntrega	:= NIL

	Default cCodCli		:= ""
	Default cLojCli		:= ""

	// CPF/CNPJ
	If Type( "oJson:Customer:Vat_Number" ) <> "U"

		cCNPJ := Rtrim( oJson:Customer:Vat_Number )

		If !Empty( cCNPJ )

			// Dados do Cliente
			If Type( "oJson:Customer" ) <> "U"

				oCustomer := oJson:Customer

				If Type( "oCustomer:Name" ) <> "U"
					cNome := Rtrim( oCustomer:Name )
				Endif 

				If Type( "oCustomer:Email" ) <> "U"
					cEmail := Rtrim( oCustomer:Email )
				Endif

				// Dados do Endereco de Entrega
				If Type( "oJson:Shipping_Address" ) <> "U"

					oEndEntrega := oJson:Shipping_Address

					If Type( "oEndEntrega:Street" ) <> "U"
						cEndereco := Rtrim( oEndEntrega:Street )
					Endif

					If Type( "oEndEntrega:Number" ) <> "U"
						cNumero := Rtrim( oEndEntrega:Number )
					Endif

					If Type( "oEndEntrega:Complement" ) <> "U"
						cComplemento := Rtrim( oEndEntrega:Complement )
					Endif

					If Type( "oEndEntrega:Neighborhood" ) <> "U"
						cBairro := Rtrim( oEndEntrega:Neighborhood )
					Endif

					If Type( "oEndEntrega:City" ) <> "U"
						cCidade := Rtrim( DecodeUtf8( oEndEntrega:City ) )
					Endif

					If Type( "oEndEntrega:Region" ) <> "U"
						cUF := Rtrim( oEndEntrega:Region )
					Endif

					If Type( "oEndEntrega:Country" ) <> "U"
						cPais := Rtrim( oEndEntrega:Country )
					Endif

					If Type( "oEndEntrega:PostCode" ) <> "U"
						cCEP := Rtrim( oEndEntrega:PostCode )
					Endif

					If Type( "oEndEntrega:Phone" ) <> "U"
						cTelefone := Rtrim( oEndEntrega:Phone )
					Endif

				Else
					
					lRet := .f.

					cMsg := "Id B2W [" + cPedB2W + "] - Pedido sem a tag <oJson:Shipping_Address> ( Endereco de Entrega )"
					AADD( aErrorLog, cMsg )
					msgPrint( cMsg )

				Endif

			Else
				
				lRet := .f.

				cMsg := "Id B2W [" + cPedB2W + "] - Pedido sem a tag <oJson:Customer> ( Dados do Cliente )"
				AADD( aErrorLog, cMsg )
				msgPrint( cMsg )

			Endif

		Else

			lRet := .F.

			cMsg := "Id B2W [" + cPedB2W + "] - Pedido com a tag <vat_number> ( CPF/CNPJ ) Vazio"
			AADD( aErrorLog, cMsg )
			msgPrint( cMsg )

		Endif

	Else

		lRet := .F.

        cMsg := "Id B2W [" + cPedB2W + "] - Pedido sem a tag <vat_number> ( CPF/CNPJ )"
		AADD( aErrorLog, cMsg )
        msgPrint( cMsg )

	Endif

	If lRet 

		AADD( aCustomer, cCNPJ )
		AADD( aCustomer, HCharEsp( Upper( cNome ) ) )
		AADD( aCustomer, cEMail )
		AADD( aCustomer, cTelefone )
		AADD( aCustomer, HCharEsp( Upper( cEndereco ) ) )
		AADD( aCustomer, cNumero )
		AADD( aCustomer, HCharEsp( Upper( cComplemento ) ) )
		AADD( aCustomer, HCharEsp( Upper( cBairro ) ) )
		//AADD( aCustomer, Upper( cCidade ) )
		AADD( aCustomer, HCharEsp( Upper( cCidade ) ) )
		AADD( aCustomer, cUF )
		AADD( aCustomer, cPais )
		AADD( aCustomer, cCEP )

		lRet := HB2WCli( aCustomer, @cCodCli, @cLojCli )
 
	Endif

Return( lRet )

/*/
------------------------------------------------------------------
{Protheus.doc} delQueue
Deleta o pedido da fila

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function delQueue( cUrl, aHeader )

	Local cMsg			:= ""
	Local cStatusCode	:= ""

	Private oRestQueue	:= Nil	

	oRestQueue:=FWRest():New( cUrl )
	oRestQueue:setPath( "/queues/orders/" + cCodB2W ) 
	oRestQueue:Delete( aHeader )

	cStatusCode := Alltrim( oRestQueue:oResponseH:cStatusCode )

	If  cStatusCode $ "204"
		cMsg := "Id B2W [" + cPedB2W + "] - Status Code [" + cStatusCode + "] - Pedido excluido da fila com sucesso"
	Else
		cMsg := "Id B2W [" + cPedB2W + "] - Status Code [" + cStatusCode + "] - " + Alltrim( oRestQueue:GetResult() )
	Endif

	msgPrint( cMsg )

Return

/*/
------------------------------------------------------------------
{Protheus.doc} postNfe
Rotina de Integracao NF na B2W

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function intNfB2W()

	Local nX 		:= 0
	Local aNfe		:= {}
	Local nRecno	:= 0
	Local cCodB2W	:= ""
	Local cNfe 		:= ""
	Local cMsg		:= ""

	// Iniciando o envio da chave da nf-e
	msgPrint( "Iniciando o envio da nf-e para SkyHub ..." )

	// Retorna os pedidos j� faturados
	aNfe := getNfe()

	If Len( aNfe ) > 0

		For nX := 1 To Len( aNfe )

			cMsg	:= ""

			nRecno	:= aNfe[nX][01]
			cCodB2W	:= aNfe[nX][02]
			cNfe	:= aNfe[nX][03]

			// API de envio da Chave da Nfe
			If postInvoice( cCodB2W, cNFe )

				SC5->( dbGoTo( nRecno ) )
				SC5->( RecLock( "SC5", .F. ) )
				SC5->C5_XNFB2W := Date()
				SC5->( MsUnlock() )

			Else

				cMsg := "Pedido [" + Alltrim( SC5->C5_NUM ) + "] - Id B2W [" + cCodB2W + "] - Chave NF-e [" + cNfe + "] Falha no envio da Chave da Nfe."
	
				msgPrint( cMsg )

			Endif

		Next

	Endif

Return

/*/
------------------------------------------------------------------
{Protheus.doc} getNfe
Retorna os Codigos B2W e as Chaves das Nfes

@author Sergio S. Fuzinaka
@since Mar/200
@version 1.00
------------------------------------------------------------------
/*/
Static Function getNfe()

    Local aRet		:= {}
    Local cAlias	:= GetNextAlias()

    BEGINSQL ALIAS cAlias

        SELECT DISTINCT SC5.R_E_C_N_O_ C5_RECNO, 
				SC5.C5_XCODB2W, 
				SF2.F2_CHVNFE
        FROM %table:SC5% SC5 
			INNER JOIN %table:SF2% SF2
				ON SF2.F2_FILIAL = %xFilial:SF2% AND
				SF2.F2_DOC = SC5.C5_NOTA AND 
				SF2.F2_SERIE = SC5.C5_SERIE AND 
				SF2.F2_CHVNFE <> '' AND 
				SF2.%notDel%
        WHERE
            SC5.C5_FILIAL = %xFilial:SC5% AND
			SC5.C5_NOTA <> '' AND
			SC5.C5_XCODB2W <> '' AND
            SC5.C5_ORIGEM = 'B2W' AND
			SC5.C5_XNFB2W = '' AND
            SC5.C5_EMISSAO >= '20200501' AND
            SC5.%notDel%

    ENDSQL

    While (cAlias)->( !Eof() )

        AADD( aRet, { (cAlias)->C5_RECNO, (cAlias)->C5_XCODB2W, (cAlias)->F2_CHVNFE } )

        (cAlias)->( dbSkip() )

    Enddo

	(cAlias)->( dbCloseArea() )

Return( aRet )

/*/
------------------------------------------------------------------
{Protheus.doc} postInvoice
API de envio da Chave da NF-e

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function postInvoice( cCodB2W, cNFe )

	Local lSuccess 	:= .T.
	Local aHeader	:= {}
	
	Local cJson		:= '{ "status": "order_invoiced", "invoice": { "key": "' + Alltrim( cNFe ) + '" } }'

	Local cUrl    	:= Alltrim( GetNewPar( "B2W_URL", "https://api.skyhub.com.br" ) )
	Local cEmail  	:= Alltrim( GetNewPar( "B2W_EMAIL", "nilton.lira@bonjourlingerie.com.br" ) )
	Local cKey		:= Alltrim( GetNewPar( "B2W_KEY", "rfrYjxfbyeE2UeQzy9Q6" ) )
	Local cToken  	:= Alltrim( GetNewPar( "B2W_TOKEN", "xk21bPa9jQ" ) )
	Local cMsg		:= ""

	Private oRest	:= NIL

	AADD( aHeader, 'Accept: application/json' )
    AADD( aHeader, 'Content-Type: application/json' )
    AADD( aHeader, 'X-User-Email: ' + cEmail )
    AADD( aHeader, 'X-Api-Key: ' + cKey )
    AADD( aHeader, 'X-Accountmanager-Key: ' + cToken )

	oRest:=FWRest():New( cUrl )
	oRest:setPath( "/orders/" + Rtrim( cCodB2W ) + "/invoice" ) 
	oRest:SetPostParams( AllTrim( cJson ) )
	
	lSuccess	:= oRest:Post( aHeader )

	cStatusCode	:= Alltrim( oRest:oResponseH:cStatusCode )

	If lSuccess
		cMsg := "Id B2W [" + Rtrim( cCodB2W ) + "] - Status Code [" + cStatusCode + "] - Chave NF-e [" + cNFe + "] enviada com sucesso."
	Else
		cMsg := "Id B2W [" + Rtrim( cCodB2W ) + "] - Status Code [" + cStatusCode + "] - Chave NF-e [" + cNFe + "] - " + Alltrim( StrTran( StrTran( StrTran( oRest:GetResult(), "{", "" ), "}", "" ), '"', "" ) )
	Endif

	msgPrint( cMsg )

Return( lSuccess )

/*/
------------------------------------------------------------------
{Protheus.doc} postOrders
Grava o Pedidos no Protheus

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function postOrders( aCabOrder, aItOrder )

	Local lRet 			:= .F.
	Local ldelQueue		:= .F.
	Local cOrigem		:= "B2W"
	
	Local cMsg			:= ""
	Local cTime   		:= Time()
	Local cDtTime 		:= DtoS(dDataBase) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
	Local cHorImp		:= SubStr(cTime,1,5)
	Local nX			:= 0
	Local nY			:= 0
	Local cPolBonif		:= AllTrim(SuperGetMV("MV_XPOLBON",.F.,"240"))	// Politica comercial de bonificacao
	Local cTesBonif		:= AllTrim(SuperGetMV("MV_XTESBON",.F.,"754"))	// Tes para pedidos de bonificacao
	Local cAmzPik 		:= AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0"))	// Armazem padrao picking
	Local cAmzPul 		:= AllTrim(SuperGetMV("MV_XAMZPUL",.F.,"E1"))	// Armazem padrao pulmao
	Local aPed 			:= {}

	//-------------------------------------------
	// Definicao das variaveis do Cabecalho
	//-------------------------------------------
	Local aCabec 		:= {}
	Local cNroPed 		:= ""
	Local cCodB2W		:= ""
	Local cPedB2W		:= ""
	Local dEmissao		:= dDataBase
	Local cCodCli		:= ""
	Local cLojCli		:= ""
	Local dEntrega  	:= dDataBase
	Local nDesconto		:= 0
	Local nFrete		:= 0
	
	// Definir
	Local cNatOper		:= Alltrim( GetMv("B2W_NATOP",.T.,"11116002") )	// Natureza da Operacao
	Local cPolCom 		:= Alltrim( GetMv("B2W_POLCOM",.T.,"018") )		// Politica Loja Virtual
	Local cTpPed		:= Alltrim( GetMv("B2W_TPPED",.T.,"132") )		// Tipo do Pedido B2W

	Local cCondPag		:= Alltrim( GetMv("B2W_CONDPG",.T.,"C61") )		// A Vista
	Local cTabela   	:= Alltrim( GetMv("B2W_TABELA",.T.,"859") )		// Tabela B2W
	
	//-------------------------------------------------------------------------------------------
	//Utilizado para teste de cond. pagto, para refletir no vencimento do titulo no financeiro
	//Local cCondPag	:= GetMv("B2W_CONDPG",.T.,"059")		// A Vista
	//Local cTabela   	:= GetMv("B2W_TABELA",.T.,"142")		// Tabela B2W
	//-------------------------------------------------------------------------------------------

	Local cPrazo		:= "001"								// Prazo de Entrega - Imediato
	Local cDesPrazo 	:= ""

	Local cXCanal  		:= Alltrim( GetNewPar( "B2W_CANAL" , "007" ) ) 
	Local cXCanalD 		:= ""
	Local cXCodReg 		:= Alltrim( GetNewPar( "B2W_CODREG" , "11045001" ) )
	Local cXDesReg 		:= ""
	Local cXMicReg 		:= Alltrim( GetNewPar( "B2W_MICREG", "11045" ) )
	Local cXMicDes 		:= ""

	//-------------------------------------------
	// Definicao das variaveis dos itens
	//-------------------------------------------
	Local cItem			:= ""
	Local aItens		:= {}
	Local aLinha		:= {}
	Local cProd 		:= ""
	Local cDescri		:= ""
	Local nQtde   		:= 0
	Local nVlrUni 		:= 0
	Local nSldPik 		:= 0
	Local nSldPul 		:= 0
	Local cTes 			:= ""
	Local aNotSld		:= {}
	Local aNotEnd		:= {}

	// Definir
	Local cItOper 		:= "01"
	Local cItAmz  		:= "E0"

	Private lMsHelpAuto 	:= .T.
	Private lMsErroAuto 	:= .F.

	// Atribuicao de variaveis
	cDtTime 	:= DtoS(dDataBase) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
	cCodB2W		:= aCabOrder[01]
	cPedB2W		:= aCabOrder[02]
	dEmissao	:= aCabOrder[03]
	cCodCli		:= aCabOrder[04]
	cLojCli		:= aCabOrder[05]
	dEntrega   	:= aCabOrder[06]
	nDesconto	:= aCabOrder[07]
	nFrete		:= aCabOrder[08]

	If !Empty( cXCanal )
		cXcanalD := Posicione("ZA3",1,xFilial("ZA3")+cXCanal,"ZA3_DESCRI")
	Endif 
	
	If !Empty( cXCodReg )
		cXDesReg := Posicione("SZ8",1,xFilial("SZ8")+cXCodReg,"Z8_DESC")
	Endif 

	If !Empty( cXMicReg )
		cXMicDes := Posicione("ZA4",1,xFilial("ZA4")+cxMicReg,"ZA4_DESCMA")
	Endif 

	msgPrint( "INICIO DO PROCESSAMENTO... PEDIDO WEB: " + cPedB2W )

	BEGIN SEQUENCE

		cNextAlias := GetNextAlias()
		
		BeginSQL Alias cNextAlias
			SELECT C5_XPEDRAK FROM %TABLE:SC5% SC5 WHERE C5_XPEDRAK = %exp:cPedB2W% AND SC5.%NOTDEL% 
		EndSQL

		// Valida se o pedido ja foi gerado
		If !Empty( Alltrim( ( cNextAlias )->C5_XPEDRAK ) )
			ldelQueue	:= .T.
			cMsg 		:= "PEDIDO WEB: " + cPedB2W + " JA IMPORTADO NO PROTHEUS."
			SalvaLog( "ERRO_PEDIDO", cMsg, .T. )
			( cNextAlias )->( dbCloseArea() )
			BREAK
		EndIf
	
		( cNextAlias )->( dbCloseArea() )

		// Gerando numero do pedido
		cNroPed := GetSX8Num( "SC5", "C5_NUM",, 1 )

		dbSelectArea( "SC5" )
		dbSetOrder( 1 )
		While dbSeek( xfilial( "SC5" ) + cNroPed )
			ConfirmSX8()
			cNroPed := SOMA1( cNroPed )
		EndDo

		msgPrint( "PEDIDO NOVO. SERA INCLUIDO NO PROTHEUS COM O NUMERO: " + cNroPed )

		If Empty( dEntrega )
			dEntrega := dDataBase
		EndIf

		// Descricao do Prazo de Entrega
		If !Empty( cPrazo )
			dbSelectArea("SZ6")
			dbSetOrder(1)
			If dbSeek( xFilial("SZ6") + cPolCom + cPrazo )
				cDesPrazo := SZ6->Z6_DESC
			Endif
		EndIf

		//----------------------------------------------------------
		// Definicao do Cabecalho do Pedido
		//----------------------------------------------------------
		aCabec   := {}

		aadd( aCabec, { "C5_FILIAL"		,xFilial( "SC5" )	,Nil } )
		aadd( aCabec, { "C5_NUM"    	,cNroPed			,Nil } )
		aadd( aCabec, { "C5_EMISSAO"	,dEmissao			,Nil } )
		aadd( aCabec, { "C5_POLCOM" 	,cPolCom			,Nil } )
		aadd( aCabec, { "C5_TPPED" 		,cTpPed				,Nil } )
		aadd( aCabec, { "C5_CLIENTE"	,cCodCli			,Nil } )
		aadd( aCabec, { "C5_LOJACLI"	,cLojCli			,Nil } )
		aadd( aCabec, { "C5_TIPO"   	,"N"				,Nil } )
		aadd( aCabec, { "C5_CONDPAG"	,cCondPag			,Nil } )
		aadd( aCabec, { "C5_FECENT" 	,dEntrega			,Nil } )
		aadd( aCabec, { "C5_XPEDRAK"	,cPedB2W			,Nil } )
		aadd( aCabec, { "C5_XPEDWEB"	,cPedB2W			,Nil } )
		aadd( aCabec, { "C5_DESCONT"	,nDesconto			,Nil } )
		aadd( aCabec, { "C5_CLIENT" 	,cCodCli			,Nil } )
		aadd( aCabec, { "C5_LOJAENT"	,cLojCli			,Nil } )
		aadd( aCabec, { "C5_FRETE"  	,nFrete				,Nil } )

		aadd( aCabec, { "C5_XBLQ"		,"L"				,Nil } )
		aadd( aCabec, { "C5_PRAZO"		,cPrazo				,Nil } )
		aadd( aCabec, { "C5_XDPRAZO" 	,cDesPrazo			,Nil } )

		aadd( aCabec, { "C5_NATUREZ"	,cNatOper			,Nil } ) 
		aadd( aCabec, { "C5_XCANAL" 	,cXCanal			,Nil } )
		aadd( aCabec, { "C5_XCANALD"	,cXCanalD			,Nil } )
		aadd( aCabec, { "C5_XCODREG"	,cXCodReg			,Nil } )
		aadd( aCabec, { "C5_XDESREG"	,cXDesReg			,Nil } )
		aadd( aCabec, { "C5_XMICRRE"	,cXMicReg			,Nil } )
		aadd( aCabec, { "C5_XMICRDE"	,cXMicDes			,Nil } )

		aadd( aCabec, { "C5_ORIGEM" 	,cOrigem			,Nil } )
		aadd( aCabec, { "C5_TABELA" 	,cTabela			,Nil } )
		
		aadd( aCabec, { "C5_XHORIMP"	,cHorImp			,Nil } )
		aadd( aCabec, { "C5_XDATIMP"	,dDataBase			,Nil } )
		aadd( aCabec, { "C5_XCODB2W"   	,cCodB2W			,Nil } )

		//---------------------------------------------
		// Definicao dos Itens do Pedido
		//---------------------------------------------
		aItens	:= {}
		cItem	:= "01"

		For nX := 1 to Len( aItOrder )

			cTes	:= ""
			cDescri	:= ""
			
			cProd	:= aItOrder[nX][1]

			dbSelectArea( "SB1" )
			dbSetOrder( 1 )
			If !dbSeek( xFilial( "SB1" ) + cProd )
				cMsg := "O PRODUTO "+ cProd + " NAO FOI ENCONTRADO NO PROTHEUS."
				SalvaLog( "ERRO_PROD", cMsg, .T. )
				BREAK
			EndIf

			nQtde   := aItOrder[nX][2]
			nVlrUni := aItOrder[nX][3]
			
			// Valida saldo do produto
			nSldPik := HFATSaldo( cProd, cAmzPik )
			nSldPul := HFATSaldo( cProd, cAmzPul )

			If nQtde > ( nSldPik + nSldPul )
				If GETMV( "HP_SLDB2W", .T., .T. )
					//aAdd( aNotSld, { cProd } )
				Endif
			EndIf

			// Valida endereco do produto
			dbSelectArea( "SBE" )
			dbOrderNickName( "PRODUTO" )
			If !dbSeek( xFilial( "SBE" ) + cProd )
				//aAdd( aNotEnd, { cProd } )
			EndIf

			If nVlrUni > 0

				// TES Inteligente
				cTes := MaTesInt( 2, cItOper, cCodCli, cLojCli, "C", cProd )

				If Empty( cTes )
					cMsg := "TES INTELIGENTE NAO ENCONTRADA PARA O PRODUTO: " + cProd + ". CLIENTE PROTHEUS: " + cCodCli + "-" + cLojCli+". TIPO OPERACAO: " + cItOper + ". PEDIDO: " + cPedB2W + "."
					SalvaLog( "ERRO_TES", cMsg, .T. )
					BREAK
				EndIf

			Else

				cMsg := "PRODUTO: " + cProd + " SEM PRECO UNITARIO. PEDIDO: " + cPedB2W + "."
				SalvaLog( "ERRO_PROD", cMsg, .T. )
				BREAK

			EndIf

			cDescri := GetAdvFVal( "SB4", "B4_DESC", xFilial("SB4") + cProd, 1, "" )

			aLinha 	:= {}

			aadd(aLinha,{"C6_FILIAL"		,xFilial("SC6")				,Nil})
			aadd(aLinha,{"C6_NUM"			,cNroPed					,Nil})
			aadd(aLinha,{"C6_ITEM"			,cItem						,Nil})
			aadd(aLinha,{"C6_PRODUTO"		,cProd						,Nil})
			aadd(aLinha,{"C6_QTDVEN"		,nQtde						,Nil})
			aadd(aLinha,{"C6_PRCVEN"		,nVlrUni					,Nil})
			aadd(aLinha,{"C6_PRUNIT"		,nVlrUni					,Nil})
			aadd(aLinha,{"C6_VALOR"			,Round(nQtde*nVlrUni,2)		,Nil})

			If AllTrim( cPolCom ) == AllTrim( cPolBonif )
				aadd(aLinha,{"C6_TES"		,cTesBonif					,Nil})
			Else
				aadd(aLinha,{"C6_OPER"		,cItOper					,Nil})
				aadd(aLinha,{"C6_TES"		,cTes						,Nil})
			EndIf

			aadd(aLinha,{"C6_LOCAL"			,cItAmz						,Nil})
			aadd(aLinha,{"C6_GRADE"			,"N"						,Nil})

			If !Empty( cDescri )
				aadd(aLinha,{"C6_DESCRI"	,cDescri					,Nil})
			Endif

			aadd(aLinha,{"C6_XITPRES"		,"N"						,Nil})
			aadd(aLinha,{"C6_ENTREG"		,dEntrega					,Nil})

			aadd(aItens,aLinha)

			cItem := SOMA1(cItem)

		Next 

		// Valida saldo do produto
		If Len( aNotSld ) > 0
			cMsg := "EXISTEM PRODUTOS SEM SALDO SUFICIENTE."
			For nY := 1 to Len( aNotSld )
				cMsg += "," + aNotSld[nY][1]
			Next
			SalvaLog( "ERRO_SALDO", cMsg, .T. )
			BREAK
		EndIf

		// Valida endereco do produto
		If Len( aNotEnd ) > 0
			cMsg := "PRODUTOS NAO POSSUEM AMARRACAO COM ENDERECO DE PICKING."
			For nY := 1 to Len( aNotEnd )
				cMsg += "," + aNotEnd[nY][1]
			Next
			SalvaLog( "ERRO_ENDERECO", cMsg, .T. )
			BREAK
		EndIf

		//------------------------------------------------
		// Inicio da geracao do pedido no Protheus
		//------------------------------------------------
		BEGIN TRANSACTION

			msgPrint( "PEDIDO: " + cNroPed + " SERA INCLUIDO NO PROTHEUS." )

			MsExecAuto( { |x,y,z| MATA410(x,y,z) }, aCabec, aItens, 3 )

			msgPrint( "EXEC_AUTO MATA410" )

			// Erro na geracao do pedido
			If lMsErroAuto

				cMsg := "ERRO NA GERACAO DO PEDIDO: " + cNroPed + ". PEDIDO WEB: " + cPedB2W

				cErro := MostraErro( "\log\API\B2W\Erro\", cDtTime + "_ERRO_PEDIDO_" + cPedB2W + ".log" )

				SalvaLog( "ERRO_EXECAUTO", cMsg, .T. )

				DisarmTransaction()

			Else

				lRet := .T.

				msgPrint( "PEDIDO: " + cNroPed + " FOI GERADO COM SUCESSO. PEDIDO WEB: " + cPedB2W )

				aAdd( aPed, { cNroPed, cCodCli, cLojCli } )

				U_HFTGER002( aPed, .T., .F. )

				msgPrint( "DAP GERADO PARA O PEDIDO: " + cNroPed )

				ConfirmSX8()

			EndIf

		END TRANSACTION

	END SEQUENCE

	// Deleta o pedido da fila
	If ldelQueue
		delQueue( cUrl, aHeader )
	Endif

	SalvaLog( "PEDIDO: " + cNroPed + " FOI GERADO COM SUCESSO. PEDIDO WEB: " + cPedB2W, "", .F. )

	msgPrint( "FIM DO PROCESSAMENTO... PEDIDO WEB: " + cPedB2W )

Return( lRet )

/*/
------------------------------------------------------------------
{Protheus.doc} SalvaLog
Rotina de Salvar o Log

@author Hope Lingerie
@since
@version
------------------------------------------------------------------
/*/
Static Function SalvaLog( cCompl, cMsg, lErro )

	If !Empty( cMsg )
		msgPrint( cMsg )
	EndIf

	If lErro
		MemoWrite( "\log\API\B2W\Erro\" + cDtTime + "_" + cCompl + "_" + cPedB2W + ".log", cLog )
	Else
		MemoWrite( "\log\API\B2W\" + cDtTime + "_" + cCompl + "_" + cPedB2W + ".log", cLog )
	EndIf

Return

/*/
---------------------------------------------------------------------------
{Protheus.doc} HB2WCli
Fonte utilizado para integrar Cliente B2W X HOPE

@author    Robson Melo
@version   1.00
@since     Abr/2020
-----------------------------------------------------------------------------
/*/
Static Function HB2WCli( aCliB2W, cCodCli, cLojCli )
 
	Local lRet        	:= .T.

	Local aArea		  	:= GetArea()
	Local aAreaSA1    	:= SA1->(GetArea())
	Local aAreaCC2    	:= CC2->(GetArea())
	Local aSA1Auto    	:= {}
	Local nOpcAuto    	:= 3 
	Local cXcanal		:= ''
	Local cXcanalD    	:= ''
	Local cXcodreg		:= ''
	Local cXdesreg    	:= ''
	Local cXmicrre		:= ''
	Local cXmicrde    	:= ''
	Local cConta      	:= ''
	Local cMun        	:= ''
	Local cCodMun     	:= ''
	Local cCodEst     	:= ''
	Local cEst        	:= ''
	Local cCgc        	:= ''
	Local cPathCli    	:= '\log\API\B2W\Erro\Cliente\'
	Local lRetMun     	:= .F.
	Local cError	  	:= ""
	Local cNaturez		:= ""

	Private cTime     	:= Time()
	Private cDtTime   	:= DTOS(dDataBase) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
	Private cArqCliN  	:= 'Erro_Cliente_B2W_'
	Private lMsErroAuto := .F.

	cCodCli := U_HFATG002() //Busca pr�ximo C�digo Disponivel 
	cLojCli := "0001"

	cCgc     := Alltrim(aCliB2W[nPCNPJ])
	cPessoa  := Iif(Len(Alltrim(StrTran(aCliB2W[nPCNPJ],'.',''))) < 14 ,"F","J")
	cTipoCli := Alltrim( GetNewPar( "B2W_TPCLI", "F" ) )
	
	cXcanal  := Alltrim( GetNewPar( "B2W_CANAL" , "007" ) ) 
	cXcanalD := Posicione("ZA3",1,xFilial("ZA3")+cXcanal,"ZA3_DESCRI")
	
	cXcodreg := Alltrim( GetNewPar( "B2W_CODREG" , "11045001" ) )
	If !Empty( cXcodreg )
		cXdesreg := Posicione("SZ8",1,xFilial("SZ8")+cXcodreg,"Z8_DESC")
	Endif 

	cXmicrre := Alltrim( GetNewPar( "B2W_MICREG", "11045" ) )
	If !Empty( cXmicrre )
		cXmicrde := Posicione("ZA4",1,xFilial("ZA4")+cXmicrre,"ZA4_DESCMA")
	Endif 
	
	cConta   := Posicione("ZA3",1,xFilial("ZA3")+cXcanal,"ZA3_CONTA")
	cNaturez := Posicione("ZA3",1,xFilial("ZA3")+cXcanal,"ZA3_NATURE")
	cEndereco:= Upper( Alltrim(aCliB2W[nPEnder]) +', '+ Alltrim(aCliB2W[nPNumero]) )
	cMun     := Upper( Alltrim( aCliB2W[nPCidade] ) )
	cCodEst  := Alltrim(aCliB2W[nPUF])
	cCodMun  := Alltrim(Posicione("CC2",4,xFilial("CC2")+cCodEst+cMun,"CC2_CODMUN"))
	cEst     := Upper( Alltrim(Posicione("SX5",1,xFilial("SX5")+'12'+cCodEst,"X5_DESCRI")) )

	//Inserido para corrigir o problema de posicionamento do execauto
	dbSelectArea("CC2")
	CC2->(dbSetOrder(1))
	If CC2->(dbSeek(xfilial("CC2")+cCodEst+cCodMun))
		lRetMun:= .T.
	Endif

	//Altera dados de entrega caso o mesmo exista
	dbSelectArea("SA1")
	dbSetOrder(3)//A1_FILIAL+A1_CGC
	If SA1->(dbSeek(xfilial("SA1")+cCgc))
		cCodCli := SA1->A1_COD
		cLojCli := SA1->A1_LOJA
		RecLock("SA1",.F.)
		SA1->A1_ESTE    := 	 cCodEst                       
		SA1->A1_MUNE    :=	 cMun
		SA1->A1_CODPAIS :=	 "01058"                        
		SA1->A1_ENDENT  :=   Upper( Rtrim( cEndereco ) )
		SA1->A1_CEPE    :=   aCliB2W[nPCEP]                 
		SA1->A1_BAIRROE :=	 Upper( Rtrim( aCliB2W[nPBairro] ) )
		SA1->(MsUnlock())
		msgPrint( "O Endereco de Entrega do Cliente: " + cCodCli + "/" + cLojCli + " foi alterado com sucesso!" )    
		Return .T.
	Endif

	// Atualiza arquivo de log com o CGC do cliente
	cArqCliN := ( cArqCliN + aCliB2W[nPCNPJ] + '.log.' )

	//----------------------------------
	// Dados do Cliente
	//----------------------------------
	aAdd(aSA1Auto,{"A1_FILIAL" 	 , xFilial("SA1")                 			, Nil})	
	aAdd(aSA1Auto,{"A1_COD" 	 , cCodCli                       			, Nil})	
	aAdd(aSA1Auto,{"A1_LOJA" 	 , cLojCli                       			, Nil})
	aAdd(aSA1Auto,{"A1_TIPO"     , Upper( cTipoCli )                       	, Nil})	 
	aAdd(aSA1Auto,{"A1_PESSOA" 	 , Upper( cPessoa )                       	, Nil})	
	aAdd(aSA1Auto,{"A1_NOME" 	 , Upper( aCliB2W[nPNome] )					, Nil})
	aAdd(aSA1Auto,{"A1_NREDUZ" 	 , Upper( SubStr(aCliB2W[nPNome],1,20) ) 	, Nil})
	aAdd(aSA1Auto,{"A1_CGC" 	 , aCliB2W[nPCNPJ]                			, Nil})
	aAdd(aSA1Auto,{"A1_INSCR"    , "ISENTO"                       			, Nil})  
	aAdd(aSA1Auto,{"A1_END"      , Upper( cEndereco )                    	, Nil})
	aAdd(aSA1Auto,{"A1_COMPLEM"  , Upper( Alltrim(aCliB2W[nPCompl]) )     	, Nil})
	aAdd(aSA1Auto,{"A1_BAIRRO"	 , Upper( aCliB2W[nPBairro] )            	, Nil})
	aAdd(aSA1Auto,{"A1_EST"	   	 , Upper( cCodEst )                       	, Nil})
	aAdd(aSA1Auto,{"A1_CEP"		 , aCliB2W[nPCEP]                 			, Nil})

	Do Case
		Case cCodEst $ "ES/MG/RJ/SP" 
			aAdd(aSA1Auto, {"A1_REGIAO" , "007"                   , Nil})
		Case cCodEst $ "PR/RS/SC" 
			aAdd(aSA1Auto, {"A1_REGIAO" , "002"                   , Nil})
		Case cCodEst $ "AC/AM/AP/PA/RO/RR/TO" 
			aAdd(aSA1Auto, {"A1_REGIAO" , "001"                   , Nil})
		Case cCodEst $ "AL/BA/CE/MA/PB/PE/PI/RN/SE" 
			aAdd(aSA1Auto, {"A1_REGIAO" , "008"                   , Nil})
		Case cCodEst $ "DF/GO/MS/MT" 
			aAdd(aSA1Auto, {"A1_REGIAO" , "006"                   , Nil})
	EndCase

	aAdd(aSA1Auto,{"A1_ESTADO"	 , Upper( cEst )                           	, Nil})
	aAdd(aSA1Auto,{"A1_COD_MUN"	 , cCodMun                        , Nil})
	aAdd(aSA1Auto,{"A1_MUN"	     , cMun                           , Nil})
	aAdd(aSA1Auto,{"A1_NATUREZ"	 , cNaturez                       , Nil})
	aAdd(aSA1Auto,{"A1_ENDCOB"   , cEndereco                      , Nil})
	aAdd(aSA1Auto,{"A1_CEPC"     , aCliB2W[nPCEP]                      , Nil})
	aAdd(aSA1Auto,{"A1_MUNC"	 , cMun                           , Nil})
	aAdd(aSA1Auto,{"A1_ENDREC"   , cEndereco                      , Nil})
	aAdd(aSA1Auto,{"A1_ENDENT"   , cEndereco                      , Nil})
	aAdd(aSA1Auto,{"A1_PAIS"	 , "105"                          , Nil})//105 - BRASIL - SYA -  aCliB2W[nPPais] 
	aAdd(aSA1Auto,{"A1_TEL"      , aCliB2W[nPTelef]               , Nil})
	aAdd(aSA1Auto,{"A1_PAISDES"  , "BRASIL"                       , Nil})
	aAdd(aSA1Auto,{"A1_EMAIL"	 , aCliB2W[nPEMail]               , Nil})
	aAdd(aSA1Auto,{"A1_CONTA"	 , cConta                         , Nil})
	aAdd(aSA1Auto,{"A1_CONTRIB"	 , "2"                            , Nil})
	aadd(aSA1Auto,{"A1_XCANAL"   , cXcanal                        , Nil})
	aadd(aSA1Auto,{"A1_XCANALD"  , cXcanalD                       , Nil})
	aAdd(aSA1Auto,{"A1_ESTE"	 , cCodEst                        , Nil})
	aAdd(aSA1Auto,{"A1_MUNE"	 , cMun                           , Nil})
	aAdd(aSA1Auto,{"A1_CODPAIS"	 , "01058"                        , Nil})//01058 - BRASIL - SYA -  aCliB2W[nPPais] 
	aAdd(aSA1Auto,{"A1_ENDENT"   , cEndereco                      , Nil})
	aAdd(aSA1Auto,{"A1_CEPE"     , aCliB2W[nPCEP]                 , Nil})
	aAdd(aSA1Auto,{"A1_BAIRROE"	 , Upper( aCliB2W[nPBairro] )     , Nil})
	aAdd(aSA1Auto,{"A1_XEMP"     , "01"                           , Nil})
	aAdd(aSA1Auto,{"A1_TELE"     , aCliB2W[nPTelef]               , Nil})
	aadd(aSA1Auto,{"A1_XCODREG"  , cXcodreg                       , Nil})//Regi�o - SZ8
	aadd(aSA1Auto,{"A1_XDESREG"  , cXdesreg                       , Nil})//Descri��o Regi�o
	aadd(aSA1Auto,{"A1_XMICRRE"  , cXmicrre                       , Nil})//Macro Regi�o - ZA4
	aadd(aSA1Auto,{"A1_XMICRDE"  , cXmicrde                       , Nil})//Descri��o Macro Regi�o

	//------------------------------------
	// Chamada para cadastrar o cliente
	//------------------------------------
	MSExecAuto( { |a,b,c| MATA030(a,b,c) }, aSA1Auto, nOpcAuto )

	If lMsErroAuto 
		lRet 	:= .F.
		cError	:= MostraErro( cPathCli, cArqCliN )
		AADD( aErrorCli, cError )
		AADD( aErrorCli, "Cliente: " + Upper( aCliB2W[nPNome] ) )
		AADD( aErrorCli, "CNPJ" + aCliB2W[nPCNPJ] )
		AADD( aErrorCli, "Municipio: " + cMun )
		AADD( aErrorCli, "Cod. Municipio: " + cCodMun )

		msgPrint( "Cliente: " + cCodCli + "/" + cLojCli + " n�o " + IIf( nOpcAuto == 3, "incluido", "alterado") + ", verifique o Log." )
	Else 
		msgPrint( "O Cliente: " + cCodCli + "/" + cLojCli + " foi " + IIf( nOpcAuto == 3, "incluido", "alterado" ) + " com sucesso!" )
	EndIf
	
	RestArea( aAreaSA1 )
	RestArea( aAreaCC2 )
	RestArea( aArea )

Return( lRet )

/*/
------------------------------------------------------------------
{Protheus.doc} HFATSALDO
Busca saldo na SB2

@author Robson Melo
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function HFatSaldo( cProduto, cArmazem )
	
	Local nRet := 0

	dbSelectArea("SB2")
	dbSetOrder(1)
	If dbSeek( xFilial("SB2") + cProduto + cArmazem )
		nRet := ( SB2->B2_QATU - SB2->B2_RESERVA - SB2->B2_QEMP - SB2->B2_XRESERV )
	EndIf

Return( nRet )

/*/
------------------------------------------------------------------
{Protheus.doc} getBody
Retorna o Texto do Body do Email

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function getBody( aErrorLog )

	Local cHtml		:= ""
	Local nX		:= 0
	Local cDtHrFim	:= DToC( Date() ) + " " + Time()

	cHtml := '<!DOCTYPE HTML>'
	cHtml += '<html lang="pt-br">'
	cHtml += '<head>'
	cHtml += '<meta charset="utf-8">'
	cHtml += '</head>'
	cHtml += '<body>'

	cHtml += '<h3>Log de Processamento:</h3>'
	cHtml += '<ul>'
	For nX := 1 To Len( aErrorLog )
		cHtml += '<li>' + aErrorLog[nX] + '</li>'
	Next
	cHtml += '</ul>'

	cHtml += '<h3>Outras Informa��es:</h3>'
	cHtml += '<b>Fonte:</b> HJOBB2W'
	cHtml += '<br><b>Data/Hora In�cio:</b> ' + cDtHrIni
	cHtml += '<br><b>Data/Hora Final:</b> ' + cDtHrFim

	cHtml += '</body>'
	cHtml += '</html>'

Return( cHtml )

/*/
------------------------------------------------------------------
{Protheus.doc} msgPrint
Printa mensagem no console.log

@author Sergio S. Fuzinaka
@since Abr/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function msgPrint( cMsg )

	cLog += ( CRLF + Dtoc( Date() ) + ": B2W - " + cMsg )

Return( ConOut( "[B2W][" + Dtoc( Date() ) + "-" + Time() + "][" + cMsg + "]" ) )

/*/
------------------------------------------------------------------
{Protheus.doc} HCharEsp
Fun��o utilizada para remover caracteres especiais

@author Sergio S. Fuzinaka
@since Jun/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function HCharEsp( cTexto )

	Local nI		:= 0
	Local aCarac 	:= {}

	Aadd(aCarac,{"á","A"})
	Aadd(aCarac,{"é","E"})
	Aadd(aCarac,{"�","A"})
	Aadd(aCarac,{"�","A"})
	Aadd(aCarac,{"�","A"})
	Aadd(aCarac,{"�","A"})
	Aadd(aCarac,{"�","a"})
	Aadd(aCarac,{"�","a"})
	Aadd(aCarac,{"�","a"})
	Aadd(aCarac,{"�","a"})
	Aadd(aCarac,{"�","E"})
	Aadd(aCarac,{"�","E"})
	Aadd(aCarac,{"�","e"})
	Aadd(aCarac,{"�","e"})
	Aadd(aCarac,{"�","I"})
	Aadd(aCarac,{"�","i"})
	Aadd(aCarac,{"�","O"})
	Aadd(aCarac,{"�","O"})
	Aadd(aCarac,{"�","O"})
	Aadd(aCarac,{"�","o"})
	Aadd(aCarac,{"�","o"})
	Aadd(aCarac,{"�","o"})
	Aadd(aCarac,{"�","U"})
	Aadd(aCarac,{"�","u"})
	Aadd(aCarac,{"�","C"})
	Aadd(aCarac,{"�","c"})

	// Ignora caracteres Extendidos da tabela ASCII
	For nI := 128 To 255
		Aadd(aCarac,{Chr(nI),""})  // Tab
	Next nI

	For nI := 1 To Len( aCarac )
		If aCarac[nI, 1] $ cTexto
			cTexto := StrTran(cTexto, aCarac[nI,1], aCarac[nI,2])
		EndIf
	Next nI

Return( cTexto )
