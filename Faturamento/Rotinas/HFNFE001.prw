#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

user function HFNFE001(l1Elem,lTipoRet)

Local MvPar
Local aArea  := GetArea()
Local cTitulo := ""                     
Local lRet  := .T.
Local aBox  := {}                        
Local nTam  := 2
Local MvParDef := ""

MvPar:=&(Alltrim(ReadVar()))   // Carrega Nome da Variavel do Get em Questao
mvRet:=Alltrim(ReadVar())    // Iguala Nome da Variavel ao Nome variavel de Retorno

dbSelectArea("ZA3")
DbSetOrder(1)
DbGoTop()
cTitulo := "Regionais"
While !Eof()
   Aadd(aBox,ZA3->ZA3_CODIGO + " - " + Alltrim(ZA3->ZA3_DESCRI))
   MvParDef += SubStr(ZA3->ZA3_CODIGO,2,2)
   dbSkip()
EndDo

If f_Opcoes( @MvPar,;  // uVarRet
      cTitulo,;  // cTitulo
      @aBox,;   // aOpcoes
      MvParDef,;  // cOpcoes
      ,;    // nLin1
      ,;    // nCol1
      ,;    // l1Elem
      nTam,;    // nTam
      Len( aBox ),; // nElemRet
      ,;    // lMultSelect
      ,;    // lComboBox
      ,;    // cCampo
      ,;    // lNotOrdena
      ,;    // NotPesq
      ,;   // ForceRetArr
       )    // F3
       
  	&MvRet := mvpar                                                                          // Devolve Resultado
EndIF

RestArea(aArea)

Return lRet