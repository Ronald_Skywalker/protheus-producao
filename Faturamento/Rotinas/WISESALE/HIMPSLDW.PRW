#INCLUDE "RWMAKE.CH"
#INCLUDE "TBICODE.CH"
#INCLUDE "FONT.CH"

// Retornos poss�veis do MessageBox
#Define IDOK			    1
#Define IDCANCEL		    2
#Define IDYES			    6
#Define IDNO			    7
#Define CRLF  CHR(13)+CHR(10)


/*/{Protheus.doc} HIMPSLDW
Importador de saldo na SB5 e disponibilizar na Wise
@author Geyson Albano
@since 06/10/2021
@version 1.0
@return Nil, Fun��o n�o tem retorno
@example
U_HIMPSLDW()
@obs
/*/

User Function HIMPSLDW()

	Local aPergs    := {}
	Private cArq       := ""
	Private cArqMacro  := "XLS2DBF.XLA"
	Private cTemp      := GetTempPath() //pega caminho do temp do client
	Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema
	Private aResps     := {}
	Private aArquivos  := {}
	Private nRet       := 0
	Private aTitle     := {}
	Private aLog     := {}

	Aadd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.T.,""})

	If ParamBox(aPergs,"Parametros", @aResps)

		Aadd(aArquivos, AllTrim(aResps[1]))

		cArq := AllTrim(aResps[1])

		Processa({|| ProcMov()})
	EndIF

Return

Static Function ProcMov()

	Local nFile		:= 0
	Local cFile		:= AllTrim(aResps[1])
	Local cLinha  	:= ""
	Local nLintit	:= 1
	Local nLin 		:= 0
	Local aDados  	:= {}
	Local nHandle 	:= 0


	nFile := fOpen(cFile,0,0)

	If nFile == -1
		If !Empty(cFile)
			MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")
		EndIf
		Return
	EndIf

	nHandle := Ft_Fuse(cFile)
	Ft_FGoTop()
	nLinTot := FT_FLastRec()-1
	ProcRegua(nLinTot)

	While nLinTit > 0 .AND. !Ft_FEof()
		Ft_FSkip()
		nLinTit--
	EndDo

	Do While !Ft_FEof()
		IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))
		nLin++
		cLinha := Ft_FReadLn()

		If Empty(AllTrim(StrTran(cLinha,',','')))
			Ft_FSkip()
			Loop
		EndIf

		cLinha := StrTran(cLinha,'"',"'")
		cLinha := '{"'+cLinha+','+alltrim(str(nLin+1))+'"}'
		cLinha := StrTran(cLinha,';',',')
		cLinha := StrTran(cLinha,',','","')

		Aadd(aDados, &cLinha)
		FT_FSkip()
	EndDo

	FT_FUse()

	ProcRegua(0)
	nRet:= MessageBox("Deseja realmente importar o arquivo:" +Upper(cFile)+ " ?","Confirma��o",4)

	If nRet == 6
		U_xSaldoB5(aDados)
	Else
		MessageBox("Cancelado pelo usu�rio !","Cancelado",16)
	EndIf

Return

//ROTINA DE INCLUSAO AUTOMATICA DE PEDIDOS
User Function xSaldoB5(pMatriz)

//pMatriz --> matriz contendo os dados do arq csv

	Local nXT           := 0
	Local lInc          := .F.

	Private pMatrizped  := pMatriz
	Private lMsErroAuto := .F.
	Private cProd       := ""
	Private nQuant      := ""
	
	For nXT:=1 to len(pMatrizped)

		cProd      := pMatrizped[nXT][1]
		nQuant     := pMatrizped[nXT][2]

		DbSelectArea("SB5")
		DbSetOrder(1)
		If !DbSeek(xFilial("SB5")+Alltrim(cProd))

			DbSelectArea("SB1")
			DbSetOrder(1)
			If DbSeek(xFilial("SB1")+Alltrim(cProd))

				RecLock("SB5",.T.)
					SB5->B5_COD := SB1->B1_COD
					SB5->B5_CEME := SB1->B1_DESC
					SB5->B5_XSALDOW := VAL(nQuant)
				MsUnlock()

                lInc := .T.
                cProd := ""
                nQuant := 0

			Else
				MessageBox("O produto " +cProd+ " n�o est� cadastrado, favor verificar","TOTVS",16)
				Return .F.
				
			EndIF
		Else

			RecLock("SB5",.F.)
				SB5->B5_XSALDOW := VAL(nQuant)
			MsUnlock()

            lInc := .T.
            nQuant := 0

		EndIf

	Next nXT

	If lInc 
		MessageBox("Saldos inclu�dos com sucesso !!! ","HFALTPED",0)
	Endif

Return (.T.)
