#Include "PROTHEUS.CH"
#include "TbiConn.ch"
#include "TOTVS.ch"
#include "topconn.ch"

/*
Programa  HATUSB5W  Autor  Geyson Albano         Data   06/10/21
?
Desc.     Faz o ajuste do saldo na B5 de acordo com  a operao.    
                                                                      
?
Uso       HOPE                                                        
*/
User Function HATUSB5W(cPed,nopc)

	DbSelectArea("SC6")
	DbSetOrder(1) 
	DbSeek(xFilial("SC6")+cPed)

	While SC6->(!EOF()) .And. SC6->C6_NUM = cPed

		DbSelectArea("SB5")
		DbSetOrder(1) 
		If DbSeek(xFilial("SB5")+SC6->C6_PRODUTO)
			RecLock("SB5",.F.)
				If nopc == 1
				SB5->B5_XSALDOW := IIf(SB5->B5_XSALDOW-SC6->C6_QTDVEN <= 0,0,SB5->B5_XSALDOW-SC6->C6_QTDVEN) // DIMINUINDO SALDO ATUAL COM QUANTIADE INCLDA DO PEDIDO
				Else
				SB5->B5_XSALDOW := SB5->B5_XSALDOW+SC6->C6_QTDVEN // SOMANDO SALDO ATUAL COM QUANTIADE EXCLUiDA DO PEDIDO
				EndIf
			MsUnlock()
		EndIf
    	SC6->(DbSkip())

	EndDo
    
Return
