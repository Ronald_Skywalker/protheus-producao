#Include 'Protheus.ch'
#include "topconn.ch"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} HFATA012
Faturamento em JOB
@author Weskley Silva
@since 25/09/2018
@version P12
@obs ZZZ_STATUS
P-Pendente Faturamento
Q-Pendente Cancelamento
F-Faturado
C-Cancelado
EF-Erro Faturamento
EC-Erro Cancelamento
/*/

User Function HFATA012()

	Local cQuery		:= ""
	Local lErro			:= .F.
	Local oError
	Local cMsgErro
	Local cDHIni   	    := Time()

	Private cnotapri   	:= ""
	Private cseriepri   := ""
	Private cSASStaErr	:= "E"
	Private lFim		:= .F.
	Private ncount 		:= 0

	Sleep(2000)
	If !LockByName("HFATA012-91",.F.,.F.)
		ConOut("Processso ja inicializado!")
		Return
	EndIf
	RpcSetEnv("01","0101",,,"FAT","U_HFATA012",{})
	cQuery	:= " SELECT TOP 1 R_E_C_N_O_ AS REGZZZ FROM "+RETSQLNAME("ZZZ")+" ZZZ WHERE ZZZ_STATUS IN ('P') AND ZZZ_FILIAL = '0101' AND ZZZ_SERIE = '' AND ZZZ_NOTA = '' AND D_E_L_E_T_ = '' ORDER BY ZZZ_PRIORI,R_E_C_N_O_ "
	// Seta que RecLock, em caso de falha, nao vai fazer retry automatico
	ConOut("Executou Select!")
	SetLoopLock(.F.)
	While !killapp()
		//Monitor("Faturamento|Aguardando tarefas...")
		TCQuery cQuery new alias TMPZZZ
		If TMPZZZ->(EOF())
			TMPZZZ->(DbCloseArea())
			ConOut("N�o existem pedidos na fila! ")
			return
			Sleep(10000)
			Loop
		EndIF
		lErro		:= .F.
		cMsgErro	:= ""
		ZZZ->(DbGoTo(TMPZZZ->REGZZZ))
		RecLock("ZZZ",.F.)
		oError := ErrorBlock({|e| lErro := .T.,ErroLog(e,cSASStaErr) } )
		//dDatabase := date()
		If ALLTRIM(ZZZ->ZZZ_STATUS)=="P"	//Pendente Faturamento
			ConOut("Entrou na funcao Faturar!")
			cDHIni := Time()
			Faturar(@lErro,@cMsgErro)
			ConOut("HFAT012 - Inicio: " + cDHIni + " - Fim: " + Time() )
			ConOut("Saiu da funcao Faturar!")
		EndIf
		Logs(Replicate("=",30))
		ZZZ->(MsUnLock())
		ErrorBlock(oError)
		TMPZZZ->(DbCloseArea())
		RETURN
	EndDo

Return

Static Function Faturar(lErro,cMsgErro)
//	Local cCodUsr	:= "000000"
	Local aDocFat	:= {}
	cSASStaErr		:= "E"

	//Monitor("Faturamento:Processando Pedido :"+ZZZ->ZZZ_PEDIDO+" do Cliente :"+ZZZ->ZZZ_CLIENT)	//Atualiza mensagem no monitor
	Logs("Pedido:"+ZZZ->ZZZ_PEDIDO)
	Logs("Processando Pedido:"+ZZZ->ZZZ_PEDIDO)
	Begin Sequence
		If !lErro .AND. Empty(ZZZ->ZZZ_NOTA) .AND. !Empty(ZZZ->ZZZ_PEDIDO)
			Monitor("Faturamento:Processando Pedido:"+ZZZ->ZZZ_PEDIDO+" do Cliente:"+ZZZ->ZZZ_CLIENT)	//Atualiza mensagem no monitor
			Logs("Faturamento:Pedido:"+ZZZ->ZZZ_PEDIDO+" do Cliente:"+ZZZ->ZZZ_CLIENT)
			Begin Transaction
				ConOut("Entrou na funcao FGeraFat!" + ZZZ_PEDIDO )
				If FGeraFat(ZZZ->ZZZ_FILIAL,ZZZ->ZZZ_PEDIDO,@aDocFat,@cMsgErro)
					While !lFim .AND. ncount < 10
						ncount++
						ConOut("Faturando notas complementares ! " + cvaltochar(ncount) )
						FGeraFat(ZZZ->ZZZ_FILIAL,ZZZ->ZZZ_PEDIDO,@aDocFat,@cMsgErro)
					EndDo
					ConOut("Saiu da funcao FGeraFat!")
					RecLock("ZZZ",.F.)
					ZZZ->ZZZ_NOTA	:= cnotapri
					ZZZ->ZZZ_SERIE	:= cseriepri
					ZZZ->ZZZ_LOG	:= ""
					ZZZ->ZZZ_STATUS := "F"
					MsUnLock()


					IF EMPTY(ZZZ->ZZZ_NUMPF)
						DbSelectarea("ZZ9")
						DbSetOrder(1)
						DbSeek(xFilial("ZZ9")+ZZZ->ZZZ_PEDIDO+ZZZ->ZZZ_CLIENT+ZZZ->ZZZ_LOJA)
						RecLock("ZZ9",.F.)
						ZZ9->ZZ9_STATUS := 'F'
						ZZ9->ZZ9_NOTA := cnotapri
						MSUNLOCK()

					ELSE

						DbSelectarea("ZZ9")
						DbSetOrder(2)
						DbSeek(xFilial("ZZ9")+ZZZ->ZZZ_NUMPF+ZZZ->ZZZ_CLIENT+ZZZ->ZZZ_LOJA)
						RecLock("ZZ9",.F.)
						ZZ9->ZZ9_STATUS := 'F'
						ZZ9->ZZ9_NOTA := cnotapri
						MSUNLOCK()

					ENDIF

				Else

					IF EMPTY(ZZZ->ZZZ_NUMPF)
						DbSelectarea("ZZ9")
						DbSetOrder(1)
						DbSeek(xFilial("ZZ9")+ZZZ->ZZZ_PEDIDO+ZZZ->ZZZ_CLIENT+ZZZ->ZZZ_LOJA)
						RecLock("ZZ9",.F.)
						ZZ9->ZZ9_STATUS := 'E'
						MSUNLOCK()

					ELSE

						DbSelectarea("ZZ9")
						DbSetOrder(2)
						DbSeek(xFilial("ZZ9")+ZZZ->ZZZ_NUMPF+ZZZ->ZZZ_CLIENT+ZZZ->ZZZ_LOJA)
						RecLock("ZZ9",.F.)
						ZZ9->ZZ9_STATUS := 'E'

						MSUNLOCK()
					ENDIF

					DisarmTransaction()
					Logs("Pedido:"+cMsgErro,.T.)
					lErro		:= .T.

					IF lErro
						RecLock("ZZZ",.F.)
						ZZZ_STATUS := 'E'
						MsUnLock()
					ENDIF

					BREAK
				EndIF
			End Transaction
		EndIf

	End Sequence
	//MsUnLockAll()
	//GETMV("MV_NUMITEN",.T.)
	//SX6->(MsRUnLock())
	If lErro
		RecLock("ZZZ",.F.)
		ZZZ_STATUS := cSASStaErr
		MsUnLock()
	EndIf
Return


Static Function ErroLog(oErro,cStatus)
	DisarmTransaction()
	Logs(oErro:Description+CRLF+oErro:ErrorStack+CRLF+oErro:ErrorEnv,.T.)
	RecLock("ZZZ",.F.)
	ZZZ_STATUS=cStatus	//Altera o status para erro
	MsUnLock()
	StartJob("U_HFATA012",getenvserver(),.F.)	//Inicia um nova thread
	__Quit()									//Finaliza a thread atual
Return


/*/{Protheus.doc} FGeraFat
Fatura o Pedido de venda
@author Weskley Silva
@since 25/09/2018
@version 1.0
@param cEmpFat, String, Filial onde esta o pedido de venda para faturar
@param cPedFat, String, Numero do pedido de venda para faturar
@param aDocFat, Array, Array que recebe por referencia para gravar o Numero da NF e Serie
@param cMsgErro, String, String que recebe por referencia para gravar a msg de erro
/*/
Static Function FGeraFat(cEmpFat,cPedFat,aDocFat,cMsgErro)
	Local aArea			:= GetArea()
	Local aPvlNfs		:= {}
	Local cSerie		:= "2"
	Local cFilBkp		:= cFilAnt
	Local cNFSaida		:= ""
	Local lRet			:= .T.
	Local nVezes
	Local lMax 			:= .F.
	//Local nItemNf   	:= GETMV("MV_NUMITEN")
	Local  nItemNf   := a460NumIt(cSerie)
	Default aDocFat		:= {}
	Default cMsgErro	:= ""
	cFilAnt := cEmpFat

	Private _txPadnfc  := ""
	Private cNumPF := ""
	//ProcRegua(4)
	Logs("Gerando Nota Fiscal de Sa�da")
	ProcessMessage()

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1))
	SC5->(dbSeek(cEmpFat+cPedFat))

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	aTesdig = GETAREA()

	dbSelectArea("SC6")
	SC6->(dbSetOrder(1))
	SC6->(dbSeek(cEmpFat+cPedFat))

	//cTesdig = SC6->C6_TES


	RESTAREA(aTesdig)

	dbSelectArea("SC9")
	SC9->(dbSetOrder(1))

	If SC9->(DbSeek(cEmpFat+cPedFat))
		While SC9->(!Eof()) .AND. SC9->C9_FILIAL == cEmpFat .AND. SC9->C9_PEDIDO == cPedFat .AND. !lMax
			cProd = SC9->C9_PRODUTO
			cQuant = CVALTOCHAR(SC9->C9_QTDLIB)
			if EMPTY(SC9->C9_NFISCAL)
				If !Empty(SC9->C9_BLEST) .and. !Empty(SC9->C9_BLCRED)
					cSASStaErr	:= "BE"
					cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de estoque/Credito "
					Return .F.
				ELSEIF !Empty(SC9->C9_BLEST)
					cSASStaErr	:= "BE"
					cMsgErro	:= cEmpFat +"|"+cPedFat +" | "+ cProd +" | "+ cQuant +" | Pedido com Bloqueio | Bloqueio de estoque "
					Return .F.
				ELSEIF  !Empty(SC9->C9_BLCRED)
					cSASStaErr	:= "BE"
					cMsgErro	:= cEmpFat +"|"+cPedFat +"| Pedido com Bloqueio | Bloqueio de Credito "
					Return .F.
				ENDIF

				SC6->(DbSeek(xFilial("SC6")+SC9->C9_PEDIDO+SC9->C9_ITEM+SC9->C9_PRODUTO))

				SE4->(DbSeek(xFilial("SE4")+SC5->C5_CONDPAG))
				SB1->(DbSeek(xFilial("SB1")+SC9->C9_PRODUTO))
				SB2->(DbSeek(xFilial("SB2")+SC9->C9_PRODUTO+SC9->C9_LOCAL))
				SF4->(DbSeek(xFilial("SF4")+SC6->C6_TES))
				If Alltrim(SC9->C9_NFISCAL) = '' .AND. Len(aPvlNfs) <= nItemNf
					aAdd(aPvlNfs,{;
						SC9->C9_PEDIDO,;
						SC9->C9_ITEM,;
						SC9->C9_SEQUEN,;
						SC9->C9_QTDLIB,;
						SC9->C9_PRCVEN,;
						SC9->C9_PRODUTO,;
						.F.,;
						SC9->(RECNO()),;
						SC5->(RECNO()),;
						SC6->(RECNO()),;
						SE4->(RECNO()),;
						SB1->(RECNO()),;
						SB2->(RECNO()),;
						SF4->(RECNO());
						})

						If Empty(cNumPF)
							cNumPF := SC9->C9_XNUMPF
						EndIf
					If Len(aPvlNfs) == nItemNf
						lMax := .T.	 // VARIAVEL QUE INDICA QUANDO CHEGOU NO MAXIMO DE IENS POR NOTA
					EndIf
				EndIf
			ENDIF
			SC9->(DbSkip())
		EndDo

		If ( Len(aPvlNfs) == nItemNf ) //NOTAS COM QTD DE ITENS M�XIMO
			If SC9->(!Eof()) .AND. SC9->C9_FILIAL == cEmpFat .AND. SC9->C9_PEDIDO == cPedFat
				lFim :=  .F.
			Else
				lFim :=  .T.
			EndIf

			dbSelectArea("SX5")
			dbSetOrder(1)

			If SX5->(MsSeek(cEmpFat+"01"+cSerie))
				nVezes := 0
				While ( !SX5->(MsRLock()) )
					nVezes ++
					Logs("Aguardando libera��o de numera��o da Nota Fiscal de Sa�da ... "+cValtochar(nVezes))
					If ( nVezes > 5 )
						Exit
					EndIf
					Sleep(1000)
				EndDo
			Endif

			Pergunte("MT460A",.F.)
			cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F.,.F.,,,,)
			If Empty(cnotapri)
				cnotapri  := cNFSaida
				cseriepri := cSerie
			EndIf
			Atumsgnf() // FUNCAO PARA ATUALIZAR MENSAGEM NF

			dbSelectArea("SX5")
			If !SX5->(MsRLock()) .and. SX5->(!EOF())
				SX5->(MsUnlock())
			Endif

		ElseIf ( Len(aPvlNfs) < nItemNf )  // NOTAS COM MENOS ITENS QUE A QUANTIDADE LIMITE

			If SC9->(!Eof()) .AND. SC9->C9_FILIAL == cEmpFat .AND. SC9->C9_PEDIDO == cPedFat
				lFim :=  .F.
			Else
				lFim :=  .T.
			EndIf

			dbSelectArea("SX5")
			dbSetOrder(1)

			If SX5->(MsSeek(cEmpFat+"01"+cSerie))
				nVezes := 0
				While ( !SX5->(MsRLock()) )
					nVezes ++
					Logs("Aguardando libera��o de numera��o da Nota Fiscal de Sa�da ... "+cValtochar(nVezes))
					If ( nVezes > 5 )
						Exit
					EndIf
					Sleep(1000)
				EndDo
			Endif

			Pergunte("MT460A",.F.)
			cNFSaida := MaPvlNfs(aPvlNfs, cSerie, .F. , .F. , .F. , .F. , .F., 0, 0, .F.,.F.,,,,)
			If Empty(cnotapri)
				cnotapri  := cNFSaida
				cseriepri := cSerie
			EndIf
			Atumsgnf() // FUNCAO PARA ATUALIZAR MENSAGEM NF

			dbSelectArea("SX5")
			If !SX5->(MsRLock()) .and. SX5->(!EOF())
				SX5->(MsUnlock())
			Endif

		EndIf

		If Empty(cNFSaida)
			cMsgErro := "N�o foi poss�vel gerar a Nota fiscal de Sa�da da Filial "+cFilAnt+ CHR(10) + CHR(13)
		Else
			aDocFat	:= {cNFSaida,cSerie}
			Logs("Nota Fiscal de Sa�da gerada")
			Monitor("Nota Fiscal de Sa�da gerada")
		Endif

		dbSelectArea("SX5")
		If !SX5->(MsRLock()) .and. SX5->(!EOF())
			SX5->(MsUnlock())
		Endif
	EndIf
	RestArea(aArea)
	cFilAnt:= cFilBkp

Return lRet


/*/{Protheus.doc} Logs
Grava log em arquivo texto no servidor
@author Weskley Silva
@since 25/09/2018
@version 1.0
@param cTexto, character, Texto do log
/*/

Static Function Logs(cTexto,lErro)
	Local cCodLog	:= SubStr(DTOS(Date()),1,6)
	Local cPasta	:= "\log\HFATA012"
	Local cTexLog
	Default lErro	:= .F.
	FWMakeDir(cPasta)
	If File(cPasta+"\"+cCodLog+".txt")
		nHdl := fopen(cPasta+"\"+cCodLog+".txt" , 2 + 64 )
	Else
		nHdl := FCREATE(cPasta+"\"+cCodLog+".txt", FC_NORMAL)
	EndIf
	FSeek(nHdl, 0, 2)
	FWrite(nHdl, DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+CRLF )
	fclose(nHdl)
	If lErro
		RecLock("ZZZ",.F.)
		cTexLog			:= Alltrim(ZZZ->ZZZ_LOG)
		ZZZ->ZZZ_LOG	:= DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+chr(10)+cTexLog
		MsUnLock()
		//lErroJob	:= .T.
	EndIf
Return

Static Function Monitor(cMsg)

	cMsg1 := DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cMsg+chr(10)

	tcsqlexec("INSERT INTO YJOB (JOBLOG) VALUES ('"+cMsg1+"')")
	PutGlbValue("HFATA012",DTOC(date(),"dd/mm/yy")+"|"+time()+cMsg)
	PtInternal(1,DTOC(date(),"dd/mm/yy")+"|"+time()+"|"+cMsg)
Return

/*/{Protheus.doc} Atumsgnf
Atualiza mensagem na NF
@author Geyson Albano
@since 21/07/2020
@version 1.0
@param cTexto, character, Texto do log
/*/

Static Function Atumsgnf()

	Local mMsgfis  := SC5->C5_MENNOTA
	//Local cNumPF   := SC9->C9_XNUMPF
	Local _txtPad  := ""
	Local cNumPCli := SC5->C5_XPEDCLI
	Local cNumPed  := SC5->C5_NUM
	Local cCodCli  := SC5->C5_CLIENTE+"-"+AllTrim(SC5->C5_LOJACLI)
	Local _cQtdNF  := 0
	Local cQuery   := ""

	cQuery := "SELECT SUM(D2_QUANT) AS QTD FROM "+RetSqlName("SD2")+" D2 (NOLOCK) WHERE D2.D2_DOC = '"+SF2->F2_DOC+"' "
	cQuery += "AND D2_SERIE = '"+SF2->F2_SERIE+"' AND D2.D_E_L_E_T_ = '' "

	If Select("QTDNF") > 0
		QTDNF->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"QTDNF",.T.,.T.)

	_cQtdNF := QTDNF->QTD

	If strTran(mMsgfis,"-","") <> ''

		If AllTrim(cNumPCli) <> '' .AND. !Empty(cNumPF)
			_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF: "+Alltrim(cNumPF)+"  - PED. CLIENTE: "+cNumPCli+" - "+AllTrim(cValToChar(_cQtdNF))+" P�S."
			mMsgfis	:= SubStr(_txtPad + Space(254),1,254)
		Elseif !Empty(cNumPF)
			_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF: "+Alltrim(cNumPF)+" - "+AllTrim(cValToChar(_cQtdNF))+" P�S."
			mMsgfis	:= SubStr(_txtPad + Space(254),1,254)
		Else
			_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF:"+AllTrim(cNumPed)+"01 -"+AllTrim(cValToChar(_cQtdNF))+" P�S."
			mMsgfis	:= SubStr(_txtPad + Space(254),1,254)
		EndIf

	Else
		If AllTrim(cNumPCli) <> '' .AND. !Empty(cNumPF)
			_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF: "+Alltrim(cNumPF)+"  - PED. CLIENTE: "+cNumPCli+" - "+AllTrim(cValToChar(_cQtdNF))+" P�S."
			mMsgfis	:= SubStr(_txtPad + Space(254),1,254)
		ElseIF !Empty(cNumPF)
			_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF: "+Alltrim(cNumPF)+" - "+AllTrim(cValToChar(_cQtdNF))+" P�S."
			mMsgfis	:= SubStr(_txtPad + Space(254),1,254)
		Else
			_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF:"+AllTrim(cNumPed)+"01 -"+AllTrim(cValToChar(_cQtdNF))+" P�S."
			mMsgfis	:= SubStr(_txtPad + Space(254),1,254)
		EndIf
	EndIf


	If Alltrim(cnotapri) == Alltrim(SF2->F2_DOC) // VALIDA��O PARA N�O GRAVAR REFERENCIA DE NOTA QUE N�O � COMPLEMENTAR.
		If !lFim // VALIDA��O PARA ENTRAR SOMENTE QUANDO HOUVER NF COMPLEMENTAR GEYSON ALBANO 24/07/20
			_txPadnfc := "ACOMPANHA NF COMPLEMENTAR "
			RecLock("SF2",.F.)
			SF2->F2_MENNOTA := SubStr(Alltrim(mMsgfis) +" - "+ _txPadnfc + Space(254),1,254) //mMsgfis
			MSUNLOCK()
		Else
			RecLock("SF2",.F.)
			SF2->F2_MENNOTA := mMsgfis
			MSUNLOCK()
		EndIf
	Else
		_txPadnfc := "NF COMPLEMENTAR REF NF: "+AllTrim(cnotapri)+""
		RecLock("SF2",.F.)
		SF2->F2_VOLUME1 := 0
		SF2->F2_PBRUTO  := 0
		SF2->F2_PLIQUI  := 0
		SF2->F2_MENNOTA := SubStr(Alltrim(mMsgfis) +" - "+ _txPadnfc + Space(254),1,254)
		MSUNLOCK()
	EndIf

Return
