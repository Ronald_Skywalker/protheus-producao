#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWPrintSetup.ch"
#Include "TOTVS.CH"
#include 'parmtype.ch'
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'
#INCLUDE "RPTDEF.CH"

// Retornos poss�veis do MessageBox
#define IDOK			    1
#define IDCANCEL		    2
#define IDYES			    6
#define IDNO			    7

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HESTA003  �Autor  �Bruno Parreira      � Data �  13/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Tela de conferencia.                                        ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE.                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������   
�����������������������������������������������������������������������������
*/
User Function HESTA003()
	Local _afields := {}
	Local _carq    := ""
	Local cQuery   := ""
	Local lBlq	   := GetNewPar("HP_CONFBLQ", .F.)
	Private lGera  := .T.
	Private lMarcar   := .F.
	Private cAlias    :="SZJ"
	Private cCadastro := "Confer�ncia"
	Private oMark
	Private cPerg     := "HESTA003"
	Private lContinua := .F.
	Private cMark     := GetMark()
	Private aCampos   := {}
	Private aRotina := {{"Apontar","U_H003PCPAP()",0,2},;
		{"Pesquisa","U_PESQUISA()",0,2},;
		{"Zerar Confer�ncia","U_H003EXC(1)",0,2},;
		{"Estornar Liberacao","U_H003EST(1)",0,2},;
		{"Conferir Tudo","U_H003CONF(TRB->ZJ_NUMPF)",0,2},;
		{"Legenda" ,"U_H003LGEST()",0,2}}

	Private aCores := {{"TRB->ACONFERIR='Nao'","DISABLE"},;
		{"TRB->ACONFERIR='Sim'.And.TRB->EMCONFERE='Nao'.And.TRB->CONFERIDO='Nao'","ENABLE"},;
		{"TRB->ACONFERIR='Sim'.And.TRB->EMCONFERE='Sim'","BR_AMARELO"},;
		{"TRB->ACONFERIR='Sim'.And.TRB->CONFERIDO='Sim'.And.TRB->FATURADO='Nao'","BR_AZUL"},;
		{"TRB->ACONFERIR='Sim'.And.TRB->FATURADO='Sim'","BR_PRETO"}}

	Private _cIndex := ""
	Private _cChave := ""

	If !lBlq
		U_Pesquisa()
	Else
		MessageBox("Confer�ncia BLOQUEADA, favor utilizar a Confer�ncia WEB!!!","HP_CONFBLQ",16)
		Return
	EndIf
	
	If lGera
		dbSelectArea("TRB")

		_cIndex:=Criatrab(Nil,.F.)
		_cChave:="ZJ_NUMPF"
		Indregua("TRB",_cIndex,_cchave,,,"Selecionando Registros...")
		dBSETINDEX(_cIndex+ordbagext())

		dbSelectArea("TRB")
		dbGoTop()

		aCampos:={{"Lote DAP"      ,"ZJ_LOTDAP" ,TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2],X3Picture("ZJ_LOTDAP")},;
			{"Armazem."      ,"ZJ_LOCAL"  ,TAMSX3("ZJ_LOCAL"  )[3],TAMSX3("ZJ_LOCAL"  )[1],TAMSX3("ZJ_LOCAL"  )[2],X3Picture("ZJ_LOCAL"  )},;
			{"Pre-Fatura."   ,"ZJ_NUMPF"  ,TAMSX3("ZJ_NUMPF"  )[3],TAMSX3("ZJ_NUMPF"  )[1],TAMSX3("ZJ_NUMPF"  )[2],X3Picture("ZJ_NUMPF"  )},;
			{"Cliente"       ,"ZJ_CLIENTE",TAMSX3("ZJ_CLIENTE")[3],TAMSX3("ZJ_CLIENTE")[1],TAMSX3("ZJ_CLIENTE")[2],X3Picture("ZJ_CLIENTE")},;
			{"Loja"          ,"ZJ_LOJA"   ,TAMSX3("ZJ_LOJA"   )[3],TAMSX3("ZJ_LOJA"   )[1],TAMSX3("ZJ_LOJA"   )[2],X3Picture("ZJ_LOJA"   )},;
			{"Nome"          ,"A1_NOME"   ,TAMSX3("A1_NOME"   )[3],TAMSX3("A1_NOME"   )[1],TAMSX3("A1_NOME"   )[2],X3Picture("A1_NOME"   )},;
			{"Qtd. Pecas"    ,"ZJ_QTDLIB" ,TAMSX3("ZJ_QTDLIB" )[3],TAMSX3("ZJ_QTDLIB" )[1],TAMSX3("ZJ_QTDLIB" )[2],X3Picture("ZJ_QTDLIB" )},;
			{"A Conferir"    ,"ACONFERIR" ,"C",3,0,""},;
			{"Em Conferencia","EMCONFERE" ,"C",3,0,""},;
			{"Conferido"     ,"CONFERIDO" ,"C",3,0,""},;
			{"Efetivado"     ,"EFETIVADO" ,"C",3,0,""},;
			{"Faturado"      ,"FATURADO"  ,"C",3,0,""},;
			{"Ult. Usuario"  ,"USUARIO"   ,TAMSX3("D3_USUARIO")[3],TAMSX3("D3_USUARIO")[1],TAMSX3("D3_USUARIO")[2],X3Picture("D3_USUARIO")}}

		mBrowse(,,,,"TRB",aCampos,,,,,aCores)

		TRB->(DbCloseArea())
		TMP->(DbCloseArea())
	endif

Return

User Function Pesquisa()

	Local aRet := {}
	Local aPerg := {}

	aAdd(aPerg,{1,"Pr�-faturamento: " 	,Space(08),""   ,""		,""	    ,""		,0	,.F.}) // Tipo caractere

	If ParamBox(aPerg,"Par�metros...",@aRet,{||.T.},,,,,,"U_HESTA003",.T.,.T.)

		Processa( {|| lGera := GeraDados(aRet) }, "Aguarde...", "Processando...",.F.)
	Else
		MessageBox("Cancelado Pelo Usu�rio!!!","Aviso",16)
		lGera := .F.
		return lGera
	Endif

/*If !lGera
	MsgInfo("N�o existem confer�ncias em aberto.","Aviso")
	Return lGera
EndIf*/

    //TODO Validar se o pr�-faturamento est� liberado para confer�ncia. Ronaldo Pereira 22/04/19
	DbSelectArea("SZJ")
	DbSetOrder(1)
	DbSeek(xFilial("SZJ")+Alltrim(aRet[1]))
If SZJ->ZJ_CONF == " "
		MessageBox("N�o Liberado para Confer�ncia!","Aviso",0)
		lGera := .F.
		//return lGera
EndIf

return lGera

Static Function Geradados(aRet)
Local _astru := {}
Local cQuery := ""
Local lRet   := .F.
Local lGera := .T.


	IF Select("TMP") > 0
	TMP->(dbCloseArea())
	Endif

	IF Select("TRB") > 0
	TRB->(dbCloseArea())
	Endif

cQuery := "select ZJ_NUMPF,ZJ_PEDIDO,ZJ_CLIENTE,ZJ_LOJA,A1_NOME,ZJ_LOTDAP,ZJ_CONF,ZJ_DOC,ZJ_SERIE,SUM(ZJ_QTDLIB) AS ZJ_QTDLIB, ZJ_LOCAL, "
cQuery += CRLF + "(select TOP 1 ZP_USR from "+RetSqlName("SZP")+" SZP WITH(NOLOCK) where ZP_NUMPF = ZJ_NUMPF and SZP.D_E_L_E_T_ = '' order by ZP_DATA,ZP_HORA DESC) AS ZP_USR "
cQuery += CRLF + "from "+RetSqlName("SZJ")+" SZJ WITH(NOLOCK) "
cQuery += CRLF + "inner join "+RetSqlName("SA1")+" SA1 WITH(NOLOCK) " 
cQuery += CRLF + "on A1_COD = ZJ_CLIENTE "
cQuery += CRLF + "and A1_LOJA = ZJ_LOJA "
cQuery += CRLF + "and SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "where SZJ.D_E_L_E_T_ = '' " 
//cQuery += CRLF + "and ZJ_DATA between '"+DTOS(mv_par03)+"' and '"+DTOS(mv_par04)+"' "
cQuery += CRLF + " AND ZJ_NUMPF = '"+Alltrim(aRet[1])+"' "
/*If mv_par01 = 2
	cQuery += CRLF + "and ZJ_DOC = '' "
EndIf
If mv_par02 = 2
	cQuery += CRLF + "and ZJ_CONF  = 'L' "
Else
	cQuery += CRLF + "and ZJ_CONF IN ('S','L') "
EndIf
*/
cQuery += CRLF + "group by ZJ_NUMPF,ZJ_PEDIDO,ZJ_CLIENTE,ZJ_LOJA,A1_NOME,ZJ_LOTDAP,ZJ_CONF,ZJ_DOC,ZJ_SERIE, ZJ_LOCAL"
cQuery += CRLF + "order by ZJ_LOTDAP,ZJ_NUMPF"

//MemoWrite("HESTA003.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)

//Estrutura da tabela temporaria
AADD(_astru,{"ZJ_LOTDAP" ,TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2]})
AADD(_astru,{"ZJ_LOCAL"  ,TAMSX3("ZJ_LOCAL"  )[3],TAMSX3("ZJ_LOCAL"  )[1],TAMSX3("ZJ_LOCAL"  )[2]})
AADD(_astru,{"ZJ_NUMPF"  ,TAMSX3("ZJ_NUMPF"  )[3],TAMSX3("ZJ_NUMPF"  )[1],TAMSX3("ZJ_NUMPF"  )[2]})
AADD(_astru,{"ZJ_CLIENTE",TAMSX3("ZJ_CLIENTE")[3],TAMSX3("ZJ_CLIENTE")[1],TAMSX3("ZJ_CLIENTE")[2]})
AADD(_astru,{"ZJ_LOJA"   ,TAMSX3("ZJ_LOJA"   )[3],TAMSX3("ZJ_LOJA"   )[1],TAMSX3("ZJ_LOJA"   )[2]})
AADD(_astru,{"A1_NOME"   ,TAMSX3("A1_NOME"   )[3],TAMSX3("A1_NOME"   )[1],TAMSX3("A1_NOME"   )[2]})
AADD(_astru,{"ZJ_QTDLIB" ,TAMSX3("ZJ_QTDLIB" )[3],TAMSX3("ZJ_QTDLIB" )[1],TAMSX3("ZJ_QTDLIB" )[2]})
AADD(_astru,{"ACONFERIR" ,"C"                    ,3                      ,0                      })
AADD(_astru,{"EMCONFERE" ,"C"                    ,3                      ,0                      })
AADD(_astru,{"CONFERIDO" ,"C"                    ,3                      ,0                      })
AADD(_astru,{"EFETIVADO" ,"C"                    ,3                      ,0                      })
AADD(_astru,{"FATURADO"  ,"C"                    ,3                      ,0                      })
AADD(_astru,{"USUARIO"   ,TAMSX3("D3_USUARIO")[3],TAMSX3("D3_USUARIO")[1],TAMSX3("D3_USUARIO")[2]})

cArqTrab  := CriaTrab(_astru,.T.)
dbUseArea(.T.,,cArqTrab,"TRB")//, .F., .F. )

nReg := 0

Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		nReg++
		TMP->(DbSkip())
	EndDo
EndIf

ProcRegua(nReg)

//TODO Validar se o pr�-faturamento est� liberado para confer�ncia. Ronaldo Pereira 22/04/19
DbSelectArea("SZJ")
DbSetOrder(1)
DbSeek(xFilial("SZJ")+Alltrim(aRet[1]))
If SZJ->ZJ_CONF == " "
	//MsgInfo("N�o Liberado para Confer�ncia!","Aviso")
	lGera := .F.
	//return lGera
EndIf

//Atribui a tabela temporaria ao alias TRB
Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF()) .AND. lGera
	lRet := .T.
	While TMP->(!EOF())
		IncProc()
		cAConferir := "Sim"
		cEmConfere := "Nao"
		cConferido := "Nao"
		cFaturado  := "Nao"
		cEfetivado := "Nao"
		nSldTran   := 0
		cPedC9     := SubStr(TMP->ZJ_NUMPF,1,6)

		If TMP->ZJ_CONF = "S"
			cConferido := "Sim"
		Else
			DbSelectArea("SZP")
			DbSetOrder(1)
			If DbSeek(xFilial("SZP")+TMP->ZJ_NUMPF)
				cEmConfere := "Sim"
			EndIf
		EndIf
//TODO FLITRO PARA PEDIDOS EFETIVADOS. - RONALDO 04/10/18
		DbSelectArea("SZP")
		DbSetOrder(1)
		If DbSeek(xFilial("SZP")+TMP->ZJ_NUMPF)
			If (SZP->ZP_QTDLIB > 0)
				cEfetivado := "Sim"
				//cAConferir := "Nao"
			EndIf
		EndIf

        //TODO Bloqueio retirado conforme solicita��o chamado GPLI 24313.
		//nSldTran := SLDSZM(TMP->ZJ_LOTDAP)
		//If nSldTran > 0
			//cAConferir := "Nao"
		//EndIf

		lFat := .F.

		If !Empty(TMP->ZJ_DOC)
			cFaturado := "Sim"
		EndIf

		DbSelectArea("TRB")
		RecLock("TRB",.T.)
		TRB->ZJ_LOTDAP  := TMP->ZJ_LOTDAP
		TRB->ZJ_LOCAL   := TMP->ZJ_LOCAL
		TRB->ZJ_NUMPF   := TMP->ZJ_NUMPF
		TRB->ZJ_CLIENTE := TMP->ZJ_CLIENTE
		TRB->ZJ_LOJA    := TMP->ZJ_LOJA
		TRB->A1_NOME    := TMP->A1_NOME
		TRB->ZJ_QTDLIB  := TMP->ZJ_QTDLIB //GERADADOS1()
		TRB->ACONFERIR  := cAConferir
		TRB->EMCONFERE  := cEmConfere
		TRB->CONFERIDO  := cConferido
		TRB->EFETIVADO  := cEfetivado
		TRB->FATURADO   := cFaturado
		TRB->USUARIO    := UsrRetName(TMP->ZP_USR)
		MsUnlock()

		TMP->(DbSkip())
	EndDo
EndIf

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function H003CONF(cNumPF)
	Local aArea := GetArea()
	Local aSZJ  := {}
	Local nx    := 0
	Local cItem := "00"
	Local cLtPd := SuperGetMV("MV_XLOTPAD",.F.,"000000")+space(4)

	DbSelectArea("SZP")
	DbSetOrder(1)
	If DbSeek(xFilial("SZP")+cNumPF)
		MessageBox("J� existe uma confer�ncia em andamento para este pr�-faturamento.","Aten��o",48)
		Return
	EndIf

	nRet3 := MessageBox("Confirma confer�ncia total do pr�-faturamento selecionado?","Confirma��o",4)
	If nRet3 == 7 //MessageBox("Confirma confer�ncia total do pr�-faturamento selecionado?","Confirma��o",4)
		Return
	EndIf

	aSZJ := DADOS2(cNumPF,.T.)

//{TMPSZJ->B1_CODBAR,TMPSZJ->ZJ_PRODUTO+" - "+SubStr(TMPSZJ->B1_DESC,1,40),nSldSep,TMPSZJ->ZJ_QTDLIB,.F.}

	DbSelectArea("SZJ")
	DbSetOrder(1)
	DbSeek(xFilial("SZJ")+cNumPF)

	If Len(aSZJ) > 0
		For nx := 1 to Len(aSZJ)
			cItem := SOMA1(cItem,2)

		/*DbSelectArea("SZR") 
		RecLock("SZR",.T.)
		SZR->ZR_FILIAL  := xFilial("SZR")
		SZR->ZR_NUMPF   := cNumPF
		SZR->ZR_NUMPED   := SubStr(cNumPF,1,6)
		SZR->ZR_ITEM    := cItem
		SZR->ZR_CODBAR  := aSZJ[nx][1]
		SZR->ZR_PRODUTO := SubStr(aSZJ[nx][2],1,15)
		SZR->ZR_QUANT   := aSZJ[nx][4]
		SZR->ZR_USR     := __cUserID
		SZR->ZR_NOMEUSR := UsrRetName(ALLTRIM(__cUserID) )
		SZR->ZR_DATA    := DDATABASE
		SZR->ZR_HORA    := Time() 
		MsUnlock()
		*/

			DbSelectArea("SZP")
			RecLock("SZP",.T.)
			SZP->ZP_FILIAL  := xFilial("SZP")
			SZP->ZP_LOTDAP  := SZJ->ZJ_LOTDAP
			SZP->ZP_NUMPF   := cNumPF
			SZP->ZP_PEDIDO  := SZJ->ZJ_PEDIDO
			SZP->ZP_CODBAR  := aSZJ[nx][1]
			SZP->ZP_PRODUTO := SubStr(aSZJ[nx][2],1,15)
			SZP->ZP_LOTECTL := cLtPd
			SZP->ZP_QUANT   := aSZJ[nx][4]
			SZP->ZP_DATA 	:= DDATABASE
			SZP->ZP_HORA	:= Time()
			SZP->ZP_NUMPED  := SubStr(cNumPF,1,6)
			SZP->ZP_ITEM    := cItem
			SZP->ZP_USR     := __cUserID
			SZP->ZP_NOMEUSR := UsrRetName(ALLTRIM(__cUserID) )
			MsUnlock()
		Next

		DbSelectArea("SZJ")
		DbSetOrder(1)
		If DbSeek(xFilial("SZJ")+cNumPF)
			While SZJ->(!EOF()) .And. SZJ->ZJ_NUMPF = cNumPF
				nSZJLib := SZJ->ZJ_QTDLIB
				RecLock("SZJ",.F.)
				SZJ->ZJ_QTDSEP := nSZJLib //CONFERENCIA TOTAL
				MsUnlock()
				SZJ->(DbSkip())
			EndDo
		EndIf

		RecLock("TRB",.F.)
		TRB->EMCONFERE := "Sim"
		MsUnlock()
	EndIf

	RestArea(aArea)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function SLDSZM(cLotDAP)
	Local aArea   := GetArea()
	Local cQuery  := ""
	Local nRet    := 0

	cQuery := "select ZM_LOTDAP,SUM(ZM_QUANT) AS ZM_QUANT,SUM(ZM_QTDTRAN) AS ZM_QTDTRAN"
	cQuery += CRLF + "from "+RetSqlName("SZM")+" SZM WITH(NOLOCK) "
	cQuery += CRLF + "where SZM.D_E_L_E_T_ = '' "
	cQuery += CRLF + "and ZM_LOTDAP = '"+cLotDAP+"' "
	cQuery += CRLF + "group by ZM_LOTDAP "

	MemoWrite("HPCPA003_SLDSZM.txt",cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSZM", .F., .T.)

	DbSelectArea("TMPSZM")

	If TMPSZM->(!EOF())
		nRet := TMPSZM->ZM_QUANT - TMPSZM->ZM_QTDTRAN
	EndIf

	TMPSZM->(DbCloseArea())

	RestArea(aArea)

Return nRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function H003EXC(_tp,_perro)
	Local aArea    := GetArea()
	Local lZera    := .F.
	Local lEstorna := .F.
	Local cPedido  := SubStr(TRB->ZJ_NUMPF,1,6)
	Local cNumPF   := TRB->ZJ_NUMPF
	Local cAmzPik  := TRB->ZJ_LOCAL //AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
	Local nQtdXRes := 0


	DbSelectArea("SZJ")
	DbSetOrder(1)
	If DbSeek(xFilial("SZJ")+TRB->ZJ_NUMPF)
		If !Empty(SZJ->ZJ_DOC)
			RecLock("TRB",.F.)
			TRB->FATURADO = "Sim"
			MsUnlock()
		EndIf
	EndIf

	If _tp = 1
		If TRB->FATURADO = "Sim"
			MessageBox("Pedido j� faturado. N�o � poss�vel excluir a confer�ncia.","Aten��o",48)
		ElseIf TRB->CONFERIDO = "Sim"
			nRet3 := MessageBox("Pedido j� liberado para faturamento. Confirma estorno e exclus�o da confer�ncia?","Confirma��o",4)
			If nRet3 == 6  //MessageBox("Pedido j� liberado para faturamento. Confirma estorno e exclus�o da confer�ncia?","Confirma��o",4)
				lEstorna := .T.
				lZera	:= .T.
			Else
				MessageBox("Cancelado pelo usu�rio","Aten��o",16)
			EndIf
		ElseIf TRB->EMCONFERE = "Nao"
			MessageBox("Confer�ncia n�o iniciada.","Aten��o",48)
		Else
			nRet3 := MessageBox("Confirma exclus�o da confer�ncia em andamento?","Confirma��o",4)
			If  nRet3 == 6 //MessageBox("Confirma exclus�o da confer�ncia em andamento?","Confirma��o",4)
				lZera := .T.
			Else
				MessageBox("Cancelado pelo usu�rio","Aten��o",16)

			EndIf
		EndIf
	Else
		lEstorna 	:= .T.
		lZera		:= .T.
	Endif

	If lEstorna
		// AJUSTE FUN��O ESTORNO CONFERENCIA CHAMADO 5557 - GEYSON ALBANO 12-08-2019
		DbSelectArea("SC9")
		DbSetOrder(11)
		If DbSeek(xFilial("SC9")+cPedido+cNumPF)
			While SC9->(!EOF()) .And. SC9->C9_PEDIDO = cPedido .and. SC9->C9_XNUMPF = cNumPF

				SC9->(a460Estorna())
				lZera := .T.
				SC9->(DbSkip())

			EndDo

		EndIf
	EndIf


	If lZera

		DbSelectArea("SZP")
		DbSetOrder(1)
		If DbSeek(xFilial("SZP")+TRB->ZJ_NUMPF)
			While SZP->(!EOF()) .and. SZP->ZP_NUMPF = TRB->ZJ_NUMPF
				If _tp = 1
					RecLock("SZP",.F.)
					SZP->ZP_NOMEUSR := "H003EXC"
					SZP->(DBDELETE()) //ZERA CONFER�NCIA
					MsUnlock()
				Else
					RecLock("SZP",.F.)
					Replace SZP->ZP_QTDLIB	with 0
					If Alltrim(SZP->ZP_PRODUTO) != "CXA.EXTRA"
						Replace SZP->ZP_CONFERI with ""
					EndIf
					MsUnlock()
				Endif
				SZP->(DbSkip())
			EndDo
		EndIf
		DbSelectArea("SZJ")
		DbSetOrder(1)
		If DbSeek(xFilial("SZJ")+TRB->ZJ_NUMPF)
			While SZJ->(!EOF()) .and. SZJ->ZJ_NUMPF = TRB->ZJ_NUMPF
				nQtdXRes := 0

				If !EMPTY(SZJ->ZJ_BLEST)
					nQtdXRes := SB2->B2_XRESERV
				Else
					nQtdXRes := SB2->B2_XRESERV + SZJ->ZJ_QTDLIB
				EndIf

				If !EMPTY(SZJ->ZJ_BLEST)
					RecLock("SZJ",.F.)
					SZJ->ZJ_BLEST := ""
					MsUnlock()
				EndIf

				RecLock("SZJ",.F.)
				IF _tp = 1
					SZJ->ZJ_QTDSEP := 0
					SZJ->ZJ_FALTA := " "
				Endif
				SZJ->ZJ_CONF   := "L"
				MsUnlock()

				If _tp = 2
					If len(_perro) > 0
						nPos := Ascan(_perro,{|x| x = SZJ->ZJ_PRODUTO})

						If nPos = 0
							DbSelectArea("SB2")
							DbSetOrder(2)
							If DbSeek(xFilial("SB2")+cAmzPik+SZJ->ZJ_PRODUTO)
								RecLock("SB2",.F.)
								SB2->B2_XRESERV := nQtdXRes
								MsUnlock()
							EndIf
						Endif
					Else
						DbSelectArea("SB2")
						DbSetOrder(2)
						If DbSeek(xFilial("SB2")+cAmzPik+SZJ->ZJ_PRODUTO)
							RecLock("SB2",.F.)
							SB2->B2_XRESERV := nQtdXRes
							MsUnlock()
						EndIf
					Endif
				Else
					DbSelectArea("SB2")
					DbSetOrder(2)
					If DbSeek(xFilial("SB2")+cAmzPik+SZJ->ZJ_PRODUTO)
						RecLock("SB2",.F.)
						SB2->B2_XRESERV := nQtdXRes
						MsUnlock()
					EndIf
				Endif

				SZJ->(DbSkip())
			EndDo
		EndIf

		DbSelectArea("TRB")
		RecLock("TRB",.F.)
		TRB->CONFERIDO := "Nao"
		TRB->EMCONFERE := "Nao"
		TRB->EFETIVADO := "Nao"
		MsUnlock()

		If _tp = 1
			MessageBox("Confer�ncia zerada com sucesso!","Aviso",0)
		Else
			MessageBox("Pedido n�o efetivado!","Aviso",0)
		Endif
	EndIf

	RestArea(aArea)

Return


User Function H003EST(_tp,_perro)
	Local aArea    := GetArea()
	Local lZera    := .F.
	Local lEstorna := .F.
	Local cPedido  := SubStr(TRB->ZJ_NUMPF,1,6)
	Local cNumPF   := TRB->ZJ_NUMPF
	Local cAmzPik  := TRB->ZJ_LOCAL //AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
	Local nQtdXRes := 0
	Local cQuery   :=""

	If TRB->CONFERIDO = "Sim"

		nRet3 := MessageBox("Pedido j� liberado para faturamento. Confirma estorno da confer�ncia?","Confirma��o",4)

		If nRet3 == 6 //MessageBox("Pedido j� liberado para faturamento. Confirma estorno da confer�ncia?","Confirma��o",4)
			lEstorna := .T.
		Else
			MessageBox("Cancelado pelo usu�rio","Aten��o",16)
		EndIf
	Else
		MessageBox("Pedido n�o liberado ainda.","Aten��o",48)
		lEstorna 	:= .F.
	Endif

	If lEstorna
		// AJUSTE FUN��O ESTORNO CHAMADO 5548 - GEYSON ALBANO 09-08-2019
		DbSelectArea("SC9")
		DbSetOrder(11)
		If DbSeek(xFilial("SC9")+cPedido+cNumPF)
			While SC9->(!EOF()) .And. SC9->C9_PEDIDO = cPedido .and. SC9->C9_XNUMPF = cNumPF

				SC9->(a460Estorna())
				lZera := .T.
				SC9->(DbSkip())

			EndDo

		EndIf
	EndIf


	If lZera
		DbSelectArea("SZP")
		DbSetOrder(1)
		If DbSeek(xFilial("SZP")+TRB->ZJ_NUMPF)
			While SZP->(!EOF()) .and. SZP->ZP_NUMPF = TRB->ZJ_NUMPF
				RecLock("SZP",.F.)
				Replace SZP->ZP_QTDLIB	with 0
				If Alltrim(SZP->ZP_PRODUTO) != "CXA.EXTRA"
					Replace SZP->ZP_CONFERI with ""
				EndIf
				MsUnlock()
				SZP->(DbSkip())
			EndDo
		EndIf
		DbSelectArea("SZJ")
		DbSetOrder(1)
		If DbSeek(xFilial("SZJ")+TRB->ZJ_NUMPF)
			While SZJ->(!EOF()) .and. SZJ->ZJ_NUMPF = TRB->ZJ_NUMPF

				RecLock("SZJ",.F.)
				SZJ->ZJ_CONF   := "L"
				MsUnlock()

				DbSelectArea("SB2")
				DbSetOrder(2)
				If DbSeek(xFilial("SB2")+cAmzPik+SZJ->ZJ_PRODUTO)

					If !EMPTY(SZJ->ZJ_BLEST)
						nQtdXRes := SB2->B2_XRESERV
					Else
						nQtdXRes := SB2->B2_XRESERV + SZJ->ZJ_QTDLIB
					EndIf

					If !EMPTY(SZJ->ZJ_BLEST)
						RecLock("SZJ",.F.)
						SZJ->ZJ_BLEST := ""
						MsUnlock()
					EndIf

					RecLock("SB2",.F.)
					SB2->B2_XRESERV := nQtdXRes
					MsUnlock()

				EndIf
				SZJ->(DbSkip())
			EndDo
		EndIf

		DbSelectArea("TRB")
		RecLock("TRB",.F.)
		TRB->ACONFERIR := 'Sim'
		TRB->EMCONFERE := 'Sim'
		TRB->EFETIVADO := "Nao"
		MsUnlock()
		MessageBox("Libera��o estornada com sucesso!","Aviso",0)
	EndIf

	RestArea(aArea)

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �H003PCPAP �Autor  �Bruno Parreira      � Data �  20/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     � Funcao para apontamento das conferencias realizadas.       ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HPCPA003                                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function H003PCPAP()
	Local cQuery := ""
	Local oBrw
	Local _afields := {}
	Local _carq    := ""
	local nX
	Local nI
	Local cCombo
	Local aArea := GetArea()
	Local lRet  := .F.
	Private oDlg
	Private oGetDados
	Private nOpc      := 0
	Private nStr      := 0
	Private nPos      := 0
	Private aColAux1  := {}
	Private aColAux2  := {}
	Private lRefresh  := .T.
	Private aHeader1  := {}
	Private aCols1    := {}
	Private aHeader2  := {}
	Private aCols2    := {}
	Private aFields1  := {}
	Private aFields2  := {}
	Private cArqTMP
	Private nCol 	  := 0
	Private cLegenda  := ""
	Private nTamanho  := 0
	Private aRotina   := {}
	Private cArqTrab
	Private cApont    := SPACE(23) //13 Digitos para EAN + 10 digitos para Lote
	Private oFont20n  := TFont():New("Arial",,20,,.t.,,,,,.f.)
	Private oFont18n  := TFont():New("Arial",,18,,.t.,,,,,.f.)
	Private lConfirm  := .F.
	Private cVolume   := SPACE(5)
	Private cTpVol    := SPACE(3)
	Private cDescVol  := SPACE(30)
	Private lConf     := .T.
	Private lImpAut   := .F. //Marcio
	Private lGrvFalta := .F.
	Private lTOk      := .F.
	Private nTotLib   := 0
	Private nTotSep   := 0
	Private lHabilita := .T.

	Private oGetDados1
	Private oGetDados2

	If TRB->ACONFERIR = "Nao"
		MessageBox("Lote DAP possui transfer�ncias pendentes e n�o poder� ser conferido.","Aten��o",48)
		Return
	EndIf

// TODO LIBERAR FLAG PARA EFETIVAR PEDIDO. -- RONALDO PEREIRA 28/09/2018
	If TRB->CONFERIDO = "Sim" .And. TRB->EFETIVADO = "Sim"
		lHabilita := .F.
	ElseIf TRB->CONFERIDO = "Sim" .And. TRB->EFETIVADO = "Nao"
		lHabilita := .T.
	EndIf

//TODO FLAG IMPRESS�O DE PRE�O AUTOMATICO -- RONALDO PEREIRA 13/11/2018
	DbSelectArea("SC5")
	SC5->(DbSetOrder(1))
	SC5->(DbSeek(xFilial("SC5")+left((TRB->ZJ_NUMPF),6)))
	If SC5->C5_TPPED $ GETMV("HP_TPEDPRC")
		lImpAut := .T.
	Else
		lImpAut := .F.
	EndIf

//BEGIN TRANSACTION

//-------------------------------------------------------
//Estrutura do primeiro aCols
//-------------------------------------------------------

	aFields1 := {"ZP_ITEM","B1_CODBAR","ZP_PRODUTO","ZP_QUANT","ZP_VOLUME","ZP_TPVOL","DESCVOL"}

	aAdd(aColAux1,{"","","","",0,SPACE(3),SPACE(3),"",.F.})

	If lHabilita
		aAlterFd1 := {"ZP_QUANT","ZP_VOLUME","ZP_TPVOL"}
	Else
		aAlterFd1 := {}
	EndIf

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields1)
		If SX3->(DbSeek(aFields1[nX]))
			nTamanho := SX3->X3_TAMANHO
			cValid   := ""
			Do Case
			Case aFields1[nX] = "B1_CODBAR"
				cTit := "Codigo"
			Case aFields1[nX] = "ZP_PRODUTO"
				cTit := "Produto"
				nTamanho := 80
			Case aFields1[nX] = "ZP_QUANT"
				cTit   := "Saldo"
				//cValid := "U_H03VDQTDE(N1)"
			Case aFields1[nX] = "ZP_TPVOL"
				cTit := "Tp Vol"
				cValid := "U_H03PESQV(M->ZP_TPVOL,.T.,oGetDados1:nAt)"
			Otherwise
				cTit := Trim(X3Titulo())
			EndCase
			Aadd(aHeader1,{cTit,SX3->X3_CAMPO,SX3->X3_PICTURE,nTamanho,SX3->X3_DECIMAL,cValid,"",SX3->X3_TIPO,"",""})
		Else
			Do Case
			Case aFields1[nX] = "DESCVOL"
				Aadd(aHeader1,{"Descri��o","ZJ_NUMPF","@!",40,0,"","","C","",""})
			EndCase
		EndIf
	Next nX

	Aadd(aCols1,Array(Len(aHeader1)+1))

	For nI := 1 To Len(aHeader1)
		If aHeader1[nI][2] = "B1_CODBAR"
			aCols1[1][nI] := "000001"
		EndIf
		aCols1[1][nI] := CriaVar(aHeader1[nI][2])
	Next nI

	aCols1[1][Len(aHeader1)+1] := .F.

	aColAux1 := DADOS1(TRB->ZJ_NUMPF)

//-------------------------------------------------------
//Estrutura do segundo aCols
//-------------------------------------------------------

	aFields2 := {"B1_CODBAR","ZJ_PRODUTO","ZJ_QTDLIB","ZJ_QTDSEP","DA1_PRCVEN"}

	aAdd(aColAux2,{"","","",0,0,0,.F.})

	aAlterFd2 := {}

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields2)
		If SX3->(DbSeek(aFields2[nX]))
			nTamanho := SX3->X3_TAMANHO
			Do Case
			Case aFields2[nX] = "B1_CODBAR"
				cTit := "Codigo"
			Case aFields2[nX] = "ZJ_PRODUTO"
				cTit := "Produto"
				nTamanho := 80
			Case aFields2[nX] = "ZJ_QTDLIB"
				cTit := "Saldo"

			Case aFields2[nX] = "ZJ_QTDSEP"
				cTit := "Qtde. Original"
			Case aFields2[nX] = "DA1_PRCVEN"
				cTit := "Preco"
			Otherwise
				cTit := Trim(X3Titulo())
			EndCase
			Aadd(aHeader2,{cTit,SX3->X3_CAMPO,SX3->X3_PICTURE,nTamanho,SX3->X3_DECIMAL,"","",SX3->X3_TIPO,"",""})
		EndIf
	Next nX

	Aadd(aCols2,Array(Len(aHeader2)+1))

	For nI := 1 To Len(aHeader2)
		If aHeader2[nI][2] = "B1_CODBAR"
			aCols2[1][nI] := "000001"
		EndIf
		aCols2[1][nI] := CriaVar(aHeader2[nI][2])
	Next nI

	aCols2[1][Len(aHeader2)+1] := .F.

	aColAux2 := DADOS2(TRB->ZJ_NUMPF,.F.)

//-------------------------------------------------------------------------

//Apontamento: 13 Digitos para EAN (B1_CODBAR) + 6 digitos para Lote
	DEFINE MSDIALOG oDlg TITLE "Confer�ncia" FROM 00,00 TO 630,1200 PIXEL Style DS_MODALFRAME  // Cria Dialog sem o bot�o de Fechar.

	@ 005, 005 Say "Pr�-Faturamento: "+TRB->ZJ_NUMPF Size 200,010 COLORS 16711680,16777215 FONT oFont20n PIXEL OF oDlg
	@ 005, 150 Say "Cliente: "+TRB->ZJ_CLIENTE+"/"+TRB->ZJ_LOJA+" - "+TRB->A1_NOME Size 400,010 COLORS 16711680,16777215 FONT oFont20n PIXEL OF oDlg

	@ 020, 005 Say "Apontamento: "               Size 150,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDlg
	@ 018, 060 MSGET  oApont    Var     cApont   SIZE 080,010 VALID IF(VALAPONT(cApont,TRB->ZJ_NUMPF,TRB->ZJ_LOTDAP,.F.,.T.),.T.,.T.) WHEN lHabilita COLOR CLR_BLACK Picture "@!" PIXEL OF oDlg
	@ 017, 145 BUTTON oButton1  PROMPT "Incluir" SIZE 040,015 OF oDlg ACTION IF(VALAPONT(cApont,TRB->ZJ_NUMPF,TRB->ZJ_LOTDAP,.F.,.T.),.T.,.T.) WHEN lHabilita PIXEL MESSAGE "Incluir"

//TODO IMPRESS�O AUTOMATICA TOTALIT
	@ 017, 190 BUTTON oButton4  PROMPT "Imprimir Etq." SIZE 040,015 OF oDlg ACTION HPPRTTQ(SubStr(AllTrim(oGetDados1:aCols[oGetDados1:oBrowse:nAt, 2]),1,13)+SPACE(2),TRB->ZJ_NUMPF) WHEN Len(oGetDados1:aCols)>0 PIXEL MESSAGE "Imprime Etiqueta" //Marcio
	@ 018, 235 CHECKBOX oImpAut VAR lImpAut  PROMPT "Imprimir Pre�o" SIZE 075,015 COLORS 0, 16777215 PIXEL OF oDlg  //
	@ 026, 235 CHECKBOX oGrvFalta VAR lGrvFalta  PROMPT "Gravar Falta" SIZE 075,015 COLORS 0, 16777215 PIXEL OF oDlg  //

	@ 020, 310 Say "Volume: "                    Size 100,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDlg
	@ 018, 340 MSGET  oVolume   Var     cVolume  SIZE 020,010 COLOR CLR_BLACK Picture "@!" WHEN lHabilita PIXEL OF oDlg
	@ 017, 375 BUTTON oButton6  PROMPT "Imprimir Vol."  SIZE 040,015 OF oDlg ACTION ETIQVOL(TRB->ZJ_NUMPF,cVolume) PIXEL MESSAGE "Imprimir Volume"
	@ 020, 420 Say "Tipo Volume: "               Size 100,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDlg
	@ 018, 470 MSGET  oTpVol    Var     cTpVol   SIZE 020,010 VALID U_H03PESQV(cTpVol,.F.) WHEN lHabilita COLOR CLR_BLACK Picture "@!" F3 "SZQ" PIXEL OF oDlg
	@ 018, 500 MSGET  oDescVol  Var     cDescVol SIZE 100,010 COLOR CLR_BLACK Picture "@!" PIXEL OF oDlg WHEN .F.
	@ 215, 500 BUTTON oButton1  PROMPT "Salvar"  SIZE 035,015 OF oDlg ACTION (Iif(SALVAAPTO(TRB->ZJ_NUMPF),(lConfirm:=.T.,oDlg:End()),)) WHEN lHabilita PIXEL MESSAGE "Salvar"
	@ 215, 550 BUTTON oButton2  PROMPT "Exc.Pe�a"  SIZE 035, 015 OF oDlg ACTION U_VISUALPTO(TRB->ZJ_NUMPF) PIXEL MESSAGE "Apontamentos"
	@ 235, 500 BUTTON oButton4  PROMPT "Volumes"  SIZE 035,015 OF oDlg ACTION TOTALVOL(TRB->ZJ_NUMPF) PIXEL MESSAGE "Volumes"
	@ 235, 550 BUTTON oButton5  PROMPT "Imp Falta"  SIZE 035,015 OF oDlg ACTION U_IMPFALTA(TRB->ZJ_NUMPF) PIXEL MESSAGE "Imprimir Falta"
	@ 260, 518 BUTTON oButton3  PROMPT "Sair"    SIZE 050,015 OF oDlg ACTION (Iif(HESTSAI03(),(oDlg:End()),)) PIXEL MESSAGE "Sair"
	@ 180, 490 Say "Qtde conferida: "+AllTrim(Str(nTotSep))+"/"+AllTrim(Str(nTotLib)) Size 150,010 COLOR CLR_BLACK FONT oFont20n PIXEL OF oDlg
	@ 200, 520 CHECKBOX oConf VAR lConf  PROMPT "Conferindo" SIZE 055,015 COLORS 0, 16777215 PIXEL OF oDlg WHEN lTOk


	// oGetDados1 := MsNewGetDados():New(035,000,175,603,GD_UPDATE,"AllwaysTrue","AllwaysTrue","",aAlterFd1,,999,"AllwaysTrue","","AllwaysTrue",oDlg,aHeader1,aColAux1)
	oGetDados1 := MsNewGetDados():New(035,000,175,603,,"AllwaysTrue","AllwaysTrue","",aAlterFd1,,999,"AllwaysTrue","","AllwaysTrue",oDlg,aHeader1,aColAux1)

	oGetDados1:aCols=aColAux1
	N1 := oGetDados1:nAt

	oGetDados2 := MsNewGetDados():New(180,000,310,485,,"AllwaysTrue","AllwaysTrue","",aAlterFd2,,999,"AllwaysTrue","","AllwaysTrue",oDlg,aHeader2,aColAux2)
	oGetDados2:aCols=aColAux2
	N2 := oGetDados2:nAt

	oDlg:Refresh()

//oGetDados:oBrowse:bHeaderClick := {|oGetDados,nCol| Ordena(nCol)}



	ACTIVATE MSDIALOG oDlg CENTERED

//TMP->(DbCloseArea())
//TRB->(DbCloseArea())

	RestArea(aArea)

//If !lConfirm
//	DisarmTransaction()
//EndIf

//END TRANSACTION	

Return lRet


Static Function HPPRTETQ(cCodBar,cPreFat,nPreco) //Marcio

	Local cDesc1        := "Este programa tem como objetivo imprimir relatorio "
	Local cDesc2        := "de acordo com os parametros informados pelo usuario."
	Local cDesc3        := "Conferencia"
	Local cPict         := ""
	Local titulo       	:= "Conferencia"
	Local nLin         	:= 80
	Local Cabec1       	:= "Conferencia"
	Local Cabec2       	:= "Conferencia"
	Local imprime      	:= .T.
	Local aOrd 			:= {}

	Local _oFile 	:= Nil
	Local _cSaida	:= CriaTrab("",.F.)
	Local _cPorta	:= "LPT1"

	Private lEnd        := .F.
	Private lAbortPrint := .F.
	Private CbTxt       := ""
	Private limite      := 220
	Private tamanho     := "G"
	Private nomeprog    := "HESTA003" // Coloque aqui o nome do programa para impressao no cabecalho
	Private nTipo       := 18
	Private aReturn		:={"",1,"",1,3,"LPT2","",1}//aReturn     := { "Zebrado", 1, "Administracao", 2, 2, 1, "", 1}
	Private nLastKey    := 0
	Private cbtxt      	:= Space(10)
	Private cbcont     	:= 00
	Private CONTFL     	:= 01
	Private m_pag      	:= 01
	Private wnrel      	:= "AMWLEETQ" // Coloque aqui o nome do arquivo usado para impressao em disco
	Private cString		:= "SZR"
	Private cTam 		:= ""
/*
wnrel := SetPrint(cString,NomeProg,"",@titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,Tamanho,,.T.)

	If nLastKey == 27
	Return
	Endif

SetDefault(aReturn,cString)


	If AllTrim(Empty(cCodBar))
	Return
	Endif
*/
// TODO Validar se o produto pertence ao prefaturamento. Ronaldo pereira 23/01/19
	DbSelectArea("SB1")
	DbSetOrder(5)
	DbSeek(xFilial("SB1")+cCodBar)
	DbSelectArea("SZJ")
	DbSetOrder(3)
	DbSeek(xFilial("SZJ")+cPreFat+SB1->B1_COD)
	If SZJ->ZJ_PRODUTO == SB1->B1_COD

		If !Empty(AllTrim(cCodBar))
			DbSelectArea("SB1")
			SB1->(DbSetOrder(5)) //B1_FILIAL+B1_CODBAR
			If SB1->(DbSeek(xFilial("SB1")+cCodBar))

				//TODO Ajuste do tamanho do produto sem os Zeros iniciais. Ronaldo Pereira 21/01/2019
				Do case
				Case SUBSTR(SB1->B1_COD,12,3) = "000"
					cTam := AllTrim(STRTRAN(SUBSTR(SB1->B1_COD,12,4),"000",""))
				Case SUBSTR(SB1->B1_COD,12,2) = "00"
					cTam := AllTrim(STRTRAN(SUBSTR(SB1->B1_COD,12,4),"00",""))
				Case SUBSTR(SB1->B1_COD,12,1) = "0"
					cTam := AllTrim(SUBSTR(SB1->B1_COD,13,3))
				Otherwise
					cTam := AllTrim(SUBSTR(SB1->B1_COD,12,4))
				EndCase

				//DbSelectArea("DA1")
				//DA1->(DbSetOrder(1))
				//If DA1->( DbSeek( xFilial("DA1") + "180" + SB1->B1_COD ))

				if nPreco > 0

					cPicture := "@E 999,999.99"

					cStrEtq := "^XA^MCY^XZ^XA^FWN^CFD,24^CI0^PR8^MNY^MTT^MMT^PW328^LL0200^LS0^MD10^PON^PMN^LRN^XZ^XA^MCY^XZ^XA^LRN^LH10,100^FT7,28^A0N,20,19^FH^FD"
					cStrEtq += "REF:" + SUBSTR(SB1->B1_COD,1,8)
					cStrEtq += "^FS^FT137,28^A0N,20,19^FH^FD"
					cStrEtq += "COR:"  + SUBSTR(SB1->B1_COD,9,3)
					cStrEtq += "^FS^FT7,54^A0N,20,19^FH^FD"
					//cStrEtq += "TAM:" + AllTrim(STRTRAN(SUBSTR(SB1->B1_COD,12,4),"0",""))
					cStrEtq += "TAM:" + cTam
					cStrEtq += "^FS^FT7,80^A0N,20,19^FH^FD"
					cStrEtq += "R$ "  + Transform(nPreco,cPicture)//AllTrim(cValToChar(DA1->DA1_PRCVEN))//STR(DA1->DA1_PRCVEN,TamSX3("DA1_PRCVEN")[1],TamSX3("DA1_PRCVEN")[2])
					cStrEtq += "^FS^PQ1,0000,1,Y^FS^XZ"

					//        Alert(SB1->B1_COD)
					//		@ 00,000 PSAY CHR(2)+cStrEtq

					//		MS_FLUSH()

					_oFile := FCreate(_cSaida,0)
					FWrite(_oFile,cStrEtq,Len(cStrEtq))
					FClose(_oFile)

					Copy File &_cSaida to &_cPorta
				Else
					MessageBox( "Pre�o n�o cadastrado na tabela de pre�os!" ,"Aten��o",16)
				Endif
			Else
				MessageBox( "Produto n�o localizado. C�digo de barras: " + cCodBar,"Aten��o",16 )
			Endif
		Endif
	EndIf

Return

Static Function HPPRTTQ(cCodBar,cPreFat) //Marcio

	Local cDesc1        := "Este programa tem como objetivo imprimir relatorio "
	Local cDesc2        := "de acordo com os parametros informados pelo usuario."
	Local cDesc3        := "Conferencia"
	Local cPict         := ""
	Local titulo       	:= "Conferencia"
	Local nLin         	:= 80
	Local Cabec1       	:= "Conferencia"
	Local Cabec2       	:= "Conferencia"
	Local imprime      	:= .T.
	Local aOrd 			:= {}

	Local _oFile 	:= Nil
	Local _cSaida	:= CriaTrab("",.F.)
	Local _cPorta	:= "LPT1"

	Private lEnd        := .F.
	Private lAbortPrint := .F.
	Private CbTxt       := ""
	Private limite      := 220
	Private tamanho     := "G"
	Private nomeprog    := "HESTA003" // Coloque aqui o nome do programa para impressao no cabecalho
	Private nTipo       := 18
	Private aReturn		:={"",1,"",1,3,"LPT2","",1}//aReturn     := { "Zebrado", 1, "Administracao", 2, 2, 1, "", 1}
	Private nLastKey    := 0
	Private cbtxt      	:= Space(10)
	Private cbcont     	:= 00
	Private CONTFL     	:= 01
	Private m_pag      	:= 01
	Private wnrel      	:= "AMWLEETQ" // Coloque aqui o nome do arquivo usado para impressao em disco
	Private cString		:= "SZR"
	Private cTam 		:= ""
/*
wnrel := SetPrint(cString,NomeProg,"",@titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,Tamanho,,.T.)

	If nLastKey == 27
	Return
	Endif

SetDefault(aReturn,cString)


	If AllTrim(Empty(cCodBar))
	Return
	Endif
*/
// TODO Validar se o produto pertence ao prefaturamento. Ronaldo pereira 23/01/19
	DbSelectArea("SB1")
	DbSetOrder(5)
	DbSeek(xFilial("SB1")+cCodBar)
	DbSelectArea("SZJ")
	DbSetOrder(3)
	DbSeek(xFilial("SZJ")+cPreFat+SB1->B1_COD)
	If SZJ->ZJ_PRODUTO == SB1->B1_COD

		If !Empty(AllTrim(cCodBar))
			DbSelectArea("SB1")
			SB1->(DbSetOrder(5)) //B1_FILIAL+B1_CODBAR
			If SB1->(DbSeek(xFilial("SB1")+cCodBar))

				//TODO Ajuste do tamanho do produto sem os Zeros iniciais. Ronaldo Pereira 21/01/2019
				Do case
				Case SUBSTR(SB1->B1_COD,12,3) = "000"
					cTam := AllTrim(STRTRAN(SUBSTR(SB1->B1_COD,12,4),"000",""))
				Case SUBSTR(SB1->B1_COD,12,2) = "00"
					cTam := AllTrim(STRTRAN(SUBSTR(SB1->B1_COD,12,4),"00",""))
				Case SUBSTR(SB1->B1_COD,12,1) = "0"
					cTam := AllTrim(SUBSTR(SB1->B1_COD,13,3))
				Otherwise
					cTam := AllTrim(SUBSTR(SB1->B1_COD,12,4))
				EndCase

				DbSelectArea("DA1")
				DA1->(DbSetOrder(1))
				If DA1->( DbSeek( xFilial("DA1") + Alltrim(GetMv("HP_TABPCON" )) + SB1->B1_COD ))

					cPicture := "@E 999,999.99"

					cStrEtq := "^XA^MCY^XZ^XA^FWN^CFD,24^CI0^PR8^MNY^MTT^MMT^PW328^LL0200^LS0^MD10^PON^PMN^LRN^XZ^XA^MCY^XZ^XA^LRN^LH10,100^FT7,28^A0N,20,19^FH^FD"
					cStrEtq += "REF:" + SUBSTR(SB1->B1_COD,1,8)
					cStrEtq += "^FS^FT137,28^A0N,20,19^FH^FD"
					cStrEtq += "COR:"  + SUBSTR(SB1->B1_COD,9,3)
					cStrEtq += "^FS^FT7,54^A0N,20,19^FH^FD"
					//cStrEtq += "TAM:" + AllTrim(STRTRAN(SUBSTR(SB1->B1_COD,12,4),"0",""))
					cStrEtq += "TAM:" + cTam
					cStrEtq += "^FS^FT7,80^A0N,20,19^FH^FD"
					cStrEtq += "R$ "  + Transform(DA1->DA1_PRCVEN,cPicture)//AllTrim(cValToChar(DA1->DA1_PRCVEN))//STR(DA1->DA1_PRCVEN,TamSX3("DA1_PRCVEN")[1],TamSX3("DA1_PRCVEN")[2])
					cStrEtq += "^FS^PQ1,0000,1,Y^FS^XZ"

					//        Alert(SB1->B1_COD)
					//		@ 00,000 PSAY CHR(2)+cStrEtq

					//		MS_FLUSH()

					_oFile := FCreate(_cSaida,0)
					FWrite(_oFile,cStrEtq,Len(cStrEtq))
					FClose(_oFile)

					Copy File &_cSaida to &_cPorta
				Else
					MessageBox( "Pre�o n�o cadastrado na tabela de pre�os!","Aten��o",16 )
				Endif
			Else
				MessageBox( "Produto n�o localizado. C�digo de barras: " + cCodBar,"Aten��o",16 )
			Endif
		Endif
	EndIf

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Ronaldo Pereira     � Data �  30/05/19   ���
�������������������������������������������������������������������������͹��
���Desc.     � Imprimi etqueta de volume do pr�-faturamento.              ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ETIQVOL(cPreFat,cNumVol)

	Local cDesc1        := "Este programa tem como objetivo imprimir etiqueta "
	Local cDesc2        := "de acordo com os parametros informados pelo usuario."
	Local cDesc3        := "Conferencia"
	Local cPict         := ""
	Local titulo       	:= "Conferencia"
	Local nLin         	:= 80
	Local Cabec1       	:= "Conferencia"
	Local Cabec2       	:= "Conferencia"
	Local imprime      	:= .T.
	Local aOrd 			:= {}

	Local _oFile 	:= Nil
	Local _cSaida	:= CriaTrab("",.F.)
	Local _cPorta	:= "LPT1"

	Private lEnd        := .F.
	Private lAbortPrint := .F.
	Private CbTxt       := ""
	Private limite      := 220
	Private tamanho     := "G"
	Private nomeprog    := "HESTA003" // Coloque aqui o nome do programa para impressao no cabecalho
	Private nTipo       := 18
	Private aReturn		:={"",1,"",1,3,"LPT2","",1}//aReturn     := { "Zebrado", 1, "Administracao", 2, 2, 1, "", 1}
	Private nLastKey    := 0
	Private cbtxt      	:= Space(10)
	Private cbcont     	:= 00
	Private CONTFL     	:= 01
	Private m_pag      	:= 01
	Private wnrel      	:= "AMWLEETQ" // Coloque aqui o nome do arquivo usado para impressao em disco
	Private cString		:= "SZR"
	Private cTam 		:= ""

	Private cCodigo     := cPreFat + AllTrim(cNumVol)

	If !Empty(AllTrim(cNumVol))

		cStrEtq := "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ"
		cStrEtq += "^XA"
		cStrEtq += "^MMT"
		cStrEtq += "^PW336"
		cStrEtq += "^LL0200"
		cStrEtq += "^LS0"
		cStrEtq += "^FT11,80^A0N,72,50^FH\^FD" + cPreFat
		cStrEtq += "^FS^FT255,77^A0N,70,48^FH\^FD" + cNumVol
		cStrEtq += "^FS^FT228,69^A0N,34,33^FH\^FD=^FS"
		cStrEtq += "^BY2,3,74^FT21,184^BCN,,N,N"
		cStrEtq += "^FD" + cCodigo
		cStrEtq += "^FS^PQ1,0,1,Y^XZ"


		_oFile := FCreate(_cSaida,0)
		FWrite(_oFile,cStrEtq,Len(cStrEtq))
		FClose(_oFile)

		Copy File &_cSaida to &_cPorta
	EndIf

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function VISUALPTO(cNumPF)
	Local oDialog
	Local aColsApt   := {}
	local aAltFld    := {}
	local aHeaderApt := {}
	Local aFldApt    := {"ZP_CODBAR","ZP_PRODUTO","B1_DESC","ZP_VOLUME","ZP_LOTECTL","ZP_QUANT"}
	local cCampo     := ""
	Local aAuxApt    := {}
	Local nx,ni      := 0
	Local lHabilita := .T.

	Local nTamanho   := 0
	Private cExclui  := SPACE(23)
	Private cVol     := SPACE(05)
	Private cTpVolEx := SPACE(03)
	Private oGetDados3
	Private oFont18n  := TFont():New("Arial",,18,,.t.,,,,,.f.)


	// TODO LIBERAR FLAG PARA EFETIVAR PEDIDO. -- RONALDO PEREIRA 28/09/2018
	If TRB->CONFERIDO = "Sim" .And. TRB->EFETIVADO = "Sim"
		lHabilita := .F.
	ElseIf TRB->CONFERIDO = "Sim" .And. TRB->EFETIVADO = "Nao"
		lHabilita := .T.
	EndIf

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFldApt)
		If SX3->(DbSeek(aFldApt[nX]))
			nTamanho := SX3->X3_TAMANHO
			Do Case
			Case aFldApt[nX] = "ZP_PRODUTO"
				cCampo := "Produto"
			Case aFldApt[nX] = "B1_DESC"
				cCampo := "Descricao"
				nTamanho := 100
			Otherwise
				cCampo := Trim(X3Titulo())
			EndCase
			Aadd(aHeaderApt,{cCampo,;
				SX3->X3_CAMPO,;
				SX3->X3_PICTURE,;
				nTamanho,;
				SX3->X3_DECIMAL,;
				SX3->X3_VALID,;
				"",;
				SX3->X3_TIPO,;
				"",;
				"" })
		EndIf
	Next nX

	Aadd(aColsApt,Array(Len(aHeaderApt)+1))

	For nI := 1 To Len(aHeaderApt)
		aColsApt[1][nI] := CriaVar(aHeaderApt[nI][2])
	Next nI

	aColsApt[1][Len(aHeaderApt)+1] := .F.

	aAuxApt := ARRAYAPT(cNumPF)

	DEFINE MSDIALOG oDialog TITLE "Apontamentos Realizados" FROM 00,00 TO 630,1200 PIXEL

//@ 295, 010 MSGET  oExclui   Var     cExclui  SIZE 080,010 VALID EXCLUIAPT(cExclui,cNumPF) WHEN lHabilita COLOR CLR_BLACK Picture "@!" PIXEL OF oDialog
	@ 295, 010 Say "V.Barras:"                   Size 100,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDialog
	@ 295, 080 MSGET  oExclui   Var     cExclui  SIZE 080,010 WHEN lHabilita COLOR CLR_BLACK Picture "@!" PIXEL OF oDialog
	@ 295, 180 Say "Volume: "                    Size 100,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDialog
	@ 295, 250 MSGET  oVol      Var     cVol     SIZE 080,010 WHEN lHabilita COLOR CLR_BLACK Picture "@!" PIXEL OF oDialog
	@ 295, 340 Say "Tp. Vol: "                   Size 100,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDialog
	@ 295, 385 MSGET  oVol      Var     cTpVolEx SIZE 040,010 WHEN lHabilita COLOR CLR_BLACK Picture "@!" PIXEL OF oDialog
	@ 295, 450 BUTTON oButton1  PROMPT "Excluir Pe�a" SIZE 050,015 OF oDialog ACTION EXCLUIAPT(cExclui,cNumPF,cVol,cTpVolEx) WHEN lHabilita PIXEL MESSAGE "Excluir Pe�a"
	@ 295, 555 BUTTON oButton2  PROMPT "Sair"   SIZE 040, 015 OF oDialog ACTION (Iif(HESTSBI03(),(oDialog:End()),oDialog:ForceRefresh())) PIXEL MESSAGE "Sair"
	// @ 295, 555 BUTTON oButton2  PROMPT "Sair" SIZE 040, 015 OF oDialog ACTION (oDialog:End(),oGetDados2:Refresh()) PIXEL MESSAGE "Sair"

	oGetDados3 := MsNewGetDados():New(000,000,290,603,GD_UPDATE,"AllwaysTrue","AllwaysTrue","",aAltFld,, 999, "AllwaysTrue", "", "AllwaysTrue",oDialog, aHeaderApt, aColsApt)

	oGetDados3:aCols=aAuxApt

	ACTIVATE MSDIALOG oDialog CENTERED

Return

//_____________________________________________________________________________
/*/{Protheus.doc} Resumo por Volumes
Rotina responsavel por mostrar a qtde de pe�as por volume;

@author Ronaldo Pereira
@since 30 de Dezembro de 2018
@version V.01
/*/
//_____________________________________________________________________________

Static Function TOTALVOL(cNumPF)
	Local oButton1
	Local oSay1
	Static oDlgVol
	Local nX, nI 	 := 0
	Local aHeaderVol := {}
	Local aColsVol 	 := {}
	Local aFieldFill := {}
	Local aFldVol 	 := {"ZP_VOLUME","ZP_QUANT","ZP_TPVOL"}
	Local aAlterFld  := {}
	Local aAuxVol    := {}
	Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)
	Static oGeraVol

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFldVol)
		If SX3->(DbSeek(aFldVol[nX]))
			nTamanho := SX3->X3_TAMANHO
			Do Case
			Case aFldVol[nX] = "ZP_VOLUME"
				cCampo := "Volume"
				nTamanho := 10
			Case aFldVol[nX] = "ZP_QUANT"
				cCampo := "Quantidade"
				nTamanho := 10
			Case aFldVol[nX] = "ZP_TPVOL"
				cCampo := "Tipo Caixa"
			Otherwise
				cCampo := Trim(X3Titulo())
			EndCase
			Aadd(aHeaderVol,{cCampo,;
				SX3->X3_CAMPO,;
				SX3->X3_PICTURE,;
				nTamanho,;
				SX3->X3_DECIMAL,;
				SX3->X3_VALID,;
				"",;
				SX3->X3_TIPO,;
				"",;
				"" })
		Endif
	Next nX


	Aadd(aColsVol,Array(Len(aHeaderVol)+1))

	For nI := 1 To Len(aHeaderVol)
		aColsVol[1][nI] := CriaVar(aHeaderVol[nI][2])
	Next nI

	aColsVol[1][Len(aHeaderVol)+1] := .F.

	aAuxVol := ARRAYVOL(cNumPF)

	DEFINE MSDIALOG oDlgVol TITLE "VOLUMES" FROM 000, 000  TO 500, 400 COLORS 0, 16777215 PIXEL

	@ 233, 152 BUTTON oButton1 PROMPT "Fechar" SIZE 037, 012 OF oDlgVol ACTION (oDlgVol:End(),oGetDados2:Refresh()) PIXEL MESSAGE "Fechar"
	@ 007, 006 SAY oSay1 PROMPT "RESUMO DE VOLUMES:" SIZE 065, 007 COLOR CLR_BLACK FONT oFont12n PIXEL OF oDlgVol


	oGeraVol := MsNewGetDados():New(  017, 004, 225, 192, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "", aAlterFld,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlgVol, aHeaderVol, aColsVol)

	oGeraVol:aCols=aAuxVol

	ACTIVATE MSDIALOG oDlgVol CENTERED

Return


Static Function ARRAYVOL(cNumPF)
	Local aArrayVol   := {}

	cQry := " SELECT ZP_NUMPF, ZP_VOLUME, SUM(ZP_QUANT) AS QUANT, ZP_TPVOL "
	cQry += " FROM "+RetSqlName("SZP")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZP_NUMPF = '"+cNumPF+"' "
	cQry += " GROUP BY ZP_NUMPF, ZP_VOLUME, ZP_TPVOL   "
	cQry += " ORDER BY CONVERT(INT, ZP_VOLUME) "

	If Select("TMPVOL") > 0
		TMPVOL->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPVOL",.T.,.T.)
	DbSelectArea("TMPVOL")
	DbGoTop()

	While TMPVOL->(!EOF()) .And. TMPVOL->ZP_NUMPF = cNumPF
		aAdd(aArrayVol,{TMPVOL->ZP_VOLUME,TMPVOL->QUANT,TMPVOL->ZP_TPVOL,.F.})
		TMPVOL->(DbSkip())
	EndDo


Return aArrayVol


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �EXCLUIAPT �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Exclusao de apontamento realizado.                          ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/    

Static Function EXCLUIAPT(cExc,cNumPF,cVolume,cTpVolEx)
	Local aAux1 := aClone(oGetDados1:aCols)
	Local aAux2 := aClone(oGetDados2:aCols)
	Local aAux3 := aClone(oGetDados3:aCols)
	Local nzjexc:= 0
	Local nQtde := 0
	Local nPos1 := 0
	Local nPos2 := 0
	Local nPos3 := 0
	Local nLen  := 0
	Local cCodB := SubStr(cExc,1,13)+SPACE(2)
	Local cLote := SubStr(cExc,14,10)
	Local cLtPd := SuperGetMV("MV_XLOTPAD",.F.,"000000")+space(4)
	Local cPrd  := ""

	If Empty(cLote)
		cLote := cLtPd
	EndIf

	If !Empty(cExc)
		nLen := Len(aAux3)
		If nLen > 0
			nPos3 := Ascan(aAux3,{|x| x[1]+x[4]+x[5]+x[7] = cCodB+cVolume+cLote+cTpVolEx})
			If nPos3 > 0
				cPrd := aAux3[nPos3][2]

				nQtde := aAux3[nPos3][6] - 1
				aAux3[nPos3][6] := nQtde

				DbSelectArea("SZP")
				DbSetOrder(5)
				If DbSeek(xFilial("SZP")+cNumPF+cCodB+cLote+cVolume)

					If nQtde = 0
						aDel(aAux3,nPos3)
						aSize(aAux3,Len(aAux3)-1)
						nLen := nLen - 1
					EndIf

					oGetDados3:SetArray(aAux3,.T.)
					oGetDados3:Refresh()

					Reclock("SZP",.F.)
					If nQtde <= 0
						SZP->ZP_NOMEUSR := "EXCLUIAPT"
						SZP->(DBDELETE()) // EXCLUIAPT
					Else
						SZP->ZP_QUANT := nQtde
					EndIf
					MsUnlock()
				Else
					MessageBox("Produto n�o encontrado no volume.","Aten��o",16)
					Return
				EndIf

				DbSelectArea("SZJ")
				DbSetOrder(3)
				DbSeek(xFilial("SZJ")+cNumPF+cPrd)
				If Found()
					While !EOF() .and. cNumPF == SZJ->ZJ_NUMPF .AND. cPrd == SZJ->ZJ_PRODUTO .and. nzjexc == 0
						If SZJ->ZJ_QTDSEP > 0
							RecLock("SZJ",.F.)
							SZJ->ZJ_QTDSEP -= 1//nSZJLib // EXCLUIAPT
							nzjexc := 1
							MsUnlock()
						Endif
						DbSkip()
					Enddo
				Endif
                /*
				If DbSeek(xFilial("SZJ")+cNumPF+cPrd)
					If SZJ->ZJ_QTDSEP > 0
                        nQtdSep := SZJ->ZJ_QTDSEP - 1
                        RecLock("SZJ",.F.)
                        SZJ->ZJ_QTDSEP := nQtdSep
                        MsUnlock()
					EndIf
				EndIf
                */

				nPos2 := Ascan(aAux2,{|z| AllTrim(z[1]) = AllTrim(cCodB)})
				If nPos2 > 0
					nQtde := aAux2[nPos2][3] + 1
					aAux2[nPos2][3] := nQtde
				EndIf
				oGetDados2:SetArray(aAux2,.T.)
				oGetDados2:ForceRefresh()

				nPos1 := Ascan(aAux1,{|y| y[2]+y[5] = cCodB+cVolume})
				If nPos1 > 0
					nQtde := aAux1[nPos1][4] - 1
					aAux1[nPos1][4] := nQtde
				EndIf

				nTotSep := nTotSep - 1

				oGetDados1:SetArray(aAux1,.T.)
				oGetDados1:ForceRefresh()
			Else
				MessageBox("Produto n�o encontrado.","Aten��o",48)
			EndIf
		EndIf
	EndIf
	cExclui := SPACE(23)
	oExclui:setfocus()
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARRAYAPT  �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Monta o array da tela de informacoes de apontamento.        ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function ARRAYAPT(cNumPF)
	Local aArray   := {}
	Local cDescPrd := ""

	DbSelectArea("SZP")
	DbSetOrder(1)
	If DbSeek(xFilial("SZP")+cNumPF)
		While SZP->(!EOF()) .And. SZP->ZP_NUMPF = cNumPF
			cDescPrd := GetAdvFVal("SB1","B1_DESC",xFilial("SB1")+SZP->ZP_PRODUTO,1,"")
			aAdd(aArray,{SZP->ZP_CODBAR,SZP->ZP_PRODUTO,cDescPrd,SZP->ZP_VOLUME,SZP->ZP_LOTECTL,SZP->ZP_QUANT,SZP->ZP_TPVOL,.F.})
			SZP->(DbSkip())
		EndDo
	Else
		aAdd(aArray,{"","","","",0,"","",.F.})
	EndIf

Return aArray

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ARRAYAPT  �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Monta o array da tela de informacoes de apontamento.        ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function H03PESQV(cCodVol,lPesq,nLinha)
	Local aAux1 := aClone(oGetDados1:aCols)
	Local lRet  := .T.
	Local cDescric := ""

	If lPesq
		If !Empty(cCodVol)
			cDescric := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+cCodVol,1,"")
			aAux1[nLinha][7] := cDescric
			If Empty(cDescric)
				lRet := .F.
				MessageBox("Tipo de volume inexistente!","Aten��o",48)
			EndIf
		Else
			aAux1[nLinha][7] := ""
		EndIf
		oGetDados1:SetArray(aAux1,.T.)
		oGetDados1:Refresh()
	Else
		If !Empty(cCodVol)
			cDescVol := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+cCodVol,1,"")
			If Empty(cDescVol)
				lRet := .F.
				MessageBox("Tipo de volume inexistente!","Aten��o",48)
			EndIf
		Else
			cDescVol := ""
		EndIf
	EndIf

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function H03VDQTDE(nPosAtu)
	Local aAux1 := aClone(oGetDados1:aCols)
	Local aAux2 := aClone(oGetDados2:aCols)
	Local cCodBar := aAux1[nPosAtu][1]
	Local nQtde   := aAux1[nPosAtu][3]
	Local nPosEAN := 0
	Local nx      := 0
	Local nQtdTot := 0
	Local nQtdOri := 0
	Local lRet    := .T.

	If !Empty(cCodBar)
		For nx := 1 to Len(aAux1)
			If aAux1[nx][1] = cCodBar .And. nx <> nPosAtu
				nQtdTot += aAux1[nx][3]
			EndIf
		Next
		nQtdTot += M->ZJ_QTDLIB
		nPosEAN := Ascan(aAux2,{|x| x[1] = cCodBar})
		If nPosEAN > 0
			nQtdOri := aAux2[nPosEAN][4]
			If nQtdTot > nQtdOri
				MessageBox("Saldo insuficiente para distribuir.","Aten��o",48)
				lRet := .F.
			Else
				aAux2[nPosEAN][3] := nQtdOri - nQtdTot
			EndIf
			oGetDados1:SetArray(aAux1,.T.)
			oGetDados1:Refresh()

			oGetDados2:SetArray(aAux2,.T.)
			oGetDados2:Refresh()
		EndIf
	EndIf

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function DADOS1(cNumPF)
//TELA DE CIMA
	Local cQuery   := ""
	Local aRet     := {}
	Local nSldSep  := 0
	Local cDescric := ""

	IF SELECT("TMPSZR") > 0
		TMPSZR->(DbCloseArea())
	ENDIF

	cQuery := "select ZP_NUMPF,ZP_ITEM,ZP_CODBAR,ZP_PRODUTO,B1_DESC,SUM(ZP_QUANT) AS ZP_QUANT,ZP_VOLUME,ZP_TPVOL"
	cQuery += CRLF + "from "+RetSqlName("SZP")+" SZP WITH(NOLOCK) "
	cQuery += CRLF + "inner join "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) "
	cQuery += CRLF + "on  B1_COD = ZP_PRODUTO "
	cQuery += CRLF + "and SB1.D_E_L_E_T_ = '' "
	cQuery += CRLF + "where SZP.D_E_L_E_T_ = '' "
	cQuery += CRLF + "and ZP_FILIAL = '"+xFilial("SZP")+"' "
	cQuery += CRLF + "and ZP_NUMPF  = '"+cNumPF+"' "
	cQuery += CRLF + "GROUP BY SZP.ZP_NUMPF,ZP_CODBAR,ZP_PRODUTO,ZP_ITEM,B1_DESC,ZP_VOLUME,ZP_TPVOL  "
	cQuery += CRLF + "order by ZP_VOLUME "


	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSZR", .F., .T.)

	DbSelectArea("TMPSZR")

	If TMPSZR->(!EOF())
		While TMPSZR->(!EOF())
			nTotSep += TMPSZR->ZP_QUANT
			cDescric := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+TMPSZR->ZP_TPVOL,1,"")
			aAdd(aRet,{TMPSZR->ZP_ITEM,TMPSZR->ZP_CODBAR,TMPSZR->ZP_PRODUTO+" - "+TMPSZR->B1_DESC,TMPSZR->ZP_QUANT,IF(EMPTY(TMPSZR->ZP_VOLUME),SPACE(3),TMPSZR->ZP_VOLUME),IF(EMPTY(TMPSZR->ZP_TPVOL),SPACE(3),TMPSZR->ZP_TPVOL),cDescric,.F.})
			TMPSZR->(DbSkip())
		EndDo
	Else
		aAdd(aRet,{"","","",0,SPACE(3),SPACE(3),"",.F.})
	EndIf

	TMPSZR->(DbCloseArea())

Return aRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function DADOS2(cNumPF,lConf)
// TELA DE BAIXO
	Local cQuery  := ""
	Local cQuery1 := ""
	Local aRet    := {}
	Local nSldSep := 0
	Local ctabprc := Alltrim(GetMv("HP_TABPCON"))

	IF SELECT("TMPQTD") > 0
		TMPQTD->(DbCloseArea())
	ENDIF

	cQuery1 :=" SELECT SUM(ZJ_QTDSEP) AS ZJ_QTDSEP,SUM(ZJ_QTDLIB) AS ZJ_QTDLIB FROM "+RetSqlName("SZJ")+" WHERE D_E_L_E_T_ = '' and ZJ_FILIAL = '"+xFilial("SZJ")+"' AND ZJ_NUMPF = '"+cNumPF+"' "

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery1),"TMPQTD", .F., .T.)

	nTotLib := TMPQTD->ZJ_QTDLIB
	nSldSep := TMPQTD->ZJ_QTDSEP

	IF SELECT("TMPSZJ") > 0
		TMPSZJ->(DbCloseArea())
	ENDIF


	cQuery := "select ZJ_PRODUTO,B1_DESC,B1_CODBAR,SUM(ZJ_QTDLIB) AS ZJ_QTDLIB,SUM(ZJ_QTDSEP) AS ZJ_QTDSEP,ISNULL(DA1_PRCVEN,0) AS DA1_PRCVEN "
	cQuery += CRLF + "from "+RetSqlName("SZJ")+" SZJ WITH(NOLOCK) "
	cQuery += CRLF + "inner join "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) "
	cQuery += CRLF + "on  B1_COD = ZJ_PRODUTO "
	cQuery += CRLF + "and SB1.D_E_L_E_T_ = '' "
	cQuery += CRLF + "LEFT JOIN "+RetSqlName("DA1")+" DA1 WITH(NOLOCK) ON ZJ_PRODUTO = DA1_CODPRO AND DA1_CODTAB = '"+ctabprc+"' AND DA1.D_E_L_E_T_ = '' "
	cQuery += CRLF + "where SZJ.D_E_L_E_T_ = ''"
	cQuery += CRLF + "and ZJ_FILIAL = '"+xFilial("SZJ")+"' "
	cQuery += CRLF + "and ZJ_NUMPF  = '"+cNumPF+"' "
	cQuery += CRLF + "group by ZJ_PRODUTO,B1_DESC,B1_CODBAR,DA1_PRCVEN  "
	cQuery += CRLF + "order by ZJ_PRODUTO "


	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSZJ", .F., .T.)

	DbSelectArea("TMPSZJ")

	If TMPSZJ->(!EOF())
		While TMPSZJ->(!EOF())
           /*
			If !lConf
                nTotLib += TMPSZJ->ZJ_QTDLIB
			EndIf
           */
			// nSldSep := TMPSZJ->ZJ_QTDLIB-TMPSZJ->ZJ_QTDSEP
			//If nSldSep <> 0	// TODO N�o carrega o produto 100 % conferido [Ronaldo Pereira 06/09/2018]
			If TMPSZJ->ZJ_QTDLIB <> TMPSZJ->ZJ_QTDSEP //.AND.
				nSldSep := TMPSZJ->ZJ_QTDLIB-TMPSZJ->ZJ_QTDSEP
				aAdd(aRet,{TMPSZJ->B1_CODBAR,TMPSZJ->ZJ_PRODUTO+" - "+SubStr(TMPSZJ->B1_DESC,1,40),nSldSep,TMPSZJ->ZJ_QTDLIB,TMPSZJ->DA1_PRCVEN ,.F.})
			EndIf
			TMPSZJ->(DbSkip())
		EndDo
	EndIf

	If !lConf
		If nTotSep == nTotLib .And. lHabilita
			lTOk := .T.
		EndIf
	EndIf

	TMPSZJ->(DbCloseArea())

Return aRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function VALAPONT(cApto,cPreFat,cLotDAP,lSalva,lAutoS)
	Local aAux1    := aClone(oGetDados1:aCols)
	Local aAux2    := aClone(oGetDados2:aCols)
	Local aAux3    := {}
	Local nx,ny    := 0
	Local cCodBar  := SubStr(cApto,1,13)+SPACE(2)
	Local cLote    := SubStr(cApto,14,10)
	Local nPosEAN  := 0
	Local nPosEAN2  := 0
	Local cProduto := ""
	Local nQtde    := 0
	Local cChave   := cCodBar+cVolume+cTpVol
	Local nQtdLot  := 0
	Local cAmzPik  := TRB->ZJ_LOCAL
	Local cDescric := ""
	Local nLen     := 0
	Local lAtu     := .F.
	Local nTotConf := 0
	Local lContinua := .T.
	Local cBarras   := ""
	Local nPosBar   := 0
	Local cLtPd     := SuperGetMV("MV_XLOTPAD",.F.,"000000")+space(4)
	Local lRet      := .T.
	Local nSldApto  := 0
	Local nPreco    := 0
	Local i         := 0
	Local lOk       := .T.
	Local nzjatu    := 0
	Local cUpd      := ""
	Local nStatus   := 0
	Local lgraApont := .T.
	Local nerro     := 0


	If Empty(cLote)
		cLote := cLtPd
	EndIf

	// If Len(aAux1) > 0 .and. lAutoS
	If !Empty(cApto) .And. lContinua .And. !lSalva
		DbSelectArea("SB1")
		DbSetOrder(5) //B1_FILIAL+B1_CODBAR
		If DbSeek(xFilial("SB1")+cCodBar)
			If SB1->B1_LOCALIZ == "S"
				DbSelectArea("SB8")
				DbSetOrder(3)
				If DbSeek(xFilial("SB8")+SB1->B1_COD+cAmzPik+cLote)
					nPosEAN := Ascan(aAux2,{|x| x[1] = cCodBar})

					If nPosEAN == 0
						nPosEAN2 := Ascan(aAux1,{|x| x[2] = cCodBar})
						lOk := .F.
					EndIf

					If lOk .AND. lRet
						nSldApto := aAux2[nPosEAN][3] //Saldo a apontar
						nPreco   := aAux2[nPosEAN][5]
						If nSldApto > 0
							cProduto := SB1->B1_COD +" - "+SB1->B1_DESC
							cDescric := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+cTpVol,1,"")
							If Len(aAux1) = 1 .And. Empty(aAux1[1][2])
								aAux1[1][1] := "01"
								aAux1[1][2] := cCodBar
								aAux1[1][3] := cProduto
								aAux1[1][4] := 1
								aAux1[1][5] := Alltrim(cVolume)
								aAux1[1][6] := cTpVol
								aAux1[1][7] := cDescric
								aAux1[1][8] := .F.
							Else
								nPosChv := Ascan(aAux1,{|x| x[2]+x[5]+x[6] == cChave})
								If nPosChv > 0
									nQtde := aAux1[nPosChv][4]
									aAux1[nPosChv][4] := nQtde + 1

								Else
									aAdd(aAux1,{SOMA1(aAux1[len(aAux1)][1],2),cCodBar,cProduto,1,cVolume,cTpVol,cDescric,.F.})
								EndIf

							EndIf
                            /*
                            nTotSep := nTotSep + 1
                            aAux2[nPosEAN][3] := nSldApto - 1
                            */
							// TODO N�o carrega o produto 100 % conferido [Weskley Silva 06/09/2018]
							If aAux2[nPosEAN][3] = 0
								aDel(aAux2,nPosEAN)
								aSize(aAux2,Len(aAux2)-1)
								nLen := nLen - 1
							EndIf


							oGetDados2:SetArray(aAux2,.T.)
							oGetDados2:Refresh()

							oApont:SetFocus()
						Else
							MessageBox("Produto j� foi 100% conferido!","Aten��o",48)
							lgraApont := .F.
						EndIf

					Elseif nPosEAN2 > 0
						MessageBox("Produto j� foi 100% conferido!","Aten��o",48)
						lgraApont := .F.
					Else
						MessageBox("Produto n�o pertence a este Pr�-Faturamento !","Aten��o",16)
						lgraApont := .F.
					EndIf
				Else
					MessageBox("Lote n�o encontrado para o produto!","Aten��o",48)
					lgraApont := .F.
				EndIf
			ElseIf SB1->B1_LOCALIZ == "N" //ADICIONADO CONDI��O PARA PRODUTOS QUE NAO CONTROLAM ENDERE�O INICIO GEYSON 16/09/19
				nPosEAN := Ascan(aAux2,{|x| x[1] = cCodBar})

				If nPosEAN == 0
					nPosEAN2 := Ascan(aAux1,{|x| x[2] = cCodBar})
					lOk := .F.
				EndIf

				If lOk .AND. lRet
					nSldApto := aAux2[nPosEAN][3] //Saldo a apontar
					nPreco   := aAux2[nPosEAN][5]
					If nSldApto > 0
						cProduto := SB1->B1_COD +" - "+SB1->B1_DESC
						cDescric := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+cTpVol,1,"")
						If Len(aAux1) = 1 .And. Empty(aAux1[1][2])
							aAux1[1][1] := "01"
							aAux1[1][2] := cCodBar
							aAux1[1][3] := cProduto
							aAux1[1][4] := 1
							aAux1[1][5] := Alltrim(cVolume)
							aAux1[1][6] := cTpVol
							aAux1[1][7] := cDescric
							aAux1[1][8] := .F.
						Else
							nPosChv := Ascan(aAux1,{|x| x[2]+x[5]+x[6] == cChave})
							If nPosChv > 0
								nQtde := aAux1[nPosChv][4]
								aAux1[nPosChv][4] := nQtde + 1
							Else
								aAdd(aAux1,{SOMA1(aAux1[len(aAux1)][1],2),cCodBar,cProduto,1,cVolume,cTpVol,cDescric,.F.})
							EndIf
						EndIf
                        /*
                        nTotSep := nTotSep + 1
                        aAux2[nPosEAN][3] := nSldApto - 1
                      
                        // TODO N�o carrega o produto 100 % conferido [Weskley Silva 06/09/2018]
						If aAux2[nPosEAN][3] = 0
                            aDel(aAux2,nPosEAN)
                            aSize(aAux2,Len(aAux2)-1)
                            nLen := nLen - 1
						EndIf
                         
                        oGetDados2:SetArray(aAux2,.T.)
                        oGetDados2:Refresh()
                         */
						oApont:SetFocus()
					Else
						MessageBox("Produto j� foi 100% conferido!","Aten��o",48)
						lgraApont := .F.
					EndIf

				Elseif nPosEAN2 > 0
					MessageBox("Produto j� foi 100% conferido!","Aten��o",48)
					lgraApont := .F.
				Else
					MessageBox("Produto n�o pertence a este Pr�-Faturamento !","Aten��o",16)
					lgraApont := .F.
				EndIf
			EndIf //ADICIONADO CONDI��O PARA PRODUTOS QUE NAO CONTROLAM ENDERE�O FIM GEYSON 16/09/19
		Else
			MessageBox("C�digo de barras nao encontrado!","Aten��o",48)
			lgraApont := .F.
		EndIf
	Else
		If Len(AllTrim(cApto)) > 0
			MessageBox("Apontamento com 13 digitos ou menos","Aten��o",48)
			lgraApont := .F.
		EndIf
	EndIf

	nx := Ascan(aAux1,{|x| x[2] = cCodBar})//len(aAux1)

	If  lgraApont .AND. Len(AllTrim(cApto)) > 0
		//BEGIN TRANSACTION
		latu := GravApont(cPreFat,nx,aAux1,aAux2,cApto,nSldApto,cLotDAP,nTotSep,nPosEAN)
		If latu
			nTotSep := nTotSep + 1
			aAux2[nPosEAN][3] := nSldApto - 1

			If aAux2[nPosEAN][3] = 0
				aDel(aAux2,nPosEAN)
				aSize(aAux2,Len(aAux2)-1)
				nLen := nLen - 1
			EndIf
			oGetDados2:SetArray(aAux2,.T.)
			oGetDados2:Refresh()

			If lImpAut//Marcio
				HPPRTETQ(cCodBar,cPreFat,nPreco)
			EndIf
		ElseIf !latu
			//  MessageBox("N�o foi poss�vel bipar, FAVOR TENTAR NOVAMENTE","ANTEN��O",16)
			nposdel:= Len(aAux1)
			aDel(aAux1,nposdel)
		EndIf
		//  END TRANSACTION
	EndIf
	If latu
		oGetDados1:SetArray(aAux1,.T.)
		oGetDados1:Refresh()

		oGetDados2:SetArray(aAux2,.T.)
		oGetDados2:Refresh()
	EndIF
	If nTotSep == nTotLib
		lTOk := .T.
	Else
		lTOk := .F.
	EndIf

	cApont := SPACE(23)

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function SALVAAPTO(cNumPF)
	Local cQuery   := ""
	Local lRet     := .T.
	Local nSldDist := 0
	Local aAux1    := aClone(oGetDados1:aCols)
	Local aAux2    := aClone(oGetDados2:aCols)
	Local nx       := 0
	Local lCont    := .T.
	Local cPedido  := ""

	//lCont := VALAPONT(cApont,TRB->ZJ_NUMPF,TRB->ZJ_LOTDAP,.F.,.F.)

//DbSelectArea("SC5")
//DbSetOrder(1)
//DbSeek(xfilial("SC5")+SubStr(cNumPF,1,6))

//If SC5->C5_XBLQ <> "L" .and. !lConf
//	MsgAlert("Pedido com Bloqueio Comercial.","Aten��o")
//	Return
//Endif
    /*
	If !lCont
        Return
	EndIf
    */
	IF SELECT("TMPAPTO") > 0
		TMPAPTO->(DbCloseArea())
	ENDIF

	cQuery := "SELECT ZP_NUMPF,ZP_PEDIDO,ZP_PRODUTO,SUM(ZP_QUANT) AS ZP_QUANT"
	cQuery += CRLF + "FROM "+RetSqlName("SZP")+" SZP WITH(NOLOCK)  "
	cQuery += CRLF + "WHERE SZP.D_E_L_E_T_ = '' "
	cQuery += CRLF + "AND ZP_FILIAL = '"+xFilial("SZP")+"' "
	cQuery += CRLF + "AND ZP_NUMPF  = '"+cNumPF+"' "
	cQuery += CRLF + "GROUP BY ZP_NUMPF,ZP_PEDIDO,ZP_PRODUTO "
	cQuery += CRLF + "ORDER BY ZP_NUMPF,ZP_PRODUTO "

	MemoWrite("HPCPA003_SALVAAPTO.txt",cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPAPTO", .F., .T.)

	DbSelectArea("TMPAPTO")

	If TMPAPTO->(!EOF())
		nRet3 := MessageBox("Confirma confer�ncia?","Confirma��o",4)
		If nRet3 == 6
			//TODO WESKLEY SILVA 01/08/2018 [Begin transaction para garantir a efetiva��o do pedido]
			Begin Transaction

				cPedido := TMPAPTO->ZP_PEDIDO
				While TMPAPTO->(!EOF())
					DbSelectArea("SZJ")
					DbSetOrder(3)
					If DbSeek(xFilial("SZJ")+TMPAPTO->ZP_NUMPF+TMPAPTO->ZP_PRODUTO)
						While SZJ->(!EOF()) .And. SZJ->ZJ_PRODUTO = TMPAPTO->ZP_PRODUTO //.And. nSldDist > 0
							If lTOk .and. !lConf
								RecLock("SZJ",.F.)
								SZJ->ZJ_CONF := "S"
								MsUnlock()
								nSldDist := 0
							EndIf
							SZJ->(DbSkip())
						EndDo
					EndIf
					TMPAPTO->(DbSkip())
				EndDo

				//TODO Fun��o para gravar como falta o saldo pendente. Ronaldo Pereira 13/09/19
				GravaFalta(cNumPF)

				DbSelectArea("TRB")
				If lTOk .and. !lConf
					lValid := .F.
					LIBERA(cNumPF,cPedido,lValid)
					RecLock("TRB",.F.)
					TRB->CONFERIDO := "Sim"
					TRB->EMCONFERE := "Nao"
					TRB->EFETIVADO := "Sim"
					MsUnlock()
				Else
					RecLock("TRB",.F.)
					TRB->EMCONFERE := "Sim"
					MsUnlock()
				EndIf

			End Transaction
		Else
			lRet := .F.
		EndIf

	Else
		MessageBox("N�o foi realizado nenhum apontamento.","Aten��o",48)
		lRet := .F.
	EndIf

	TMPAPTO->(DbCloseArea())

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function LIBERA(cNumPF,cPedido,lValid)
	Local cQuery  := ""
	Local nQtdLib := 0
	Local nQtdRes := 0
	Local lValid := .T.
	Local cAmzPik := TRB->ZJ_LOCAL //AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
	Local nTotPed := 0
	Local nTotLib := 0
	Local nLib    := 0

	IF SELECT("TMPLIB") > 0
		TMPLIB->(DbCloseArea())
	ENDIF

	cQuery := "select ZJ_NUMPF,ZJ_PEDIDO,ZJ_ITEM,ZJ_ITEMGRD,ZJ_PRODUTO,B1_CODBAR,SUM(ZJ_QTDSEP) AS ZJ_QTDSEP,ZJ_LOCAL "
	cQuery += CRLF + "from "+RetSqlName("SZJ")+" SZJ WITH (NOLOCK) "
	cQuery += CRLF + "inner join "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) "
	cQuery += CRLF + "on B1_COD = ZJ_PRODUTO "
	cQuery += CRLF + "and SB1.D_E_L_E_T_ = '' "
	cQuery += CRLF + "where SZJ.D_E_L_E_T_ = '' "
	cQuery += CRLF + "and ZJ_FILIAL = '"+xFilial("SZJ")+"' "
	cQuery += CRLF + "and ZJ_NUMPF  = '"+cNumPF+"' "
	cQuery += CRLF + "group by ZJ_NUMPF,ZJ_PEDIDO,ZJ_ITEM,ZJ_ITEMGRD,ZJ_PRODUTO,B1_CODBAR,ZJ_LOCAL "
	cQuery += CRLF + "order by ZJ_PEDIDO,ZJ_ITEM,ZJ_ITEMGRD,ZJ_PRODUTO,ZJ_LOCAL "

	MemoWrite("HESTA003_LIBERA.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPLIB", .F., .T.)

	DbSelectArea("SC6")
	DbSetOrder(1)
	If DbSeek(xFilial("SC6")+cPedido)
		While SC6->(!EOF()) .And. SC6->C6_NUM = cPedido
			nTotPed += SC6->C6_QTDVEN
			SC6->(DbSkip())
		EndDo
	EndIf

	DbSelectArea("TMPLIB")

	lProd := .T.

	_aprode := {}

	If TMPLIB->(!EOF())
		Begin Transaction
			While TMPLIB->(!EOF())
				DbSelectArea("SC6")
				DbSetOrder(1)
				If DBSeek(xFilial("SC6")+TMPLIB->ZJ_PEDIDO+TMPLIB->ZJ_ITEM+TMPLIB->ZJ_PRODUTO)
					If POSICIONE("SB1", 1, xFilial("SB1") + TMPLIB->ZJ_PRODUTO, "B1_LOCALIZ") == "S"
						aEmpenho := GERAARRAY(TMPLIB->ZJ_NUMPF,TMPLIB->B1_CODBAR,TMPLIB->ZJ_QTDSEP,TMPLIB->ZJ_PRODUTO)
						If Len(aEmpenho) > 0

							//Essa funcao MaLibDoFat e padrao. Ela que gera o SC9.
							//nQtdLib := MaLibDoFat(SC6->(RecNo()),TMPLIB->ZJ_QTDSEP,.F.,.F.,.T.,.T.,.T.,.F.,,,)//aEmpenho)
							nQtdLib := MaLibDoFat(SC6->(RecNo()),TMPLIB->ZJ_QTDSEP,.F.,.F.,.T.,.T.,.F.,.T.)
							// nQtdLib := MaLibDoFat(SC6->(RecNo()),TMPLIB->ZJ_QTDSEP,.T.,.T.)

							DbSelectArea("SB2")
							DbSetOrder(2)
							If DbSeek(xFilial("SB2")+SC6->C6_LOCAL+SC6->C6_PRODUTO)
								nQtdRes := SB2->B2_XRESERV

								DbSelectArea("SZJ")
								DbSetOrder(3)
								DbSeek(xFilial("SZJ")+TMPLIB->ZJ_NUMPF+TMPLIB->ZJ_PRODUTO+TMPLIB->ZJ_ITEM)

								If EMPTY(SC9->C9_BLEST)
									RecLock("SB2",.F.)
									SB2->B2_XRESERV := IF((nQtdRes - nQtdLib) < 0,0,(nQtdRes - nQtdLib))
									MsUnlock()
								ElseIf SC9->C9_BLEST != "10"
									RecLock("SZJ",.F.)
									SZJ->ZJ_BLEST := "02"
									MsUnlock()
								EndIf
							EndIf
						EndIf

					ElseIf POSICIONE("SB1", 1, xFilial("SB1") + TMPLIB->ZJ_PRODUTO, "B1_LOCALIZ") == "N"

						//Essa funcao MaLibDoFat e padrao. Ela que gera o SC9.
						//nQtdLib := MaLibDoFat(SC6->(RecNo()),TMPLIB->ZJ_QTDSEP,.F.,.F.,.T.,.T.,.T.,.F.,,,)//aEmpenho)
						//nQtdLib := MaLibDoFat(SC6->(RecNo()),TMPLIB->ZJ_QTDSEP,.T.,.T.)
						nQtdLib := MaLibDoFat(SC6->(RecNo()),TMPLIB->ZJ_QTDSEP,.F.,.F.,.T.,.T.,.F.,.T.)

						DbSelectArea("SB2")
						DbSetOrder(2)
						If DbSeek(xFilial("SB2")+SC6->C6_LOCAL+SC6->C6_PRODUTO)
							nQtdRes := SB2->B2_XRESERV

							DbSelectArea("SZJ")
							DbSetOrder(3)
							DbSeek(xFilial("SZJ")+TMPLIB->ZJ_NUMPF+TMPLIB->ZJ_PRODUTO+TMPLIB->ZJ_ITEM)

							If EMPTY(SC9->C9_BLEST)
								RecLock("SB2",.F.)
								SB2->B2_XRESERV := IF((nQtdRes - nQtdLib) < 0,0,(nQtdRes - nQtdLib))
								MsUnlock()
							ElseIf SC9->C9_BLEST != "10"
								RecLock("SZJ",.F.)
								SZJ->ZJ_BLEST := "02"
								MsUnlock()
							EndIf
						EndIf

						DbSelectArea("SZP")
						DbSetOrder(1)
						If DbSeek(xFilial("SZP")+cNumPF+TMPLIB->B1_CODBAR)
							While SZP->(!EOF()) .And. SZP->ZP_NUMPF+SZP->ZP_CODBAR = cNumPF+TMPLIB->B1_CODBAR
								nLib := SZP->ZP_QUANT
								RecLock("SZP",.F.)
								SZP->ZP_QTDLIB := SZP->ZP_QTDLIB + nLib
								SZP->ZP_CONFERI := "F"
								MsUnlock()
								SZP->(DbSkip())
							EndDo

						EndIf
						nTotLib += nQtdLib
					Else
						MessageBox("Falha na libera��o de estoque do produto: "+TMPLIB->ZJ_PRODUTO,"Aten��o",48)
						Aadd(_aprode,TMPLIB->ZJ_PRODUTO)
						lProd := .F.
						lValid := .F.
					EndIf
					/*
					DbSelectArea("SB2")
					DbSetOrder(2)
					If DbSeek(xFilial("SB2")+SC6->C6_LOCAL+SC6->C6_PRODUTO)
						nQtdRes := SB2->B2_XRESERV

						RecLock("SB2",.F.)
						SB2->B2_XRESERV := IF((nQtdRes - nQtdLib) < 0,0,(nQtdRes - nQtdLib))
						MsUnlock()
					EndIf
					*/
				Else
					MessageBox("Falha na libera��o de estoque do produto: "+TMPLIB->ZJ_PRODUTO,"Aten��o",48)
					Aadd(_aprode,TMPLIB->ZJ_PRODUTO)
					lProd := .F.
					lValid := .F.
				EndIf

				TMPLIB->(DbSkip())
			EndDo
			If nTotLib > 0
				cBloq := ""
				DbSelectArea("SC5")
				DbSetOrder(1)
				If DbSeek(xFilial("SC5")+cPedido)
					cBloq := SC5->C5_XBLQ
					If nTotPed = nTotLib
						If lProd
							RecLock("SC5",.F.)
							SC5->C5_LIBEROK := "S"
							MsUnlock()
						Endif
					EndIf
				EndIf
				DbSelectArea("SC9")
				DbSetOrder(11)
				If DbSeek(xFilial("SC9")+cPedido+cNumPF)
					While SC9->(!EOF()) .And. SC9->C9_PEDIDO = cPedido //.And. SC9->C9_XNUMPF = " "
						If Empty(SC9->C9_NFISCAL) .And. Empty(SC9->C9_XNUMPF)
							RecLock("SC9",.F.)
							SC9->C9_XBLQ := cBloq
							SC9->C9_XNUMPF := cNumPF
							MsUnlock()
						EndIf
						SC9->(DbSkip())
					EndDo
				EndIf
			EndIf
		End Transaction
	EndIf

	TMPLIB->(DbCloseArea())

	IF !lprod
		U_H003EXC(2,_aprode)
	Endif

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function GERAARRAY(cNumPF,cCodBar,nQtdALib,cProd)
	Local aRet     := {}
	Local cQuery   := ""
	Local cAmzPik  := TRB->ZJ_LOCAL //AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
	Local dDtValid := DDATABASE
	Local nQtdLot  := 0

	DbSelectArea("SZP")
	DbSetOrder(1)
	If DbSeek(xFilial("SZP")+cNumPF+cCodBar)
		While SZP->(!EOF()) .And. SZP->ZP_NUMPF+SZP->ZP_CODBAR = cNumPF+cCodBar
			nQtdLot := SZP->ZP_QUANT

			DbSelectArea("SB8")
			DbSetOrder(3)
			If DbSeek(xFilial("SB8")+SZP->ZP_PRODUTO+TMPLIB->ZJ_LOCAL+SZP->ZP_LOTECTL)
				While SB8->(!EOF()) .And. SB8->B8_PRODUTO+SB8->B8_LOCAL+SB8->B8_LOTECTL = SZP->ZP_PRODUTO+TMPLIB->ZJ_LOCAL+SZP->ZP_LOTECTL

					nLib := nQtdLot

					// TODO WESKLEY 10/05 VERIFICA��O DO SALDO

					//If nQtdLot > 0 .And. SB8->B8_SALDO > 0
					//	If nQtdLot <= SB8->B8_SALDO
					//	nLib := nQtdLot
					//Else
					//	nLib := SB8->B8_SALDO
					//	EndIf
					//LOTECTL,NUMLOTE,,,QTDELIB,QTDLIB2,DTVALID,,,,POTENCIA


					aAdd(aRet,{SB8->B8_LOTECTL,SB8->B8_NUMLOTE,"","",nLib,0,SB8->B8_DTVALID,"","","",SB8->B8_LOCAL,0})

					nQtdLot := nQtdLot - nLib

					RecLock("SZP",.F.)
					SZP->ZP_QTDLIB := SZP->ZP_QTDLIB + nLib
					SZP->ZP_CONFERI := "F"
					MsUnlock()



					SB8->(DbSkip())

				EndDo
			EndIf



			SZP->(DbSkip())
		EndDo
	EndIf

Return aRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HESTSAI02 �Autor  �Bruno Parreira      � Data �  23/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Pergunta para confirmacao de saida da tela de transferencia ���
���          �pendente.                                                   ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/    

Static Function HESTSAI03()
	// FUN��O CHAMADA AO SAIR DA ROTINA DE BIPAGEM
	Local lRet := .F.

	nRet3 := MessageBox("Deseja realmente sair?","Confer�ncia",4)
	If nRet3 == 6
		lRet := .T.
		SALVAPTB(TRB->ZJ_NUMPF)
	Else
		MessageBox("Cancelado pelo usu�rio","Aten��o",16)
	EndIf

Return lRet

Static Function HESTSBI03()
	// FUN��O CHAMADA AO SAIR DA ROTINA DE EXCLUS�O DE PE�A. (VISUALPTO)
	Local lRet := .F.

	nRet3 := MessageBox("Deseja realmente sair?","Confer�ncia",4)
	If nRet3 == 6
		lRet := .T.
		SALVAPTB(TRB->ZJ_NUMPF)
		U_H003NEW()
	Else
		MessageBox("Cancelado pelo usu�rio","Aten��o",16)
	EndIf

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HESTLEG01 �Autor  �Bruno Parreira      � Data �  23/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Legenda da tela de transferencias pendentes.                ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function H003LGEST()

	Local aCores := {}

//BR_AMARELO,BR_AZUL,BR_BRANCO,BR_CINZA,BR_LARANJA,BR_MARROM,BR_VERDE,BR_VERMELHO,BR_PINK,BR_PRETO,BR_VIOLETA

	aAdd(aCores,{"BR_VERMELHO","Transferencia em aberto" })
	aAdd(aCores,{"BR_VERDE"   ,"Apto a Conferir" })
	aAdd(aCores,{"BR_AMARELO" ,"Em Conferencia" })
	aAdd(aCores,{"BR_AZUL"    ,"Conferido" })
	aAdd(aCores,{"BR_PRETO"   ,"Faturado" })

	BrwLegenda("Conferencia","Legenda",aCores)

Return .T.


//_____________________________________________________________________________
/*/{Protheus.doc} Imprimir Faltas
Rotina para Imprimir a rela��o de pe�as que falta conferir;

@author Ronaldo Pereira
@since 12 de Mar�o de 2019
@version V.01
/*/
//_____________________________________________________________________________

User Function IMPFALTA(cNumPF)
	Local cDesc1       := "Este programa tem como objetivo imprimir relatorio "
	Local cDesc2       := "de acordo com os parametros informados pelo usuario."
	Local cDesc3       := "Rela��o de Faltas da Confer�ncia"
	Local cPict        := ""
	Local titulo       := "Faltas"
	Local nLin         := 600
	//0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
	//012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
	Local Cabec1       := ""//"PRODUTO           FALTA        ENDERECO            ESTACAO"
	Local Cabec2       := ""
	Local imprime      := .T.
	Local aOrd         := {}
	Private cNumPF
	//  1   2   3   4   5   6   7   8   9  10  11  12  13  14  // {000,010,020,035,091,105,110,130,150}
	Private aCol       := {080,500,1000,1500} //{000,020,030,055}
	Private lEnd       := .F.
	Private lAbortPrint:= .F.
	Private CbTxt      := ""
	Private limite     := 132//220
	Private tamanho    := "M"
	Private nomeprog   := "IMPFALTA"
	Private nTipo      := 15
	Private aReturn    := {"Zebrado",1,"Administracao", 2, 2, 1, "",0 }
	Private nLastKey   := 0
	Private cbtxt      := Space(10)
	Private cbcont     := 00
	Private CONTFL     := 01
	Private m_pag      := 01
	Private wnrel      := "IMPFALTA"
	Private cPerg      := 'IMPFALTA'
	Private cString    := "SZJ"
	Private cArqTEMP
	Private Titulo 	:= "Relat�rio de Faltas: "+cNumPF


	wnrel := SetPrint(cString,NomeProg,,@titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.T.,Tamanho,,.F.)

	If nLastKey == 27
		Return
	Endif

	oPrn 	 := TMSPrinter():New()

	Processa({||RunReport(cNumPF)}, Titulo, "Imprimindo, aguarde...")

	If( aReturn[5] == 1 ) //1-Disco, 2-Impressora
		oPrn:Preview()
	Else
		oPrn:Setup()
		oPrn:Print()
	EndIf

	MS_FLUSH()


Return

//Descricao Busca registros no banco e gera relatorio 
Static Function RunReport(cNumPF)
	Local cQuery   := ""
	Local nTotReg  := 0
	Local nContar  := 0
	Local nCont    := 0
	Local nLin     := 700
	Local nPula    := 60
	Private oFont09  := TFont():New("Arial",,10,,.f.,,,,,.f.)

	If Select("TBF") > 0
		TBF->(DbCloseArea())
	EndIf

	cQuery := " SELECT (SELECT CONVERT(CHAR, CAST(MAX(ZP_DATA) AS SMALLDATETIME), 103)"
	cQuery += CRLF + "       FROM SZP010 AS ZP WITH (NOLOCK)"
	cQuery += CRLF + "       WHERE ZP.D_E_L_E_T_ <> '*'"
	cQuery += CRLF + "       AND ZP.ZP_NUMPF = ZJ_NUMPF) AS DATA,"
	cQuery += CRLF + "       ZJ_PEDIDO AS PEDIDO,"
	cQuery += CRLF + "       A1.A1_NOME AS CLIENTE,"
	cQuery += CRLF + "       ZJ_NUMPF AS PREFAT,"
	cQuery += CRLF + "       ZJ_PRODUTO AS PRODUTO,"
	cQuery += CRLF + "       (ZJ_QTDLIB - ZJ_QTDSEP) AS FALTA,"
	cQuery += CRLF + "       ZJ_LOCALIZ AS ENDERECO,"
	cQuery += CRLF + "       BE_XCODEST AS ESTACAO,"
	cQuery += CRLF + "  (SELECT TOP 1 ZP_USR "
	cQuery += CRLF + "   FROM SZP010 AS ZP WITH (NOLOCK) "
	cQuery += CRLF + "   WHERE ZP.D_E_L_E_T_ <> '*' "
	cQuery += CRLF + "     AND ZP.ZP_NUMPF = ZJ_NUMPF) AS CONFERENTE "
	cQuery += CRLF + "FROM SZJ010 (NOLOCK) "
	cQuery += CRLF + "RIGHT  JOIN SBE010 AS BE WITH (NOLOCK) ON (BE_LOCALIZ = ZJ_LOCALIZ "
	cQuery += CRLF + "                                           AND BE.D_E_L_E_T_ <> '*') "
	cQuery += CRLF + "INNER JOIN SA1010 AS A1 WITH (NOLOCK) ON (A1.A1_COD = ZJ_CLIENTE "
	cQuery += CRLF + "                                          AND A1.D_E_L_E_T_ <> '*') "
	cQuery += CRLF + "WHERE SZJ010.D_E_L_E_T_ <> '*' "
	cQuery += CRLF + "  AND ZJ_QTDLIB <> ZJ_QTDSEP "
	cQuery += CRLF + "  AND ZJ_NUMPF = '"+cNumPF+"' "
	cQuery += CRLF + "GROUP BY ZJ_PEDIDO, "
	cQuery += CRLF + "         A1.A1_NOME, "
	cQuery += CRLF + "         ZJ_NUMPF, "
	cQuery += CRLF + "         ZJ_PRODUTO,"
	cQuery += CRLF + "		 (ZJ_QTDLIB - ZJ_QTDSEP), "
	cQuery += CRLF + "		 ZJ_LOCALIZ,"
	cQuery += CRLF + "         BE_XCODEST"
	cQuery += CRLF + "ORDER BY BE_XCODEST"

	MemoWrite("IMPFALTA.TXT",cQuery)


	DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),"TBF",.F.,.T.)

	DbSelectArea("TBF")
	TBF->(DbEval({ || nTotReg++ },,{|| !Eof()}))
	TBF->(DbGoTop())

	cNomeConf := UsrRetName(TBF->CONFERENTE)

	Cabec()
	nLin := 700

	While TBF->(!EOF())
		IncProc("Gerando relatorio, regs : "+StrZero(++nContar,6)+" de "+StrZero(nTotReg,6))

		oPrn:Say(nLin,aCol[1],TBF->PRODUTO,oFont09,100)
		oPrn:Say(nLin,aCol[2],cValtoChar(TBF->FALTA),oFont09,100)
		oPrn:Say(nLin,aCol[3],TBF->ENDERECO,oFont09,100)
		oPrn:Say(nLin,aCol[4],TBF->ESTACAO,oFont09,100)

		nLin += nPula

		TBF->(DbSkip())
	EndDo

	//@ nLin,000 PSay __PrtThinLine()
	nLin++
	oPrn:EndPage()

	TBF->(DbCloseArea())

Return

Static Function Cabec()
	Local aArea := GetArea()
	Local nx    := 0
	Local cEmp  := ""
	Local Titulo := "Relat�rio de Falta"
	Private	nfim    := 2400
	Private	oFont14	 := TFont():New("Arial",,14,,.f.,,,,,.f.)
	Private oFont14n := TFont():New("Arial",,14,,.t.,,,,,.f.)
	Private oFont09  := TFont():New("Arial",,10,,.f.,,,,,.f.)
	Private oFont09n := TFont():New("Arial",,10,,.t.,,,,,.f.)
	Private oFont10  := TFont():New("Arial",,10,,.f.,,,,,.f.)
	Private oFont10n := TFont():New("Arial",,10,,.t.,,,,,.f.)
	Private oFont12  := TFont():New("Arial",,12,,.f.,,,,,.f.)
	Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)
	nLin	:= 100
	nCol	:= 60
	nPula	:= 60
	nEsp    := 350
	nPosTit := 60

	aColCab := {80,1000,2000}
	aTit    := {"Produto","Falta","Endere�o","Esta��o"}
	aColTit := AClone(aCol)

//oPrn:EndPage()
	oPrn:StartPage()

	oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
	nLin += nPula

	oPrn:Say(nLin,aColCab[3]-50,dtoc(date())+" - "+Time(),oFont10,100)

	nLin += nPula*2

	oPrn:Say(nLin,aColCab[1],"Pr�-Faturamento:",oFont10n,100)
	oPrn:Say(nLin,aColCab[1]+nEsp,TBF->PREFAT,oFont10,100)
	nLin += nPula
	oPrn:Say(nLin,aColCab[1],"Pedido:",oFont10n,100)
	oPrn:Say(nLin,aColCab[1]+nEsp,TBF->PEDIDO,oFont10,100)
	MSBAR('CODE128',2.3,15.0,TBF->PREFAT,oPrn,.F.,,.T.,0.040,1.1,,,,.F.)
	nLin += nPula
	oPrn:Say(nLin,aColCab[1],"Cliente:",oFont10n,100)
	oPrn:Say(nLin,aColCab[1]+nEsp,TBF->CLIENTE,oFont10,100)
	oPrn:Say(nLin,aColCab[3],TBF->PREFAT,oFont14n,100)
	nLin += nPula
	oPrn:Say(nLin,aColCab[1],"Data Conf.:",oFont10n,100)
	oPrn:Say(nLin,aColCab[1]+nEsp,TBF->DATA,oFont10,100)
	nLin += nPula
	oPrn:Say(nLin,aColCab[1],"Conferente:",oFont10n,100)
	oPrn:Say(nLin,aColCab[1]+nEsp,cNomeConf,oFont10,100)


	nLin += nPula
	nLin += nPula

	oPrn:Box(nLin-10,50,nLin+50,nfim)

	For nx := 1 to Len(aTit)
		oPrn:Say(nLin,aColTit[nx],aTit[nx],oFont10n,100)
	Next

	nLin += nPula
	imprp	:= .T.

	RestArea(aArea)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Ronaldo Pereira     � Data �  23/09/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �Grava Falta saldo Pendente.                                 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function GravaFalta(cNumPF)
	Local cQryFlt   := ""

	If SELECT("TMPFLT") > 0
		TMPFLT->(DbCloseArea())
	EndIf

	cQryFlt := "SELECT ZJ_NUMPF, ZJ_PRODUTO, ZJ_QTDLIB, ZJ_QTDSEP, (ZJ_QTDLIB - ZJ_QTDSEP) AS SALDO "
	cQryFlt += CRLF + "FROM "+RetSqlName("SZJ")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZJ_FILIAL = '"+xFilial("SZJ")+"' AND ZJ_NUMPF = '"+cNumPF+"' "
	cQryFlt += CRLF + "GROUP BY ZJ_NUMPF, ZJ_PRODUTO, ZJ_QTDLIB, ZJ_QTDSEP "

	MemoWrite("HESTA003_GravaFalta.txt",cQryFlt)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQryFlt),"TMPFLT", .F., .T.)

	DbSelectArea("TMPFLT")

	While TMPFLT->(!EOF())
		DbSelectArea("SZJ")
		DbSetOrder(3)
		If DbSeek(xFilial("SZJ")+TMPFLT->ZJ_NUMPF+TMPFLT->ZJ_PRODUTO)
			If lGrvFalta .And. TMPFLT->SALDO > 0
				RecLock("SZJ",.F.)
				SZJ->ZJ_FALTA := "F"
				MsUnlock()
			Else
				RecLock("SZJ",.F.)
				SZJ->ZJ_FALTA := " "
				MsUnlock()
			EndIf
			SZJ->(DbSkip())
		EndIf
		TMPFLT->(DbSkip())
	EndDo

	TMPFLT->(DbCloseArea())

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data �  05/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria perguntas da rotina.                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

	Local aAreaAtu	:= GetArea()
	Local aAreaSX1	:= SX1->( GetArea() )

	PutSx1(cPerg,"01","Mostra j� Faturados ?","","","Mv_ch1","N",1,0,2,"C","","","","N","Mv_par01","Sim","","","","Nao","","","","","","","","","","","","","","","","","","",{"",""},{""},{""},"")

	RestArea( aAreaSX1 )
	RestArea( aAreaAtu )

Return(cPerg)

Static Function SALVAPTB(cNumPF)
	Local cQuery   := ""
	Local lRet     := .T.
	Local nSldDist := 0
	Local aAux1    := aClone(oGetDados1:aCols)
	Local aAux2    := aClone(oGetDados2:aCols)
	Local nx       := 0
	Local lCont    := .F.
	Local cPedido  := ""

	IF SELECT("TMPAPTO") > 0
		TMPAPTO->(DbCloseArea())
	ENDIF

	cQuery := "SELECT ZP_NUMPF,ZP_PEDIDO,ZP_PRODUTO,SUM(ZP_QUANT) AS ZP_QUANT"
	cQuery += CRLF + "FROM "+RetSqlName("SZP")+" SZP WITH(NOLOCK)  "
	cQuery += CRLF + "WHERE SZP.D_E_L_E_T_ = '' "
	cQuery += CRLF + "and ZP_FILIAL = '"+xFilial("SZP")+"' "
	cQuery += CRLF + "and ZP_NUMPF  = '"+cNumPF+"' "
	cQuery += CRLF + "group by ZP_NUMPF,ZP_PEDIDO,ZP_PRODUTO "
	cQuery += CRLF + "order by ZP_NUMPF,ZP_PRODUTO "

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPAPTO", .F., .T.)

	DbSelectArea("TMPAPTO")

	If TMPAPTO->(!EOF())

		cPedido := TMPAPTO->ZP_PEDIDO
		While TMPAPTO->(!EOF())
			DbSelectArea("SZJ")
			DbSetOrder(3)
			If DbSeek(xFilial("SZJ")+TMPAPTO->ZP_NUMPF+TMPAPTO->ZP_PRODUTO)
				nSldDist := TMPAPTO->ZP_QUANT
				While SZJ->(!EOF()) .And. SZJ->ZJ_PRODUTO = TMPAPTO->ZP_PRODUTO //.And. nSldDist > 0
					If lTOk .and. !lConf
						RecLock("SZJ",.F.)
						SZJ->ZJ_CONF := "S"
						MsUnlock()
					EndIf
					SZJ->(DbSkip())
				EndDo
			EndIf
			TMPAPTO->(DbSkip())
		EndDo

		lRet := .F.

	Else
		lRet := .F.
	EndIf

	TMPAPTO->(DbCloseArea())

	oGetDados2:ForceRefresh()

	oGetDados1:ForceRefresh()
Return lRet

User Function H003NEW()
	local nX
	Local nI
	Local lcontinua := .T.

	Private oDlg
	Private oGetDados
	Private nOpc      := 0
	Private nStr      := 0
	Private nPos      := 0
	Private aColAux1  := {}
	Private aColAux2  := {}
	Private lRefresh  := .T.
	Private aHeader1  := {}
	Private aCols1    := {}
	Private aHeader2  := {}
	Private aCols2    := {}
	Private aFields1  := {}
	Private aFields2  := {}
	Private cArqTMP
	Private nCol 	  := 0
	Private cLegenda  := ""
	Private nTamanho  := 0
	Private aRotina   := {}
	Private cArqTrab
	Private cApont    := SPACE(23) //13 Digitos para EAN + 10 digitos para Lote
	Private oFont20n  := TFont():New("Arial",,20,,.t.,,,,,.f.)
	Private oFont18n  := TFont():New("Arial",,18,,.t.,,,,,.f.)
	Private lConfirm  := .F.
	Private cVolume   := SPACE(5)
	Private cTpVol    := SPACE(3)
	Private cDescVol  := SPACE(30)
	//Private lConf     := .T.
	// Private lImpAut   := .F. //Marcio
	Private lGrvFalta := .F.
	Private lTOk      := .F.
	Private nTotLib   := 0
	Private nTotSep   := 0
	Private lHabilita := .T.

	// Local aAux1    := aClone(oGetDados1:aCols)
	//Local aAux2    := aClone(oGetDados2:aCols)

	//Private oGetDados1
	//Private oGetDados2


//-------------------------------------------------------
//Estrutura do primeiro aCols
//-------------------------------------------------------
	If lcontinua
		aFields1 := {"ZP_ITEM","B1_CODBAR","ZP_PRODUTO","ZP_QUANT","ZP_VOLUME","ZP_TPVOL","DESCVOL"}

		aAdd(aColAux1,{"","","","",0,SPACE(3),SPACE(3),"",.F.})

		If lHabilita
			aAlterFd1 := {"ZP_QUANT","ZP_VOLUME","ZP_TPVOL"}
		Else
			aAlterFd1 := {}
		EndIf

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		For nX := 1 to Len(aFields1)
			If SX3->(DbSeek(aFields1[nX]))
				nTamanho := SX3->X3_TAMANHO
				cValid   := ""
				Do Case
				Case aFields1[nX] = "B1_CODBAR"
					cTit := "Codigo"
				Case aFields1[nX] = "ZP_PRODUTO"
					cTit := "Produto"
					nTamanho := 80
				Case aFields1[nX] = "ZP_QUANT"
					cTit   := "Saldo"
					//cValid := "U_H03VDQTDE(N1)"
				Case aFields1[nX] = "ZP_TPVOL"
					cTit := "Tp Vol"
					cValid := "U_H03PESQV(M->ZP_TPVOL,.T.,oGetDados1:nAt)"
				Otherwise
					cTit := Trim(X3Titulo())
				EndCase
				Aadd(aHeader1,{cTit,SX3->X3_CAMPO,SX3->X3_PICTURE,nTamanho,SX3->X3_DECIMAL,cValid,"",SX3->X3_TIPO,"",""})
			Else
				Do Case
				Case aFields1[nX] = "DESCVOL"
					Aadd(aHeader1,{"Descri��o","ZJ_NUMPF","@!",40,0,"","","C","",""})
				EndCase
			EndIf
		Next nX

		Aadd(aCols1,Array(Len(aHeader1)+1))

		For nI := 1 To Len(aHeader1)
			If aHeader1[nI][2] = "B1_CODBAR"
				aCols1[1][nI] := "000001"
			EndIf
			aCols1[1][nI] := CriaVar(aHeader1[nI][2])
		Next nI

		aCols1[1][Len(aHeader1)+1] := .F.

		aColAux1 := DADOS1(TRB->ZJ_NUMPF)

//-------------------------------------------------------
//Estrutura do segundo aCols
//-------------------------------------------------------

		aFields2 := {"B1_CODBAR","ZJ_PRODUTO","ZJ_QTDLIB","ZJ_QTDSEP","DA1_PRCVEN"}

		aAdd(aColAux2,{"","","",0,0,0,.F.})

		aAlterFd2 := {}

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		For nX := 1 to Len(aFields2)
			If SX3->(DbSeek(aFields2[nX]))
				nTamanho := SX3->X3_TAMANHO
				Do Case
				Case aFields2[nX] = "B1_CODBAR"
					cTit := "Codigo"
				Case aFields2[nX] = "ZJ_PRODUTO"
					cTit := "Produto"
					nTamanho := 80
				Case aFields2[nX] = "ZJ_QTDLIB"
					cTit := "Saldo"

				Case aFields2[nX] = "ZJ_QTDSEP"
					cTit := "Qtde. Original"
				Case aFields2[nX] = "DA1_PRCVEN"
					cTit := "Preco"
				Otherwise
					cTit := Trim(X3Titulo())
				EndCase
				Aadd(aHeader2,{cTit,SX3->X3_CAMPO,SX3->X3_PICTURE,nTamanho,SX3->X3_DECIMAL,"","",SX3->X3_TIPO,"",""})
			EndIf
		Next nX

		Aadd(aCols2,Array(Len(aHeader2)+1))

		For nI := 1 To Len(aHeader2)
			If aHeader2[nI][2] = "B1_CODBAR"
				aCols2[1][nI] := "000001"
			EndIf
			aCols2[1][nI] := CriaVar(aHeader2[nI][2])
		Next nI

		aCols2[1][Len(aHeader2)+1] := .F.

		aColAux2 := DADOS2(TRB->ZJ_NUMPF,.F.)


		oGetDados1:SetArray(aColAux1,.T.)
		oGetDados1:Refresh()

		oGetDados2:SetArray(aColAux2,.T.)
		oGetDados2:Refresh()

		If nTotSep == nTotLib
			lTOk := .T.
		Else
			lTOk := .F.
		EndIf

	EndIf
Return

Static Function GravApont(cPreFat,nx,aAux1,aAux2,cApto,nSldApto,cLotDAP,nTotSep,nPosEAN)

	Local nzjatu  := 0
	Local cCodBar := SubStr(cApto,1,13)+SPACE(2)
	Local cLote   := SubStr(cApto,14,10)
	Local cLtPd   := SuperGetMV("MV_XLOTPAD",.F.,"000000")+space(4)
	local lbpip   := .F.
	Local cQuery  := ""
	Local nZP     := 0


	If Empty(cLote)
		cLote := cLtPd
	EndIf


	DbSelectArea("SB1")
	DbSetOrder(5) //B1_FILIAL+B1_CODBAR
	DbSeek(xFilial("SB1")+alltrim(cCodBar))

	cQuery := " SELECT TOP 1 * FROM " + RetSqlName( "SZJ" ) + " ZJ (NOLOCK) WHERE ZJ_NUMPF = '"+Alltrim(cPreFat)+"'  "
	cQuery += " AND ZJ_PRODUTO = '"+Alltrim(SB1->B1_COD)+"' AND ZJ.D_E_L_E_T_ = '' AND ZJ_QTDLIB <> ZJ_QTDSEP "

	If Select("TMPZJ") > 0
		TMPZJ->(DbCloseArea())
	EndIf

	dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), "TMPZJ", .T., .T. )

	cQuZP := " SELECT  COUNT(*) AS QTDZP FROM " + RetSqlName( "SZP" ) + " ZP (NOLOCK)  "
	cQuZP += " WHERE ZP_NUMPF = '"+Alltrim(cPreFat)+"' AND ZP.D_E_L_E_T_ = '' "

	If Select("TMPZP") > 0
		TMPZP->(DbCloseArea())
	EndIf

	dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuZP), "TMPZP", .T., .T. )

	nZP := TMPZP->QTDZP

	//ATUALIZACAO ZJ
	DbSelectArea("SZJ")
	DbSetOrder(3)
	DbSeek(xFilial("SZJ")+cPreFat+SB1->B1_COD+TMPZJ->ZJ_ITEM)

	If Found()
		//  While TMPZJ->(!EOF()) .and. cPreFat == SZJ->ZJ_NUMPF .AND. SB1->B1_COD == SZJ->ZJ_PRODUTO   .and. nzjatu == 0
		If SZJ->ZJ_PRODUTO == SubStr(aAux1[nx][3],1,15)  .AND. SUBSTR(SB1->B1_COD,1,2) <> "SV" //.AND. SZJ->ZJ_QTDLIB !=  SZJ->ZJ_QTDSEP
			BEGIN TRANSACTION
				RecLock("SZJ",.F.)
				SZJ->ZJ_QTDSEP := TMPZJ->ZJ_QTDSEP + 1 //GravApont
				lbpip := .T.
				//MsUnlock()
				SZJ->(MSUNLOCK())

				//ATUALIZACAO ZP
				DbSelectArea("SZP")
				DbSetOrder(1)
				MSSeek(xFilial("SZP")+cPreFat)

				If Found() .OR. nZP == 0
					If SZP->(SimpleLock("SZP")) .OR. nZP == 0
						RecLock("SZP",.T.)
						SZP->ZP_FILIAL  := xFilial("SZP")
						SZP->ZP_LOTDAP  := cLotDAP
						SZP->ZP_NUMPF   := cPreFat
						SZP->ZP_PEDIDO  := SubStr(cPreFat,1,6)
						SZP->ZP_CODBAR  := cCodBar
						SZP->ZP_PRODUTO := SB1->B1_COD
						SZP->ZP_LOTECTL := cLote
						SZP->ZP_QUANT   := 1
						SZP->ZP_DATA 	:= DDATABASE
						SZP->ZP_HORA	:= Time()
						SZP->ZP_ITEM    := aAux1[nx][1]
						SZP->ZP_VOLUME  := cVolume//aAux1[nx][5]
						SZP->ZP_TPVOL   := cTpVol//aAux1[nx][6]
						SZP->ZP_USR     := __cUserID
						SZP->ZP_NUMPED  := SubStr(cPreFat,1,6)
						//MsUnlock()
						SZP->(MSUNLOCK())

					Else
						MessageBox("N�o criou ZP, FAVOR TENTAR NOVAMENTE","aten��o",16)
						lbpip := .F.
						DbSelectArea("SZJ")
						DbSetOrder(3)
						DbSeek(xFilial("SZJ")+cPreFat+SB1->B1_COD+TMPZJ->ZJ_ITEM)
						RecLock("SZJ",.F.)
						SZJ->ZJ_QTDSEP := TMPZJ->ZJ_QTDSEP - 1 //GravApont
						SZJ->(MSUNLOCK())
					EndIf
				Else
					MessageBox("n�o achou","aten��o",16)
					lbpip := .F.
					DbSelectArea("SZJ")
					DbSetOrder(3)
					DbSeek(xFilial("SZJ")+cPreFat+SB1->B1_COD+TMPZJ->ZJ_ITEM)
					RecLock("SZJ",.F.)
					SZJ->ZJ_QTDSEP := TMPZJ->ZJ_QTDSEP - 1 //GravApont
					SZJ->(MSUNLOCK())

				EndIf
			End TRANSACTION
		Else
			nbip:=1
			MessageBox("n�o bipou","aten��o",16)
		Endif
		//   TMPZJ->(DbSkip())
		// Enddo
	Else
		MessageBox("n�o bipou2","aten��o",16)
	EndIf


Return lbpip
