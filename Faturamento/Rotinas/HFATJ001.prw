#Include 'Protheus.ch'
#Include 'Rwmake.ch'
#INCLUDE "TOPCONN.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATJOB001 �Autor  � Mauro Nagata       � Data �  29/04/17  ���
�������������������������������������������������������������������������͹��
���Desc.     � Integracao Transpofrete                                    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Hope                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATJ001(pMetodo)

If pMetodo == NIL                   //rotina automatica JOB
	Conout("[HFATJ001] Job Iniciado")
	ProcFat({.F.,"01","0101"})
	Conout("[HFATJ001] Job Finalizado")
Else   										//rotina manual
   ProcFat()       
EndIf   


Static Function ProcFat(aParams)

Local cQuery
Local lJobTF   	:= .T. 				//SuperGetMV(�HO_TFJOB�, .F.,.T.)			//permite job da transpofrete executar
Local cDtIniTF 	:= "20170101"   //SuperGetMV(�HO_TFDTIPV�, .F., "20170801") 		//serao considerados os pedidos de vendas a partir desta data
Local cNumSC5	:= ""

Private lManual := IIf( aParams ==   NIL, .T., aParams[1] )

If lManual        
   If !lJobTF
	   Alert("HO_TFJOB est� definida para n�o executar o job Integra��o Transpofrete")
      Return(.F.)
   EndIf   
Else
   RpcSetType(3)
   RpcSetEnv( aParams[2],aParams[3],,,'FAT' )
   Conout("Job aParams")

	If !lJobTF
      Return(.F.)
   EndIf
EndIf              

//ZZ7_STATUS:
//1=Retorno com Transportadora
//2=Retorno sem Transportadora
//3=Retorno Diverg�ncia Transpofrete
//4=Retorno N�o Definiu
//5=Integrado Protheus
//6=Integrado Diverg�ncia Protheus

cQuery := " SELECT * "
cQuery += " FROM SC5010 (NOLOCK) C5 "
cQuery += " WHERE C5.D_E_L_E_T_ <> '*' "
cQuery += " 	AND C5.C5_FILIAL = '"+xFilial("SC5")+"' "
cQuery += " 	AND C5_XLIDOTF = ' ' "
cQuery += "		AND C5_EMISSAO >= '"+cDtIniTF+"' "
cQuery += "	    AND C5_TIPO <> 'D' "
cQuery += " ORDER BY C5_FILIAL,C5_NUM "

Memowrite("SOLTF.sql",cQuery)

If Select("TRB") > 0 
	DbSelectArea("TRB")
	TRB->(DbCloseArea())
EndIf

TcQuery cQuery Alias "TRB" New

DbSelectArea("TRB")
DbGoTop() 
Do While !Eof()
	
	cNumSC5 := TRB->C5_NUM
	Conout("TRB Pedido "+cNumSC5)
	
	SC5->(DbSeek(xFilial("SC5")+cNumSC5))							//posicionando o pedido de vendas
	SA1->(DbSeek(xFilial("SA1")+TRB->C5_CLIENTE+TRB->C5_LOJACLI))	//posicionando o cliente do pedido de vendas
	SYA->(DbSeek(xFilial("SYA")+SA1->A1_PAIS))								//posicionando a descricao do pais
	CCH->(DbSeek(xFilial("CCH")+SA1->A1_CODPAIS))							//posicionando a descricao do pais
	SA3->(DbSeek(xFilial("SA3")+TRB->C5_VEND1))								//posicionando o vendedor do pedido de vendas
	SZ1->(DbSeek(xFilial("SZ1")+SC5->C5_TPPED))								//posicionando o tipo de pedidos de vendas

	nContIt  := 0			//contador de item do pedido de vendas
	nTVlrPed := 0			//valor do preco de lista do pedido de vendas
	nTVlrMrc := 0			//valor da mercadoria do pedido de vendas
	nTPLiq   := 0			//total do peso liquidos dos produtos do pedido de vendas
	
	SC6->(DbSeek(xFilial("SC6")+cNumSC5))	//posicionando o item do pedido de vendas
	Do While SC6->(!Eof()).And.SC6->C6_NUM = cNumSC5
		nContIt++
		nTVlrPed += SC6->C6_QTDVEN*SC6->C6_PRCVEN
		nTVlrMrc += SC6->C6_QTDVEN*SC6->C6_PRCVEN
		nTPLiq   += Posicione("SB1",1,xFilial("SB1")+SC6->C6_PRODUTO,"B1_PESO")*SC6->C6_QTDVEN
		SC6->(DbSkip()) 
	EndDo
	
	
	// TODO Altera��o  da regra para verificar antes de come�ar a grava na ZZ7 [Weskley Silva 12/06/18]
	
	DbSelectArea("ZZ7")
	ZZ7->(DbSetOrder(2))

	IF ZZ7->(!DbSeek(xFilial("ZZ7")+cNumSC5))
	
		ZZ7->(RecLock("ZZ7",.T.))  //posicionando o pedido de vendas
		
		ZZ7->ZZ7_FILIAL := xFilial("ZZ7")
		ZZ7->ZZ7_CNPJE  := SM0->M0_CGC
		ZZ7->ZZ7_RAZAOE := SM0->M0_NOME
		ZZ7->ZZ7_CEPE   := SM0->M0_CEPCOB
		ZZ7->ZZ7_PEDIDO := cNumSC5
		ZZ7->ZZ7_STATUS := "0"
		ZZ7->ZZ7_EMISSA := Ctod(Substr(TRB->C5_EMISSAO,7,2)+"/"+Substr(TRB->C5_EMISSAO,5,2)+"/"+Substr(TRB->C5_EMISSAO,1,4))
		//ZZ7->ZZ7_ENTREG := Ctod(Substr(TRB->C5_EMISSAO,7,2)+"/"+Substr(TRB->C5_EMISSAO,5,2)+"/"+Substr(TRB->C5_EMISSAO,1,4))
		ZZ7->ZZ7_CODCLI := TRB->C5_CLIENTE
		ZZ7->ZZ7_LOJA   := TRB->C5_LOJACLI
		ZZ7->ZZ7_NOMCLI := SA1->A1_NOME
		ZZ7->ZZ7_CNPJC  := SA1->A1_CGC
		ZZ7->ZZ7_ENDFIS := SA1->A1_END
		ZZ7->ZZ7_BAIFIS := SA1->A1_BAIRRO
		ZZ7->ZZ7_CIDFIS := SA1->A1_MUN
		ZZ7->ZZ7_ESTFIS := SA1->A1_EST
		ZZ7->ZZ7_CEPFIS := SA1->A1_CEP
		ZZ7->ZZ7_PAIFIS := SYA->YA_DESCR

		If alltrim(SA1->A1_ESTE) = ""
			ZZ7->ZZ7_ENDENT := SA1->A1_END
			ZZ7->ZZ7_BAIENT := SA1->A1_BAIRRO
			ZZ7->ZZ7_CIDENT := SA1->A1_MUN
			ZZ7->ZZ7_ESTENT := SA1->A1_EST    
			ZZ7->ZZ7_CEPENT := SA1->A1_CEP
		Else
			ZZ7->ZZ7_ENDENT := SA1->A1_ENDENT
			ZZ7->ZZ7_BAIENT := SA1->A1_BAIRROE
			ZZ7->ZZ7_CIDENT := SA1->A1_MUNE
			ZZ7->ZZ7_ESTENT := SA1->A1_ESTE    
			ZZ7->ZZ7_CEPENT := SA1->A1_CEPE
		Endif
		
		ZZ7->ZZ7_PAIENT := CCH->CCH_PAIS
		ZZ7->ZZ7_MACREG := TRB->C5_XMICRRE
		ZZ7->ZZ7_DESCMR := TRB->C5_XMICRDE
		ZZ7->ZZ7_DIVISA := TRB->C5_XCODDIV
		ZZ7->ZZ7_DIVISA := TRB->C5_XCODDIV
		ZZ7->ZZ7_DESDIV := TRB->C5_XNONDIV
		ZZ7->ZZ7_GRPLJ  := TRB->C5_XGRUPO
		ZZ7->ZZ7_DESGRL := TRB->C5_XGRUPON
		ZZ7->ZZ7_REGIAO := TRB->C5_XCODREG
		ZZ7->ZZ7_DESREG := TRB->C5_XDESREG
		ZZ7->ZZ7_CNLDST := TRB->C5_XCANAL
		ZZ7->ZZ7_DESCDI := TRB->C5_XCANALD
		ZZ7->ZZ7_REPRES := TRB->C5_VEND1
		ZZ7->ZZ7_NOMREP := TRB->C5_NOMVEND
		ZZ7->ZZ7_CNPJR  := SA3->A3_CGC
		ZZ7->ZZ7_QTDEIT := nContIt
		ZZ7->ZZ7_VLRMRC := nTVlrMrc
		ZZ7->ZZ7_VLRPED := nTVlrPed
		ZZ7->ZZ7_VOLUME := TRB->C5_VOLUME1
		ZZ7->ZZ7_PESLIQ := nTPLiq
		ZZ7->ZZ7_PESBRT := TRB->C5_PBRUTO
		ZZ7->ZZ7_TIPOPV := SC5->C5_TPPED		//tipo de pedido de vendas SZ1
		ZZ7->ZZ7_GERTRP := If(SZ1->Z1_GERATF="S","S","N")		//gera dados da transportadora
		ZZ7->ZZ7_SLDQI  := nContIt
		ZZ7->ZZ7_SLDPL  := nTPLiq
		ZZ7->ZZ7_SLDVLR := nTVlrMrc
	
		ZZ7->(MsUnLock())
		
		Conout("ZZ7 Gravado "+cNumSC5)
	
	endif
	
	//TODO Verifica se o pedido foi gravado no transpofrete e atualiza o status na SC5 [Weskley Silva 12/06/2018]
	
	DbSelectArea("ZZ7")
	DbSetOrder(2)
	
	IF Dbseek(xFilial("ZZ7")+cNumSC5)
	
		//atualizando o pedido de vendas como lido/processado
		SC5->(DbSetOrder(1))
		SC5->(DbSeek(xFilial("SC5")+cNumSC5))
		SC5->(RecLock("SC5",.F.))
		SC5->C5_XLIDOTF := "S"		//lido e integrado o pedido de vendas
		SC5->(MsUnLock())
	
	ELSE 
	
		//atualizando o pedido de vendas como lido/processado
		SC5->(DbSetOrder(1))
		SC5->(DbSeek(xFilial("SC5")+cNumSC5))
		SC5->(RecLock("SC5",.F.))
		SC5->C5_XLIDOTF := " "		//lido e integrado o pedido de vendas
		SC5->(MsUnLock())
	
	endif
	
	Conout("TRB Pedido Lido "+cNumSC5)
	
	DbSelectArea("TRB")
	DbSkip()
	
EndDo

TRB->(DbCloseArea())

If Empty( cNumSC5 )
	Return
Endif

cQuery := " SELECT * "
cQuery += " FROM ZZ7010 (NOLOCK) ZZ7 "
cQuery += " WHERE ZZ7.D_E_L_E_T_ <> '*' "
cQuery += " AND ZZ7_PEDIDO = '"+Alltrim(cNumSC5)+"' " 

Memowrite("STATUS_TF.sql",cQuery)

If Select("TRB") > 0 
	DbSelectArea("TRB")
	TRB->(DbCloseArea())
EndIf

TcQuery cQuery Alias "TRB" New

DbSelectArea("TRB")
DbGoTop()
While !Eof()
	Do Case
		Case TRB->ZZ7_STATUS = "1"
				cOcorr := ""
				cGerTrp := TRB->ZZ7_GERTRP   //gerar dados da transportadora
				If cGerTrp = "S"
					lOk := .T.
					If Empty(TRB->ZZ7_CNPJT)
						cOcorr += "* "+Dtoc(dDatabase)+"-"+Time()+" - CNPJ da Transportadora nao informado *"
						lOk    := .F.
					EndIf
					If Empty(TRB->ZZ7_ENDTRP)
						cOcorr += "* "+Dtoc(dDatabase)+"-"+Time()+" - Endere�o da Transportadora nao informado *"
						lOk    := .F.
					EndIf
					
					If lOk
						aDados := {}
						
						lMsErroAuto := .F.
						
						//verificando se a transportadora existe
						DbSelectArea("SA4")
						SA4->(DbSetOrder(3))
						If SA4->(DbSeek(xFilial("SA4")+TRB->ZZ7_CNPJT))
							cCodTra := SA4->A4_COD
							nOp := 4 //manutencao
						Else
							cCodTra := GetSxeNum("SA4","A4_COD")
		          			RollBackSx8()
		          			nOp := 3  //inclusao
		          		EndIf
						aAdd(aDados, {"A4_FILIAL", xFilial("SA4")		, nil})
						aAdd(aDados, {"A4_COD"   , cCodTra	          	, nil})
						aAdd(aDados, {"A4_NOME"  , TRB->ZZ7_NOMTRP	, nil})
						aAdd(aDados, {"A4_CGC"   , TRB->ZZ7_CNPJT		, nil})
						aAdd(aDados, {"A4_END"   , TRB->ZZ7_ENDTRP	, nil})
						aAdd(aDados, {"A4_BAIRRO", TRB->ZZ7_BAITRP	, nil})
						aAdd(aDados, {"A4_MUN"   , TRB->ZZ7_CIDTRP	, nil})
						aAdd(aDados, {"A4_EST"   , TRB->ZZ7_ESTTRP	, nil})
						aAdd(aDados, {"A4_CEP"   , TRB->ZZ7_CEPTRP	, nil})
						aAdd(aDados, {"A4_DDI"   , TRB->ZZ7_DDITRP	, nil})
						aAdd(aDados, {"A4_DDD"   , TRB->ZZ7_DDDTRP	, nil})
						aAdd(aDados, {"A4_TEL"   , TRB->ZZ7_TELTRP	, nil})
						
						DbSelectArea("SA4")
						MSExecAuto({|x,y|mata050(x,y)},aDados,nOp) 	
						If lMsErroAuto		
							cMsgErro := MostraErro()
							
							//atualizando a tabela intermediaria que houve divergencia
							ZZ7->(DbSeek(xFilial("ZZ7")+TRB->ZZ7_PEDIDO))
							If ZZ7->(!Eof())
								ZZ7->(RecLock("ZZ7",.F.))
								ZZ7->ZZ7_STATUS := "6"  //divergencia na integracao com os dados da transportadora
								ZZ7->ZZ7_OCORR  += "* "+Dtoc(dDataBase)+"-"+Time()+"-[MsExecAuto}-"+cMsgErro+" *"
								ZZ7->(MsUnLock())
							EndIf			
						EndIf
					
						If !lMsErroAuto
							ZZ7->(DbSeek(xFilial("ZZ7")+TRB->ZZ7_PEDIDO))
							If ZZ7->(!Eof())
								ZZ7->(RecLock("ZZ7",.F.))
								ZZ7->ZZ7_TRANSP := cCodTra
								ZZ7->ZZ7_STATUS := "5"  //integrado os dados da transportadora
								ZZ7->(MsUnLock())
							EndIf	
							SC5->(DbSeek(xFilial("SC5")+TRB->ZZ7_PEDIDO))
							RecLock("SC5",.F.)
							//incluida linha abaixo [Mauro Nagata, Actual Trend, 20170613]
							SC5->C5_TRANSP  := cCodTra
							SC5->C5_XOBSNF  := TRB->ZZ7_OBSPV
							SC5->C5_XPEDMIL := TRB->ZZ7_PEDMIL
							SC5->(MsUnLock())
						EndIf
					Else
						ZZ7->(DbSeek(xFilial("ZZ7")+TRB->ZZ7_PEDIDO))
						If ZZ7->(!Eof())
							ZZ7->(RecLock("ZZ7",.F.))
							ZZ7->ZZ7_STATUS := "6"  //divergencia na integracao com os dados da transportadora
							ZZ7->ZZ7_OCORR  += "* "+Dtoc(dDataBase)+"-"+Time()+"-"+cOcorr
							ZZ7->(MsUnLock())
						EndIf	
					EndIf	
				EndIf	
			
		Case TRB->ZZ7_STATUS = "3"
				SC5->(DbSeek(xFilial("SC5")+TRB->ZZ7_PEDIDO))
				SC5->(RecLock("SC5",.F.))
				SC5->C5_XOBSTF  := TRB->ZZ7_OBSTFT
				SC5->(MsUnLock())

				ZZ7->(DbSetOrder(1))
				ZZ7->(DbSeek(xFilial("ZZ7")+TRB->ZZ7_PEDIDO))
				ZZ7->(RecLock("ZZ7",.F.))
				ZZ7->ZZ7_STATUS := "5"
				ZZ7->(MsUnLock())
	EndCase				

	DbSelectArea("TRB")
	DbSkip()

EndDo

TRB->(DbCloseArea())
	
Return

