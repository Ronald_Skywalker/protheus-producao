#INCLUDE "RWMAKE.CH"
#INCLUDE "TBICODE.CH"
#INCLUDE "FONT.CH"

// Retornos poss�veis do MessageBox
#Define IDOK			    1
#Define IDCANCEL		    2
#Define IDYES			    6
#Define IDNO			    7
#Define CRLF  CHR(13)+CHR(10)
User Function ImpShowR()

	Local aPergs    := {}
	Private cArq       := ""
	Private cArqMacro  := "XLS2DBF.XLA"
	Private cTemp      := GetTempPath() //pega caminho do temp do client
	Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema
	Private aResps     := {}
	Private aArquivos  := {}
	Private nRet       := 0
	Private aTitle     := {}
	Private aLog     := {}

	aAdd(aTitle, "Os seguintes pedidos foram criados a partir dessa planilha" )
	aAdd(aLog,{} )
	aAdd(aLog[Len(aLog)], Padr("Pedido",12) + Padr("Cliente",12) + Padr("Sub-Cole��o",30) + Padr("Entrega",12))

	Aadd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.T.,""})
	aAdd(aPergs,{1,"Tabela",Space(3),"","","DA0","",0,.T.}) // Tipo caractere

	If ParamBox(aPergs,"Parametros", @aResps)

		Aadd(aArquivos, AllTrim(aResps[1]))

		cArq := AllTrim(aResps[1])

		Processa({|| ProcMov()})

	EndIF
Return

Static Function ProcMov()

	Local nFile		:= 0
	Local cFile		:= AllTrim(aResps[1])
	Local cLinha  	:= ""
	Local nLintit	:= 1
	Local nLin 		:= 0
	Local aDados  	:= {}
	Local nHandle 	:= 0


	nFile := fOpen(cFile,0,0) //fOpen(cFile, FO_READ + FO_DENYWRITE)

	If nFile == -1
		If !Empty(cFile)
			MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")
		EndIf
		Return
	EndIf

	nHandle := Ft_Fuse(cFile)
	Ft_FGoTop()
	nLinTot := FT_FLastRec()-1
	ProcRegua(nLinTot)

	While nLinTit > 0 .AND. !Ft_FEof()
		Ft_FSkip()
		nLinTit--
	EndDo

	Do While !Ft_FEof()
		IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))
		nLin++
		cLinha := Ft_FReadLn()

		If Empty(AllTrim(StrTran(cLinha,',','')))
			Ft_FSkip()
			Loop
		EndIf

		cLinha := StrTran(cLinha,'"',"'")
		cLinha := '{"'+cLinha+','+alltrim(str(nLin+1))+'"}'
		cLinha := StrTran(cLinha,';',',')
		cLinha := StrTran(cLinha,',','","')

		Aadd(aDados, &cLinha)
		FT_FSkip()
	EndDo

	FT_FUse()

	//ProcRegua(Len(aDados))
	ProcRegua(0)
	nRet:= MessageBox("Deseja realmente importar o arquivo:" +Upper(cFile)+ " ?","Confirma��o",4)

	If nRet == 6
		U_xMata410(aDados)
	Else
		MessageBox("Cancelado pelo usu�rio !","Cancelado",16)
	EndIf

Return

//ROTINA DE INCLUSAO AUTOMATICA DE PEDIDOS
User Function xMata410(pMatriz)

//pMatriz --> matriz contendo os dados do arq csv

	Local nXT           := 0
	Local aLinha        := {}
	Local nContIT		:= 0
	Private pMatrizped  := pMatriz
	Private lMsErroAuto := .F.
	Private cTipoped    := ""
	Private cTabPed     := Alltrim(MV_PAR02)
	Private cCliente    := ""
	Private cLjcli      := ""
	Private cLjEnt      := ""
	Private cCondPag    := ""
	Private cProd       := ""
	Private cQtd        := ""
	Private cPrcVenda   := ""
	Private cPrcUni     := ""
	Private cValor      := ""
	Private cTES        := ""
	Private cCFOP       := ""
	Private cTpped      := ""
	Private cTppra      := ""
	Private cPolCom     := ""
	Private dDtent      := ""
	Private lOK         := .F.
	Private cPedCli     := ""
	Private nRet2       := 0

	aCabec := {}   // monta SC5
	aItens := {}   // monta SC6

	For nXT:=1 to len(pMatrizped)

		cCliente      := pMatrizped[nXT][2]
		cLjcli        := "0001"//pMatrizped[nXT][3]
		cLjEnt        := "0001"//pMatrizped[nXT][3]
		cCondPag      := pMatrizped[nXT][6]
		cTpped        := Strzero(Val(pMatrizped[nXT][4]),3)
		cTppra        := Strzero(Val(pMatrizped[nXT][5]),3)
		cPolCom       := Strzero(Val(pMatrizped[nXT][3]),3)
		cPedCli       := pMatrizped[nXT][12]

		cProd          := pMatrizped[nXT][11]
		cQtd           := Val(pMatrizped[nXT][21])
		cPrcVenda      := Val(pMatrizped[nXT][22])
		cPrcUni        := Val(pMatrizped[nXT][22])
		cValor         := Round(cQtd * cPrcVenda,2)
		//cTES           := pMatrizped[nXT][16]
		//cCFOP          := pMatrizped[nXT][17]
		dDtent         := Dtos(Ctod(pMatrizped[nXT][20]))


		aLinha := {}
		cTpOper		:= "01"

		If existReg( "SZ1", 1, cTpPed )
			If !Empty( SZ1->Z1_TPOPER )
				cTpOper := SZ1->Z1_TPOPER
			EndIf
		EndIf

		cTes 	:= getTes(cTpOper,cCliente,cLjcli,cProd )
		nContIT++

		Aadd(aLinha,{"C6_ITEM"   ,RetAsc(nContIT,GetSx3Cache("C6_ITEM","X3_TAMANHO"),.T.) ,Nil})
		Aadd(aLinha,{"C6_PRODUTO",cProd                     ,Nil})
		Aadd(aLinha,{"C6_QTDVEN" ,cQtd                      ,Nil})
		Aadd(aLinha,{"C6_PRCVEN" ,Round(cPrcUni,3)          ,Nil})
		Aadd(aLinha,{"C6_PRUNIT" ,Round(cPrcUni,3)          ,Nil})
		Aadd(aLinha,{"C6_VALOR" ,cValor                     ,Nil})
		Aadd(aLinha,{"C6_TES"    ,cTES                      ,Nil})
		//Aadd(aLinha,{"C6_CF"    ,cCFOP                      ,Nil})

		Aadd(aItens,aLinha)

		If nXT == len(pMatrizped)
			lOk := .T.
		ElseIf (dDtent != Dtos(Ctod(pMatrizped[nXT+1][20])) .OR. (cPedCli != pMatrizped[nXT+1][12]) .OR. cCliente != pMatrizped[nXT+1][2])
			lOk := .T.
		EndIF
		aCabec := {}   // monta SC5
		If lOK
			Aadd(aCabec,{"C5_POLCOM" ,cPolCom                  ,Nil})
			Aadd(aCabec,{"C5_CLIENTE",Padr(cCliente,6)         ,Nil})
			Aadd(aCabec,{"C5_LOJACLI",StrZero(val(cLjcli),4)   ,Nil})
			Aadd(aCabec,{"C5_TIPO"   ,"N"                      ,Nil})
			Aadd(aCabec,{"C5_TPPED"  ,cTpped                   ,Nil})
			Aadd(aCabec,{"C5_TABELA"   ,cTabPed                ,Nil})
			Aadd(aCabec,{"C5_LOJAENT",StrZero(val(cLjEnt),4)   ,Nil})
			Aadd(aCabec,{"C5_CONDPAG",cCondPag                 ,Nil})
			Aadd(aCabec,{"C5_ORIGEM" ,"IMPORTEXCE"             ,Nil})
			Aadd(aCabec,{"C5_XPEDCLI",cPedCli                  ,Nil})
			Aadd(aCabec,{"C5_FECENT",stod(dDtent)              ,Nil})
			Aadd(aCabec,{"C5_PRAZO",cTppra                     ,Nil})

			//* Inclusao Utilizando MsExecAuto
			If lOk .AND. STOD(dDtent) > Date()
				MsExecAuto({|x, y, z| MATA410(x, y, z)}, aCabec, aItens,3)
			ElseIf lOk .AND. STOD(dDtent) < Date()
				Messagebox("Pedido do cliente : " +Alltrim(cCliente)+" Sub-Cole��o : "+Alltrim(cPedCli)+ " n�o ser� gerado, DATA DE ENTREGA MENOR QUE DATA DE EMISS�O FAVOR VERIFICAR","ATEN��O",16)
				lOK := .F.
			EndIf

			If !lMsErroAuto .AND. STOD(dDtent) > Date()
				aAdd(aLog[Len(aLog)], Padr(SC5->C5_NUM,12) + Padr(cCliente,12) + Padr(cPedCli,30) + Padr(pMatrizped[nXT][20],12))

				Logs("Pedido: " + SC5->C5_NUM)
				Logs("Cliente: " + cCliente)
				Logs("Sub-Cole��o: " + cPedCli)
				Logs("Dt. Entrega: " + dDtent)
				Logs("==============================================================")

				aItens := {}   // monta SC6
				lOK := .F.
				nContIT := 0
			ElseIf !lMsErroAuto .AND. STOD(dDtent) < Date()
				aCabec := {}   // monta SC5
				aItens := {}   // monta SC6
				lOK := .F.
				nContIT := 0
				Loop
			Else
				// Se erro , avisa, e sai do FOR
				Messagebox("Erro na inclusao!, favor verificar linha:" +cvaltochar(nXT+1)+ " do arquivo ! ","Erro" ,16)
				Mostraerro()
				aCabec := {}   // monta SC5
				aItens := {}   // monta SC6
				lOK := .F.
				nXT := len(pMatrizped)
				nContIT := 0
				Loop
			EndIf
		EndIf

	Next nXT
	nRet2 := MessageBox("Deseja imprimir o LOG dos pedidos gerados ?","LOG",4)
	If nRet2 == 6
		fMakeLog( aLog , aTitle , NIL  , .T. , "ImpShowR" , NIL , NIL, NIL, NIL, .F. )
	Endif
Return(.T.)

/*
* Gera��o do arquivo de log.
* @Author Geyson Albano
* @Data 10/06/2020
*/
Static Function Logs(cTexto)
	Local cCodLog	:= SubStr(DtoS(Date()),1,6)
	Local cPasta	:= "\log\ImpShowRoom"
	Local FC_NORMAL := 0

	ConOut(DtoC(Date(),"dd/mm/yy")+"|"+Time()+"|"+cUserName+"| "+cTexto)

	FWMakeDir(cPasta)
	If File(cPasta+"\"+cCodLog+".txt")
		nHdl := FOpen(cPasta+"\"+cCodLog+".txt" , 2 + 64 )
	Else
		nHdl := FCreate(cPasta+"\"+cCodLog+".txt",FC_NORMAL)
	EndIf
	FSeek(nHdl, 0, 2)
	FWrite(nHdl, DtoC(Date(),"dd/mm/yy")+"|"+Time()+"|"+cUserName+"| "+cTexto+CRLF )

	FClose(nHdl)
Return

/*/
	------------------------------------------------------------------
	{Protheus.doc} getTes()
	Retorna a TES Inteligente

	@author Sergio S. Fuzinaka
	@since Set/2020
	@version 1.00
	------------------------------------------------------------------
/*/

Static Function getTes(cTpOper,cCliente,cLjcli,cProd)

	Local cRet	:= ""
	Local cTpOp := Padl(Alltrim(cTpOper),2,"0")
	Local cTpCli := Alltrim(POSICIONE("SA1",1,xFilial("SA1")+Alltrim(cCliente)+cLjcli,"A1_TIPO"))

	cRet := MaTesInt( 2, cTpOp, cCliente, cLjcli, "C", cProd,"",cTpCli )

Return( cRet )

/*/
	------------------------------------------------------------------
	{Protheus.doc} existReg()
	Posiciona no registro se existir

	@author Sergio S. Fuzinaka
	@since Set/2020
	@version 1.00
	------------------------------------------------------------------
/*/
Static Function existReg( cAlias, nOrder, cKey )

	Local lRet 	:= .F.
	Local aArea	:= GetArea()

	dbSelectArea( cAlias )
	dbSetOrder( nOrder )
	If dbSeek( xFilial( cAlias ) + cKey )
		lRet := .T.
	Endif

	If !lRet
		RestArea( aArea )
	Endif

Return( lRet )
