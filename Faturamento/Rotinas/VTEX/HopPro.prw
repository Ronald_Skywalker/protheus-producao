#include 'totvs.ch'
#include 'restful.ch'
 
//**************************************************************************************
// Fun��o....: HopPro 
// Descri��o.: WebService API de consulta de produto por Id
// Autor.....: Edson Deluca
// Vers�o....: 1.0
// Cliente...: Hope
// Data......: 07/10/2020
//************************************************************************************** 

User Function HopPro()
Return

WSRESTFUL prodid DESCRIPTION 'API para Consulta de Produto por id' SECURITY 'MATA010' FORMAT APPLICATION_JSON
	
	WSDATA IdProduto As Character
    WSDATA IdTabela  As Character

	WSMETHOD GET IdProd   DESCRIPTION 'Consulta Produto individual por id'    WSSYNTAX '/prodid/{IdProduto}/{IdTabela}'    PRODUCES APPLICATION_JSON
	
ENDWSRESTFUL
 
//**************************************************************************************

WSMETHOD GET IdProd QUERYPARAM IdProduto,IdTabela  WSRECEIVE IdProduto,IdTabela WSSERVICE prodid

    Local lRet           := .T.
    Local cAlias         := ""
    Local aData          := {}
    Local oData          := NIL
    Local cMarca         := SuperGetMV("MV_MARCWEB",.F.,"'00001','00002','00006'")
    Local cTabPor        := SuperGetMV("MV_TABPOR",.F.,"009")
    Local _xI            := 0
    Local cCod           := ""
    Local _aOcasiao      := {}
    Local oCaso          := NIL

    IF Empty(Self:IdProduto)
        SetRestFault(500,EncodeUTF8('O parametro IdProduto � obrigat�rio'))
        lRet    := .F.
        Return(lRet)
    EndIF

    IF Empty(Self:IdTabela)
        SetRestFault(500,EncodeUTF8('O parametro IdTabela � obrigat�rio'))
        lRet    := .F.
        Return(lRet)
    EndIF

    cQuery := " "
	cQuery := " SELECT SB4.B4_COD,SB4.B4_DESC,SB4.B4_GRUPO,SBM.BM_GRUPO,SBM.BM_DESC,SB4.B4_YCATEGO,SB4.B4_YNCATEG,SB4.B4_YNOMARC,SB4.B4_YMARCA, "
    cQuery += " SB1.B1_YDATLIB,SB1.B1_YDWEB,SB4.B4_YNSUBGR,SB4.B4_YSUBGRP,SB4.B4_PESO,SB4.B4_YNCICLO,SZD.ZD_DESCLAV,SB4.B4_YTECIDO,SB4.B4_YNSUBCO, "
    cQuery += " SB4.B4_YOCASIA,DA1.DA1_PRCVEN AS PRECODE,DA12.DA1_PRCVEN AS PRECOPOR,SB4.R_E_C_N_O_ AS NREG "
 	cQuery += " FROM "+ RetSqlName("SB4") +" SB4 "
    cQuery += " INNER JOIN "+ RetSqlName("SB1") +" SB1 ON SB1.B1_FILIAL = '"+ xFilial("SB1") +"' AND SUBSTRING(SB1.B1_COD,1,8) = SB4.B4_COD AND SB1.B1_YDWEB = 'S' AND SB1.D_E_L_E_T_='' "
    cQuery += " LEFT JOIN "+ RetSqlName("SBM") +" SBM ON SBM.BM_FILIAL = '"+ xFilial("SBM") +"' AND SB4.B4_GRUPO = SBM.BM_GRUPO AND SBM.D_E_L_E_T_='' "
    cQuery += " LEFT JOIN "+ RetSqlName("SZD") +" SZD ON SZD.ZD_FILIAL = '"+ xFilial("SZD") +"' AND SZD.ZD_PRODUTO = SUBSTRING(SB1.B1_COD,1,8) and SZD.ZD_COR = SUBSTRING(SB1.B1_COD,9,3) and SZD.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN "+ RetSqlName("DA1") +" DA1 ON DA1.DA1_FILIAL = '"+ xFilial("DA1") +"' AND SB1.B1_COD = DA1.DA1_CODPRO AND DA1.DA1_CODTAB = '"+AllTrim(Self:IdTabela)+"' AND DA1.D_E_L_E_T_='' "
    cQuery += " LEFT JOIN "+ RetSqlName("DA1") +" DA12 ON DA12.DA1_FILIAL = '"+ xFilial("DA1") +"' AND SB1.B1_COD = DA12.DA1_CODPRO AND DA12.DA1_CODTAB = '"+cTabPor+"' AND DA12.D_E_L_E_T_='' "
 	cQuery += "  WHERE B4_FILIAL  =  '"+ xFilial("SB4") +"' "
	cQuery += "    AND SB4.B4_COD =  '"+ AllTrim(Self:IdProduto) +"' "
    cQuery += "    AND SB4.B4_YMARCA IN ("+cMarca+") " 
	cQuery += "    AND SB4.D_E_L_E_T_ =  ' ' "

    cAlias := GetNextAlias()

    DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)

    dbSelectArea(cAlias)
    (cAlias)->(dbGoTop())

    IF (cAlias)->(.NOT. Eof())
 
        While (cAlias)->(.NOT. Eof())
 
            oData := JsonObject():New()

            dbSelectArea("SB1")
            dbSetOrder(1)
            dbSeek(xFilial("SB1")+AllTrim((cAlias)->B4_COD),.F.)
            
            cYDESCPT  := SB1->B1_YDESCPT
            _aOcasiao := {}

            For _xI := 1 to Len((cAlias)->B4_YOCASIA) STEP 6
                cCod := Substr((cAlias)->B4_YOCASIA,_xI,5)
                If !Empty(cCod) 
                    dbSelectArea("ZAD")
                    dbSetOrder(1)
                    If dbSeek(xFilial("ZAD")+cCod,.F.)
                       oCaso := JsonObject():New()
                       oCaso['id']   := EncodeUTF8(AllTrim(ZAD->ZAD_CODIGO))
                       oCaso['name'] := EncodeUTF8(AllTrim(ZAD->ZAD_DESCRI))
                       AADD(_aOcasiao,oCaso) 
                       FreeObj(oCaso)
                    EndIf
                EndIf
            Next _xI
                   
            oData['id']               := AllTrim((cAlias)->B4_COD)
            oData['nome']             := EncodeUTF8(AllTrim((cAlias)->B4_DESC))
            oData['idprotheus']       := EncodeUTF8(AllTrim((cAlias)->B4_COD))
            oData['idCategoria']      := EncodeUTF8(AllTrim((cAlias)->BM_GRUPO))
            oData['NomeCategoria']    := EncodeUTF8(AllTrim((cAlias)->BM_DESC))
            oData['idsubcategoria']   := EncodeUTF8(AllTrim((cAlias)->B4_YCATEGO))
            oData['nomesubcategoria'] := EncodeUTF8(AllTrim((cAlias)->B4_YNCATEG))
            oData['descricao']        := EncodeUTF8(AllTrim(cYDESCPT))
            oData['Marca']            := EncodeUTF8(AllTrim((cAlias)->B4_YNOMARC))
            oData['datalancamento']   := AllTrim((cAlias)->B1_YDATLIB)
            oData['dataliberacao']    := AllTrim((cAlias)->B1_YDATLIB)
            oData['status']           := (cAlias)->B1_YDWEB
            oData['idtipo']           := EncodeUTF8(AllTrim((cAlias)->B4_YSUBGRP))
            oData['tipo']             := EncodeUTF8(AllTrim((cAlias)->B4_YNSUBGR))
            oData['peso']             := (cAlias)->B4_PESO
            oData['ciclo']            := EncodeUTF8(AllTrim((cAlias)->B4_YNCICLO))
            oData['instrucaolavagem'] := EncodeUTF8(AllTrim((cAlias)->ZD_DESCLAV))
            oData['tecido']           := EncodeUTF8(AllTrim((cAlias)->B4_YTECIDO))
            oData['colecao']          := EncodeUTF8(AllTrim((cAlias)->B4_YNSUBCO))
            oData['ocasioes']         := _aOcasiao
            oData['precode']          := (cAlias)->PRECODE
            oData['precopor']         := (cAlias)->PRECOPOR

            aAdd(aData,oData)
            FreeObj(oData)
            
            _cProduto := (cAlias)->B4_COD
            
            While (cAlias)->B4_COD == _cProduto  
               (cAlias)->(dbSkip())
            End

        EndDo
 
        Self:SetResponse(FwJsonSerialize(aData))
 
    ELSE
        SetRestFault(404,EncodeUTF8('N�O existe produto para os parametros apresentados'))
        lRet    := .F.
    EndIF    
 
    (cAlias)->(dbCloseArea())
 
Return(lRet)
