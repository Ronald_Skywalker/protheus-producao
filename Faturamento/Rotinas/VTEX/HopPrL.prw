#include 'totvs.ch'
#include 'restful.ch'
 
//**************************************************************************************
// Fun��o....: HopPrL
// Descri��o.: WebService API de consulta de lista de produtos
// Autor.....: Edson Deluca
// Vers�o....: 1.0
// Cliente...: Hope
// Data......: 07/10/2020
//************************************************************************************** 

User Function HopPrL()
Return

WSRESTFUL prodList DESCRIPTION 'API para Consulta de lista de Produtos' SECURITY 'MATA010' FORMAT APPLICATION_JSON
	
    WSDATA IdTabela     As Character
    WSDATA nInicial     As Character
    WSDATA nQuantidade  As Character
    WSDATA lPreco       As Boolean 

	WSMETHOD GET ListProd   DESCRIPTION 'Consulta lista de Produtos'    WSSYNTAX '/prodlist/{IdTabela}/{nInicial}/{nQuantidade}/{lPreco}'    PRODUCES APPLICATION_JSON
	
ENDWSRESTFUL
 
//**************************************************************************************

WSMETHOD GET ListProd QUERYPARAM IdTabela,nInicial,nQuantidade,lPreco  WSRECEIVE IdTabela,nInicial,nQuantidade,lPreco WSSERVICE prodlist

    Local lRet            := .T.
    Local cAlias          := ""
    Local aData           := {}
    Local oData           := NIL
    Local cMarca          := SuperGetMV("MV_MARCWEB",.F.,"'00001','00002','00006'")
    Local cTabPor         := SuperGetMV("MV_TABPOR",.F.,"009")
    Local cJson           := ""
    Local nTotalRegistros := 0
    Local _xI            := 0
    Local cCod           := ""
    Local _aOcasiao      := {}
    Local oCaso          := NIL
    Local nCont          := 0
    Local nContIni       := 1
 
    IF Empty(Self:IdTabela)
        SetRestFault(500,EncodeUTF8('O parametro IdTabela � obrigat�rio'))
        lRet    := .F.
        Return(lRet)
    EndIF

    IF Empty(Self:nInicial)
        SetRestFault(500,EncodeUTF8('O parametro nInicial � obrigat�rio'))
        lRet    := .F.
        Return(lRet)
    EndIF

    IF Empty(Self:nQuantidade)
        SetRestFault(500,EncodeUTF8('O parametro nQuantidade � obrigat�rio'))
        lRet    := .F.
        Return(lRet)
    EndIF

    IF Self:lPreco == NIL
        Self:lPreco := .F.

    EndIF

    nInicial := Val(Self:nInicial)
    nQuantidade := Val(Self:nQuantidade)

    If !Self:lPreco 

         //COUNT DA MESMA QUERY ABAIXO PARA PREENCHER TOTAL ITENS
        cQuery := ""
        cQuery += " SELECT COUNT(*) AS TOTALREG"
        cQuery += " FROM "+RetSqlName("SB4")+" SB4 "
        cQuery += " LEFT JOIN SBM010 SBM ON SBM.BM_FILIAL = '"+xFilial("SB4")+"' AND SB4.B4_GRUPO = SBM.BM_GRUPO AND SBM.D_E_L_E_T_=' ' "
        cQuery += " WHERE SB4.B4_YMARCA IN ("+cMarca+") "
        cQuery += " AND SB4.D_E_L_E_T_ = ' ' AND SB4.B4_DWEB = 'S' "

        cTotReg := GetNextAlias()

        DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cTotReg, .F., .T.)

        dbSelectArea(cTotReg)
        (cTotReg)->(dbGoTop())

        nTotalRegistros := (cTotReg)->TOTALREG

        (cTotReg)->(dbCloseArea())

        cQuery := ""
        cQuery := " SELECT * FROM ( "
        cQuery += " SELECT SB4.B4_COD,SB4.B4_DESC,SB4.B4_GRUPO,SBM.BM_GRUPO,SBM.BM_DESC,SB4.B4_YCATEGO,SB4.B4_YNCATEG,SB4.B4_YNOMARC,SB4.B4_YMARCA, "
        cQuery += " SB4.B4_DWEB,SB4.B4_YNSUBGR,SB4.B4_YSUBGRP,SB4.B4_PESO,SB4.B4_YNCICLO,SB4.B4_YTECIDO,SB4.B4_YNSUBCO, "
        cQuery += " SB4.B4_YOCASIA,SB4.R_E_C_N_O_ AS NREG,ROW_NUMBER() OVER ( ORDER BY SB4.B4_COD ) AS ROWNUM "
        cQuery += " FROM "+RetSqlName("SB4")+" SB4 " 
        cQuery += " LEFT JOIN "+RetSqlName("SBM")+" SBM ON SBM.BM_FILIAL = '"+xFilial("SBM")+"' AND SB4.B4_GRUPO = SBM.BM_GRUPO AND SBM.D_E_L_E_T_=' ' "
        cQuery += " WHERE SB4.B4_YMARCA IN ("+cMarca+") "
        cQuery += " AND SB4.D_E_L_E_T_ =  ' ' AND SB4.B4_DWEB = 'S'"
        cQuery += " ) AS TABFIM "
        cQuery += " WHERE TABFIM.ROWNUM > "+AllTrim(Str(nInicial))+" AND TABFIM.ROWNUM <= "+AllTrim(Str(nInicial+nQuantidade))+" "

        cAlias := GetNextAlias()
            
        DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)

    Else    

        //COUNT DA MESMA QUERY ABAIXO PARA PREENCHER TOTAL ITENS COM PRE�O
        cQuery := ""
        cQuery += " SELECT COUNT(*) AS TOTALPREC FROM ( "
        cQuery += " SELECT SUBSTRING(DA1.DA1_CODPRO,1,8) AS TOTALREG"
        cQuery += " FROM "+RetSqlName("SB4")+" SB4 "
        cQuery += " LEFT JOIN SBM010 SBM ON SBM.BM_FILIAL = '"+xFilial("SB4")+"' AND SB4.B4_GRUPO = SBM.BM_GRUPO AND SBM.D_E_L_E_T_=' ' "
        cQuery += " LEFT JOIN "+RetSqlName("DA1")+" DA1 ON DA1.DA1_FILIAL = '"+xFilial("DA1")+"' AND DA1.DA1_CODTAB = '"+Alltrim(Self:IdTabela)+"' AND SUBSTRING(DA1.DA1_CODPRO,1,8) = SB4.B4_COD AND DA1.DA1_PRCVEN > 0  AND DA1.D_E_L_E_T_ = '' "
        cQuery += " WHERE SB4.B4_YMARCA IN ("+cMarca+") "
        cQuery += " AND SB4.D_E_L_E_T_ = ' ' AND SB4.B4_DWEB = 'S' "
        cQuery += " GROUP BY SUBSTRING(DA1.DA1_CODPRO,1,8)) AS TABQUANT "

        cTotReg := GetNextAlias()

        DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cTotReg, .F., .T.)

        dbSelectArea(cTotReg)
        (cTotReg)->(dbGoTop())

        nTotalRegistros := (cTotReg)->TOTALPREC

        (cTotReg)->(dbCloseArea())

        cQuery := ""
        cQuery := " SELECT * FROM ( "
        cQuery += " SELECT SB4.B4_COD,SB4.B4_DESC,SB4.B4_GRUPO,SBM.BM_GRUPO,SBM.BM_DESC,SB4.B4_YCATEGO,SB4.B4_YNCATEG,SB4.B4_YNOMARC,SB4.B4_YMARCA, "
        cQuery += " SB4.B4_DWEB,SB4.B4_YNSUBGR,SB4.B4_YSUBGRP,SB4.B4_PESO,SB4.B4_YNCICLO,SB4.B4_YTECIDO,SB4.B4_YNSUBCO, "
        cQuery += " SB4.B4_YOCASIA,SB4.R_E_C_N_O_ AS NREG,ROW_NUMBER() OVER ( ORDER BY SB4.B4_COD ) AS ROWNUM "
        cQuery += " FROM "+RetSqlName("SB4")+" SB4  " 
        cQuery += " LEFT JOIN "+RetSqlName("SBM")+" SBM ON SBM.BM_FILIAL = '"+xFilial("SBM")+"' AND SB4.B4_GRUPO = SBM.BM_GRUPO AND SBM.D_E_L_E_T_=' ' "
        cQuery += " RIGHT JOIN "+RetSqlName("DA1")+" DA1 ON DA1.DA1_FILIAL = '"+xFilial("DA1")+"' AND DA1.DA1_CODTAB = '"+Alltrim(Self:IdTabela)+"' AND DA1.DA1_PRCVEN > 0  AND SUBSTRING(DA1.DA1_CODPRO,1,8) = SB4.B4_COD AND DA1.D_E_L_E_T_ = '' " 
        cQuery += " WHERE SB4.B4_YMARCA IN ("+cMarca+") "
        cQuery += " AND SB4.D_E_L_E_T_ =  ' ' AND SB4.B4_DWEB = 'S'"
        cQuery += " GROUP BY SB4.B4_COD ,SB4.B4_DESC,SB4.B4_GRUPO,SBM.BM_GRUPO,SBM.BM_DESC,SB4.B4_YCATEGO,SB4.B4_YNCATEG,SB4.B4_YNOMARC,SB4.B4_YMARCA,SB4.B4_DWEB,SB4.B4_YNSUBGR,SB4.B4_YSUBGRP,SB4.B4_PESO,SB4.B4_YNCICLO,SB4.B4_YTECIDO,SB4.B4_YNSUBCO, SB4.B4_YOCASIA,SB4.R_E_C_N_O_ "
        cQuery += " ) AS TABFIM "
        cQuery += " WHERE TABFIM.ROWNUM > "+AllTrim(Str(nInicial))+" AND TABFIM.ROWNUM <= "+AllTrim(Str(nInicial+nQuantidade))+" "

        cAlias := GetNextAlias()
        
        DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)

    EndIF

    dbSelectArea(cAlias)
    (cAlias)->(dbGoTop())

    IF (cAlias)->(.NOT. Eof())

        While (cAlias)->(.NOT. Eof())
                
            oData := JsonObject():New()

            dbSelectArea("SB1")
            dbSetOrder(1)
            dbSeek(xFilial("SB1")+AllTrim((cAlias)->B4_COD),.F.)
        
            cYDESCPT := SB1->B1_YDESCPT

            dbSelectArea("SZD")
            dbSetOrder(1)
            dbSeek(xFilial("SZD")+AllTrim((cAlias)->B4_COD),.F.)

            dbSelectArea("DA1")
            dbSetOrder(1)
            dbSeek(xFilial("DA1")+Self:IdTabela+AllTrim((cAlias)->B4_COD),.F.)

            nPRECODE := DA1->DA1_PRCVEN

            dbSelectArea("DA1")
            dbSetOrder(1)
            dbSeek(xFilial("DA1")+cTabPor+AllTrim((cAlias)->B4_COD),.F.)
        
            nPRECOPOR := DA1->DA1_PRCVEN

            _aOcasiao := {}

            For _xI := 1 to Len((cAlias)->B4_YOCASIA) STEP 6
                cCod := Substr((cAlias)->B4_YOCASIA,_xI,5)
                If !Empty(cCod) 
                    dbSelectArea("ZAD")
                    dbSetOrder(1)
                    If dbSeek(xFilial("ZAD")+cCod,.F.)
                    oCaso := JsonObject():New()
                    oCaso['id']   := EncodeUTF8(AllTrim(ZAD->ZAD_CODIGO))
                    oCaso['name'] := EncodeUTF8(AllTrim(ZAD->ZAD_DESCRI))
                    AADD(_aOcasiao,oCaso) 
                    FreeObj(oCaso)
                    EndIf
                EndIf
            Next _xI

            oData['id']               := AllTrim((cAlias)->B4_COD)
            oData['nome']             := EncodeUTF8(AllTrim((cAlias)->B4_DESC))
            oData['idprotheus']       := EncodeUTF8(AllTrim((cAlias)->B4_COD))
            oData['idCategoria']      := EncodeUTF8(AllTrim((cAlias)->BM_GRUPO))
            oData['NomeCategoria']    := EncodeUTF8(AllTrim((cAlias)->BM_DESC))
            oData['idsubcategoria']   := EncodeUTF8(AllTrim((cAlias)->B4_YCATEGO))
            oData['nomesubcategoria'] := EncodeUTF8(AllTrim((cAlias)->B4_YNCATEG))
            oData['descricao']        := EncodeUTF8(AllTrim(cYDESCPT))
            oData['Marca']            := EncodeUTF8(AllTrim((cAlias)->B4_YNOMARC))
            oData['datalancamento']   := AllTRim(SB1->B1_YDATLIB)
            oData['dataliberacao']    := AllTRim(SB1->B1_YDATLIB)
            oData['status']           := AllTRim((cAlias)->B4_DWEB)
            oData['idtipo']           := EncodeUTF8(AllTrim((cAlias)->B4_YSUBGRP))
            oData['tipo']             := EncodeUTF8(AllTrim((cAlias)->B4_YNSUBGR))
            oData['peso']             := (cAlias)->B4_PESO
            oData['ciclo']            := EncodeUTF8(AllTrim((cAlias)->B4_YNCICLO))
            oData['instrucaolavagem'] := EncodeUTF8(AllTrim(SZD->ZD_DESCLAV))
            oData['tecido']           := EncodeUTF8(AllTrim((cAlias)->B4_YTECIDO))
            oData['colecao']          := EncodeUTF8(AllTrim((cAlias)->B4_YNSUBCO))
            oData['ocasioes']         := _aOcasiao
            oData['precode']          := nPRECODE
            oData['precopor']         := nPRECOPOR

            aAdd(aData,oData)
            FreeObj(oData)
            
            (cAlias)->(dbSkip())

        EndDo

        cJson := FwJsonSerialize(aData)
        
        cJsonFim := '{"Inicial": '+AllTrim(Str(nInicial))+','
        cJsonFim += '"quantidade": '+AllTrim(Str(nQuantidade))+',''
        cJsonFim += '"totalitens": '+AllTrim(Str(nTotalRegistros))+','
        cJsonFim += '"items": '+cJson+'}'      
        
        Self:SetResponse(cJsonFim)

    ELSE
        SetRestFault(404,EncodeUTF8('N�O existe produto para os parametros apresentados'))
        lRet    := .F.
    EndIF    

    (cAlias)->(dbCloseArea())

Return(lRet)
