#Include "totvs.ch"

//**************************************************************************************
// Fun��o....: FindEnd 
// Descri��o.: Compara endere�o enviado no par�metro com endere�o do cadastro
// Autor.....: Edson Deluca
// Vers�o....: 1.0
// Cliente...: Hope
// Data......: 09/10/2020
//************************************************************************************** 
/*

Se o retorno for zero n�o existe o cliente ou n�o existe o cliente com o endere�o pesquisado,
caso contr�rio ele retorna o recno do cadastro de cliente com o endere�o pesquisado.

*/
User Function FindEnd(cCPF,cEnd,cCep)

Local cPesq1  := ""
Local cPesq2  := ""
Local cQuery  := ""
Local cAlias  := "" 
Local nRecno  := 0 
Local nI      := 0
Local nInicio := 0
Local nK      := 0
Local nCount  := 0

cEnd   := Upper(Alltrim(NoAcento(cEnd)))
// Tratamento para ap�strofo
cEnd := Replace( cEnd, "'", "''")

For nI := 1 to Len(cEnd)

   If (Substr(cEnd,nI,1) == " " .Or. Substr(cEnd,nI,1) == ",") .And. nInicio <> 0
      nFim := nI
      cPesq1 := Substr(cEnd,nInicio,nFim-nInicio)
      Exit
   EndIf

   If Substr(cEnd,nI,1) == " " .And. nInicio == 0
      nInicio := nI + 1
   EndIf

Next nI

For nI := Len(cEnd) to 1 Step -1

    If Substr(cEnd,nI,1) == "," 
        nInicio := nI + 2
        For nK := nInicio to Len(cEnd) 
            nCount ++
            If Substr(cEnd,nK,1) == " " .or. nK >= Len(cEnd)
               nFim := nK
               cPesq2 := Substr(cEnd,nInicio,nCount)
               Exit
            EndIf
        Next nK
    EndIf

Next nI

cQuery := " "
cQuery := " SELECT R_E_C_N_O_ As NREG "
cQuery += "   FROM "+ RetSqlName("SA1") +" "
cQuery += "  WHERE A1_FILIAL  =  '"+ xFilial("SA1") +"' "
cQuery += "    AND A1_CGC =  '"+ AllTrim(cCPF) +"' "
cQuery += "    AND A1_CEP =  '"+ AllTrim(cCep) +"' "
//cQuery += "    AND A1_ENDENT LIKE '%"+ AllTrim(cPesq1)+"%' "
//cQuery += "    AND A1_ENDENT LIKE '%"+ AllTrim(cPesq2)+"%' "
cQuery += "    AND A1_END LIKE '%"+ AllTrim(cPesq1)+"%' "
cQuery += "    AND A1_END LIKE '%"+ AllTrim(cPesq2)+"%' "
cQuery += "    AND D_E_L_E_T_ =  ' ' "

cAlias := GetNextAlias()

DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)

DbSelectArea( cAlias )
(cAlias)->( DbGoTop() )

If (cAlias)->(!Eof())
   nRecno := (cAlias)->NREG
EndIf

Return nRecno
