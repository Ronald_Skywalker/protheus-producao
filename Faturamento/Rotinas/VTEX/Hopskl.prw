#include 'totvs.ch'
#include 'restful.ch'
 
//**************************************************************************************
// Fun��o....: WSApiSkL 
// Descri��o.: WebService API de consulta de Lista de Sku por Produto
// Autor.....: Edson Deluca
// Vers�o....: 1.0
// Cliente...: Hope
// Data......: 02/10/2020
//************************************************************************************** 

User Function HopSkL()
Return

WSRESTFUL produtos DESCRIPTION 'API para Consulta de Sku por Produto' SECURITY 'MATA010' FORMAT APPLICATION_JSON
	
	WSDATA IdProduto As Character
	WSDATA IdTabela  As Character
	
	WSMETHOD GET ListSku DESCRIPTION 'Consulta list de Sku dos Produtos' WSSYNTAX '/produtos/skus/{IdProduto}/{IdTabela}'  PRODUCES APPLICATION_JSON
	
ENDWSRESTFUL
 
//**************************************************************************************

WSMETHOD GET ListSku QUERYPARAM IdProduto, IdTabela  WSRECEIVE IdProduto,IdTabela WSSERVICE produtos

    Local lRet           := .T.
    Local cAlias         := ""
    Local aData          := {}
    Local oData          := NIL
    Local cLocais        := SuperGetMV("MV_LOCWEB",.F.,"'E0','E1'")
    Local cMarca         := SuperGetMV("MV_MARCWEB",.F.,"'00001','00002','00006'")
    Local cTabPor        := SuperGetMV("MV_TABPOR",.F.,"009")
    Local cDtEntreg      := SuperGetMV("MV_XDTENT",.F.,"20200101")
    Local _nTotalEstoque := 0.000
    Local _cSku          := ""
    Local cQryCrt        := ""
    Local nCartAtu       := 0
    Local cQryEC         := ""   
    Local nEstEC         := 0
    Local EST_EC         := ""
    Local CRT_ATUAL      := ""    
    Local cStopper       := ''
    Local cDscComp       := '' 

    IF Empty(Self:IdProduto)
        SetRestFault(500,EncodeUTF8('O parametro IdProduto � obrigat�rio'))
        lRet    := .F.
        Return(lRet)
    EndIF

    IF Empty(Self:IdTabela)
        SetRestFault(500,EncodeUTF8('O parametro IdTabela � obrigat�rio'))
        lRet    := .F.
        Return(lRet)
    EndIF

     // Retorna o saldo em estoque do EC onde � apresentado quando o saldo atual for menor que 0
    cQryEC := " "
	cQryEC := " SELECT  SUM(B2_QATU) - (SUM(B2_RESERVA) + SUM(B2_XRESERV)) AS ESTOQUE_EC FROM "+ RetSqlName("SB2") +" AS EC WITH (NOLOCK)"
    cQryEC += " WHERE (D_E_L_E_T_ = '') AND (B2_FILIAL = '"+ xFilial("SB2") +"') AND (B2_LOCAL = 'EC')"
    cQryEC += " AND (SUBSTRING(B2_COD,1,8) = '"+ AllTrim(Self:IdProduto) +"')  GROUP BY B2_COD"

    EST_EC := GetNextAlias()
	
    DbUseArea( .T., "TOPCONN", TcGenQry(,,cQryEC), EST_EC, .F., .T.)

    nEstEC := (EST_EC)->ESTOQUE_EC

    (EST_EC)->(dbCloseArea())

    // Consulta que verifica todos os pedidos em aberto para considerar como j� atendidos sobre o estoque.
    cQryCrt := " "
	cQryCrt := " SELECT SUM(C6_QTDVEN-C6_QTDENT) AS CRT_ATU FROM "+ RetSqlName("SC6") +" SC6 (NOLOCK)"
    cQryCrt += " JOIN "+ RetSqlName("SC5") +" SC5 (NOLOCK) ON (C6_NUM = C5_NUM AND C6_CLI = C5_CLIENT AND C6_LOJA = C5_LOJACLI  AND SC5.D_E_L_E_T_ = ''"
    cQryCrt += " AND SC6.D_E_L_E_T_ = '' AND  C5_FILIAL = '"+ xFilial("SC5") +"')"
    cQryCrt += " JOIN "+ RetSqlName("SF4") +" SF4 (NOLOCK) ON (F4_CODIGO = C6_TES AND SF4.D_E_L_E_T_ = '')"
 	cQryCrt += " WHERE SC6.D_E_L_E_T_='' AND SUBSTRING(C6_PRODUTO,1,8) = '"+ AllTrim(Self:IdProduto) +"' AND C6_BLQ<>'R' AND C5_TPPED NOT IN ('097','027','104')"
    cQryCrt += " AND C6_ENTREG >= '"+cDtEntreg+"' AND C6_LOCAL = 'E0' AND F4_ESTOQUE = 'S' GROUP BY C6_PRODUTO"

    CRT_ATUAL := GetNextAlias()
	
    DbUseArea( .T., "TOPCONN", TcGenQry(,,cQryCrt), CRT_ATUAL, .F., .T.)

    nCartAtu := (CRT_ATUAL)->CRT_ATU

    If nCartAtu < 0
       nCartAtu := nCartAtu * -1
    
    EndIf

    (CRT_ATUAL)->(dbCloseArea())

    cQuery := " "
	cQuery := " SELECT SB1.B1_COD,ISNULL(CONVERT(VARCHAR(2047), CONVERT(VARCHAR(2047), ZD_DESCCP2)),'')  DSCCMP2 ,SB1.B1_DESC,SB1.R_E_C_N_O_,SB1.B1_YDATLIB,SB4.B4_XALTU,SB4.B4_XCOMP,SB4.B4_XLARG,SBV.BV_DESCRI AS TAM,SBV2.BV_DESCRI AS COR,"
    cQuery += " SB4.B4_PESO,SB1.B1_YDWEB,SB1.B1_YSITUAC,SB4.B4_YTECIDO,SB1.B1_CODBAR,SZD.ZD_DESCLAV,SZD.ZD_DESCCOM,SB1.B1_YCICLO,SZD.ZD_DESCCOM,SB4.B4_COLUNA,"
    cQuery += " DA1.DA1_PRCVEN AS PRECODE, SB2.B2_QATU AS ESTOQUE,SB4.B4_YMARCA,SB4.B4_YNOMARC,DA12.DA1_PRCVEN AS PRECOPOR,"
    cQuery += " LTRIM(RTRIM(REPLACE(SUBSTRING(SUBSTRING(SB1.B1_COD,12,4),PATINDEX('%[A-D]%',SUBSTRING(SB1.B1_COD,12,4)),100),SUBSTRING(SB1.B1_COD,12,4),''))) AS TACA, SB1.B1_YESTMIN, SB1.B1_YBLQDEM "
 	cQuery += " FROM "+ RetSqlName("SB1") +" SB1 "
    cQuery += " INNER JOIN "+ RetSqlName("SB4") +" SB4 ON SB4.B4_FILIAL = '"+ xFilial("SB4") +"' AND SUBSTRING(SB1.B1_COD,1,8) = SB4.B4_COD AND SB4.B4_YMARCA IN ("+cMarca+") AND SB4.D_E_L_E_T_='' "
    cQuery += " LEFT JOIN "+ RetSqlName("SB2") +" SB2 ON SB2.B2_FILIAL = '"+ xFilial("SB2") +"' AND SB2.B2_LOCAL IN ("+cLocais+") AND SB2.B2_COD = SB1.B1_COD AND SB2.D_E_L_E_T_='' "
    cQuery += " LEFT JOIN "+ RetSqlName("SZD") +" SZD ON SZD.ZD_FILIAL = '"+ xFilial("SZD") +"' AND SZD.ZD_PRODUTO = SUBSTRING(SB1.B1_COD,1,8) and SZD.ZD_COR = SUBSTRING(SB1.B1_COD,9,3) and SZD.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN "+ RetSqlName("DA1") +" DA1 ON DA1.DA1_FILIAL = '"+ xFilial("DA1") +"' AND SB1.B1_COD = DA1.DA1_CODPRO AND DA1.DA1_CODTAB = '"+AllTrim(Self:IdTabela)+"' AND DA1.D_E_L_E_T_='' "
    cQuery += " LEFT JOIN "+ RetSqlName("DA1") +" DA12 ON DA12.DA1_FILIAL = '"+ xFilial("DA1") +"' AND SB1.B1_COD = DA12.DA1_CODPRO AND DA12.DA1_CODTAB = '"+cTabPor+"' AND DA12.D_E_L_E_T_='' "
    cQuery += " LEFT JOIN "+ RetSqlName("SBV") +" SBV ON SBV.BV_FILIAL = '"+ xFilial("SBV") +"' AND SBV.BV_TABELA = SB4.B4_COLUNA AND SBV.BV_TIPO = '2' AND SBV.BV_CHAVE = SUBSTRING(SB1.B1_COD,12,4) AND SBV.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN "+ RetSqlName("SBV") +" SBV2 ON SBV2.BV_FILIAL = '"+ xFilial("SBV") +"' AND SBV2.BV_TABELA = SB4.B4_LINHA AND SBV2.BV_TIPO = '1' AND SBV2.BV_CHAVE = SUBSTRING(B1_COD, 9, 3) AND SBV2.D_E_L_E_T_ = '' "
 	cQuery += "  WHERE B1_FILIAL  =  '"+ xFilial("SB1") +"' "
	cQuery += "    AND SUBSTRING(SB1.B1_COD,1,8) =  '"+ AllTrim(Self:IdProduto) +"' "
    cQuery += "    AND SB1.B1_YDWEB = 'S' "
	cQuery += "    AND SB1.D_E_L_E_T_ =  ' ' "
 
    cAlias := GetNextAlias()
	
    DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)

    dbSelectArea(cAlias)
    (cAlias)->(dbGoTop())

    IF (cAlias)->(.NOT. Eof())
 
        While (cAlias)->(.NOT. Eof())

            dbSelectArea("ZBD")
            ZBD->(dbSetOrder(1))
            If dbSeek(xFilial("ZBD")+SubStr(AllTrim((cAlias)->B1_COD),1,11),.F.)
                cStopper:= EncodeUTF8(AllTrim(ZBD->ZBD_STOPPE))
            Endif

            oData := JsonObject():New()
            
            oData['id']               := AllTrim((cAlias)->B1_COD)
            oData['nome']             := EncodeUTF8(AllTrim((cAlias)->B1_DESC))
            oData['idprotheus']       := AllTrim(Str((cAlias)->R_E_C_N_O_,10,0))
            oData['idproduto']        := AllTrim(Substr(AllTrim((cAlias)->B1_COD),1,8))
            oData['descricao']        := EncodeUTF8(AllTrim((cAlias)->B1_DESC))
            oData['datalancamento']   := AllTrim((cAlias)->B1_YDATLIB)
            oData['dataliberacao']    := AllTrim((cAlias)->B1_YDATLIB)
            oData['altura']           := (cAlias)->B4_XALTU
            oData['comprimento']      := (cAlias)->B4_XCOMP
            oData['largura']          := (cAlias)->B4_XLARG
            oData['peso']             := (cAlias)->B4_PESO
            oData['status']           := (cAlias)->B1_YDWEB
            oData['situacao']         := AllTrim((cAlias)->B1_YSITUAC)
            oData['tecido']           := EncodeUTF8(AllTrim((cAlias)->B4_YTECIDO))
            oData['codigobarra']      := AllTrim((cAlias)->B1_CODBAR)
            oData['instrucaolavagem'] := EncodeUTF8(AllTrim((cAlias)->ZD_DESCLAV))
            oData['ciclo']            := AllTrim((cAlias)->B1_YCICLO)
            oData['composicao']       := EncodeUTF8(AllTrim((cAlias)->DSCCMP2))
            oData['precode']          := (cAlias)->PRECODE
            oData['precopor']         := (cAlias)->PRECOPOR
            oData['idcanal']          := AllTrim((cAlias)->B4_YMARCA)
            oData['canal']            := AllTrim((cAlias)->B4_YNOMARC)
            oData['estoqueminimo']    := IIF((cAlias)->B1_YESTMIN==nil,"",EncodeUTF8(Alltrim(cValToChar((cAlias)->B1_YESTMIN))))
            oData['tamanho']          := EncodeUTF8(IIF((cAlias)->B4_COLUNA$("TCA/TCB/TCC/TCD/TDD"),Substr(AllTrim((cAlias)->TAM),1,2),AllTrim((cAlias)->TAM)))
            oData['idcor']            := EncodeUTF8(SUBSTR(AllTrim((cAlias)->B1_COD),9,3))
            oData['cor']              := EncodeUTF8(AllTrim((cAlias)->COR))
            oData['taca']             := EncodeUTF8(AllTrim((cAlias)->TACA))
            oData['caracteristicas']  := EncodeUTF8(AllTrim((cAlias)->ZD_DESCLAV)+" "+AllTrim((cAlias)->DSCCMP2))
            oData['bloqueiodemanda']  := IIF((cAlias)->B1_YBLQDEM==" " .OR. (cAlias)->B1_YBLQDEM==nil ,"N",EncodeUTF8((cAlias)->B1_YBLQDEM))
            oData['stopper'] := {} 

            If !Empty(cStopper)
            
                aStopper:= Separa(cStopper,"-",.T.)
                dbSelectArea("ZBD")
                ZBC->(dbSetOrder(1))

                For nI:= 1 to Len(aStopper)
                    If !Empty(aStopper[nI]) .AND. ZBC->(dbSeek(xFilial("ZBC")+aStopper[nI],.F.))
                        aadd(oData['stopper'], JSonObject():New()) 
                        oData['stopper'][Len(oData['stopper'])]['id']   := aStopper[nI]
                        oData['stopper'][Len(oData['stopper'])]['desc'] := POSICIONE("ZBC",1,xFilial("ZBC")+aStopper[nI],"ZBC_DESCRI") 
                    Endif
                Next

            Endif

            _nTotalEstoque := 0.000
            _cSku          := (cAlias)->B1_COD

            While (cAlias)->(.NOT. Eof()) .And. _cSku == (cAlias)->B1_COD

              _nTotalEstoque := _nTotalEstoque + (cAlias)->ESTOQUE
              
              (cAlias)->(dbSkip())
        
            End

            _nTotalEstoque := IIF(_nTotalEstoque - nCartAtu < 0, nEstEC , _nTotalEstoque - nCartAtu + nEstEC)

            oData['estoque']          := _nTotalEstoque

            aAdd(aData,oData)
            FreeObj(oData)
 
        EndDo

        Self:SetResponse(FwJsonSerialize(aData))
 
    ELSE
        SetRestFault(404,EncodeUTF8('N�O existem dados para serem apresentados'))
        lRet    := .F.
    EndIF    
 
    (cAlias)->(dbCloseArea())
 
Return(lRet)

