#Include "totvs.ch"
#Include "restful.ch"


/*/{Protheus.doc} Hope001

	TODO API para consulta, inclus�o e manuten��o de pedido de venda.

	@author 	Alex Lima [FIT Gest�o]
	@since 		05/10/2020
	@version 	12.1.25
	@type 		User Function

	@Obs 		Function Dummy, apenas para reserva do nome do fonte.

	@history	05/10/2020, Alex Lima [FIT Gest�o], Desenvolvimento inicial.
/*/
User Function Hope001()
Return

// API para pedido de venda
WSRESTFUL Pedidos DESCRIPTION 'API para manuten��o de pedido de venda' SECURITY 'MATA410' FORMAT APPLICATION_JSON
	
	WSDATA PedVtex	As Character
	WSDATA FatVtex	As Character
	WSDATA DatIni	As Character
	WSDATA DatFim	As Character
	WSDATA nInicio	As Character
	WSDATA nQuant	As Character
	WSDATA cPedOri  As Character
	
	//get pedidos
	WSMETHOD GET PedOne DESCRIPTION "Consulta pedido de venda pelo n�mero";
	WSSYNTAX "/pedidos/{PedVtex}";
	PATH "/pedidos/{PedVtex}";
	PRODUCES APPLICATION_JSON
	
	WSMETHOD GET PedAll DESCRIPTION "Consulta pedido de venda por per�odo";
	WSSYNTAX "/pedidos/all/";
	PATH "/pedidos/{DatIni}/{DatFim}";
	PRODUCES APPLICATION_JSON
	
	//get faturamento 
	WSMETHOD GET FatOne DESCRIPTION "Consulta faturamento pelo pedido de venda";
	WSSYNTAX "/faturamento/";
	PATH "/faturamento/{FatVtex}";
	PRODUCES APPLICATION_JSON
	
	WSMETHOD GET FatAll DESCRIPTION "Consulta faturamento por per�odo";
	WSSYNTAX "/faturamento/all/";
	PATH "/faturamento/{DatIni}/{DatFim}";
	PRODUCES APPLICATION_JSON
	
	//post inclusao pedido
	WSMETHOD POST InsPed DESCRIPTION "Inclus�o de pedido de venda";
	WSSYNTAX "/pedidos/inc/";
	PRODUCES APPLICATION_JSON
	
	//put atualizacao pedido
	WSMETHOD PUT UpdPed DESCRIPTION "Altera��o de pedido de venda";
	WSSYNTAX "/pedidos/alt/";
	PRODUCES APPLICATION_JSON
	
ENDWSRESTFUL


//PedOne - Busca pedido por c�digo de pedido vtex
//Modelo = http://homolog.grupohope.com:8081/pedidos/pedidos?PedVtex=666000
WSMETHOD GET PedOne PATHPARAM PedVtex WSRECEIVE PedVtex WSSERVICE Pedidos

	Local cAlias 		:= ""
	Local cQuery 		:= ""
	Local cMsgGift 		:= ""
	Local aData 		:= {}
	Local lRet 			:= .T.
	Local lProc 		:= .T.
	Local lIsGift 		:= ""
	Local oData 		:= Nil
	
	
	//obrigatorio informar o numero do pedido
	IF Empty(Self:PedVtex)
		SetRestFault(400,EncodeUTF8('O parametro PedVtex � obrigat�rio'))
		lRet := .F.
		lProc := .F.
	EndIF
	
	If lProc
		//montagem da consulta
		cQuery := " "
		cQuery := " SELECT C5_FILIAL "
		cQuery += "      , C5_NUM "
		cQuery += "      , C5_LIBEROK "
		cQuery += "      , C5_XPEDWEB "
		cQuery += "      , C5_NOTA "
		cQuery += "      , C5_BLQ "
		cQuery += "   FROM "+ RetSqlName("SC5") +" (NOLOCK) "
		cQuery += "  WHERE C5_FILIAL  =  '"+ xFilial("SC5") +"' "
		cQuery += "    AND C5_XPEDWEB =  '"+ AllTrim(Self:PedVtex) +"' "
		cQuery += "    AND D_E_L_E_T_ =  ' ' "
		cAlias := GetNextAlias()
		DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
		If (cAlias)->( Eof())
			SetRestFault(400,EncodeUTF8('N�o existem dados para serem apresentados'))
			lRet := .F.
			lProc := .F.
		EndIf
		
	EndIf
	
	If lProc
		
		DbSelectArea( cAlias )
		(cAlias)->( DbGoTop() )
		While (cAlias)->( !Eof())
			
			//Cria um objeto JSON
			oData := JsonObject():New()
			
			// Adiciona elemento
			oData['IdPedProtheus'] := AllTrim( (cAlias)->C5_NUM )
			oData['IdPedVtex'] := AllTrim( (cAlias)->C5_XPEDWEB )
			
			//Se o pedido tiver em aberto no Protheus
			If Empty( (cAlias)->C5_LIBEROK ) .And. Empty( (cAlias)->C5_NOTA ) .And. Empty( (cAlias)->C5_BLQ )
				oData['IdStatus'] := "ready-for-handling"
				oData['TpStatus'] := "Pronto+para+o+manuseio"
				
			//Pedido encerrado (faturado) no Protheus
			ElseIf !Empty( (cAlias)->C5_NOTA ) .And. (cAlias)->C5_LIBEROK == "E" .And. Empty( (cAlias)->C5_BLQ )
				oData['IdStatus'] := "invoiced"
				oData['TpStatus'] := "Faturado"
				
			//Se o pedido for eliminado por res�duo, ou seja, cancelado no Protheus
			ElseIf (cAlias)->C5_NOTA == "XXXXXXXXX" .And. (cAlias)->C5_LIBEROK == "S" .And. Empty( (cAlias)->C5_BLQ )
				oData['IdStatus'] := "canceled"
				oData['TpStatus'] := "Cancelado"
				
			EndIf
			
			// Identificar se � para presente
			lIsGift := .F.
			IsGift( (cAlias)->C5_FILIAL, (cAlias)->C5_NUM, @lIsGift, @cMsgGift )
			oData['IsGift']  := lIsGift
			oData['MsgGift'] := cMsgGift
			aAdd(aData,oData)
			FreeObj( oData )
			
			(cAlias)->(dbSkip())
		EndDo
		
		//Define o retorno do m�todo
		Self:SetResponse( FwJsonSerialize(aData) )
		(cAlias)->(DbCloseArea())
		
	EndIf
	
Return lRet

// PedAll - Busca pedidos por per�odo
// Modelo = http://homolog.grupohope.com:8081/pedidos/all/pedidos?DatIni=2020-01-01 00:01:00&DatFim=2020-01-02 23:59:00&nInicio=1&nQuant=10
WSMETHOD GET PedAll PATHPARAM DatIni, DatFim, nInicio, nQuant, cPedOri WSRECEIVE DatIni, DatFim, nInicio, nQuant, cPedOri WSSERVICE Pedidos
	
	Local cAlias 		:= ""
	Local cQuery 		:= ""
	Local cMsgGift 		:= ""
	Local cDatIni 		:= ""
	Local cDatFim 		:= ""
	Local cHorIni 		:= ""
	Local cHorFim 		:= ""
	Local cPedOri 		:= ""
	Local cJson 		:= ""
	Local cJsonFim 		:= ""
	Local nInicio 		:= 0
	Local nQuant 		:= 0
	Local nTotReg 		:= 0
	Local aData 		:= {}
	Local lRet 			:= .T.
	Local lProc 		:= .T.
	Local oData 		:= Nil
	
	
	//obrigatorio informar o numero do pedido ::aURLParms
	If Empty(Self:DatIni)
		SetRestFault(400,EncodeUTF8('O parametro DatIni � obrigat�rio'))
		lRet := .F.
		lProc := .F.
	EndIf
	
	If lProc
		If Empty(Self:DatFim)
			SetRestFault(400,EncodeUTF8('O parametro DatFim � obrigat�rio'))
			lRet := .F.
			lProc := .F.
		EndIf
	EndIf
	
	If lProc
		IF Empty(Self:nInicio)
			SetRestFault(400,EncodeUTF8('O parametro nInicial � obrigat�rio'))
			lRet := .F.
			lProc := .F.
		EndIF
	EndIF
	
	If lProc
		IF Empty(Self:nQuant)
			SetRestFault(400,EncodeUTF8('O parametro nQuant � obrigat�rio'))
			lRet := .F.
			lProc := .F.
		EndIF
	EndIF
	
	If lProc
		
		// Tratamento para data e hora
		cDatIni := ""
		cDatFim := ""
		cHorIni := ""
		cHorFim := ""
		cDatIni := StrTran( SubString( Self:DatIni, 01, 10 ), "-", "" ) //AAAA-MM-DD HH:MM:SS //data e hora de faturamento da NF-e
		cDatFim := StrTran( SubString( Self:DatFim, 01, 10 ), "-", "" )
		cHorIni := SubString( Self:DatIni, 12, 05 ) //AAAA-MM-DD HH:MM:SS //data e hora de faturamento da NF-e
		cHorFim := SubString( Self:DatFim, 12, 05 )
		
		cQuery += " SELECT COUNT(*) AS [TOT_REG] "
		cQuery += "   FROM "+ RetSqlName("SC5") +" (NOLOCK) "
		cQuery += "  WHERE C5_FILIAL  =  '"+ xFilial("SC5") +"' "
		cQuery += "    AND C5_EMISSAO BETWEEN '"+ cDatIni +"' AND '"+ cDatIni +"' "
		IF !Empty(Self:cPedOri)
			cQuery += "    AND UPPER(C5_ORIGEM) LIKE '%"+ Upper( Self:cPedOri  ) +"%' "
		Endif
		cQuery += "    AND C5_XPEDWEB <> '' "
		cQuery += "    AND D_E_L_E_T_ =  ' ' "
		cAlias := GetNextAlias()
		DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
		If (cAlias)->( Eof())
			SetRestFault(400,EncodeUTF8('N�o existem dados para serem apresentados'))
			lRet := .F.
			lProc := .F.
		Else
			nTotReg := 0
			nTotReg := (cAlias)->( TOT_REG )
		EndIf
		(cAlias)->( DbCloseArea() )
	EndIf
	
	
	If lProc
		
		// Tratamento para data e hora
		cDatIni := ""
		cDatFim := ""
		cHorIni := ""
		cHorFim := ""
		nInicio := 0
		nQuant  := 0
		cDatIni := StrTran( SubString( Self:DatIni, 01, 10 ), "-", "" ) //AAAA-MM-DD HH:MM:SS //data e hora de faturamento da NF-e
		cDatFim := StrTran( SubString( Self:DatFim, 01, 10 ), "-", "" )
		cHorIni := SubString( Self:DatIni, 12, 05 )
		cHorFim := SubString( Self:DatFim, 12, 05 )
		nInicio := Val( Self:nInicio )
		nQuant  := Val( Self:nQuant )
		
		//montagem da consulta
		cQuery := " "
		cQuery := " SELECT * "
		cQuery += "   FROM ( SELECT C5_FILIAL "
		cQuery += "               , C5_NUM "
		cQuery += "               , C5_LIBEROK "
		cQuery += "               , C5_XPEDWEB "
		cQuery += "               , C5_NOTA "
		cQuery += "               , C5_BLQ "
		cQuery += "               , ROW_NUMBER() OVER ( ORDER BY C5_NUM ) AS ROWNUM "
		cQuery += "            FROM "+ RetSqlName("SC5") +" (NOLOCK) "
		cQuery += "           WHERE C5_FILIAL  =  '"+ xFilial("SC5") +"' "
		cQuery += "             AND C5_EMISSAO BETWEEN '"+ cDatIni +"' AND '"+ cDatIni +"' "
		IF !Empty(Self:cPedOri)
			cQuery += "    AND UPPER(C5_ORIGEM) LIKE '%"+ Upper( Self:cPedOri  ) +"%' "
		Endif
		cQuery += "             AND C5_XPEDWEB <> '' "
		cQuery += "             AND D_E_L_E_T_ =  ' ' ) AS TABFIM "
		cQuery += "  WHERE TABFIM.ROWNUM BETWEEN "+ AllTrim( Str( nInicio ) ) +" AND "+ AllTrim( Str( nInicio + nQuant - 1 ) ) +" "
		cAlias := GetNextAlias()
		DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
		If (cAlias)->( Eof())
			SetRestFault(400,EncodeUTF8('N�o existem dados para serem apresentados'))
			lRet := .F.
			lProc := .F.
		EndIf
		
	EndIf
	
	If lProc
		
		DbSelectArea( cAlias )
		(cAlias)->( DbGoTop() )
		While (cAlias)->( !Eof())
			
			//Cria um objeto JSON
			oData := JsonObject():New()
			
			// Adiciona elemento
			oData['IdPedProtheus'] := AllTrim( (cAlias)->C5_NUM )
			oData['IdPedVtex'] := AllTrim( (cAlias)->C5_XPEDWEB )
			
			//Se o pedido tiver em aberto no Protheus
			If Empty( (cAlias)->C5_LIBEROK ) .And. Empty( (cAlias)->C5_NOTA ) .And. Empty( (cAlias)->C5_BLQ )
				oData['IdStatus'] := "ready-for-handling"
				oData['TpStatus'] := "Pronto+para+o+manuseio"
				
			//Pedido encerrado (faturado) no Protheus
			ElseIf !Empty( (cAlias)->C5_NOTA ) .And. (cAlias)->C5_LIBEROK == "E" .And. Empty( (cAlias)->C5_BLQ )
				oData['IdStatus'] := "invoiced"
				oData['TpStatus'] := "Faturado"
				
			//Se o pedido for eliminado por res�duo, ou seja, cancelado no Protheus
			ElseIf (cAlias)->C5_NOTA == "XXXXXXXXX" .And. (cAlias)->C5_LIBEROK == "S" .And. Empty( (cAlias)->C5_BLQ )
				oData['IdStatus'] := "canceled"
				oData['TpStatus'] := "Cancelado"
				
			EndIf
			
			// Identificar se � para presente
			lIsGift := .F.
			IsGift( (cAlias)->C5_FILIAL, (cAlias)->C5_NUM, @lIsGift, @cMsgGift )
			oData['IsGift']  := lIsGift
			oData['MsgGift'] := cMsgGift
			aAdd( aData, oData )
			FreeObj( oData )
			
			(cAlias)->(dbSkip())
		EndDo
		
		//Define o retorno do m�todo
		cJson := FwJsonSerialize( aData )
		cJsonFim := '{"inicio": '+ AllTrim( Str( nInicio ) ) +','
		cJsonFim += '"quantidade": '+ AllTrim( Str( nQuant) ) +','
		cJsonFim += '"totalitens": '+ AllTrim( Str( nTotReg ) ) +','
		cJsonFim += '"items": '+cJson+'}'
		
		Self:SetResponse(cJsonFim)
		(cAlias)->(DbCloseArea())
		
	EndIf
		
Return lRet


//FatOne - Busca faturamento por c�digo de pedido vtex
//Modelo = http://homolog.grupohope.com:8081/pedidos/faturamento/faturamento?FatVtex=551302
WSMETHOD GET FatOne PATHPARAM FatVtex WSRECEIVE PedVtex WSSERVICE Pedidos
	
	Local cAlias 		:= ""
	Local cQuery 		:= ""
	Local aData 		:= {}
	Local lRet 			:= .T.
	Local lProc 		:= .T.
	Local oData 		:= Nil
	
	
	//obrigatorio informar o numero do pedido
	IF Empty(Self:FatVtex)
		SetRestFault(400,EncodeUTF8('O parametro FatVtex � obrigat�rio'))
		lRet := .F.
		lProc := .F.
	EndIF
	
	If lProc
		//montagem da consulta
		cQuery := " "
		cQuery := " SELECT SC5.C5_NUM "
		cQuery += "      , SC5.C5_XPEDWEB "
		cQuery += "      , SF2.F2_DOC "
		cQuery += "      , SF2.F2_SERIE "
		cQuery += "      , SF2.F2_EMISSAO "
		cQuery += "      , SF2.F2_HORA "
		cQuery += "      , SF2.F2_VALFAT "
		cQuery += "      , SF2.F2_CHVNFE "
		cQuery += "   FROM "+ RetSqlName("SC5") +" SC5 (NOLOCK) "
		cQuery += "  INNER JOIN "+ RetSqlName("SF2") +" SF2 (NOLOCK) "
		cQuery += "     ON SF2.F2_FILIAL  =  SC5.C5_FILIAL "
		cQuery += "    AND SF2.F2_DOC     =  SC5.C5_NOTA "
		cQuery += "    AND SF2.F2_SERIE   =  SC5.C5_SERIE "
		cQuery += "    AND SF2.F2_CLIENTE =  SC5.C5_CLIENTE "
		cQuery += "    AND SF2.F2_LOJA    =  SC5.C5_LOJACLI "
		cQuery += "    AND SF2.F2_CHVNFE  <> '' "
		cQuery += "    AND SF2.D_E_L_E_T_ =  ' ' "
		cQuery += "  WHERE SC5.C5_FILIAL  =  '"+ xFilial("SC5") +"' "
		cQuery += "    AND SC5.C5_XPEDWEB =  '"+ AllTrim(Self:FatVtex) +"' "
		cQuery += "    AND SC5.C5_NOTA    <> '' "
		cQuery += "    AND SC5.C5_BLQ     =  '' "
		cQuery += "    AND SC5.D_E_L_E_T_ =  ' ' "
		cAlias := GetNextAlias()
		DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
		If (cAlias)->( Eof())
			SetRestFault( 400, EncodeUTF8('N�o existem dados para serem apresentados') )
			lRet := .F.
			lProc := .F.
		EndIf
		
	EndIf
	
	If lProc
		
		DbSelectArea( cAlias )
		(cAlias)->( DbGoTop() )
		While (cAlias)->( !Eof())
			
			//Cria um objeto JSON
			oData := JsonObject():New()
			
			// Adiciona elemento
			oData['IdPedProtheus'] 	:= AllTrim( (cAlias)->C5_NUM )
			oData['IdPedVtex'] 		:= AllTrim( (cAlias)->C5_XPEDWEB )
			oData['IdStatus'] 		:= "invoiced"
			oData['TpStatus'] 		:= "Faturado"
			oData['NotaFiscal'] 	:= AllTrim( (cAlias)->F2_DOC )
			oData['SerieFiscal'] 	:= AllTrim( (cAlias)->F2_SERIE )
			oData['DataFatura'] 	:= Transform( GravaData( SToD( (cAlias)->F2_EMISSAO ), .F., 8 ), "@R 9999-99-99") +" "+ Transform( (cAlias)->F2_HORA+":00", "@R 99:99:99" )
			oData['ValorNota'] 		:= StrTran( StrTran( AllTrim( Transform( (cAlias)->F2_VALFAT, "@E 99,999,999,999.99" ) ) , ".", "" ), ",", "." )
			oData['CodigoNota'] 	:= AllTrim( (cAlias)->F2_CHVNFE )
			aAdd(aData,oData)
			FreeObj( oData )
			
			(cAlias)->(dbSkip())
		EndDo
		
		//Define o retorno do m�todo
		Self:SetResponse( FwJsonSerialize(aData) )
		(cAlias)->(DbCloseArea())
		
	EndIf
	
Return lRet


//FatAll - Busca Faturamento por per�odo
//Modelo = http://homolog.grupohope.com:8081/pedidos/faturamento/all/faturamento?DatIni=2020-01-01 00:01:00&DatFim=2020-01-02 23:59:00&nInicio=1&nQuant=10
WSMETHOD GET FatAll PATHPARAM DatIni, DatFim, nInicio, nQuant, cPedOri WSRECEIVE DatIni, DatFim, nInicio, nQuant, cPedOri WSSERVICE Pedidos
	
	Local cAlias 		:= ""
	Local cQuery 		:= ""
	Local cDatIni 		:= ""
	Local cDatFim 		:= ""
	Local cHorIni 		:= ""
	Local cHorFim 		:= ""
	Local cJson 		:= ""
	Local cJsonFim 		:= ""
	Local cPedOri 		:= ""
	Local nInicio 		:= 0
	Local nQuant 		:= 0
	Local nTotReg 		:= 0
	Local aData 		:= {}
	Local lRet 			:= .T.
	Local lProc 		:= .T.
	Local oData 		:= Nil
	
	
	//obrigatorio informar o numero do pedido ::aURLParms
	If Empty(Self:DatIni)
		SetRestFault(400,EncodeUTF8('O parametro DatIni � obrigat�rio'))
		lRet := .F.
		lProc := .F.
	EndIf
	
	If lProc
		If Empty(Self:DatFim)
			SetRestFault(400,EncodeUTF8('O parametro DatFim � obrigat�rio'))
			lRet := .F.
			lProc := .F.
		EndIf
	EndIf
	
	If lProc
		IF Empty(Self:nInicio)
			SetRestFault(400,EncodeUTF8('O parametro nInicial � obrigat�rio'))
			lRet := .F.
			lProc := .F.
		EndIF
	EndIF
	
	If lProc
		IF Empty(Self:nQuant)
			SetRestFault(400,EncodeUTF8('O parametro nQuant � obrigat�rio'))
			lRet := .F.
			lProc := .F.
		EndIF
	EndIF
	
	If lProc
		// Tratamento para data e hora
		cDatIni := ""
		cDatFim := ""
		cHorIni := ""
		cHorFim := ""
		cDatIni := StrTran( SubString( Self:DatIni, 01, 10 ), "-", "" ) //AAAA-MM-DD HH:MM:SS //data e hora de faturamento da NF-e
		cDatFim := StrTran( SubString( Self:DatFim, 01, 10 ), "-", "" )
		cHorIni := SubString( Self:DatIni, 12, 05 ) //AAAA-MM-DD HH:MM:SS //data e hora de faturamento da NF-e
		cHorFim := SubString( Self:DatFim, 12, 05 )
		
		// Montagem da consulta
		cQuery := " "
		cQuery := " SELECT COUNT(*) AS [TOT_REG] "
		cQuery += "   FROM "+ RetSqlName("SC5") +" SC5 (NOLOCK) "
		cQuery += "  INNER JOIN "+ RetSqlName("SF2") +" SF2 (NOLOCK) "
		cQuery += "     ON SF2.F2_FILIAL  =  SC5.C5_FILIAL "
		cQuery += "    AND SF2.F2_DOC     =  SC5.C5_NOTA "
		cQuery += "    AND SF2.F2_SERIE   =  SC5.C5_SERIE "
		cQuery += "    AND SF2.F2_CLIENTE =  SC5.C5_CLIENTE "
		cQuery += "    AND SF2.F2_LOJA    =  SC5.C5_LOJACLI "
		cQuery += "    AND SF2.F2_EMISSAO + SF2.F2_HORA >= '"+ cDatIni + cHorIni +"' "
		cQuery += "    AND SF2.F2_EMISSAO + SF2.F2_HORA <= '"+ cDatFim + cHorFim +"' "
		cQuery += "    AND SF2.F2_CHVNFE  <> '' "
		cQuery += "    AND SF2.D_E_L_E_T_ =  ' ' "
		cQuery += "  WHERE SC5.C5_FILIAL  =  '"+ xFilial("SC5") +"' "
		IF !Empty(Self:cPedOri)
			cQuery += "    AND UPPER(C5_ORIGEM) LIKE '%"+ Upper( Self:cPedOri  ) +"%' "
		Endif
		cQuery += "    AND SC5.C5_XPEDWEB <> '' "
		cQuery += "    AND SC5.C5_NOTA    <> '' "
		cQuery += "    AND SC5.C5_BLQ     =  '' "
		cQuery += "    AND SC5.D_E_L_E_T_ =  ' ' "
		cAlias := GetNextAlias()
		DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
		If (cAlias)->( Eof())
			SetRestFault(400,EncodeUTF8('N�o existem dados para serem apresentados'))
			lRet := .F.
			lProc := .F.
		Else
			nTotReg := 0
			nTotReg := (cAlias)->( TOT_REG )
		EndIf
		(cAlias)->( DbCloseArea() )
		
	EndIf
	
	If lProc
		// Tratamento para data e hora
		cDatIni := ""
		cDatFim := ""
		cHorIni := ""
		cHorFim := ""
		nInicio := 0
		nQuant  := 0
		cDatIni := StrTran( SubString( Self:DatIni, 01, 10 ), "-", "" ) //AAAA-MM-DD HH:MM:SS //data e hora de faturamento da NF-e
		cDatFim := StrTran( SubString( Self:DatFim, 01, 10 ), "-", "" )
		cHorIni := SubString( Self:DatIni, 12, 05 ) //AAAA-MM-DD HH:MM:SS //data e hora de faturamento da NF-e
		cHorFim := SubString( Self:DatFim, 12, 05 )
		nInicio := Val( Self:nInicio )
		nQuant  := Val( Self:nQuant )
		
		//montagem da consulta
		cQuery := " "
		cQuery := " SELECT * "
		cQuery += "   FROM ( SELECT SC5.C5_NUM "
		cQuery += "               , SC5.C5_XPEDWEB "
		cQuery += "               , SF2.F2_DOC "
		cQuery += "               , SF2.F2_SERIE "
		cQuery += "               , SF2.F2_EMISSAO "
		cQuery += "               , SF2.F2_HORA "
		cQuery += "               , SF2.F2_VALFAT "
		cQuery += "               , SF2.F2_CHVNFE "
		cQuery += "               , ROW_NUMBER() OVER ( ORDER BY SF2.F2_DOC ) AS ROWNUM "
		cQuery += "            FROM "+ RetSqlName("SC5") +" SC5 (NOLOCK) "
		cQuery += "           INNER JOIN "+ RetSqlName("SF2") +" SF2 (NOLOCK) "
		cQuery += "              ON SF2.F2_FILIAL  =  SC5.C5_FILIAL "
		cQuery += "             AND SF2.F2_DOC     =  SC5.C5_NOTA "
		cQuery += "             AND SF2.F2_SERIE   =  SC5.C5_SERIE "
		cQuery += "             AND SF2.F2_CLIENTE =  SC5.C5_CLIENTE "
		cQuery += "             AND SF2.F2_LOJA    =  SC5.C5_LOJACLI "
		cQuery += "             AND SF2.F2_EMISSAO + SF2.F2_HORA >= '"+ cDatIni + cHorIni +"' "
		cQuery += "             AND SF2.F2_EMISSAO + SF2.F2_HORA <= '"+ cDatFim + cHorFim +"' "
		cQuery += "             AND SF2.F2_CHVNFE  <> '' "
		cQuery += "             AND SF2.D_E_L_E_T_ =  ' ' "
		cQuery += "           WHERE SC5.C5_FILIAL  =  '"+ xFilial("SC5") +"' "
		cQuery += "             AND SC5.C5_XPEDWEB <> '' "
		cQuery += "             AND SC5.C5_NOTA    <> '' "
		IF !Empty(Self:cPedOri)
			cQuery += "    AND UPPER(C5_ORIGEM) LIKE '%"+ Upper( Self:cPedOri  ) +"%' "
		Endif
		cQuery += "             AND SC5.C5_BLQ     =  '' "
		cQuery += "             AND SC5.D_E_L_E_T_ =  ' ' ) AS TABFIM "
		cQuery += "  WHERE TABFIM.ROWNUM BETWEEN "+ AllTrim( Str( nInicio ) ) +" AND "+ AllTrim( Str( nInicio + nQuant - 1 ) ) +" "
		cAlias := GetNextAlias()
		DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
		If (cAlias)->( Eof())
			SetRestFault(400,EncodeUTF8('N�o existem dados para serem apresentados'))
			lRet := .F.
			lProc := .F.
		EndIf
		
	EndIf
	
	If lProc
		
		DbSelectArea( cAlias )
		(cAlias)->( DbGoTop() )
		While (cAlias)->( !Eof())
			
			//Cria um objeto JSON
			oData := JsonObject():New()
			
			// Adiciona elemento
			oData['IdPedProtheus'] 	:= AllTrim( (cAlias)->C5_NUM )
			oData['IdPedVtex'] 		:= AllTrim( (cAlias)->C5_XPEDWEB )
			oData['IdStatus'] 		:= "invoiced"
			oData['TpStatus'] 		:= "Faturado"
			oData['NotaFiscal'] 	:= AllTrim( (cAlias)->F2_DOC )
			oData['SerieFiscal'] 	:= AllTrim( (cAlias)->F2_SERIE )
			oData['DataFatura'] 	:= Transform( GravaData( SToD( (cAlias)->F2_EMISSAO ), .F., 8 ), "@R 9999-99-99") +" "+ Transform( (cAlias)->F2_HORA+":00", "@R 99:99:99" )
			oData['ValorNota'] 		:= StrTran( StrTran( AllTrim( Transform( (cAlias)->F2_VALFAT, "@E 99,999,999,999.99" ) ) , ".", "" ), ",", "." )
			oData['CodigoNota'] 	:= AllTrim( (cAlias)->F2_CHVNFE )
			aAdd(aData,oData)
			FreeObj( oData )
			
			(cAlias)->(dbSkip())
		EndDo
		
		//Define o retorno do m�todo
		cJson := FwJsonSerialize( aData )
		cJsonFim := '{"inicio": '+ AllTrim( Str( nInicio ) ) +','
		cJsonFim += '"quantidade": '+ AllTrim( Str( nQuant) ) +','
		cJsonFim += '"totalitens": '+ AllTrim( Str( nTotReg ) ) +','
		cJsonFim += '"items": '+cJson+'}'
		
		Self:SetResponse(cJsonFim)
		(cAlias)->(DbCloseArea())
		
	EndIf
		
Return lRet


//InsPed - Inclus�o do pedido de vendas
//Modelo = http://homolog.grupohope.com:8081/pedidos/pedidos/inc/
WSMETHOD POST InsPed WSRECEIVE WSRESTFUL pedidos
	
Local cAlias 			:= ""
Local cQuery 			:= ""
Local cJSon 			:= ""
Local cErro 			:= ""
Local cA1Cod 			:= ""
Local cA1Loja 			:= ""
Local cC5Tabela 		:= ""
Local cC5Condpag 		:= ""
Local cIdLincros 		:= ""
Local cC6Oper 			:= SuperGetMv( "MV_XOPER", .F., "01", "" )
Local cC6Descri 		:= ""
Local cC6Item 			:= "00"
Local n					:= 0
Local nSldPik 			:= 0
Local nSldPul 			:= 0
Local nXi 				:= 0
Local nXy 				:= 0
Local nTaxa				:= 0
Local nVlPIX			:= 0
Local aC5Cabec 			:= {}
Local aC6Linha 			:= {}
Local aC6Itens 			:= {}
Local aLogAuto 			:= {}
Local aArea 			:= {}
Local aData 			:= {}
Local aNotSld 			:= {}
Local aNotEnd 			:= {}
Local aPedido 			:= {}
Local lProc 			:= .T.
Local lRet 				:= .T.
Local lWsB2C 			:= SuperGetMV( "HP_WSB2C", .F., .F. ) // Habilita/Desabilita Integra��o Protheus x Lincros (B2C)
Local oJSon 			:= Nil
Local oItems 			:= Nil
Local oData 			:= Nil
Local nCondicao			:= 0
Local nPrazo			:= 0
Local aC5CondP			:= {}
Local aZBECond			:= {}
Local aCupom			:= {}
Local nLimCPO			:= FWTamSX3("C5_XPEDWEB")[1]
Local cFuncao			:= "HOPE001"
Local cMetodo			:= "1"
Local cEndPoint			:= "http://sistemas.hopelingerie.com.br:18012/rest/pedidos/inc/"
Local cPath				:= ""
Local cResult			:= ""
Local cAcao				:= "Incluir Pedido VTEX"
Local cTipError			:= "4"		// Erro na Comunicacao
Local cHrIni			:= Time()
Local cHrFim			:= Time()
Local cOrderId			:= ""
Local aAlertas			:= {}

Private cAmzPik 		:= AllTrim( SuperGetMV( "MV_XAMZPIC", .F., "E0" ) ) //Armazem padrao picking
Private cAmzPul 		:= AllTrim( SuperGetMV( "MV_XAMZPUL", .F., "E1" ) ) //Armazem padrao pulmao
Private lMsHelpAuto 	:= .T.
Private lMsErroAuto 	:= .F.
Private lAutoErrNoFile 	:= .T.
Private cToken			:= ""
	
aArea := GetArea()

BEGIN TRANSACTION 

	DbSelectArea("ZBE")	
	DbSelectArea("ZBF")	

	// Definindo o conte�do como JSon e validando estrutura
	Self:SetContentType("application/json")
	cJson 	:= Self:GetContent()
	oJson 	:= JsonObject():New()
	cErro 	:= oJson:FromJson( cJson )
	If Empty( cErro )
		cErro 	  := ""	// Quando nao tem erro no JSON a variavel fica NIL
	Else
		cTipError := "3"	// Erro no JSON
		cErro	  := "Falha ao obter o JSon (Parser)"
		lRet	  := .F.
		lProc	  := .F.
	EndIf
	
	If lProc
//	Daqui pra frente, se der erro sera nas TAGs
		cTipError := "2"	// Erro na TAG
		cOrderId  := oJson:GetJsonObject( "cC5XPedweb" )
		If Len( AllTrim( oJson:GetJsonObject( "cC5XPedweb" ) ) ) > nLimCpo
			cErro	  := EncodeUTF8('O numero do Pedido WEB / V-TEX tem tamanho superior a '  + str( nLimCPO, 3) + ' caracteres !' )
			lRet	  := .F.
			lProc	  := .F.
		Endif
	Endif

	If lProc
		// Cadastro do cliente
		Hope001A( oJSon, @cA1Cod, @cA1Loja, @cErro )
		If !Empty( @cErro )
			cErro	:= "Falha ao cadastrar o cliente: " + cErro
			lRet	:= .F.
			lProc	:= .F.
		EndIf
	EndIf
	
	If lProc
		If Empty( oJson:GetJsonObject( "cC5XPedweb" ) )
			cTipError := "2"	// Erro na TAG
			cErro	  := EncodeUTF8('Pedido web/ VTEX n�o informado') 
			lRet	  := .F.
			lProc	  := .F.
		EndIf
	EndIf
	
	If lProc
		cQuery := " "
		cQuery := " SELECT C5_NUM "
		cQuery += "      , C5_XPEDRAK "
		cQuery += "      , C5_XPEDWEB "
		cQuery += "   FROM "+ RetSqlName("SC5") +" (NOLOCK) "
		cQuery += "  WHERE C5_FILIAL  =  '"+ xFilial("SC5") +"' "
		cQuery += "    AND C5_XPEDWEB =  '"+ AllTrim( oJson:GetJsonObject( "cC5XPedweb" ) ) +"'
		cQuery += "    AND D_E_L_E_T_ =  ' ' "
		cAlias := GetNextAlias()
		DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
		If (cAlias)->( !Eof())
			cTipError:= "4"		// Erro na Comunicacao
			cErro	 := EncodeUTF8('Pedido web '+ AllTrim( (cAlias)->C5_XPEDWEB ) +' ja integrado com o Protheus. Pedido protheus '+ (cAlias)->C5_NUM +'.')
			lRet	 := .F.
			lProc	 := .F.
		EndIf
		(cAlias)->( DbCloseArea() )
	EndIf
	
	If lProc
		cC5Tabela := oJson:GetJsonObject( "cC5Tabela" )
		If Empty( cC5Tabela )
			cTipError := "2"	// Erro na TAG
			cErro	  := EncodeUTF8('Tabela de pre�os n�o informada.') 
			lRet 	  := .F.
			lProc 	  := .F.
		EndIf
	EndIf

  	If lProc
		// Pega as varias condicoes de pagamento ( Codigo: "MFP - Multiplas Formas de Pagamento")
		oCondPag := JSonObject():New()
		oCondPag := oJson:GetJsonObject( "FormaPG" )
		// Condi��o de pagamento
        
        If Len( oCondPag) == 0 
			cTipError := "2"	// Erro na TAG
			cErro	  := EncodeUTF8('Condi��o de pagamento n�o informada') 
			lRet	  := .F.
			lProc	  := .F.
        Else
			aCupom	:= {}
			cTipo	:= ""
			For nCondicao := 1 to Len( oCondPag )
                cCartao		:= oCondPag[nCondicao]:GetJsonObject( "cC5XCartao"	)   
                cBandeira	:= oCondPag[nCondicao]:GetJsonObject( "cC5XBande"	)    
                cNsu		:= oCondPag[nCondicao]:GetJsonObject( "cC5XNsu"		)      
                cCartID		:= oCondPag[nCondicao]:GetJsonObject( "cC5XCartid"	)   
                nValor		:= oCondPag[nCondicao]:GetJsonObject( "cC5XCredrk"	)   
                nParcela	:= oCondPag[nCondicao]:GetJsonObject( "cC5XParcel"	)   
                cTID		:= oCondPag[nCondicao]:GetJsonObject( "tid"			)          
                cAutCard	:= oCondPag[nCondicao]:GetJsonObject( "cC5XAutcar"	)   
                cDtAutorz	:= oCondPag[nCondicao]:GetJsonObject( "DtAutorz"	)
                cHrAutorz	:= oCondPag[nCondicao]:GetJsonObject( "HrAutorz"	)

 				cAux		:= "" 
 				cC5Condpag	:= ""
				aC5CondP	:= {}
				nPrazo		:= 0
				nTaxa		:= 0
				nVlPIX		:= 0
				If	  cCartao == 'CREDITO'
					nPrazo		:= 30
					cTipo		:= '1'
					cAux		:= ">> Bandeira: " + Alltrim( cBandeira )
					CondCartao( cCartao, cBandeira, nParcela, @cC5Condpag, @nTaxa )
				ElseIf cCartao == 'BOLETO'
					cTipo		:= '2'
					cC5Condpag	:= "V01"		// V01 - BOLETO (LJ VIRTUAL)                                   
				ElseIf cCartao == 'VALE-COMPRA'
					cTipo 		:= '3'
					cC5Condpag 	:= "070"		// A VISTA / ANTECIPADO                                                                           
					lProc		:= VerCUPOM( cCartID, "1", nValor, @aCupom, @cErro )
				ElseIf cCartao == 'VALE-PRESENTE'
					cTipo 		:= '4'
					cC5Condpag 	:= "070"		// A VISTA / ANTECIPADO                                                                           
					lProc		:= VerCUPOM( cCartID, "2", nValor, @aCupom, @cErro )
				ElseIf cCartao == 'BONIFICACAO'
					cTipo 		:= '5'
					cC5Condpag	:= "070"		// A VISTA / ANTECIPADO                                                                          
					lProc		:= VerCUPOM( cCartID, "3", nValor, @aCupom, @cErro )
				ElseIf cCartao == 'DEPOSITO'
					cTipo		:= '6'
					cC5Condpag	:= "070"		// A VISTA / ANTECIPADO                                                                          
				ElseIf cCartao == 'DEBITO'
					cTipo		:= '7'
					cC5Condpag	:= "070"		// A VISTA / ANTECIPADO                                                                          
				ElseIf cCartao == 'PIX'
					cTipo		:= '8'
					cC5Condpag	:= "PIX"		// A VISTA / ANTECIPADO                                                                          
					nTaxa		:= Posicione("SE4",1,xFilial("SE4")+cC5Condpag,"E4_XTXCC")	// Taxa %  PIX
					nVlPIX		:= Posicione("SE4",1,xFilial("SE4")+cC5Condpag,"E4_XVLPIX")	// Taxa R$ PIX
				Else
					cTipError := "2"	// Erro na TAG
					cErro	  := EncodeUTF8('Nao reconheceu a Condi��o de pagamento.' ) 
					lProc	  := .F.
				Endif
			
				If Empty( cC5Condpag )
					cTipError := "2"	// Erro na TAG
					cErro	  := EncodeUTF8('Condi��o de pagamento n�o encontrada no Protheus.' + cAux ) 
					lProc	  := .F.
				EndIf
				If lProc
					aAdd( aC5CondP,cC5Condpag   )
					aAdd( aC5CondP,cTipo		)
					aAdd( aC5CondP,nPrazo		)
					aAdd( aC5CondP,nTaxa		)
					aAdd( aC5CondP,cCartao		)
					aAdd( aC5CondP,cBandeira    )
					aAdd( aC5CondP,cNsu       	)
					aAdd( aC5CondP,cCartID    	)
					aAdd( aC5CondP,nValor    	)
					aAdd( aC5CondP,nParcela   	)
					aAdd( aC5CondP,cTID         )
					aAdd( aC5CondP,cAutCard    	)
					aAdd( aC5CondP,cDtAutorz    )
					aAdd( aC5CondP,cHrAutorz    )
					aAdd( aC5CondP,nVlPIX	    )
					
					aAdd( aZBECond,aC5CondP		)
				else
					Exit
				Endif
			Next
//			Variavel para colocar as parcelas no pedido
			lRet	  := lProc
			aParcelas := CalcParc( aZBECond )	
        Endif
	EndIf

	If lProc
        cC5Condpag	:= SuperGetMv("MV_MULPAG",,"MFP")// Condi��o de pagamento SE4 para multiplas formas de pagamento

		dbSelectArea("SA1")
		dbSetOrder(1) // A1_FILIAL+A1_COD+A1_LOJA
		If ! dbSeek( xFilial("SA1") + cA1Cod + cA1Loja ) // Posicionando no cliente correto de acordo com a consulta Hope001A
			cErro	:= "Falha ao cadastrar o cliente"
			lProc	:= .F.
			lRet	:= .F.
		Endif
	Endif
	If lProc

		aAdd( aC5Cabec, { "C5_FILIAL"  , xFilial("SC5")                             		, Nil } )
		aAdd( aC5Cabec, { "C5_EMISSAO" , SToD( oJson:GetJsonObject( "cC5Emissao" ) )		, Nil } )
		aAdd( aC5Cabec, { "C5_POLCOM"  , SuperGetMv( "MV_XPOLCO", .F., "018", "" )  		, Nil } )
		aAdd( aC5Cabec, { "C5_TPPED"   , SuperGetMv( "MV_XTPPED", .F., "016", "" )  		, Nil } )
		aAdd( aC5Cabec, { "C5_TIPO"    , SuperGetMv( "MV_XTIPO", .F., "N", "" )     		, Nil } )
		aAdd( aC5Cabec, { "C5_CLIENTE" , cA1Cod                                    	 		, Nil } )
		aAdd( aC5Cabec, { "C5_LOJACLI" , cA1Loja                                    		, Nil } )
		//aAdd( aC5Cabec, { "C5_DESCONT" , oJson:GetJsonObject( "cC5Descont" )				, Nil } )
		aAdd( aC5Cabec, { "C5_CONDPAG" , cC5Condpag                                 		, Nil } )
		If cC5Condpag == "MFP"
			For n := 1 to Len( aParcelas )
				aAdd( aC5Cabec, { "C5_PARC"+fConvHexa(n) , aParcelas[n][1]					, Nil } )
				aAdd( aC5Cabec, { "C5_DATA"+fConvHexa(n) , aParcelas[n][2]					, Nil } )
			Next n
		Else 
			aAdd( aC5Cabec, { "C5_XNSU"    , oJson:GetJsonObject( "cC5XNsu" )           	, Nil } )
			aAdd( aC5Cabec, { "C5_XAUTCAR" , oJson:GetJsonObject( "cC5XAutcar" )        	, Nil } )
			aAdd( aC5Cabec, { "C5_XCARTID" , oJson:GetJsonObject( "cC5XCartid" )        	, Nil } )
			aAdd( aC5Cabec, { "C5_XCREDRK" , oJson:GetJsonObject( "cC5XCredrk" )        	, Nil } )
		Endif
		aAdd( aC5Cabec, { "C5_FECENT"  , SToD( oJson:GetJsonObject( "cC5Fecent" ) ) 		, Nil } )
		aAdd( aC5Cabec, { "C5_XPEDRAK" , oJson:GetJsonObject( "cC5XPedRak" )        		, Nil } )
		aAdd( aC5Cabec, { "C5_XPEDWEB" , oJson:GetJsonObject( "cC5XPedweb" )       		 	, Nil } )
		aAdd( aC5Cabec, { "C5_DESCONT" , oJson:GetJsonObject( "cC5Descont" )        		, Nil } )
		aAdd( aC5Cabec, { "C5_CLIENT"  , cA1Cod                                     		, Nil } )
		aAdd( aC5Cabec, { "C5_LOJAENT" , cA1Loja                                    		, Nil } )
		aAdd( aC5Cabec, { "C5_DESPESA" , oJson:GetJsonObject( "cC5Despesa" )        		, Nil } )
		aAdd( aC5Cabec, { "C5_FRETE"   , oJson:GetJsonObject( "cC5Frete" )          		, Nil } )
		aAdd( aC5Cabec, { "C5_NATUREZ" , SuperGetMv( "MV_XNATUR", .F., "11116001", "" )  	, Nil } )
		aAdd( aC5Cabec, { "C5_XCANAL"  , SA1->A1_XCANAL                             		, Nil } )
		aAdd( aC5Cabec, { "C5_XCANALD" , SA1->A1_XCANALD                            		, Nil } )
		aAdd( aC5Cabec, { "C5_XCODREG" , SA1->A1_XCODREG                            		, Nil } )
		aAdd( aC5Cabec, { "C5_XDESREG" , SA1->A1_XDESREG                            		, Nil } )
		aAdd( aC5Cabec, { "C5_XMICRRE" , SA1->A1_XMICRRE                            		, Nil } )
		aAdd( aC5Cabec, { "C5_XMICRDE" , SA1->A1_XMICRDE                            		, Nil } )
		aAdd( aC5Cabec, { "C5_ORIGEM"  , oJson:GetJsonObject( "cC5Origem" )         		, Nil } )
		aAdd( aC5Cabec, { "C5_TABELA"  , cC5Tabela                                  		, Nil } )
		aAdd( aC5Cabec, { "C5_XOPCENT" , oJson:GetJsonObject( "cC5XOpcent" )        		, Nil } )
		aAdd( aC5Cabec, { "C5_XHORIMP" , cValToChar( SubStr( Time(),1,5 ) )         		, Nil } )
		aAdd( aC5Cabec, { "C5_XDATIMP" , dDataBase                                  		, Nil } )
		If lWsB2C .And. AllTrim( Upper( oJson:GetJsonObject( "cC5Origem" ) ) ) == "B2C"
			cIdLincros:= cValToChar( Val( GetMV( "ID_LINCROS" ) ) + 1 )
			PutMV( "ID_LINCROS", cIdLincros )
			aAdd( aC5Cabec, { "C5_XIDLINC" , cIdLincros                                 	, Nil } )
		Endif
		
		DbSelectArea("SB1")
		SB1->( DbSetOrder(1) ) //B1_FILIAL, B1_COD
		
		DbSelectArea("SF4")
		SF4->( DbSetOrder(1) ) //F4_FILIAL, F4_CODIGO
		oItems  := oJson:GetJsonObject( "Items" )
		For nXi := 1 To Len( oItems )
			
			If SB1->( !DbSeek( xFilial("SB1") + oItems[nXi]:GetJsonObject( "cC6Produto" ) ) )
				cTipError	:= "2"	// Erro na TAG
				cErro		:= EncodeUTF8('Produto '+ oItems[nXi]:GetJsonObject( "cC6Produto" ) +' n�o cadastrado no Protheus.') 
				lRet		:= .F.
				lProc		:= .F.
				Exit
			EndIf
			
			If SB1->B1_YBLQDEM == "S"
				cTipError	:= "2"	// Erro na TAG
				cErro		:= EncodeUTF8('Produto '+ SB1->B1_COD +' bloqueado por demanda.')
				lRet		:= .F.
				lProc		:= .F.
				Exit
			EndIf
			
			cC6Tes := ""
			cC6Tes  := MaTesInt( 2, cC6Oper, SA1->A1_COD, SA1->A1_LOJA, "C", SB1->B1_COD, )
			If Empty( cC6Tes )
				cTipError	:= "2"	// Erro na TAG
				cErro		:= EncodeUTF8('N�o existe um TES cadastrado para o tipo de opera��o ('+ cC6Oper +'), cliente('+ cC5Cliente + cC5Lojacli +') e produto('+ SB1->B1_COD +').')
				lRet		:= .F.
				lProc		:= .F.
				Exit
			EndIf
			
			// Regra de B2C do programa HAPIFAT.PRW
			If AllTrim( Upper( oJson:GetJsonObject( "cC5Origem" ) ) ) == "B2C"
				nSldPik := HFATSALDO( SB1->B1_COD, cAmzPik )
				nSldPul := HFATSALDO( SB1->B1_COD, cAmzPul )
				
				If oItems[nXi]:GetJsonObject( "cC6Qtdven" ) > ( nSldPik + nSldPul )
					If GETMV( "HP_SLDB2C", .T., .T. )
						aAdd( aNotSld, { SB1->B1_COD } )
					EndIf
				EndIf
				
				DbSelectArea("SBE")
				SBE->( DbOrderNickName("PRODUTO") )
				If SBE->( !DbSeek( xFilial("SBE") + SB1->B1_COD ) )
					aAdd( aNotEnd, { SB1->B1_COD } )
				EndIf
			EndIf
			
			cC6Descri := ""
			cC6Descri := AllTrim( SB1->B1_DESC )
			
			aC6Linha := {}
			cC6Item := Soma1( cC6Item )
			aAdd( aC6Linha, { "C6_FILIAL" 	, xFilial("SC6")                                    , Nil } )
			aAdd( aC6Linha, { "C6_ITEM" 	, cC6Item                                           , Nil } )
			aAdd( aC6Linha, { "C6_PRODUTO" 	, oItems[nXi]:GetJsonObject( "cC6Produto" )         , Nil } )
			aAdd( aC6Linha, { "C6_DESCRI" 	, cC6Descri                                         , Nil } )
			aAdd( aC6Linha, { "C6_QTDVEN" 	, oItems[nXi]:GetJsonObject( "cC6Qtdven" )          , Nil } )
			aAdd( aC6Linha, { "C6_PRCVEN" 	, oItems[nXi]:GetJsonObject( "cC6Prcven" )          , Nil } )
			aAdd( aC6Linha, { "C6_PRUNIT" 	, oItems[nXi]:GetJsonObject( "cC6Prunit" )          , Nil } )
			aAdd( aC6Linha, { "C6_VALOR" 	, oItems[nXi]:GetJsonObject( "cC6Valor" )           , Nil } )
			aAdd( aC6Linha, { "C6_VALDESC" 	, oItems[nXi]:GetJsonObject( "cC6Valdesc" )         , Nil } )
			aAdd( aC6Linha, { "C6_OPER" 	, cC6Oper                                           , Nil } )
			aAdd( aC6Linha, { "C6_TES" 		, cC6Tes                                            , Nil } )
			aAdd( aC6Linha, { "C6_LOCAL" 	, SuperGetMv( "MV_XLOCAL", .F., "01", "" )          , Nil } )
			aAdd( aC6Linha, { "C6_GRADE" 	, SuperGetMv( "MV_XGRADE", .F., "N", "" )           , Nil } )
			aAdd( aC6Linha, { "C6_XPRESEN" 	, DecodeUTF8(oItems[nXi]:GetJsonObject( "cC6XPresen" ))         , Nil } )
			aAdd( aC6Linha, { "C6_XITPRES" 	, oItems[nXi]:GetJsonObject( "cC6Xitpres" )         , Nil } )
			aAdd( aC6Linha, { "C6_ENTREG" 	, SToD( oItems[nXi]:GetJsonObject( "cC6Entreg" ) )  , Nil } )
			aAdd( aC6Itens, aC6Linha )
		Next nXi
		
		If Len( aNotSld ) > 0
			cTipError:= "2"	// Erro na TAG
			cErro	 := "Produtos sem saldo suficiente: "
			For nXy  := 1 To Len( aNotSld )
				cErro += Iif( nXy == 1, aNotSld[nXy][1], ", "+ aNotSld[nXy][1] )
			Next nXy
			lRet	 := .F.
			lProc	 := .F.
		EndIf
		
		If Len( aNotEnd ) > 0
			cTipError := "2"	// Erro na TAG
			cErro	  := ""
			cErro	  := "Produtos sem amarracao com endereco de picking: "
			For nXy := 1 To Len( aNotEnd )
				cErro += Iif( nXy == 1, aNotEnd[nXy][1], ", "+ aNotEnd[nXy][1] )
			Next nXy
			lRet	  := .F.
			lProc	  := .F.
		EndIf
		
		If lProc
			MsExecAuto( {|x, y, z| MATA410(x, y, z) }, aC5Cabec, aC6Itens, 3 )
			If !lMsErroAuto
				//Define o retorno do m�todo
				oData := JsonObject():New()
				oData['IdStatus'] 		:= "ready-for-handling"
				oData['TpStatus'] 		:= "Pronto+para+o+manuseio"
				oData['IdPedVtex'] 		:= AllTrim( SC5->C5_XPEDWEB )
				oData['IdPedProtheus'] 	:= AllTrim( SC5->C5_NUM )
				If AllTrim( Upper( oJson:GetJsonObject( "cC5Origem" ) ) ) == "B2C"
					aAdd( aPedido, { SC5->C5_NUM, SA1->A1_COD, SA1->A1_LOJA } )
					U_HFTGER002( aPedido, .T., .F. )
					If cC5Condpag == "MFP"
						fCriaZBE( SC5->C5_FILIAL, SC5->C5_NUM, aZBECond )
					Endif
					If Len( aCupom ) > 0
						fCriaCup( aCupom )
					Endif
				EndIf
				aAdd( aData, oData )
				FreeObj( oData )
				cResult := FwJsonSerialize( aData )
				Self:SetResponse( cResult )
				lRet	:= .T.
			Else
				cTipError := "2"	// Erro na TAG
				cErro     := ""
				aLogAuto  := {}
				aLogAuto  := GetAutoGrLog()
				For nXy   := 1 To Len( aLogAuto )
					cErro += aLogAuto[nXy] + Chr(13)+Chr(10)
				Next nXy
				cErro	  := NoAcento(cErro) 
				lRet	  := .F.
			EndIf

		EndIf

		SF4->( DbCloseArea() )
		SB1->( DbCloseArea() )
		SA1->( DbCloseArea() )
		
	EndIf
	If ! lRet
		If Empty( cErro )
			cErro	:= "Erro indeterminado"
		Endif	
		cResult	:= cErro
		SetRestFault( 400, cErro )
	Endif
	If ExistBlock("GRAVALOG")
		cHrFim		:= Time()
		aAlertas	:= aLogAuto
		U_GravaLog( cFuncao, cMetodo, ( cEndPoint + cPath ), cJson, cResult, cAcao, iif(lRet,"1",cTipError), cHrIni , cHrFim, "SC5", cOrderId, , aAlertas )
	EndIf

END TRANSACTION 	
RestArea( aArea )
Return lRet


//UpdPed - Altera��o do pedido de vendas
//Modelo = http://homolog.grupohope.com:8081/pedidos/pedidos/alt/
WSMETHOD PUT UpdPed WSRECEIVE WSRESTFUL pedidos
	
Local cQuery 			:= ""
Local cAlias 			:= ""
Local cJSon 			:= ""
Local cErro 			:= ""
Local cC5Num 			:= ""
Local cC5XPedweb 		:= ""
Local cC5Cliente 		:= ""
Local cC5Lojacli 		:= ""
Local cC5Tabela 		:= ""
Local cC5Condpag 		:= ""
Local cIdLincros 		:= ""
Local cC6Oper 			:= SuperGetMv( "MV_XOPER", .F., "01", "" )
Local cC6Tes 			:= ""
Local cC6Descri 		:= ""
Local cC6Item 			:= "00"
Local nSldPik 			:= 0
Local nSldPul 			:= 0
Local nXi 				:= 0
Local nXy 				:= 0
Local aC5Cabec 			:= {}
Local aC6Linha 			:= {}
Local aC6Itens 			:= {}
Local aLogAuto 			:= {}
Local aArea 			:= {}
Local aData 			:= {}
Local aNotSld 			:= {}
Local aNotEnd 			:= {}
Local lProc 			:= .T.
Local lRet 				:= .T.
Local lWsB2C 			:= SuperGetMV( "HP_WSB2C", .F., .F. ) // Habilita/Desabilita Integra��o Protheus x Lincros (B2C)
Local oJSon 			:= Nil
Local oItems 			:= Nil
Local oData 			:= Nil
Local aC5CondP			:= {}
Local nLimCPO			:= FWTamSX3("C5_XPEDWEB")[1]
Local nCondicao			:= 0
Local aCondPag			:= {}
Local n					:= 0
Private cAmzPik 		:= AllTrim( SuperGetMV( "MV_XAMZPIC", .F., "E0" ) ) //Armazem padrao picking
Private cAmzPul 		:= AllTrim( SuperGetMV( "MV_XAMZPUL", .F., "E1" ) ) //Armazem padrao pulmao
Private lMsHelpAuto 	:= .T.
Private lMsErroAuto 	:= .F.
Private lAutoErrNoFile 	:= .T.

aArea := GetArea()

BEGIN TRANSACTION
	
	// Definindo o conte�do como JSon e validando estrutura
	Self:SetContentType("application/json")
	cJson 	:= Self:GetContent()
//	cJson   := MEMOREAD( "C:\TEMP\TESTE.JSON")
	oJson 	:= JsonObject():New()
	cErro 	:= oJson:FromJson( cJson )
	If !Empty( cErro )
		SetRestFault( 400, "Falha ao obter o JSon (Parser)" )
		lRet := .F.
		lProc := .F.
	EndIf
	
	If lProc
		If Len( AllTrim( oJson:GetJsonObject( "cC5XPedweb" ) ) ) > nLimCpo
			cErro	 := EncodeUTF8('O numero do Pedido WEB / V-TEX tem tamanho superior a '  + str( nLimCPO, 3) + ' caracteres !' )
			lRet	 := .F.
			lProc	 := .F.
			SetRestFault( 400, cErro )
		Endif
	Endif

	If lProc
		If Empty( oJson:GetJsonObject( "cC5XPedweb" ) )
			cErro := EncodeUTF8('Pedido WEB / V-TEX n�o informado') 
			lRet  := .F.
			lProc := .F.
			SetRestFault( 400, cErro )
		EndIf
	EndIf
	
	If lProc
		cQuery := " "
		cQuery := " SELECT C5_NUM "
		cQuery += "      , C5_XPEDRAK "
		cQuery += "      , C5_XPEDWEB "
		cQuery += "      , C5_CLIENTE "
		cQuery += "      , C5_LOJACLI "
		cQuery += "   FROM "+ RetSqlName("SC5") +" (NOLOCK) "
		cQuery += "  WHERE C5_FILIAL  =  '"+ xFilial("SC5") +"' "
		cQuery += "    AND C5_XPEDWEB =  '"+ AllTrim( oJson:GetJsonObject( "cC5XPedweb" ) ) +"' "
		cQuery += "    AND D_E_L_E_T_ =  ' ' "
		cAlias := GetNextAlias()
		DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
		If (cAlias)->( Eof())
			cErro	:= EncodeUTF8('Pedido web '+ AllTrim( oJson:GetJsonObject( "cC5XPedweb" ) ) +' n�o integrado com o Protheus.' )
			lRet	:= .F.
			lProc	:= .F.
			SetRestFault( 400, cErro )
		Else
			cC5Num 		:= (cAlias)->C5_NUM
			cC5XPedweb 	:= (cAlias)->C5_XPEDWEB
			cC5Cliente 	:= (cAlias)->C5_CLIENTE
			cC5Lojacli 	:= (cAlias)->C5_LOJACLI
		EndIf
		(cAlias)->( DbCloseArea() )
	EndIf
	
	If lProc
		
		If Upper( AllTrim( oJson:GetJsonObject( "IdStatus" ) ) ) == "CANCELED"
			DbSelectArea("SC5")
			SC5->( DbSetOrder(1) )//C5_FILIAL, C5_NUM
			SC5->( DbGoTop() )
			If SC5->( DbSeek( xFilial("SC5") + cC5Num ) )
				If Empty( SC5->C5_NOTA ) .Or. SC5->C5_NOTA == "XXXXXXXXX"
					RecLock( "SC5", .F. )
						Replace C5_LIBEROK 	With "S"
						Replace C5_NOTA 	With "XXXXXXXXX"
					SC5->( MsUnLock() )
				Else
					lProc := .F.
				EndIf
			EndIf
			SC5->( DbCloseArea() )
			
			If lProc
				DbSelectArea("SC6")
				SC6->( DbSetOrder(1) )//C6_FILIAL, C6_NUM, C6_ITEM, C6_PRODUTO
				SC6->( DbGoTop() )
				If SC6->( DbSeek( xFilial("SC6") + cC5Num ) )
					While SC6->( !EoF() ) .And. SC6->C6_NUM == cC5Num
						If AllTrim( SC6->C6_BLQ ) == "" .And. AllTrim( SC6->C6_NOTA ) == ""
							RecLock( "SC6", .F. )
								Replace C6_BLQ 	With "R"
							SC6->( MsUnLock() )
						EndIf
					SC6->( DbSkip() )
					EndDo
				EndIf
				SC6->( DbCloseArea() )
				
				oData := JsonObject():New()
				oData['IdStatus'] 		:= "canceled"
				oData['TpStatus'] 		:= "Cancelado"
				oData['IdPedVtex'] 		:= AllTrim( cC5XPedweb )
				oData['IdPedProtheus'] 	:= AllTrim( cC5Num )
				aAdd( aData, oData )
				FreeObj( oData )
				Self:SetResponse( FwJsonSerialize( aData ) )
				
				lRet := .T.
				lProc := .F.
			Else
				lProc := .T.
			EndIf
			
		EndIf
		
	EndIf
	
	If lProc
		cC5Tabela := oJson:GetJsonObject( "cC5Tabela" )
		If Empty( cC5Tabela )
			cErro	:= EncodeUTF8('Tabela de pre�os n�o informada.')
			lRet	:= .F.
			lProc	:= .F.
			SetRestFault( 400, cErro )
		EndIf
	EndIf
	
	If lProc
		// Pega as varias condicoes de pagamento ( Codigo: "MFP - Multiplas Formas de Pagamento")
		oCondPag := JSonObject():New()
		oCondPag := oJson:GetJsonObject( "cC5XCondPag" )
		// Condi��o de pagamento
        
        If Len( oCondPag) == 1

        	cC5Condpag := ""
			CondCartao( oCondPag[nCondicao]:GetJsonObject( "cC5XCartao" ), oCondPag[nCondicao]:GetJsonObject( "cC5XBande" ), oCondPag[nCondicao]:GetJsonObject( "cC5XParcel" ), @cC5Condpag, 0 )
			If Empty( cC5Condpag )
				cErro	:= EncodeUTF8('Condi��o de pagamento n�o encontrada no Protheus.')
				lRet	:= .F.
				lProc	:= .F.
				SetRestFault( 400, cErro )
			EndIf

        Else
            cC5Condpag := SuperGetMv("MV_MULPAG",,"MFP")// Condi��o de pagamento SE4 para multiplas formas de pagamento
            For nCondicao := 1 to Len( oCondPag )
                aAdd( aC5CondP,{ "C5_XNSU"     , oCondPag[nCondicao]:GetJsonObject( "cC5XNsu" )            , Nil } )
                aAdd( aC5CondP,{ "C5_XAUTCAR"  , oCondPag[nCondicao]:GetJsonObject( "cC5XAutcar" )         , Nil } )
                aAdd( aC5CondP,{ "C5_XCARTID"  , oCondPag[nCondicao]:GetJsonObject( "cC5XCartid" )         , Nil } )
                aAdd( aC5CondP,{ "C5_XCREDRK"  , oCondPag[nCondicao]:GetJsonObject( "cC5XCredrk" )         , Nil } )
            
			Next
			aCondPag := CalcParc( aC5CondP )
        Endif
	EndIf
	
	If lProc
		
		aAdd( aC5Cabec,{ "C5_FILIAL"   , xFilial("SC5")                              , Nil } )
		aAdd( aC5Cabec,{ "C5_NUM"      , cC5Num                                      , Nil } )
		aAdd( aC5Cabec,{ "C5_EMISSAO"  , SToD( oJson:GetJsonObject( "cC5Emissao" ) ) , Nil } )
		aAdd( aC5Cabec,{ "C5_POLCOM"   , SuperGetMv( "MV_XPOLCO", .F., "018", "" )   , Nil } )
		aAdd( aC5Cabec,{ "C5_TPPED"    , SuperGetMv( "MV_XTPPED", .F., "016", "" )   , Nil } )
		aAdd( aC5Cabec,{ "C5_TIPO"     , SuperGetMv( "MV_XTIPO", .F., "N", "" )      , Nil } )
		aAdd( aC5Cabec,{ "C5_CONDPAG"  , cC5Condpag                                  , Nil } )
		aAdd( aC5Cabec,{ "C5_FECENT"   , SToD( oJson:GetJsonObject( "cC5Fecent" ) )  , Nil } )
		aAdd( aC5Cabec,{ "C5_XPEDRAK"  , oJson:GetJsonObject( "cC5XPedRak" )         , Nil } )
		aAdd( aC5Cabec,{ "C5_XPEDWEB"  , oJson:GetJsonObject( "cC5XPedweb" )         , Nil } )
		aAdd( aC5Cabec,{ "C5_DESPESA"  , oJson:GetJsonObject( "cC5Despesa" )         , Nil } )
		aAdd( aC5Cabec,{ "C5_FRETE"    , oJson:GetJsonObject( "cC5Frete" )           , Nil } )
		aAdd( aC5Cabec,{ "C5_NATUREZ"  , SuperGetMv( "MV_XNATUR", .F., "11116001", "" )  , Nil } )

		For n := 1 to Len(aCondPag)
			aAdd( aC5Cabec,{ "C5_PARC"+n  , aCondPag[1]  , Nil } )
			aAdd( aC5Cabec,{ "C5_DATA"+n  , aCondPag[2]  , Nil } )
			aAdd( aC5Cabec,{ "C5_XCODZBE" , aCondPag[3]  , Nil } )
			aAdd( aC5Cabec,{ "C5_XITEZBE" , aCondPag[4]  , Nil } )
		Next n

		aAdd( aC5Cabec,{ "C5_ORIGEM"   , oJson:GetJsonObject( "cC5Origem" )          , Nil } )
		aAdd( aC5Cabec,{ "C5_TABELA"   , cC5Tabela                                   , Nil } )
		aAdd( aC5Cabec,{ "C5_XOPCENT"  , oJson:GetJsonObject( "cC5XOpcent" )         , Nil } )
		aAdd( aC5Cabec,{ "C5_XHORIMP"  , cValToChar( SubStr( Time(),1,5 ) )          , Nil } )
		aAdd( aC5Cabec,{ "C5_XDATIMP"  , dDataBase                                   , Nil } )
		If lWsB2C .And. AllTrim( Upper( oJson:GetJsonObject( "cC5Origem" ) ) ) == "B2C"
			cIdLincros:= cValToChar( Val( GetMV( "ID_LINCROS" ) ) + 1 )
			PutMV( "ID_LINCROS", cIdLincros )
			aAdd( aC5Cabec, { "C5_XIDLINC"  , cIdLincros                                 , Nil } )
		Endif
		
		DbSelectArea("SB1")
		SB1->( DbSetOrder(1) ) //B1_FILIAL, B1_COD
		
		DbSelectArea("SF4")
		SF4->( DbSetOrder(1) ) //F4_FILIAL, F4_CODIGO
		oItems  := oJson:GetJsonObject( "Items" )
		For nXi := 1 To Len( oItems )
			
			If SB1->( !DbSeek( xFilial("SB1") + oItems[nXi]:GetJsonObject( "cC6Produto" ) ) )
				cErro	:= EncodeUTF8('Produto '+ oItems[nXi]:GetJsonObject( "cC6Produto" ) +' nao cadastrado no Protheus.')
				lRet	:= .F.
				lProc	:= .F.
				SetRestFault( 400, cErro )
				Exit
			EndIf
			
			If SB1->B1_YBLQDEM == "S"
				cErro	:= EncodeUTF8('Produto '+ SB1->B1_COD +' bloqueado por demanda.')
				lRet	:= .F.
				lProc	:= .F.
				SetRestFault( 400, cErro )
				Exit
			EndIf
			
			cC6Tes := ""
			cC6Tes := MaTesInt( 2, cC6Oper, cC5Cliente, cC5Lojacli, "C", SB1->B1_COD, )
			If Empty( cC6Tes )
				cErro	:= EncodeUTF8('N�o existe um TES cadastrado para o tipo de opera��o ('+ cC6Oper +'), cliente('+ cC5Cliente + cC5Lojacli +') e produto('+ SB1->B1_COD +').')
				lRet	:= .F.
				lProc	:= .F.
				SetRestFault( 400, cErro )
				Exit
			EndIf
			
			// Regra de B2C do programa HAPIFAT.PRW
			If AllTrim( Upper( oJson:GetJsonObject( "cC5Origem" ) ) ) == "B2C"
				nSldPik := HFATSALDO( SB1->B1_COD, cAmzPik )
				nSldPul := HFATSALDO( SB1->B1_COD, cAmzPul )
				
				If oItems[nXi]:GetJsonObject( "cC6Qtdven" ) > ( nSldPik + nSldPul )
					If GETMV( "HP_SLDB2C", .T., .T. )
						aAdd( aNotSld, { SB1->B1_COD } )
					EndIf
				EndIf
				
				DbSelectArea("SBE")
				SBE->( DbOrderNickName("PRODUTO") )
				If SBE->( !DbSeek( xFilial("SBE") + SB1->B1_COD ) )
					aAdd( aNotEnd, { SB1->B1_COD } )
				EndIf
			EndIf
			
			cC6Descri := ""
			cC6Descri := AllTrim( SB1->B1_DESC )
			
			aC6Linha := {}
			cC6Item := Soma1( cC6Item )
			aAdd( aC6Linha, { "C6_FILIAL" 	, xFilial("SC6")                                    , Nil } )
			aAdd( aC6Linha, { "C6_ITEM" 	, cC6Item                                           , Nil } )
			aAdd( aC6Linha, { "C6_PRODUTO" 	, oItems[nXi]:GetJsonObject( "cC6Produto" )         , Nil } )
			aAdd( aC6Linha, { "C6_DESCRI" 	, cC6Descri                                         , Nil } )
			aAdd( aC6Linha, { "C6_QTDVEN" 	, oItems[nXi]:GetJsonObject( "cC6Qtdven" )          , Nil } )
			aAdd( aC6Linha, { "C6_PRCVEN" 	, oItems[nXi]:GetJsonObject( "cC6Prcven" )          , Nil } )
			aAdd( aC6Linha, { "C6_PRUNIT" 	, oItems[nXi]:GetJsonObject( "cC6Prunit" )          , Nil } )
			aAdd( aC6Linha, { "C6_VALOR" 	, oItems[nXi]:GetJsonObject( "cC6Valor" )           , Nil } )
			aAdd( aC6Linha, { "C6_OPER" 	, cC6Oper                                           , Nil } )
			aAdd( aC6Linha, { "C6_TES" 		, cC6Tes                                            , Nil } )
			aAdd( aC6Linha, { "C6_LOCAL" 	, SuperGetMv( "MV_XLOCAL", .F., "01", "" )          , Nil } )
			aAdd( aC6Linha, { "C6_GRADE" 	, SuperGetMv( "MV_XGRADE", .F., "N", "" )           , Nil } )
			aAdd( aC6Linha, { "C6_XPRESEN" 	, DecodeUTF8(oItems[nXi]:GetJsonObject( "cC6XPresen" )), Nil } )
			aAdd( aC6Linha, { "C6_XITPRES" 	, oItems[nXi]:GetJsonObject( "cC6Xitpres" )         , Nil } )
			aAdd( aC6Linha, { "C6_ENTREG" 	, SToD( oItems[nXi]:GetJsonObject( "cC6Entreg" ) )  , Nil } )
			aAdd( aC6Itens, aC6Linha )
		Next nXi
		
		If Len( aNotSld ) > 0
			cErro := ""
			cErro := "Produtos sem saldo suficiente: "
			For nXy := 1 To Len( aNotSld )
				cErro += Iif( nXy == 1, aNotSld[nXy][1], ", "+ aNotSld[nXy][1] )
			Next nXy
			SetRestFault( 400, cErro )
			lRet := .F.
			lProc := .F.
		EndIf
		
		If Len( aNotEnd ) > 0
			cErro := ""
			cErro := "Produtos sem amarra��o com endere�o de picking: 
			For nXy := 1 To Len( aNotEnd )
				cErro += Iif( nXy == 1, aNotEnd[nXy][1], ", "+ aNotEnd[nXy][1] )
			Next nXy
			SetRestFault( 400, cErro )
			lRet := .F.
			lProc := .F.
		EndIf
		
		If lProc
			MsExecAuto( {|x, y, z| MATA410(x, y, z) }, aC5Cabec, aC6Itens, 4 )
			If !lMsErroAuto
				//Define o retorno do m�todo
				oData := JsonObject():New()
				oData['IdStatus'] 		:= "ready-for-handling"
				oData['TpStatus'] 		:= "Pronto+para+o+manuseio"
				oData['IdPedVtex'] 		:= AllTrim( cC5XPedweb )
				oData['IdPedProtheus'] 	:= AllTrim( cC5Num )
				aAdd( aData, oData )
				FreeObj( oData )
				Self:SetResponse( FwJsonSerialize( aData ) )
				lRet := .T.
//				Incluir as condicoes na tabela auxiliar ZBE	
				If cC5Condpag == "MFP"
					fCriaZBE( SC5->C5_FILIAL, cC5Num, aCondPag )
				Endif
			Else
				cErro    := ""
				aLogAuto := {}
				aLogAuto := GetAutoGrLog()
				For nXy := 1 To Len( aLogAuto )
					cErro += aLogAuto[nXy] + Chr(13)+Chr(10)
				Next nXy
				SetRestFault( 400, cErro )
				lRet := .F.
			EndIf
		EndIf
		
		SF4->( DbCloseArea() )
		SB1->( DbCloseArea() )
		
	EndIf
	
END TRANSACTION
RestArea( aArea )
Return lRet


// Inclus�o de cliente
// Utilizado na inclus�o de pedido de venda
Static Function Hope001A( oJSon, cA1Cod, cA1Loja, cErro )
	
	Local cA1Est 			:= ""
	Local cA1Regiao 		:= ""
	Local cA1Codmun 		:= ""
	Local cA1Mun 			:= ""
	Local nXi 				:= 0
	Local nRecno 			:= 0
	Local aLogErro 			:= {}
	Local aSa1Auto 			:= {}
	Local nOpcAuto 			:= 0
	Local lProc 			:= .T.
	Private lMsHelpAuto 	:= .T.
	Private lMsErroAuto 	:= .F.
	Private lAutoErrNoFile 	:= .T.
	
	Default cA1Cod 			:= ""
	Default cA1Loja 		:= ""
	Default cErro 			:= ""
	
	nRecno := U_FindEnd( oJson:GetJsonObject( "A1Cgc" ), DecodeUTF8( oJson:GetJsonObject( "A1Endent" ), "cp1252"), oJson:GetJsonObject( "A1Cepe" ) )
	
	If nRecno > 0
		lProc := .F.
		DbSelectArea("SA1")
		SA1->( DbGoTo( nRecno ) )
		cA1Cod 		:= SA1->A1_COD
		cA1Loja 	:= SA1->A1_LOJA
		SA1->( DbCloseArea() )

	Else
		nOpcAuto 	:= 3
		//aAdd( aSa1Auto, { "A1_COD" 	    , GETSXENUM("SA1","A1_COD")  , Nil } )
		//aAdd( aSa1Auto, { "A1_LOJA" 	, '0001' 		             , Nil } )
	EndIf
	//Obtendo informa��es do cliente, via JSon
	//DecodeUTF8('Fabricação', "cp1252")

	aAdd( aSa1Auto, { "A1_TIPO" 	, oJson:GetJsonObject( "A1Tipo" ) 		, Nil } )
	aAdd( aSa1Auto, { "A1_PESSOA" 	, oJson:GetJsonObject( "A1Pessoa" ) 	, Nil } )
	aAdd( aSa1Auto, { "A1_NOME" 	, Alltrim(StrTran(NoAcento(Upper(DecodeUTF8(oJson:GetJsonObject( "A1Nome" ), "cp1252"))),"'",""))		, Nil } )
	aAdd( aSa1Auto, { "A1_NREDUZ" 	, Alltrim(StrTran(NoAcento(Upper(DecodeUTF8( oJson:GetJsonObject( "A1Nreduz" ), "cp1252"))),"'","")) 	, Nil } )
	aAdd( aSa1Auto, { "A1_CGC" 		, oJson:GetJsonObject( "A1Cgc" ) 		, Nil } )
	aAdd( aSa1Auto, { "A1_INSCR" 	, oJson:GetJsonObject( "A1Inscr" ) 		, Nil } )
	aAdd( aSa1Auto, { "A1_END" 		, StrTran(NoAcento(Upper(DecodeUTF8( oJson:GetJsonObject( "A1End" ), "cp1252"))),"'","") 		, Nil } )
	aAdd( aSa1Auto, { "A1_COMPLEM"  , StrTran(NoAcento(Upper(DecodeUTF8( oJson:GetJsonObject( "A1Complem" ), "cp1252"))),"'","")    , Nil } )
	aAdd( aSa1Auto, { "A1_BAIRRO" 	, StrTran(NoAcento(Upper(DecodeUTF8( oJson:GetJsonObject( "A1Bairro" ), "cp1252"))),"'","") 	, Nil } )
	aAdd( aSa1Auto, { "A1_EST" 		, oJson:GetJsonObject( "A1Est" ) 		, Nil } )
	aAdd( aSa1Auto, { "A1_CEP" 		, StrTran( oJson:GetJsonObject( "A1Cep" ), "-", "" ) 		, Nil } )
	
	cA1Est := AllTrim( oJson:GetJsonObject( "A1Est" ) )
	
	Do Case
		Case cA1Est $ "ES/MG/RJ/SP"
			cA1Regiao := "007" //007-SUDESTE
		Case cA1Est $ "PR/RS/SC"
			cA1Regiao := "002" //002-SUL
		Case cA1Est $ "AC/AM/AP/PA/RO/RR/TO"
			cA1Regiao := "001" //001-NORTE 
		Case cA1Est $ "AL/BA/CE/MA/PB/PE/PI/RN/SE"
			cA1Regiao := "008" //008-NORDESTE
		Case cA1Est $ "DF/GO/MS/MT" 
			cA1Regiao := "006" //006-CENTRO-OESTE
		Otherwise
			cA1Regiao := "" // N�o identificado 
	EndCase
	
    cA1Mun := DecodeUTF8(oJson:GetJsonObject("A1Mun"),"cp1252")
	cA1Mun := UPPER(cA1Mun)
	cA1Mun := NoAcento(cA1Mun)
	cA1Mun := AllTrim(cA1Mun)+Space(60-Len(AllTrim(cA1Mun)))

	fGetMun( StrTran( oJson:GetJsonObject( "A1Cep" ), "-", "" ), @cA1Mun, @cA1Codmun )

	If Empty( cA1Codmun )
		lProc := .F.
		cErro := "Falha ao consultar o CEP"
        aAdd(aLogErro,"Campo com codigo do municipio vazio...")
	Else
		DbSelectArea("CC2")
		CC2->( DbSetOrder(1) ) //CC2_FILIAL+CC2_EST+CC2_CODMUN
		
		If CC2->( DbSeek( xFilial("CC2") + cA1Est + cA1Codmun ) )
			cA1Codmun := AllTrim( CC2->CC2_CODMUN )
			cA1Mun    := AllTrim( CC2->CC2_MUN )
		Else
			
			If !Empty(AllTrim(cA1Mun))
				dbSelectArea("CC2")
				If RecLock("CC2",.T.)
					CC2->CC2_FILIAL := FwxFilial("CC2")
					CC2->CC2_EST    := cA1Est
					CC2->CC2_CODMUN := cA1Codmun
					CC2->CC2_MUN    := cA1Mun
					MsUnLock("CC2")
				EndIf
			EndIf

		EndIf
		CC2->( DbCloseArea() )
	EndIf

	If lProc

		aAdd( aSa1Auto, { "A1_REGIAO" 	, cA1Regiao 							, Nil } )
		aAdd( aSa1Auto, { "A1_COD_MUN" 	, cA1Codmun 							, Nil } )
		aAdd( aSa1Auto, { "A1_MUN" 		, cA1Mun 		                        , Nil } )
		aAdd( aSa1Auto, { "A1_NATUREZ" 	, oJson:GetJsonObject( "A1Naturez" ) 	, Nil } )
		aAdd( aSa1Auto, { "A1_PAIS" 	, SuperGetMv( "MV_XPAIS", .F., "105", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_CONTA" 	, SuperGetMv( "MV_XPCONTA", .F., "101020201501", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_EMAIL" 	, oJson:GetJsonObject( "A1Email" ) 		, Nil } )
		aAdd( aSa1Auto, { "A1_ENDENT" 	, Alltrim(StrTran(NoAcento(Upper(DecodeUTF8( oJson:GetJsonObject( "A1Endent" ), "cp1252"))),"'","")) 	, Nil } ) 
		//aAdd( aSa1Auto, { "A1_ENDENT" , StrTran(AllTrim(NoAcento(DecodeUTF8( oJson:GetJsonObject( "A1Endent" ), "cp1252"))),"'","") 	, Nil } )
		aAdd( aSa1Auto, { "A1_CEPE" 	, oJson:GetJsonObject( "A1Cepe" ) 		, Nil } )
		aAdd( aSa1Auto, { "A1_BAIRROE" 	, StrTran(AllTrim(NoAcento(DecodeUTF8( oJson:GetJsonObject( "A1Bairroe" ), "cp1252"))),"'","") 	, Nil } )
		aAdd( aSa1Auto, { "A1_CODPAIS" 	, SuperGetMv( "MV_XCDPAIS", .F., "01058", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_MUNE" 	, oJson:GetJsonObject( "A1Mune" ) 		, Nil } )
		aAdd( aSa1Auto, { "A1_ESTE" 	, oJson:GetJsonObject( "A1Este" ) 		, Nil } )
		aAdd( aSa1Auto, { "A1_XCANAL" 	, SuperGetMv( "MV_XCANAL", .F., "007", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XCANALD" 	, SuperGetMv( "MV_XCANALD", .F., "E COMMERCE HOPE", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XEMP" 	, SuperGetMv( "MV_XEMP", .F., "01", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XDESEMP" 	, SuperGetMv( "MV_XDESEMP", .F., "CONSUMIDOR FINAL", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XGRUPO" 	, SuperGetMv( "MV_XGRUPO", .F., "00022", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XGRUPON" 	, SuperGetMv( "MV_XGRUPON", .F., "FAST COMMERCE", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XCODDIV" 	, SuperGetMv( "MV_XCODDIV", .F., "000001", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XNONDIV" 	, SuperGetMv( "MV_XNONDIV", .F., "CLIENTE LOJA VIRTUAL", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XCODREG" 	, SuperGetMv( "MV_XCODREG", .F., "11040010", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XDESREG" 	, SuperGetMv( "MV_XDESREG", .F., "LOJA VIRTUAL - INTERNET", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XMICRRE" 	, SuperGetMv( "MV_XMICRRE", .F., "11040", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XMICRDE" 	, SuperGetMv( "MV_XMICRDE", .F., "LOJA VIRTUAL", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_XBLQVEN" 	, oJson:GetJsonObject( "A1XBlqven" ) 	, Nil } )
		aAdd( aSa1Auto, { "A1_XBLQPED" 	, oJson:GetJsonObject( "A1XBlqped" ) 	, Nil } )
		aAdd( aSa1Auto, { "A1_CONTRIB" 	, SuperGetMv( "MV_XCONTRI", .F., "2", "" ) , Nil } )
		aAdd( aSa1Auto, { "A1_TEL" 		, oJson:GetJsonObject( "A1Tel" ) 		, Nil } )
		aAdd( aSa1Auto, { "A1_TELC" 	, oJson:GetJsonObject( "A1Telc" ) 		, Nil } )
		aAdd( aSa1Auto, { "A1_TELE" 	, oJson:GetJsonObject( "A1Tele" ) 		, Nil } )
		aAdd( aSa1Auto, { "A1_DDD" 		, oJson:GetJsonObject( "A1Ddd" ) 		, Nil } )
		aAdd( aSa1Auto, { "A1_DDDC" 	, oJson:GetJsonObject( "A1Dddc" ) 		, Nil } )
		aAdd( aSa1Auto, { "A1_DDDE" 	, oJson:GetJsonObject( "A1Ddde" ) 		, Nil } )
		// ExecAuto Inclus�o de cliente
		MSExecAuto( { |x,y| Mata030( x,y) }, aSa1Auto, nOpcAuto ) //3- Inclus�o, 4- Altera��o, 5- Exclus�o
		If !lMsErroAuto
			cA1Cod 		:= SA1->A1_COD
			cA1Loja 	:= SA1->A1_LOJA
		Else
			cErro    := ""
			aLogErro := {}
			aLogErro := GetAutoGrLog()
			For nXi := 1 To Len( aLogErro )
				cErro += aLogErro[nXi] + Chr(13)+Chr(10)
			Next nY

			cErro := NoAcento(DecodeUTF8( cErro, "cp1252"))

		EndIf
	EndIf
Return


// Analisa se � para presente
Static Function IsGift( cC5Filial, cC5Num, lIsGift, cMsgGift )
	
	
	DbSelectArea("SC6")
	SC6->( DbSetOrder(1) ) //C6_FILIAL, C6_NUM, C6_ITEM, C6_PRODUTO
	If SC6->( DbSeek( cC5Filial + cC5Num ) )
		
		While SC6->( !EoF() ) .And. SC6->C6_FILIAL = cC5Filial .And. SC6->C6_NUM = cC5Num
			
			If SC6->C6_XITPRES == "S"
				lIsGift  := .T.
				cMsgGift := AllTrim( SC6->C6_XPRESEN )
				Exit
			EndIf
			
		SC6->( DbSkip() )
		EndDo
		
	EndIf
	SC6->( DbCloseArea() )
	
Return


// Fun��o de verifica��o de saldo do cliente. Programa "HAPIFAT.PRW"
Static Function HFATSALDO( cProduto, cArmazem )
	
	Local nRet := 0
	
	
	DbSelectArea("SB2")
	SB2->( DbSetOrder(1) ) //B2_FILIAL, B2_COD, B2_LOCAL
	If SB2->( DbSeek( xFilial("SB2") + cProduto + cArmazem ) )
		nRet := SB2->B2_QATU - SB2->B2_RESERVA - SB2->B2_QEMP - SB2->B2_XRESERV
	EndIf
	
Return nRet

// Fun��o para buscar o CEP
Static Function fGetMun( cA1Cep, cA1Mun, cA1Codmun)
Local cUrl          := "https://viacep.com.br/ws/"
Local cError 		:= ""
Local cWarning 		:= ""
Local cXML 			:= ""
Local oXML 			:= Nil
Local aHeader       := {}
Local cHeaderRet    := ""
Local _cRetorno     := ""
Local _cStatus      := ""
Local cErr          := ""
Local cIBGE         := ""
Local cMetodo       := "GET"
Local cGetParms     := ""
Local cDesc         := ""
Local oJson 

Default cA1Cep 		:= ""
Default cA1Mun 		:= ""
Default cA1Codmun 	:= ""

    cA1Cep := StrTran(cA1Cep,"-","")

       aadd(aHeader,'accept: application/json')
       aadd(aHeader,'content-type: application/json')

       _cRetorno := HTTPQuote( cURL+cA1Cep+"/json/", cMetodo, cGETParms,"", 120, aHeader, @cHeaderRet )
       _cStatus  := CValToChar(HttpGetStatus(@cDesc))
       
       oJson     := JSonObject():New()
       cErr      := oJSon:fromJson(_cRetorno)

       cA1Mun    := UPPER(NoAcento(oJson:GetJSonObject('localidade')))
	   If .not. EMPTY( cA1Mun )
	       cIBGE     := oJson:GetJSonObject('ibge')
	       cA1Codmun := Substr(cIBGE,3,5)
	   Endif
  	
Return


// Busca condi��o de pagamento e Taxa

Static Function CondCartao( cC5XCartao, cC5XBande, nC5XParcel, cC5Condpag, nTaxa )
Local cQuery 			:= ""
Local cAlias 			:= GetNextAlias()

Default cC5XCartao 		:= ""
Default cC5XBande 		:= ""
Default nC5XParcel 		:= 1
Default cC5Condpag 		:= ""
	
	cQuery := " SELECT E4_CODIGO, E4_XTXCC "
	cQuery += " FROM "+ RetSqlName("SE4") +" (NOLOCK) "
	cQuery += " WHERE E4_FILIAL  =  '"+ xFilial("SE4") +"' "
	cQuery += "   AND E4_XECOM   =  '1' "
	cQuery += "   AND UPPER( E4_DESCRI )  LIKE '%"+ Upper( cC5XBande  ) +"%' "
	If Upper( AllTrim( cC5XCartao ) ) == "CREDITO" 
		cQuery += "   AND UPPER( E4_DESCRI )  LIKE '%"+ Alltrim(Str( nC5XParcel,2 )) +"%' "
	EndIf
	cQuery += "   AND D_E_L_E_T_ =  ' ' "
	DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)
	If (cAlias)->( !Eof())
		cC5Condpag	:= (cAlias)->E4_CODIGO
		nTaxa		:= (cAlias)->E4_XTXCC
	EndIf
	(cAlias)->( DbCloseArea() )
	
Return

Static Function CalcParc( aZBECond ) 
Local m
Local n
Local nItem		:= 1	
Local aRet		:= {}
Local cFracao	:= "0"
Local cPicVar	:= ""

For n := 1 to Len( aZBECond )
	dVencto := CtoD(SubStr(aZBECond[1][13],7,2)+"/"+SubStr(aZBECond[1][13],5,2)+"/"+SubStr(aZBECond[1][13],1,4))
	For m := 1 to aZBECond[n][10]
		If aZBECond[n][2] == "1"
			dVencto := MonthSum(dVencto,1)
		Endif
		If m == 1
			nParcela := NoRound(aZBECond[n][9] / aZBECond[n][10],3)
			cPicVar	 := Transform(nParcela, "@E 999,999,999.999")
			cFracao	 := Substr( cPicVar,Len(cPicVar),1)
			If cFracao == "0"
				nParcela := NoRound(aZBECond[n][9] / aZBECond[n][10],2)
			Else
				nParcela := NoRound(aZBECond[n][9] / aZBECond[n][10],2) + 0.01
			Endif
			nParcela := (aZBECond[n][9] - nParcela * aZBECond[n][10]) + nParcela
		Else
			If cFracao == "0"
				nParcela := NoRound(aZBECond[n][9] / aZBECond[n][10],2)
			Else
				nParcela := NoRound(aZBECond[n][9] / aZBECond[n][10],2) + 0.01
			Endif
		Endif

		aAdd(aRet,{nParcela,dVencto})
		nItem++	
	Next m
Next N

Return aRet

Static Function fCriaZBE(cC5Fil, cNumSC5, aZBECond ) 
//Local nItem		:= 1	
Local cParcela 	:= Chr(Asc(GetMV("MV_1DUP"))-1)
Local nTamParc 	:= TAMSX3("E1_PARCELA")[1]
Local lAlfa     := ( GetMV("MV_1DUP") == "A" )
Local nParcela	:= 0
Local nPrazo	:= 0
Local lUnica	:= ( Len( aZBECond ) == 1 )	// Parcela Unica nao grava a informaca da parcela na SE1
Local m
Local n

cParcela := AllTrim( GetMV("MV_1DUP") )
// E uma unica forma de pagamento, agora sera testado se esta forma esta parcelada
If lUnica
	lUnica	 := aZBECond[1][10] == 1	// Array com o numero de parcelas 
Endif

DbSelectArea("SE4")
DbSetOrder(1)
DbSelectArea("ZBE")
DbSetOrder(1)
nNrParc	:= 0	
For n := 1 to Len( aZBECond )
	nPrazo	:= 0
	dVencto := CtoD(SubStr(aZBECond[1][13],7,2)+"/"+SubStr(aZBECond[1][13],5,2)+"/"+SubStr(aZBECond[1][13],1,4))
	For m	:= 1 to aZBECond[n][10]
		nNrParc++
		If aZBECond[n][2] == "1"		// Cartao de credito
			nPrazo	+= aZBECond[n][3]
			dVencto := MonthSum(dVencto,1)
		Endif
		If m == 1
			nParcela := NoRound(aZBECond[n][9] / aZBECond[n][10],2)
			nParcela := (aZBECond[n][9] - nParcela * aZBECond[n][10]) + nParcela
		Else
			nParcela := NoRound(aZBECond[n][9] / aZBECond[n][10],2)
		Endif
		DbSelectArea("ZBE")
		RECLOCK("ZBE",.T.)
		ZBE->ZBE_FILIAL := cC5Fil					// Filial
		ZBE->ZBE_NUMSC5	:= cNumSC5					// Numero do Pedido de Venda
		If ! lUnica
			ZBE->ZBE_PARCEL := cParcela				// Numero da Parcela ( Quando nao for parcela unica )
		Endif
		ZBE->ZBE_VALOR  := nParcela					// Valor da Parcela
		ZBE->ZBE_CONDPA := aZBECond[n][01]			// Condicao de pagamento
		ZBE->ZBE_TIPO   := aZBECond[n][02] 			// Tipo da forma de pagamento ( Semelhante ao campo FORPAG ) 
		ZBE->ZBE_PRAZO  := nPrazo		 			// Prazo de pagamento 
		ZBE->ZBE_VENCTO := dVencto					// Data de Vencimento
		ZBE->ZBE_TXCC	:= aZBECond[n][04]			// Taxa do parcelamento do Cartao
		ZBE->ZBE_FORPAG := aZBECond[n][05]			// Forma de Pagamento
		ZBE->ZBE_CARTAO := aZBECond[n][06]			// Bandeira do Cartao ( Credito ou Debito )
		ZBE->ZBE_NSU    := aZBECond[n][07]			// Numero NSU ( Numero de Seguran�a da transacao do Cartao de credito )						   
		ZBE->ZBE_CARDID := aZBECond[n][08]			// Identificacao do Cartao
		ZBE->ZBE_TID    := aZBECond[n][11]			// TID ( Transacao Interna de Documentos - CIELO )
		ZBE->ZBE_AUTCAR := aZBECond[n][12]			// Numero de autoriza��o do Cartao
		ZBE->ZBE_DTAUTO := StoD( aZBECond[n][13] )	// Data da autorizacao do Cartao
		ZBE->ZBE_HRAUTO := aZBECond[n][14]			// Hora de autorizacao do Cartao
		ZBE->ZBE_VLPIX	:= aZBECond[n][15]			// Valor Administrativo fixo do PIX ( R$ 0.20)
		ZBE->ZBE_SEQ	:= nNrParc					// Sequencia da forma de pagamento
		ZBE->(MsUnLock())

        If lAlfa 
            cParcela:= Chr(Asc(cParcela) + 1 )
        Else
            cParcela:= Soma1( cParcela,nTamParc )
        Endif

	Next m
Next N

Return

Static Function fConvHexa(nNumero)
Local cAlfabeto := "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0"
Local cRet
cRet := Substr(cAlfabeto,nNumero,1)

Return cRet

Static Function VerCupom( _cCupom, _cTipo, _nValor, aCupom, cErro )
Local lRet	 := .F.
Local nSaldo := 0
Local cCupom := PADR(_cCupom, TamSX3("ZBF_IDVTEX")[1] )

DbSelectArea("ZBF")
DbSetOrder(4)
If DbSeek( cCupom )
	If ZBF->ZBF_OPCAO $ "34"				// OPCAO = 3-CUPOM ou 4-MISTO
		If 	ZBF->ZBF_TIPO <> _cTipo
			cErro	:= EncodeUTF8('Tipo de CUPOM [ ' + cCupom + ' ] invalido !')
		Else
			nSaldo := ZBF_VLREAL - ( ZBF_VLPED + ZBF_VLFAT )
			If nSaldo < _nValor
				cErro	:= EncodeUTF8('Saldo do CUPOM [ ' + cCupom + ' ] insuficiente !') 
			Else
				aAdd( aCupom, { cCupom, _nValor } )
				lRet := .T.
			EndIf
		Endif
	Else
		cErro := EncodeUTF8('Erro no CUPOM [ ' + cCupom + ' ] !')
	Endif
Else
	cErro	:= EncodeUTF8('CUPOM [ ' + cCupom + ' ] n�o encontrado!')
Endif
Return lRet

Static Function fCriaCup( aCupom )
Local nCupom	:= 0

DbSelectArea("ZBF")
DbSetOrder(4)
For nCupom := 1 to Len( aCupom )
	If DbSeek( aCupom[nCupom,1] )
		Reclock("ZBF",.F.)
		ZBF->ZBF_VLPED := ZBF->ZBF_VLPED + aCupom[nCupom,2] 
		ZBF->( MsUnLock() )
	Endif
Next
Return
