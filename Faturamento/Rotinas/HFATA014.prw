#Include 'Protheus.ch'
#include "topconn.ch"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} HFATA012
Libera��o de pedidos em JOB
@author Geyson Albano
@since 15/02/2021
@version P12
@obs ZZ0_STATUS
P- Pendente Libera��o
L-Liberado
X-Pendente Estorno
E- Estornado 
Z - Erro
/*/

User Function HFATA014()

	Local cQuery		:= ""
	Local lErro			:= .F.
	Local oError
	Local cMsgErro

	Private	cNumPF 		:= ""
	Private cSASStaErr	:= "Z"

	Sleep(2000)
	If !LockByName("HFATA014-01",.F.,.F.)
		ConOut("Processso ja inicializado!")
		Return
	EndIf
	RpcSetEnv("01","0101",,,"FAT","U_HFATA014",{})
	cQuery	:= " SELECT TOP 1 R_E_C_N_O_ AS REGZZ0 FROM "+RETSQLNAME("ZZ0")+" ZZ0 WHERE ZZ0_STATUS IN ('P','X') AND ZZ0_FILIAL = '0101' AND D_E_L_E_T_ = '' ORDER BY ZZ0_PRIORI,R_E_C_N_O_ "
	// Seta que RecLock, em caso de falha, nao vai fazer retry automatico
	ConOut("Executou SELECT!")
	SetLoopLock(.F.)
	While !killapp()
		//Monitor("Faturamento|Aguardando tarefas...")
		TCQuery cQuery new alias TMPZZ0
		If TMPZZ0->(EOF())
			TMPZZ0->(DbCloseArea())
			ConOut("N�o existem pedidos na fila! ")
			return
			Sleep(10000)
			Loop
		EndIF
		lErro		:= .F.
		cMsgErro	:= ""
		ZZ0->(DbGoTo(TMPZZ0->REGZZ0))
		RecLock("ZZ0",.F.)
		oError := ErrorBlock({|e| lErro := .T.,ErroLog(e,cSASStaErr) } )

		If ALLTRIM(ZZ0->ZZ0_STATUS)=="P"	//Pendente Faturamento
			ConOut("Entrou na funcao SALVAAPTO!")
			//Faturar(@lErro,@cMsgErro)
			cNumPF := Alltrim(ZZ0->ZZ0_NUMPF)
			SALVAAPTO(cNumPF)
			ConOut("Saiu da funcao SALVAAPTO!")
		ElseIf ALLTRIM(ZZ0->ZZ0_STATUS)=="X"
			ConOut("Entrou na funcao SALVAAPTO!")
			//Faturar(@lErro,@cMsgErro)
			cNumPF := Alltrim(ZZ0->ZZ0_NUMPF)
			H003EST(cNumPF)
			ConOut("Saiu da funcao SALVAAPTO!")
		EndIf
		Logs(Replicate("=",30))
		ZZ0->(MsUnLock())
		ErrorBlock(oError)
		TMPZZ0->(DbCloseArea())
		RETURN
	EndDo

Return

Static Function SALVAAPTO(cNumPF)
	Local cQuery   := ""
	Local lRet     := .T.
	Local nSldDist := 0
	Local cPedido  := ""
	Local lValid := .T.

	IF SELECT("TMPAPTO") > 0
		TMPAPTO->(DbCloseArea())
	ENDIF

	cQuery := "SELECT ZP_NUMPF,ZP_PEDIDO,ZP_PRODUTO,SUM(ZP_QUANT) AS ZP_QUANT"
	cQuery += CRLF + "FROM "+RetSqlName("SZP")+" SZP WITH(NOLOCK)  "
	cQuery += CRLF + "WHERE SZP.D_E_L_E_T_ = '' "
	cQuery += CRLF + "AND ZP_FILIAL = '"+xFilial("SZP")+"' "
	cQuery += CRLF + "AND ZP_NUMPF  = '"+cNumPF+"' "
	cQuery += CRLF + "GROUP BY ZP_NUMPF,ZP_PEDIDO,ZP_PRODUTO "
	cQuery += CRLF + "ORDER BY ZP_NUMPF,ZP_PRODUTO "

	MemoWrite("HPCPA003_SALVAAPTO.txt",cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPAPTO", .F., .T.)

	DbSelectArea("TMPAPTO")

	If TMPAPTO->(!EOF())
		//TODO WESKLEY SILVA 01/08/2018 [Begin transaction para garantir a efetiva��o do pedido]
		Begin Transaction

			cPedido := TMPAPTO->ZP_PEDIDO
			While TMPAPTO->(!EOF())
				DbSelectArea("SZJ")
				DbSetOrder(3)
				If DbSeek(xFilial("SZJ")+TMPAPTO->ZP_NUMPF+TMPAPTO->ZP_PRODUTO)
					While SZJ->(!EOF()) .AND. SZJ->ZJ_PRODUTO = TMPAPTO->ZP_PRODUTO
						RecLock("SZJ",.F.)
						SZJ->ZJ_CONF := "S"
						MsUnlock()
						nSldDist := 0
						SZJ->(DbSkip())
					EndDo
				EndIf
				TMPAPTO->(DbSkip())
			EndDo
			LIBERA(cNumPF,cPedido,lValid)

		End Transaction

	Else
		RecLock("SZJ",.F.)
		  ZZ0->ZZ0_STATUS := "Z"
		MsUnlock()
	EndIf

	TMPAPTO->(DbCloseArea())

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function LIBERA(cNumPF,cPedido,lValid)
	Local cQuery  := ""
	Local nQtdLib := 0
	Local nQtdRes := 0
	Local lValid := .T.
	Local nTotPed := 0
	Local nTotLib := 0
	Local nLib    := 0

	IF SELECT("TMPLIB") > 0
		TMPLIB->(DbCloseArea())
	ENDIF

	cQuery := "SELECT ZJ_NUMPF,ZJ_PEDIDO,ZJ_ITEM,ZJ_ITEMGRD,ZJ_PRODUTO,B1_CODBAR,SUM(ZJ_QTDSEP) AS ZJ_QTDSEP,ZJ_LOCAL "
	cQuery += CRLF + "FROM "+RetSqlName("SZJ")+" SZJ WITH (NOLOCK) "
	cQuery += CRLF + "INNER JOIN "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) "
	cQuery += CRLF + "ON B1_COD = ZJ_PRODUTO "
	cQuery += CRLF + "AND SB1.D_E_L_E_T_ = '' "
	cQuery += CRLF + "where SZJ.D_E_L_E_T_ = '' "
	cQuery += CRLF + "AND ZJ_FILIAL = '"+xFilial("SZJ")+"' "
	cQuery += CRLF + "AND ZJ_NUMPF  = '"+cNumPF+"' "
	cQuery += CRLF + "GROUP by ZJ_NUMPF,ZJ_PEDIDO,ZJ_ITEM,ZJ_ITEMGRD,ZJ_PRODUTO,B1_CODBAR,ZJ_LOCAL "
	cQuery += CRLF + "ORDER by ZJ_PEDIDO,ZJ_ITEM,ZJ_ITEMGRD,ZJ_PRODUTO,ZJ_LOCAL "

	MemoWrite("HESTA003_LIBERA.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPLIB", .F., .T.)

	DbSelectArea("SC6")
	DbSetOrder(1)
	If DbSeek(xFilial("SC6")+cPedido)
		While SC6->(!EOF()) .AND. SC6->C6_NUM = cPedido
			nTotPed += SC6->C6_QTDVEN
			SC6->(DbSkip())
		EndDo
	EndIf

	DbSelectArea("TMPLIB")

	lProd := .T.

	_aprode := {}

	If TMPLIB->(!EOF())
		Begin Transaction
			While TMPLIB->(!EOF())
				DbSelectArea("SC6")
				DbSetOrder(1)
				If DBSeek(xFilial("SC6")+TMPLIB->ZJ_PEDIDO+TMPLIB->ZJ_ITEM+TMPLIB->ZJ_PRODUTO)
					If POSICIONE("SB1", 1, xFilial("SB1") + TMPLIB->ZJ_PRODUTO, "B1_LOCALIZ") == "S"
						aEmpenho := GERAARRAY(TMPLIB->ZJ_NUMPF,TMPLIB->B1_CODBAR,TMPLIB->ZJ_QTDSEP,TMPLIB->ZJ_PRODUTO)
						If Len(aEmpenho) > 0

							nQtdLib := MaLibDoFat(SC6->(RecNo()),TMPLIB->ZJ_QTDSEP,.F.,.F.,.T.,.T.,.F.,.T.) // CRIA SC9

							DbSelectArea("SB2")
							DbSetOrder(2)
							If DbSeek(xFilial("SB2")+SC6->C6_LOCAL+SC6->C6_PRODUTO)
								nQtdRes := SB2->B2_XRESERV

								DbSelectArea("SZJ")
								DbSetOrder(3)
								DbSeek(xFilial("SZJ")+TMPLIB->ZJ_NUMPF+TMPLIB->ZJ_PRODUTO+TMPLIB->ZJ_ITEM)

								If EMPTY(SC9->C9_BLEST)
									RecLock("SB2",.F.)
									SB2->B2_XRESERV := IF((nQtdRes - nQtdLib) < 0,0,(nQtdRes - nQtdLib))
									MsUnlock()
								ElseIf SC9->C9_BLEST != "10"
									RecLock("SZJ",.F.)
									SZJ->ZJ_BLEST := "02"
									MsUnlock()
								EndIf
							EndIf
						EndIf

					ElseIf POSICIONE("SB1", 1, xFilial("SB1") + TMPLIB->ZJ_PRODUTO, "B1_LOCALIZ") == "N"

						nQtdLib := MaLibDoFat(SC6->(RecNo()),TMPLIB->ZJ_QTDSEP,.F.,.F.,.T.,.T.,.F.,.T.) // CRIA C9

						DbSelectArea("SB2")
						DbSetOrder(2)
						If DbSeek(xFilial("SB2")+SC6->C6_LOCAL+SC6->C6_PRODUTO)
							nQtdRes := SB2->B2_XRESERV

							DbSelectArea("SZJ")
							DbSetOrder(3)
							DbSeek(xFilial("SZJ")+TMPLIB->ZJ_NUMPF+TMPLIB->ZJ_PRODUTO+TMPLIB->ZJ_ITEM)

							If EMPTY(SC9->C9_BLEST)
								RecLock("SB2",.F.)
								SB2->B2_XRESERV := IF((nQtdRes - nQtdLib) < 0,0,(nQtdRes - nQtdLib))
								MsUnlock()
							ElseIf SC9->C9_BLEST != "10"
								RecLock("SZJ",.F.)
								SZJ->ZJ_BLEST := "02"
								MsUnlock()
							EndIf
						EndIf

						DbSelectArea("SZP")
						DbSetOrder(1)
						If DbSeek(xFilial("SZP")+cNumPF+TMPLIB->B1_CODBAR)
							While SZP->(!EOF()) .AND. SZP->ZP_NUMPF+SZP->ZP_CODBAR = cNumPF+TMPLIB->B1_CODBAR
								nLib := SZP->ZP_QUANT
								RecLock("SZP",.F.)
								SZP->ZP_QTDLIB := SZP->ZP_QTDLIB + nLib
								SZP->ZP_CONFERI := "F"
								MsUnlock()
								SZP->(DbSkip())
							EndDo

						EndIf
						nTotLib += nQtdLib
					Else
						//MessageBox("Falha na libera��o de estoque do produto: "+TMPLIB->ZJ_PRODUTO,"Aten��o",48)
						Conout("Falha na libera��o de estoque do produto: "+TMPLIB->ZJ_PRODUTO)
						Aadd(_aprode,TMPLIB->ZJ_PRODUTO)
						lProd := .F.
						lValid := .F.
					EndIf

				Else
					//MessageBox("Falha na libera��o de estoque do produto: "+TMPLIB->ZJ_PRODUTO,"Aten��o",48)
					Conout("Falha na libera��o de estoque do produto: "+TMPLIB->ZJ_PRODUTO)
					Aadd(_aprode,TMPLIB->ZJ_PRODUTO)
					lProd := .F.
					lValid := .F.
				EndIf

				TMPLIB->(DbSkip())
			EndDo
			If nTotLib > 0
				cBloq := ""
				DbSelectArea("SC5")
				DbSetOrder(1)
				If DbSeek(xFilial("SC5")+cPedido)
					cBloq := SC5->C5_XBLQ
					If nTotPed = nTotLib
						If lProd
							RecLock("SC5",.F.)
							SC5->C5_LIBEROK := "S"
							MsUnlock()
						Endif
					EndIf
				EndIf
				DbSelectArea("SC9")
				DbSetOrder(11)
				If DbSeek(xFilial("SC9")+cPedido+cNumPF)
					While SC9->(!EOF()) .AND. SC9->C9_PEDIDO = cPedido
						If Empty(SC9->C9_NFISCAL) .AND. Empty(SC9->C9_XNUMPF)
							RecLock("SC9",.F.)
							SC9->C9_XBLQ := cBloq
							SC9->C9_XNUMPF := cNumPF
							MsUnlock()
						EndIf
						SC9->(DbSkip())
					EndDo
				EndIf
			EndIf
		End Transaction
	EndIf

	TMPLIB->(DbCloseArea())

	IF lValid
		RecLock("ZZ0",.F.)
		ZZ0_STATUS = "L"
		MsUnLock()
	Endif

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �          �Autor  �Bruno Parreira      � Data �  21/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function GERAARRAY(cNumPF,cCodBar,nQtdALib,cProd)
	Local aRet     := {}
	Local nQtdLot  := 0

	DbSelectArea("SZP")
	DbSetOrder(1)
	If DbSeek(xFilial("SZP")+cNumPF+cCodBar)
		While SZP->(!EOF()) .AND. SZP->ZP_NUMPF+SZP->ZP_CODBAR = cNumPF+cCodBar
			nQtdLot := SZP->ZP_QUANT

			DbSelectArea("SB8")
			DbSetOrder(3)
			If DbSeek(xFilial("SB8")+SZP->ZP_PRODUTO+TMPLIB->ZJ_LOCAL+SZP->ZP_LOTECTL)
				While SB8->(!EOF()) .AND. SB8->B8_PRODUTO+SB8->B8_LOCAL+SB8->B8_LOTECTL = SZP->ZP_PRODUTO+TMPLIB->ZJ_LOCAL+SZP->ZP_LOTECTL

					nLib := nQtdLot

					aAdd(aRet,{SB8->B8_LOTECTL,SB8->B8_NUMLOTE,"","",nLib,0,SB8->B8_DTVALID,"","","",SB8->B8_LOCAL,0})

					nQtdLot := nQtdLot - nLib

					RecLock("SZP",.F.)
					SZP->ZP_QTDLIB := SZP->ZP_QTDLIB + nLib
					SZP->ZP_CONFERI := "F"
					MsUnlock()

					SB8->(DbSkip())

				EndDo
			EndIf

			SZP->(DbSkip())
		EndDo
	EndIf

Return aRet

Static Function H003EST(cNumPF)
	Local aArea    := GetArea()
	Local lZera    := .F.
	Local cPedido  := SubStr(cNumPF,1,6)
	Local nQtdXRes := 0
	Local lValid := .T.

	// AJUSTE FUN��O ESTORNO CHAMADO 5548 - GEYSON ALBANO 09-08-2019
	DbSelectArea("SC9")
	DbSetOrder(11)
	If DbSeek(xFilial("SC9")+cPedido+cNumPF)
		While SC9->(!EOF()) .And. SC9->C9_PEDIDO = cPedido .and. SC9->C9_XNUMPF = cNumPF
			SC9->(a460Estorna())
			lZera := .T.
			SC9->(DbSkip())
		EndDo
	EndIf

	If lZera
		DbSelectArea("SZP")
		DbSetOrder(1)
		If DbSeek(xFilial("SZP")+cNumPF)
			While SZP->(!EOF()) .and. SZP->ZP_NUMPF = cNumPF

				RecLock("SZP",.F.)
				Replace SZP->ZP_QTDLIB	with 0
				If Alltrim(SZP->ZP_PRODUTO) != "CXA.EXTRA"
					Replace SZP->ZP_CONFERI with ""
				EndIf
				MsUnlock()
				SZP->(DbSkip())

			EndDo
		EndIf
		DbSelectArea("SZJ")
		DbSetOrder(1)
		If DbSeek(xFilial("SZJ")+cNumPF)
			While SZJ->(!EOF()) .and. SZJ->ZJ_NUMPF = cNumPF

				RecLock("SZJ",.F.)
				SZJ->ZJ_CONF   := "L"
				MsUnlock()

				DbSelectArea("SB2")
				DbSetOrder(2)
				If DbSeek(xFilial("SB2")+SZJ->ZJ_LOCAL+SZJ->ZJ_PRODUTO)

					If !EMPTY(SZJ->ZJ_BLEST)
						nQtdXRes := SB2->B2_XRESERV
					Else
						nQtdXRes := SB2->B2_XRESERV + SZJ->ZJ_QTDLIB
					EndIf

					If !EMPTY(SZJ->ZJ_BLEST)
						RecLock("SZJ",.F.)
						SZJ->ZJ_BLEST := ""
						MsUnlock()
					EndIf

					RecLock("SB2",.F.)
					SB2->B2_XRESERV := nQtdXRes
					MsUnlock()

				EndIf
				SZJ->(DbSkip())
			EndDo
		EndIf
		Conout("Libera��o estornada com sucesso!")

		IF lValid
			RecLock("ZZ0",.F.)
			ZZ0_STATUS := "E"
			ZZ0_LOG := ""
			MsUnLock()
		Endif

	EndIf

	RestArea(aArea)

Return

Static Function ErroLog(oErro,cStatus)
	DisarmTransaction()
	Logs(oErro:Description+CRLF+oErro:ErrorStack+CRLF+oErro:ErrorEnv,.T.)
	RecLock("ZZ0",.F.)
	ZZ0_STATUS=cStatus	//Altera o status para erro
	MsUnLock()
	StartJob("U_HFATA014",getenvserver(),.F.)	//Inicia um nova thread
	__Quit()									//Finaliza a thread atual
Return

/*/{Protheus.doc} Logs
Grava log em arquivo texto no servidor
@author Geyson Albano
@since 19/02/2021
@version 1.0
@param cTexto, character, Texto do log
/*/

Static Function Logs(cTexto,lErro)
	Local cCodLog	:= SubStr(DTOS(Date()),1,6)
	Local cPasta	:= "\log\HFATA014"
	Local cTexLog
	Default lErro	:= .F.
	FWMakeDir(cPasta)
	If File(cPasta+"\"+cCodLog+".txt")
		nHdl := fopen(cPasta+"\"+cCodLog+".txt" , 2 + 64 )
	Else
		nHdl := FCREATE(cPasta+"\"+cCodLog+".txt", FC_NORMAL)
	EndIf
	FSeek(nHdl, 0, 2)
	FWrite(nHdl, DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+CRLF )
	fclose(nHdl)
	If lErro
		RecLock("ZZ0",.F.)
		cTexLog			:= Alltrim(ZZ0->ZZ0_LOG)
		ZZ0->ZZ0_LOG	:= DTOC(Date(),"dd/mm/yy")+"|"+Time()+"| "+cTexto+chr(10)+cTexLog
		MsUnLock()
	EndIf
Return
