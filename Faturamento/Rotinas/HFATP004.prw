// #########################################################################################
// Projeto: Hope
// Modulo : Faturamento
// Fonte  : hFATP004
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 09/10/16 | Actual Trend      | Cadastro de Prazos de Entrega 
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

user function HFATP004
	local cVldAlt := ".T." 
	local cVldExc := ".T." 
	
	local cAlias
	
	cAlias := "ZA3"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	axCadastro(cAlias, "Cadastro de Canais", cVldExc, cVldAlt)
	
return
