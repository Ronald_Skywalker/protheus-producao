#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"  
#INCLUDE "RWMAKE.CH"
#include "TbiConn.ch"
#include "TbiCode.ch"  

/*///-------------------------------------------------------------------------------------------------------------------
{Protheus.doc} HJOBGREEN
@author    TOTVS | HOPE - Fonte utilizado para trocar prioridade de produtos de acordo com a ZB5 e Saldo em estoque
@version   1.xx
@since     25/03/2020
/*///-------------------------------------------------------------------------------------------------------------------
User Function HJOBGREEN()

Local cQuery    := ""
Local cQryUpd   := ""
Local cAliasQry
Local cCliGreen 
Local cTipPedGreen
Local cTES :=""
Local cSitClass:= ""
Local cOrigem:= ""
Local cClasFis:=""
Local cCFOP := ""
Local cUF:= ""
Local cProdSaldo := ""
Local nx   := 0
Local ny   := 0
Private cTipos    := ""
Private cClientes := ""
    

PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101' 

cCliGreen :=  AllTrim(SuperGetMV("HP_CLIGRN",.F.,"")) 
cTipPedGreen:=  AllTrim(SuperGetMV("HP_TIPGRN",.F.,""))  

If !Empty(cCliGreen)
	For ny := 1 to len(cCliGreen) step 7 
		cAux := SubStr(cCliGreen,ny,7)
		If cAux <> '*******'
			cClientes += "'"+SubStr(cAux,1,6)+"',"
		EndIf
	Next
EndIf

cClientes := SubStr(cClientes,1,Len(cClientes)-1)

If !Empty(cTipPedGreen)
	For nx := 1 to len(cTipPedGreen) step 4 
		cAux := SubStr(cTipPedGreen,nx,4)
		If cAux <> '****'
			cTipos += "'"+SubStr(cAux,1,3)+"',"
		EndIf
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)

cAliasQry := GetNextAlias()   

//--REL. ANALITICO REGRA DE PRIORIDADE 
cQuery += "SELECT *, (SELECT SUM(B2_QATU) - (SUM(B2_RESERVA) + SUM(B2_XRESERV)) AS SALDO FROM "+RetSqlName("SB2")+" WITH (NOLOCK)"+ CRLF 
cQuery += "			WHERE D_E_L_E_T_ = '' AND B2_FILIAL = '0101' AND B2_LOCAL IN ('E0','E1') AND B2_COD = R.PRODUTO_PRIORIDADE GROUP BY B2_FILIAL, B2_COD"+ CRLF
cQuery += "		   ) AS SALDO"+ CRLF
cQuery += "FROM ("+ CRLF
cQuery += "SELECT C6.C6_FILIAL AS FILIAL,"+ CRLF
cQuery += "       C5.C5_ORIGEM AS ORIGEM,"+ CRLF
cQuery += "       Z1_TPOPER AS TIPO_OPERACAO,"+ CRLF
cQuery += "       CONVERT(CHAR, CAST(C5.C5_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO,"+ CRLF
cQuery += "	   IIF(C5.C5_XBLQ = 'B','BLOQUEADO','LIBERADO') AS BLQ_COMERCIAL,"+ CRLF
cQuery += "	   IIF(C6.C6_GRADE = 'S','SIM','N�O') AS GRADE,"+ CRLF
cQuery += "       C6.C6_NUM AS PEDIDO,"+ CRLF
cQuery += " 	  C5.C5_CLIENTE,"+ CRLF
cQuery += " 	  C5.C5_LOJACLI,"+ CRLF
cQuery += "       C5.C5_TIPOCLI,"+ CRLF
cQuery += "	   ISNULL(ZJ.ZJ_NUMPF,'') AS PRE_FATURAMENTO,"+ CRLF
cQuery += "	   C5.C5_CLIENTE +' - '+ C5.C5_XNOMCLI AS CLIENTE,"+ CRLF
cQuery += "	   C5.C5_POLCOM +' - '+ C5.C5_XDESCPO AS POLITICA,"+ CRLF
cQuery += "	   C5.C5_TPPED +' - '+ Z1.Z1_DESC AS TIPO_PEDIDO,"+ CRLF
cQuery += "	   C6.C6_PRODUTO AS PRODUTO_PEDIDO,"+ CRLF
cQuery += "	   C6_QTDVEN AS QTDE_VENDA,"+ CRLF
cQuery += "	   C6_QTDENT AS QTDE_ENTREGUE,"+ CRLF
cQuery += "	   CASE WHEN (SELECT ZB5_PROIRI FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO) = 'N' THEN (SELECT ZB5_PRDNAC FROM ZB5010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO)"+ CRLF
cQuery += "	   ELSE CASE WHEN (SELECT ZB5_PROIRI FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO) = 'I' THEN (SELECT ZB5_PRDIMP FROM ZB5010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO)"+ CRLF
cQuery += "	   ELSE CASE WHEN (SELECT ZB5_PROIRI FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDIMP  = C6.C6_PRODUTO) = 'N' THEN (SELECT ZB5_PRDNAC FROM ZB5010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDIMP  = C6.C6_PRODUTO)"+ CRLF
cQuery += "	   ELSE CASE WHEN (SELECT ZB5_PROIRI FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDIMP  = C6.C6_PRODUTO) = 'I' THEN (SELECT ZB5_PRDIMP FROM ZB5010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDIMP  = C6.C6_PRODUTO)"+ CRLF
cQuery += "	   ELSE '' END END END END AS PRODUTO_PRIORIDADE,"+ CRLF
cQuery += ""+ CRLF
cQuery += "	   CASE WHEN (SELECT ZB5_PROIRI FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO) = 'N' THEN (SELECT ZB5_PROIRI FROM ZB5010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO)"+ CRLF
cQuery += "	   ELSE CASE WHEN (SELECT ZB5_PROIRI FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO) = 'I' THEN (SELECT ZB5_PROIRI FROM ZB5010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO)"+ CRLF
cQuery += "	   ELSE CASE WHEN (SELECT ZB5_PROIRI FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDIMP  = C6.C6_PRODUTO) = 'N' THEN (SELECT ZB5_PROIRI FROM ZB5010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDIMP  = C6.C6_PRODUTO)"+ CRLF
cQuery += "	   ELSE CASE WHEN (SELECT ZB5_PROIRI FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDIMP  = C6.C6_PRODUTO) = 'I' THEN (SELECT ZB5_PROIRI FROM ZB5010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDIMP  = C6.C6_PRODUTO)"+ CRLF
cQuery += "	   ELSE '' END END END END AS PROIRIDADE,"+ CRLF
cQuery += ""+ CRLF
cQuery += "	   CASE WHEN (SELECT ZB5_NSUBCO FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO) <> '' THEN (SELECT ZB5_NSUBCO FROM ZB5010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDNAC  = C6.C6_PRODUTO)"+ CRLF+ CRLF
cQuery += "	   ELSE (SELECT ZB5_NSUBCO FROM ZB5010 WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZB5_PRDIMP  = C6.C6_PRODUTO)"+ CRLF
cQuery += "	   END AS SUBCOLECAO"+ CRLF
cQuery += ""+ CRLF
cQuery += "FROM "+RetSqlName("SC6")+" AS C6 WITH (NOLOCK) "+ CRLF
cQuery += "INNER JOIN "+RetSqlName("SC5")+" AS C5 WITH (NOLOCK) ON (C5.C5_FILIAL = C6.C6_FILIAL AND C5.C5_NUM = C6.C6_NUM AND C5.C5_CLIENTE = C6.C6_CLI AND C5.D_E_L_E_T_ = '')"+ CRLF
cQuery += "INNER JOIN "+RetSqlName("SZ1")+" AS Z1 WITH (NOLOCK) ON (Z1.Z1_CODIGO = C5.C5_TPPED AND Z1.D_E_L_E_T_ = '')"+ CRLF
cQuery += "LEFT  JOIN "+RetSqlName("SZJ")+" AS ZJ WITH (NOLOCK) ON (ZJ.ZJ_FILIAL = C6.C6_FILIAL AND ZJ.ZJ_PEDIDO = C6.C6_NUM AND C6.C6_PRODUTO = ZJ.ZJ_PRODUTO AND ZJ.ZJ_DOC = '' AND ZJ.D_E_L_E_T_ = '')"+ CRLF
cQuery += "WHERE C6.D_E_L_E_T_ = '' AND C6.C6_BLQ = '' AND C5.C5_EMISSAO BETWEEN '20180101' AND '"+DtoS(dDatabase)+"'"+ CRLF
cQuery += "AND C5.C5_TPPED NOT IN ("+cTipos+")"+ CRLF// --HP_TIPGRN
cQuery += "AND C5.C5_CLIENTE NOT IN ("+cClientes+")" + CRLF//--HP_CLIGRN

//cQuery += "AND C6_NUM IN ('RDWMWT') "+ CRLF
//cQuery += "AND C6.C6_PRODUTO = '00I26320PTO000P'"+ CRLF

cQuery += "GROUP BY C6.C6_FILIAL,"+ CRLF
cQuery += "    C5.C5_CLIENTE,"+ CRLF
cQuery += "    C5.C5_LOJACLI,"+ CRLF
cQuery += "    C5.C5_TIPOCLI,"+ CRLF
cQuery += "    C5.C5_ORIGEM,"+ CRLF
cQuery += "    C5.C5_EMISSAO,"+ CRLF
cQuery += "	   C5.C5_XBLQ,"+ CRLF
cQuery += "	   C6.C6_GRADE,"+ CRLF
cQuery += "    C6.C6_NUM,"+ CRLF
cQuery += "	   ZJ.ZJ_NUMPF,"+ CRLF
cQuery += "	   C5.C5_CLIENTE,"+ CRLF
cQuery += "	   C5.C5_XNOMCLI,"+ CRLF
cQuery += "	   C5.C5_POLCOM,"+ CRLF
cQuery += "	   C5.C5_XDESCPO,"+ CRLF
cQuery += "	   C5.C5_TPPED,"+ CRLF
cQuery += "	   Z1.Z1_DESC,"+ CRLF
cQuery += "	   C6.C6_PRODUTO,"+ CRLF
cQuery += "	   C6.C6_QTDVEN,"+ CRLF
cQuery += "	   Z1_TPOPER,"+ CRLF
cQuery += "	   C6_QTDENT"+ CRLF
cQuery += "HAVING (C6_QTDVEN - C6_QTDENT) > 0 "+ CRLF
cQuery += ""+ CRLF
cQuery += ") R WHERE R.PRODUTO_PRIORIDADE <> '' AND R.PRE_FATURAMENTO = ''"+ CRLF

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),cAliasQry, .F., .T.)       

(cAliasQry)->(dbgotop())

While (cAliasQry)->(!EOF())

    IF (cAliasQry)->PRODUTO_PRIORIDADE == (cAliasQry)->PRODUTO_PEDIDO .AND. (cAliasQry)->SALDO >= (cAliasQry)->QTDE_VENDA
    	(cAliasQry)->(dbSkip())
    ELSE
        //Verifica produto que tem saldo:
	    cProdSaldo := VerSaldo((cAliasQry)->PRODUTO_PRIORIDADE,(cAliasQry)->PROIRIDADE,(cAliasQry)->QTDE_VENDA)
	
	    cTES  := MaTesInt(2,(cAliasQry)->TIPO_OPERACAO,(cAliasQry)->C5_CLIENTE,(cAliasQry)->C5_LOJACLI,"C",cProdSaldo,"",(cAliasQry)->C5_TIPOCLI)
	    cSitClass:= POSICIONE("SF4",1,xFilial("SF4")+cTES,"F4_SITTRIB")
	    cOrigem:= POSICIONE("SB1",1,xFilial("SB1")+cProdSaldo,"B1_ORIGEM")
	    cClasFis:= cOrigem+cSitClass
	    cUF := GetAdvFVal("SA1","A1_EST",xFilial("SA1")+(cAliasQry)->C5_CLIENTE+(cAliasQry)->C5_LOJACLI,1,"CE")
	    cCFOP:= POSICIONE("SF4",1,xFilial("SF4")+cTES,"F4_CF")
	    
	    If cUF = "CE"
	        cDig := "5"
	    ElseIf cUF = "EX"
	        cDig := "7"
	    Else
	        cDig := "6"
	    EndIf	
	    //MV_ESTADO
	    cCFOP := cDig + SubStr(cCFOP,2,3)
	
	    cQryUpd:= "UPDATE "+RetSQLName("SC6")+" SET "+ CRLF
	    cQryUpd+= "        C6_PRODUTO = '"+cProdSaldo+"',"+ CRLF
	    cQryUpd+= "        C6_CF = '"+cCFOP+"',"+ CRLF
	    cQryUpd+= "        C6_TES = '"+cTES+"',"+ CRLF
	    cQryUpd+= "        C6_CLASFIS = '"+cClasFis+"'"+ CRLF
	    cQryUpd+= " WHERE  C6_FILIAL = '"+(cAliasQry)->FILIAL+"'"+ CRLF
	    cQryUpd+= "        AND C6_NUM = '"+(cAliasQry)->PEDIDO+"'"+ CRLF
	    cQryUpd+= "        AND C6_CLI = '"+(cAliasQry)->C5_CLIENTE+"'"+ CRLF
	    cQryUpd+= "        AND C6_PRODUTO = '"+(cAliasQry)->PRODUTO_PEDIDO+"'"
	
	    TcSqlExec(cQryUpd)
	    
	    (cAliasQry)->(dbSkip())
	EndIf    
Enddo


/*/{Protheus.doc} VerSaldo
	(Verifica se o produto prioridade tem saldo disponivel, caso n�o, troca o produto.)
	@type  Function
	@author Ronaldo pereira
	@since 30/06/2020
	@version version 1.0
	@return cProduto (Produto com Saldo)
	/*/
Static Function VerSaldo(cProd,cPrio,cQtped)

local cProduto	:= ""
Local cQry    	:= ""

    If Select("ESTOQUE") > 0
    	ESTOQUE->(DbCloseArea())
	EndIf

	cQry := "SELECT SUM(B2_QATU) - (SUM(B2_RESERVA) + SUM(B2_XRESERV)) AS SALDO "
	cQry += "FROM "+RetSqlName("SB2")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND B2_FILIAL = '0101' AND B2_LOCAL IN ('E0','E1') "
	cQry += "AND B2_COD = '"+AllTrim(cProd)+"' "
	cQry += "GROUP BY B2_FILIAL, B2_COD "
	
	TCQUERY cQry NEW ALIAS ESTOQUE

	IF ESTOQUE->SALDO >= cQtped	
		cProduto := cProd
	ELSE
		IF cPrio = 'N' 
			DbSelectArea("ZB5")
			DbSetOrder(1)//Indice prod. Nacional
		    If	DbSeek(xfilial("ZB5")+cProd)	
				cProduto := ZB5->ZB5_PRDIMP
	        EndIf
		ELSE
			DbSelectArea("ZB5")
			DbSetOrder(2)//Indice prod. Importado
		    If	DbSeek(xfilial("ZB5")+cProd)	
				cProduto := ZB5->ZB5_PRDNAC
	        EndIf
		EndIf	
	EndIf
	
Return cProduto

Return