/*#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "AARRAY.CH"
#INCLUDE "JSON.CH"

WSRESTFUL consultacliente DESCRIPTION "Retorna os dados de um cliente"
	WSDATA Cpf AS STRING 
	WSMETHOD GET DESCRIPTION "Retorna os dados de um cliente" WSSYNTAX "/consultacliente/{Cpf}"
END WSRESTFUL

WSMETHOD GET WSRECEIVE Cpf WSSERVICE consultacliente
	Local lRet := .F.
	Local aResult := {"",""}
	
	::SetContentType("application/json")
	
	If Len(::aURLParms) > 0
		dbSelectArea('SA1')
		dbSetOrder(3)
		DbSeek(xFilial('SA1')+::aURLParms[1])
		If Found()
			// Informações do cliente
			aResult[#'Codigo'] := alltrim(SA1->A1_COD)
			aResult[#'Loja'] := alltrim(SA1->A1_LOJA)
			::SetResponse(ToJson(aResult))
			lRet := .T.
		Else
			SetRestFault(404, "Cliente nao encontrado")
		EndIf
	Else
		SetRestFault(400, "Favor informar o CFP do cliente")
	EndIf
Return(lRet)*/

#Include 'Protheus.ch'
#Include 'FWMVCDEF.ch'
#Include 'RestFul.CH'
#include 'Totvs.ch'

User Function APICLIENTE()
Return

Class CONSULTACLI
	
	Data cCodigo	As String
	Data cLoja	  	As String
	Data cPk        As String

	Method New(cCodigo,cLoja,cPk) Constructor
	 
EndClass

Method New(cCodCli, cLojCli, cPk) Class CONSULTACLI
	::cCodigo := cCodCli
	::cLoja   := cLojCli
	::cPk     := cPk
Return(Self)

Class ERROCLI
	
	Data cErro	As String
	
	Method New(cErro) Constructor
	 
EndClass

Method New(cCodErro) Class ERROCLI
	::cErro := cCodErro
Return(Self)

WSRESTFUL CONSULTACLI DESCRIPTION "Servico REST para consulta de Clientes"
	WSDATA CPF As String
	WSMETHOD GET DESCRIPTION "Retorna o codigo e loja do cliente" WSSYNTAX "/CONSULTACLI/{CPF}"
END WSRESTFUL

WSMETHOD GET WSRECEIVE CPF WSSERVICE CONSULTACLI
Local cCPF     := Self:CPF
Local aArea    := GetArea()
Local oObjCli  := Nil
Local oObjErro := Nil
//Local cStatus  := ""
Local cJson    := ""

::SetContentType("application/json")

If !Empty(cCPF)
	DbSelectArea("SA1")
	SA1->( DbSetOrder(3) )
	If SA1->( DbSeek( xFilial("SA1") + cCPF ) )
	    //cStatus  := Iif( SB1->B1_MSBLQL == "1", "Sim", "Nao" )
	    oObjCli := CONSULTACLI():New(SA1->A1_COD,SA1->A1_LOJA,Encode64(xFilial("SA1")+SA1->A1_COD+SA1->A1_LOJA))
	    
	    cJson := FWJsonSerialize(oObjCli)

		::SetResponse(cJson)
	Else
		oObjErro := ERROCLI():New("Cliente nao encontrado")
		cJson := FWJsonSerialize(oObjErro)
		::SetResponse(cJson)
	EndIf
Else
	//::SetResponse("Favor informar o CFP do cliente para consulta")
	oObjErro := ERROCLI():New("Favor informar o CFP do cliente para consulta")
	cJson := FWJsonSerialize(oObjErro)
	::SetResponse(cJson)
EndIf

RestArea(aArea)
Return(.T.)