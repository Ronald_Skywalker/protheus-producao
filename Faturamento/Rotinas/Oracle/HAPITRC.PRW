#Include 'Protheus.ch'
#Include 'FWMVCDef.ch'
#Include 'RestFul.CH'
#Include 'Parmtype.ch'

PUBLISH USER MODEL REST NAME HAPITROCA SOURCE HAPITRC

Static cTitulo := "Pre-Nota de Entrada Protheus"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HAPITRC   �Autor  �Bruno Parreira      � Data �  14/02/18   ���
�������������������������������������������������������������������������͹��
���Desc.     �API para geracao de pre-bota de entrada. Utilizada na troca ���
���          �automatica do ORACLE.                                       ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HAPITRC()
Local aArea       := GetArea()
Local oBrowse

oBrowse := FWMBrowse():New()
oBrowse:SetAlias("ZZE")
oBrowse:SetDescription(cTitulo)
oBrowse:Activate()

RestArea(aArea)

Return Nil

/*---------------------------------------------------------------------*
 | Func:  MenuDef                                                      |
 | Desc:  Cria��o do menu MVC                                          |
 *---------------------------------------------------------------------*/

Static Function MenuDef()
Local aRot := {}

//Adicionando op��es
//ADD OPTION aRot TITLE 'Visualizar' ACTION 'VIEWDEF.HAPITRC' OPERATION MODEL_OPERATION_VIEW   ACCESS 0 //OPERATION 1
ADD OPTION aRot TITLE 'Incluir'    ACTION 'VIEWDEF.HAPITRC' OPERATION MODEL_OPERATION_INSERT ACCESS 0 //OPERATION 3
//ADD OPTION aRot TITLE 'Alterar'    ACTION 'VIEWDEF.HAPITRC' OPERATION MODEL_OPERATION_UPDATE ACCESS 0 //OPERATION 4
//ADD OPTION aRot TITLE 'Excluir'    ACTION 'VIEWDEF.HAPITRC' OPERATION MODEL_OPERATION_DELETE ACCESS 0 //OPERATION 5

Return aRot

/*---------------------------------------------------------------------*
 | Func:  ModelDef                                                     |
 | Desc:  Cria��o do modelo de dados MVC                               |
 *---------------------------------------------------------------------*/

Static Function ModelDef()
Local oModel   := Nil
Local oStPai   := FWFormStruct(1, 'ZZE') //FWFormModelStruct():New() //Cabecalho
Local oStFilho := FWFormStruct(1, 'ZZF') //FWFormModelStruct():New() //Itens

//Local bVldAbre := {|| .T.}//{|| u_HAPIABRE()} //Antes de abrir a Tela
Local bVldConf := {|| u_HAPICONF()} //Validacao ao clicar no Confirmar
Local bVldComi := {|| u_HAPICOMI()} //Funcao executada no Commit Data
//Local bVldCanc := {|| FWFORMCANCEL(SELF)}//{|| u_HAPICANC()} //Funcao chamada ao cancelar

Local aZZFRel := {}

//Instanciando o modelo, n�o � recomendado colocar nome da user function (por causa do u_), respeitando 10 caracteres
oModel := MPFormModel():New("HAPITRCM",/*bVldAbre*/,/*bVldConf*/,bVldComi,/*bVldCanc*/) 

//Atribuindo formul�rios para o modelo
oModel:AddFields("ZZEMASTER",/*cOwner*/,oStPai)
oModel:AddGrid('ZZFDETAIL','ZZEMASTER',oStFilho,/*bLinePre*/, /*bLinePost*/,/*bPre - Grid Inteiro*/,/*bPos - Grid Inteiro*/,/*bLoad - Carga do modelo manualmente*/)  //cOwner � para quem pertence

//Fazendo o relacionamento entre o Pai e Filho
//aAdd(aZZFRel,{'ZZF_FILIAL',xFilial("ZZF")})
aAdd(aZZFRel,{'ZZF_NFORI','ZZE_NFORI'})

oModel:SetRelation('ZZFDETAIL', aZZFRel, ZZF->(IndexKey(1))) //IndexKey -> quero a ordena��o e depois filtrado
oModel:GetModel('ZZFDETAIL'):SetUniqueLine({"ZZF_COD"})	//N�o repetir informa��es ou combina��es {"CAMPO1","CAMPO2","CAMPOX"}

//Setando a chave prim�ria da rotina
oModel:SetPrimaryKey({'ZZE_NFORI'})

//Adicionando descri��o ao modelo
oModel:SetDescription("Modelo de Dados do Cadastro "+cTitulo)

//Setando a descri��o do formul�rio
oModel:GetModel("ZZEMASTER"):SetDescription("Cabecalho")
oModel:GetModel("ZZFDETAIL"):SetDescription("Itens")

Return oModel

/*---------------------------------------------------------------------*
 | Func:  ViewDef                                                      |
 | Desc:  Cria��o da vis�o MVC                                         |
 *---------------------------------------------------------------------*/

Static Function ViewDef()
Local oModel   := FWLoadModel("HAPITRC")
Local oStPai   := FWFormStruct(2, 'ZZE')
Local oStFilho := FWFormStruct(2, 'ZZF')
Local oView    := Nil

//Criando a view que ser� o retorno da fun��o e setando o modelo da rotina
oView := FWFormView():New()
oView:SetModel(oModel)

//Atribuindo formul�rios para interface
oView:AddField("VIEW_ZZE",oStPai,"ZZEMASTER")
oView:AddGrid("VIEW_ZZF",oStFilho,"ZZFDETAIL")

//Setando o dimensionamento de tamanho
oView:CreateHorizontalBox('CABEC',30)
oView:CreateHorizontalBox('GRID' ,70)

//Amarrando a view com as box
oView:SetOwnerView('VIEW_ZZE','CABEC')
oView:SetOwnerView('VIEW_ZZF','GRID')

//Habilitando t�tulo
oView:EnableTitleView('VIEW_ZZE','Cabe�alho - Pre-Nota')
oView:EnableTitleView('VIEW_ZZF','Itens - Pre-Nota')

//For�a o fechamento da janela na confirma��o
oView:SetCloseOnOk({||.T.})

Return oView

User Function HAPICONF()
Local lRet      := .T.

Return lRet

User Function HAPICOMI()
Local oModelPad := FWModelActive()
Local cCbFil    := oModelPad:GetValue('ZZEMASTER','ZZE_FILIAL')
Local nOpc      := oModelPad:GetOperation()
Local lRet      := .T.
Local oModelFil := oModelPad:GetModel( 'ZZFDETAIL' )
Local nI        := 0
Local ny 		:= 0
Local aItWs     := {}
Local cItPed    := ""
Local cSerie    := AllTrim(SuperGetMV("MV_XSERTRC",.F.,"2")) //Serie utilizada nas pre-notas de troca automatica (API)
Local cLocal    := AllTrim(SuperGetMV("MV_XLOCTRC",.F.,"E1")) //Armazem para  inclusao das pre-notas de troca automatica. 

Private cLog    := ""
Private lMsErroAuto := .F.

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

GravaLog("In�cio do processamento...")

lContinua := .T.

If nOpc == MODEL_OPERATION_INSERT

	aCabec := {}
	
	aadd(aCabec,{"F1_TIPO"   ,oModelPad:GetValue('ZZEMASTER','ZZE_TIPO')})
	aadd(aCabec,{"F1_SERIE"  ,""})
	aadd(aCabec,{"F1_FORMUL" ,oModelPad:GetValue('ZZEMASTER','ZZE_FORMUL')})
	aadd(aCabec,{"F1_DOC"    ,""})
	aadd(aCabec,{"F1_EMISSAO",oModelPad:GetValue('ZZEMASTER','ZZE_EMIS')})
	aadd(aCabec,{"F1_FORNECE",oModelPad:GetValue('ZZEMASTER','ZZE_FORNEC')})
	aadd(aCabec,{"F1_LOJA"   ,oModelPad:GetValue('ZZEMASTER','ZZE_LOJA')})
	aadd(aCabec,{"F1_MENNOTA",oModelPad:GetValue('ZZEMASTER','ZZE_MENNOT')})
	aadd(aCabec,{"F1_XNFORI" ,oModelPad:GetValue('ZZEMASTER','ZZE_NFORI')})
	aadd(aCabec,{"F1_XSERORI",oModelPad:GetValue('ZZEMASTER','ZZE_SERORI')})
	
	cSerieNF := oModelPad:GetValue('ZZEMASTER','ZZE_SERORI')

	aItWs := {}

	For ny := 1 to oModelFil:Length()
		oModelFil:GoLine(ny)
		
		cFilPre  := oModelFil:GetValue("ZZF_FILIAL")
		
		cNFOrig := oModelFil:GetValue("ZZF_NFORI")
		cItemNF := oModelFil:GetValue("ZZF_ITEORI")
		cProd   := oModelFil:GetValue("ZZF_COD")
		nQtde   := oModelFil:GetValue("ZZF_QTDE")
		nVlrUni := ROUND(oModelFil:GetValue("ZZF_VUNIT"),8)
		nTotal  := ROUND(nQtde*nVlrUni,2)
		
		cUM := GetAdvFVal("SB4","B4_UM",xFilial("SB4")+SubStr(cProd,1,8),1,"")
		
		//             1   2    3      4      5       6      7    
		aAdd(aItWs,{cProd,cUM,nQtde,nVlrUni,nTotal,cNFOrig,cItemNF})
	Next
	
	aLinha := {}
	aItens := {}

	For ny := 1 to Len(aItWs)
	
		aadd(aLinha,{"D1_COD"    ,aItWs[ny][1],Nil})
		aadd(aLinha,{"D1_UM"     ,aItWs[ny][2],Nil})
		aadd(aLinha,{"D1_QUANT"  ,aItWs[ny][3],Nil})
		aadd(aLinha,{"D1_VUNIT"  ,aItWs[ny][4],Nil})
		aadd(aLinha,{"D1_TOTAL"  ,aItWs[ny][5],Nil})
		aadd(aLinha,{"D1_VALDESC",0,Nil})
		aadd(aLinha,{"D1_DOC"    ,"", Nil})
		aadd(aLinha,{"D1_LOCAL"  ,cLocal,Nil})
		aadd(aLinha,{"D1_NFORI"  ,aItWs[ny][6],Nil})
		aadd(aLinha,{"D1_SERORI" ,cSerieNF,Nil})
		aadd(aLinha,{"D1_ITEORI" ,aItWs[ny][7],Nil})
		
		aadd(aItens,aLinha)
	Next
	
	BEGIN TRANSACTION
	
	If Len(aCabec)>0 .and. Len(aItens)>0
	 	MATA140(aCabec,aItens)
	Endif
	
	If lMsErroAuto
	 	cMsg := "Erro na gera��o da Pr�-Nota"
		MostraErro("\log\API\",cDtTime+"_ERRO_PRE-NF.log")
		SalvaLog("ERRO_EXECAUTO",cMsg)
		oModelPad:SetErrorMessage('ZZEMASTER','ZZE_NFORI','ZZEMASTER','ZZE_NFORI',"PRENOTA",cMsg,'Verificar Log.')
		DisarmTransaction()
		lRet := .F.
	Else
		GravaLog("Pr�-Nota foi gerada com sucesso.")
		FWFormCommit(oModelPad)
	Endif
		
	END TRANSACTION	

EndIf

GravaLog("Fim do processamento.")

SalvaLog("PRE-NOTA_INCLUIDA")

Return lRet

Static Function SalvaLog(cCompl,cMsg)

If !Empty(cMsg)
	GravaLog(cMsg)
EndIf

MemoWrite("\log\API\"+cDtTime+"_"+cCompl+".log",cLog)

Return

Static Function GravaLog(cMsg)
Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cMsg)
cLog += CRLF+cDtHora+": HAPITRC - "+cMsg

Return