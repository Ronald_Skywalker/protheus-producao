#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

User Function GERASZM()
Local cQuery := ""

//PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101' TABLES 'SD3,SZM' MODULO 'FAT'

cQuery := "select D3_COD, "
cQuery += CRLF + "(select D3_LOCALIZ from SD3010 SD3_2 where SD3_2.D3_COD = SD3_1.D3_COD and SD3_2.D_E_L_E_T_ = '' and SD3_2.D3_DOC = SD3_1.D3_DOC and D3_TM='999') AS ENDORIG, "
cQuery += CRLF + "D3_LOCALIZ,D3_LOTECTL,D3_NUMLOTE,D3_QUANT "
cQuery += CRLF + "from SD3010 SD3_1 "
cQuery += CRLF + "where D3_DOC between '000000006' and '000000031' and D3_TM = '499' "

MemoWrite("GERASZM.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),"TRB",.F.,.T.)

DbSelectArea("TRB")

cItem := "01"

While TRB->(!EOF())
	DbSelectArea("SZM")
	RecLock("SZM",.T.)
	SZM->ZM_FILIAL  := xFilial("SZM")
	SZM->ZM_PRODUTO := TRB->D3_COD
	SZM->ZM_AMZORIG := "E1"
	SZM->ZM_ENDORIG := TRB->ENDORIG
	SZM->ZM_AMZDEST := "E0"
	SZM->ZM_ENDDEST := TRB->D3_LOCALIZ
	SZM->ZM_LOTECTL := TRB->D3_LOTECTL
	SZM->ZM_NUMLOTE := TRB->D3_NUMLOTE
	SZM->ZM_QUANT   := TRB->D3_QUANT
	SZM->ZM_NUMPF   := "DIFINTEG"
	SZM->ZM_ITEM    := cItem
	SZM->ZM_DATA    := DDATABASE
	SZM->ZM_HORA    := Time()
	SZM->ZM_EMPILHA := If(SubStr(TRB->ENDORIG,1,1)="1","F","C")
	MsUnlock()

	cItem := SOMA1(cItem,2)

	TRB->(DbSkip())
EndDo

//RESET ENVIRONMENT

Return

