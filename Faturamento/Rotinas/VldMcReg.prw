#Include 'Protheus.ch'

//Protheus 12.1.25 - Hope Lingerie
//Fun��o: VldMcReg()
//Descri��o: Verifica se o c�digo da macro regi�o j� foi cadastrado anteriormente para evitar duplicidade de registros.
//Autor: Jos� Carlos Ferrari
//Data: 29/05/2020
User Function VldMcReg()		

Local cQuery 	:= ""
Local cTabReg	:= ""
Local cCodMac	:= M->ZA4_CODMAC 
Local lRet 		:= .F.

IF SELECT("REG") > 0
	REG->(DbCloseArea())
ENDIF

cQuery := "SELECT * FROM "+RetSqlName("ZA4")+" WHERE D_E_L_E_T_ = '' "
cQuery += "AND ZA4_FILIAL ='"+xFilial("ZA4")+"' AND ZA4_CODMAC = '"+cCodMac+"'"

dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"REG",.F.,.T.)

cTabReg := REG->ZA4_CODMAC

IF Alltrim(cTabReg) == Alltrim(cCodMac)
	MsgAlert("J� existe uma macro regi�o para este c�digo!!")
	lRet := .F.
Else 
	lRet := .T.
EndIf

Return lRet