#include 'protheus.ch'
#include 'fwprintsetup.ch'
#include 'rptdef.ch'
#include 'danfeetiqueta.ch'
#INCLUDE "TOPCONN.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} ImpDfEtq
Impress�o de danfe simplificada - Etiqueta

@param		cUrl			Endere�o do Web Wervice no TSS
            cIdEnt			Entidade do TSS para processamento
			lUsaColab		Totvs Colabora��o ou n�o
/*/
//-------------------------------------------------------------------
user function ImpDfEtq(cUrl, cIdEnt, lUsaColab)
	local lOk        := .F.
	local aArea      := {}
	local aAreaCB5   := {}
	local aAreaSF3   := {}
	local aAreaSA1   := {}
	local aAreaSA2   := {}
	local aAreaSC5   := {}
	local aAreaSC6   := {}
	local cNotaIni   := ""
	local cNotaFim   := ""
	local cSerie     := ""
	local dDtIni     := ctod("")
	local dDtFim     := ctod("")
	local nTipoDoc   := 0
	local nTipImp    := 0
	local cLocImp    := ""
	local nTamSerie  := 0
	local lSdoc      := .F.
	local cAliasQry  := ""
	local cWhere     := ""
	local lMv_Logod  := .F.
	local cLogo      := ""
	local cLogoD	 := ""
	local oPrinter   := nil
	local oSetup     := nil
	local nLinha     := 0
	local nColuna    := 0
	local cGrpCompany:= ""
	local cCodEmpGrp := ""
	local cUnitGrp	 := ""
	local cFilGrp	 := ""
	local cDescLogo  := ""
	local oFontTit   := nil
	local oFontInf   := nil
	local aParam     := {}
	local aNotas     := {}
	local cAviso     := ""
	local cErro      := ""
	local nNotas     := 0
	local cProtocolo := ""
	local cDpecProt  := ""
	local cXml       := ""
	local oTotal     := nil
	local cTotNota   := ""
	local cHautNfe   := ""
	local dDautNfe   := ctod("")
	local aNFe       := {}
	local aEmit      := {}
	local aDest      := {}
	local cCgc       := ""
	local cNome      := ""
	local cInscr     := ""
	local cUF        := ""
	local nContDanfe := 0
	Local nX		:= 0

	default cUrl      := PARAMIXB[1]
	default cIdEnt    := PARAMIXB[2]
	default lUsaColab := PARAMIXB[3]

	private oRetNF   := nil
	private oNFe     := nil
	Private aInfAd := {}
	Private cTipos := ""

	begin sequence

		aArea := getArea()

		dbSelectArea("CB5")
		aAreaCB5 := CB5->(getArea())

		dbSelectArea("SF3")
		aAreaSF3 := SF3->(getArea())

		dbSelectArea("SA1")
		aAreaSA1 := SA1->(getArea())

		dbSelectArea("SA2")
		aAreaSA2 := SA2->(getArea())

		dbSelectArea("SC6")
		aAreaSC6 := SC6->(getArea())

		dbSelectArea("SC5")
		aAreaSC5 := SC5->(getArea())

		If !Pergunte("NFDANFETIQ",.T.)
			break
		EndIf

		cNotaIni := MV_PAR01 // Nota Inicial
		cNotaFim := MV_PAR02 // Nota Final
		cSerie := MV_PAR03 // Serie
		dDtIni := MV_PAR04 // Data de emiss�o Inicial
		dDtFim := MV_PAR05 // Data de emiss�o Final
		nTipoDoc := MV_PAR06 // Tipo de Opera��o (1 - Entrada / 2 - Sa�da)
		nTipImp := MV_PAR07 // Tipo de Impressora (1 - T�rmica / 2 - Normal)
		cLocImp := MV_PAR08 // Impressora
		// TODO CUSTOMIZADO INICIO
		cCfIni  := MV_PAR09 // CFOP de ?
		cCfim  := MV_PAR10 // CFOP at� ?
		cRegio  := MV_PAR11 // Regional ?
		// TODO CUSTOMIZADO FIM

		// TODO CUSTOMIZADO INICIO
	cRegio := AllTrim(MV_PAR11) 

If !Empty(cRegio)
	For nx := 1 to len(cRegio) step 2
		cAux := SubStr(cRegio,nx,2)
		If cAux <> '**'
		if nx == len(cRegio)
			cTipos += "'0"+cAux+"'"
		else
			cTipos += "'0"+cAux+"',"
		endif	
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)
// TODO CUSTOMIZADO FIM

		// Valida��es para impressoras termicas
		If nTipImp == 1
			If empty(cLocImp)
				Help(NIL, NIL, STR0001, NIL, STR0002, 1, 0, NIL, NIL, NIL, NIL, NIL, {STR0003}) //local de impress�o n�o informado., Informe um local de impress�o cadastrado., Acesse a rotina 'Locais de Impress�o'.
				break
			Else
				CB5->(dbSetOrder(1))
				If !CB5->(DbSeek( xFilial("CB5") + padR(cLocImp, GetSX3Cache("CB5_CODIGO", "X3_TAMANHO")) )) .or. !CB5SetImp(cLocImp)
					Help(NIL, NIL, STR0004 + " - " + alltrim(cLocImp) + ".", NIL, STR0002, 1, 0, NIL, NIL, NIL, NIL, NIL, {STR0003}) //local de impress�o n�o encontrado, Informe um local de impress�o cadastrado., Acesse a rotina 'Locais de Impress�o'.
					break
				EndIf
			EndIf
		EndIf

		If val(cNotaIni) > val(cNotaFim)
			Help(NIL, NIL, STR0005, NIL, STR0006, 1, 0, NIL, NIL, NIL, NIL, NIL, {STR0007}) //Valores de numera��o de documentos inv�lidos., Informe um intervalo v�lido de notas., Verifique as informa��es do intervalo de notas.
			break
		EndIf

		nTamSerie := GetSX3Cache("F3_SERIE", "X3_TAMANHO")
		lSdoc := nTamSerie == 14
		cSerie := Padr(cSerie, nTamSerie )
		cWhere := "% SF3.F3_SERIE = '"+ cSerie + "' AND SF3.F3_ESPECIE IN ('SPED','NFCE') "

		If lSdoc
			cSerie := Padr(cSerie, GetSX3Cache("F3_SDOC", "X3_TAMANHO") )
			cWhere := "% SF3.F3_SDOC = '"+ cSerie + "' AND SF3.F3_ESPECIE IN ('SPED','NFCE') "
		EndIf

		If nTipoDoc == 1
			cWhere += " AND SubString(SF3.F3_CFO,1,1) < '5' AND SF3.F3_FORMUL = 'S' "
		ElseIf nTipoDoc == 2
			cWhere += " AND SubString(SF3.F3_CFO,1,1) >= '5' "

			//TODO Tratamento para impress�o via CFOP.
			//   	Geyson Albano 02/09/21

			If !Empty(MV_PAR09)

				If lSdoc
					//	cCampos += ", SF3.F3_SDOC"
					cSerie := Padr(MV_PAR03,TamSx3("F3_SDOC")[1])
					cWhere += " AND SF3.F3_SDOC = '"+ cSerie + "' AND SF3.F3_CFO >= '"+Alltrim(MV_PAR09)+"' AND SF3.F3_CFO <= '"+Alltrim(MV_PAR10)+"'  "
				Else
					cSerie := Padr(MV_PAR03,TamSx3("F3_SERIE")[1])
					cWhere += " AND SF3.F3_SERIE = '"+ cSerie + "'  AND SF3.F3_CFO >= '"+Alltrim(MV_PAR09)+"' AND SF3.F3_CFO <= '"+Alltrim(MV_PAR10)+"' "
				EndIf

			Else
				If lSdoc
					//cCampos += ", SF3.F3_SDOC"
					cSerie := Padr(MV_PAR03,TamSx3("F3_SDOC")[1])
					cWhere += " AND SF3.F3_SDOC = '"+ cSerie + "'"
				Else
					cSerie := Padr(MV_PAR03,TamSx3("F3_SERIE")[1])
					cWhere += "  AND SF3.F3_SERIE = '"+ cSerie + "'"
				EndIf
			EndIf

			If !Empty(MV_PAR11) .AND.  Empty(MV_PAR09)

				If lSdoc
					//cCampos += ", SF3.F3_SDOC"
					cSerie := Padr(MV_PAR03,TamSx3("F3_SDOC")[1])
					cWhere += " AND SF3.F3_SDOC = '"+ cSerie + "'  AND SF3.F3_XCODREG IN ("+cTipos+") "
				Else
					cSerie := Padr(MV_PAR03,TamSx3("F3_SERIE")[1])
					cWhere += " AND SF3.F3_SERIE = '"+ cSerie + "'  AND SF3.F3_XCODREG IN ("+cTipos+") "
				EndIf


			EndIf
		EndIf


		If !empty(dDtIni) .or. !empty(dDtFim)
			cWhere += " AND (SF3.F3_EMISSAO >= '"+ SubStr(DTOS(dDtIni),1,4) + SubStr(DTOS(dDtIni),5,2) + SubStr(DTOS(dDtIni),7,2) + "' AND SF3.F3_EMISSAO <= '"+ SubStr(DTOS(dDtFim),1,4) + SubStr(DTOS(dDtFim),5,2) + SubStr(DTOS(dDtFim),7,2) + "')"
		EndIf

		cWhere += " %"
		cAliasQry := getNextAlias()

		BeginSql Alias cAliasQry
        SELECT MIN(SF3.F3_NFISCAL) NOTAINI, MAX(SF3.F3_NFISCAL) NOTAFIM
        FROM
            %Table:SF3% SF3
        WHERE
            SF3.F3_FILIAL = %xFilial:SF3% AND
            SF3.F3_NFISCAL >= %Exp:cNotaIni% AND
            SF3.F3_NFISCAL <= %Exp:cNotaFim% AND
            SF3.F3_DTCANC = %Exp:Space(8)% AND
            %Exp:cWhere% AND
            SF3.D_E_L_E_T_ = ' '
		EndSql

		(cAliasQry)->(dbGoTop())
		cNotaIni := (cAliasQry)->NOTAINI
		cNotaFim := (cAliasQry)->NOTAFIM
		(cAliasQry)->(dbCloseArea())

		If val(cNotaIni) == 0 .or. val(cNotaFim) == 0
			Help(NIL, NIL, STR0008, NIL, STR0006, 1, 0, NIL, NIL, NIL, NIL, NIL, {STR0007}) //Documentos n�o encontrados para impress�o do DANFE., Informe um intervalo v�lido de notas., Informe um intervalo v�lido de notas.
			break
		EndIf

		If nTipImp == 2
			oPrinter := FWMSPrinter():New("DANFE_ETIQUETA_" + cIdEnt + "_" + Dtos(MSDate())+StrTran(Time(),":",""),,.F.,,.T.,,,,,.F.)
			oSetup := FWPrintSetup():New(PD_ISTOTVSPRINTER + PD_DISABLEORIENTATION + PD_DISABLEPAPERSIZE + PD_DISABLEPREVIEW + PD_DISABLEMARGIN,"DANFE SIMPLIFICADA")
			oSetup:SetPropert(PD_PRINTTYPE , 2) //Spool
			oSetup:SetPropert(PD_ORIENTATION , 2)
			oSetup:SetPropert(PD_DESTINATION , 1)
			oSetup:SetPropert(PD_MARGIN , {0,0,0,0})
			oSetup:SetPropert(PD_PAPERSIZE , 2)
			If !oSetup:Activate() == PD_OK
				break
			EndIf

			lMv_Logod  := If(GetNewPar("MV_LOGOD", "N" ) == "S", .T., .F.   )
			If lMv_Logod
				cGrpCompany	:= alltrim(FWGrpCompany())
				cCodEmpGrp	:= alltrim(FWCodEmp())
				cUnitGrp	:= alltrim(FWUnitBusiness())
				cFilGrp		:= alltrim(FWFilial())

				If !empty(cUnitGrp)
					cDescLogo := cGrpCompany + cCodEmpGrp + cUnitGrp + cFilGrp
				Else
					cDescLogo := cEmpAnt + cFilAnt
				EndIf

				cLogoD := GetSrvProfString("Startpath","") + "DANFE" + cDescLogo + ".BMP"
				If !file(cLogoD)
					cLogoD	:= GetSrvProfString("Startpath","") + "DANFE" + cEmpAnt + ".BMP"
					If !file(cLogoD)
						lMv_Logod := .F.
					EndIf
				EndIf
			EndIf
			If lMv_Logod
				cLogo := cLogoD
			Else
				cLogo := FisxLogo("1")
			EndIf
			oFontTit := TFont():New( "Arial", , -8, .T.) //Fonte para os titulos
			oFontTit:Bold := .T.						 //Setado negrito
			oFontInf := TFont():New( "Arial", , -8, .T.) //Fonte para as informa��es
			oFontInf:Bold := .F.						 //Setado negrito := .F.
			//oSetup:GetOrientation() // Retorna a orienta��o (Retrato ou Paisagem) do objeto.
			oPrinter:SetLandscape()		//Define a orientacao como paisagem
			oPrinter:setPaperSize(9) 	//Define tipo papel A4
			If oSetup:GetProperty(PD_PRINTTYPE) == IMP_PDF
				oPrinter:nDevice := IMP_PDF
				oPrinter:cPathPDF := If( empty(oSetup:aOptions[PD_VALUETYPE]), SuperGetMV('MV_RELT',,"\SPOOL\") , oSetup:aOptions[PD_VALUETYPE] )
			ElseIf oSetup:GetProperty(PD_PRINTTYPE) == IMP_SPOOL
				oPrinter:nDevice := IMP_SPOOL
				fwWriteProfString(GetPrinterSession(),"DEFAULT", oSetup:aOptions[PD_VALUETYPE], .T.)
				oPrinter:cPrinter := oSetup:aOptions[PD_VALUETYPE]
			EndIf

		EndIf

		aParam := {cSerie, cNotaIni, cNotaFim}
		If lUsaColab
			aNotas := colNfeMonProc( aParam, 1,,, @cAviso)
		Else
			aNotas := procMonitorDoc(cIdEnt, cUrl, aParam, 1,,, @cAviso)
		EndIf

		aEmit := array(4)
		aEmit[1] := SM0->M0_NOMECOM
		aEmit[2] := SM0->M0_CGC
		aEmit[3] := SM0->M0_INSC
		aEmit[4] := If(!GetNewPar("MV_SPEDEND",.F.),SM0->M0_ESTCOB,SM0->M0_ESTENT)

		SA1->(dbSetOrder(1))
		SA2->(dbSetOrder(1))
		SF3->(dbSetOrder(5))

		for nNotas := 1 to len(aNotas)

			cXml := aTail(aNotas[nNotas])[2]
			If empty(cXml)
				loop
			EndIf

			cProtocolo := aNotas[nNotas][4]
			cDpecProt := aTail(aNotas[nNotas])[3]
			fwFreeObj(oRetNF)
			fwFreeObj(oNfe)
			fwFreeObj(oTotal)

			oRetNF := XmlParser(cXml,"_",@cAviso,@cErro)
			If ValAtrib("oRetNF:_NFEPROC") <> "U"
				oNfe := WSAdvValue( oRetNF,"_NFEPROC","string",NIL,NIL,NIL,NIL,NIL)
			Else
				oNfe := oRetNF
			EndIf

			If ValAtrib("oNFe:_NFe:_InfNfe:_Total") == "U"
				loop
			Else
				oTotal := oNFe:_NFe:_InfNfe:_Total
			EndIf

			If (!empty(cProtocolo) .or. !empty(cDpecProt))

				If !SF3->(dbSeek(xFilial("SF3") + aNotas[nNotas][1]))
					loop
				EndIf

				cTotNota := alltrim(Transform(Val(oTotal:_ICMSTOT:_vNF:TEXT),"@e 9,999,999,999,999.99"))

				cHautNfe := aTail(aNotas[nNotas])[5]
				dDautNfe := If( !empty(aTail(aNotas[nNotas])[6]), aTail(aNotas[nNotas])[6], SToD("  /  /  ") )

				aSize(aNFe, 0)
				aSize(aDest, 0)

				aNFe := array(9)
				aNfe[1] := SF3->F3_CHVNFE
				aNfe[2] := cProtocolo
				aNfe[3] := cDpecProt
				aNfe[4] := cLogo
				aNfe[5] := If( SubStr(SF3->F3_CFO,1,1) >= '5', "1", "0" ) // 0 - Entrada / 1 - Sa�da
				aNfe[6] := SF3->F3_NFISCAL
				aNfe[7] := SF3->F3_SERIE
				aNfe[8] := SF3->F3_EMISSAO
				aNfe[9] := cTotNota

				aDest := array(4)
				If ( SubStr(SF3->F3_CFO,1,1) < '5' .and. SF3->F3_TIPO $ "DB") .or. (SubStr(SF3->F3_CFO,1,1) >= '5' .and. !SF3->F3_TIPO $ "DB")
					If SA1->(dbSeek(xFilial("SA1")+SF3->F3_CLIEFOR+SF3->F3_LOJA))
						cCgc := SA1->A1_CGC
						cNome := SA1->A1_NOME
						cInscr := SA1->A1_INSCR
						cUF := SA1->A1_EST
					EndIf
				Else // If ( SubStr(SF3->F3_CFO,1,1) < '5' .and. !SF3->F3_TIPO $ "DB") .or. (SubStr(SF3->F3_CFO,1,1) >= '5' .and. SF3->F3_TIPO $ "DB")
					If SA2->(dbSeek(xFilial("SA2")+SF3->F3_CLIEFOR+SF3->F3_LOJA))
						cCgc := SA2->A2_CGC
						cNome := SA2->A2_NOME
						cInscr := SA2->A2_INSCR
						cUF := SA2->A2_EST
					EndIf
				EndIf
				aDest[1] := cNome
				aDest[2] := cCgc
				aDest[3] := cInscr
				aDest[4] := cUF

				nContDanfe += 1
				If nTipImp == 1 // 1 - T�rmica
					GetInfAdc() // CUSTOMIZADO
					impZebra(aNfe, aEmit, aDest)

				ElseIf nTipImp == 2 // 2 - Normal

					If nContDanfe == 1
						oPrinter:StartPage() 		//Define inicio da pagina
						nLinha := 0
						nColuna := 0
					ElseIf nContDanfe == 2
						nLinha := 0
						nColuna := 250
					ElseIf nContDanfe == 3
						nLinha := 0
						nColuna := 500
					ElseIf nContDanfe == 4
						nLinha := 250
						nColuna := 0
					ElseIf nContDanfe == 5
						nLinha := 250
						nColuna := 250
					ElseIf nContDanfe == 6
						nLinha := 250
						nColuna := 500
						oPrinter:EndPage()
						nContDanfe := 0
					EndIf
					GetInfAdc() // CUSTOMIZADO
					DanfeSimp(oPrinter, nLinha, nColuna, oFontTit, oFontInf, aEmit, aNfe, aDest)

				EndIf
				lOk := .T.

			EndIf

		next

		If lOk
			If nTipImp == 1 // 1 - T�rmica
				MSCBCLOSEPRINTER()
			ElseIf nTipImp == 2 // 2 - Normal
				If nContDanfe <> 6
					oPrinter:EndPage()
				EndIf
				oPrinter:Print()
			EndIf
		EndIf

	end sequence

	fwFreeObj(oPrinter)
	fwFreeObj(oSetup)
	fwFreeObj(oFontTit)
	fwFreeObj(oFontInf)
	restArea(aAreaSA2)
	restArea(aAreaSA1)
	restArea(aAreaSF3)
	restArea(aAreaCB5)
	restArea(aArea)

return

static Function ValAtrib(atributo)
Return (type(atributo) )

//-------------------------------------------------------------------
/*/{Protheus.doc} DanfeSimp
Impress�o normal de danfe simplificada - Etiqueta 

/*/
//-------------------------------------------------------------------
static function DanfeSimp(oPrinter, nPosY, nPosX, oFontTit, oFontInf, aEmit, aNfe, aDest)
	local cTitDanfe		:= "DANFE SIMPLIFICADO - ETIQUETA"
	local cChAces		:= "CHAVE DE ACESSO: "
	local cTitProt		:= "PROTOCOLO DE AUTORIZACAO: "
	local cTitPrtEPEC	:= "PROTOCOLO DE AUTORIZACAO EPEC: "
	local cTitNome		:= "NOME/RAZAO SOCIAL: "
	local cCpf			:= "CPF: "
	local cCnpj 		:= "CNPJ: "
	local cIETxt		:= "IE: "
	local cUFTxt		:= "UF: "
	local cSerieTxt		:= "S�RIE: "
	local cNumTxt		:= "N�: "
	local cDtEmi		:= "DATA EMISS�O:"
	local cTipOp		:= "TIPO DE OPERA��O: "
	local cEntTxt		:= "0 - Entrada "
	local cSaiTxt		:= "1 - Saida"
	local cDestTxt		:= "DESTINATARIO: "
	local cValTotTxt	:= "VALOR TOTAL DA NOTA FISCAL: "
	local cValor 		:= "R$: "
	Local cDadosAdc     := "DADOS ADIC." // CUSTOMIZADO

	// Box Principal
	// box (eixo Y inicio, eixo X inicio, eixo y fim, eixo x fim)
	oPrinter:Box( 30 + nPosY, 30 + nPosX, 270 + nPosY, 270 + nPosX, "-6") // box (eixo Y inicio, eixo X inicio, eixo y fim, eixo x fim)

	// Box T�tulo
	oPrinter:Box( 30 + nPosY, 30 + nPosX, 50 + nPosY, 270 + nPosX, "-4")
	oPrinter:Say( 45 + nPosY, 80 + nPosX, cTitDanfe, oFontTit) //say (y,x)

	// Box Chave de acesso
	oPrinter:Box( 50 + nPosY, 30 + nPosX, 120 + nPosY, 270 + nPosX, "-4")
	oPrinter:Say( 65 + nPosY, 35 + nPosX, cChAces, oFontTit)
	oPrinter:Say( 110 + nPosY, 50 + nPosX, Transform(aNfe[1],"@R 9999 9999 9999 9999 9999 9999 9999 9999 9999 9999 9999"), oFontInf)
	oPrinter:Code128c( 100 + nPosY, 40 + nPosX, aNfe[1], 30)

	// Box Protocolo de Autorizacao
	oPrinter:Box( 120 + nPosY, 30 + nPosX, 135 + nPosY, 270 + nPosX, "-4")
	If !empty(aNfe[2])
		oPrinter:Say( 130 + nPosY, 50 + nPosX, cTitProt, oFontTit)
		oPrinter:Say( 130 + nPosY, 180 + nPosX, aNfe[2], oFontInf)
	Else
		oPrinter:Say( 130 + nPosY, 50 + nPosX, cTitPrtEPEC, oFontTit)
		oPrinter:Say( 130 + nPosY, 180 + nPosX, aNfe[3], oFontInf)
	EndIf

	//Remetente
	oPrinter:SayBitmap( 140 + nPosY, 35 + nPosX, aNfe[4], 25, 25)
	oPrinter:Say( 145 + nPosY, 80 + nPosX, cTitNome, oFontTit)
	If len(aEmit[1]) > 21
		oPrinter:Say( 145 + nPosY, 175 + nPosX, SubStr(aEmit[1],1,20), oFontInf)
		oPrinter:Say( 153 + nPosY, 80 + nPosX, SubStr(aEmit[1],21,59), oFontInf)
	Else
		oPrinter:Say( 145 + nPosY, 175 + nPosX, aEmit[1], oFontInf)
	EndIf
	oPrinter:Say( 161 + nPosY, 80 + nPosX, cIETxt, oFontTit)
	oPrinter:Say( 161 + nPosY, 90 + nPosX, aEmit[3], oFontInf)
	If len(aEmit[2]) == 11
		oPrinter:Say( 161 + nPosY, 175 + nPosX, cCpf, oFontTit)
		oPrinter:Say( 161 + nPosY, 200 + nPosX, Transform(aEmit[2],"@R 999.999.999-99"), oFontInf)
	Else
		oPrinter:Say( 161 + nPosY, 175 + nPosX, cCnpj, oFontTit)
		oPrinter:Say( 161 + nPosY, 200 + nPosX, Transform(aEmit[2],"@R 99.999.999/9999-99"), oFontInf)
	EndIf
	oPrinter:Say( 169 + nPosY, 80 + nPosX, cUFTxt, oFontTit)
	oPrinter:Say( 169 + nPosY, 100 + nPosX, aEmit[4], oFontInf)

	// Box serie
	oPrinter:Box( 175 + nPosY, 30 + nPosX, 210 + nPosY, 100 + nPosX, "-4")
	oPrinter:Say( 185 + nPosY, 38 + nPosX, cSerieTxt, oFontTit)
	oPrinter:Say( 185 + nPosY, 65 + nPosX, aNfe[7], oFontInf)
	oPrinter:Say( 193 + nPosY, 38 + nPosX, cNumTxt, oFontTit)
	oPrinter:Say( 193 + nPosY, 53 + nPosX, aNfe[6], oFontInf)

	// Box Data Emissao
	oPrinter:Box( 175 + nPosY, 100 + nPosX, 210 + nPosY, 180 + nPosX, "-4")
	oPrinter:Say( 185 + nPosY, 110 + nPosX, cDtEmi, oFontTit)
	oPrinter:Say( 193 + nPosY, 120 + nPosX, dtoc(aNfe[8]), oFontInf)

	// Box Tipo da operacao
	oPrinter:Box( 175 + nPosY, 175 + nPosX, 210 + nPosY, 270 + nPosX, "-4")
	oPrinter:Say( 185 + nPosY, 180 + nPosX, cTipOp, oFontTit)
	oPrinter:Say( 185 + nPosY, 262 + nPosX, aNfe[5], oFontInf)
	oPrinter:Say( 193 + nPosY, 200 + nPosX, cEntTxt, oFontInf)
	oPrinter:Say( 201 + nPosY, 200 + nPosX, cSaiTxt, oFontInf)

	// Destinatario
	oPrinter:Box( 210 + nPosY, 30 + nPosX, 220 + nPosY, 270 + nPosX, "-4")
	oPrinter:Say( 218 + nPosY, 120 + nPosX, cDestTxt, oFontTit)
	oPrinter:Say( 228 + nPosY, 35 + nPosX, cTitNome, oFontTit)
	If len(aDest[1]) > 21
		oPrinter:Say( 228 + nPosY, 130 + nPosX, SubStr(aDest[1], 1, 30), oFontInf)
		oPrinter:Say( 236 + nPosY, 35 + nPosX, SubStr(aDest[1], 31, 49), oFontInf)
	Else
		oPrinter:Say( 228 + nPosY, 130 + nPosX, aDest[1], oFontInf)
	EndIf
	oPrinter:Say( 244 + nPosY, 35 + nPosX, cIETxt, oFontTit)
	oPrinter:Say( 244 + nPosY, 45 + nPosX, aDest[3], oFontInf)
	If len(aEmit[2]) == 11
		oPrinter:Say( 244 + nPosY, 175 + nPosX, cCpf, oFontTit)
		oPrinter:Say( 244 + nPosY, 200 + nPosX, Transform(aDest[2], "@R 999.999.999-99"), oFontInf)
	Else
		oPrinter:Say( 244 + nPosY, 175 + nPosX, cCnpj, oFontTit)
		oPrinter:Say( 244 + nPosY, 200 + nPosX, Transform(aDest[2], "@R 99.999.999/9999-99"), oFontInf)
	EndIf
	oPrinter:Say( 252 + nPosY, 35 + nPosX, cUFTxt, oFontTit)
	oPrinter:Say( 252 + nPosY, 55 + nPosX, aDest[4], oFontInf)

	// Box Valor Total
	oPrinter:Box( 255 + nPosY, 30 + nPosX, 270 + nPosY, 270 + nPosX, "-4") // box (inicio, Margem esquerda, fim, largura)
	oPrinter:Say( 265 + nPosY, 35 + nPosX, cValTotTxt , oFontTit)
	oPrinter:Say( 265 + nPosY, 190 + nPosX, cValor + aNfe[9], oFontInf)


	// Box Dados Adicionais CUSTOMIZADO INICIO
	oPrinter:Box( 266 + nPosY, 30 + nPosX, 300 + nPosY, 270 + nPosX, "-4") // box (inicio, Margem esquerda, fim, largura)
	oPrinter:Say( 278 + nPosY, 35 + nPosX, cDadosAdc , oFontTit)
	oPrinter:Say( 286 + nPosY, 35 + nPosX, "PEDIDO: "+ aInfAd[1] +" - "+ aInfAd[2], oFontInf)
	oPrinter:Say( 294 + nPosY, 35 + nPosX, "REDESPACHO: "+ aInfAd[3] +" - "+ aInfAd[4], oFontInf)
	oPrinter:Say( 294 + nPosY, 35 + nPosX, "REDESPACHO: "+ aInfAd[3] +" - "+ aInfAd[4], oFontInf)

return

//-------------------------------------------------------------------
/*/{Protheus.doc} impZebra
Impress�o de danfe simplificada - Etiqueta para impressora Zebra

@param		aNfe		Dados da Nota
            aEmit		Dados do Emitente da Nota
			aDest		Dados do Destinat�rio da Nota

/*/ 
//-------------------------------------------------------------------
static function impZebra(aNFe, aEmit, aDest)

	local cFontMaior := "016,013" //Fonte maior - t�tulos dos campos obrigat�rios do DANFE ("altura da fonte, largura da fonte")
	local cFontMenor := "015,008" //Fonte menor - campos vari�veis do DANFE ("altura da fonte, largura da fonte")
	local cFontMedia := "016,010" //Fonte menor - campos vari�veis do DANFE ("altura da fonte, largura da fonte")


	local lProtEPEC  := .F.
	local lNomeEmit  := .F.
	local lNomeDest  := .F.

	local nNome      := 1
	local nCNPJ      := 2
	local nIE        := 3
	local nUF        := 4
	local nChave     := 1
	local nProtocolo := 2
	local nProt_EPEC := 3
	local nOperacao  := 5
	local nNumero    := 6
	local nSerie     := 7
	local nData      := 8
	local nValor     := 9
	local nTamEmit   := len( allTrim( aEmit[nNome] ) ) //Quantidade de caracteres da raz�o social do emitente
	local nTamDest   := len( allTrim( aDest[nNome] ) ) //Quantidade de caracteres da raz�o social do destinat�rio
	local nMaxNome   := 34 //Quantidade de caracteres m�xima da primeira linha da raz�o social

	Default aNFe     := {}
	Default aEmit    := {}
	Default aDest    := {}

//Inicializa a impress�o
	MSCBBegin(1,6)

//Cria��o do Box // INICIO CUSTOMIZADO INICIO DA IMPRESS�O NA POSI��O 03
	MSCBBox(03,02,98,148)

//Cria��o das linhas Horizontais - sentido: de cima para baixo
	MSCBLineH(03, 012, 98)
	MSCBLineH(03, 047, 98)
	MSCBLineH(03, 057, 98)
	MSCBLineH(03, 084, 98)
	MSCBLineH(40, 101, 98)
	MSCBLineH(03, 101, 98)
	MSCBLineH(03, 111, 98)
	MSCBLineH(03, 138, 98)
// FIM CUSTOMIZADO
//Cria��o das linhas verticais - sentido: da direita para esquerda
	MSCBLineV(32, 84, 101)
	MSCBLineV(64, 84, 101)

//Imprime o c�digo de barras
	MSCBSayBar(14, 24, aNFe[nChave], "N", "C", 10, .F., .F., .F., "C", 2, 1, .F., .F., "1", .T.)

//INICIO CUSTOMIZADO CRIA��O DE If'S PARA RECEBER RETORNO DA VARI�VEL CORRETAMENTE
	If !Empty( aNFe[nProt_EPEC] ) //Se utilizado evento EPEC para emiss�o da Nota lProtEPEC = .T.
		lProtEPEC  := .T.
	Else
		lProtEPEC  := .F.
	EndIf
	If len( Alltrim(aEmit[nCNPJ]) ) == 14 //Se emitente pessoa jur�dica lEmitJurid = .T.
		lEmitJurid := .T.
	Else
		lEmitJurid := .F.
	EndIf

 	If len( Alltrim(aDest[nCNPJ]) ) == 14 //Se destinat�rio pessoa jur�dica lDestJurid = .T.
		lDestJurid := .T.
	Else
		lDestJurid := .F.
	EndIf
//FIM CUSTOMIZADO CRIA��O DE If'S PARA RECEBER RETORNO DA VARI�VEL CORRETAMENTE

//Cria��o dos campos de textos fixos da etiqueta
	MSCBSay(17.5, 06.25, "DANFE SIMPLIFICADO - ETIQUETA", "N", "A", cFontMaior)
	MSCBSay(04  , 15   , "CHAVE DE ACESSO:"             , "N", "A", cFontMaior)

	If !lProtEPEC
		MSCBSay(22.5, 48.75, "PROTOCOLO DE AUTORIZACAO:"     , "N", "A", cFontMaior)
	Else
		MSCBSay(16.5, 48.75, "PROTOCOLO DE AUTORIZACAO EPEC:", "N", "A", cFontMaior)
	EndIf

	MSCBSay(04, 60, "NOME/RAZAO SOCIAL:", "N", "A", cFontMaior)

	If lEmitJurid
		MSCBSay(04, 66.25 , "CNPJ:", "N", "A", cFontMaior)
	Else
		MSCBSay(04, 66.25 , "CPF:", "N", "A", cFontMaior)
	EndIf

	MSCBSay(04  , 70    , "IE:"               , "N", "A", cFontMaior)
	MSCBSay(04  , 73.75 , "UF:"               , "N", "A", cFontMaior)
	MSCBSay(04  , 88.75 , "SERIE:"            , "N", "A", cFontMaior)
	MSCBSay(04  , 93.75 , "N_A7:"             , "N", "A", cFontMaior)
	MSCBSay(34  , 88.75 , "DATA EMISSAO:"     , "N", "A", cFontMaior)
	MSCBSay(65.5, 88.75 , "TIPO OPER.:"       , "N", "A", cFontMaior)
	MSCBSay(65.5, 92.5  , "0 - ENTRADA"       , "N", "A", cFontMenor)
	MSCBSay(65.5, 96.25 , "1 - SAIDA"         , "N", "A", cFontMenor)
	MSCBSay(35  , 105.5 , "DESTINATARIO"      , "N", "A", cFontMaior)
	MSCBSay(04  , 113.75, "NOME/RAZAO SOCIAL:", "N", "A", cFontMaior)

	If lDestJurid
		MSCBSay(04, 120, "CNPJ:", "N", "A", cFontMaior)
	Else
		MSCBSay(04, 120, "CPF:" , "N", "A", cFontMaior)
	EndIf

	MSCBSay(04  , 123.75, "IE:"         , "N", "A", cFontMaior)
	MSCBSay(04  , 127.5 , "UF:"         , "N", "A", cFontMaior)
	// INICIO CUSTOMIZADO
	MSCBSay(04  , 131.5	, "PEDIDO: "	, "N", "A",	cFontMaior)
	MSCBSay(04  , 135.5	, "REDESPACHO: ", "N", "A",	cFontMaior)
	// FIM CUSTOMIZADO
	MSCBSay(04  , 142.5 , "VALOR TOTAL:", "N", "A", cFontMaior)
	MSCBSay(62.5, 142.5 , "R$"          , "N", "A", cFontMaior)

	lNomeEmit := nTamEmit > nMaxNome //Se quantidade de caracteres da raz�o social do emitente for maior que o permitido para a primeira linha lNomeEmit := T
	lNomeDest := nTamDest > nMaxNome //Se quantidade de caracteres da raz�o social do destinat�rio for maior que o permitido para a primeira linha lNomeDest := T

//Cria��o dos campos de textos vari�veis da etiqueta
	MSCBSay(09, 39, transform( aNFe[nChave], "@R 9999 9999 9999 9999 9999 9999 9999 9999 9999 9999 9999" ), "N", "A", cFontMenor)

	If !lProtEPEC
		MSCBSay(38.75, 53.75, aNFe[nProtocolo], "N", "A", cFontMenor)
	Else
		MSCBSay(38.75, 53.75, aNFe[nProt_EPEC], "N", "A", cFontMenor)
	EndIf

	If lNomeEmit
		MSCBSay(44, 60, allTrim( subStr( aEmit[nNome], 1, nMaxNome ) ), "N", "A", cFontMenor)
		MSCBSay(27.5, 62.5, allTrim( subStr( aEmit[nNome], nMaxNome + 1, nTamEmit ) ), "N", "A", cFontMenor)
	Else
		MSCBSay(44, 60, allTrim( aEmit[nNome] ), "N", "A", cFontMenor)
	EndIf

	If lEmitJurid
		MSCBSay(15, 66.25, transform( aEmit[nCNPJ], "@R 99.999.999/9999-99" ), "N", "A", cFontMenor) //Emitente pessoa jur�dica
	Else
		MSCBSay(15, 66.25, transform( aEmit[nCNPJ], "@R 999.999.999-99" ), "N", "A", cFontMenor) //Emitente pessoa f�sica
	EndIf

	MSCBSay(11, 70,    aEmit[nIE], "N", "A", cFontMenor)
	MSCBSay(11, 73.75, aEmit[nUF], "N", "A", cFontMenor)
	MSCBSay(18, 88.75, aNFe[nSerie], "N", "A", cFontMenor)
	MSCBSay(11, 93.75, aNFe[nNumero], "N", "A", cFontMenor)
	MSCBSay(40, 93.75, ajustaData( aNFe[nData] ) , "N", "A", cFontMenor)
	MSCBSay(93, 88.75, aNFe[nOperacao], "N", "A", cFontMenor)

	If lNomeDest
		MSCBSay(44, 113.75, allTrim( subStr( aDest[nNome], 1, nMaxNome ) ), "N", "A", cFontMenor)
		MSCBSay(04, 116.25, allTrim( subStr( aDest[nNome], nMaxNome + 1, nTamDest ) ), "N", "A", cFontMenor)
	Else
		MSCBSay(44, 113.75, allTrim( aDest[nNome] ), "N", "A", cFontMenor)
	EndIf

	If lDestJurid
		MSCBSay(15, 120, transform( aDest[nCNPJ], "@R 99.999.999/9999-99" ), "N", "A", cFontMenor) //Destinat�rio pessoa jur�dica
	Else
		MSCBSay(15, 120, transform( aDest[nCNPJ], "@R 999.999.999-99" ), "N", "A", cFontMenor) //Destinat�rio pessoa f�sica
	EndIf

	MSCBSay(11, 123.75, aDest[nIE]  , "N", "A", cFontMenor)
	MSCBSay(11, 127.5 , aDest[nUF]  , "N", "A", cFontMenor)
// INICIO CUSTOMIZADO
	//MSCBSay(20, 131.5,  aInfAd[1] +" - "+ aInfAd[2],"N", "A",cFontMedia )
	MSCBSay(20, 131.5,  aInfAd[1] ,"N", "A",cFontMaior )
	MSCBSay(34, 131.5,  " - "+ aInfAd[2],"N", "A",cFontMenor )
	MSCBSay(29, 135.5,  aInfAd[3] +"- "+ aInfAd[4],"N", "A",cFontMenor )
	MSCBSay(70, 142.5 , aNFe[nValor], "N", "A", cFontMenor)
	// FIM CUSTOMIZADO
//Finaliza a impress�o
	MSCBEND()

return

//-------------------------------------------------------------------
/*/{Protheus.doc} ajustaData
Recebe um dado do tipo data (AAAAMMDD) e devolve uma string no
formato (DD/MM/AAAA)

@param		dData		Dado do tipo data(AAAAMMDD)
@return     cDataForm	String formatada com a data (DD/MM/AAAA)

/*/
//-------------------------------------------------------------------
static function ajustaData( dData )

	local cDia      := ""
	local cMes      := ""
	local cAno      := ""
	local cDataForm := ""

	default dData   := Date()

	cDia := strZero( day( dData ), 2 )
	cMes := strZero( month( dData ), 2 )
	cAno := allTrim( str( year( dData ) ) )

	cDataForm = cDia + "/" + cMes + "/" + cAno

return cDataForm
// INICIO CUSTOMIZADO
/*/{Protheus.doc} GetInfAdc
    (long_description)
    @type  Static Function
    @author user
    @since 06/07/2021
    @version version
    @param param_name, param_type, param_descr
    @return return_var, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
    /*/
Static Function GetInfAdc()

	Local cQuery := ""
	Local cQryC6 := ""
	Local cQryA4 := ""


	cQryC6 += "SELECT TOP 1 * FROM "+RetSqlName("SC6")+" (NOLOCK) WHERE C6_FILIAL+C6_NOTA+C6_SERIE = '"+SF3->F3_FILIAL+SF3->F3_NFISCAL+SF3->F3_SERIE+"' AND D_E_L_E_T_ = '' "

	If Select("TMPSC6") > 0
		TMPSC6->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQryC6),"TMPSC6",.T.,.T.)

	If TMPSC6->(!Eof())

		cQuery += "SELECT ISNULL(CONVERT(VARCHAR(2047), CONVERT(VARBINARY(2047), C5_XMENNT2)),'') AS OBSC5, * FROM "+RetSqlName("SC5")+" "
		cQuery += " (NOLOCK) WHERE C5_NUM = '"+TMPSC6->C6_NUM+"' AND C5_FILIAL = '"+TMPSC6->C6_FILIAL+"' AND D_E_L_E_T_ = '' "
		If Select("TMPSC5") > 0
			TMPSC5->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSC5",.T.,.T.)
		If TMPSC5->(!Eof())

			aInfAd := array(9)
			aInfAd[1] := TMPSC5->C5_NUM
			aInfAd[2] := Alltrim(TMPSC5->OBSC5)
			aInfAd[3] := TMPSC5->C5_REDESP

			cQryA4+= "SELECT * FROM "+RetSqlName("SA4")+" (NOLOCK) WHERE A4_COD = '"+Alltrim(TMPSC5->C5_REDESP)+"'"
			If Select("TMPSA4") > 0
				TMPSA4->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQryA4),"TMPSA4",.T.,.T.)
			aInfAd[4] := Alltrim(TMPSA4->A4_NREDUZ)
		EndIf
	EndIf
Return
// FIM CUSTOMIZADO
