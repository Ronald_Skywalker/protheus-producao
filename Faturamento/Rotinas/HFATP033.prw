#include 'protheus.ch'
#include 'parmtype.ch'

//--------------------------------------------------------------
/*/{Protheus.doc} HFATP033
Description
Rotina para liberar e bloquear produtos no processo de DAP.
@param xParam Parameter Description
@return xRet Return Description
@author Ronaldo Pereira -
@since 12/11/2020
/*/
//--------------------------------------------------------------
User Function HFATP033()
Local oButton1
Local oButton2
Local oButton3
Local oSay1
Local oSay2
Local oSay3
Local oSay4
Local oSay5
Local oGet2
Local oGet3
Local oGet4

private oGet1
private cGetPrd1 	:= SPACE(8)
private cGetPrd2 	:= SPACE(8)
private cGetCor 	:= SPACE(3)
private cGetTam		:= SPACE(4)
private nComboBo1	:= "LIBERAR"
private lCheckBo1 	:= .F.

Static oDlg

    DEFINE MSDIALOG oDlg TITLE "DAP - Liberar e Bloquear Produtos" FROM 000, 000  TO 550, 600 COLORS 0, 16777215 PIXEL

    @ 014, 005 SAY oSay1 PROMPT "Refer�ncia de:" SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 012, 043 MSGET oGet1 VAR cGetPrd1 SIZE 035, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 014, 082 SAY oSay3 PROMPT "At�:" SIZE 011, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 012, 093 MSGET oGet2 VAR cGetPrd2 SIZE 035, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 014, 132 SAY oSay4 PROMPT "COR" SIZE 015, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 012, 148 MSGET oGet3 VAR cGetCor SIZE 022, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 014, 174 SAY oSay5 PROMPT "TAM" SIZE 015, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 012, 190 MSGET oGet4 VAR cGetTam SIZE 022, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 014, 219 CHECKBOX oCheckBo1 VAR lCheckBo1 PROMPT "Bloqueado" SIZE 036, 008 OF oDlg COLORS 0, 16777215 PIXEL
    @ 011, 258 BUTTON oButton1 PROMPT "Consultar" SIZE 035, 012 OF oDlg ACTION Progresso() PIXEL
    fMSNewGe1()
    @ 260, 008 SAY oSay2 PROMPT "DAP:" SIZE 015, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 258, 026 MSCOMBOBOX oComboBo1 VAR nComboBo1 ITEMS {"LIBERAR","BLOQUEAR"} SIZE 054, 010 OF oDlg COLORS 0, 16777215 PIXEL
    //@ 260, 130 SAY "Total de Produtos:" SIZE 060, 007 OF oDlg COLORS 0, 16777215 PIXEL    
    @ 257, 084 BUTTON oButton2 PROMPT "Salvar" SIZE 035, 012 OF oDlg ACTION Salvar() PIXEL
    @ 258, 258 BUTTON oButton3 PROMPT "Sair" SIZE 035, 012 OF oDlg ACTION (oDlg:End()) PIXEL
    
  ACTIVATE MSDIALOG oDlg CENTERED
  
Return

Static Function Progresso()
		Processa( {|| fMSNewGe1() }, "Aguarde...", "Processando...",.F.)
Return

//------------------------------------------------ 
Static Function fMSNewGe1()
//------------------------------------------------ 
Local nX
Local aHeaderEx := {}

Local aFieldFill := {}
Local aFields 	:= {"B1_XLIBDAP","B1_COD","B1_COD"    ,"B1_COD","B1_COD","B1_YDESSCO"}
Local aFields2 	:= {"DAP"       ,"SKU"   ,"REFERENCIA","COR"   ,"TAM"   ,"SUBCOLECAO"}
Local aAlterFields := {}
Static oMSNewGe1

  	aColsEx := Consulta()
  
  // Define field properties
  DbSelectArea("SX3")
  SX3->(DbSetOrder(2))
  For nX := 1 to Len(aFields)
    If SX3->(DbSeek(aFields[nX]))
      Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
                       SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
    Endif
  Next nX

  // Define field values
  For nX := 1 to Len(aFields)
    If DbSeek(aFields[nX])
      Aadd(aFieldFill,"")
    Endif
  Next nX
  Aadd(aFieldFill, .F.)
  Aadd(aColsEx, aFieldFill)

  oMSNewGe1 := MsNewGetDados():New( 031, 003, 253, 297, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
  
return


Static Function Consulta()
local cQry      := ""
Local aColsEx2  := {}
local cProd1    := alltrim(cGetPrd1)
local cProd2    := alltrim(cGetPrd2)
local cCor		:= alltrim(cGetCor)			
local cTam		:= alltrim(cGetTam)
				
	If !Empty(cProd1) .OR. lCheckBo1 = .T. 
		
		cQry := " SELECT * FROM ( "
	    cQry += " SELECT B1_XLIBDAP AS DAP, " 
		cQry += "    B1_COD AS SKU, " 
	    cQry += "    LEFT(B1_COD,8) AS REFERENCIA, " 
		cQry += "    SUBSTRING(B1_COD,9,3) AS COR, "
		cQry += "    CASE WHEN RIGHT(B1_COD,4) LIKE '000%' THEN REPLACE(RIGHT(B1_COD,4),'000','') "
		cQry += " 		WHEN RIGHT(B1_COD,4) LIKE '00%' THEN REPLACE(RIGHT(B1_COD,4),'00','') "
		cQry += " 		WHEN RIGHT(B1_COD,4) LIKE '0%' THEN SUBSTRING(RIGHT(B1_COD,4),2,4) "
	    cQry += "    ELSE REPLACE(RIGHT(B1_COD,4),'00','') END TAMANHO, "	   
		cQry += "    B1_YDESSCO AS SUBCOLECAO "
	    cQry += " FROM "+RetSqlName("SB1")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND B1_TIPO IN ('PF','KT','PI','ME') "
	    
	    If !Empty(cProd1)
	    	cQry += " AND LEFT(B1_COD,8) BETWEEN '"+cProd1+"' AND '"+cProd2+"' "
	    EndIf
	    
	    If !Empty(cCor)
	    	cQry += " AND SUBSTRING(B1_COD,9,3) = '"+cCor+"' "
	    EndIf
	    
	    If lCheckBo1 = .T.
	    	cQry += " AND B1_XLIBDAP = 'N' "
	    EndIf
	    
	    	cQry += " ) T "
	    
	    If !Empty(cTam)
	    	cQry += " WHERE T.TAMANHO = '"+cTam+"' "
	    EndIf 
	    	
	    If Select("TMPRD") > 0
	        TMPRD->(DbCloseArea())
	    EndIf
	
	    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPRD",.T.,.T.)
	    DbSelectArea("TMPRD")
	    DbGoTop()
	
	    While TMPRD->(!EOF()) 
	        aAdd(aColsEx2,{TMPRD->DAP,TMPRD->SKU,TMPRD->REFERENCIA,TMPRD->COR,TMPRD->TAMANHO,TMPRD->SUBCOLECAO,.F.})
	        TMPRD->(DbSkip())
	    EndDo	    
	    
	EndIf      
    
return (aColsEx2)


Static Function Salvar()
Local nX
Local aColSKU := {}

    aColSKU := Consulta()
        
	If !Empty(aColSKU)
		nRet3 := MessageBox("Confirmar atualiza��o?","Confirma��o",4)
	    If  nRet3 == 6     	
	     	DbselectArea("SB1")
	    	DbSetOrder(1)
	    	For nX := 1 to Len(aColSKU)
	    		If DbSeek(xfilial("SB1")+aColSKU[nX][2])	    			
	    			RECLOCK("SB1",.F.)
		    		If nComboBo1 = "LIBERAR"
		    			SB1->B1_XLIBDAP := 'S' 
		    		ElseIf nComboBo1 = "BLOQUEAR"
		    			SB1->B1_XLIBDAP := 'N'
		    		EndIf
		    		MSUNLOCK()				
	    		Endif
	    	Next nX		
				MessageBox("Atualiza��o realizada com Sucesso!","Aviso",0)
				
				oDlg:refresh()
				oGet1:setfocus()
			    fMSNewGe1()
		Else
	        MessageBox("Cancelado pelo usu�rio!","Aten��o",16)
	        oGet1:setfocus()
	    EndIf
	Else
	   MessageBox("Consulte um Produto!","Aten��o",16)
	   oGet1:setfocus()
	EndIf    

Return