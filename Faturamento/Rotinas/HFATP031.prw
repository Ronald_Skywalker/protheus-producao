#Include "Protheus.ch"
#include "TOTVS.CH"
#INCLUDE "topconn.ch"
#include "rwmake.ch"

#define DS_MODALFRAME   128

// Ajustes para o novo tratamento de comissoes - TOTALIT 10/2019
User Function HFATP031()

	Local oButton1
	Local oButton2
	Local oRadMenu1
	Local nRadMenu1 := 1
	Static oDlg

	DEFINE MSDIALOG oDlg TITLE "Gera��o de Debito de Comiss�o" FROM 000, 000  TO 150, 400 COLORS 0, 16777215 PIXEL

	@ 013, 007 RADIO oRadMenu1 VAR nRadMenu1 ITEMS "GERA COMISS�O NEGATIVA SOBRE CLIENTES INADIMPLENTES","GERA COMISS�O NEGATIVA SOBRE NOTAS DE MOSTRU�RIOS" SIZE 185, 033 OF oDlg COLOR 0, 16777215 PIXEL
	@ 054, 109 BUTTON oButton2 PROMPT "Confirma" 	SIZE 037, 012 ACTION (xopc := 1,oDlg:End()) OF oDlg PIXEL
	@ 054, 154 BUTTON oButton1 PROMPT "Cancela" 	SIZE 037, 012 ACTION (xopc := 2,oDlg:End()) OF oDlg PIXEL

	ACTIVATE MSDIALOG oDlg CENTERED

	If xopc == 1

		If nRadMenu1 == 1       //GERA COMISS�O NEGATIVA SOBRE CLIENTES INADIMPLENTES.
			oProcesso := MsNewProcess():New({|lEnd| HFATP31A() },"Lendo Arquivo(s)...")
			oProcesso:Activate()
		ElseIf nRadMenu1 == 2   //GERA COMISS�O NEGATIVA SOBRE NOTAS DE MOSTRU�RIOS.
			HFATP31B()
		End

	End

Return nil

Static Function HFATP31A()

	Private cPergA		:= "HFATP31A"
	Private dDtBase		:= ""

	VldPergA()
	Pergunte(cPergA,.T.)
	dDtBase		:= 	MV_PAR01

	oProcesso:SetRegua1( 2 )
	aNegComiss	:= HFATP31A1(dDtBase) //Gera Arrey com itens do SQL

/*	If Len(aNegComiss) > 0
		oProcesso:IncRegua1("Gravado Informacoes: ")
		lRet	:= HFATP31A2(@aNegComiss)
		If lRet > 0
			MsgSTop("Foram gerados "+alltrim(str(lRet))+" registros negativos na data "+dToC(dDtBase),"Info")
		Else
			MsgSTop("N�o h� registros a para gera��o de extornos de Comiss�o !","Info")
		EndIf
	Else
		MsgSTop("N�o h� registros a para gera��o de extornos de Comiss�o !","Info")
	EndIf
*/
Return Nil

Static Function HFATP31A1(dDtBase)
Local ni
Local cQuery		:= ""
Local cDatabase		:= DaySub(dDtBase, Val(GetMV("MV_HDCOMSN")))
Local cDtLimite		:= DaySub(cDatabase, Val(GetMV("MV_HDCOMSN")))
Local cEOL			:= chr(13) + chr(10)
Local aRegs			:= {}
Local aNegComiss	:= {}
Local cNewAlias  := GetNextAlias()
Local nTotNota   := 0
Local aPercMarca := {}
Local aValor     := {}
Local nValBase   := 0
Local cChave	 := ""
Local i	:=  0
local dEmissao   := dDtBase

sa3->(dBSetOrder(1)) // A3_FILIAL+A3_COD

oProcesso:IncRegua1("Verificando Registros :") // Atualiza a barra de processamento

For i := 1 to 2
	If i = 1
		cQuery := "SELECT COUNT(1) as nTotal "
	Else
		cQuery := "SELECT E3_VEND,E3_XMARCA,E3_PREFIXO,E3_NUM,E3_PARCELA,E3_TIPO,E3_CODCLI,E3_LOJA,E3_XORIGEM,E3_PEDIDO,E3_MOEDA,E3_BASE,E3_PORC,E3_COMIS,"
		cQuery += "E1_PARCELA,E1_EMISSAO,E1_VENCREA,C5_CONDPAG,E1_VALOR,E1_PARCELA, "
		cQuery += "(SELECT SUM(E3_COMIS) FROM "+RetSqlName("SE3")+" E3B WITH (NOLOCK) WHERE "
		cQuery += "E3B.E3_FILIAL = E3.E3_FILIAL AND E3B.E3_PREFIXO = E3.E3_PREFIXO AND E3B.E3_NUM = E3.E3_NUM AND "
		cQuery += "E3B.E3_CODCLI = E3.E3_CODCLI AND E3B.E3_LOJA = E3.E3_LOJA AND E3B.E3_VEND = E3.E3_VEND AND "
		cQuery += "E3B.E3_PARCELA = E3.E3_PARCELA AND "
		cQuery += "E3B.E3_XMARCA = E3.E3_XMARCA AND E3B.E3_XORIGEM IN ('3','4') AND E3B.D_E_L_E_T_ = ' ') nValor "
	Endif

	cQuery += "FROM "+RetSqlName("SE3")+" E3 WITH (NOLOCK) INNER JOIN "+RetSqlName("SE1")+ " E1 WITH (NOLOCK) ON "
	cQuery += "E3_FILIAL = '"+xFilial("SE3")+"' AND E3_PREFIXO = E1_PREFIXO AND E3_NUM = E1_NUM AND E3_CODCLI = E1_CLIENTE AND "
	cQuery += "E3_LOJA = E1_LOJA AND E3_TIPO = E1_TIPO AND E3_XORIGEM = '1' AND E3_BAIEMI <> 'B' AND E3.D_E_L_E_T_ = ' ' "
	cQuery += "INNER JOIN "+RetSqlName("SC5")+ " C5 WITH (NOLOCK) ON "
	cQuery += "C5_FILIAL = '"+xFilial("SC5")+"' AND C5_NUM = E3_PEDIDO AND C5.D_E_L_E_T_ = ' ' " 
	//cQuery += "WHERE E1_FILIAL = '"+xFilial("SE1")+"' AND E1_BAIXA = '        ' AND E1_VENCREA <= '"+dtos(cDatabase)+"' AND "
	cQuery += "WHERE E1_FILIAL = '"+xFilial("SE1")+"' AND E1_BAIXA = '        ' AND E1.E1_VENCREA BETWEEN '"+dtos(cDtLimite)+"' and '"+dtos(cDatabase)+"' AND 
	cQuery += "E1_TIPO = 'NF' AND E1.D_E_L_E_T_ = ' 'AND E3.E3_COMIS > 0 "

	If i = 2
		cQuery += "ORDER BY E3_VEND,E3_XMARCA"
	Endif

	TcQuery cQuery Alias (cNewAlias) New

	If i = 1
		oProcesso:SetRegua2((cNewAlias)->nTotal)
		(cNewAlias)->(dBCloseArea())
	Endif
Next
TcSetField(cNewAlias,"E1_EMISSAO","D")
TcSetField(cNewAlias,"E1_VENCREA","D")

(cNewAlias)->(dBGotop())
While (cNewAlias)->(!Eof())
	oProcesso:IncRegua2()

	// Verificar se o somatorio dos debitos por inadimplencia com os creditos for menor que zero para nao gerar debitos em duplicidade  
	If (cNewAlias)->nValor < 0
		(cNewAlias)->(dBSkip())
		Loop
	Endif

	sa3->(MSSeek(xFilial("SA3")+(cNewAlias)->E3_VEND))
	If SA3->A3_MSBLQL == "2"
		// Se a parcela do SE3 estiver em branco e porque foi gerada pelo total da nota, ja que a comissao e gerada pelo faturamento
		// Se a parcela do titulo estiver preenchida sera necessario gerar a comissao negativa apenas da parcela em atraso  
		nValBase := (cNewAlias)->E3_BASE
		If Empty((cNewAlias)->E3_PARCELA) .and. !Empty((cNewAlias)->E1_PARCELA)
			aValor := Condicao((cNewAlias)->E3_BASE,(cNewAlias)->C5_CONDPAG,,(cNewAlias)->E1_EMISSAO)

			//ANALISAR NOVAMENTE: Ajuste emergencial para que ele n�o recalcule as datas de vencimento das parcelas de a cordo com a linha 134 - Ferrari - 30/07/2020
			nValBase := (cNewAlias)->E1_VALOR 

			For i := 1 to Len(aValor)
				If aValor[i,1] = (cNewAlias)->E1_VENCREA
					nValBase := aValor[i,2]

				Endif
			Next
		Endif
	EndIf
	
	cChave := xfilial("SE3") + (cNewAlias)->E3_PREFIXO +  (cNewAlias)->E3_NUM +  (cNewAlias)->E1_PARCELA +  (cNewAlias)->E3_TIPO +  (cNewAlias)->E3_CODCLI +  (cNewAlias)->E3_LOJA +  (cNewAlias)->E3_VEND + "3"

	dbSelectArea("SE3")
	dbSetOrder(7) //E3_FILIAL + E3_PREFIXO + E3_NUM + E3_PARCELA + E3_TIPO + E3_CODCLI + E3_LOJA + E3_VEND + E3_XORIGEM
	
	If !dbSeek(cChave)
		RecLock("SE3",.T.)
			SE3->E3_FILIAL  := xFilial("SE3")
			SE3->E3_VEND    := (cNewAlias)->E3_VEND
			SE3->E3_XMARCA  := (cNewAlias)->E3_XMARCA
			SE3->E3_XORIGEM := "3"  // debito por inadimplencia
			SE3->E3_XSITUAC := "S"	// Considerar para o fechamento
			SE3->E3_EMISSAO := dEmissao
			SE3->E3_NUM     := (cNewAlias)->E3_NUM
			SE3->E3_SERIE   := (cNewAlias)->E3_PREFIXO
			SE3->E3_CODCLI  := (cNewAlias)->E3_CODCLI
			SE3->E3_LOJA    := (cNewAlias)->E3_LOJA
			SE3->E3_PREFIXO := (cNewAlias)->E3_PREFIXO
			SE3->E3_PARCELA := (cNewAlias)->E1_PARCELA
			SE3->E3_TIPO    := (cNewAlias)->E3_TIPO
			SE3->E3_BAIEMI  := "E"
			SE3->E3_PEDIDO  := (cNewAlias)->E3_PEDIDO
			SE3->E3_ORIGEM  := "F"
			SE3->E3_VENCTO	:= (cNewAlias)->E1_VENCREA //dEmissao
			//se3->e3_vencto	:= U_RFATM011(sa3->a3_dia,sa3->a3_ddd)
			SE3->E3_MOEDA   := (cNewAlias)->E3_MOEDA
			SE3->E3_SDOC    := (cNewAlias)->E3_PREFIXO
			SE3->E3_BASE    := -nValBase  //(cNewAlias)->e3_base
			SE3->E3_PORC    := (cNewAlias)->E3_PORC
			SE3->E3_COMIS   := -(nValBase*(cNewAlias)->E3_PORC/100)
			SE3->(MsUnlock())

	EndIf

	(cNewAlias)->(dBSkip())

	
EndDo

(cNewAlias)->(dBCloseArea())

Return Nil
/*
For ni := 1 to 4
	cNi	:= AllTrim(Str(ni))
	If ni == 1
		cQuery := "SELECT E1_FILIAL,E1_VEND"+cNi+",E1_NUM,E1_EMISSAO,E1_SERIE,E1_CLIENTE,E1_LOJA,E1_BASCOM"+cNi+",E1_COMIS"+cNi+", "+cEOL
	Else
		cQuery += "UNION ALL "+cEOL
		cQuery += "SELECT E1_FILIAL,E1_VEND"+cNi+",E1_NUM,E1_EMISSAO,E1_SERIE,E1_CLIENTE,E1_LOJA,E1_BASCOM"+cNi+",E1_COMIS"+cNi+", "+cEOL
	EndIf
	cQuery += "		E1_VALCOM"+cNi+",E1_PREFIXO,E1_PARCELA,E1_TIPO,E1_PEDIDO,E1_EMISSAO,E1_MOEDA,E1_SDOC "+cEOL
	cQuery += "FROM "+RetSQLName("SE1")+" SE1 WITH (NOLOCK) "+cEOL
	cQuery += "	JOIN "+RetSQLName("SE3")+" SE3 WITH (NOLOCK) ON E3_VEND=E1_VEND"+cNi+" AND E3_NUM=E1_NUM AND E3_SERIE=E1_SERIE "+cEOL
	cQuery += "									AND SE3.D_E_L_E_T_<>'*' AND NOT E3_COMIS = -E1_VALCOM"+cNi+" "+cEOL
	cQuery += "WHERE SE1.D_E_L_E_T_<>'*' AND E1_BAIXA='' AND E1_VENCREA <= '"+dtos(cDatabase)+"' AND E1_TIPO='NF' AND E1_COMIS"+cNi+"<>'0' "+cEOL
Next ni

If Select("TMPSE1") > 0
	TMPSE1->(DbCloseArea())
EndIf
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSE1",.T.,.T.)

While tmpse1->(!EOF())
	oProcesso:IncRegua2("Gravando Tabela Tempor�ria")

	// Ajustes para o novo tratamento de comissoes - TOTALIT 09/2019
	/*
	aRegs := {}
	aadd(aRegs,{"E3_FILIAL"		, TMPSE1->E1_FILIAL				,Nil})
	aadd(aRegs,{"E3_VEND"		, TMPSE1->E1_VEND1				,Nil})
	aadd(aRegs,{"E3_NUM"		, TMPSE1->E1_NUM				,Nil})
	aadd(aRegs,{"E3_EMISSAO"	, dDtBase						,Nil})
	aadd(aRegs,{"E3_SERIE"		, TMPSE1->E1_SERIE				,Nil})
	aadd(aRegs,{"E3_CODCLI"		, TMPSE1->E1_CLIENTE			,Nil})
	aadd(aRegs,{"E3_LOJA"		, TMPSE1->E1_LOJA				,Nil})
	aadd(aRegs,{"E3_BASE"		, -TMPSE1->E1_BASCOM1			,Nil})
	aadd(aRegs,{"E3_PORC"		, TMPSE1->E1_COMIS1				,Nil})
	aadd(aRegs,{"E3_COMIS"		, -TMPSE1->E1_VALCOM1			,Nil})
	aadd(aRegs,{"E3_PREFIXO"	, TMPSE1->E1_PREFIXO			,Nil})
	aadd(aRegs,{"E3_PARCELA"	, TMPSE1->E1_PARCELA			,Nil})
	aadd(aRegs,{"E3_TIPO"		, TMPSE1->E1_TIPO				,Nil})
	aadd(aRegs,{"E3_BAIEMI"		, "E"							,Nil})
	aadd(aRegs,{"E3_PEDIDO"		, TMPSE1->E1_PEDIDO				,Nil})
	aadd(aRegs,{"E3_ORIGEM"		, "E"							,Nil}) //"E" - Emiss�o Financeiro //"B" - Baixa Financeiro //"F" - Faturamento //"D" - Devolu��o de Venda //"R" - Rec�lculo quando n�o h� origem //"L" - SigaLoja
	aadd(aRegs,{"E3_VENCTO"		, dDtBase						,Nil})
	aadd(aRegs,{"E3_MOEDA"		, "01"							,Nil})
	aadd(aRegs,{"E3_SDOC"		, TMPSE1->E1_SDOC				,Nil})
	aadd(aRegs,{"E3_XINADIM"	, "DEB.INAD. "					,Nil})

	aadd(aNegComiss , aRegs)
	*/

	// Como a comissao e por marca, descobrir o percentual que cada marca representou na venda para calcular o debito
/*	cQuery := "SELECT B1_YMARCA,SUM(D2_VALOR) VALOR FROM "
	cQuery += RetSqlName("SD2")+" D2 INNER JOIN "+RetSqlName("SB1")+" B1 ON "
	cQuery += "B1_FILIAL = '"+xFilial("SB1")+"' AND B1_COD = D2_COD AND B1.D_E_L_E_T_ = ' ' "
	cQuery += "WHERE D2_FILIAL = '"+xFilial("SD2")+"' AND D2_DOC = "+tmpse1->e1_num+" AND D2_SERIE = "+tmpse1->e1_prefixo+" AND "
	cQuery += "D2_CLIENTE = "+tmpse1->e1_cliente+" AND D2_LOJA = "+tmpse1->e1_loja+" AND D2.D_E_L_E_T_ = ' ' "
	cQuery += "GROUP BY B1_YMARCA"
	TcQuery cQuery Alias (cNewAlias) New

	(cNewAlias)->(dBGotop())
	nTotNota := 0
	While (cNewAlias)->(!Eof())
		aAdd(aPercMarca,{(cNewAlias)->b1_ymarca,(cNewAlias)->valor,0})
		nTotNota += (cNewAlias)->valor

		(cNewAlias)->(dBSkip())
	Enddo
	(cNewAlias)->(dBCloseArea())

	For i := 1 to Len(aPercMarca)
		aPercMarca[i,3] := Round(aPercMarca[i,2]/nTotNota*100,2)
	Next

	// Identificar quais os vendedores envolvidos na venda ja que pode ter havido divisao de comissao
	cQuery := "SELECT E3_MARCA,E3_VEND,E3_PORC,"
	cQuery += "(SELECT COUNT(1) FROM "+RetSqlName("SE3")+" E3B WHERE E3B.E3_FILIAL = '"+xFilial("SE3")+"' AND E3B.E3_MARCA = E3.E3_MARCA AND "
	cQuery += "E3B.E3_PREFIXO = E3.E3_PREFIXO AND E3B.E3_NUM = E3.E3_NUM AND E3B.E3_SERIE = E3.E3_SERIE AND E3B.E3_CLIENTE = E3.E3_CLIENTE AND "
	cQuery += "E3B.E3_LOJA = E3.E3_LOJA AND E3B.E3_XORIGEM = '1' AND E3B.D_E_L_E_T_ = ' ') NDIV "
	cQuery += "FROM "+RetSqlName("SE3")+" E3 "
	cQuery += "WHERE E3.E3_FILIAL = '"+xFilial("SE3")+"' AND E3.E3_PREFIXO = "+tmpse1->e1_prefixo+" AND E3.E3_NUM = "+tmpse1->e1_num+" AND "
	cQuery += "E3.E3_CLIENTE = "+tmpse1->e1_cliente+" E3.E3_LOJA = "+tmpse1->e1_loja+" AND E3.E3_XORIGEM = '1' AND E3.D_E_L_E_T_ = ' '"
	TcQuery cQuery Alias (cNewAlias) New

	(cNewAlias)->(dBGotop())
	While (cNewAlias)->(!Eof())
		nBase  := tmpse1->e1_bascom1*aPercMarca[aScan(aPercMarca,{|x| x[1] = (cNewAlias)->e3_marca}),3]/100
		nComis := nBase*(cNewAlias)->e3_porc/If((cNewAlias)->nDiv>1,(cNewAlias)->nDiv,1)

		aAdd(aNegComiss,{{"E3_FILIAL" , TMPSE1->E1_FILIAL    ,Nil},;
						 {"E3_VEND"   , TMPSE1->E1_VEND1     ,Nil},;
						 {"E3_XMARCA" , (cNewAlias)->e3_marca,Nil},;
						 {"E3_NUM"    , TMPSE1->E1_NUM       ,Nil},;
						 {"E3_EMISSAO", dDtBase              ,Nil},;
						 {"E3_SERIE"  , TMPSE1->E1_SERIE     ,Nil},;
						 {"E3_CODCLI" , TMPSE1->E1_CLIENTE   ,Nil},;
						 {"E3_LOJA"   , TMPSE1->E1_LOJA      ,Nil},;
						 {"E3_BASE"   , -nBase               ,Nil},;
						 {"E3_PORC"   , (cNewAlias)->e3_porc ,Nil},;
						 {"E3_COMIS"  , -nComis              ,Nil},;
						 {"E3_PREFIXO", TMPSE1->E1_PREFIXO   ,Nil},;
						 {"E3_PARCELA", TMPSE1->E1_PARCELA   ,Nil},;
						 {"E3_TIPO"   , TMPSE1->E1_TIPO      ,Nil},;
						 {"E3_BAIEMI" , "E"                  ,Nil},;
						 {"E3_PEDIDO" , TMPSE1->E1_PEDIDO    ,Nil},;
						 {"E3_ORIGEM" , "E"                  ,Nil},; //"E" - Emiss�o Financeiro //"B" - Baixa Financeiro //"F" - Faturamento //"D" - Devolu��o de Venda //"R" - Rec�lculo quando n�o h� origem //"L" - SigaLoja
						 {"E3_VENCTO" , dDtBase              ,Nil},;
						 {"E3_MOEDA"  , "01"                 ,Nil},;
						 {"E3_SDOC"   , TMPSE1->E1_SDOC      ,Nil},;
						 {"E3_XORIGEM", "3"                  ,Nil},; // 3-Debito por inadimplencia
						 {"E3_XSITUAC", "S"                  ,Nil}}) // S-Considera para o fechamento de comissoes

		(cNewAlias)->(dBSkip())
	Enddo
	(cNewAlias)->(dBCloseArea())

	tmpse1->(DbSkip())
EndDo

TMPSE1->(DbCloseArea())

Return aNegComiss
*/
static function HFATP31A2(aNegComiss)

	Local _i		:= 0
	Local _x		:= 0
	Local cCount	:= 0
	Local ntam		:= len(aNegComiss)

	oProcesso:SetRegua2(ntam) //Define o limite da regua

	For _i := 1 to len(aNegComiss)

		oProcesso:IncRegua2("Gravando item: "+alltrim(str( _i )) +" de "+ alltrim(str( ntam )))

		DbSelectArea("SE3")
/*		DbOrderNickName("SE3INAD")
		If !(dbSeek(xFilial("SE3")+aNegComiss[_i][2][2]+aNegComiss[_i][11][2]+aNegComiss[_i][3][2]+aNegComiss[_i][12][2]+aNegComiss[_i][20][2]))

			If !Found() */

				RecLock("SE3",.T.)
				For _x := 1 to len(aNegComiss[_i])
					cCampo := aNegComiss[_i][_x][1]
					Replace &cCampo  with aNegComiss[_i][_x][2]
				Next _x
				MsUnlock()

				cCount++

//			EndIf

//		EndIf

	Next _i

return (cCount)

Static Function VldPergA()

	Local _sAlias := Alias()
	Local aRegs := {}
	Local i,j

	dbSelectArea("SX1")
	dbSetOrder(1)
	cPergA := PADR(cPergA,10)

	//Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/                             Var01/ Def01     /Cnt01/Var02/Def02/Cnt02/Var03/Def03/Cnt03/Var04/Def04/Cnt04/Var05/Def05/Cnt05/F3
	/*1*/aAdd(aRegs,{cPergA,"01","Data Base              ? ","","","MV_CH1","D",8,0,0,"G","","MV_PAR01",""   ,"","",""        ,"",""   ,"","","","","","","","","","","","","","","","","","","",""   })

	For i:=1 to Len(aRegs)
		If !dbSeek(cPergA+aRegs[i,2])
			RecLock("SX1",.T.)
			For j:=1 to FCount()
				If j <= Len(aRegs[i])
					FieldPut(j,aRegs[i,j])
				Endif
			Next
			MsUnlock()
		Endif
	Next

	DbSelectArea(_sAlias)

Return Nil

Static Function HFATP31B()

	Local aCores      := {}
	Local oButton1
	Local oButton2
	Local oFont1      := TFont():New("MS Sans Serif",,014,,.T.,,,,,.F.,.F.)
	Local oFont2 	  := TFont():New("MS Sans Serif",,010,,.T.,,,,,.F.,.F.)
	Local oSay1
	Local aCpoBro     := {}

	Private cArquivo  := ""
	Private lInverte  := .F.
	Private cMark     := GetMark()
	Private oMark
	Private oSay2
	Private oSay3
	Private nQtdN     := 0
	Private cPergB    := "HFATP31B"

	Static oDlg1

	VldPergB()
	If !Pergunte(cPergB,.T.)
		Return
	EndIf

	Processa( {|| cArquivo  := HFATP31B1() }, "Aten��o", "Gerando Tabela Temporaria...", .F.)

	If Empty(Alltrim(cArquivo))
		MsgInfo("N�o existem titulos de Mostru�rio.","Aten��o")
		Return(Nil)
	EndIf

	aCpoBro	:= {	{ "OK"				,,"   " 			,"@!"						},;
	{ "F2_FILIAL"		,,"Filial"			,"@!"						},;
	{ "F2_DOC"			,,"N. Fiscal"		,"@!"						},;
	{ "F2_SERIE"		,,"Serie"			,"@!"						},;
	{ "D2_PEDIDO"		,,"Ped. Venda"		,"@!"						},;
	{ "F2_EMISSAO"		,,"Dt. Emiss�o"		,"@D 99/99/9999"			},;
	{ "E1_VENCREA"		,,"Dt. Vencto"		,"@D 99/99/9999"			},;
	{ "E1_PARCELA"		,,"Parcela"			,"@!"						},;
	{ "E1_VALOR"		,,"Valor"			,"@E 9,999,999.99"			},;
	{ "E1_CLIENTE"		,,"Cliente"			,"@!"						},;
	{ "E1_LOJA"			,,"Loja"			,"@!"						},;
	{ "A3_COD"			,,"Cod.Repres."		,"@!"						},;
	{ "A1_NOME"			,,"Representante"	,"@!"						}}

	If(Select("TRB") <> 0)
		dbSelectArea("TRB")
		dbCloseArea()
	EndIf

	cChave   := "A1_NOME"
	dbUseArea(.T.,,cArquivo,"TRB",.T.,.F.)

	IndRegua("TRB",cArquivo,cChave,,,"Selecionando Registros...")
	dbSelectArea("TRB")

	DEFINE MSDIALOG oDlg1 TITLE "Titulos de Mostru�rio no Contas a Receber" FROM 000, 000  TO 580, 1200 COLORS 0, 16777215 PIXEL Style DS_MODALFRAME

	@ 005, 005 SAY oSay1 PROMPT "Lista de Titulos de Mostru�rio para quitar no financeiro e gerar Debito na Comiss�o:" SIZE 262, 007 OF oDlg1 FONT oFont1 COLORS CLR_RED PIXEL
	oMark := MsSelect():New("TRB","OK","",aCpoBro,@lInverte,@cMark,{  019, 005, 262, 595},,,,,aCores)
	oMark:bMark := {| | HFATP31B3()}
	@ 269, 005 BUTTON oButton1 PROMPT "Inverte Sele��o"		SIZE 050, 013 OF oDlg1 ACTION HFATP31B4()	    			PIXEL
	@ 272, 065 SAY oSay2 PROMPT "Titulos Selecionados :"	SIZE 064, 007 OF oDlg1 FONT oFont2 COLORS 0, 16777215		PIXEL
	@ 272, 140 SAY oSay3 PROMPT Alltrim(Str(nQtdN))			SIZE 025, 007 OF oDlg1 FONT oFont2 COLORS CLR_RED    		PIXEL
	@ 269, 500 BUTTON oButton1 PROMPT "Gera D�bito"			SIZE 040, 013 OF oDlg1 ACTION HFATP31B5(oDlg1)    			PIXEL
	@ 269, 550 BUTTON oButton2 PROMPT "Fechar"     			SIZE 040, 013 OF oDlg1 ACTION HFATP31B2(cArquivo, oDlg1) 	PIXEL

	ACTIVATE MSDIALOG oDlg1 CENTERED

	If(Select("TRB") <> 0)
		dbSelectArea("TRB")
		dbCloseArea()
	EndIf

Return(Nil)

Static Function HFATP31B1()

	Local cEOL		:= +Chr(13)+Chr(10)
	Local cSql		:= ""
	Local cArqNome	:= ""

	aCampos		:= {{ "OK"				,"C" ,02 ,0},;
	{ "F2_FILIAL"		,"C" ,04 ,0},;
	{ "F2_DOC"			,"C" ,09 ,0},;
	{ "F2_SERIE"		,"C" ,03 ,0},;
	{ "E1_TIPO"			,"C" ,03 ,0},;
	{ "D2_PEDIDO"		,"C" ,06 ,0},;
	{ "F2_EMISSAO"		,"D" ,10 ,0},;
	{ "E1_VENCREA"		,"D" ,10 ,0},;
	{ "E1_PARCELA"		,"C" ,03 ,0},;
	{ "E1_VALOR"		,"N" ,16 ,2},;
	{ "E1_CLIENTE"		,"C" ,06 ,0},;
	{ "E1_LOJA"			,"C" ,04 ,0},;
	{ "A3_COD"			,"C" ,06 ,0},;
	{ "A1_NOME"			,"C" ,60 ,0},;
	{ "A1_NREDUZ"		,"C" ,40 ,0},;
	{ "E1_NATUREZ"		,"C" ,10 ,0},;
	{ "REGISTRO"		,"C" ,15 ,0}}

	cArqNome	:= CriaTrab(aCampos)
	dbUseArea( .T. ,, cArqNome, "TRB", .F., .F. )

	If (Select("TRI") <> 0)
		dbSelectArea("TRI")
		dbCloseArea()
	EndIf

	cSql := "SELECT F2_FILIAL, F2_DOC, F2_SERIE, E1_TIPO, D2_PEDIDO, E1_CLIENTE, E1_LOJA, F2_EMISSAO, E1_VENCREA, E1_PARCELA, "+cEOL
	cSql += "		E1_VALOR, A3_COD, A1_COD, A1_NOME, A1_NREDUZ, E1_NATUREZ, "+cEOL
	cSql += "		LTRIM(RTRIM(CONVERT(NCHAR,SE1.R_E_C_N_O_))) AS REGISTRO, D2_PEDIDO, C5_VEND1, E3_XORIGEM "+cEOL
	cSql += "FROM "+RetSqlName("SF2")+" SF2 WITH (NOLOCK) "+cEOL
	cSql += "	INNER JOIN "+RetSqlName("SD2")+" SD2 WITH (NOLOCK) ON "
	cSql += "   D2_FILIAL=F2_FILIAL AND D2_DOC=F2_DOC AND D2_SERIE=F2_SERIE AND SD2.D_E_L_E_T_= ' ' "+cEOL
	cSql += "	INNER JOIN "+RetSqlName("SC5")+" SC5 WITH (NOLOCK) ON C5_FILIAL=D2_FILIAL AND C5_NUM=D2_PEDIDO  AND SC5.D_E_L_E_T_= ' ' "+cEOL
	cSql += "	INNER JOIN "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) ON C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM  AND SC6.D_E_L_E_T_= ' ' "+cEOL
	cSql += "	INNER JOIN "+RetSqlName("SE1")+" SE1 WITH (NOLOCK) ON E1_NUM=F2_DOC AND E1_PREFIXO=F2_SERIE AND E1_TIPO<>'NCC' AND SE1.D_E_L_E_T_= ' ' "+cEOL
	cSql += "	INNER JOIN "+RetSqlName("SF4")+" SF4 WITH (NOLOCK) ON F4_CODIGO=C6_TES AND SF4.D_E_L_E_T_= ' ' "+cEOL
	cSql += "	LEFT JOIN "+RetSqlName("SE3")+" SE3 WITH (NOLOCK) ON E3_NUM = F2_DOC AND E1_PREFIXO = E3_SERIE AND E3_PARCELA = E1_PARCELA "+cEOL
	cSql += " AND E3_CODCLI = F2_CLIENTE AND E3_LOJA = F2_LOJA AND SE3.D_E_L_E_T_ = ' ' "+cEOL
	cSql += "	INNER JOIN "+RetSqlName("SA1")+" SA1 WITH (NOLOCK) ON A1_COD=F2_CLIENTE AND A1_LOJA=F2_LOJA AND SA1.D_E_L_E_T_= ' ' "
	cSql += "	LEFT JOIN "+RetSqlName("SA3")+" SA3 WITH (NOLOCK) ON A3_CGC=A1_CGC AND SA3.D_E_L_E_T_= ' ' "
	cSql += "WHERE SF2.D_E_L_E_T_ = ' ' "+cEOL
	cSql += "	AND C5_POLCOM = '"+mv_par01+"' "+cEOL
	cSql += "	AND E1_VENCREA >= '"+dToS(mv_par02)+"' AND E1_VENCREA <= '"+dToS(mv_par03)+"' "+cEOL
	cSql += "	AND (A3_COD> = '"+mv_par04+"' AND A3_COD<='"+mv_par05+"' OR A3_COD IS NULL) "
	cSql += "	AND A3_MSBLQL = '2' AND E1_BAIXA = '        ' "+cEOL
	cSql += "	AND (E3_XORIGEM <> '6' OR E3_XORIGEM IS NULL) "+cEOL
//	cSql += "	AND (E3_XINADIM<>'MOSTRUARIO' OR E3_XINADIM IS NULL) "+cEOL
	cSql += "GROUP BY F2_FILIAL, F2_DOC, F2_SERIE, E1_TIPO, E1_CLIENTE, E1_LOJA, F2_EMISSAO, E1_VENCREA, E1_PARCELA, E1_VALOR, "+cEOL
	cSql += "		A3_COD, A1_COD, A1_NOME, A1_NREDUZ, E1_NATUREZ, D2_PEDIDO, C5_VEND1, E3_XORIGEM, SE1.R_E_C_N_O_ "+cEOL
	cSql += "ORDER BY A3_COD, E1_VENCREA "+cEOL

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cSql),"TRI",.T.,.T.)
	dbSelectArea("TRI")
	TRI->(dbGoTop())

	While !TRI->(Eof())
		RecLock("TRB",.T.)
		TRB->OK				:= Space(2)
		TRB->F2_FILIAL		:= TRI->F2_FILIAL
		TRB->F2_DOC			:= TRI->F2_DOC
		TRB->F2_SERIE		:= TRI->F2_SERIE
		TRB->E1_TIPO		:= TRI->E1_TIPO
		TRB->D2_PEDIDO		:= TRI->D2_PEDIDO
		TRB->F2_EMISSAO		:= STOD(TRI->F2_EMISSAO)
		TRB->E1_VENCREA		:= STOD(TRI->E1_VENCREA)
		TRB->E1_PARCELA		:= TRI->E1_PARCELA
		TRB->E1_VALOR		:= TRI->E1_VALOR
		TRB->E1_CLIENTE		:= TRI->E1_CLIENTE
		TRB->E1_LOJA		:= TRI->E1_LOJA
		TRB->A3_COD			:= TRI->A3_COD
		TRB->A1_NOME		:= TRI->A1_NOME
		TRB->A1_NREDUZ		:= TRI->A1_NREDUZ
		TRB->E1_NATUREZ		:= TRI->E1_NATUREZ
		TRB->REGISTRO		:= AllTrim(TRI->REGISTRO)
		TRB->(MsUnLock())

		TRI->(dbSkip())
	EndDo

	TRI->(dbCloseArea())

	If (Select("TRI") <> 0)
		dbSelectArea ("TRI")
		dbCloseArea()
	EndIf

Return(cArqNome)

Static Function HFATP31B2(cArq, oDlg1)

	oDlg1:End()

	If(Select("TRB") <> 0)
		dbSelectArea("TRB")
		dbCloseArea()
	EndIf

	Ferase(cArquivo+ ".DBF")

Return(Nil)

Static Function HFATP31B3()

	If AllTrim(TRB->A3_COD)<>''
		RecLock("TRB",.F.)
		If Marked("OK")
			TRB->OK := cMark
			nQtdN++
		Else
			TRB->OK := Space(1)
			nQtdN--
		EndIf
		MSUNLOCK()
	Else
		Alert ("Favor verificar o cadastro do Representante!")
		RecLock("TRB",.F.)
		TRB->OK := Space(1)
		MSUNLOCK()
	EndIf

	oMark:oBrowse:Refresh()
	oSay3:Refresh()

Return(Nil)

Static Function HFATP31B4()

	dbSelectArea("TRB")
	TRB->(dbGoTop())
	While TRB->(!Eof())

		If AllTrim(TRB->A3_COD)<>''
			RecLock("TRB",.F.)
			If(TRB->OK <> cMark)
				TRB->OK := cMark
				nQtdN++
			Else
				TRB->OK := Space(1)
				nQtdN--
			EndIf
			MSUNLOCK()
		Else
			Alert ("Favor verificar o cadastro do Representante!")
			RecLock("TRB",.F.)
			TRB->OK := Space(1)
			MSUNLOCK()
		EndIf

		TRB->(DBSKIP())
	EndDo

	dbSelectArea("TRB")
	TRB->(dbGoTop())

	oMark:oBrowse:Refresh()
	oSay3:Refresh()

Return(Nil)

Static Function HFATP31B5(oDlg1)

	Local oButton1
	Local oButton2
	Local oGet1
	Local oSay1
	Local xopc			:= 2
	Local nCount		:= 0
	Private cGetDate	:= Date()
	Static oDlg2

	If(nQtdN <= 0)
		MsgAlert("Selecione os t�tulos para gerar o D�bito de Comiss�o.","Aten��o")
	Else
		DEFINE MSDIALOG oDlg2 TITLE "Gera D�bito de Comiss�o" FROM 000, 000  TO 150, 390 COLORS 0, 16777215 PIXEL

		@ 009, 014 SAY oSay1 PROMPT "Selecione a data de fechamento da Comiss�o" SIZE 169, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
		@ 030, 060 MSGET oGet1 VAR cGetDate SIZE 074, 010 PICTURE "@D 99/99/9999" OF oDlg2 COLORS 0, 16777215 PIXEL
		@ 053, 026 BUTTON oButton1 PROMPT "Confirma" 	SIZE 055, 012 ACTION (xopc := 1,oDlg2:End()) OF oDlg2 PIXEL
		@ 052, 112 BUTTON oButton2 PROMPT "Cancela" 	SIZE 055, 012 ACTION (xopc := 2,oDlg2:End()) OF oDlg2 PIXEL

		ACTIVATE MSDIALOG oDlg2 CENTERED

		If xopc == 1
			While nCount <=2 //executa 2x para certificar que alterou tudo
				HFATP31B6()
				nCount++
			EndDo

			MsgAlert("Gerado T�tulo de Comiss�o e T�tulo de NCC no ambiente de Financeiro.","Aten��o")

			dbSelectArea("TRB")
			oDlg1:End()
		End
	EndIf

Return(Nil)

Static Function HFATP31B6()
Local nTotNota := 0
Local nPos     := 0
Local aComis   := {}
 
SB1->(dBSetOrder(1))  // B1_FILIAL+B1_COD
SD2->(dBSetOrder(3))  // D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM

dbSelectArea("TRB")
TRB->(dbGoTop())
While TRB->(!Eof())
	If (TRB->OK == cMark .AND. AllTrim(TRB->A3_COD)<>'')
		aComis   := {}
		nTotNota := 0

		// Separar por marca
		SD2->(MSSeek(TRB->(F2_FILIAL+F2_DOC+F2_SERIE+E1_CLIENTE+E1_LOJA)))
		While SD2->(!Eof() .AND. D2_FILIAL = TRB->F2_FILIAL .AND. D2_DOC = TRB->F2_DOC .AND. D2_SERIE = TRB->F2_SERIE .AND. ;
					D2_CLIENTE = TRB->E1_CLIENTE .AND. D2_LOJA = TRB->E1_LOJA)
	
			SB1->(MSSeek(xFilial("SB1")+SD2->d2_cod))
	
			nPos := aScan(aComis,{|x| x[1] = SB1->B1_YMARCA})
			If nPos = 0
				aAdd(aComis,{SB1->B1_YMARCA,0})
				nPos := Len(aComis)
			Endif
			aComis[nPos,2] += SD2->D2_TOTAL
			nTotNota       += SD2->D2_TOTAL
	
			SD2->(dBSkip())
		Enddo

/*		DbSelectArea("SE3")
		DbOrderNickName("SE3INAD")
		dbSeek(xFilial("SE3")+TRB->A3_COD+TRB->F2_SERIE+TRB->F2_DOC+TRB->E1_PARCELA+"MOSTRUARIO")
		If !Found() */
		For nPos := 1 to Len(aComis)
			cBlq := POSICIONE("SA3",1,xFilial("SA3")+Alltrim(TRB->A3_COD),"A3_MSBLQL")
			If cBlq == "2"
				RecLock("SE3",.T.)
				SE3->E3_FILIAL  := xFilial("SE3")
				SE3->E3_VEND    := TRB->A3_COD
				SE3->E3_XMARCA  := aComis[nPos,1]
				SE3->E3_XORIGEM := "6"  // 6-Debito mostruario
				SE3->E3_XSITUAC := "S"	// Considerar para o fechamento
				SE3->E3_EMISSAO := cGetDate
				SE3->E3_NUM     := TRB->F2_DOC
				SE3->E3_SERIE   := TRB->F2_SERIE
				SE3->E3_CODCLI  := TRB->E1_CLIENTE
				SE3->E3_LOJA    := TRB->E1_LOJA
				SE3->E3_PREFIXO := TRB->F2_SERIE
				SE3->E3_PARCELA := TRB->E1_PARCELA
				SE3->E3_TIPO    := TRB->E1_TIPO
				SE3->E3_BAIEMI  := "E"
				SE3->E3_PEDIDO  := TRB->D2_PEDIDO
				SE3->E3_ORIGEM  := "E"
				SE3->E3_VENCTO	:= TRB->E1_VENCREA //cGetDate
				SE3->E3_MOEDA   := "01"
				SE3->E3_SDOC    := TRB->F2_SERIE
				SE3->E3_BASE    := -(TRB->E1_VALOR*(aComis[nPos,2]/nTotNota))
				SE3->E3_PORC    := 100
				SE3->E3_COMIS   := -(TRB->E1_VALOR*(aComis[nPos,2]/nTotNota))
				SE3->(MsUnlock())
			EndIf

/*			RecLock("SE3",.T.)
			SE3->E3_FILIAL		:= xFilial("SE3")
			SE3->E3_VEND		:= TRB->A3_COD
			SE3->E3_NUM			:= TRB->F2_DOC
			SE3->E3_EMISSAO		:= cGetDate
			SE3->E3_SERIE		:= TRB->F2_SERIE
			SE3->E3_CODCLI		:= TRB->E1_CLIENTE
			SE3->E3_LOJA		:= TRB->E1_LOJA
			SE3->E3_BASE		:= -TRB->E1_VALOR
			SE3->E3_PORC		:= 100
			SE3->E3_COMIS		:= -TRB->E1_VALOR
			SE3->E3_PREFIXO		:= TRB->F2_SERIE
			SE3->E3_PARCELA		:= TRB->E1_PARCELA
			SE3->E3_TIPO		:= TRB->E1_TIPO
			SE3->E3_BAIEMI		:= "E"
			SE3->E3_PEDIDO		:= TRB->D2_PEDIDO
			SE3->E3_ORIGEM		:= "E"
			SE3->E3_VENCTO		:= cGetDate
			SE3->E3_MOEDA		:= "01"
			SE3->E3_SDOC		:= "2"
			SE3->E3_XINADIM		:= "MOSTRUARIO"
			MsUnLock()  */
		Next
//		End

/*  n�o gerar NCC conforme solicita��o do Sr. Fernando
		dbSelectArea("SE1")
		SE1->(DbSetOrder(1)) //E1_FILIAL, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO
		Dbseek(xFilial("SE1")+TRB->F2_SERIE+TRB->F2_DOC+TRB->E1_PARCELA+"NCC")
		If !Found() 
			RecLock("SE1",.T.)
			SE1->E1_FILIAL	:= xFilial("SE1")
			SE1->E1_PREFIXO := TRB->F2_SERIE
			SE1->E1_NUM 	:= TRB->F2_DOC
			SE1->E1_PARCELA := TRB->E1_PARCELA
			SE1->E1_TIPO  	:= "NCC"
			SE1->E1_NATUREZ := TRB->E1_NATUREZ
			SE1->E1_CLIENTE := TRB->E1_CLIENTE
			SE1->E1_LOJA	:= TRB->E1_LOJA
			SE1->E1_NOMCLI	:= TRB->A1_NREDUZ
			SE1->E1_EMISSAO	:= cGetDate
			SE1->E1_EMIS1	:= cGetDate
			SE1->E1_VENCTO	:= cGetDate
			SE1->E1_VENCORI	:= cGetDate
			SE1->E1_VENCREA	:= DataValida(cGetDate)
			SE1->E1_VALOR	:= TRB->E1_VALOR
			SE1->E1_SALDO 	:= TRB->E1_VALOR
			SE1->E1_VLCRUZ	:= TRB->E1_VALOR
			SE1->E1_HIST	:= "TITULO PARA QUITACAO DE MOSTRUARIO"
			SE1->E1_ORIGEM	:= "HFATP031"
			SE1->E1_SITUACA	:= "0"
			SE1->E1_MOEDA   := 1
			SE1->E1_FILORIG := cFilAnt
			SE1->E1_LA 		:= "S"
			SE1->E1_SERIE	:= TRB->F2_SERIE
			SE1->E1_STATUS	:= "A"
			SE1->E1_RELATO	:= "2"
			SE1->E1_TPDESC	:= "C"
			SE1->E1_MSFIL	:= cFilAnt
			SE1->E1_MSEMP	:= cEmpAnt
			MsUnLock()
		EndIf
*/

		RecLock("TRB",.F.)
		TRB->OK := Space(1)
		MsUnLock()
	EndIf

	TRB->(DBSKIP())
EndDo

Return(Nil)

Static Function VldPergB()

	Local _sAlias := Alias()
	Local aRegs := {}
	Local i,j

	dbSelectArea("SX1")
	dbSetOrder(1)
	cPergB := PADR(cPergB,10)

	//Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/                      Var01/    Def01    /Cnt01 /Var02/Def02/Cnt02 /Var03/Def03/Cnt03 /Var04/Def04/Cnt04 /Var05/Def05/Cnt05 /F3
	/*1*/aAdd(aRegs,{cPergB,"01","Politica de Comiss�o      ? ","","","MV_CH1","C",3,0,0,"G","","MV_PAR01","","","","007"   ,"","","","",""    ,"","","","",""    ,"","","","",""    ,"","","","",""    ,"SZ2"})
	/*2*/aAdd(aRegs,{cPergB,"02","Da Data de Vencimento     ? ","","","MV_CH2","D",8,0,0,"G","","MV_PAR02","","","",""      ,"","","","",""    ,"","","","",""    ,"","","","",""    ,"","","","",""    ,""   })
	/*3*/aAdd(aRegs,{cPergB,"03","At� a Data de Vencimento  ? ","","","MV_CH3","D",8,0,0,"G","","MV_PAR03","","","",""      ,"","","","",""    ,"","","","",""    ,"","","","",""    ,"","","","",""    ,""   })
	/*4*/aAdd(aRegs,{cPergB,"04","Do Vendedor               ? ","","","MV_CH4","C",6,0,0,"G","","MV_PAR04","","","","      ","","","","",""    ,"","","","",""    ,"","","","",""    ,"","","","",""    ,"SA3"})
	/*5*/aAdd(aRegs,{cPergB,"05","At� o Vendedor            ? ","","","MV_CH5","C",6,0,0,"G","","MV_PAR05","","","","ZZZZZZ","","","","",""    ,"","","","",""    ,"","","","",""    ,"","","","",""    ,"SA3"})

	For i:=1 to Len(aRegs)
		If !dbSeek(cPergB+aRegs[i,2])
			RecLock("SX1",.T.)
			For j:=1 to FCount()
				If j <= Len(aRegs[i])
					FieldPut(j,aRegs[i,j])
				Endif
			Next
			MsUnlock()
		Endif
	Next

	DbSelectArea(_sAlias)

Return Nil
