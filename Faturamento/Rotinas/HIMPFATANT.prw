#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

//Importar direto na tabela

User Function HIMPFATANT()


	Processa( {|| U_SF2CSVant(.T.) }, "Aguarde...", "Processando...",.F.) //.T. Importa .F. Simula


Return

User Function SF2CSVant(lImporta)
	Local nPos   := 0
	Local cLinha := ""
	Local cCab   := ""
	Local nIni   := 0
	Local i      := 0
	Local x      := 0
	Local aCSV   := {}
	Local nCols  := 0
	Local cQuery := ""
	Local cPerg  := 'HPREL031  '  
	Local cHaveNf:= ""    
	If !Pergunte(cPerg, .T.)
		Return
	Endif


	//cQuery := "SELECT TOP 10 * FROM MOBILE.dbo.HISTORICO_NF_CABECALHO "

	cQuery := " SELECT F2_FILIAL,F2_DOC,F2_SERIE,CNPJ_CLIENTE  AS F2_CNPJ ,F2_COND,F2_DUPL,F2_EMISSAO,F2_EST,DATA,F2_VOLUME1,F2_TRANSP,F2_REDESP, "
	cQuery += " F2_PBRUTO,F2_PLIQUI,F2_TIPOCLI,F2_VEND1,F2_VEND2,F2_CHVNFE,NAT_OPERACAO AS NAT_OPER,QUANTIDADE	 AS QUANTIDADE,VALOR_TOTAL_SEM_DESCONTO AS VALOR_TO, "
	cQuery += " F2_FRETE,	F2_SEGURO,	F2_DESPESA,	F2_DESCONT,	F2_VALBRUT,	VALOR_NOTA,	F2_BASEICM,	F2_VALICM,	PIS,	COFINS, VALOR_MERCADORIA AS VALOR_ME"
	cQuery += " FROM MOBILE.dbo.HISTORICO_NF_CABECALHO WITH (NOLOCK) WHERE F2_EMISSAO BETWEEN '"+dTos(MV_PAR01)+"' AND '"+dTos(MV_PAR02)+"'  ORDER BY F2_DOC " 

	//cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRB", .T., .T.)

	TcSetField("XTRB","EMISSAO","D",08,0)
	TcSetField("XTRB","ENTREGA","D",08,0)

	cNumReg := "000000"

	cLog := "Pedidos abaixo importados com sucesso na SC5:"
	cLog += CRLF + "CNPJ_FILIAL / PEDIDO_MILLENIUM"

	cEFIL := "Erro Filial n�o encontrada:"
	cEDA0 := "Tabelas de pre�o n�o encontradas:"
	cESA1 := "Cliente n�o encontrado:"
	cESA4 := "Transportadora n�o encontrada:"
	cESE4 := "Condicao de Pagamento n�o encontrada:"
	cESA3 := "Vendedor n�o encontrado:"                                                      
	cESZ2 := "Politica comercial nao encontrada:"
	cESZ4 := "Tipo de Pedido Nao encontrado:"
	cESZ6 := "Prazo nao encontrado:"

	lEFIL := .F.
	lEDA0 := .F.
	lESA1 := .F.
	lESA4 := .F.
	lESE4 := .F.
	lESA3 := .F.
	lESZ2 := .F.
	lESZ4 := .F.
	lESZ6 := .F.

	xv:= 0
	XTRB->(DbGoTop())
	lFaz := .t.
	//nqtdNF := 10  
	//ProcRegua(nqtdNF)
	xnqtdNF := XTRB->(LASTREC())
	If  xnqtdNF == 0
		xnqtdNF := 1000
	Endif
	ProcRegua(xnqtdNF )
	Nfeito := 0
	XTRB->(DbGoTop())
	While XTRB->(!EOF())
		IncProc("Gravando Nota Fiscal  No. " + XTRB->F2_DOC + "/ " + transform(Nfeito,'@e 9999999' ) + " de  "+transform(xnqtdNF,'@e 9999999' ) +" ! " )		//"Gravando os Registros..."
		cFil := "0101" 	
		If !Empty(XTRB->F2_CNPJ)
			DbSelectArea("SA1")
			SA1->(DbSetOrder(3))
			If SA1->(DbSeek(xFilial("SA1")+PADR(XTRB->F2_CNPJ,14)))
				cCliente := SA1->A1_COD
				cLoja    := SA1->A1_LOJA
				cCanal   := SA1->A1_XCANAL 
				cDCanal  := SA1->A1_XCANALD
				cEmp     := SA1->A1_XEMP 
				cDEmp    := SA1->A1_XDESEMP
				cGrupo   := SA1->A1_XGRUPO
				cDGrupo  := SA1->A1_XGRUPON
				cDiv     := SA1->A1_XCODDIV
				cDDiv    := SA1->A1_XNONDIV 
				cMacro   := SA1->A1_XMICRRE
				cDMacro  := SA1->A1_XMICRDE
				cReg     := SA1->A1_XCODREG 
				cDReg    := SA1->A1_XDESREG
				cNomecli := SA1->A1_NOME
				cTipoCli := SA1->A1_TIPO
			else
				cESA1 += CRLF + "CNPJ Cliente : "+XTRB->F2_CNPJ+" Documento : "+XTRB->F2_DOC+" Erro: Cliente n�o encontrado CNPJ: "+XTRB->F2_CNPJ
				lESA1 := .T.                                                    
				XTRB->(DbSkip())
				Loop	

			Endif	
		Else
			cESA1 += CRLF + "CNPJ Cliente "+XTRB->F2_CNPJ+" dOCUMENTO : "+XTRB->F2_DOC+" Erro: CNPJ Inexistente?: "+XTRB->F2_CNPJ
			lESA1 := .T.
			xTRB->(DbSkip())
			Loop	
		EndIf


		//F2_FILIAL	F2_DOC	F2_SERIE	CNPJ_CLIENTE	F2_COND	F2_DUPL	F2_EMISSAO	F2_EST	DATA	F2_VOLUME1	F2_TRANSP	F2_REDESP	F2_PBRUTO	F2_PLIQUI	F2_TIPOCLI	
		//F2_VEND1	F2_VEND2	F2_CHVNFE	NAT_OPERACAO	COD_PEDIDOV	EVENTO	COD_MREGIAO	DESC_MACRO_REGIAO	REGIONAL	COD_CLIENTE	NOME_CLIENTE	DATA_ANIVERSARIO	
		//GRUPO_LOJA	TIPO_MOVIMENTO	HSM	QUANTIDADE	VALOR_TOTAL_SEM_DESCONTO	F2_FRETE	F2_SEGURO	F2_DESPESA	F2_DESCONT	F2_VALBRUT	VALOR_NOTA	F2_BASEICM	
		//F2_VALICM	PIS	COFINS	VALOR_MERCADORIA

		IF Select("yTRv") > 0
			yTRv->(dbCloseArea())
		Endif
		if  XTRB->F2_SERIE = 2
			cSerie := '2  '
		Else
			cSerie := '1  '
		Endif
		cQry :=  "SELECT R_E_C_N_O_ AS REG,  * FROM SF2010 WHERE F2_DOC = '"+XTRB->F2_DOC+"' AND F2_SERIE = '"+cSerie+"' AND D_E_L_E_T_ <> '*' "
		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQry),"yTRv", .T., .T.)
		yTRv->(DbGoTop())
		lAchou := .f.
		If yTRv->(!EOF())
			cReg := yTRv->REG
			lAchou := .t.
		ELSE
			lAchou := .f.
		Endif
		
		   
		DbSelectArea("SF2")
		IF lAchou 
			SF2->(dbGoto(cReg))
			SF2->(RecLock("SF2",.F.))
		Else   
			SF2->(RecLock("SF2",.T.))
			SF2->F2_FILIAL	:= XTRB->F2_FILIAL	
			SF2->F2_DOC	    := XTRB->F2_DOC	
			SF2->F2_SERIE	:= cSerie
		Endif
		SF2->F2_FILIAL	:= XTRB->F2_FILIAL	
		SF2->F2_DOC	    := XTRB->F2_DOC	
		SF2->F2_SERIE	:= cSerie
		SF2->F2_CLIENTE	:= cCliente 
		SF2->F2_LOJA	:= cLoja   
		SF2->F2_COND	:= XTRB->F2_COND
		SF2->F2_DUPL	:= XTRB->F2_DUPL
		SF2->F2_EMISSAO	:= ctod(Substr(XTRB->F2_EMISSAO,7,2)+'/'+Substr(XTRB->F2_EMISSAO,5,2)+'/'+Substr(XTRB->F2_EMISSAO,3,2)) //	XTRB->F2_EMISSAO
		SF2->F2_EST	    := XTRB->F2_EST
		SF2->F2_FRETE	:= XTRB->F2_FRETE
		SF2->F2_SEGURO	:= XTRB->F2_SEGURO
		SF2->F2_TIPOCLI	:= cTipoCli
		SF2->F2_VALBRUT	:= XTRB->F2_VALBRUT
		SF2->F2_VALICM	:= XTRB->F2_VALICM
		SF2->F2_BASEICM	:= XTRB->F2_BASEICM
		SF2->F2_VALIPI	:= 0
		SF2->F2_BASEIPI	:= 0
		SF2->F2_VALMERC	:= XTRB->VALOR_NOTA
		SF2->F2_DESCONT	:= XTRB->F2_DESCONT
		SF2->F2_TIPO	:= 'N'
		SF2->F2_ESPECI1	:= 'CAIXA'
		SF2->F2_VOLUME1 :=  VAL(XTRB->F2_VOLUME1) 
		SF2->F2_ICMSRET	:= 0
		SF2->F2_PLIQUI	:= XTRB->F2_PLIQUI
		SF2->F2_PBRUTO	:= XTRB->F2_PBRUTO
		SF2->F2_TRANSP	:= XTRB->F2_TRANSP
		SF2->F2_REDESP	:= XTRB->F2_REDESP
		SF2->F2_VEND1	:= StrZero(XTRB->F2_VEND1,6)
		SF2->F2_FIMP	:= 'S'
		SF2->F2_DTLANC 	:= ctod(Substr(XTRB->F2_EMISSAO,7,2)+'/'+Substr(XTRB->F2_EMISSAO,5,2)+'/'+Substr(XTRB->F2_EMISSAO,3,2))     //XTRB->F2_EMISSAO
		SF2->F2_VALFAT	:= XTRB->VALOR_NOTA
		SF2->F2_DESPESA	:= XTRB->F2_DESPESA	 
		SF2->F2_ESPECIE := 'SPED'
		SF2->F2_PREFIXO := '2' 
		SF2->F2_BASIMP5 := XTRB->VALOR_NOTA
		SF2->F2_BASIMP6 := XTRB->VALOR_NOTA
		SF2->F2_VALIMP5	:= XTRB->PIS
		SF2->F2_VALIMP6	:= XTRB->COFINS
		SF2->F2_HORA	:= TIME() 
		SF2->F2_MOEDA   := 1 
		SF2->F2_REGIAO	:=  ''
		SF2->F2_VALCSLL	:=  0
		SF2->F2_VALCOFI	:=  0
		SF2->F2_VALPIS	:=  0
		SF2->F2_RECFAUT := '1' 
		SF2->F2_CHVNFE	:= XTRB->F2_CHVNFE
		SF2->F2_TPFRETE	:= 'C' 
		SF2->F2_DAUTNFE := ctod(Substr(XTRB->F2_EMISSAO,7,2)+'/'+Substr(XTRB->F2_EMISSAO,5,2)+'/'+Substr(XTRB->F2_EMISSAO,3,2))    // XTRB->F2_EMISSAO
		SF2->F2_CLIENT	:= cCliente
		SF2->F2_LOJENT  := cLoja 
		SF2->(MsUnlock())
		U_SD2CSVant(XTRB->F2_FILIAL,XTRB->F2_DOC,cSerie,cCliente,cLoja,Nfeito,xnqtdNF,cLoja)
		cLog += CRLF + cNumReg + " - " + xTRB->F2_CNPJ+" - "+xTRB->F2_DOC
		cHaveNf:= XTRB->F2_FILIAL+XTRB->F2_DOC+STRZERO(XTRB->F2_SERIE,1)+XTRB->F2_CNPJ
		XTRB->(DbSkip())
		IncProc("Gravando Nota Fiscal  No. " + XTRB->F2_DOC + "/ " + transform(Nfeito,'@e 9999999' ) + " de  "+transform(xnqtdNF,'@e 9999999' ) +" !" )		//"Gravando os Registros..."
		If XTRB->F2_FILIAL+XTRB->F2_DOC+STRZERO(XTRB->F2_SERIE,1)+XTRB->F2_CNPJ == cHaveNf
			while XTRB->(!eof()) .and.  XTRB->F2_FILIAL+XTRB->F2_DOC+STRZERO(XTRB->F2_SERIE,1)+XTRB->F2_CNPJ == cHaveNf
                dbselectarea('SF2')
 			    SF2->(RecLock("SF2",.F.))
				SF2->F2_VALBRUT	:= SF2->F2_VALBRUT+XTRB->F2_VALBRUT
				SF2->F2_VALICM	:= SF2->F2_VALICM+XTRB->F2_VALICM
				SF2->F2_VALMERC	:= SF2->F2_VALMERC+XTRB->VALOR_NOTA
				SF2->F2_PBRUTO	:= SF2->F2_PBRUTO+XTRB->F2_PBRUTO
				SF2->(MsUnlock())
				dbselectarea('XTRB')
				XTRB->(DbSkip())
			enddo
		endif    
		Nfeito++
	EndDo

	XTRB->(DbCloseArea())

	MemoWrite("\log_import\SC5\_IMPORT_"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cLog)

	IF(lEFIL,MemoWrite("\log_import\SC5\FIL"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEFIL),)
	IF(lEDA0,MemoWrite("\log_import\SC5\DA0"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEDA0),)
	IF(lESA1,MemoWrite("\log_import\SC5\SA1"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESA1),)
	IF(lESA4,MemoWrite("\log_import\SC5\SA4"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESA4),)
	IF(lESE4,MemoWrite("\log_import\SC5\SE4"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESE4),)
	IF(lESA3,MemoWrite("\log_import\SC5\SA3"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESA3),)
	IF(lESZ2,MemoWrite("\log_import\SC5\SZ2"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESZ2),)
	IF(lESZ4,MemoWrite("\log_import\SC5\SZ4"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESZ4),)
	IF(lESZ6,MemoWrite("\log_import\SC5\SZ6"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESZ6),)

	MsgInfo("Feito Sf2","INTEGRA��O EFETUADA ")

Return


User Function SD2CSVant(CFILIAL,CDOC,cSerie,cCli,cLoj,Nfeito,xnqtdNF,cLoja)
	Local nPos   := 0
	Local cLinha := ""
	Local cCab   := ""
	Local nIni   := 0
	Local i      := 0
	Local x      := 0
	Local aCSV   := {}
	Local nCols  := 0
	Local cTes   := ""

	IF Select("XTRC") > 0
		XTRC->(dbCloseArea())
	Endif

	cTpOper := AllTrim(SuperGetMV("MV_XRTOV2C",.F.,"01")) //Tipo de Operacao de venda para pedidos de venda
	cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking

	cQuery := " SELECT * FROM MOBILE.dbo.HISTORICO_NF_ITEM WITH (NOLOCK) WHERE " 
	cQuery += " D2_FILIAL = '"+XTRB->F2_FILIAL+"' AND D2_DOC = '"+XTRB->F2_DOC+"' AND D2_SERIE = '"+cSerie+"' ORDER BY D2_ITEM  " 

	//XTRB->F2_FILIAL,XTRB->F2_DOC,cSerie

	//cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRC", .F., .T.)

	DbSelectArea("XTRC")

	ProcRegua(155000)

	cNumReg := "000000"
	cDocSeq := "000000"
	cLog := "Pedidos abaixo importados com sucesso na SC6:"
	cLog += CRLF + "CNPJ_FILIAL / PEDIDO_MILLENIUM  / PRODUTO "

	cNumAnt := ""

	cEFIL := "Filial nao encontrada:"
	cESC5 := "Pedido Millenium nao encontrado: "
	cESB1 := "Produto nao encontrado: "
	cETES := "TES nao encontrada: "

	lEFIL := .F.
	lESC5 := .F.
	lESB1 := .F.
	lETES := .F.

	XTRC->(DbGoTop())

	nCont := 0
	cItem := "00"
	XDocSeq := 0	
	While XTRC->(!EOF())
		MemoWrite("\log_import\SC6\_IMPORT_"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cLog)
		cLog := "Pedidos abaixo importados com sucesso na SC5:"
		cLog += CRLF + "CNPJ_FILIAL / PEDIDO_MILLENIUM"

		IF(lEFIL,MemoWrite("\log_import\SC6\FIL"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEFIL),)
		IF(lESC5,MemoWrite("\log_import\SC6\SC5"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESC5),)
		IF(lESB1,MemoWrite("\log_import\SC6\SB1"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESB1),)
		IF(lETES,MemoWrite("\log_import\SC6\TES"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cETES),)

		lEFIL := .F.
		lESC5 := .F.
		lESB1 := .F.
		lETES := .F.

		cEFIL := "Filial nao encontrada:"
		cESC5 := "Pedido Millenium nao encontrado: "
		cESB1 := "Produto nao encontrado: "
		cETES := "TES nao encontrada: "
		nCont := 0
		IncProc("Gravando Itens da Nota Fiscal  No. " + XTRB->F2_DOC + "/ " + transform(Nfeito,'@e 9999999' ) + " de  "+transform(xnqtdNF,'@e 9999999' ) +" !" )		//"Gravando os Registros..."
		Nfeito++
		If !Empty(XTRC->CODIGO_BARRA)
			DbSelectArea("SB1")
			DbSetOrder(5)
			//CNPJ_FIL	PED_MIL	COD_BAR	COD_PRO  	NF_FATU 	QTDE 	QTDENT 	SALDO 	PRECUNI
			If DbSeek(xFilial("SB1")+XTRC->CODIGO_BARRA) 
				cProd := SB1->B1_COD
				cDesc := SB1->B1_DESC
				cUM   := SB1->B1_UM
			Else
				XTRC->(DbSkip())
				loop	
			EndIf
		Else
			XTRC->(DbSkip())
			Loop
		EndIf
		//	MaTesInt(2,cTpOper,cCodCli,cLojCli,"C",SB1->B1_COD,)
		cTES  := MaTesInt(2,cTpOper,cCli,cLoj,"C",cProd,)
		If Empty(cTes)
			cETES += CRLF + "Fil:"+cFil+" Ped: "+cNumPed+" Prod: "+cProd+" Cli:"+cCli+"/"+cLoj
			lETES := .T.
			cTes := "999"
		EndIf
		DbSelectArea("SF4")
		DbSetOrder(1)
		If DbSeek(xFilial("SF4")+cTes)
			cCf := SF4->F4_CF
		Else
			cCf  := "5101"
		EndIf

		//	CNPJ_FIL	PED_MIL	COD_BAR	COD_PRO  	NF_FATU 	QTDE 	QTDENT 	SALDO 	PRECUNI
		//03007414000130	01829534	7891306143002	00034850_19_G	000507355	0	3	-3	15,11

		cItem := SOMA1(cItem,2)
		XDocSeq := 	SOMA1(cDocSeq,5)
		cDocSeq :=  'I'+XDocSeq 
		DbSelectArea("SD2")
		SD2->(DBSETORDER(3))
		IF DbSeek(XTRC->D2_FILIAL+XTRB->F2_DOC+cSerie+cCliente+cLoja+cProd) 
			SD2->(RecLock("SD2",.F.))
		ELSE   
			SD2->(RecLock("SD2",.T.))
			SD2->D2_FILIAL  := XTRC->D2_FILIAL 
			SD2->D2_ITEM    := cItem //STRZERO(D2_ITEM,2)     	
			SD2->D2_COD	    := cProd 
		ENDIF      
		SD2->D2_SEGUM   := cUM
		SD2->D2_UM	    := cUM
		SD2->D2_QUANT   := XTRC->QUANTIDADE
		SD2->D2_PRCVEN  := XTRC->PRECO_UNIT
		SD2->D2_TOTAL   := XTRC->D2_VALBRUT
		SD2->D2_VALIPI  := XTRC->VALOR_IPI
		SD2->D2_VALICM  := XTRC->D2_VALICM
		SD2->D2_TES	    := cTes
		SD2->D2_CF	    := cCf
		SD2->D2_DESC	:= 0
		SD2->D2_IPI	    := 0
		SD2->D2_PICM	:= XTRC->AL_ICMS
		SD2->D2_VALCSL  := 0
		SD2->D2_CONTA	:= '101030204003'
		SD2->D2_PESO	:= 0.54
		SD2->D2_PEDIDO	:= 'ANTIGO'
		SD2->D2_ITEMPV	:= ''
		SD2->D2_CLIENTE	:= cCliente  
		SD2->D2_LOCAL	:= 'E0'
		SD2->D2_DOC	    := XTRB->F2_DOC
		SD2->D2_SERIE	:= cSerie
		SD2->D2_GRUPO	:= 'PA01' 
		SD2->D2_TP	    := 'PF'
		SD2->D2_EMISSAO := ctod(Substr(XTRC->D2_EMISSAO,7,2)+'/'+Substr(XTRC->D2_EMISSAO,5,2)+'/'+Substr(XTRC->D2_EMISSAO,3,2))    // XTRC->D2_EMISSAO 
		SD2->D2_CUSTO1	:= 0
		SD2->D2_CUSTO2	:= 0
		SD2->D2_PRUNIT	:= XTRC->PRECO_UNIT
		SD2->D2_NUMSEQ	:= cDocSeq 
		SD2->D2_EST	    := XTRC->D2_EST
		SD2->D2_DESCON	:= 0  
		SD2->D2_TIPO	:= 'N'
		SD2->D2_BASEICM := XTRC->D2_BASEICM 
		SD2->D2_GRADE   := '2'
		SD2->D2_BASIMP5 := XTRC->BASE_PIS
		SD2->D2_BASIMP6 := XTRC->BASE_COFINS
		SD2->D2_VALIMP5 := XTRC->COFINS
		SD2->D2_VALIMP6 := XTRC->PIS
		SD2->D2_ALQIMP5	:= 7.6
		SD2->D2_ALQIMP6 := 1.65
		SD2->D2_BASEIPI := 0
		SD2->D2_VALBRUT := XTRC->D2_VALBRUT
		SD2->D2_ALQPIS  := 1.65
		SD2->D2_ALQCOF  := 7.6
		SD2->D2_LOJA    := cLoja
		MsUnlock()
		cNumReg := SOMA1(cNumReg,6)
		cLog    += CRLF + cNumReg + XTRB->F2_DOC+" -  PRODUDO CODBARRAS" - "+XTRC->CODIGO_BARRA
		nCont++
		XTRC->(DbSkip())
	EndDo

	MemoWrite("\log_import\SC6\_IMPORT_"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cLog)

	IF(lEFIL,MemoWrite("\log_import\SC6\FIL"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cEFIL),)
	IF(lESC5,MemoWrite("\log_import\SC6\SC5"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESC5),)
	IF(lESB1,MemoWrite("\log_import\SC6\SB1"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cESB1),)
	IF(lETES,MemoWrite("\log_import\SC6\TES"+StrTran(Dtoc(dDatabase),"/","")+StrTran(Time(),":","")+".txt",cETES),)

	//MsgInfo("Feito SC6","Feito")

Return

User Function INSRECNO()
	Local cQuery := ""

	cQuery := "select * from P12_HOM2.dbo.IMPITENS order by PEDMIL "

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"ITENS", .F., .T.)

	DbSelectArea("ITENS")

	While !EOF()

		ITENS->(DbSkip())
	EndDo

Return

/*
//Importar para um array

User Function HIMPPEDMIL()
Local aCabec := {}
Local aItens := {}
Local nx     := 0

PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101' TABLES 'SC5,SC6' MODULO 'FAT'

U_GETCSV("SC5",@aCabec,"D:\CABEC.csv",.t.) //.T. quando a primeira linha contem cabecalho 

U_GETCSV("SC6",@aItens,"D:\ITENS.csv",.t.) 

If Len(aCabec) > 0
For nx := 1 to Len(aCabex)


Next
EndIf

//cDebug := 0

RESET ENVIRONMENT

Return

User Function GETCSV(aMatriz,cArquivo,lUsaCab)
Local nPos   := 0
Local cLinha := ""
Local cCab   := ""
Local nIni   := 0
Local i      := 0
Local x      := 0
Local aCSV   := {}
Local nCols  := 0

Default lUsaCab := .F.

aMatriz := {}
If (!File(cArquivo))
Return
EndIf

// Abrindo o arquivo
FT_FUse(cArquivo)
FT_FGoTop()

// Capturando as linhas do arquivo
While (!FT_FEof())
If (Empty(cCab))
cCab := FT_FREADLN()
EndIf
If (lUsaCab)
AADD(aCSV,FT_FREADLN())
ElseIf (!lUsaCab) .and. (i > 0)
AADD(aCSV,FT_FREADLN())
EndIf
i++
FT_FSkip()
EndDo

FT_FUSE()

// Pegando o numero de colunas com base no cabecalho
For i := 1 to Len(cCab)
nPos := At(";",cCab)
If (nPos > 0)
nCols+= 1
cCab := SubStr(cCab,nPos+1,Len(cCab)-nPos)
EndIf
Next

// Definindo o tamanho da Matriz que recebera os dados
aMatriz := Array(Len(aCSV),nCols+1)

// Carregando os dados
For i := 1 to Len(aCSV)
cLinha := aCSV[i]
For x := 1 to nCols+1
nPos := At(";",cLinha)
If ( nPos > 0 )
aMatriz[i,x] := AllTrim(SubStr(cLinha,1,nPos-1))
cLinha := SubStr(cLinha,nPos+1,Len(cLinha)-nPos)
Else
aMatriz[i,x] := AllTrim(cLinha)
cLinha := ""
EndIf
Next x
Next i

Return*/