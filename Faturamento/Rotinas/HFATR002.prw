#include "topconn.ch"
#INCLUDE "PROTHEUS.CH"
#include "rwmake.ch"

// Retornos poss�veis do MessageBox
 #define IDOK			    1
 #define IDCANCEL		    2
 #define IDYES			    6
 #define IDNO			    7

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATR002  �Autor  �Bruno Parreira      � Data �  13/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio de pick-list de separacao.                        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATR002()                    

//��������������������������������������������������������������Ŀ
//� Define Variaveis                                             �
//����������������������������������������������������������������
Local cPerg     := "HFATR002"
Local nx   := 0

Private cDesc1 	:= "Este relatorio tem o objetivo de informar para os usu�rios do"
Private cDesc2 	:= "estoque, quais itens dever�o ser separados para os"
Private cDesc3 	:= "pr�-faturamentos liberados."
Private cString	:= "SZJ"
Private tamanho	:= "P"
Private cbCont,cabec1,cabec2,cbtxt
Private aOrd	:= {}//{"Endere�o"}
Private aReturn := {"Zebrado",1,"Administracao", 2, 2, 1, "",0 }
Private nomeprog:= "HFATR002",nLastKey := 0
Private li		:= 80, limite:=132, lRodape:=.F.
Private wnrel  	:= "HFATR002"
Private titulo 	:= "Mapa de Separa��o"
Private cSerIni := ""
Private cSerFin := ""
Private nLin 	:= 100
Private nCol 	:= 60
Private nPula 	:= 60
Private	nfim    := 2400
Private imprp	:= .F.
Private a_cols 	:= {50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000}

Private	oFont14	 := TFont():New("Arial",,14,,.f.,,,,,.f.)
Private oFont14n := TFont():New("Arial",,14,,.t.,,,,,.f.)
Private oFont17n := TFont():New("Arial",,17,,.t.,,,,,.f.)
Private oFont09  := TFont():New("Arial",,09,,.f.,,,,,.f.)
Private oFont09n := TFont():New("Arial",,09,,.t.,,,,,.f.)
Private oFont08  := TFont():New("Arial",,08,,.F.,,,,,.f.)
Private oFont10  := TFont():New("Arial",,10,,.f.,,,,,.f.)
Private oFont10n := TFont():New("Arial",,10,,.t.,,,,,.f.)
Private oFont12  := TFont():New("Arial",,12,,.f.,,,,,.f.)
Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)
Private oFont22n := TFont():New("Arial",,22,,.t.,,,,,.f.)
Private oFont25n := TFont():New("Arial",,25,,.t.,,,,,.f.)
Private oFont30n := TFont():New("Arial",,30,,.t.,,,,,.f.)
Private oFont40n := TFont():New("Arial",,40,,.t.,,,,,.f.)

Private lMarcar  := .F.
Private cTipos   := ""

Private nPagina  := 0

cbtxt   := SPACE(10)
cbcont  := 0
Li      := 80
m_pag   := 1
cabec2  := ""
cabec1  := ""

AjustaSX1(cPerg)

If !Pergunte(cPerg,.T.)
	Return
EndIf

cMvPar06 := AllTrim(mv_par06) 

If !Empty(cMvPar06)
	For nx := 1 to len(cMvPar06) step 3
		cAux := SubStr(cMvPar06,nx,3)
		If cAux <> '***'
			cTipos += "'"+cAux+"',"
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)

MontaTela()

Return

Static Function MontaTela()
Local cQuery := ""
Local _astru := {}

cQuery := "select ZJ_LOTDAP,ZJ_NUMPF,ZJ_PEDIDO,C5_EMISSAO,ZJ_CLIENTE,ZJ_LOJA,ZJ_DATA,A1_NOME,SUM(ZJ_QTDLIB) as ZJ_QTDLIB,C5_XPEDRAK, C5_CONDPAG, C5_XDATIMP, C5_XHORIMP "
cQuery += CRLF + "from "+RetSqlName("SZJ")+" SZJ "
cQuery += CRLF + "inner join "+RetSqlName("SA1")+" SA1 "
cQuery += CRLF + "on A1_COD = ZJ_CLIENTE "
cQuery += CRLF + "and A1_LOJA = ZJ_LOJA "
cQuery += CRLF + "and SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "inner join "+RetSqlName("SC5")+" SC5 "
cQuery += CRLF + "on C5_NUM = ZJ_PEDIDO "
cQuery += CRLF + "and SC5.D_E_L_E_T_ = '' "
If !Empty(cTipos)
	cQuery += CRLF + "and C5_TPPED IN ("+cTipos+") "
EndIf
cQuery += CRLF + "where SZJ.D_E_L_E_T_ = '' "
cQuery += CRLF + "and ZJ_PEDIDO between '"+mv_par01+"' and '"+mv_par02+"' "
cQuery += CRLF + "and ZJ_LOTDAP between '"+mv_par04+"' and '"+mv_par05+"' "
cQuery += CRLF + "and ZJ_DATA   between '"+DtoS(mv_par07)+"' and '"+DtoS(mv_par08)+"' "
If mv_par03 = 2
	cQuery += CRLF + "and ZJ_QTDSEP = 0 "
EndIf
If mv_par09 = 1
	cQuery += CRLF + "and ZJ_CONF IN ('L','S') "
Else
	cQuery += CRLF + "and ZJ_CONF = '' "
EndIf
cQuery += CRLF + "group by ZJ_LOTDAP,ZJ_NUMPF,ZJ_PEDIDO,C5_EMISSAO,ZJ_CLIENTE,ZJ_LOJA,ZJ_DATA,A1_NOME,C5_XPEDRAK, C5_CONDPAG, C5_XDATIMP, C5_XHORIMP"
cQuery += CRLF + "order by ZJ_LOTDAP,ZJ_NUMPF "

MemoWrite("HFATR002_MontaTela.txt",cQuery)

cQuery := ChangeQuery(cQuery)

	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)

//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"    ,"C",2,0})
AADD(_astru,{"ZJ_LOTDAP" ,TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2]})
AADD(_astru,{"PERCENTUAL" ,"C",5,0})
AADD(_astru,{"ZJ_NUMPF"  ,TAMSX3("ZJ_NUMPF"  )[3],TAMSX3("ZJ_NUMPF"  )[1],TAMSX3("ZJ_NUMPF"  )[2]})
AADD(_astru,{"ZJ_PEDIDO" ,TAMSX3("ZJ_PEDIDO" )[3],TAMSX3("ZJ_PEDIDO" )[1],TAMSX3("ZJ_PEDIDO" )[2]})
AADD(_astru,{"C5_EMISSAO",TAMSX3("C5_EMISSAO")[3],TAMSX3("C5_EMISSAO")[1],TAMSX3("C5_EMISSAO")[2]})
AADD(_astru,{"ZJ_CLIENTE",TAMSX3("ZJ_CLIENTE")[3],TAMSX3("ZJ_CLIENTE")[1],TAMSX3("ZJ_CLIENTE")[2]})
AADD(_astru,{"ZJ_LOJA"   ,TAMSX3("ZJ_LOJA"   )[3],TAMSX3("ZJ_LOJA"   )[1],TAMSX3("ZJ_LOJA"   )[2]})
AADD(_astru,{"A1_NOME"   ,TAMSX3("A1_NOME"   )[3],TAMSX3("A1_NOME"   )[1],TAMSX3("A1_NOME"   )[2]})
AADD(_astru,{"ZJ_QTDLIB" ,TAMSX3("ZJ_QTDLIB" )[3],TAMSX3("ZJ_QTDLIB" )[1],TAMSX3("ZJ_QTDLIB" )[2]})
AADD(_astru,{"ZJ_DATA"   ,TAMSX3("ZJ_DATA"   )[3],TAMSX3("ZJ_DATA"   )[1],TAMSX3("ZJ_DATA"   )[2]})
AADD(_astru,{"C5_XPEDRAK",TAMSX3("C5_XPEDRAK")[3],TAMSX3("C5_XPEDRAK")[1],TAMSX3("C5_XPEDRAK")[2]})
AADD(_astru,{"C5_CONDPAG",TAMSX3("C5_CONDPAG")[3],TAMSX3("C5_CONDPAG")[1],TAMSX3("C5_CONDPAG")[2]})
AADD(_astru,{"VALORTOTAL","C",15,0})
AADD(_astru,{"C5_XDATIMP",TAMSX3("C5_XDATIMP")[3],TAMSX3("C5_XDATIMP")[1],TAMSX3("C5_XDATIMP")[2]})
AADD(_astru,{"C5_XHORIMP",TAMSX3("C5_XHORIMP")[3],TAMSX3("C5_XHORIMP")[1],TAMSX3("C5_XHORIMP")[2]})

cArqTrab  := CriaTrab(_astru,.T.)

If Select("TRB") > 0
	TRB->(DbCloseArea())
EndIf

dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )
IndRegua("TRB",cArqTrab,"ZJ_LOTDAP+ZJ_NUMPF")
TRB->(dbClearIndex())
TRB->(dbSetIndex(cArqTrab + OrdBagExt()))

//Atribui a tabela temporaria ao alias TRB
Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		nPercAt := Percent(TMP->ZJ_NUMPF)
		nValtt  := CalcValor(TMP->ZJ_NUMPF) 
		DbSelectArea("TRB")
		RecLock("TRB",.T.)
		TRB->ZJ_LOTDAP  := TMP->ZJ_LOTDAP
		TRB->ZJ_NUMPF   := TMP->ZJ_NUMPF
		TRB->ZJ_PEDIDO  := TMP->ZJ_PEDIDO
		TRB->C5_EMISSAO := StoD(TMP->C5_EMISSAO)
		TRB->ZJ_CLIENTE := TMP->ZJ_CLIENTE
		TRB->ZJ_LOJA    := TMP->ZJ_LOJA
		TRB->A1_NOME    := TMP->A1_NOME
		TRB->ZJ_QTDLIB  := TMP->ZJ_QTDLIB
		TRB->ZJ_DATA    := StoD(TMP->ZJ_DATA)
		TRB->C5_XPEDRAK := TMP->C5_XPEDRAK
		TRB->C5_CONDPAG := TMP->C5_CONDPAG
		TRB->PERCENTUAL := AllTrim(Str(nPercAt))+"%"
		TRB->VALORTOTAL := Transform(nValtt,"@E 9,999,999.99") 
		TRB->C5_XDATIMP := StoD(TMP->C5_XDATIMP)
		TRB->C5_XHORIMP := TMP->C5_XHORIMP
		MsUnlock()
		
		TMP->(DbSkip())
	EndDo
Else
	MessageBox("Nenhum registro foi encontrado.","Aten��o",48)
	lRet := .F.
	LimpaTab()
	Return
EndIf

//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('TRB')        

//define as colunas para o browse
aColunas := {;
{"Lote DAP"   ,"ZJ_LOTDAP" ,TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2],X3Picture("ZJ_LOTDAP" )},;
{"%"          ,"PERCENTUAL","C"                    ,5                      ,0                      ,"@!"                   },;	
{"Num Pre-Fat","ZJ_NUMPF"  ,TAMSX3("ZJ_NUMPF"  )[3],TAMSX3("ZJ_NUMPF"  )[1],TAMSX3("ZJ_NUMPF"  )[2],X3Picture("ZJ_NUMPF"  )},;
{"Pedido"     ,"ZJ_PEDIDO" ,TAMSX3("ZJ_PEDIDO" )[3],TAMSX3("ZJ_PEDIDO" )[1],TAMSX3("ZJ_PEDIDO" )[2],X3Picture("ZJ_PEDIDO" )},;
{"Emiss�o"    ,"C5_EMISSAO",TAMSX3("C5_EMISSAO")[3],TAMSX3("C5_EMISSAO")[1],TAMSX3("C5_EMISSAO")[2],X3Picture("C5_EMISSAO")},;
{"Cliente"    ,"ZJ_CLIENTE",TAMSX3("ZJ_CLIENTE")[3],TAMSX3("ZJ_CLIENTE")[1],TAMSX3("ZJ_CLIENTE")[2],X3Picture("ZJ_CLIENTE")},;
{"Loja"       ,"ZJ_LOJA"   ,TAMSX3("ZJ_LOJA"   )[3],TAMSX3("ZJ_LOJA"   )[1],TAMSX3("ZJ_LOJA"   )[2],X3Picture("ZJ_LOJA"   )},;
{"Nome"       ,"A1_NOME"   ,TAMSX3("A1_NOME"   )[3],TAMSX3("A1_NOME"   )[1],TAMSX3("A1_NOME"   )[2],X3Picture("A1_NOME"   )},;
{"Qtd Pre-Fat","ZJ_QTDLIB" ,TAMSX3("ZJ_QTDLIB" )[3],TAMSX3("ZJ_QTDLIB" )[1],TAMSX3("ZJ_QTDLIB" )[2],X3Picture("ZJ_QTDLIB" )},;
{"Dt. Pre-Fat","ZJ_DATA"   ,TAMSX3("ZJ_DATA"   )[3],TAMSX3("ZJ_DATA"   )[1],TAMSX3("ZJ_DATA"   )[2],X3Picture("ZJ_DATA"   )},;
{"Codigo WEB" ,"C5_XPEDRAK",TAMSX3("C5_XPEDRAK")[3],TAMSX3("C5_XPEDRAK")[1],TAMSX3("C5_XPEDRAK")[2],X3Picture("C5_XPEDRAK")},;
{"Cond. PGTO" ,"C5_CONDPAG",TAMSX3("C5_CONDPAG")[3],TAMSX3("C5_CONDPAG")[1],TAMSX3("C5_CONDPAG")[2],X3Picture("C5_CONDPAG")},;
{"Valor Total","VALORTOTAL" ,"C"                    ,15                      ,0                      ,""                },;
{"Data Importa��o" ,"C5_XDATIMP",TAMSX3("C5_XDATIMP")[3],TAMSX3("C5_XDATIMP")[1],TAMSX3("C5_XDATIMP")[2],X3Picture("C5_XDATIMP")},;
{"Hora Importa��o" ,"C5_XHORIMP",TAMSX3("C5_XHORIMP")[3],TAMSX3("C5_XHORIMP")[1],TAMSX3("C5_XHORIMP")[2],X3Picture("C5_XHORIMP")}}

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.F.)
oMark:SetDescription('Mapa de Separa��o')
oMark:SetFieldMark('MK_OK')
                              
//oMark:AddButton("Gerar Pre-Faturamento","MsgRun('Gerando pr�-faturamento...','DAP',{|| U_HFAT02PROX(oMark:Mark())})",,2,0)
oMark:AddButton("Imprimir e Liberar","U_HFTR002REL(oMark,1)",,2,0)
oMark:AddButton("Apenas Imprimir","U_HFTR002REL(oMark,2)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_HFTR02INV(oMark:Mark())",,2,0)
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || oMark:AllMark()})

oMark:SetTemporary()

//Ativando a janela
oMark:Activate()

LimpaTab()

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFTR002REL�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Funcao para controlar a transicao de uma tela para outra.   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

User Function HFTR002REL(oMark,nOpc) 
Local aAreaTRB := GetArea()
Local aPed     := {}
Local cPerg    := "HFATR002"

Private cResumo  := .F.

nRet3 := MessageBox("Confirma a impress�o dos pr�-faturamentos selecionados?","Confirma��o",4)
If nRet3 == 7//!MessageBox("Confirma a impress�o dos pr�-faturamentos selecionados?","Confirma��o",4)
	RestArea(aAreaTRB)
	lContinua := .F.
	Return
Else
	nRet4 := MessageBox("Deseja Imprimir com Resumo das Refer�ncias","Confirma��o",4)
    If nRet4 == 6
		cResumo := .T.
	EndIf		
EndIf

aPed := GeraArray(oMark)

wnrel:=SetPrint(cString,wnrel,cPerg,@Titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho)

If nLastKey == 27
	Return
Endif

//SetDefault(aReturn,cString)

If nLastKey == 27
	Return
Endif

oPrn := TMSPrinter():New()
oPrn:SetPortrait()

If nOpc = 1
	Processa({||LiberaPF(aPed)},Titulo,"Liberando para confer�ncia, aguarde...")
EndIf

Processa({||ImpRel(aPed,oMark)},Titulo,"Imprimindo, aguarde...")

RestArea(aAreaTRB)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GeraArray �Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta array com pedidos para pre-faturamento.               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function LiberaPF(aPedido) 
Local nx := 0 

ProcRegua(Len(aPedido))

For nx := 1 to Len(aPedido)
	IncProc()
	DbSelectArea("SZJ")
	DbSetOrder(1)
	If Dbseek(xFilial("SZJ")+aPedido[nx][2])
		If Empty(SZJ->ZJ_CONF)
			While !EOF() .and. SZJ->ZJ_NUMPF = aPedido[nx][2]
				RecLock("SZJ",.F.)
				SZJ->ZJ_CONF = "L"
				MsUnlock()
				DbSkip()
			End
			
			//ATUSTATUS(aPedido[nx][1])
			
		Else
			MessageBox("Pedido "+SZJ->ZJ_NUMPF+" j� liberado!","Aviso",0)
		Endif
	EndIf
Next

Return

Static Function ATUSTATUS(cPedSta)
Local cQuery  := ""
Local cPedRak := ""
Local cLog    := ""
Local oWsPedido

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

oWsPedido := WSPedido():New()

cLog += CRLF + "In�cio do processamento..."

DbSelectArea("SC5")
DbSetOrder(1)
If DbSeek(xFilial("SC5")+cPedSta)
	If Empty(SC5->C5_XSTATUS) .And. SC5->C5_ORIGEM = "B2B"
		cPedRak := AllTrim(SC5->C5_XPEDRAK)
		If !Empty(cPedRak) 
			cLog += CRLF + "Processando pedido Rakuten: "+cPedRak
			
			cLog += CRLF + "Par�metros: 0,"+cPedRak+",,134,,,0,"+cChvA1+","+cChvA2+")"
			
			If oWsPedido:AlterarStatus(0,Val(cPedRak),"",134,"","",0,"",cChvA1,cChvA2)
				cWsRet := oWsPedido:oWSAlterarStatusResult:cDescricao
				
				cLog += CRLF + "Retorno da Funcao AlterarStatus: "+cWsRet
				If "SUCESSO" $ UPPER(cWsRet)
					RecLock("SC5",.F.)
					SC5->C5_XSTATUS := "A" //Status Alterado
					MsUnlock()
					cLog += CRLF + "Status atualizado com sucesso para o pedido Protheus: "+cPedSta
				EndIf
			Else
				cLog += CRLF + "Erro na Funcao AlterarStatus: "+GetWSCError()
			EndIf
		Else
			cLog += CRLF + "Campo com o pedido Rakuten (C5_XPEDRAK) vazio. Pedido Protheus: "+cPedSta
		EndIf
	EndIf
EndIf

cLog += CRLF + "T�rmino do processamento..."

MemoWrite("\log_rakuten\B2B\"+cDtTime+"_ATU-STATUS-PED-"+cPedSta+".txt",cLog)	
	
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GeraArray �Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta array com pedidos para pre-faturamento.               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function GeraArray(oMark) 
Local lMark    := .F.
Local aArray   := {}

TRB->(DbGoTop())
While TRB->(!Eof())
    If !Empty(TRB->MK_OK)  	
		lMark := .T.
		aAdd(aArray,{TRB->ZJ_PEDIDO,TRB->ZJ_NUMPF})
    Endif
    TRB->(DbSkip())
EndDo

If !lMark
	MessageBox("Nenhum item foi selecionado.","Aten��o",48)
EndIf

Return aArray

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �LimpaTab  �Autor  �Bruno Parreira      � Data � 14/03/2017  ���
�������������������������������������������������������������������������͹��
���Desc      �Limpa os arquivos e alias teporarios que estao abertos.     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function LimpaTab()

//Limpar o arquivo tempor�rio
If !Empty(cArqTrab)
    Ferase(cArqTrab+GetDBExtension())
    Ferase(cArqTrab+OrdBagExt())
    cArqTrab := ""
    TRB->(DbCloseArea())
Endif

TMP->(DbCloseArea())

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFTR02INV �Autor  �Bruno Parreira      � Data � 06/01/2017  ���
�������������������������������������������������������������������������͹��
���Desc      �Marca ou desmarca todos os itens da FWMarkBrowse.           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFTR02INV(cMarca)
Local aAreaTRB  := TRB->(GetArea())

lMarcar := !lMarcar 
 
dbSelectArea("TRB")
TRB->(dbGoTop())
While !TRB->(Eof())
    RecLock("TRB",.F.)
    TRB->MK_OK := IIf(lMarcar,cMarca,'  ')
    MsUnlock()
    TRB->(dbSkip())
EndDo

RestArea( aAreaTRB )

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � ImpRel   � Autor � Bruno Parreira        � Data � 07/02/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chamada do Relatorio                                       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � HFATR002	                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpRel(aPedido,oMark)
Local cQuery  := ""
Local _cQuery3 := ""
Local _cQuery2 := ""
Local cAmzPic := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0"))
Local nx      := 0
Local cPedido := ""

If Len(aPedido) > 0
	For nx := 1 to Len(aPedido)
		cPedido += "'"+aPedido[nx][1]+"',"
	Next
	
	cPedido := SubStr(cPedido,1,Len(cPedido)-1)
EndIf

// AND ZJ_ITEMGRD = C6_ITEMGRD

cQuery := "select ZJ_LOTDAP, ZJ_NUMPF, ZJ_PEDIDO, ZJ_ITEM, ZJ_PRODUTO, B1_DESC, B1_UM, ZJ_LOCALIZ, BE_XCODEST, BE_XSEQUEN, "+CRLF 
cQuery += "		SUM(ZJ_QTDLIB) as ZJ_QTDLIB, C5_PRAZO, C5_POLCOM, C5_TPPED, C5_CONDPAG, C5_XMICRDE, C5_XPEDRAK, C5_XPEDWEB, C5_XDATIMP, C5_XHORIMP, "+CRLF
cQuery += "		C5_CLIENTE, C5_LOJACLI, C5_TRANSP, C5_XINSTRU, C5_XPEDMIL, C5_XPEDVIN, C5_XPRZVIN, C5_FECENT, C5_FRETE, C5_DESCONT, C5_EMISSAO, "+CRLF
cQuery += "		C6_PRCVEN, C6_VALOR, C5_DESPESA, A1_NOME, A1_END, A1_EST, A1_MUN, A1_BAIRRO, A1_CEP, C6_XITPRES, "+CRLF
cQuery += "		LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(C6_XPRESEN, CHAR(10), ''), CHAR(13), ' '), CHAR(9), ''))) AS C6_XPRESEN "+CRLF
cQuery += "from "+RetSqlName("SZJ")+" SZJ WITH (NOLOCK) "+CRLF
cQuery += "		inner join "+RetSqlName("SC5")+" SC5 WITH (NOLOCK) on C5_NUM = ZJ_PEDIDO and SC5.D_E_L_E_T_ = '' "+CRLF
cQuery += "		inner join "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) on C6_NUM = ZJ_PEDIDO and ZJ_PRODUTO=C6_PRODUTO "+CRLF 
cQuery += "							and C6_ITEM = ZJ_ITEM  and SC6.D_E_L_E_T_ = '' "+CRLF
cQuery += "		inner join "+RetSqlName("SA1")+" SA1 WITH (NOLOCK) on A1_COD = C5_CLIENTE and A1_LOJA = C5_LOJACLI and SA1.D_E_L_E_T_ = '' "+CRLF
cQuery += "		inner join "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) on B1_COD = ZJ_PRODUTO and SB1.D_E_L_E_T_ = '' "+CRLF
cQuery += "		inner join "+RetSqlName("SBE")+" SBE WITH (NOLOCK) on BE_LOCAL = ZJ_LOCAL and BE_LOCALIZ = ZJ_LOCALIZ and SBE.D_E_L_E_T_ = '' "+CRLF
cQuery += "where SZJ.D_E_L_E_T_ = '' "+CRLF
cQuery += "		and ZJ_PEDIDO IN ("+cPedido+") "+CRLF
cQuery += "		and ZJ_LOTDAP between '"+mv_par04+"' and '"+mv_par05+"' "+CRLF
If mv_par03 = 2
	cQuery += "		and ZJ_QTDSEP = 0 "+CRLF
EndIf
//If mv_par09 = 1
//	cQuery += "		and ZJ_CONF IN ('L','S') "+CRLF
//Else
//	cQuery += "		and ZJ_CONF = '' "+CRLF
//EndIf
cQuery += "group by ZJ_LOTDAP, ZJ_NUMPF, ZJ_PEDIDO, ZJ_ITEM, ZJ_PRODUTO, B1_DESC, B1_UM, ZJ_LOCALIZ, BE_XCODEST, BE_XSEQUEN, "+CRLF
cQuery += "			C5_PRAZO, C5_POLCOM, C5_TPPED, C5_CONDPAG, C5_XMICRDE, C5_XPEDRAK, C5_XPEDWEB, C5_XDATIMP, C5_XHORIMP, C5_CLIENTE, C5_LOJACLI, C5_TRANSP, "+CRLF
cQuery += "			C5_XINSTRU, C5_XPEDMIL, C5_XPEDVIN, C5_XPRZVIN, C5_FECENT, C5_FRETE, C5_DESCONT, C5_EMISSAO, C6_PRCVEN, C6_VALOR, C5_DESPESA, "+CRLF
cQuery += "			A1_NOME, A1_END, A1_EST, A1_MUN, A1_BAIRRO, A1_CEP, C6_XITPRES, C6_XPRESEN "+CRLF
cQuery += "order by ZJ_LOTDAP, ZJ_NUMPF, BE_XCODEST, BE_XSEQUEN, ZJ_LOCALIZ, ZJ_PEDIDO, ZJ_ITEM "+CRLF

memowrite("HFATR002.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

If Select("QRY") > 0
	QRY->(DbCloseArea())
EndIf

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"QRY", .F., .T.)

DbSelectArea("QRY")
DbGoTop()
n_cont := 0

If QRY->(!EOF())

	While QRY->(!EOF())
		n_cont++
		QRY->(DbSkip())
	EndDo
	DbGoTop()
	ProcRegua(n_cont)
	NPAG := 1
Else
	MessageBox("N�o h� dados a serem impressos.","Aten��o",48)
	Return	
EndIf

nTotQtd := 0
nTotVlr := 0
cNumPF  := ""
cCodEst := ""
//aColItens := {080,300,1500,1700,1900,2150}
aColItens := {080,250,450,1900,2050,2200}
aColItens2 :={080,300,500,700,1900,2150}


lEst := .T.

While QRY->(!Eof())
	
	IncProc()
	
	If cNumPF <> QRY->ZJ_NUMPF .Or. Li > 40 //.or. nlin > 30
		If cNumPF <> QRY->ZJ_NUMPF
			nPagina := 1
			CabecSep(nPagina)
		Else
			If Li > 45
				nPagina++
				Cabec2(nPagina)
			EndIf		
		EndIf
		//CabecSep(nPagina)
		Li := 0
		lEst := .T.
	EndIf
	
	if nLin > 3040
		nPagina++
		Cabec2(nPagina)
		Li := 0
		lEst := .T. 
	endif
	
	If cCodEst <> QRY->BE_XCODEST .Or. lEst
		oPrn:Say(nLin,aColItens[1],"Esta��o: "+QRY->BE_XCODEST,oFont09n,100)
		oPrn:Box(nLin-10,50,nLin+50,nfim)
		nLin += nPula
		lEst := .F.
	EndIf

	cEnderec := QRY->ZJ_LOCALIZ
	cProduto := QRY->ZJ_PRODUTO+" - "+SubStr(QRY->B1_DESC,1,65)
	nQuant   := QRY->ZJ_QTDLIB
	nPrcVen  := QRY->C6_PRCVEN
	nPrcTot  := QRY->ZJ_QTDLIB*QRY->C6_PRCVEN//QRY->C6_VALOR
	nPeso    := 0
	
	oPrn:Say(nLin,aColItens[1],cEnderec,oFont10n,100)
	oPrn:Say(nLin,aColItens[2],Transform(nQuant,"@E 9,999,999"),oFont10n,100)
	oPrn:Say(nLin,aColItens[3],cProduto,oFont08,100)
	oPrn:Say(nLin,aColItens[4],Transform(nPrcVen,"@E 9,999,999.99"),oFont09,100)
	oPrn:Say(nLin,aColItens[5],Transform(nPrcTot,"@E 9,999,999.99"),oFont09,100)
	oPrn:Say(nLin,aColItens[6],Iif(alltrim(QRY->C6_XITPRES)="S","Presente"," "),oFont09,100)
	
	oPrn:Box(nLin,2360,nLin+40,nfim)
	
	nLin += nPula
	Li++

	If Li > 40 //.or. nlin > 30
		nPagina++
		Cabec2(nPagina)
		Li := 0
		lEst := .T.
	EndIf
	
	cNumPF  := QRY->ZJ_NUMPF
	cCodEst := QRY->BE_XCODEST
	nDescont := QRY->C5_DESCONT
	nFrete := QRY->C5_FRETE
	nTotQtd += QRY->ZJ_QTDLIB
	nTotVlr += nPrcTot//QRY->C6_VALOR
	//nPresent := QRY->C5_DESPESA
	
	QRY->(DbSkip())
	
	//nTotVal := (nTotVlr + nFrete + nPresent) - nDescont
	nTotVal := (nTotVlr + nFrete) - nDescont
	
	If cNumPF <> QRY->ZJ_NUMPF //.Or. Li > 30
	
		If Li > 45 //.or. nlin > 30
			nPagina++
			Cabec2(nPagina)
			Li := 0
			lEst := .T.
		EndIf
	
		oPrn:Say(nLin,aColItens[3]-150,"Total:",oFont09n,100)
		oPrn:Say(nLin,aColItens[3],Transform(nTotQtd,"@E 9,999,999"),oFont09n,100)
		oPrn:Say(nLin,aColItens[5],Transform(nTotVal,"@E 9,999,999.99"),oFont09n,100)
		oPrn:Box(nLin-10,50,nLin+50,nfim)
		nTotQtd := 0
		nTotVlr := 0
		
		
		_cQuery3 := " SELECT LEFT(ZJ_PRODUTO,8) AS REF , SUM(ZJ_QTDLIB) AS QTDE, ZJ_LOTDAP, ZJ_NUMPF, ZJ_PEDIDO,C5_DESPESA, " 
		_cQuery3 += " C5_PRAZO, C5_POLCOM, C5_TPPED, C5_CONDPAG, C5_XMICRDE, C5_XPEDRAK, C5_XPEDWEB, "
		_cQuery3 += " C5_CLIENTE, C5_LOJACLI, C5_TRANSP, C5_XINSTRU, C5_XPEDMIL, C5_XPEDVIN, C5_XPRZVIN, C5_FECENT, C5_FRETE, C5_DESCONT, C5_EMISSAO, " 
		_cQuery3 += " A1_NOME, A1_END, A1_EST, A1_MUN, A1_BAIRRO, A1_CEP, C6_XITPRES, "
		_cQuery3 += " LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(C6_XPRESEN, CHAR(10), ''), CHAR(13), ' '), CHAR(9), ''))) AS C6_XPRESEN  "
		_cQuery3 += " FROM "+RetSqlname("SZJ")+"  SZJ WITH (NOLOCK) " 
		_cQuery3 += " inner join "+RetSqlname("SC5")+" SC5 WITH (NOLOCK) on C5_NUM = ZJ_PEDIDO and SC5.D_E_L_E_T_ = '' "
		_cQuery3 += " inner join "+RetSqlname("SC6")+" SC6 WITH (NOLOCK) on C6_NUM = ZJ_PEDIDO and ZJ_PRODUTO=C6_PRODUTO  "
		_cQuery3 += " and C6_ITEM = ZJ_ITEM  and SC6.D_E_L_E_T_ = '' "
		_cQuery3 += " inner join "+RetSqlname("SA1")+" SA1 WITH (NOLOCK) on A1_COD = C5_CLIENTE and A1_LOJA = C5_LOJACLI and SA1.D_E_L_E_T_ = '' "
		_cQuery3 += " inner join "+RetSqlname("SB1")+" SB1 WITH (NOLOCK) on B1_COD = ZJ_PRODUTO and SB1.D_E_L_E_T_ = '' "
		_cQuery3 += " inner join "+RetSqlname("SBE")+" SBE WITH (NOLOCK) on BE_LOCAL = '"+cAmzPic+"' and BE_LOCALIZ = ZJ_LOCALIZ and SBE.D_E_L_E_T_ = '' "
		_cQuery3 += " WHERE SZJ.D_E_L_E_T_ = '' "
		_cQuery3 += " and ZJ_NUMPF = '"+cNumPF+"' "
		_cQuery3 += " GROUP BY ZJ_LOTDAP, ZJ_NUMPF, ZJ_PEDIDO, LEFT(ZJ_PRODUTO,8), "
		_cQuery3 += " C5_PRAZO, C5_POLCOM, C5_TPPED, C5_CONDPAG, C5_XMICRDE, C5_XPEDRAK, C5_XPEDWEB, C5_CLIENTE, C5_LOJACLI, C5_TRANSP, "
		_cQuery3 += " C5_XINSTRU, C5_XPEDMIL, C5_XPEDVIN, C5_XPRZVIN, C5_FECENT, C5_FRETE, C5_DESCONT, C5_EMISSAO, " 
		_cQuery3 += " A1_NOME, A1_END, A1_EST, A1_MUN, A1_BAIRRO, A1_CEP, C6_XITPRES,C6_XPRESEN,C5_DESPESA  "
		
		
		If Select("TRT") > 0
			TRT->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_cQuery3),"TRT",.T.,.T.)
	  
		DbSelectArea("TRT")
		
		nLin += nPula
		nLin += nPula
		nLin += nPula
					
		WHILE TRT->(!EOF())
		
			If Li > 45 //.or. nlin > 30
				nPagina++
				Cabec2(nPagina)
				Li := 0
				lEst := .T.
				
				//QRY->(DbCloseArea())
			EndIf
	
			IF cResumo
				IF QRY->C5_TPPED <> "016" 	
					oPrn:Say(nLin,aColItens2[1],"Referencias",oFont09n,100)
					oPrn:Say(nLin,aColItens2[3],"Quantidade",oFont09n,100)
					oPrn:Box(nLin-10,50,nLin+50,nfim)
							
					oPrn:Say(nLin,aColItens2[2],TRT->REF,oFont09n,100)
					oPrn:Say(nLin,aColItens2[4],Transform(TRT->QTDE,"@E 9,999,999"),oFont09n,100)
					
					nLin += nPula
					Li++
				EndIf

				_cQuery2 := " SELECT LEFT(ZJ_PRODUTO,11) AS REF_COR, SUM(ZJ_QTDLIB) AS QTDE " 
				_cQuery2 += " FROM "+RetSqlName("SZJ")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZJ_NUMPF = '"+cNumPF+"' AND LEFT(ZJ_PRODUTO,8) = '"+TRT->REF+"'  " 
				_cQuery2 += " GROUP BY LEFT(ZJ_PRODUTO,11)  "		
				
				If Select("TRP") > 0
					TRP->(DbCloseArea())
				EndIf
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,_cQuery2),"TRP",.T.,.T.)
			
				DbSelectArea("TRP")
				
				WHILE TRP->(!EOF())			
					oPrn:Say(nLin,aColItens2[2],TRP->REF_COR,oFont08,100)
					oPrn:Say(nLin,aColItens2[4],Transform(TRP->QTDE,"@E 9,999,999"),oFont08,100)
					nLin += nPula
					Li++			
					TRP->(DbSkip())
				END

			EndIf	
			
			TRT->(DbSkip())
			
		END
	 	
	EndIf
  	
	/*
	If nLin > nPula*50
		roda(cbcont,cbtxt,tamanho)
		NPAG++
	EndIf*/
	
EndDo

/*
If len(a_dados) > 0
	IMPRIMET()
EndIf
*/
/*If Li != 80
	roda(cbcont,cbtxt,tamanho)
EndIf*/

MS_FLUSH()

If	( aReturn[5] == 1 ) //1-Disco, 2-Impressora
	oPrn:Preview()
Else
	oPrn:Setup()
	oPrn:Print()
EndIf

QRY->(DbCloseArea())
oMark:DeActivate()

Return

//TODO Gerar o percentual de atendiento no Mapa. Ronaldo Pereira 21/03/19
Static Function Percent(cPrefat)
Local cQry     := ""
Local nPercent := 0
Local nQtdPre  := 0
Local nQPedTot := 0 

	cQry := "SELECT T.ZJ_NUMPF, T.QTDE_PRE,T.QTDE_PEDIDO,ROUND(((T.QTDE_PRE / T.QTDE_PEDIDO) * 100),2) AS PERC "
	cQry += "FROM (SELECT ZJ_NUMPF, SUM(ZJ_QTDLIB) AS QTDE_PRE, " 
	cQry += "     (SELECT SUM(C6_QTDVEN) FROM SC6010 (NOLOCK) AS C6 WHERE C6.C6_NUM = ZJ.ZJ_PEDIDO AND C6.D_E_L_E_T_ = '') AS QTDE_PEDIDO "
	cQry += "FROM SZJ010 (NOLOCK) AS ZJ "
	cQry += "WHERE ZJ.D_E_L_E_T_ = '' AND ZJ_NUMPF = '"+cPrefat+"' GROUP BY ZJ_PEDIDO, ZJ_NUMPF) T "
	
	If select ("TPE") > 0
		TPE->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TPE",.T.,.T.)
	  
	DbSelectArea("TPE")

	nPercent := TPE->PERC

Return nPercent

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �CabecSep  � Autor � Bruno Parreira        � Data � 07/02/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Imprime o cabecalho do relatorio                           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � HFATR002	                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CabecSep(nPag)
Local aArea := GetArea()
Local nx    := 0 
nLin	:= 100
nCol	:= 60
nPula	:= 60
nEsp    := 350
nPosTit := 60

aColCab := {80,850,1600,2050}

aTit    := {"Endere�o","    Qtde","Produto","Pre�o","Vl. Total","Presente"}
aColTit := AClone(aColItens)

oPrn:EndPage()

oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
oPrn:Say(nLin,aColCab[4]+150,"P�g.: "+AllTrim(Str(nPag)),oFont09,100)
nLin += nPula

oPrn:Say(nLin,aColCab[4]-50,dtoc(date())+" - "+Time(),oFont10,100)

IF QRY->C5_TPPED $ GETMV("HP_TPEDPRC")
	oPrn:Say(nLin,aColCab[2],"PEDIDO C/ PRE�O",oFont22n,100)
EndIf

IF QRY->C5_TPPED = "016" .And. QRY->C5_CONDPAG = "C61"
	oPrn:Say(nLin,aColCab[2],"TROCA",oFont25n,100)
EndIf

nLin += nPula*2

oPrn:Say(nLin,aColCab[1],"Pr�-Faturamento:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->ZJ_NUMPF,oFont10,100)
oPrn:Say(nLin,aColCab[2],"Pedido Web:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,AllTrim(If(!Empty(QRY->C5_XPEDWEB),QRY->C5_XPEDWEB,QRY->C5_XPEDRAK)),oFont10,100)
oPrn:Say(nLin,aColCab[3],"Pedido Protheus:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,QRY->ZJ_PEDIDO,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Lote DAP:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->ZJ_LOTDAP,oFont10,100)
oPrn:Say(nLin,aColCab[2],"Data Integra��o:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,DtoC(StoD(QRY->C5_XDATIMP))+" - "+QRY->C5_XHORIMP,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Ped Vinc./Prazo:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,IIF(!Empty(QRY->C5_XPEDVIN),(QRY->C5_XPEDVIN + " / " + AllTrim(Str(QRY->C5_XPRZVIN))),""),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cond. Pagamento:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,GetAdvFVal("SE4","E4_DESCRI",xFilial("SE4")+QRY->C5_CONDPAG,1,""),oFont10,100)
oPrn:Say(nLin,aColCab[2],"Tipo Prazo:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,GetAdvFVal("SZ6","Z6_DESC",xFilial("SZ6")+QRY->C5_POLCOM+QRY->C5_PRAZO,1,""),oFont10,100)
oPrn:Say(nLin,aColCab[3],"Tipo Pedido:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,GetAdvFVal("SZ4","Z4_DESC",xFilial("SZ4")+QRY->C5_POLCOM+QRY->C5_TPPED,1,""),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cliente:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_CLIENTE+"/"+QRY->C5_LOJACLI+" - "+SubStr(QRY->A1_NOME,1,35),oFont10,100)
oPrn:Say(nLin,aColCab[3],"CEP:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,Transform(QRY->A1_CEP,"@R 99999-999"),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Endere�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->A1_END,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Data Emiss�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,DtoC(StoD(QRY->C5_EMISSAO)),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cidade:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->A1_MUN,oFont10,100)
oPrn:Say(nLin,aColCab[2],"UF:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,QRY->A1_EST,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Bairro:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,QRY->A1_BAIRRO,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Macro Regi�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_XMICRDE,oFont10,100)

IF QRY->C5_TPPED <> "016"
	If QRY->A1_EST = "DF" .OR. QRY->C5_CLIENTE $ GETMV("HP_AUDCLI")
		oPrn:Say(nLin,aColCab[2]+200,"( AUDITAR )",oFont17n,100)
	EndIf
EndIf

nLin += nPula
oPrn:Say(nLin,aColCab[1],"Frete:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,Transform(QRY->C5_FRETE,"@E 9,999,999.99"),oFont10,100)
oPrn:Say(nLin,aColCab[2],"Data Entrega:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,DtoC(StoD(QRY->C5_FECENT)),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Transportador:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_TRANSP+" - "+GetAdvFVal("SA4","A4_NOME",xFilial("SA4")+QRY->C5_TRANSP,1,""),oFont10,100)
nLin += nPula
					
	IF QRY->C5_TPPED = "016"
		nPerc := Percent(QRY->ZJ_NUMPF)
		oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - VIRTUAL",oFont30n,100)
	ElseIf QRY->C5_TPPED = "133"
		nPerc := Percent(QRY->ZJ_NUMPF)
		oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - MAGALU",oFont30n,100)
	ElseIf QRY->C5_TPPED = "143"
		nPerc := Percent(QRY->ZJ_NUMPF)
		oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - ZZMALL",oFont30n,100)	
	ElseIf QRY->C5_TPPED = "144"
		nPerc := Percent(QRY->ZJ_NUMPF)
		oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - APP-HR",oFont30n,100)
	ElseIf QRY->C5_TPPED = "147"
		nPerc := Percent(QRY->ZJ_NUMPF)
		oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - APP-HL",oFont30n,100)	
	ElseIf QRY->C5_TPPED = "150"
		nPerc := Percent(QRY->ZJ_NUMPF)
		oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - BLOW",oFont30n,100)		
	ElseIf QRY->C5_TPPED = "151"
		nPerc := Percent(QRY->ZJ_NUMPF)
		oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - BABADOTOP",oFont30n,100)	
	ElseIf QRY->C5_TPPED = "152"
		nPerc := Percent(QRY->ZJ_NUMPF)
		oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - DECATHLON",oFont30n,100)				
	ElseIf QRY->C5_TPPED $ GETMV("B2W_TPPED")
		nPerc := Percent(QRY->ZJ_NUMPF)
		oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - B2W",oFont30n,100)			
	ElseIf QRY->C5_TPPED <> "004"
		DbSelectArea("SC6")
		DbSetOrder(1)
		IF DbSeek(xFilial("SC6")+QRY->ZJ_PEDIDO)
		nQtdEntr := 0
			While SC6->(!EOF()) .And. SC6->C6_NUM = QRY->ZJ_PEDIDO
				nQtdEntr += SC6->C6_QTDENT
				SC6->(DbSkip())
			EndDo
			If nQtdEntr > 0 .And. SubStr(QRY->ZJ_NUMPF,7,2) <> "01"
				nPerc := Percent(QRY->ZJ_NUMPF)
				oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"% - SALDO",oFont30n,100)
			Else
				nPerc := Percent(QRY->ZJ_NUMPF)
				oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"%",oFont30n,100)
		    EndIf
	    EndIf
	EndIf

oPrn:Say(nLin,aColCab[1],"Presente:",oFont10n,100)

oPrn:Say(nLin,aColCab[1]+nEsp,Transform(QRY->C5_DESPESA,"@E 9,999,999.99"),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Desconto:",oFont10n,100) //NEW
oPrn:Say(nLin,aColCab[1]+nEsp,Transform(QRY->C5_DESCONT,"@E 9,999,999.99"),oFont10,100) //NEW

MSBAR('CODE128',5.8,15.0,QRY->ZJ_NUMPF,oPrn,.F.,,.T.,0.040,1.1,,,,.F.)
nLin += nPula
nLin += nPula
oPrn:Say(nLin-150,aColCab[4]-250,QRY->ZJ_NUMPF,oFont14n,100)
nLin += nPula

nLimite := 130

cObs := AllTrim(QRY->C5_XINSTRU)
If !Empty(cObs)
	oPrn:Say(nLin,aColCab[1],"Instru��es:",oFont10n,100)
	cAux := ""
	If Len(cObs) > nLimite
		nDe  := 1
		nAte := nLimite
		While nDe < Len(cObs)
			cAux := SubStr(cObs,nDe,nAte)
			oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10,100)
			nLin += nPula
			nDe  := nDe+nLimite
			nAte := nAte+nLimite
		EndDo
	Else
		oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10,100)
		nLin += nPula	
	EndIf
EndIf

cObs := Alltrim(GetAdvFVal("SC5","C5_XOBSINT",xFilial("SC5")+QRY->ZJ_PEDIDO,1,""))

/*For x:=1 to len(cObs)
    cString:=alltrim(cObs)
    If AT(chr(13)+chr(10),cString) > 0
        cString:=STRTRAN(cString,chr(13)+chr(10),' | ')
    Endif
    cObs:=cString
Next x*/

aObs := {}

If !Empty(cObs)
	aObs := U_MemoToAr(cObs,90,"",.T.)
	
	If Len(aObs) > 0
		oPrn:Say(nLin,aColCab[1],"Observa��o:",oFont10n,100)
		For nx := 1 to Len(aObs)
			oPrn:Say(nLin,aColCab[1]+nEsp,aObs[nx],oFont10,100)
        	nLin += nPula
		Next
	EndIf
	
	/*nLinhas := MLCount(cObs,90)
	For nX:= 1 To nLinhas
        cTxtLinha := MemoLine(cObs,90,nX)
        If !Empty(cTxtLinha)
        	oPrn:Say(nLin,aColCab[1]+nEsp,cTxtLinha,oFont10,100)
        	nLin += nPula
        EndIf
	Next nX*/
	
	/*cAux := ""
	If Len(cObs) > nLimite
		nDe  := 1
		nAte := nLimite
		While nDe < Len(cObs)
			cAux := SubStr(cObs,nDe,nAte)
			oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10,100)
			nLin += nPula
			nDe  := nDe+nLimite
			nAte := nAte+nLimite
		EndDo
	Else
		oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10,100)
		nLin += nPula	
	EndIf*/		
EndIf

DbSelectArea("SC6")
DbSetOrder(1)
If DbSeek(xFilial("SC6")+QRY->ZJ_PEDIDO)
	lPre := .T.
	While SC6->(!EOF()) .And. SC6->C6_NUM = QRY->ZJ_PEDIDO
		If !Empty(SC6->C6_XPRESEN)
			cObs := SC6->C6_PRODUTO+" - "+AllTrim(SC6->C6_XPRESEN)
			Memowrite("HFATR002_cObs.txt",cObs)
			If lPre
				oPrn:Say(nLin,aColCab[1],"Msg. Presente:",oFont10n,100)
				lPre := .F.
			EndIf
			cAux := ""
			If Len(cObs) > nLimite
				nDe  := 1
				nAte := nLimite
				While nDe < Len(cObs)
					cAux := SubStr(cObs,nDe,nAte)
					If nDe = 1
						Memowrite("HFATR002_cAux.txt",cAux)
					EndIf
					oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10,100)
					nLin += nPula
					nDe  := nDe+nLimite
					nAte := nAte+nLimite
				EndDo
			Else
				oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10,100)
				nLin += nPula	
			EndIf		
		EndIf
		SC6->(DbSkip())
	EndDo
EndIf
nLin += nPula

oPrn:Box(nLin-10,50,nLin+50,nfim)

oPrn:Say(nLin,aColTit[1],aTit[1],oFont10n,100)
oPrn:Say(nLin,aColTit[2],aTit[2],oFont10n,100)
oPrn:Say(nLin,aColTit[3]+20,aTit[3],oFont10n,100)
oPrn:Say(nLin,aColTit[4],aTit[4],oFont10n,100)
oPrn:Say(nLin,aColTit[5],aTit[5],oFont10n,100)
oPrn:Say(nLin,aColTit[6],aTit[6],oFont10n,100)

nLin += nPula
imprp	:= .T.

RestArea(aArea)

Return

Static Function Cabec2(nPag)
Local aArea := GetArea() 
Local nx   := 0
nLin	:= 100
nCol	:= 60
nPula	:= 60
nEsp    := 350
nPosTit := 60

aColCab := {80,850,1600,2050}

aTit    := {"Endere�o","    Qtde","Produto","Pre�o","Vl. Total","Presente"}
aColTit := AClone(aColItens)

oPrn:EndPage()

oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
oPrn:Say(nLin,aColCab[4]+120,"P�g.: "+AllTrim(Str(nPag)),oFont09,100)
nLin += nPula

oPrn:Say(nLin,aColCab[4]-50,dtoc(date())+" - "+Time(),oFont10,100)

nLin += nPula

oPrn:Say(nLin,aColCab[1],"Pr�-Faturamento:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,cNumPF,oFont14,100)
nLin += nPula*2
/*oPrn:Say(nLin,aColCab[2],"Pedido Web:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,AllTrim(If(!Empty(QRY->C5_XPEDWEB),QRY->C5_XPEDWEB,QRY->C5_XPEDRAK)),oFont14,100)
oPrn:Say(nLin,aColCab[3],"Pedido Protheus:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,QRY->ZJ_PEDIDO,oFont14,100)
nLin += nPula*2
oPrn:Say(nLin,aColCab[1],"Lote DAP:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,TRT->ZJ_LOTDAP,oFont10,100)
oPrn:Say(nLin,aColCab[2],"Data Integra��o:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,DtoC(StoD(QRY->C5_XDATIMP))+" - "+QRY->C5_XHORIMP,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Ped Vinc./Prazo:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,IIF(!Empty(TRT->C5_XPEDVIN),(TRT->C5_XPEDVIN + " / " + AllTrim(Str(TRT->C5_XPRZVIN))),""),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cond. Pagamento:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,GetAdvFVal("SE4","E4_DESCRI",xFilial("SE4")+TRT->C5_CONDPAG,1,""),oFont10,100)
oPrn:Say(nLin,aColCab[2],"Tipo Prazo:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,GetAdvFVal("SZ6","Z6_DESC",xFilial("SZ6")+TRT->C5_POLCOM+TRT->C5_PRAZO,1,""),oFont10,100)
oPrn:Say(nLin,aColCab[3],"Tipo Pedido:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,GetAdvFVal("SZ4","Z4_DESC",xFilial("SZ4")+TRT->C5_POLCOM+TRT->C5_TPPED,1,""),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cliente:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,TRT->C5_CLIENTE+"/"+TRT->C5_LOJACLI+" - "+SubStr(TRT->A1_NOME,1,35),oFont10,100)
oPrn:Say(nLin,aColCab[3],"CEP:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,Transform(TRT->A1_CEP,"@R 99999-999"),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Endere�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,TRT->A1_END,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Data Emiss�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,DtoC(StoD(TRT->C5_EMISSAO)),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cidade:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,TRT->A1_MUN,oFont10,100)
oPrn:Say(nLin,aColCab[2],"UF:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,TRT->A1_EST,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Bairro:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,TRT->A1_BAIRRO,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Macro Regi�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,TRT->C5_XMICRDE,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Frete:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,Transform(TRT->C5_FRETE,"@E 9,999,999.99"),oFont10,100)
oPrn:Say(nLin,aColCab[2],"Data Entrega:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,DtoC(StoD(TRT->C5_FECENT)),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Transportador:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,TRT->C5_TRANSP+" - "+GetAdvFVal("SA4","A4_NOME",xFilial("SA4")+TRT->C5_TRANSP,1,""),oFont10,100)
nLin += nPula

IF QRY->C5_TPPED = "016"
	oPrn:Say(nLin,aColCab[2],"VIRTUAL",oFont40n,100)
ElseIf QRY->C5_TPPED = "133"
	oPrn:Say(nLin,aColCab[2],"MAGALU",oFont40n,100)	
ElseIf QRY->C5_TPPED = "144"
	oPrn:Say(nLin,aColCab[2],"APP-HR",oFont40n,100)
ElseIf QRY->C5_TPPED = "147"
	oPrn:Say(nLin,aColCab[2],"APP-HL",oFont40n,100)		
ElseIf QRY->C5_TPPED $ GETMV("B2W_TPPED")
	oPrn:Say(nLin,aColCab[2],"B2W",oFont40n,100)	
Else
	nPerc := Percent(TRT->ZJ_NUMPF)
	oPrn:Say(nLin,aColCab[2],AllTrim(Str(nPerc))+"%",oFont30n,100)
EndIf

oPrn:Say(nLin,aColCab[1],"Presente:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,Transform(TRT->C5_DESPESA,"@E 9,999,999.99"),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Desconto:",oFont10n,100) //NEW
oPrn:Say(nLin,aColCab[1]+nEsp,Transform(TRT->C5_DESCONT,"@E 9,999,999.99"),oFont10,100) //NEW

MSBAR('CODE128',5.8,15.0,TRT->ZJ_NUMPF,oPrn,.F.,,.T.,0.040,1.1,,,,.F.)
nLin += nPula
nLin += nPula
oPrn:Say(nLin-150,aColCab[4]-250,TRT->ZJ_NUMPF,oFont14n,100)
nLin += nPula

nLimite := 130

cObs := AllTrim(TRT->C5_XINSTRU)
If !Empty(cObs)
	oPrn:Say(nLin,aColCab[1],"Instru��es:",oFont10n,100)
	cAux := ""
	If Len(cObs) > nLimite
		nDe  := 1
		nAte := nLimite
		While nDe < Len(cObs)
			cAux := SubStr(cObs,nDe,nAte)
			oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10,100)
			nLin += nPula
			nDe  := nDe+nLimite
			nAte := nAte+nLimite
		EndDo
	Else
		oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10,100)
		nLin += nPula	
	EndIf
EndIf

cObs := Alltrim(GetAdvFVal("SC5","C5_XOBSINT",xFilial("SC5")+TRT->ZJ_PEDIDO,1,""))

/*For x:=1 to len(cObs)
    cString:=alltrim(cObs)
    If AT(chr(13)+chr(10),cString) > 0
        cString:=STRTRAN(cString,chr(13)+chr(10),' | ')
    Endif
    cObs:=cString
Next x

aObs := {}

If !Empty(cObs)
	aObs := U_MemoToAr(cObs,90,"",.T.)
	
	If Len(aObs) > 0
		oPrn:Say(nLin,aColCab[1],"Observa��o:",oFont10n,100)
		For nx := 1 to Len(aObs)
			oPrn:Say(nLin,aColCab[1]+nEsp,aObs[nx],oFont10,100)
        	nLin += nPula
		Next
	EndIf
	
	/*nLinhas := MLCount(cObs,90)
	For nX:= 1 To nLinhas
        cTxtLinha := MemoLine(cObs,90,nX)
        If !Empty(cTxtLinha)
        	oPrn:Say(nLin,aColCab[1]+nEsp,cTxtLinha,oFont10,100)
        	nLin += nPula
        EndIf
	Next nX*/
	
	/*cAux := ""
	If Len(cObs) > nLimite
		nDe  := 1
		nAte := nLimite
		While nDe < Len(cObs)
			cAux := SubStr(cObs,nDe,nAte)
			oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10,100)
			nLin += nPula
			nDe  := nDe+nLimite
			nAte := nAte+nLimite
		EndDo
	Else
		oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10,100)
		nLin += nPula	
	EndIf		
EndIf

DbSelectArea("SC6")
DbSetOrder(1)
If DbSeek(xFilial("SC6")+TRT->ZJ_PEDIDO)
	lPre := .T.
	While SC6->(!EOF()) .And. SC6->C6_NUM = TRT->ZJ_PEDIDO
		If !Empty(SC6->C6_XPRESEN)
			cObs := SC6->C6_PRODUTO+" - "+AllTrim(SC6->C6_XPRESEN)
			Memowrite("HFATR002_cObs.txt",cObs)
			If lPre
				oPrn:Say(nLin,aColCab[1],"Msg. Presente:",oFont10n,100)
				lPre := .F.
			EndIf
			cAux := ""
			If Len(cObs) > nLimite
				nDe  := 1
				nAte := nLimite
				While nDe < Len(cObs)
					cAux := SubStr(cObs,nDe,nAte)
					If nDe = 1
						Memowrite("HFATR002_cAux.txt",cAux)
					EndIf
					oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10,100)
					nLin += nPula
					nDe  := nDe+nLimite
					nAte := nAte+nLimite
				EndDo
			Else
				oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10,100)
				nLin += nPula	
			EndIf		
		EndIf
		SC6->(DbSkip())
	EndDo
EndIf
nLin += nPula*/

oPrn:Box(nLin-10,50,nLin+50,nfim)

oPrn:Say(nLin,aColTit[1],aTit[1],oFont10n,100)
oPrn:Say(nLin,aColTit[2],aTit[2],oFont10n,100)
oPrn:Say(nLin,aColTit[3]+20,aTit[3],oFont10n,100)
oPrn:Say(nLin,aColTit[4],aTit[4],oFont10n,100)
oPrn:Say(nLin,aColTit[5],aTit[5],oFont10n,100)
oPrn:Say(nLin,aColTit[6],aTit[6],oFont10n,100)

nLin += nPula
imprp	:= .T.

RestArea(aArea)

Return

User Function MemoToAr(cTexto, nMaxCol, cQuebra, lTiraBra)
Local aArea     := GetArea()
Local aTexto    := {}
Local aAux      := {}
Local nAtu      := 0
Default cTexto  := ''
Default nMaxCol := 80
Default cQuebra := ';'
Default lTiraBra:= .T.
 
//Quebrando o Array, conforme -Enter-
aAux:= StrTokArr(cTexto,Chr(13))
 
//Correndo o Array e retirando o tabulamento
For nAtu:=1 TO Len(aAux)
    aAux[nAtu]:=StrTran(aAux[nAtu],Chr(10)," | ")
Next
 
//Correndo as linhas quebradas
For nAtu:=1 To Len(aAux)
 
    //Se o tamanho de Texto, for maior que o n�mero de colunas
    If (Len(aAux[nAtu]) > nMaxCol)
     
        //Enquanto o Tamanho for Maior
        While (Len(aAux[nAtu]) > nMaxCol)
            //Pegando a quebra conforme texto por par�metro
            nUltPos:=RAt(cQuebra,SubStr(aAux[nAtu],1,nMaxCol))
             
            //Caso n�o tenha, a �ltima posi��o ser� o �ltimo espa�o em branco encontrado
            If nUltPos == 0
                nUltPos:=Rat(' ',SubStr(aAux[nAtu],1,nMaxCol))
            EndIf
             
            //Se n�o encontrar espa�o em branco, a �ltima posi��o ser� a coluna m�xima
            If(nUltPos==0)
                nUltPos:=nMaxCol
            EndIf
             
            //Adicionando Parte da Sring (de 1 at� a �lima posi��o v�lida)
            aAdd(aTexto,SubStr(aAux[nAtu],1,nUltPos))
             
            //Quebrando o resto da String
            aAux[nAtu] := SubStr(aAux[nAtu], nUltPos+1, Len(aAux[nAtu]))
        EndDo
         
        //Adicionando o que sobrou
        aAdd(aTexto,aAux[nAtu])
    Else
        //Se for menor que o M�ximo de colunas, adiciona o texto
        aAdd(aTexto,aAux[nAtu])
    EndIf
Next
 
//Se for para tirar os brancos
If lTiraBra
    //Percorrendo as linhas do texto e aplica o AllTrim
    For nAtu:=1 To Len(aTexto)
        aTexto[nAtu] := Alltrim(aTexto[nAtu])
    Next
EndIf
 
RestArea(aArea)

Return aTexto


//_____________________________________________________________________________
/*/{Protheus.doc} Valor Total do Pedido
Rotina para calcular o valor total do pedido;

@author Ronaldo Pereira
@since 24 de Maio de 2019
@version V.01
/*/
//_____________________________________________________________________________
Static Function CalcValor(cNumPre)
Local _Qry := ""
Local nValTot := 0
 	
 	_Qry := " SELECT ZJ.ZJ_NUMPF,(SUM(ZJ.ZJ_QTDLIB * C6.C6_PRCVEN) - C5.C5_DESCONT) + C5.C5_FRETE AS VALORTT "
 	_Qry += " FROM "+RetSqlName("SZJ")+" AS ZJ WITH (NOLOCK) "
 	_Qry += " INNER JOIN "+RetSqlName("SC5")+" AS C5 WITH (NOLOCK) ON (C5.C5_FILIAL = ZJ.ZJ_FILIAL "
 	_Qry += "                                                      AND C5.C5_CLIENTE = ZJ.ZJ_CLIENTE "
 	_Qry += "                                           		   AND C5.C5_NUM = ZJ.ZJ_PEDIDO "
 	_Qry += "                                           		   AND C5.D_E_L_E_T_ = '') "
 	_Qry += " INNER JOIN "+RetSqlName("SC6")+" AS C6 WITH (NOLOCK) ON (C6.C6_FILIAL = ZJ.ZJ_FILIAL "
 	_Qry += "                                           		   AND C6.C6_CLI = ZJ.ZJ_CLIENTE "
 	_Qry += "                                                      AND C6.C6_NUM = ZJ.ZJ_PEDIDO "
 	_Qry += "                                                      AND C6.C6_ITEM = ZJ.ZJ_ITEM "
 	_Qry += "                                                      AND C6.C6_PRODUTO = ZJ.ZJ_PRODUTO "
 	_Qry += "                                                      AND C6.D_E_L_E_T_ = '') "
 	_Qry += " WHERE ZJ.D_E_L_E_T_ = '' "
 	_Qry += "   AND ZJ.ZJ_NUMPF = '"+cNumPre+"' "
 	_Qry += " GROUP BY ZJ.ZJ_NUMPF, "
 	_Qry += "          C5.C5_DESCONT, "
 	_Qry += "          C5.C5_FRETE " 	
 	         
 	If Select("TMPRE") > 0 
 		TMPRE->(DbCloseArea()) 
 	EndIf 

 	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPRE",.T.,.T.) 
 	
 	nValTot := TMPRE->VALORTT        

 	TMPRE->(DbCloseArea()) 

Return nValTot


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data �  05/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria perguntas da rotina.                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Pedido de            ","","","Mv_ch1","C",6,0,0,"G","",   "","","N","Mv_par01",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o pedido de.",""},{""},{""},"")
PutSx1(cPerg,"02","Pedido ate           ","","","Mv_ch2","C",6,0,0,"G","",   "","","N","Mv_par02",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o pedido ate.",""},{""},{""},"")
PutSx1(cPerg,"03","Imprime j� separados?","","","Mv_ch3","N",1,0,2,"C","",   "","","N","Mv_par03",       "Sim","","","",      "Nao","","",     "","","","","","","","","","","","","","","","",{"Imprime itens j� separados?",""},{""},{""},"")
PutSx1(cPerg,"04","Lote DAP de          ","","","Mv_ch4","C",6,0,0,"G","",   "","","N","Mv_par04",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o Lote DAP de.",""},{""},{""},"")
PutSx1(cPerg,"05","Lote DAP ate         ","","","Mv_ch5","C",6,0,0,"G","",   "","","N","Mv_par05",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o Lote DAP ate.",""},{""},{""},"")
PutSx1(cPerg,"06","Tipo dos Pedidos     ","","","Mv_ch6","C",99,0,0,"G","U_HF002TP()","","","N","Mv_par06","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione os tipos de pedido.",""},{""},{""},"")
PutSx1(cPerg,"07","Data de              ","","","Mv_ch7","D",8,0,0,"G","",   "","","N","Mv_par07",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione a Data de.",""},{""},{""},"")
PutSx1(cPerg,"08","Data ate             ","","","Mv_ch8","D",8,0,0,"G","",   "","","N","Mv_par08",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione a Data ate.",""},{""},{""},"")
PutSx1(cPerg,"09","Somente Liberados ?  ","","","Mv_ch9","N",1,0,2,"C","",   "","","N","Mv_par09",       "Sim","","","",      "Nao","","",     "","","","","","","","","","","","","","","","",{"Imprime itens j� liberados?",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)
