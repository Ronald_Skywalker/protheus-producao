#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HWSRK001  �Autor  �Bruno Parreira      � Data �  01/12/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa utilizado para importar os pedidos de venda B2C    ���
���          �da RAKUTEN                                                  ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HWSRK001() //Pedido Metodo: 1 - ListarNovos 2- Listar
Local aWsRet   := {}
Local cWsRet     := ""

Private oWs      := NIL
Private cLog     := ""
Private cPerg  	 := "HWSRK001"
Private cTime    := ""
Private cDtTime  := ""
Private cTpOper  := ""
Private cChvA1   := ""
Private cChvA2   := ""
Private cCnB2C   := ""
Private cPlB2C   := ""
Private cTpB2C   := ""
Private cNature  := ""
Private cPolitc  := ""
Private cTipPed  := ""
Private cCndPag  := ""
Private cCdEnd   := ""
Private cCdMun   := ""
Private cCdEnt   := ""
Private cCdHop   := ""
Private cCdGer   := ""
Private cCdTip   := ""
Private cCdGrp   := ""
Private cCdDiv   := ""
Private cCdReg   := ""
Private cCdMac   := ""
Private cNtB2C   := ""
Private cNmCan   := ""
Private cNmTip   := ""
Private cNmGrp   := ""
Private cNmDiv   := ""
Private cNmReg   := ""
Private cNmMac   := ""
Private cAmzPik  := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Private cAmzPul  := AllTrim(SuperGetMV("MV_XAMZPUL",.F.,"E1")) //Armazem padrao pulmao
Private nErro    := 0
Private nGerados := 0

lMenu := .f.
If Select("SX2") <> 0
	lMenu := .t.
Else
	Prepare Environment Empresa "01" Filial "0101"
Endif

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

GravaLog("In�cio do processamento...")

oWsClient := WSPedido():New()

cChvA1 := AllTrim(SuperGetMV("MV_XRA1B2C",.F.,"8B3CD696-D70A-4E59-8E2D-24A96ED8115E")) //Chave A1 para validar acesso ao webservice //8B3CD696-D70A-4E59-8E2D-24A96ED8115
cChvA2 := AllTrim(SuperGetMV("MV_XRA2B2C",.F.,"2DD1CBF7-4B95-43BA-9F16-2C295E80E8CF")) //Chave A2 para validar acesso ao webservice //2DD1CBF7-4B95-43BA-9F16-2C295E80E8CF
cCnB2C := AllTrim(SuperGetMV("MV_XRCNB2C",.F.,"007")) //Canal de vendas dos clientes do B2C
cPlB2C := AllTrim(SuperGetMV("MV_XRPLB2C",.F.,"018")) //Politica comercial dos pedidos B2C
cTpB2C := AllTrim(SuperGetMV("MV_XRTPB2C",.F.,"016")) //Tipo de Pedido dos pedidos B2C
cCdEnd := AllTrim(SuperGetMV("MV_XRCDEND",.F.,"1628139")) //Codigo Endereco Aba HOPE
cCdMun := AllTrim(SuperGetMV("MV_XRCDMUN",.F.,"00000")) //Codigo Municipio Aba HOPE
cCdEnt := AllTrim(SuperGetMV("MV_XRCDENT",.F.,"1628139")) //Codigo Endereco de Entrega Aba HOPE
cCdHop := AllTrim(SuperGetMV("MV_XRCDHOP",.F.,"182973")) //Codigo HOPE Aba HOPE
cCdGer := AllTrim(SuperGetMV("MV_XRCDGER",.F.,"348247")) //Codigo Gerador Aba HOPE
cCdTip := AllTrim(SuperGetMV("MV_XRTPEMP",.F.,"01")) //Tipo Empresa Aba HOPE
cCdGrp := AllTrim(SuperGetMV("MV_XRCDGRP",.F.,"09")) //Codigo Grupo Aba HOPE
cCdDiv := AllTrim(SuperGetMV("MV_XRCDDIV",.F.,"A")) //Codigo Divisao Aba HOPE
cCdReg := AllTrim(SuperGetMV("MV_XRCDREG",.F.,"11040010")) //Codigo Regiao Aba HOPE
cCdMac := AllTrim(SuperGetMV("MV_XRCDMAC",.F.,"11040")) //Codigo Macro Regiao Aba HOPE
cCdPrz := AllTrim(SuperGetMV("MV_XRPRZBC",.F.,"004")) //Prazo de Entrega Pedido B2C
cAmzPk := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking

cNtB2C := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCnB2C,1,"11116001")
cNmCan := GetAdvFVal("ZA3","ZA3_DESCRI",xFilial("ZA3")+cCnB2C,1,"")
cNmTip := GetAdvFVal("ZA1","ZA1_DESCRI",xFilial("ZA1")+cCdTip,1,"")
cNmGrp := GetAdvFVal("ZA2","ZA2_DESCRI",xFilial("ZA2")+cCdGrp,1,"")
cNmDiv := GetAdvFVal("ZA5","ZA5_DESCRI",xFilial("ZA5")+cCdDiv,1,"")
cNmReg := GetAdvFVal("ZA9","ZA9_DESC",xFilial("ZA9")+cCdReg,1,"")
cNmMac := GetAdvFVal("ZA4","ZA4_DESCMA",xFilial("ZA4")+cCdMac,1,"")

cTpOpad := AllTrim(SuperGetMV("MV_XRTOV2C",.F.,"01")) //Tipo de Operacao de venda para pedidos B2C
cTESPad := AllTrim(SuperGetMV("MV_XB2CTES",.F.,"501")) //TES Padrao para importacao dos pedidos B2C
cTESBri := AllTrim(SuperGetMV("MV_XB2CTEB",.F.,"754")) //TES Padrao para importacao dos pedidos B2C Brinde

If !lMenu
	GravaLog("Executando funcao ListarNovos automatica")
	If oWsClient:ListarNovos(0,7,,cChvA1,cChvA2)  // Codigo Loja, Status Pedido
		cWsRet := oWsClient:oWSListarNovosResult:cDescricao
		If "SUCESSO" $ UPPER(cWsRet)
			aWsRet := aClone(oWsClient:oWSListarNovosResult:oWSLista:oWSClsPedido)
			IMPORTA(aWsRet)
		Else
			GravaLog("Problema nos parametros da fun��o ListarNovos: "+cWsRet)
		EndIf
	Else
		GravaLog("Erro na funcao ListarNovos: "+GetWSCError())
		Return
	EndIf
Else
	GravaLog("Executando via menu.")
	AjustaSX1(cPerg)
	If Pergunte(cPerg)
		If mv_par01 = 1
			GravaLog("Executando Listar via menu.")
			If oWsClient:Listar(0,Val(mv_par02),7,,cChvA1,cChvA2)  // Codigo Loja, Codigo Pedido, Status Pedido
				cWsRet := oWsClient:oWSListarResult:cDescricao
				If "SUCESSO" $ UPPER(cWsRet)
					aWsRet := aClone(oWsClient:oWSListarResult:oWSLista:oWSClsPedido)
					Processa( {|| IMPORTA(aWsRet,.F.) }, "Aguarde...", "Processando...",.F.)
				Else
					GravaLog("Problema nos parametros da fun��o Listar: "+cWsRet)
				EndIf
			Else
				MsgAlert("Erro Listar: "+GetWSCError(),"Erro")
				GravaLog("Erro na funcao Listar via menu: "+GetWSCError())
				Return
			EndIf
		ElseIf mv_par01 = 2
			GravaLog("Executando ListarNovos via menu.")
			If oWsClient:ListarNovos(0,7,,cChvA1,cChvA2)  // Codigo Loja, Status Pedido
				cWsRet := oWsClient:oWSListarNovosResult:cDescricao
				If "SUCESSO" $ UPPER(cWsRet)
					aWsRet := aClone(oWsClient:oWSListarNovosResult:oWSLista:oWSClsPedido)
					Processa( {|| IMPORTA(aWsRet,.F.) }, "Aguarde...", "Processando...",.F.)
				Else
					GravaLog("Problema nos parametros da fun��o ListarNovos: "+cWsRet)
				EndIf
			Else
				MsgAlert("Erro ListarNovos: "+GetWSCError(),"Erro")
				GravaLog("Erro na funcao ListarNovos via menu: "+GetWSCError())
				Return
			EndIf
		Else
			GravaLog("Executando Listar SZT via menu.")
			Processa( {|| IMPORTASZT() }, "Aguarde...", "Processando...",.F.)
		EndIf

		If lMenu
			If mv_par01 <> 1
				If nGerados > 0
					MsgInfo("Foram importados "+AllTrim(Str(nGerados))+" pedidos com sucesso.","Aviso")
					GravaLog("Foram importados "+AllTrim(Str(nGerados))+" pedidos com sucesso.","Aviso")
				Else
					MsgInfo("N�o foram encontrados pedidos aptos a importar.","Aviso")
					GravaLog("N�o foram encontrados pedidos aptos a importar.","Aviso")
				EndIf
			EndIf
		Endif
	Else
		GravaLog("Processo cancelado pelo usu�rio.")
	EndIf
Endif

GravaLog("Fim do processamento.")

MemoWrite("\log_rakuten\B2C\"+cDtTime+"_IMP.log",cLog)

If !lMenu
	Reset Environment
EndIf

Return

Static Function IMPORTASZT()
Local nCont := 0
Local nPed  := 0

DbSelectArea("SZT")
DbSetOrder(2)
If DbSeek(xFilial("SZT")+"B2C")
	While SZT->(!EOF()) .And. SZT->ZT_ORIGEM = "B2C"
		If Empty(SZT->ZT_STATUS)
			nCont++
		EndIf
		SZT->(DbSkip())
	EndDo
EndIf

If nCont = 0
	GravaLog("N�o h� itens a serem importados na SZT.")
	Return
EndIf

ProcRegua(nCont)

DbSelectArea("SZT")
DbSetOrder(2)
If DbSeek(xFilial("SZT")+"B2C")
	While SZT->(!EOF()) .And. SZT->ZT_ORIGEM = "B2C"
		If Empty(SZT->ZT_STATUS)
			IncProc()
			DbSelectArea("SC5")
			DbOrderNickName("XPEDRAK")
			If DbSeek(xFilial("SC5")+SZT->ZT_PEDIDO)
				GravaLog("Pedido Rakuten: "+SZT->ZT_PEDIDO+" j� importado no protheus. Alterando status...")
				RecLock("SZT",.F.)
				SZT->ZT_STATUS = "X"
				MsUnlock()
				SZT->(DbSkip())
				Loop
			EndIf
			//BEGIN TRANSACTION
			nPed++
			If oWsClient:Listar(0,Val(SZT->ZT_PEDIDO),7,,cChvA1,cChvA2)  // Codigo Loja, Codigo Pedido, Status Pedido
				cWsRet := oWsClient:oWSListarResult:cDescricao
				If "SUCESSO" $ UPPER(cWsRet)
					aWsRet := aClone(oWsClient:oWSListarResult:oWSLista:oWSClsPedido)
					IMPORTA(aWsRet,.T.,SZT->ZT_EMISSAO)
				Else
					GravaLog("Problema nos parametros da fun��o Listar: "+cWsRet)
				EndIf
			Else
				GravaLog("Pedido "+SZT->ZT_PEDIDO+".Erro na funcao Listar SZT via menu: "+GetWSCError())
			EndIf
			//END TRANSACTION
			If nPed > 50
				cTime   := Time()
				cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
				MemoWrite("\log_rakuten\B2C\"+cDtTime+"_IMP.log",cLog)
				cLog := "_"
				nPed := 0
			EndIf
		EndIf
		SZT->(DbSkip())
	EndDo
EndIf

Return

Static Function IMPORTA(aWsRet,lSZT,dEmissao)
Local nx,ny    := 0
Local cNroPed  := ""
Local cCGC     := ""
Local cCodCli  := ""
Local cLojCli  := ""
Local cNovCli  := ""
Local cTipPes  := ""
Local cTPesso  := ""
Local cNomCli  := ""
Local nPosP    := 0
Local cProd    := ""
Local cRef     := ""
Local cCor     := ""
Local cTam     := ""
Local cPedRak  := ""
Local cEndere  := ""
Local cComple  := ""
Local cBairro  := ""
Local cEstado  := ""
Local cCEP     := ""
Local cMunici  := ""
Local cEmail   := ""
Local cDDDTel  := ""
Local cNumTel  := ""
Local cDDDCel  := ""
Local cNumCel  := ""
Local cTES     := ""
Local nSldPik  := ""
Local nSldPul  := ""
Local aNotSld  := {}
Local aNotEnd  := {}
Local aChkEstoq:= {}
Local cProd2   := ""
Local nQtd2Prd := 0
Local nxx        := 0
Local _i        := 0
Private lMsErroAuto := .F.

cPolitc := cPlB2C
cTipPed := cTpB2C

If Len(aWsRet) > 0
	If lMenu .And. !lSZT
		ProcRegua(Len(aWsRet))
	EndIf

	For nx := 1 to Len(aWsRet)
		If lMenu .And. !lSZT
			IncProc()
		EndIf

		cPedRak := StrZero(aWsRet[nx]:nPedidoCodigo,6)

		GravaLog("Iniciando a importa��o do pedido: "+cPedRak)

		DbSelectArea("SC5")
		DbOrderNickName("XPEDRAK")
		If DbSeek(xFilial("SC5")+cPedRak)
			GravaLog("Pedido "+cPedRak+" j� importado no protheus")
			If lMenu
				If mv_par01 = 1
					If !MsgYesNo("Pedido "+cPedRak+" j� importado. Importar novamente?","Aviso")
						Exit
					EndIf
				ElseIf mv_par01 = 3
					DbSelectArea("SZT")
					DbSetOrder(1)
					If DbSeek(xFilial("SZT")+cPedRak)
						RecLock("SZT",.F.)
						SZT->ZT_STATUS = "X"
						MsUnlock()
					EndIf
					Loop
					//ValidPed(cPedRak)
				Else
					//ValidPed(cPedRak)
					Loop
				EndIf
			Else
				//ValidPed(cPedRak)
				Loop
			EndIf
		EndIf

		Do Case
			Case AllTrim(aWsRet[nx]:oWSPessoa:Value) = 'PessoaF�sica'
				cTipPes := "F"
				cCGC    := aWsRet[nx]:cCPF
				cTPesso := "PF"
			Case AllTrim(aWsRet[nx]:oWSPessoa:Value) = 'PessoaJur�dica'
				cTipPes := "J"
				cCGC    := aWsRet[nx]:cCNPJ
				cTPesso := "PF"
			Otherwise
				GravaLog("Tipo de cliente diferente de F ou J. Pedido: "+cPedRak)
				Loop
		EndCase

		cEndere := UPPER(FwNoAccent(aWsRet[nx]:oWSTipoLogradouro:Value))+" "+UPPER(FwNoAccent(aWsRet[nx]:cLogradouro))
		cComple := UPPER(FwNoAccent(aWsRet[nx]:cComplemento))
		cBairro := UPPER(FwNoAccent(aWsRet[nx]:cBairro))
		cEstado := aWsRet[nx]:cEstado
		cCEP    := aWsRet[nx]:cCEP
		cMunici := UPPER(FwNoAccent(aWsRet[nx]:cCidade))
		cEmail  := aWsRet[nx]:cEmail
		cDDDTel := aWsRet[nx]:cDDD1
		cNumTel := aWsRet[nx]:cTelefone1
		cDDDCel := aWsRet[nx]:cDDDCelular
		cNumCel := aWsRet[nx]:cCelular

		DbSelectArea("SA1")
		DbSetOrder(3)
		If DbSeek(xFilial("SA1")+cCGC)
			cCodCli := ""
			cLojCli := ""

			If SA1->A1_MSBLQL <> "1" //Inativo
				While SA1->(!EOF()) .And. SA1->A1_CGC = cCGC
					If SA1->A1_MSBLQL <> "1"
						cCodCli := SA1->A1_COD
						cLojCli := SA1->A1_LOJA
						Exit
					EndIf
					SA1->(DbSkip())
				EndDo
			EndIf

			If Empty(cCodCli)
				GravaLog("Cliente ativo n�o encontrado. Pedido: "+cPedRak)
				Loop
			EndIf

			If !Empty(SA1->A1_NATUREZ)
				cNature := SA1->A1_NATUREZ
			Else
				cNature := cNtB2C
			EndIf

			//GravaLog("Cliente "+cCodCli+"-"+cLojCli+" encontrado no Protheus. Pedido: "+cPedRak)

			RecLock("SA1",.F.)
			SA1->A1_END     := cEndere
			SA1->A1_COMPLEM := cComple
			SA1->A1_BAIRRO  := cBairro
			SA1->A1_EST     := cEstado
			SA1->A1_CEP     := cCEP
			SA1->A1_MUN     := cMunici
			SA1->A1_ENDENT  := cEndere+" "+cComple
			SA1->A1_BAIRROE := cBairro
			SA1->A1_ESTE    := cEstado
			SA1->A1_CEPE    := cCEP
			SA1->A1_MUNE    := cMunici
			SA1->A1_EMAIL   := cEmail
			SA1->A1_DDD     := cDDDTel
			SA1->A1_TEL     := cNumTel
			SA1->A1_XDDDCEL := cDDDCel
			SA1->A1_XCEL    := cNumCel
			MsUnlock()
		Else
			GravaLog("Cliente n�o encontrado. Pedido: "+cPedRak)

			cNovCli := GETSX8NUM("SA1","A1_COD")

			If cTipPes == 'F'
				cNomCli := FwNoAccent(aWsRet[nx]:cNome)+" "+FwNoAccent(aWsRet[nx]:cSobrenome)
			Else
				cNomCli := FwNoAccent(aWsRet[nx]:cRazaoSocial)
			EndIf

			DbSelectArea("ZAR")
			DbSetOrder(1)
			DbSeek(xfilial("ZAR")+cCEP)

			DbSelectArea("CC2")
			DbSetOrder(2)
			DbSeek(xfilial("CC2")+FwNoAccent(alltrim(ZAR->ZAR_MUN)))
			ccodmun := ""
			If Found()
				ccodmun := CC2->CC2_CODMUN
			Endif

			RecLock("SA1",.T.)
			SA1->A1_COD     := cNovCli
			SA1->A1_LOJA    := "0001"
			SA1->A1_CGC     := cCGC
			SA1->A1_INSCR   := "ISENTO"
			SA1->A1_PESSOA  := cTipPes
			SA1->A1_TPESSOA := cTPesso
			SA1->A1_TIPO    := "F" //Consumidor Final
			SA1->A1_CONTRIB := "2" //Nao contribuinte
			SA1->A1_NOME    := UPPER(cNomCli)
			SA1->A1_NREDUZ  := UPPER(cNomCli)
			SA1->A1_END     := cEndere
			SA1->A1_COMPLEM := cComple
			SA1->A1_BAIRRO  := cBairro
			SA1->A1_EST     := cEstado
			SA1->A1_CEP     := cCEP
			SA1->A1_COD_MUN := cCODMUN
			SA1->A1_MUN     := cMunici
			SA1->A1_ENDENT  := cEndere+" "+cComple
			SA1->A1_BAIRROE := cBairro
			SA1->A1_ESTE    := cEstado
			SA1->A1_CEPE    := cCEP
			SA1->A1_MUNE    := cMunici
			SA1->A1_EMAIL   := cEmail
			SA1->A1_DDD     := cDDDTel
			SA1->A1_TEL     := cNumTel
			SA1->A1_XDDDCEL := cDDDCel
			SA1->A1_XCEL    := cNumCel
			SA1->A1_NATUREZ := cNtB2C
			SA1->A1_XCODEND := cCdEnd
			SA1->A1_COD_MUC := cCdMun
			SA1->A1_XCODENC := cCdEnt
			SA1->A1_XCLI    := cCdHop
			SA1->A1_XGER    := cCdGer
			SA1->A1_XCANAL  := cCnB2C
			SA1->A1_XCANALD := cNmCan
			SA1->A1_XEMP    := cCdTip
			SA1->A1_XDESEMP := cNmTip
			SA1->A1_XGRUPO  := cCdGrp
			SA1->A1_XGRUPON := cNmGrp
			SA1->A1_XCODDIV := cCdDiv
			SA1->A1_XNONDIV := cNmDiv
			SA1->A1_XCODREG := cCdReg
			SA1->A1_XDESREG := cNmReg
			SA1->A1_XMICRRE := cCdMac
			SA1->A1_XMICRDE := cNmMac
			MsUnlock()

			CONFIRMSX8()

			GravaLog("Inclu�do cliente "+cNovCli+" no Protheus. Pedido: "+cPedRak)

			cCodCli := cNovCli
			cLojCli := "0001"
			cNature := cNtB2C
			//Loop
		EndIf

		cNroPed := GetMV("MV_XNMPVRK")

		DbSelectArea("SE4")
		DbOrderNickName("XCONDRAK")
		If DbSeek(xFilial("SE4")+SubStr(AllTrim(Str(aWsRet[nx]:nPagamento))+SPACE(3),1,3)+SubStr(AllTrim(Str(aWsRet[nx]:nQtdeParcelas))+SPACE(3),1,2))
			cCndPag := SE4->E4_CODIGO
		Else
			cCndPag := ""
			GravaLog("Condicao de pagamento nao encontrada. Pedido: "+cPedRak)
			Loop
		EndIf

		aCabec   := {}
		cDataEmi := SubStr(aWsRet[nx]:cData,1,4)+SubStr(aWsRet[nx]:cData,6,2)+SubStr(aWsRet[nx]:cData,9,2)

		DbSelectArea("SA1")
		DbSetOrder(1)
		DbSeek(xFilial("SA1")+cCodCli+cLojCli)

	   	aadd(aCabec,{"C5_NUM"    ,cNroPed,Nil})
		aadd(aCabec,{"C5_TIPO"   ,"N",Nil})
		aadd(aCabec,{"C5_CLIENTE",cCodCli,Nil})
		aadd(aCabec,{"C5_LOJACLI",cLojCli,Nil})
		aadd(aCabec,{"C5_CLIENT" ,cCodCli,Nil})
		aadd(aCabec,{"C5_LOJAENT",cLojCli,Nil})
		aadd(aCabec,{"C5_POLCOM" ,cPolitc,Nil})
		aadd(aCabec,{"C5_TPPED"  ,cTipPed,Nil})
		aadd(aCabec,{"C5_CONDPAG",cCndPag,Nil})
		aadd(aCabec,{"C5_NATUREZ",cNature,Nil})
		aadd(aCabec,{"C5_XPEDRAK",StrZero(aWsRet[nx]:nPedidoCodigo,6),Nil})
		aadd(aCabec,{"C5_XPEDWEB","IKE"+aWsRet[nx]:cPedidoCodigoCliente,Nil})
		aadd(aCabec,{"C5_FRETE"  ,aWsRet[nx]:nValorFreteCobrado,Nil})
		aadd(aCabec,{"C5_DESCONT",0,Nil})
		aadd(aCabec,{"C5_PRAZO"  ,cCdPrz,Nil})
		aadd(aCabec,{"C5_XCREDRK",aWsRet[nx]:nValorVale,Nil}) // Altera��o do campo cortesia para Rakuten [Weskley Silva 29/08/2018]
		//aadd(aCabec,{"C5_DESCONT",aWsRet[nx]:nValorVale,Nil})
		aadd(aCabec,{"C5_XCARTID",aWsRet[nx]:oWsCartao:cCartID,Nil})
		If !Empty(aWsRet[nx]:oWsCartao:cCartID)
			aadd(aCabec,{"C5_XNSU"   ,Right(aWsRet[nx]:oWsCartao:cCarRetMsg,6),Nil})
		EndIf
		aadd(aCabec,{"C5_XAUTCAR",aWsRet[nx]:oWsCartao:cCarAutorizacaoCodigo,Nil})
		aadd(aCabec,{"C5_XDESTIN",aWsRet[nx]:cNomeDestinatario,Nil})
		If lSZT
			aadd(aCabec,{"C5_EMISSAO",dEmissao,Nil})
		Else
			aadd(aCabec,{"C5_EMISSAO",StoD(cDataEmi),Nil})
		EndIf

		aadd(aCabec,{"C5_ORIGEM","B2C",Nil})

		//aadd(aCabec,{"C5_FECENT",dEmissao,Nil})

		aadd(aCabec,{"C5_XCANAL" ,SA1->A1_XCANAL,Nil})
		aadd(aCabec,{"C5_XCANALD",SA1->A1_XCANALD,Nil})
		aadd(aCabec,{"C5_XCODREG",SA1->A1_XCODREG,Nil})
		aadd(aCabec,{"C5_XDESREG",SA1->A1_XDESREG,Nil})
		aadd(aCabec,{"C5_XMICRRE",SA1->A1_XMICRRE,Nil})
		aadd(aCabec,{"C5_XMICRDE",SA1->A1_XMICRDE,Nil})
		aadd(aCabec,{"C5_DESPESA",aWsRet[nx]:nValorPresente,Nil})

		aItens := {}

		lContinua := .T.
		cItem := "01"

		aWsItens := aClone(aWsRet[nx]:oWsItens:oWsItem)
		aNotSld  := {}

		aItWs   := {}
		nVlrPre := 0

		For ny := 1 to len(aWsItens)
			//Formato do codigo do produto vindo da RAKUTEN. Ex.: OUT_12345678_PRT_P  -> PROMOCAO_REFERENCIA_COR_TAMANHO
			cProd := aWsItens[ny]:cItemPartNumber   //Codigo do produto que vem da RAKUTEN
			nPosP := Len(cProd)
			cProd := AllTrim(If(SubStr(cProd,1,4)='OUT_',SubStr(cProd,5),cProd)) //Retira o OUT_ caso seja um produto de promocao

			nVlrPre += aWsItens[ny]:nItVlrPresente
			//aAdd(aItWs,{cProd,aWsItens[ny]:nItVlrFinal,aWsItens[ny]:nItemQtde,,})
			aAdd(aItWs,{cProd,aWsItens[ny]:nItVlrFinal,aWsItens[ny]:nItemQtde,SubStr(cProd,1,8),AllTrim(Str(aWsItens[ny]:nItVlrFinal*100)),aWsItens[ny]:cIntCod,aWsItens[ny]:cItemMsgPresente,aWsItens[ny]:nItVlrPresente})
		Next

		aadd(aCabec,{"C5_XMICRDE",SA1->A1_XMICRDE,Nil})

		//ASORT(aItWs,,,{ |x,y| x[1] < y[1] } )
		ASORT(aItWs,,,{ |x,y| x[4]+x[5] < y[4]+y[5] } ) //Grade: Ordenacao por Referencia 8 digitos + Valor

		//aItWs := AGRUPA()

		nVlrPre := 0 //Valor presente

		For ny := 1 to Len(aItWs)

			/*cProd   := aWsItens[ny]:cItemPartNumber   //Codigo do produto que vem da RAKUTEN
			nPosP   := Len(cProd)
			cProd   := AllTrim(If(SubStr(cProd,1,4)='OUT_',SubStr(cProd,5),cProd)) //Retira o OUT_ caso seja um produto de promocao*/

			cProd  := aItWs[ny][1]
			cCodInt:= aItWs[ny][6] //aWsItens[ny]:cIntCod
			nQtdPrd:= aItWs[ny][3]
			//nVlrPre += aItWs[ny][7]

			//Chama rotina que faz a troca do produto Nacional para Importado
			aChkEstoq:= fTradeNacImp(cProd, nQtdPrd)
			
			If lExEst:= aChkEstoq[6]//Existe estoque para o pedido?
				If lQbrPed:= aChkEstoq[1]//Havera nova linha de produto para completar estoque?
					//Caso tenha sido feita a quebra de quantidade em produtos diferentes por falta de estoque
					cProd:= aChkEstoq[2]
					nQtdPrd:= aChkEstoq[3]
					cProd2:= aChkEstoq[4]
					nQtd2Prd:= aChkEstoq[5]
				Else
					cProd:= aChkEstoq[2]
					nQtdPrd:= aChkEstoq[3]
				Endif
			Endif
			/*QQQQWEWEFEWWEFDSRWRSADEQWDFQEwefwfwfwEWFWEFWEFWEFEWFWEFWE
			nPosP := At("_",cProd)
			cRef  := SubStr(cProd,1,nPosP-1)		//Codigo da Referencia
			cProd := SubStr(cProd,nPosP+1)

			nPosP := At("_",cProd)
			cCor  := SubStr(cProd,1,nPosP-1)		//Codigo da Cor
			cProd := SubStr(cProd,nPosP+1)

			cTam  := cProd						//Codigo do Tamanho

			cProd := cRef+cCor+cTam             //Codigo do produto sem os "_"
			*/
			//Informa a quantidade de itens a serem inseridos no pedido
			nQtPr:= Iif(lQbrPed,2,1)
			
			For nXX:= 1 to Len(nQtPr)
				
				If nXX > 1 
					//Informa a quantidade e codigo do item 2 que foi dividido por falta de estoque do item 1
					cProd  := cProd2
					nQtdPrd:= nQtd2Prd
				Endif
				
				If !Empty(cProd)
					DbSelectArea("SB1")
					DbSetOrder(1)
					If !DbSeek(xFilial("SB1")+cProd)
				    	DbSelectArea("SB1")
				    	DbOrderNickName("YFORMAT")
						If !DbSeek(xFilial("SB1")+cProd)
							GravaLog("Produto: "+cProd+" n�o encontrado. Pedido: "+cPedRak)
							lContinua := .F.
							Exit
						EndIf
			    	EndIf
			    Else
			    	DbSelectArea("SB1")
		    		DbOrderNickName("YFORMAT")
					If !DbSeek(xFilial("SB1")+cCodInt)
						GravaLog("Produto: "+cCodInt+" n�o encontrado. Pedido: "+cPedRak)
						lContinua := .F.
						Exit
					EndIf
			    EndIf
	
		    	nSldPik := HFATSALDO(SB1->B1_COD,cAmzPik)
				nSldPul := HFATSALDO(SB1->B1_COD,cAmzPul)
	
				nQtde   := aItWs[ny][3]
		    	nVlrRak := aItWs[ny][2]
	
				If nQtde > (nSldPik+nSldPul)
					aAdd(aNotSld,{SB1->B1_COD})
				EndIf
	
				DbSelectArea("SBE")
				DbOrderNickName("PRODUTO")
				If !DbSeek(xFilial("SBE")+SB1->B1_COD)
					aAdd(aNotEnd,{SB1->B1_COD})
				EndIf
	
		    	If nVlrRak > 0
		    		cTpOper := cTpOpad
		    		//cTES   := cTESPad
		    		cTES  := MaTesInt(2,cTpOper,cCodCli,cLojCli,"C",SB1->B1_COD,)
			    	If Empty(cTes)
			    		GravaLog("TES nao encontrada para o produto: "+SB1->B1_COD+". Cliente Protheus: "+cCodCli+"/"+cLojCli+" Tp Operacao: "+cTpOper+". Pedido: "+cPedRak)
						lContinua := .F.
						Exit
			    	EndIf
		    		nValor := nVlrRak
		    	Else
		    		cTpOper := ""
		    		cTES    := cTESBri
		    		nValor  := 0.01
		    	EndIf
	
		    	/*cTES  := MaTesInt(2,cTpOper,cCodCli,cLojCli,"C",SB1->B1_COD,)
		    	If Empty(cTes)
		    		GravaLog("TES nao encontrada para o produto: "+SB1->B1_COD+". Cliente Protheus: "+cCodCli+"/"+cLojCli+" Tp Operacao: "+cTpOper+". Pedido: "+cPedRak)
					lContinua := .F.
					Exit
		    	EndIf*/
	
		    	cMsgPre := aItWs[ny][7]
	
		    	cDescri := GetAdvFVal("SB4","B4_DESC",xFilial("SB4")+SubStr(SB1->B1_COD,1,8),1,"")
	
				aLinha := {}
				aadd(aLinha,{"C6_ITEM",cItem,Nil})
				aadd(aLinha,{"C6_PRODUTO",SB1->B1_COD,Nil})
				aadd(aLinha,{"C6_QTDVEN",nQtde,Nil})
				aadd(aLinha,{"C6_PRCVEN",nValor,Nil})
				aadd(aLinha,{"C6_PRUNIT",nValor,Nil})
				aadd(aLinha,{"C6_VALOR",ROUND(nQtde*nValor,2),Nil})
				aadd(aLinha,{"C6_OPER",cTpOper,Nil})
				aadd(aLinha,{"C6_TES",cTES,Nil})
				aadd(aLinha,{"C6_NUM",cNroPed,Nil})
				aadd(aLinha,{"C6_LOCAL",cAmzPk,Nil})
				aadd(aLinha,{"C6_ENTREG",DDATABASE,Nil})
				aadd(aLinha,{"C6_GRADE","S",Nil})
				aadd(aLinha,{"C6_ITEMGRD",strzero(val(cItem),3),Nil})
				If !Empty(cDescri)
					aadd(aLinha,{"C6_DESCRI",cDescri,Nil})
				EndIf
				aadd(aLinha,{"C6_XPRESEN",cMsgPre,Nil})
				If aItWs[ny][8] > 0
					aadd(aLinha,{"C6_XITPRES","S",Nil})
				Else
					aadd(aLinha,{"C6_XITPRES","N",Nil})
				EndIf
				
				aadd(aItens,aLinha)
	
				cItem := SOMA1(cItem)
				
				cDescri := ""
				cTES := ""
			
			Next nXX
			
		Next ny

		If Len(aNotSld) > 0
			cLogSld := "Produtos abaixo sem saldo suficiente. Pedido Rakuten N� "+cPedRak
			GravaLog("Produtos abaixo sem saldo suficiente. Pedido Rakuten N� "+cPedRak)
			For ny := 1 to Len(aNotSld)
				GravaLog(aNotSld[ny][1])
				cLogSld += CRLF + aNotSld[ny][1]
			Next
			MemoWrite("\log_rakuten\B2C\SALDO\"+cDtTime+"_PED_"+cPedRak+".log",cLogSld)
			aNotSld := {}
			lContinua := .F.
		EndIf

		If Len(aNotEnd) > 0
			cLogEnd := "Produtos abaixo n�o possuem amarra��o com endere�o de Picking. Pedido Rakuten N� "+cPedRak+" n�o ser� importado."
			GravaLog("Produtos abaixo amarra��o com endere�o de Picking. Pedido Rakuten N� "+cPedRak+" n�o ser� importado.")
			For ny := 1 to Len(aNotEnd)
				GravaLog(aNotEnd[ny][1])
				cLogEnd += CRLF + aNotEnd[ny][1]
			Next
			MemoWrite("\log_picking\"+cDtTime+"_PED_RAK_"+cPedRak+".log",cLogEnd)
			aNotEnd := {}
			lContinua := .F.
		EndIf

		If !lContinua
			GravaLog("Erro na importa��o dos itens do pedido. Pedido N� "+cPedRak+" n�o foi importado.")
			Loop
		EndIf

		For _i := 1 to len(aItens)
			If _i <> 1
				If SubStr(aitens[_i][2][2],1,8)+AllTrim(Str(aitens[_i][5][2]*100)) = SubStr(aitens[_i-1][2][2],1,8)+AllTrim(Str(aitens[_i-1][5][2]*100)) //Grade: Alterado If para quebrar por referencia + Valor
					aItens[_i][1][2] := aItens[_i-1][1][2]
					aItens[_i][13][2] := SOMA1(aItens[_i-1][13][2])
				Else
					aItens[_i][1][2] := SOMA1(aItens[_i-1][1][2])
					aItens[_i][13][2] := "001"
				Endif
			Endif
		Next

		BEGIN TRANSACTION

		MATA410(aCabec,aItens,3)

		If lMsErroAuto
			GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedRak)
			If lMenu
				MostraErro()
			Else
				MostraErro("\log_rakuten\B2C\",cDtTime+"_PED.log")
			EndIf
			nErro++
			lMsErroAuto := .T.
		Else
			If lMenu
				If mv_par01 == 1
					MsgInfo("Pedido importado com sucesso. N� Protheus: "+cNroPed,"Aviso")
				EndIf
			EndIf
			GravaLog("Pedido Protheus n� "+cNroPed+" foi gerado com sucesso. Pedido: "+cPedRak)
			nGerados++
			aPed := {}
			aAdd(aPed,{cNroPed,cCodCli,cLojCli})
			U_HFTGER002(aPed,.T.,.F.)
			cNroPed := SOMA1(SubStr(cNroPed,2,5))
			PutMV("MV_XNMPVRK","R"+cNroPed)

			DbSelectArea("SZT")
			DbSetOrder(1)
			If DbSeek(xFilial("SZT")+cPedRak)
				RecLock("SZT",.F.)
				SZT->ZT_STATUS = "X"
				MsUnlock()
			EndIf

			//ValidPed(cPedRak)
		EndIf

		END TRANSACTION
	Next
EndIf

Return

Static Function AGRUPA()
Local aAux := aClone(aItWs)
Local nLen,ny := 0
Local aRet := {}
Local nPos := 0
Local nQtd := 0

For ny := 1 to Len(aAux)
	nPos := Ascan(aRet,{|x| x[4]+x[5] = aAux[ny][4]+aAux[ny][5] })
	If nPos > 0
		nQtd := aRet[nPos][3]
		aRet[nPos][3] := nQtd + aAux[ny][3]
	Else
		aAdd(aRet,{aAux[ny][1],aAux[ny][2],aAux[ny][3],aAux[ny][4],aAux[ny][5],aAux[ny][6]})
	EndIf
Next

Return aRet

/*
Static Function ValidPed(cPedido)

If oWsClient:Validar(0,Val(cPedido),'?',cChvA1,cChvA2)
	GravaLog("Executado comando Validar para o pedido "+cPedido)
Else
	GravaLog("Erro na funcao Validar: "+GetWSCError())
EndIf

Return
*/

Static Function GravaLog(cMsg)
Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cMsg)
cLog += CRLF+cDtHora+": HWSRK001 - "+cMsg

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Funcao:"        ,"Funcao:"        ,"Funcao:"        ,"mv_ch1","N",1,0,2,"C","","","","" ,"mv_par01","Listar","Listar","Listar","","ListarNovos","ListarNovos","ListarNovos","","","","","","","","","",{"Informe a funcao para importacao",""},{""},{""},"")
PutSx1(cPerg,"02","Pedido RAKUTEN:","Pedido Rakuten:","Pedido Rakuten:","Mv_ch2","C",6,0,0,"G","","","","N","Mv_par02",""   ,""   ,""   ,"",""   ,""   ,""   ,"","","","","","","","","",{"Informe o numero do pedido RAKUTEN",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )


Return(cPerg)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �fTradeNacImp �Autor  �R.Melo - Totalit � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Verifica se existe estoque para o produto e caso nao haja   ���
���          �o programa pega do mesmo produto porem da origem nac ou imp ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function fTradeNacImp(cProd, nQtdPrd)
Local cPrd1   := cProd
Local nQtdOrig   := nQtdPrd
Local cPrd2   := ""
Local nQtd2   := 0
Local aRet    := {}
Local lBlqDem := .f.
Local cPV	  := ""
Local lConEst := .t. //Consulta Estoque
Local aAreaSB1:= SB1->(GetArea())
Local aAreaSB2:= SB2->(GetArea())
Local cCodSeq := ''
Local cCodPrio:= ''
Local cPriori := ''
Local cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local cAmzPul := AllTrim(SuperGetMV("MV_XAMZPUL",.F.,"E1")) //Armazem padrao pulmao

dbSelectArea("ZB5")
dbSetOrder(1)
ZB5->(dbGoTop())
//Verifica se existe priorizacao de produtos, caso nao exista o pedido segue igual.
If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
	cPriori:= ZB5->ZB5_PROIRI
	cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
	//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
	cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
Else
	dbSelectArea("ZB5")
	dbSetOrder(2)
	ZB5->(dbGoTop())
	If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
		cPriori:= ZB5->ZB5_PROIRI
		cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
		//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
		cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
		//Retorna pedido original pois nao existe priorizacao cadastrada na ZB5
	Else
		aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2 , .t.}
		RestArea(aAreaSB1)
		RestArea(aAreaSB2)
		Return (aRet) 
	Endif
Endif

//Verifica aqui o Bloqueio por Demanda
cBlqDem:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YBLQDEM")
lBlqDem:= Iif(Alltrim(cBlqDem)== "S",.T.,.F.) 

If !lBlqDem
	//Verifica aqui a Situacao do Produto onde:
	//PV - Nao consulta estoque, venda liberada
	//NPV - Consulta estoque, venda so acontece com disponibilidade do produto
	cPV:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YSITUAC")
	lConEst:= Iif(Alltrim(cPV)== "PV",.f.,.t.)

	If lConEst
		dbSelectArea("SB2")
		dbSetOrder(1)
		SB2->(dbGoTop())

		nSldPik := HFATSALDO(cCodPrio,cAmzPik)
		nSldPul := HFATSALDO(cCodPrio,cAmzPul)

		nSldSecPik := HFATSALDO(cCodSeq,cAmzPik)
		nSldSecPul := HFATSALDO(cCodSeq,cAmzPul)

		//If SB2->(DbSeek(xFilial("SB2")+cCodPrio))
		If nQtdOrig<= nSldPik+nSldPul//SB2->B2_QATU
			aRet:= { .f., cCodPrio, nQtdOrig, cPrd2, nQtd2 ,.t.}
		Else
			nPriQatu:= nSldPik + nSldPul
			nSecQatu:= nSldSecPik + nSldSecPul
			nSldAtu := nQtdOrig - nPriQatu

			If nSecQatu<= nSldAtu
				nQtd2:= nSldAtu - nSecQatu
				aRet:= { .f., cCodPrio, nQtdOrig, cCodSeq, nQtd2, .t. }
			Else
				Conout('Produtos: '+cCodPrio+' e '+cCodSeq+' nao possuem estoque para contemplar pedido.')
				aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2 ,.f.}
				RestArea(aAreaSB1)
				RestArea(aAreaSB2)
				Return (aRet)
				//Produtos (Nac+Imp) juntos nao contem a quantidade total necessaria do pedido.
			Endif	
		Endif
	Else
		//Retornar aqui quantidade original + produto priorizado 
		aRet:= { .f., cCodPrio, nQtdOrig, cPrd2, nQtd2 , .t.} 
	Endif
Else
	Conout('Produto: '+cCodPrio+' com bloqueio de demanda.')
	aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2, .t. } 
	//Retornar aqui pedido original - o pedido nao podera ser feito por conta do bloqueio por demanda
Endif

RestArea(aAreaSB1)
RestArea(aAreaSB2)

Return (aRet) 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATSALDO �Autor  �R.Melo - Totalit � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Busca saldo na SB2  										 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function HFATSALDO(cProduto,cArmazem)
Local nRet := 0

DbSelectArea("SB2")
DbSetOrder(1)
If SB2->(DbSeek(xFilial("SB2")+cProduto+cArmazem))
	nRet := SB2->B2_QATU-SB2->B2_RESERVA-SB2->B2_QEMP-SB2->B2_XRESERV //SaldoSb2()
EndIf

Return nRet