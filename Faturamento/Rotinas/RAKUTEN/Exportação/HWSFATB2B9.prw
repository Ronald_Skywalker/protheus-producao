#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWEBSRV.CH"

/* ===============================================================================
WSDL Location    http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/portifolio.asmx?wsdl
Gerado em        08/03/17 16:40:39
Observa��es      C�digo-Fonte gerado por ADVPL WSDL Client 1.120703
                 Altera��es neste arquivo podem causar funcionamento incorreto
                 e ser�o perdidas caso o c�digo-fonte seja gerado novamente.
=============================================================================== */

User Function _FZUPOVR ; Return  // "dummy" function - Internal Use 

/* -------------------------------------------------------------------------------
WSDL Service WSPortifolio
------------------------------------------------------------------------------- */

WSCLIENT WSPortifolio

	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD RESET
	WSMETHOD CLONE
	WSMETHOD Salvar
	WSMETHOD Excluir
	WSMETHOD Listar
	WSMETHOD SalvarItem
	WSMETHOD ExcluirItem

	WSDATA   _URL                      AS String
	WSDATA   _HEADOUT                  AS Array of String
	WSDATA   _COOKIES                  AS Array of String
	WSDATA   nLojaCodigo               AS int
	WSDATA   cCodIntPort  AS string
	WSDATA   cNomePortifolio           AS string
	WSDATA   cDescricaoPortifolio      AS string
	WSDATA   nStatusPortifolio         AS int
	WSDATA   cA1                       AS string
	WSDATA   cA2                       AS string
	WSDATA   oWS                       AS SCHEMA
	WSDATA   oWSSalvarResult           AS Portifolio_clsRetornoOfclsPortifolio
	WSDATA   oWSExcluirResult          AS Portifolio_clsRetornoOfclsPortifolio
	WSDATA   oWSListarResult           AS Portifolio_clsRetornoOfclsPortifolio
	WSDATA   cCodIntPrd     AS string
	WSDATA   cPartNumber               AS string
	WSDATA   nStatusPortifolioItem     AS int
	WSDATA   oWSSalvarItemResult       AS Portifolio_clsRetornoOfclsPortifolio
	WSDATA   oWSExcluirItemResult      AS Portifolio_clsRetornoOfclsPortifolio

ENDWSCLIENT

WSMETHOD NEW WSCLIENT WSPortifolio
::Init()
If !FindFunction("XMLCHILDEX")
	UserException("O C�digo-Fonte Client atual requer os execut�veis do Protheus Build [7.00.131227A-20170624 NG] ou superior. Atualize o Protheus ou gere o C�digo-Fonte novamente utilizando o Build atual.")
EndIf
If val(right(GetWSCVer(),8)) < 1.040504
	UserException("O C�digo-Fonte Client atual requer a vers�o de Lib para WebServices igual ou superior a ADVPL WSDL Client 1.040504. Atualize o reposit�rio ou gere o C�digo-Fonte novamente utilizando o reposit�rio atual.")
EndIf
Return Self

WSMETHOD INIT WSCLIENT WSPortifolio
	::oWS                := NIL 
	::oWSSalvarResult    := Portifolio_CLSRETORNOOFCLSPORTIFOLIO():New()
	::oWSExcluirResult   := Portifolio_CLSRETORNOOFCLSPORTIFOLIO():New()
	::oWSListarResult    := Portifolio_CLSRETORNOOFCLSPORTIFOLIO():New()
	::oWSSalvarItemResult := Portifolio_CLSRETORNOOFCLSPORTIFOLIO():New()
	::oWSExcluirItemResult := Portifolio_CLSRETORNOOFCLSPORTIFOLIO():New()
Return

WSMETHOD RESET WSCLIENT WSPortifolio
	::nLojaCodigo        := NIL 
	::cCodIntPort := NIL 
	::cNomePortifolio    := NIL 
	::cDescricaoPortifolio := NIL 
	::nStatusPortifolio  := NIL 
	::cA1                := NIL 
	::cA2                := NIL 
	::oWS                := NIL 
	::oWSSalvarResult    := NIL 
	::oWSExcluirResult   := NIL 
	::oWSListarResult    := NIL 
	::cCodIntPrd := NIL 
	::cPartNumber        := NIL 
	::nStatusPortifolioItem := NIL 
	::oWSSalvarItemResult := NIL 
	::oWSExcluirItemResult := NIL 
	::Init()
Return

WSMETHOD CLONE WSCLIENT WSPortifolio
Local oClone := WSPortifolio():New()
	oClone:_URL          := ::_URL 
	oClone:nLojaCodigo   := ::nLojaCodigo
	oClone:cCodIntPort := ::cCodIntPort
	oClone:cNomePortifolio := ::cNomePortifolio
	oClone:cDescricaoPortifolio := ::cDescricaoPortifolio
	oClone:nStatusPortifolio := ::nStatusPortifolio
	oClone:cA1           := ::cA1
	oClone:cA2           := ::cA2
	oClone:oWSSalvarResult :=  IIF(::oWSSalvarResult = NIL , NIL ,::oWSSalvarResult:Clone() )
	oClone:oWSExcluirResult :=  IIF(::oWSExcluirResult = NIL , NIL ,::oWSExcluirResult:Clone() )
	oClone:oWSListarResult :=  IIF(::oWSListarResult = NIL , NIL ,::oWSListarResult:Clone() )
	oClone:cCodIntPrd := ::cCodIntPrd
	oClone:cPartNumber   := ::cPartNumber
	oClone:nStatusPortifolioItem := ::nStatusPortifolioItem
	oClone:oWSSalvarItemResult :=  IIF(::oWSSalvarItemResult = NIL , NIL ,::oWSSalvarItemResult:Clone() )
	oClone:oWSExcluirItemResult :=  IIF(::oWSExcluirItemResult = NIL , NIL ,::oWSExcluirItemResult:Clone() )
Return oClone

// WSDL Method Salvar of Service WSPortifolio

WSMETHOD Salvar WSSEND nLojaCodigo,cCodIntPort,cNomePortifolio,cDescricaoPortifolio,nStatusPortifolio,cA1,cA2,oWS WSRECEIVE oWSSalvarResult WSCLIENT WSPortifolio
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<Salvar xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CodigoInternoPortifolio", ::cCodIntPort, cCodIntPort , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("NomePortifolio", ::cNomePortifolio, cNomePortifolio , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("DescricaoPortifolio", ::cDescricaoPortifolio, cDescricaoPortifolio , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("StatusPortifolio", ::nStatusPortifolio, nStatusPortifolio , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</Salvar>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/Salvar",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/portifolio.asmx")

::Init()
::oWSSalvarResult:SoapRecv( WSAdvValue( oXmlRet,"_SALVARRESPONSE:_SALVARRESULT","clsRetornoOfclsPortifolio",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method Excluir of Service WSPortifolio

WSMETHOD Excluir WSSEND nLojaCodigo,cCodIntPort,cA1,cA2,oWS WSRECEIVE oWSExcluirResult WSCLIENT WSPortifolio
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<Excluir xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CodigoInternoPortifolio", ::cCodIntPort, cCodIntPort , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</Excluir>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/Excluir",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/portifolio.asmx")

::Init()
::oWSExcluirResult:SoapRecv( WSAdvValue( oXmlRet,"_EXCLUIRRESPONSE:_EXCLUIRRESULT","clsRetornoOfclsPortifolio",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method Listar of Service WSPortifolio

WSMETHOD Listar WSSEND nLojaCodigo,cCodIntPort,nStatusPortifolio,cA1,cA2,oWS WSRECEIVE oWSListarResult WSCLIENT WSPortifolio
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<Listar xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CodigoInternoPortifolio", ::cCodIntPort, cCodIntPort , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("StatusPortifolio", ::nStatusPortifolio, nStatusPortifolio , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</Listar>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/Listar",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/portifolio.asmx")

::Init()
::oWSListarResult:SoapRecv( WSAdvValue( oXmlRet,"_LISTARRESPONSE:_LISTARRESULT","clsRetornoOfclsPortifolio",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method SalvarItem of Service WSPortifolio

WSMETHOD SalvarItem WSSEND nLojaCodigo,cCodIntPort,cCodIntPrd,cPartNumber,nStatusPortifolioItem,cA1,cA2,oWS WSRECEIVE oWSSalvarItemResult WSCLIENT WSPortifolio
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<SalvarItem xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CodigoInternoPortifolio", ::cCodIntPort, cCodIntPort , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CodigoInternoProduto", ::cCodIntPrd, cCodIntPrd , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("PartNumber", ::cPartNumber, cPartNumber , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("StatusPortifolioItem", ::nStatusPortifolioItem, nStatusPortifolioItem , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</SalvarItem>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/SalvarItem",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/portifolio.asmx")

::Init()
::oWSSalvarItemResult:SoapRecv( WSAdvValue( oXmlRet,"_SALVARITEMRESPONSE:_SALVARITEMRESULT","clsRetornoOfclsPortifolio",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method ExcluirItem of Service WSPortifolio

WSMETHOD ExcluirItem WSSEND nLojaCodigo,cCodIntPort,cCodIntPrd,cPartNumber,cA1,cA2,oWS WSRECEIVE oWSExcluirItemResult WSCLIENT WSPortifolio
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<ExcluirItem xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CodigoInternoPortifolio", ::cCodIntPort, cCodIntPort , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CodigoInternoProduto", ::cCodIntPrd, cCodIntPrd , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("PartNumber", ::cPartNumber, cPartNumber , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</ExcluirItem>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/ExcluirItem",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/portifolio.asmx")

::Init()
::oWSExcluirItemResult:SoapRecv( WSAdvValue( oXmlRet,"_EXCLUIRITEMRESPONSE:_EXCLUIRITEMRESULT","clsRetornoOfclsPortifolio",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.


// WSDL Data Structure clsRetornoOfclsPortifolio

WSSTRUCT Portifolio_clsRetornoOfclsPortifolio
	WSDATA   cAcao                     AS string OPTIONAL
	WSDATA   cData                     AS dateTime
	WSDATA   nCodigo                   AS int
	WSDATA   cDescricao                AS string OPTIONAL
	WSDATA   oWSLista                  AS Portifolio_ArrayOfClsPortifolio OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Portifolio_clsRetornoOfclsPortifolio
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Portifolio_clsRetornoOfclsPortifolio
Return

WSMETHOD CLONE WSCLIENT Portifolio_clsRetornoOfclsPortifolio
	Local oClone := Portifolio_clsRetornoOfclsPortifolio():NEW()
	oClone:cAcao                := ::cAcao
	oClone:cData                := ::cData
	oClone:nCodigo              := ::nCodigo
	oClone:cDescricao           := ::cDescricao
	oClone:oWSLista             := IIF(::oWSLista = NIL , NIL , ::oWSLista:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Portifolio_clsRetornoOfclsPortifolio
	Local oNode5
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cAcao              :=  WSAdvValue( oResponse,"_ACAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cData              :=  WSAdvValue( oResponse,"_DATA","dateTime",NIL,"Property cData as s:dateTime on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::nCodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,"Property nCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::cDescricao         :=  WSAdvValue( oResponse,"_DESCRICAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	oNode5 :=  WSAdvValue( oResponse,"_LISTA","ArrayOfClsPortifolio",NIL,NIL,NIL,"O",NIL,NIL) 
	If oNode5 != NIL
		::oWSLista := Portifolio_ArrayOfClsPortifolio():New()
		::oWSLista:SoapRecv(oNode5)
	EndIf
Return

// WSDL Data Structure ArrayOfClsPortifolio

WSSTRUCT Portifolio_ArrayOfClsPortifolio
	WSDATA   oWSclsPortifolio          AS Portifolio_clsPortifolio OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Portifolio_ArrayOfClsPortifolio
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Portifolio_ArrayOfClsPortifolio
	::oWSclsPortifolio     := {} // Array Of  Portifolio_CLSPORTIFOLIO():New()
Return

WSMETHOD CLONE WSCLIENT Portifolio_ArrayOfClsPortifolio
	Local oClone := Portifolio_ArrayOfClsPortifolio():NEW()
	oClone:oWSclsPortifolio := NIL
	If ::oWSclsPortifolio <> NIL 
		oClone:oWSclsPortifolio := {}
		aEval( ::oWSclsPortifolio , { |x| aadd( oClone:oWSclsPortifolio , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Portifolio_ArrayOfClsPortifolio
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_CLSPORTIFOLIO","clsPortifolio",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSclsPortifolio , Portifolio_clsPortifolio():New() )
			::oWSclsPortifolio[len(::oWSclsPortifolio)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure clsPortifolio

WSSTRUCT Portifolio_clsPortifolio
	WSDATA   cPortifolioCodigoInterno  AS string OPTIONAL
	WSDATA   cNome                     AS string OPTIONAL
	WSDATA   cDescricao                AS string OPTIONAL
	WSDATA   oWSStatus                 AS Portifolio_PortifolioStatus
	WSDATA   nLojaCodigo               AS int
	WSDATA   oWSItens                  AS Portifolio_ArrayOfClsPortifolioItem OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Portifolio_clsPortifolio
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Portifolio_clsPortifolio
Return

WSMETHOD CLONE WSCLIENT Portifolio_clsPortifolio
	Local oClone := Portifolio_clsPortifolio():NEW()
	oClone:cPortifolioCodigoInterno := ::cPortifolioCodigoInterno
	oClone:cNome                := ::cNome
	oClone:cDescricao           := ::cDescricao
	oClone:oWSStatus            := IIF(::oWSStatus = NIL , NIL , ::oWSStatus:Clone() )
	oClone:nLojaCodigo          := ::nLojaCodigo
	oClone:oWSItens             := IIF(::oWSItens = NIL , NIL , ::oWSItens:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Portifolio_clsPortifolio
	Local oNode4
	Local oNode6
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cPortifolioCodigoInterno :=  WSAdvValue( oResponse,"_PORTIFOLIOCODIGOINTERNO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cNome              :=  WSAdvValue( oResponse,"_NOME","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cDescricao         :=  WSAdvValue( oResponse,"_DESCRICAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	oNode4 :=  WSAdvValue( oResponse,"_STATUS","PortifolioStatus",NIL,"Property oWSStatus as tns:PortifolioStatus on SOAP Response not found.",NIL,"O",NIL,NIL) 
	If oNode4 != NIL
		::oWSStatus := Portifolio_PortifolioStatus():New()
		::oWSStatus:SoapRecv(oNode4)
	EndIf
	::nLojaCodigo        :=  WSAdvValue( oResponse,"_LOJACODIGO","int",NIL,"Property nLojaCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	oNode6 :=  WSAdvValue( oResponse,"_ITENS","ArrayOfClsPortifolioItem",NIL,NIL,NIL,"O",NIL,NIL) 
	If oNode6 != NIL
		::oWSItens := Portifolio_ArrayOfClsPortifolioItem():New()
		::oWSItens:SoapRecv(oNode6)
	EndIf
Return

// WSDL Data Enumeration PortifolioStatus

WSSTRUCT Portifolio_PortifolioStatus
	WSDATA   Value                     AS string
	WSDATA   cValueType                AS string
	WSDATA   aValueList                AS Array Of string
	WSMETHOD NEW
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Portifolio_PortifolioStatus
	::Value := NIL
	::cValueType := "string"
	::aValueList := {}
	aadd(::aValueList , "Nenhum" )
	aadd(::aValueList , "Ativo" )
	aadd(::aValueList , "Inativo" )
Return Self

WSMETHOD SOAPSEND WSCLIENT Portifolio_PortifolioStatus
	Local cSoap := "" 
	cSoap += WSSoapValue("Value", ::Value, NIL , "string", .F. , .F., 3 , NIL, .F.,.F.) 
Return cSoap

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Portifolio_PortifolioStatus
	::Value := NIL
	If oResponse = NIL ; Return ; Endif 
	::Value :=  oResponse:TEXT
Return 

WSMETHOD CLONE WSCLIENT Portifolio_PortifolioStatus
Local oClone := Portifolio_PortifolioStatus():New()
	oClone:Value := ::Value
Return oClone

// WSDL Data Structure ArrayOfClsPortifolioItem

WSSTRUCT Portifolio_ArrayOfClsPortifolioItem
	WSDATA   oWSclsPortifolioItem      AS Portifolio_clsPortifolioItem OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Portifolio_ArrayOfClsPortifolioItem
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Portifolio_ArrayOfClsPortifolioItem
	::oWSclsPortifolioItem := {} // Array Of  Portifolio_CLSPORTIFOLIOITEM():New()
Return

WSMETHOD CLONE WSCLIENT Portifolio_ArrayOfClsPortifolioItem
	Local oClone := Portifolio_ArrayOfClsPortifolioItem():NEW()
	oClone:oWSclsPortifolioItem := NIL
	If ::oWSclsPortifolioItem <> NIL 
		oClone:oWSclsPortifolioItem := {}
		aEval( ::oWSclsPortifolioItem , { |x| aadd( oClone:oWSclsPortifolioItem , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Portifolio_ArrayOfClsPortifolioItem
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_CLSPORTIFOLIOITEM","clsPortifolioItem",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSclsPortifolioItem , Portifolio_clsPortifolioItem():New() )
			::oWSclsPortifolioItem[len(::oWSclsPortifolioItem)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure clsPortifolioItem

WSSTRUCT Portifolio_clsPortifolioItem
	WSDATA   cProdutoCodigoInterno     AS string OPTIONAL
	WSDATA   cPartNumber               AS string OPTIONAL
	WSDATA   oWSStatus                 AS Portifolio_PortifolioStatus
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Portifolio_clsPortifolioItem
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Portifolio_clsPortifolioItem
Return

WSMETHOD CLONE WSCLIENT Portifolio_clsPortifolioItem
	Local oClone := Portifolio_clsPortifolioItem():NEW()
	oClone:cProdutoCodigoInterno := ::cProdutoCodigoInterno
	oClone:cPartNumber          := ::cPartNumber
	oClone:oWSStatus            := IIF(::oWSStatus = NIL , NIL , ::oWSStatus:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Portifolio_clsPortifolioItem
	Local oNode3
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cProdutoCodigoInterno :=  WSAdvValue( oResponse,"_PRODUTOCODIGOINTERNO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cPartNumber        :=  WSAdvValue( oResponse,"_PARTNUMBER","string",NIL,NIL,NIL,"S",NIL,NIL) 
	oNode3 :=  WSAdvValue( oResponse,"_STATUS","PortifolioStatus",NIL,"Property oWSStatus as tns:PortifolioStatus on SOAP Response not found.",NIL,"O",NIL,NIL) 
	If oNode3 != NIL
		::oWSStatus := Portifolio_PortifolioStatus():New()
		::oWSStatus:SoapRecv(oNode3)
	EndIf
Return


