#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA005  �Autor  �Bruno Parreira      � Data �  02/13/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Exportacao de Estoque B2C                                   ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATA005()

Local oWs     	:= NIL
Local oWsEstq	:= NIL
Local oWsPrdFil := NIL
Local oWsPrdPai := NIL
Local aWsRet  	:= {}
Local cWsRet	:= ""
Local cQryEstq	:= ""
Local nQtEstq	:= 0
Local nQtdMin	:= 5 //Criar par�metro para retornar quantidade m�nima de produto
Local nx        := 0

Private cChvA1   := ""
Private cChvA2   := ""
Private cLog     := ""
Private cPerg := "HFATA005"
Private cHoraIni := Time()
Private cHoraFim := ""
Private cMinEst  := ""
Private nMinEst  := 0

lMenu := .f.
If Select("SX2") <> 0
	lMenu := .t.
Else
	Prepare Environment Empresa "01" Filial "0101"
Endif

cMinEst  := AllTrim(SuperGetMV("MV_XSLDMIN",.F.,"10"))
nMinEst  := Val(cMinEst)

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

GravaLog("In�cio do processamento...")

oWsEstq	  := WSEstoque():New()
oWsPrdFil := WSSKU():New()
oWsPrdPai := WSProduto():New()

cChvA1 := AllTrim(SuperGetMV("MV_XRA1B2C",.F.,"8B3CD696-D70A-4E59-8E2D-24A96ED8115E")) //Chave A1 para validar acesso ao webservice
cChvA2 := AllTrim(SuperGetMV("MV_XRA2B2C",.F.,"2DD1CBF7-4B95-43BA-9F16-2C295E80E8CF")) //Chave A2 para validar acesso ao webservice

If !lMenu
	GravaLog("Executando funcao atualizar estoque automatica")
	xFiltros()
Else
	GravaLog("Executando via menu.")
	AjustaSX1(cPerg)
	If !Pergunte(cPerg)
		GravaLog("Processo cancelado pelo usu�rio.")
		Return
	Else
		//If Empty(mv_par01)
		//	MsgAlert("N�o foi digitado nenhum produto.","Aten��o")
		//	Return	
		//EndIf
		xFiltros(AllTrim(mv_par01))	
	EndIf
EndIf

nPrd := 0	
cRef := ""
lInativ := .F.

//-----------------------------//
//--- Exporta��o do Estoque ---//
DbSelectArea("TRB")
TRB->(dbGoTop())
If TRB->(!Eof())
	While TRB->(!Eof())
		If !Empty(TRB->CODRAK)
			cProdPrt	:= AllTrim(TRB->CODPROT)
			cPartNbr	:= AllTrim(TRB->CODRAK)
		Else
			cProdPrt	:= AllTrim(TRB->CODPROT)
			cPartNbr	:= AllTrim(TRB->CODPROT)
		EndIf
		
		cRef := SubStr(cProdPrt,1,8)
		
		For nx := 1 to 1//2
			
			If nx = 2
				cProdPrt := "OUT_"+cProdPrt
				cPartNbr := "OUT_"+cPartNbr
			EndIf

			If oWsEstq:Salvar(0,cPartNbr,cPartNbr,IF(TRB->TOTAL_E0E1<=0,0,TRB->TOTAL_E0E1), nMinEst, 3,cChvA1,cChvA2)			
				cWsRet := oWsEstq:oWSSalvarResult:cDescricao
				GravaLog("Produto: "+TRB->CodProt+" - "+cWsRet)
			Else			
				GravaLog("Erro na funcao Salvar o Estoque: "+GetWSCError())
				Loop		
			EndIf
			
			If TRB->TOTAL_E0E1 > nMinEst
				oWsPrdFil:AlterarStatus( 0 , cPartNbr , 1 ,cChvA1,cChvA2)
				cWsRet := oWsPrdFil:oWSAlterarStatusResult:cDescricao
				GravaLog("Parametros Ativa Produto oWsPrdFil:AlterarStatus: 0,"+cPartNbr+",1")
				If !Empty(cWsRet)
					GravaLog("Resultado Ativa Produto: oWsPrdFil:AlterarStatus: "+cPartNbr + " - " + cWsRet)
				EndIf
			//EndIf
			Else
			//If TRB->TOTAL_E0E1 <= nMinEst
				oWsPrdFil:AlterarStatus( 0 , cPartNbr , 2 ,cChvA1,cChvA2)
				cWsRet := oWsPrdFil:oWSAlterarStatusResult:cDescricao
				GravaLog("Parametros Inativa Produto oWsPrdFil:AlterarStatus: 0,"+cPartNbr+",2")
				If !Empty(cWsRet)
					GravaLog("Resultado Inativa Produto: oWsPrdFil:AlterarStatus: " + cPartNbr + " - " + cWsRet)
				EndIf
			EndIf
		Next
		
		TRB->(dbSkip())
		
		If cRef <> SubStr(TRB->CODPROT,1,8)
			lInativ := SLDFILHOS(cRef)
			If lInativ
				oWsPrdPai:AlterarStatus(0,cRef,2,cChvA1,cChvA2)
				cWsRet := oWsPrdPai:oWSAlterarStatusResult:cDescricao
				GravaLog("Resultado Inativa Produto: oWsPrdPai:AlterarStatus: "+cRef+" - "+cWsRet)
			Else
				oWsPrdPai:AlterarStatus(0,cRef,1,cChvA1,cChvA2)
				cWsRet := oWsPrdPai:oWSAlterarStatusResult:cDescricao
				GravaLog("Resultado Ativa Produto: oWsPrdPai:AlterarStatus: "+cRef+" - "+cWsRet)
			EndIf
		EndIf
		
		If nPrd > 50
			cTime   := Time()
			cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
			MemoWrite("\log_rakuten\B2C\"+cDtTime+"_EXP-ESTQ.log",cLog)
			cLog := "_"
			nPrd := 0
		EndIf
		
	EndDo		
Else
	GravaLog("Produto n�o encontrado.")
EndIf

GravaLog("Fim do processamento.")

MemoWrite("\log_rakuten\B2C\"+cDtTime+"_EXP-ESTQ.log",cLog)

cHoraFim := Time()

If lMenu
	MsgInfo("Extra��o de estoque finalizada."+cHoraIni+" - "+cHoraFim,"Aviso")
EndIf

TRB->(DbCloseArea())

Return

//--- Log do processamento
Static Function GravaLog(cMsg)
Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cDtHora+": HFATA005 - "+cMsg)
cLog += CRLF+cDtHora+": HFATA005 - "+cMsg

Return

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

  
PutSx1(cPerg,"01","Produto?","Produto?","Produto?","Mv_ch1","C",15,0,0,"G","","","","N","Mv_par01",""   ,""   ,""   ,"",""   ,""   ,""   ,"","","","","","","","","",{"Atualiza qual Produto? (Vazio atualiza todos)",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)

//---------------------------------//
//--- Regras Exporta��o Estoque ---//
Static Function xFiltros(cProduto)

Local cQuery	:= GetNextAlias()
Local cWhere	:= ""
Local cArmazem	:= AllTrim(SuperGetMV("MV_XARMB2C",.F.,"'E0','E1'"))
Local cTabDE	:= SuperGetMV("MV_XTABDC",.F.,"042")
Local cTabPOR	:= SuperGetMV("MV_XTABPC",.F.,"009")

If !Empty(cProduto)
	cWhere := "SB1.B1_YDWEB = 'S' "
	If Len(cProduto) <= 8 
		cWhere += "AND SB1.B1_COD like '"+AllTrim(cProduto)+"%' "
	Else
		cWhere += "AND SB1.B1_COD = '"+AllTrim(cProduto)+"' "
	EndIf	
Else
	cWhere := "SB1.B1_YDWEB = 'S' "
EndIf

//BeginSql Alias cQuery
	cQuery := "SELECT * FROM ( "
	cQuery += CRLF + "SELECT SB2.B2_COD as CODPROT, SB1.B1_YFORMAT as CODRAK, SUM(SB2.B2_QATU)-SUM(SB2.B2_QEMP)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV) as TOTAL_E0E1, "
	cQuery += CRLF + "(SELECT DA1_PRCVEN FROM "+RetSqlName("DA1")+" DA11 WHERE DA11.DA1_FILIAL = '"+xFilial("DA1")+"' AND DA11.D_E_L_E_T_ = '' AND DA11.DA1_CODTAB = '"+cTabDE+"' AND DA11.DA1_CODPRO = SB2.B2_COD ) AS PRCDE, "
	cQuery += CRLF + "(SELECT DA1_PRCVEN FROM "+RetSqlName("DA1")+" DA12 WHERE DA12.DA1_FILIAL = '"+xFilial("DA1")+"' AND DA12.D_E_L_E_T_ = '' AND DA12.DA1_CODTAB = '"+cTabPOR+"' AND DA12.DA1_CODPRO = SB2.B2_COD ) AS PRCPOR "
	cQuery += CRLF + "FROM "+RetSqlName("SB2")+" SB2 "
	cQuery += CRLF + "INNER JOIN "+RetSqlName("SB1")+" SB1  "
	cQuery += CRLF + "		ON SB1.B1_FILIAL = '"+xFilial("SB1")+"' " 
	cQuery += CRLF + "		AND SB1.B1_COD = SB2.B2_COD "
	cQuery += CRLF + "		AND SB1.D_E_L_E_T_ = '' "
	If !Empty(cProduto)
		cQuery += CRLF + "		AND "+cWhere
	Else
		cQuery += CRLF + "       AND SB1.B1_YDWEB = 'S' "
	EndIf	
	cQuery += CRLF + "		AND SB1.B1_YCOLECA IN	( SELECT ZAA.ZAA_CODIGO  " 
	cQuery += CRLF + "								  FROM "+RetSqlName("ZAA")+" ZAA "
	cQuery += CRLF + "								  WHERE ZAA.ZAA_FILIAL = '"+xFilial("ZAA")+"' "
	cQuery += CRLF + "								  AND ZAA.D_E_L_E_T_ = '' "
	cQuery += CRLF + "								  AND ZAA.ZAA_DWEB = 'S' "
	cQuery += CRLF + "								) "
	cQuery += CRLF + "		AND SB1.B1_YSITUAC IN	( SELECT ZA8.ZA8_CODIGO  "
	cQuery += CRLF + "								  FROM "+RetSqlName("ZA8")+" ZA8 "
	cQuery += CRLF + "								  WHERE ZA8.ZA8_FILIAL = '"+xFilial("ZA8")+"' "
	cQuery += CRLF + "								  AND ZA8.D_E_L_E_T_ = '' "
	cQuery += CRLF + "								  AND ZA8.ZA8_DWEB = 'T' "
	cQuery += CRLF + "								) "
	cQuery += CRLF + "		AND SB1.B1_COD IN 	( SELECT DA1.DA1_CODPRO "
	cQuery += CRLF + "								  FROM "+RetSqlName("DA1")+" DA1 "
	cQuery += CRLF + "								  WHERE DA1.DA1_FILIAL = '"+xFilial("DA1")+"' "
	cQuery += CRLF + "								  AND DA1.D_E_L_E_T_ = '' "
	cQuery += CRLF + "								  AND DA1.DA1_CODTAB IN ( '"+cTabDE+"' , '"+cTabPOR+"' ) "
	cQuery += CRLF + "								) "
	cQuery += CRLF + "		AND SB1.B1_YFORMAT <> '' "  //Retirar quando passar para producao
	cQuery += CRLF + "WHERE 	SB2.B2_FILIAL = '"+xFilial("SB2")+"' "
	cQuery += CRLF + "		AND SB2.D_E_L_E_T_ = '' "
	cQuery += CRLF + "		AND SB2.B2_LOCAL IN ("+cArmazem+") "
	//cQuery += CRLF + "		AND SB2.B2_MSEXP = '' "
			
	cQuery += CRLF + "GROUP BY SB2.B2_COD, SB1.B1_YFORMAT ) Z "
	cQuery += CRLF + "WHERE PRCPOR > 0 AND PRCDE >= PRCPOR "
	cQuery += CRLF + "ORDER BY CODPROT "
	
	MemoWrite("HFATA005.txt",cQuery)
		    
    cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)
						
//EndSql

Return cQuery

Static Function SLDFILHOS(cPai)
Local lRet     := .T.
Local cQuery   := ""
Local cArmazem := AllTrim(SuperGetMV("MV_XARMB2C",.F.,"'E0','E1'"))

cQuery := "SELECT B2_COD,SUM(SB2.B2_QATU)-SUM(SB2.B2_QEMP)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV) as TOTAL_E0E1 "
cQuery += CRLF + "FROM "+RetSqlName("SB2")+" SB2 "
cQuery += CRLF + "WHERE 	SB2.B2_FILIAL = '"+xFilial("SB2")+"' "
cQuery += CRLF + "		AND SB2.B2_COD like '"+AllTrim(cPai)+"%' "
cQuery += CRLF + "		AND SB2.D_E_L_E_T_ = '' "
cQuery += CRLF + "		AND SB2.B2_LOCAL IN ("+cArmazem+") "
cQuery += CRLF + "GROUP BY SB2.B2_COD

MemoWrite("HFATA004_SLD.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"SLD", .F., .T.)

DbSelectArea("SLD")

If SLD->(!EOF())
	While SLD->(!EOF())
		If SLD->TOTAL_E0E1 > nMinEst 
			lRet := .F.
			Exit
		EndIf
		SLD->(DbSkip())
	EndDo	
EndIf

SLD->(DbCloseArea())

Return lRet