#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA004  �Autor  �Bruno Parreira      � Data �  02/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Exportacao de produtos B2C                                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATA004() 

Local cQryPai	:= ""
Local cQryFilho	:= ""
Local cQryEstq	:= ""
Local cProdFil	:= ""
Local cPartNbr	:= ""
Local cCor		:= ""
Local cTam		:= ""
Local nPreco	:= 0
Local cTabDE	:= ""
Local nPrecoDE	:= 0
Local cTabPOR	:= ""
Local nPrecoPOR	:= 0
Local cCodPai	:= ""
Local cDescPai	:= ""
Local cDescPrd	:= ""
Local cDescLvg	:= ""
Local cGrupo	:= ""
Local cSubGrp	:= ""
Local cCateg	:= ""
Local nI		:= 1
Local aCat		:= {}
Local nPos		:= 0
Local nx        := 0
Local yy        := 0
Local ne        := 0

Private cChvA1	:= ""
Private cChvA2	:= ""
Private cLog	:= ""
Private lMsErroAuto	:= .F.
Private cPerg	:= "HFATA004"

Private cArmazem := ""
Private cMinEst  := ""
Private cOutlet  := ""
Private nMinimo  := 0
Private nMinEst  := 0//Val(cMinEst)
Private oWs     	:= NIL
Private oWsPrdPai	:= NIL
Private oWsPrdFil	:= NIL
Private oWsPrdCat	:= NIL
Private oWsEstq	:= NIL
Private oWSskuA	:= NIL
Private oWSskuB	:= NIL
Private cWsRet  	:= ""
Private aWsRet	:= {}

Private cHoraIni := Time()
Private cHoraFim := ""

Private cTabDE	:= ""
Private cTabPOR	:= ""

Private cCodSub := ""
Private cCodOca := ""
Private cSubExc := "" 

Static oWsCat   	:= NIL
Static oWsFilPrd   	:= NIL

//---------- PREPARA��O DO AMBIENTE ----------//
lMenu := .f.
If Select("SX2") <> 0
	lMenu := .t.
Else
	Prepare Environment Empresa "01" Filial "0101"
Endif

cArmazem := AllTrim(SuperGetMV("MV_XARMB2C",.F.,"'E0','E1'")) //Armazem pulmao e picking
cMinEst  := AllTrim(SuperGetMV("MV_XSLDMIN",.F.,"10"))  //Saldo minimo para exportacao de estoque
cOutlet  := AllTrim(SuperGetMV("MV_XOUTLET",.F.,"817073652"))  //Codigo da categoria de outlet B2C
cTabDE	 := SuperGetMV("MV_XTABDC",.F.,"042")  //Tabela de preco B2C "DE"
cTabPOR	 := SuperGetMV("MV_XTABPC",.F.,"009")  //Tabela de preco B2C "POR"
cCodSub  := SuperGetMV("MV_XOCASIA",.F.,"2504210593") //Codigo da Sub-Colecao Rakuten B2C
cCodOca  := SuperGetMV("MV_XSUBCOL",.F.,"2714730602") //Codigo da Ocasiao R-kuten B2C
cSubExc  := SuperGetMV("MV_XSUBEXC",.F.,"00090") //Codigo da sub-colecao que nao sera considerada no estoque minimo 

nMinimo  := Val(cMinEst)

cTime    := Time()
cDtTime  := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

GravaLog("In�cio do processamento...") 

oWsPrdPai := WSProduto():New()
oWsPrdFil := WSSKU():New()
oWsPrdCat := WSProdutoCategoria():New()
oWsEstq	  := WSEstoque():New()
oWsCat    := WSCategoria():New()
oWsFilPrd := WSFiltroProduto():New()

cChvA1 := AllTrim(SuperGetMV("MV_XRA1B2C",.F.,"8B3CD696-D70A-4E59-8E2D-24A96ED8115E")) //Chave A1 para validar acesso ao webservice
cChvA2 := AllTrim(SuperGetMV("MV_XRA2B2C",.F.,"2DD1CBF7-4B95-43BA-9F16-2C295E80E8CF")) //Chave A2 para validar acesso ao webservice

lProdPai := .F.
lSku     := .T.
If lMenu
	GravaLog("Executando via menu.")
	AjustaSX1(cPerg)
	If !Pergunte(cPerg)
		GravaLog("Processo cancelado pelo usu�rio.")
		Return
	Else
		//If Empty(mv_par01)
		//	MsgAlert("N�o foi digitado nenhum produto.","Aten��o")
		//	Return
		//Else
			If Len( AllTrim( mv_par01 ) ) <= 8
				lProdPai := .T.
			EndIf	
		//EndIf	
	EndIf
Else
	GravaLog("Executando exportacao automatica")
	lProPai := .T.
	mv_par01 := ""	
EndIf

nPrd := 0
aExcCat := {}
aCateg  := {}
lOutlet := .F.
lInativ := .F.
	
//---------------------------------//
//--- Exporta��o do Produto Pai ---//
If lProdPai
	GravaLog("Executando Produto Pai")
	
	ATUCAT(mv_par01)
	
	xFiltros(IF(lMenu,AllTrim(mv_par01),""), "PAI" )			
	TRBPAI->(dbGoTop())
	
	If TRBPAI->(!EOF())
		While TRBPAI->(!EOF()) 
			cCodPai	:= AllTrim(TRBPAI->B4_COD)
			cDescPai:= AllTrim(TRBPAI->B4_DESC)
			cDescPrd:= AllTrim(TRBPAI->DESCPT)+" -"
			cGrupo	:= AllTrim(TRBPAI->CATGRUPO)
			cCateg	:= AllTrim(TRBPAI->CATCATEGORIA)
			//cSubGrp	:= AllTrim(TRBPAI->CATSUBGRUPO)
			nPreco	:= TRBPAI->PRCDE
			
			If TRBPAI->B4_YSUBCOL $ cSubExc
				nMinEst := 0
			Else
				nMinEst := nMinimo
			EndIf
			
			lInativ := SLDFILHOS(cCodPai,nMinEst)   // Saldo total dos produtos filhos
			
			cLavag := StrTran(TRBPAI->LAVAG,"|",", ")
			cTecid := AllTrim(TRBPAI->B4_YTECIDO)
			cCompo := AllTrim(TRBPAI->COMPO)+";"
			nDe    := 1
			nAte   := 0
			cDComp := ""
			
			//Composicao
			If !Empty(cCompo)
				nAte := At("-",cCompo)
				While nDe < Len(cCompo)
					cDComp += AllTrim(Str(Val(SubStr(cCompo,nAte+1,3))))+"% "+SubStr(cCompo,nDe,nAte-nDe)+", "
					nDe  := At(";",cCompo,nAte)+1
					nAte := At("-",cCompo,nDe)
				Enddo
			EndIf
			
			//Descricao do produto
			nDe    := 1
			nAte   := 0
			cDPrd  := ""
			If !Empty(cDescPrd)
				nAte   := At("-",cDescPrd,nDe+1)
				While nDe < Len(cDescPrd)
					cDPrd += "<br>"+SubStr(cDescPrd,nDe,nAte-nDe)
					nDe  := At("-",cDescPrd,nAte)
					nAte := At("-",cDescPrd,nDe+1)
				Enddo
			EndIf
			cCarac := "<strong>Modo de Lavagem: </strong>"+AllTrim(cLavag)
			cCarac += "<br><strong>Composi��o: </strong> "+AllTrim(cDComp)
			cCarac += "<br><strong>Tecido: </strong>"+AllTrim(cTecid)
			
			lOutlet := VEROUTLET(cCodPai)
			
			aCateg := {AllTrim(TRBPAI->GRUPO),AllTrim(TRBPAI->CATGRUPO),AllTrim(TRBPAI->CATEGORIA),AllTrim(TRBPAI->CATCATEGORIA),,AllTrim(TRBPAI->SUBGRUPO),AllTrim(TRBPAI->CATSUBGRUPO),IF(lOutlet,cOutlet,"")}

			For nx := 1 to 1 //2	
				If nx = 2
					cCodPai	:= "OUT_"+cCodPai
				EndIf
				
				nStatus := 0
				If lInativ
					nStatus := 2
				Else
					nStatus := 1
				EndIf	
				
				//If TRBPAI->PRCPOR > 0 .And. TRBPAI->PRCDE >= TRB->PRCPOR
					//If !lInativ
					If oWsPrdPai:Salvar(0,cCodPai,"0001",cDescPai,cDescPai,"",cDPrd,cCarac,"","","","","","","","","","",0,0,0,0,0,0,0,0,0,0,"","",0.5,0,0,0,0,0,0,0,0,999,nStatus,"1",2,nPreco,nPreco,2,"2","","","",cChvA1,cChvA2)  	
						cWsRet := oWsPrdPai:oWSSalvarResult:cDescricao
						GravaLog("Resultado: oWsPrdPai:Salvar: "+cCodPai + " - "+cWsRet)
						
						If "SUCESSO" $ UPPER(cWsRet)
							//xDatExp("PAI",Right(cCodPai,15))
						//EndIf

							If !lInativ
								//Sub-Colecao
								If oWsFilPrd:Salvar(0,cCodSub,cCodPai,AllTrim(TRBPAI->B4_YNSUBCO),1,cChvA1,cChvA2)
									cWsRet := oWsFilPrd:oWSSalvarResult:cDescricao
									GravaLog("SubCole��o: 0,"+cCodSub+","+cCodPai+","+AllTrim(TRBPAI->B4_YNSUBCO)+",1")
									If "SUCESSO" $ UPPER(cWsRet)
										GravaLog("SubCole��o Salva com sucesso. Produto: "+cCodPai+" Sub-Colecao: "+AllTrim(TRBPAI->B4_YNSUBCO))
									Else
										GravaLog("Problema na fun��o oWsFilPrd:Salvar. Produto: "+cCodPai+" Sub-Colecao: "+AllTrim(TRBPAI->B4_YNSUBCO)+" - "+cWsRet)
									EndIf
								Else
									GravaLog("Erro na fun��o oWsFilPrd:Salvar. Produto: "+cCodPai+" Sub-Colecao: "+AllTrim(TRBPAI->B4_YNSUBCO))
								EndIf

								nDe    := 1
								nAte   := 0
								aOcasi := {}
								cOcasi := AllTrim(TRBPAI->B4_YOCASIA)
								If !Empty(cOcasi)
									While nDe < Len(cOcasi)
										aAdd(aOcasi,SubStr(cOcasi,nDe,5))
										nDe += 6
									EndDo
								EndIf
								
								//Ocasiao
								If Len(aOcasi) > 0
									For yy := 1 to Len(aOcasi)
										DbSelectArea("ZAD")
										DbSetOrder(1)
										If DbSeek(xFilial("ZAD")+aOcasi[yy])
											If oWsFilPrd:Salvar(0,cCodOca,cCodPai,AllTrim(ZAD->ZAD_DESCRI),1,cChvA1,cChvA2)
												cWsRet := oWsFilPrd:oWSSalvarResult:cDescricao
												If "SUCESSO" $ UPPER(cWsRet)
													GravaLog("SubCole��o Salva com sucesso. Produto: "+cCodPai+" Ocasi�o: "+AllTrim(ZAD->ZAD_DESCRI))
												Else
													GravaLog("Problema na fun��o oWsFilPrd:Salvar. Produto: "+cCodPai+" OCasi�o: "+AllTrim(ZAD->ZAD_DESCRI)+" - "+cWsRet)
												EndIf
											Else
												GravaLog("Erro na fun��o oWsFilPrd:Salvar. Produto: "+cCodPai+" Ocasi�o: "+AllTrim(ZAD->ZAD_DESCRI))
											EndIf
										EndIf
									Next
								EndIf
								
								
								//Verifica��o Produto x Categoria
								If oWsPrdCat:ListarCategoriasAtivasPorProduto( 0 , cCodPai , cChvA1 , cChvA2 )
									cWsRet := oWsPrdCat:oWSListarCategoriasAtivasPorProdutoResult:cDescricao
									
									//Se produto possui relacionamento
									If "SUCESSO" $ UPPER(cWsRet)
										For nI := 1 to Len(OWSPRDCAT:OWSLISTARCATEGORIASATIVASPORPRODUTORESULT:OWSLISTA:OWSCLSCATEGORIA)
											cCodInt := OWSPRDCAT:OWSLISTARCATEGORIASATIVASPORPRODUTORESULT:OWSLISTA:OWSCLSCATEGORIA[nI]:CCATEGORIACODIGOINTERNO
											If !Empty(cCodInt)
												If !(AllTrim(cCodInt) $ (AllTrim(aCateg[2])+"/"+AllTrim(aCateg[4])+"/"+AllTrim(aCateg[6])+"/"+AllTrim(aCateg[7])))
													aAdd(aExcCat,cCodInt)
												EndIf
												AADD(aCat,cCodInt)
											EndIf
										Next nI
										
										If Len(aExcCat) > 0
											For nE := 1 to Len(aExcCat) 
												If oWsPrdCat:Excluir(0,cCodPai,AllTrim(aExcCat[nE]),cChvA1,cChvA2)
													cWsRet := oWsPrdCat:oWSExcluirResult:cDescricao
													If "SUCESSO" $ UPPER(cWsRet)
														GravaLog("Categoria "+AllTrim(aExcCat[nE])+" do produto "+cCodPai+" exclu�da com sucesso.")
													EndIf
												Else
													GravaLog("Erro na func�o excluir. Produto: "+cCodPai+" Categoria: "+AllTrim(aExcCat[nE])+". Erro: "+GetWSCError())
													Loop
												EndIf
											Next
										EndIf
										
										If ( nPos := aScan( aCat , cGrupo ) ) = 0
											//Grupo
											If oWsPrdCat:Salvar( 0 , cCodPai , cGrupo , cChvA1 , cChvA2 )
												cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
												GravaLog("Produto: "+cCodPai + " x Categoria: "+ cGrupo +" - "+cWsRet)
											Else
												GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
												Loop				
											EndIf
										EndIf
										
										If ( nPos := aScan( aCat , cCateg ) ) = 0
											//Categoria
											If oWsPrdCat:Salvar( 0 , cCodPai , cCateg , cChvA1 , cChvA2 )
												cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
												GravaLog("Produto: "+cCodPai + " x Categoria: "+ cCateg +" - "+cWsRet)
											Else
												GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
												Loop					
											EndIf
										EndIf
										
										/*If ( nPos := aScan( aCat , cSubGrp ) ) = 0
											//SubGrupo
											If oWsPrdCat:Salvar( 0 , cCodPai , cSubGrp , cChvA1 , cChvA2 )
												cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
												GravaLog("Produto: "+cCodPai + " x Categoria: "+ cSubGrp +" - "+cWsRet)
											Else
												GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
												Loop				
											EndIf
										EndIf*/
										
										If lOutlet
											If ( nPos := aScan( aCat , cOutlet ) ) = 0
												//SubGrupo
												If oWsPrdCat:Salvar( 0 , cCodPai , cOutlet , cChvA1 , cChvA2 )
													cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
													GravaLog("Produto: "+cCodPai + " x Outlet: "+ cSubGrp +" - "+cWsRet)
												Else
													GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
													Loop				
												EndIf
											EndIf
										EndIf
									//Se nao possui relacionamento com alguma categoria	
									Else
										//Grupo
										If oWsPrdCat:Salvar( 0 , cCodPai , cGrupo , cChvA1 , cChvA2 )
											cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
											GravaLog("Produto: "+cCodPai + " x Categoria: "+ cGrupo +" - "+cWsRet)
										Else
											GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
											Loop				
										EndIf
										//Categoria
										If oWsPrdCat:Salvar( 0 , cCodPai , cCateg , cChvA1 , cChvA2 )
											cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
											GravaLog("Produto: "+cCodPai + " x Categoria: "+ cCateg +" - "+cWsRet)
										Else
											GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
											Loop
										EndIf
										//SubGrupo
										/*If oWsPrdCat:Salvar( 0 , cCodPai , cSubGrp , cChvA1 , cChvA2 )
											cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
											GravaLog("Produto: "+cCodPai + " x Categoria: "+ cSubGrp +" - "+cWsRet)
										Else
											GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
											Loop				
										EndIf*/
										//Outlet
										If lOutlet
											If oWsPrdCat:Salvar( 0 , cCodPai , cOutlet , cChvA1 , cChvA2 )
												cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
												GravaLog("Produto: "+cCodPai + " x Outlet: "+ cSubGrp +" - "+cWsRet)
											Else
												GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
												Loop				
											EndIf
										EndIf
									EndIf
									
								Else
									GravaLog("Erro na funcao Listar Categorias Ativas Por Produto: "+GetWSCError())
									Loop
								EndIf
							EndIf
						EndIf	
					Else
						GravaLog("Erro na funcao Salvar Produto Pai: "+GetWSCError())
						Loop	
					EndIf
				/*Else
					If TRBPAI->PRCPOR = 0
						GravaLog("Pre�o POR zerado para o produto: "+cCodPai)
					EndIf
					If TRBPAI->PRCPOR > TRB->PRCDE
						GravaLog("Pre�o POR maior que pre�o DE para o produto: "+cCodPai)
					EndIf
				EndIf*/	
				//Else
				
				/*If lInativ
					oWsPrdPai:AlterarStatus(0,cCodPai,2,cChvA1,cChvA2)
					cWsRet := oWsPrdPai:oWSAlterarStatusResult:cDescricao
					GravaLog("Parametros Inativa Produto oWsPrdPai:AlterarStatus: 0,"+cCodPai+",1")
					GravaLog("Resultado Inativa Produto: oWsPrdPai:AlterarStatus: "+cCodPai+" - "+cWsRet)	
					
					If "SUCESSO" $ UPPER(cWsRet)
						xDatExp("SKU",Right(cProdPrt,15))					
					EndIf
				EndIf*/	
				//EndIf
				
				aExcCat := {}
			Next
			
			//Else
			//	TRBPAI->(DbSkip())
			//	Loop
			//EndIf

			nPrd++
			
			If nPrd > 200
				cTime   := Time()
				cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
				MemoWrite("\log_rakuten\B2C\"+cDtTime+"_EXP-Produtos.log",cLog)
				cLog := "_"
				nPrd := 0
			EndIf
			
			aCateg  := {}
			
			TRBPAI->(DbSkip())
		EndDo
	Else
		If lMenu
			MsgAlert('Produto PAI n�o encontrado.',"Aten��o")
			GravaLog('Produto PAI n�o encontrado.')
			lSku := .F.
		EndIf	
	EndIf	
EndIf

nPrd := 0
cSubCol := ""

//---------------------------------//
//--- Exporta��o do Produto SKU ---//
If lSku
	GravaLog("Executando Produto Filho(SKU)")
	
	xFiltros( IF(lMenu,AllTrim( mv_par01 ),) , "SKU" )
	TRBSKU->(dbGoTop())
	If TRBSKU->(!Eof())
		/*nCont := 0
		While TRBSKU->(!Eof())
			nCont++
			TRBSKU->(DbSkip())
		EndDo
		ProcRegua(nCont)
		TRBSKU->(dbGoTop())*/
		While TRBSKU->(!Eof())
			//IncProc()
			
			If !Empty(TRBSKU->B1_YFORMAT)
				cProdFil	:= AllTrim(SubStr( TRBSKU->B1_YFORMAT , 1 , AT("_" , TRBSKU->B1_YFORMAT ) -1 ))
				cProdPrt	:= AllTrim(TRBSKU->B1_COD)
				cPartNbr	:= AllTrim(TRBSKU->B1_YFORMAT)
				cCor		:= AllTrim(SubStr( TRBSKU->B1_YFORMAT , AT("_" , TRBSKU->B1_YFORMAT ) +1 , RAT("_" , TRBSKU->B1_YFORMAT ) - AT("_" , TRBSKU->B1_YFORMAT ) -1 ))
				cTam		:= AllTrim(SubStr( TRBSKU->B1_YFORMAT , RAT("_" , TRBSKU->B1_YFORMAT ) +1  ))
			Else
				cProdFil	:= AllTrim(SubStr(TRBSKU->B1_COD,1,8))
				cProdPrt	:= AllTrim(TRBSKU->B1_COD)
				cPartNbr	:= AllTrim(TRBSKU->B1_COD)
				cCor		:= AllTrim(SubStr(TRBSKU->B1_COD,9,3))
				cTam		:= AllTrim(SubStr(TRBSKU->B1_COD,12,4))
			EndIf	
			//nPrecoDe	:= GetAdvFVal("DA1","DA1_PRCVEN",xFilial("DA1")+cTabDe+cProdPrt,1,0)
			//nPrecoPor	:= GetAdvFVal("DA1","DA1_PRCVEN",xFilial("DA1")+cTabPor+cProdPrt,1,0)
			nPrecoDe	:= TRBSKU->PRCDE
			nPrecoPor	:= TRBSKU->PRCPOR
			
			GravaLog("Codigo Protheus: "+cProdPrt)
			
			If nPrecoPor > 0 .And. nPrecoDe >= nPrecoPor

				oWSskuA	:= oWsPrdFil:oWSsku1
				oWSskuB	:= oWsPrdFil:oWSsku2
				oWSskuC	:= oWsPrdFil:oWSsku3
				oWSskuD	:= oWsPrdFil:oWSsku4
				oWSskuE	:= oWsPrdFil:oWSsku5
				AADD(oWSskuA:cstring, "COR")
				AADD(oWSskuA:cstring, cCor)
				AADD(oWSskuB:cstring, "TAMANHO")
				AADD(oWSskuB:cstring, cTam)
				AADD(oWSskuC:cstring, "")
				AADD(oWSskuC:cstring, "")
				AADD(oWSskuD:cstring, "")
				AADD(oWSskuD:cstring, "")
				AADD(oWSskuE:cstring, "")
				AADD(oWSskuE:cstring, "")
				
				nEstoque := 0
				
				//xFiltros(cProdPrt,"ESTQ")
				//DbSelectArea("TRBEST")
				//If TRBEST->(!EOF())
					nEstoque := TRBSKU->TOTAL //ESTOQUE(cProdPrt)
				/*Else
					GravaLog("N�o foi encontrado Saldo em estoque para o produto: "+cProdPrt)
					TRBEST->(DbCloseArea())
					TRBSKU->(DbSkip())
					Loop
				EndIf*/		 
				
				//--- Produto SKU novo
				//If TRBSKU->B1_YSINC <> 'S'
				nProds := 1
				/*If nPrecoPor < nPrecoDE
					nProds := 2 
				EndIf*/
				
				cSubCol := AllTrim(GetAdvFVal("SB4","B4_YSUBCOL",xFilial("SB4")+SubStr(cProdPrt,1,8),1,""))
				
				If cSubCol $ cSubExc
					nMinEst := 0
				Else
					nMinEst := nMinimo
				EndIf
				
				For nx := 1 to nProds
					If nx = 2
						cPartNbr := "OUT_"+cPartNbr
						cProdPrt := "OUT_"+cProdPrt
						cProdFil := "OUT_"+cProdFil
					EndIf
					
					//GravaLog("Parametros: 0,"+cProdFil+","+cPartNbr+","+AllTrim(Str(nPrecoPor))+",oWSskuA,oWSskuB,oWSskuC,oWSskuD,oWSskuE,1,,"+cChvA1+","+cChvA2)
					
					If nEstoque > nMinEst
						nStatus := 1
					Else
						nStatus := 2
					EndIf

					If oWsPrdFil:Salvar(0,cProdFil,cPartNbr,nPrecoPor,oWSskuA,oWSskuB,oWSskuC,oWSskuD,oWSskuE,nStatus,"",cChvA1,cChvA2)
						cWsRet := oWsPrdFil:oWSSalvarResult:cDescricao
						//GravaLog("Parametros oWsPrdFil:Salvar: 0,"+cProdFil+","+cPartNbr+","+AllTrim(Str(nPrecoPor))+","+cCor+","+cTam)
						GravaLog("Resultado oWsPrdFil:Salvar: Pre�o Por: "+AllTrim(Str(nPrecoPor))+" - "+cPartNbr +" - "+cWsRet)
						
						If "SUCESSO" $ UPPER(cWsRet)
							//Atualiza Estoque do SKU	
							If oWsEstq:Salvar(0, cPartNbr, cPartNbr, nEstoque, nMinEst, 3,cChvA1,cChvA2) //Atualiza estoque	
								cWsRet := oWsEstq:oWSSalvarResult:cDescricao
								//GravaLog("Parametros oWsEstq:Salvar: 0,"+cPartNbr+","+cPartNbr+","+AllTrim(Str(TRBEST->TOTAL_E0E1)))		
								GravaLog("Resultado: oWsEstq:Salvar: Estoque: "+AllTrim(Str(nEstoque))+" - "+cPartNbr+" - "+cWsRet)
								
								If "SUCESSO" $ UPPER(cWsRet)
									xDatExp("ESTQ",Right(cProdPrt,15))
								EndIf
								
							Else
								GravaLog("Erro na funcao Salvar Estoque: "+GetWSCError())
								Loop
							EndIf
						EndIf
					Else
						GravaLog("Erro na funcao Salvar: "+GetWSCError())
						Loop		
					EndIf
					
					/*If TRBEST->TOTAL_E0E1 > 0 //Ativo o produto caso o saldo seja maior que 0
						GravaLog("Atualiza��o de estoque. Produto: "+cPartNbr)
						oWsPrdFil:AlterarStatus( 0 , cPartNbr , 1 ,cChvA1,cChvA2)
						cWsRet := oWsPrdFil:oWSAlterarStatusResult:cDescricao
						GravaLog("Parametros Ativa Produto oWsPrdFil:AlterarStatus: 0,"+cPartNbr+",1")
						GravaLog("Resultado Ativa Produto: oWsPrdFil:AlterarStatus: " + cPartNbr + " - " + cWsRet)	
						
						If "SUCESSO" $ UPPER(cWsRet)
							xDatExp("SKU",Right(cProdPrt,15))					
						EndIf
					EndIf/*
				//--- Altera��o de Produto	
				//Else
					//Verifica estoque e inativa o SKU caso esteja zerado
					
					//If oWsEstq:Retornar( 0 , cPartNbr , cPartNbr ,cChvA1,cChvA2)  	
					//	aWsRet := oWsEstq:oWSRetornarResult:oWSLista	
						/*If aWsRet:OWSCLSESTOQUE[1]:NQTDE <= 0					
							oWsPrdFil:AlterarStatus( 0 , cPartNbr , 2 ,cChvA1,cChvA2)
							cWsRet:oWsPrdFil:oWSAlterarStatusResult:cDescricao
							GravaLog("Resultado Produto SKU inativado: " + cProdPrt + " - " + cWsRet)
							
							If "Status da SKU Alterado com Sucesso" $ cWsRet
								xDatExp("SKU",Right(cProdPrt,15))					
							EndIf					
							
						//Else*/
					/*	If TRBEST->TOTAL_E0E1 > 0 .And. aWsRet:OWSCLSESTOQUE[1]:NQTDE <> TRBEST->TOTAL_E0E1
							If oWsEstq:Salvar(0, cPartNbr, cPartNbr, TRBEST->TOTAL_E0E1, 5, 3,cChvA1,cChvA2) //Atualiza estoque
								cWsRet := oWsEstq:oWSSalvarResult:cDescricao
								GravaLog("Parametros oWsEstq:Salvar: 0,"+cPartNbr+","+cPartNbr+","+AllTrim(Str(TRBEST->TOTAL_E0E1)))		
								GravaLog("Resultado: oWsEstq:Salvar: "+cPartNbr+" - "+cWsRet)
								
								If "SUCESSO" $ UPPER(cWsRet)
									xDatExp("ESTQ",Right(cProdPrt,15))
								EndIf			
											
							Else
								GravaLog("Erro na funcao Salvar Estoque: "+GetWSCError())
								Loop
							EndIf				
						EndIf
										
					Else
						GravaLog("Erro na funcao Retornar: "+GetWSCError())
						Loop		
					EndIf
					*/
					//Verifica o pre�o e atualiza caso necess�rio
					/*If oWsPrdFil:Listar( 0 , cPartNbr , cPartNbr , 1 ,cChvA1,cChvA2)
						cWsRet := oWsPrdFil:oWsListarResult:cDescricao
						GravaLog("Parametros oWsPrdFil:Listar 0,"+cPartNbr+","+cPartNbr+",1")		
						GravaLog("Resultado: oWsPrdFil:Listar "+cPartNbr+" - "+cWsRet)
						
						If "SUCESSO" $ UPPER(cWsRet)
							aWsRet := oWsPrdFil:oWSListarResult:oWSLista
							GravaLog("Pre�o Atual: "+AllTrim(Str(aWsRet:OWSCLSPRODUTOCARACTERISTICA[1]:NPRECOPOR))+" Preco Por: "+ Alltrim(Str(nPrecoPor)))
							If aWsRet:OWSCLSPRODUTOCARACTERISTICA[1]:NPRECOPOR <> nPrecoPor 
								If oWsPrdFil:AlterarPreco( 0 , cPartNbr , nPrecoPor ,cChvA1,cChvA2)
									cWsRet := oWsPrdFil:oWSAlterarPrecoResult:cDescricao
									GravaLog("Resultado: oWsPrdFil:AlterarPreco: " + cProdPrt + " - " + cWsRet)
									
									If "SUCESSO" $ UPPER(cWsRet)
										xDatExp("SKU",Right(cProdPrt,15))					
									EndIf
											
								Else
									GravaLog("Erro na funcao AlterarPreco: "+GetWSCError())
									Loop
								EndIf
							EndIf
						EndIf	
					Else
						GravaLog("Erro na funcao Retornar: "+GetWSCError())
						Loop
					EndIf*/
					
					/*If TRBEST->TOTAL_E0E1 <= nMinEst
						GravaLog("Estoque zerado. Inativando Produto: "+cPartNbr)
						INATIVA(cPartNbr)
					EndIf*/	
				Next
			Else
				If nPrecoPor = 0
					GravaLog("Pre�o POR zerado para o produto: "+cCodPai)
				EndIf
				If nPrecoPor > nPrecoDe
					GravaLog("Pre�o POR maior que pre�o DE para o produto: "+cCodPai)
				EndIf
			EndIf
			
			nPrd++
			
			If nPrd > 200
				cTime   := Time()
				cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
				MemoWrite("\log_rakuten\B2C\"+cDtTime+"_EXP-Produtos.log",cLog)
				cLog := "_"
				nPrd := 0
			EndIf

			TRBSKU->(dbSkip())
		
		EndDo
	Else
		If lMenu
			MsgAlert("Produto SKU n�o encontrado.","Aten��o")
			GravaLog("Produto SKU n�o encontrado.")
		EndIf	
	EndIf		
EndIf

GravaLog("Fim do processamento.")

MemoWrite("\log_rakuten\B2C\"+cDtTime+"_EXP-Produtos.log",cLog)

//DbCloseAll()
If Select("TRBSKU") > 0
	TRBSKU->(DbCloseArea())
EndIf
If Select("TRBPAI") > 0
	TRBPAI->(DbCloseArea())
EndIf	

If lMenu
	cHoraFim := Time()
	MsgInfo("Exporta��o de Produtos finalizada. "+cHoraIni+" - "+cHoraFim,"Aten��o")
Else
	Reset Environment
Endif

Return

Static Function VEROUTLET(cRef)
Local cQuery := ""
Local lRet   := .F.
Local cTabDE	:= SuperGetMV("MV_XTABDC",.F.,"042")
Local cTabPOR	:= SuperGetMV("MV_XTABPC",.F.,"009")

cQuery := "select * from ( "
cQuery += CRLF + "select ISNULL(DA11.DA1_PRCVEN,0) AS PRCDE,ISNULL(DA12.DA1_PRCVEN,0) AS PRCPOR "
cQuery += CRLF + "from "+RetSqlName("DA1")+" DA11 "
cQuery += CRLF + "inner join "+RetSqlName("DA1")+" DA12 "
cQuery += CRLF + "on DA12.DA1_CODTAB = '"+cTabPOR+"' "
cQuery += CRLF + "and DA11.DA1_CODPRO = DA12.DA1_CODPRO "
cQuery += CRLF + "and DA12.D_E_L_E_T_ = '' "
cQuery += CRLF + "where DA11.DA1_CODTAB = '"+cTabDE+"' and DA11.D_E_L_E_T_ = '' and DA11.DA1_CODPRO like '"+cRef+"%' "
cQuery += CRLF + "group by DA11.DA1_PRCVEN,DA12.DA1_PRCVEN ) Z "
cQuery += CRLF + "where PRCDE > PRCPOR "

MemoWrite("HFATA004_OUTLET.txt",cQuery)
	
cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"OUT", .F., .T.)

DbSelectArea("OUT")

If OUT->(!EOF())
	lRet := .T.
EndIf

OUT->(DbCloseArea())

Return lRet

Static Function INATIVA(cPartNbr)

oWsPrdFil:AlterarStatus( 0 , cPartNbr , 2 ,cChvA1,cChvA2)
cWsRet := oWsPrdFil:oWSAlterarStatusResult:cDescricao
GravaLog("Parametros Inativa Produto oWsPrdFil:AlterarStatus: 0,"+cPartNbr+",2")
GravaLog("Resultado Inativa Produto: oWsPrdFil:AlterarStatus: " + cPartNbr + " - " + cWsRet)	

If "SUCESSO" $ UPPER(cWsRet)
	xDatExp("SKU",Right(cProdPrt,15))					
EndIf

Return

Static Function ATUCAT(cProduto)
Local cquery := ""
Local xx        := 0

For xx := 1 to 2
	cQuery := "SELECT * FROM ("
	//If xx = 1
	cQuery += CRLF + "SELECT B4_GRUPO AS GRUPO,ISNULL(BM_YCTRAK,'') AS CATGRUPO,BM_DESC, "
	cQuery += CRLF + "B4_YCATEGO AS CATEGORIA,ISNULL(ZAC_YCTRAK,'') AS CATCATEGORIA,ZAC_DESCRI,B4_YSUBGRP AS SUBGRUPO,ISNULL(ZAG_YCTRAK,'') AS CATSUBGRUPO,ZAG_DESCRI "	
	/*ElseIf xx = 2
		cQuery += CRLF + "SELECT B4_YCATEGO AS CATEGORIA,ISNULL(ZAC_YCTRAK,'') AS CATCATEGORIA "
	Else
		cQuery += CRLF + "SELECT B4_YSUBGRP AS SUBGRUPO,ISNULL(ZAG_YCTRAK,'') AS CATSUBGRUPO "
	EndIf*/
	cQuery += CRLF + "FROM "+RetSqlName("SB4")+" SB4 "
	cQuery += CRLF + "LEFT JOIN "+RetSqlName("SBM")+" SBM "
	cQuery += CRLF + "ON		SBM.BM_GRUPO = SB4.B4_GRUPO "
	cQuery += CRLF + "		AND SBM.BM_FILIAL = '"+xFilial("SBM")+"' "
	cQuery += CRLF + "		AND SBM.D_E_L_E_T_ = '' "
			
	cQuery += CRLF + "LEFT JOIN "+RetSqlName("ZAC")+" ZAC "
	cQuery += CRLF + "ON		ZAC.ZAC_CODIGO = SB4.B4_YCATEGO "
	cQuery += CRLF + "		AND ZAC.ZAC_FILIAL = '"+xFilial("ZAC")+"' "
	cQuery += CRLF + "		AND ZAC.D_E_L_E_T_ = '' "
	
	cQuery += CRLF + "LEFT JOIN "+RetSqlName("ZAG")+" ZAG "
	cQuery += CRLF + "ON		ZAG.ZAG_CODIGO = SB4.B4_YSUBGRP
	cQuery += CRLF + "		AND ZAG.ZAG_FILIAL = '"+xFilial("ZAG")+"' "
	cQuery += CRLF + "		AND ZAG.D_E_L_E_T_ = '' "
			
	cQuery += CRLF + "WHERE	SB4.B4_FILIAL = '"+xFilial("SB4")+"' "
	cQuery += CRLF + "        AND SB4.D_E_L_E_T_ = '' "
	If !Empty(cProduto)
		If Len(AllTrim(cProduto)) <= 8
			cQuery += CRLF + " AND SB4.B4_COD like '"+AllTrim(cProduto)+"%' "
		Else
			cQuery += CRLF + " AND SB4.B4_COD = '"+AllTrim(cProduto)+"' "
		EndIf
	EndIf 
	cQuery += CRLF + "        AND SB4.B4_DWEB = 'S' "
	cQuery += CRLF + "AND 	SB4.B4_YCOLECA IN   ( SELECT ZAA.ZAA_CODIGO " 
	cQuery += CRLF + "                                      FROM "+RetSqlName("ZAA")+" ZAA "
	cQuery += CRLF + "                                      WHERE ZAA.ZAA_FILIAL = '"+xFilial("ZAA")+"' "
	cQuery += CRLF + "                                      AND ZAA.D_E_L_E_T_ = '' "
	cQuery += CRLF + "                                      AND ZAA.ZAA_DWEB = 'S' ) ) Z "
	If xx = 1
		cQuery += CRLF + " WHERE  CATGRUPO = ''"
		//cQuery += CRLF + " GROUP BY B4_GRUPO "
	ElseIf xx = 2
		cQuery += CRLF + "WHERE  CATCATEGORIA = ''"
		//cQuery += CRLF + " GROUP BY B4_YCATEGO "
	Else
		cQuery += CRLF + "WHERE  CATSUBGRUPO = ''"
		//cQuery += CRLF + " GROUP BY B4_YSUBGRP "
	EndIf
	cQuery += CRLF + " GROUP BY GRUPO,CATGRUPO,BM_DESC,CATEGORIA,CATCATEGORIA,ZAC_DESCRI,SUBGRUPO,CATSUBGRUPO,ZAG_DESCRI "
	//cQuery += CRLF + " order by B4_COD"
	
	MemoWrite("HFATA004_ATUCAT.txt",cQuery)
	
	cQuery := ChangeQuery(cQuery)
	
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRBCAT", .F., .T.)

	DbSelectArea("TRBCAT")
	
	If TRBCAT->(!EOF())
		While TRBCAT->(!EOF())
			If xx = 1
				cCodCat := AllTrim(TRBCAT->GRUPO)
				cNomCat := AllTrim(TRBCAT->BM_DESC)
				If oWsCat:Salvar(0,cCodCat,cNomCat,1,,cChvA1,cChvA2)
					cWsRet := oWsCat:oWSSalvarResult:cDescricao
					GravaLog("Categoria GRUPO: "+cCodCat+" - "+cNomCat+". Resultado: "+cWsRet)
					If "SUCESSO" $ UPPER(cWsRet)
						DbSelectArea("SBM")
						DbSetOrder(1)
						If DbSeek(xFilial("SBM")+TRBCAT->GRUPO)
							RecLock("SBM",.F.)
							SBM->BM_YCTRAK := TRBCAT->GRUPO
							MsUnlock()
						EndIf
					EndIf
				Else
					GravaLog("Erro na funcao Salvar Grupo: "+GetWSCError())
				EndIf
			ElseIf xx = 2
				cCodCat := AllTrim(TRBCAT->CATEGORIA)
				cNomCat := AllTrim(TRBCAT->ZAC_DESCRI)
				If oWsCat:Salvar(0,cCodCat,cNomCat,1,TRBCAT->CATGRUPO,cChvA1,cChvA2)
					cWsRet := oWsCat:oWSSalvarResult:cDescricao
					GravaLog("Categoria CATEGORIA: "+cCodCat+" - "+cNomCat+". Resultado: "+cWsRet)
					If "SUCESSO" $ UPPER(cWsRet)
						DbSelectArea("ZAC")
						DbSetOrder(1)
						If DbSeek(xFilial("ZAC")+TRBCAT->CATEGORIA)
							RecLock("ZAC",.F.)
							ZAC->ZAC_YCTRAK := TRBCAT->CATEGORIA
							MsUnlock()
						EndIf
					EndIf
				Else
					GravaLog("Erro na funcao Salvar Categoria: "+GetWSCError())
				EndIf
			Else
				cCodCat := AllTrim(TRBCAT->SUBGRUPO)
				cNomCat := AllTrim(TRBCAT->ZAG_DESCRI)
				If oWsCat:Salvar(0,cCodCat,cNomCat,1,TRBCAT->CATCATEGORIA,cChvA1,cChvA2)
					cWsRet := oWsCat:oWSSalvarResult:cDescricao
					GravaLog("Categoria SUBGRUPO: "+cCodCat+" - "+cNomCat+". Resultado: "+cWsRet)
					If "SUCESSO" $ UPPER(cWsRet)
						DbSelectArea("ZAG")
						DbSetOrder(1)
						If DbSeek(xFilial("ZAG")+TRBCAT->SUBGRUPO)
							RecLock("ZAG",.F.)
							ZAG->ZAG_YCTRAK := TRBCAT->SUBGRUPO
							MsUnlock()
						EndIf
					EndIf
				Else
					GravaLog("Erro na funcao Salvar Categoria: "+GetWSCError())
				EndIf
			EndIf
			TRBCAT->(DbSkip())
		EndDo
	EndIf

	TRBCAT->(DbCloseArea())
Next


Return

//---------------------------//
//-------- Log --------------//
Static Function GravaLog(cMsg)
Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cDtHora+": HFATA004 - "+cMsg)
cLog += CRLF+cDtHora+": HFATA004 - "+cMsg

Return

//---------------------------------//
//--- Regras Exporta��o Estoque ---//
Static Function xFiltros(cProduto,cTab)

Local cQuery	:= ""//GetNextAlias()
Local cWhere	:= ""
Local cTabDE	:= SuperGetMV("MV_XTABDC",.F.,"042")
Local cTabPOR	:= SuperGetMV("MV_XTABPC",.F.,"009")
Default cProduto := ""

Do Case

	//--- Regras Exporta��o Produto Pai e amarra��o Produto x Categoria ---//
	Case cTab == "PAI"		      
		cQuery := "SELECT B4_COD,B4_DESC,ISNULL(CONVERT(VARCHAR(2047), CONVERT(VARBINARY(2047), B4_YDESCPT)),'') AS DESCPT, B4_YTECIDO,B4_YOCASIA,B4_YNSUBCO,B4_YSUBCOL, " 
		cQuery += CRLF + "ISNULL((select MAX(DA1_PRCVEN) from "+RetSqlName("DA1")+" DA1 where DA1.D_E_L_E_T_ = '' and DA1_CODTAB = '"+cTabDE+"' AND DA1_CODPRO like SUBSTRING(B4_COD,1,8)+'%'),0) AS PRCDE,"
		
		cQuery += CRLF + "ISNULL((select TOP 1 ISNULL(CONVERT(VARCHAR(2047), CONVERT(VARBINARY(2047), ZD_DESCCP2)),'') from "+RetSqlName("SZD")+" SZD1 "
		cQuery += CRLF + "where ZD_PRODUTO = B4_COD and SZD1.D_E_L_E_T_ = '' and ISNULL(CONVERT(VARCHAR(2047), CONVERT(VARBINARY(2047), ZD_DESCCP2)),'') <> '' "
		cQuery += CRLF + "group by ZD_PRODUTO,ISNULL(CONVERT(VARCHAR(2047), CONVERT(VARBINARY(2047), ZD_DESCCP2)),'')),'') AS COMPO, "
		
		cQuery += CRLF + "ISNULL((select TOP 1 ZD_DESCLAV from "+RetSqlName("SZD")+" SZD2 where ZD_PRODUTO = B4_COD and SZD2.D_E_L_E_T_ = '' and ZD_DESCLAV <> '' group by ZD_PRODUTO,ZD_DESCLAV),'') AS LAVAG, "
		cQuery += CRLF + "B4_GRUPO AS GRUPO,ISNULL(BM_YCTRAK,'') AS CATGRUPO,B4_YCATEGO AS CATEGORIA,ISNULL(ZAC_YCTRAK,'') AS CATCATEGORIA,B4_YSUBGRP AS SUBGRUPO,ISNULL(ZAG_YCTRAK,'') AS CATSUBGRUPO "
		
		cQuery += CRLF + "FROM "+RetSqlName("SB4")+" SB4 "
		cQuery += CRLF + "LEFT JOIN "+RetSqlName("SBM")+" SBM "
		cQuery += CRLF + "ON		SBM.BM_GRUPO = SB4.B4_GRUPO "
		cQuery += CRLF + "		AND SBM.BM_FILIAL = '"+xFilial("SBM")+"' "
		cQuery += CRLF + "		AND SBM.D_E_L_E_T_ = '' "
				
		cQuery += CRLF + "LEFT JOIN "+RetSqlName("ZAC")+" ZAC "
		cQuery += CRLF + "ON		ZAC.ZAC_CODIGO = SB4.B4_YCATEGO "
		cQuery += CRLF + "		AND ZAC.ZAC_FILIAL = '"+xFilial("ZAC")+"' "
		cQuery += CRLF + "		AND ZAC.D_E_L_E_T_ = '' "
		
		cQuery += CRLF + "LEFT JOIN "+RetSqlName("ZAG")+" ZAG "
		cQuery += CRLF + "ON		ZAG.ZAG_CODIGO = SB4.B4_YSUBGRP
		cQuery += CRLF + "		AND ZAG.ZAG_FILIAL = '"+xFilial("ZAG")+"' "
		cQuery += CRLF + "		AND ZAG.D_E_L_E_T_ = '' "
				
		cQuery += CRLF + "WHERE	SB4.B4_FILIAL = '"+xFilial("SB4")+"' "
		cQuery += CRLF + "        AND SB4.D_E_L_E_T_ = '' "
		If !Empty(cProduto)
			cQuery += CRLF + " AND SB4.B4_COD = '"+cProduto+"' "
		EndIf 
		cQuery += CRLF + "        AND SB4.B4_DWEB = 'S' "
		cQuery += CRLF + "AND 	SB4.B4_YCOLECA IN   ( SELECT ZAA.ZAA_CODIGO " 
		cQuery += CRLF + "                                      FROM "+RetSqlName("ZAA")+" ZAA "
		cQuery += CRLF + "                                      WHERE ZAA.ZAA_FILIAL = '"+xFilial("ZAA")+"' "
		cQuery += CRLF + "                                      AND ZAA.D_E_L_E_T_ = '' "
		cQuery += CRLF + "                                      AND ZAA.ZAA_DWEB = 'S' )"         			              
	    cQuery += CRLF + " order by B4_COD"
	    
	    MemoWrite("HFATA004_PAI.txt",cQuery)
	    
	    cQuery := ChangeQuery(cQuery)

		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRBPAI", .F., .T.)                               

	//--- Regras Exporta��o Produto SKU ---//
	Case cTab == "SKU"
		cQuery := "SELECT * FROM ( "
		cQuery += CRLF + "SELECT B1_COD, B1_YFORMAT, "
		cQuery += CRLF + "ISNULL((SELECT DA1_PRCVEN FROM "+RetSqlName("DA1")+" DA11 WHERE DA11.DA1_FILIAL = '"+xFilial("DA1")+"' AND DA11.D_E_L_E_T_ = '' AND DA11.DA1_CODTAB = '"+cTabDE+"' AND DA11.DA1_CODPRO = SB1.B1_COD),0) AS PRCDE, "
		cQuery += CRLF + "ISNULL((SELECT DA1_PRCVEN FROM "+RetSqlName("DA1")+" DA12 WHERE DA12.DA1_FILIAL = '"+xFilial("DA1")+"' AND DA12.D_E_L_E_T_ = '' AND DA12.DA1_CODTAB = '"+cTabPOR+"' AND DA12.DA1_CODPRO = SB1.B1_COD),0) AS PRCPOR, "
		cQuery += CRLF + "ISNULL((SELECT SUM(SB2.B2_QATU)-SUM(SB2.B2_QEMP)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV) " 
		cQuery += CRLF + "FROM SB2010 SB2 " 
		cQuery += CRLF + "WHERE SB2.B2_FILIAL = '"+xFilial("SB2")+"' " 
      	cQuery += CRLF + "AND SB2.B2_COD = B1_COD "
		cQuery += CRLF + "AND SB2.D_E_L_E_T_ = '' "
		cQuery += CRLF + "AND SB2.B2_LOCAL IN ("+cArmazem+") " 
		cQuery += CRLF + "GROUP BY SB2.B2_COD ),0) AS TOTAL"
		cQuery += CRLF + "FROM "+RetSqlName("SB1")+" SB1 "
		cQuery += CRLF + "WHERE 	SB1.B1_FILIAL = '"+xFilial("SB1")+"' "
		cQuery += CRLF + "		AND SB1.D_E_L_E_T_ = '' "
		If !Empty(cProduto)
			If Len(cProduto) <= 8
				cQuery += CRLF + " AND SB1.B1_COD LIKE '"+cProduto+"%' "
			Else
				cQuery += CRLF + " AND SB1.B1_COD = '"+cProduto+"' "
			EndIf
		//Else
			//cWhere := "SB1.B1_MSEXP = '' "
		EndIf
		cQuery += CRLF + "		AND SB1.B1_YDWEB = 'S' "
		cQuery += CRLF + "		AND SB1.B1_YCOLECA IN	( SELECT ZAA.ZAA_CODIGO  " 
		cQuery += CRLF + "								  FROM "+RetSqlName("ZAA")+" ZAA "
		cQuery += CRLF + "								  WHERE ZAA.ZAA_FILIAL = '"+xFilial("ZAA")+"' "
		cQuery += CRLF + "								  AND ZAA.D_E_L_E_T_ = '' "
		cQuery += CRLF + "								  AND ZAA.ZAA_DWEB = 'S' "
		cQuery += CRLF + "								) "
		cQuery += CRLF + "		AND SB1.B1_YSITUAC IN	( SELECT ZA8.ZA8_CODIGO  "
		cQuery += CRLF + "								  FROM "+RetSqlName("ZA8")+" ZA8 "
		cQuery += CRLF + "								  WHERE ZA8.ZA8_FILIAL = '"+xFilial("ZA8")+"' "
		cQuery += CRLF + "								  AND ZA8.D_E_L_E_T_ = '' "
		cQuery += CRLF + "								  AND ZA8.ZA8_DWEB = 'T' "
		cQuery += CRLF + "								) "
		cQuery += CRLF + "		AND SB1.B1_COD IN 		( SELECT DA1.DA1_CODPRO "
		cQuery += CRLF + "								  FROM "+RetSqlName("DA1")+" DA1 "
		cQuery += CRLF + "								  WHERE DA1.DA1_FILIAL = '"+xFilial("DA1")+"' "
		cQuery += CRLF + "								  AND DA1.D_E_L_E_T_ = '' "
		cQuery += CRLF + "								  AND DA1.DA1_CODTAB IN ( '"+cTabDE+"' , '"+cTabPOR+"' )	) ) Z"
		//cQuery += CRLF + " WHERE PRCPOR > 0 AND PRCDE >= PRCPOR "
		cQuery += CRLF + " order by B1_COD"

		MemoWrite("HFATA004_SKU.txt",cQuery)

		cQuery := ChangeQuery(cQuery)

		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRBSKU", .F., .T.)
EndCase

Return //cQuery

Static Function ESTOQUE(cProduto)
Local cQuery := ""
Local nRet   := 0

cQuery := "SELECT B2_COD,SUM(SB2.B2_QATU)-SUM(SB2.B2_QEMP)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV) as TOTAL "
cQuery += CRLF + "FROM "+RetSqlName("SB2")+" SB2 "
cQuery += CRLF + "WHERE 	SB2.B2_FILIAL = '"+xFilial("SB2")+"' "
cQuery += CRLF + "      AND SB2.B2_COD = '"+cProduto+"' "
cQuery += CRLF + "		AND SB2.D_E_L_E_T_ = '' "
cQuery += CRLF + "		AND SB2.B2_LOCAL IN ("+cArmazem+") "
cQuery += CRLF + "GROUP BY SB2.B2_COD "

MemoWrite("HFATA004_ESTQ.txt",cQuery)

cQuery := ChangeQuery(cQuery)

If Select("EST")
	EST->(DbCloseArea())
EndIf

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"EST", .F., .T.)

DbSelectArea("EST")

If EST->(!EOF())
	nRet := EST->TOTAL
EndIf

EST->(DbCloseArea())

Return nRet

Static Function SLDFILHOS(cPai,nLimite)
Local lRet := .T.
Local cQuery := ""

cQuery := "SELECT B2_COD,SUM(SB2.B2_QATU)-SUM(SB2.B2_QEMP)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV) as TOTAL_E0E1 "
cQuery += CRLF + "FROM "+RetSqlName("SB2")+" SB2 "
cQuery += CRLF + "WHERE 	SB2.B2_FILIAL = '"+xFilial("SB2")+"' "
cQuery += CRLF + "		AND SB2.B2_COD like '"+AllTrim(cPai)+"%' "
cQuery += CRLF + "		AND SB2.D_E_L_E_T_ = '' "
cQuery += CRLF + "		AND SB2.B2_LOCAL IN ("+cArmazem+") "
cQuery += CRLF + "GROUP BY SB2.B2_COD

MemoWrite("HFATA004_SLD.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"SLD", .F., .T.)

DbSelectArea("SLD")

If SLD->(!EOF())
	While SLD->(!EOF())
		If SLD->TOTAL_E0E1 > nLimite 
			lRet := .F.
			Exit
		EndIf
		SLD->(DbSkip())
	EndDo	
EndIf

SLD->(DbCloseArea())

Return lRet

//---------------------------------//
//--- Regras Exporta��o Estoque ---//
Static Function xDatExp(cTab,cFil)
Local aAreaSB1	:= SB1->(GetArea())
Local aAreaSB2	:= SB2->(GetArea())
Local aAreaSB4	:= SB4->(GetArea())

Do Case
	Case cTab == "PAI"
		DbSelectArea('SB4')
		DbSetOrder(1)
		If DbSeek(xFilial('SB4')+cFil)
			RecLock('SB4',.F.)
			SB4->B4_MSEXP := DTOS(DDATABASE)
			MsUnlock()
		EndIf
	
	Case cTab == "SKU"
		DbSelectArea('SB1')
		DbSetOrder(1)
		If DbSeek(xFilial('SB1')+cFil)
			RecLock('SB1',.F.)
			SB1->B1_MSEXP := DTOS(DDATABASE)
			MsUnlock()
		EndIf
		
	Case cTab == "ESTQ"
		DbSelectArea('SB2')
		DbSetOrder(1)
		If DbSeek(xFilial('SB2')+cFil)
			While xFilial('SB2') == SB2->B2_FILIAL .AND. SB2->B2_COD == cFil  
				RecLock('SB2',.F.)
				SB2->B2_MSEXP := DTOS(DDATABASE)
				MsUnlock()
				SB2->(dbSkip())
			EndDO
		EndIf
EndCase

RestArea(aAreaSB1)
RestArea(aAreaSB2)
RestArea(aAreaSB4)
Return

//----- Via Menu ----//
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

  
PutSx1(cPerg,"01","Produto?","Produto?","Produto?","Mv_ch1","C",15,0,0,"G","","","","N","Mv_par01",""   ,""   ,""   ,"",""   ,""   ,""   ,"","","","","","","","","",{"Atualiza qual Produto? (Vazio atualiza todos)",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)