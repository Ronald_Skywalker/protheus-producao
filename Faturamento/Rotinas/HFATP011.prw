#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#INCLUDE "shell.ch"
#include 'totvs.ch'


User Function HFATP011()

	Local oButton1
	Local oButton2
	Local oButton3
	Local oButton4
	Local oButton5
	Local oButton6
	Local oSay1
	Local oSay2
	Local oGet1
	Local oGet2

	Private cOperacao
	Private cTotValPed		:= 0
	Private cTotValDesc		:= 0
	Private cTotValTot		:= 0
	Private cTotQtdPed		:= 0
	Private cTotQtdPFat		:= 0
	Private cTotQtdEnt		:= 0
	Private cTotQtdPerda	:= 0
	Private cTotQtdSaldo	:= 0
	Private cDtTipo			:= "Emiss�o"
	Private cDtDe			:= Ctod("01/01/00")
	Private cDtAte			:= Ctod("31/12/49")
	Private cFili			:= Space(04)
	Private cPed			:= Space(06)
	Private cTpOpera		:= Space(02)
	Private cProduto		:= Space(15)
	Private cCliente		:= Space(06)
	Private cCliNome		:= Space(20)
	Private cRepres 		:= Space(06)
	Private cPedMill 		:= Space(20)
	Private cPedCli 		:= Space(20)
	Private cRegiao			:= Space(10)
	Private cMicRegiao		:= Space(10)
	Private cPolcom			:= Space(03)
	Private cTpPed			:= Space(03)
	Private cTabPreco		:= Space(03)
	Private cOrigem			:= "Todos"
	Private cStatus			:= "Bloqueados"
	Private cPedFat			:= "Nao Faturado/Parcial"
	Private cOrdem 			:= "Data de Emiss�o"
	Private cTpValTot		:= "Todas"
	Private cQuant 			:= 0
	Private _vend 			:= ""
	Private _xvend 			:= ""
	Private aVendItem		:= {}
	Private aScreens 		:= getScreenRes()
	Private aColsEx 		:= {}

	PUBLIC lPassou			:= .F.

	DbSelectArea("SA3")
	DbSetOrder(7)
	DbSeek(xfilial("SA3")+__cUserID)

	If Found()
		_vend 		:= SA3->A3_COD
		_xvend += "'"+_vend+"',"

		Aadd (aVendItem,"")
		Aadd (aVendItem,_vend)

		DbSelectArea('ZA4')
		ZA4->(DbSetOrder(1))
		ZA4->(DbGoTop())
		ZA4->(DbSeek(xFilial("ZA4")))
		While ZA4->(!Eof())
			If ZA4->ZA4_GEREN = _vend
				_xvend += "'"+ZA4->ZA4_REPRE+"',"
				Aadd (aVendItem,ZA4->ZA4_REPRE)
			End
			ZA4->(DbSkip())
		EndDo

	Else
		_xvend := ""
	Endif

	Static oDlg

	SetKey( VK_F4, { || ConsPed() } )
	//SetKey( VK_F5, { || Pquery1(1,_xvend) } )
	SetKey( VK_F5, { || Pquery(1,_xvend) } )
	SetKey( VK_F6, { || AjustaPrc() } )

	/*If aScreens[2] >= 800		//resolu��o da tela
		DEFINE MSDIALOG oDlg TITLE "Pedidos de Vendas" FROM 000, 000  TO 705, 1400 COLORS 0, 16777215 PIXEL
		_nLinha		:= 335
		_nLinha2	:= _nLinha - 26
		_nLinha3	:= _nLinha2 - 11
	If AllTrim(_xvend) == ""
			_nCol1		:= 45
	Else
			_nCol1		:= 429
	End
		_nCol1a		:= 55
		_nCol2		:= 6
		_nCol2a		:= 88
		_nAOmsPed	:= 297
		_nLOmsPed	:= 695
		_nTam		:= 74
Else*/
		DEFINE MSDIALOG oDlg TITLE "Pedidos de Vendas" FROM -025, -050  TO 650, 1250 COLORS 0, 16777215 PIXEL
		_nLinha		:= 278
		_nLinha2	:= _nLinha - 25
		_nLinha3	:= _nLinha2 - 11
		
	If AllTrim(_xvend) == ""
			_nCol1		:= 6
	Else
			//_nCol1		:= 383
			_nCol1		:= 38
	End
		_nCol1a		:= 54
		_nCol2		:= 6
		_nCol2a		:= 72//82
		_nAOmsPed	:= 242
		_nLOmsPed	:= 647
		_nTam		:= 66
	//End
	
	If AllTrim(_xvend) == ""
		@ _nLinha, _nCol1 BUTTON oButton6 PROMPT "Canc./Rec. Ped."	SIZE 045, 012 ACTION U_HCCANCP()  	OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton6 PROMPT "Copia NF"	 		SIZE 045, 012 ACTION HCOPNF()  		OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton6 PROMPT "Legendas"	 		SIZE 045, 012 ACTION Legenda()  	OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton6 PROMPT "Exportar XLS"		SIZE 045, 012 ACTION RpExcell(1)  	OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton6 PROMPT "Posicao" 			SIZE 045, 012 ACTION ConsCli(_xvend)  	OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton6 PROMPT "Liberar" 			SIZE 045, 012 ACTION U_PolCo3(2)	OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton5 PROMPT "Estornar" 		SIZE 045, 012 ACTION U_PolCo3(3)	OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton4 PROMPT "Visualizar"		SIZE 045, 012 ACTION ManPed(3) 		OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton4 PROMPT "Incluir" 			SIZE 045, 012 ACTION ManPed(2) 		OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton3 PROMPT "Alterar" 			SIZE 045, 012 ACTION ManPed(1) 		OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton2 PROMPT "Filtro" 			SIZE 045, 012 ACTION FILTRO() 		OF oDlg PIXEL
		_nCol1 += _nCol1a
		//@ _nLinha, _nCol1 BUTTON oButton1 PROMPT "Fechar" 			SIZE 025, 012 ACTION Close(oDlg) 	OF oDlg PIXEL
	Else
		lPassou := .T.
		@ _nLinha, _nCol1 BUTTON oButton6 PROMPT "Legendas"	 		SIZE 045, 012 ACTION Legenda()  	OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton6 PROMPT "Exportar XLS"		SIZE 045, 012 ACTION RpExcell(1)  	OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton6 PROMPT "Posicao" 			SIZE 045, 012 ACTION ConsCli(_xvend)  	OF oDlg PIXEL
		_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton4 PROMPT "Visualizar"		SIZE 045, 012 ACTION ManPed(3) 		OF oDlg PIXEL
		_nCol1 += _nCol1a
	//	@ _nLinha, _nCol1 BUTTON oButton4 PROMPT "Inclui" 			SIZE 045, 012 ACTION ManPed(2) 		OF oDlg PIXEL
	//	_nCol1 += _nCol1a
    //	@ _nLinha, _nCol1 BUTTON oButton3 PROMPT "Altera" 			SIZE 045, 012 ACTION ManPed(1) 		OF oDlg PIXEL
	//	_nCol1 += _nCol1a
		@ _nLinha, _nCol1 BUTTON oButton2 PROMPT "Filtrar" 			SIZE 045, 012 ACTION FILTRO() 		OF oDlg PIXEL
		_nCol1 += _nCol1a
		//@ _nLinha, _nCol1 BUTTON oButton1 PROMPT "Fechar" 			SIZE 025, 012 ACTION Close(oDlg) 	OF oDlg PIXEL
	End

	fMSPedido()
    
	_nLinha += -30
	@ _nLinha2  , _nCol2 SAY oSay1		VAR "Total Pedido " 		SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha2+8, _nCol2 MSGET oGet1 	VAR cTotValPed  			SIZE _nTam, 010 OF oDlg PICTURE "@E 999,999,999,999.99" COLORS 0, 16777215 PIXEL
	_nCol2 += _nCol2a
	@ _nLinha2  , _nCol2 SAY oSay2		VAR "Total Desconto"		SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha2+8, _nCol2 MSGET oGet2 	VAR cTotValDesc 			SIZE _nTam, 010 OF oDlg PICTURE "@E 999,999,999,999.99" COLORS 0, 16777215 PIXEL
	_nCol2 += _nCol2a
	@ _nLinha2  , _nCol2 SAY oSay3		VAR "Total Ped.+Desc."		SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha2+8, _nCol2 MSGET oGet3 	VAR cTotValTot				SIZE _nTam, 010 OF oDlg PICTURE "@E 999,999,999,999.99" COLORS 0, 16777215 PIXEL
	_nCol2 += _nCol2a
	@ _nLinha2  , _nCol2 SAY oSay4		VAR "Qtd. Pedido"		 	SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha2+8, _nCol2 MSGET oGet4 	VAR cTotQtdPed 				SIZE _nTam, 010 OF oDlg PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL
	_nCol2 += _nCol2a
	@ _nLinha2  , _nCol2 SAY oSay4		VAR "Qtd. Entregue"		 	SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha2+8, _nCol2 MSGET oGet4 	VAR cTotQtdEnt 				SIZE _nTam, 010 OF oDlg PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL
	_nCol2 += _nCol2a
	@ _nLinha2  , _nCol2 SAY oSay4		VAR "Qtd. Pre Faturado"		SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha2+8, _nCol2 MSGET oGet4 	VAR cTotQtdPFat				SIZE _nTam, 010 OF oDlg PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL
	_nCol2 += _nCol2a
	@ _nLinha2  , _nCol2 SAY oSay4		VAR "Qtd. Perda"		 	SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha2+8, _nCol2 MSGET oGet4 	VAR cTotQtdPerda 			SIZE _nTam, 010 OF oDlg PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL
	_nCol2 += _nCol2a
	@ _nLinha2  , _nCol2 SAY oSay4		VAR "Qtd. Saldo"		 	SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha2+8, _nCol2 MSGET oGet4 	VAR cTotQtdSaldo 			SIZE _nTam, 010 OF oDlg PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL

	@ _nLinha3  , 006 SAY oSay2			VAR "F4 -> Consulta Itens com Saldo"		SIZE 100, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha3  , 130 SAY oSay2			VAR "F5 -> Atualiza Tela"					SIZE 070, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ _nLinha3  , 260 SAY oSay2			VAR "F6 -> Atualiza Tab.Prc/Cond.Pagto."	SIZE 110, 010 OF oDlg COLORS 0, 16777215 PIXEL
	
	ACTIVATE MSDIALOG oDlg CENTERED
   
Return


//------------------------------------------------
Static Function fMSPedido()
//------------------------------------------------
	Local nX
	Local aHeaderEx := {}
	Local aFieldFill := {}
	Local aFields :=  {"STATUS"    ,"C5_EMISSAO"   ,"C6_ENTREG"   ,"C5_NUM" ,"C5_XPEDMIL"     ,"C6_NOTA","C6_DATFAT"        ,"C5_CLIENTE" ,"A1_NOME"      ,"A1_XCOD"       , "C6_VALOR"    ,"C5_DESC1" ,"C6_VALOR","C6_VALOR" ,"C6_QTDVEN"  ,"C6_QTDVEN"      ,"C6_QTDVEN"     ,"C6_QTDVEN"  ,"C6_QTDVEN"  ,"E4_CODIGO" ,"E4_DESCRI"         ,"C5_TPPED"  ,"C5_DESCTPP"             ,"C5_XPEDCLI"     ,"C5_ORIGEM" ,"C5_XCANALD"    ,"C5_TABELA"  ,"C5_XDESTAB"             ,"C5_POLCOM"   ,"C5_XDESCPO"      ,"C5_FILIAL"     ,"A3_COD"       ,"A3_NOME"            ,"A1_END"   ,"A1_BAIRRO" ,"A1_MUN" ,"A1_EST" }
	Local aFields2 := {"Legenda"   ,"Dt. Emissao"  ,"Dt. Entrega" ,"Numero" ,"Ped. Millenium" ,"Notas Emitidas" ,"Data Faturamento", "Cod. Clie"  ,"Nome Cliente" ,"Cli.Millenium" , "Valor Venda" ,"Desconto" ,"Valor Total" ,"Vlr. Pre. Fat.","Qtd. Venda" ,"Qtd. Pre. Fat." ,"Qtd. Entregue" ,"Qtd. Perda" ,"Qtd. Saldo" ,"Cond Pag"  ,"Descri. Cond Pag." ,"Tp Pedido" ,"Descri. Tipo de Pedido" ,"Pedido Cliente" ,"Origem"    ,"Descri. Canal" ,"Tab. Preco" ,"Descr. Tabela de Pre�o" ,"Cod. Polit." ,"Descr. Politica" ,"Codigo Filial" ,"Cod. Repres." ,"Nome Representante" ,"Endereco" ,"Bairro"    ,"Cidade" ,"Estado" }
	Local aAlterFields := {}
	Static oMSPedido

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
  
	For nX := 1 to Len(aFields)
		If aFields[nX] $ "C6_QTDVEN|C6_QTDLIB|C6_SLDALIB|QTD_PREFAT"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,"@E 99,999,999,999.9999",16,4,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		ElseIf aFields[nX] $ "C5_DESC1"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,"@E 99.99",5,2,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		ElseIf aFields[nX] $ "C6_PRCVEN|C6_VALOR"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,"@E 9,999,999,999,999.99",16,2,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		ElseIf aFields[nX] $ "C5_EMISSAO"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,"@D 99/99/9999",SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		ElseIf aFields[nX] $ "STATUS"
			Aadd(aHeaderEx, {aFields2[nX],"COR"        ,"@BMP"         , 2             , 0             , ".F." ,;
				""           , "C"        , ""       , "V"           ,""          , ""   })
		Else
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		End
	Next nX

	Processa( {|| fQuery(0,_xvend,.f.) }, "Processando...", "Carregando defini��o dos campos...",.F.)
  
	oMSPedido := MsNewGetDados():New( 007, 007, _nAOmsPed, _nLOmsPed, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields, , 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return


Static Function Filtro()
	Local oButton1
	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Local oSay7
	Local oSay8
	Local oSay9
	Local oSay10
	Local oSay11
	Local oSay12
	Local oSay13
	Local oSay14
	Local oSay15
	Local oSay16
	Local oSay17
	Local oSay18
	Local oSay19
	Local oSay20
	Local oSay21
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oGet6
	Local oGet7
	Local oGet8
	Local oGet9
	Local oGet10
	Local oGet11
	Local oGet12
	Local oGet13
	Local oGet14
	Local oGet15
	Local oGet16
	Local oDtTipo
	Local oOrigem
	Local oStatus
	Local oPedFat
	Local oOrdem
	Local oOperacao
	Local oTpValTot

	Static oFiltro

	cProduto := Space(15)
	DEFINE MSDIALOG oFiltro TITLE "Filtro" FROM 000, 000  TO 410, 610 COLORS 0, 16777215 PIXEL

	@ 010, 018 SAY oSay1	VAR "Data Pesqisa " 			SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 009, 070 MSCOMBOBOX oDtTipo VAR cDtTipo			ITEMS {"Emiss�o","Entrega"}					SIZE 060, 013 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 010, 170 SAY oSay2	VAR "Filial"				 	SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 009, 232 MSGET oGet2	VAR cFili						SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "SM0EMP" PIXEL

	@ 025, 018 SAY oSay3	VAR "Data de " 					SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 024, 070 MSGET oGet3	VAR cDtDe	 					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 025, 170 SAY oSay4	VAR "Data At� " 				SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 024, 232 MSGET oGet4	VAR cDtAte 						SIZE 060, 010 OF oFiltro COLORS 0, 16777215 PIXEL
    
	@ 040, 018 SAY oSay5	VAR "Pedido " 					SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 039, 070 MSGET oGet5	VAR cPed 						SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "SC5"		PIXEL
	@ 040, 170 SAY oSay11	VAR "Representante "	 		SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	If AllTrim(_xvend) == ""
		@ 039, 232 MSGET oGet11	VAR cRepres 				SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "SA3"		PIXEL
	Else
		@ 039, 232 MSCOMBOBOX oGet11	VAR cRepres 	ITEMS aVendItem		SIZE 060, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	End

	@ 055, 018 SAY oSay7	VAR "Produto "	 				SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 054, 070 MSGET oGet7	VAR cProduto 					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "SB1"		PIXEL
	@ 055, 170 SAY oSay6	VAR "Tipo de Opera��o " 		SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 054, 232 MSGET oGet6	VAR cTpOpera 					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "DJ"			PIXEL
        
	@ 070, 018 SAY oSay9	VAR "Pedido Millenium"			SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 069, 070 MSGET oGet9	VAR cPedMill 					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 070, 170 SAY oSay10	VAR "Pedido Cliente"			SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 069, 232 MSGET oGet10	VAR cPedCli 					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 PIXEL

	@ 085, 018 SAY oSay9	VAR "Cliente " 					SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 084, 070 MSGET oGet9	VAR cCliente 					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "SA1AZ0"		PIXEL
	@ 085, 170 SAY oSay11	VAR "Nome Clinte "		 		SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 084, 232 MSGET oGet11	VAR cCliNome 					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 PIXEL

	cRegiao := Space(10)
	@ 100, 018 SAY oSay12	VAR "Regiao " 					SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 099, 070 MSGET oGet12	VAR cRegiao 					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "SZ8"		PIXEL
	@ 100, 170 SAY oSay13	VAR "Macro Regiao " 			SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 099, 232 MSGET oGet13	VAR cMicRegiao					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "ZA4-01"		PIXEL

	@ 115, 018 SAY oSay14	VAR "Pol. Comercial " 			SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 114, 070 MSGET oGet14	VAR cPolcom						SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "SZ2"		PIXEL
	@ 115, 170 SAY oSay15	VAR "Tabela de Preco "			SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 114, 232 MSGET oGet15	VAR cTabPreco					SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "DA0"		PIXEL

	@ 130, 018 SAY oSay16	VAR "Tipo de Pedido " 			SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 129, 070 MSGET oGet16	VAR cTpPed 						SIZE 060, 010 OF oFiltro COLORS 0, 16777215 F3 "SZ1"		PIXEL
	@ 130, 170 SAY oSay17 	VAR "Pedido Faturamento " 		SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 129, 232 MSCOMBOBOX oPedFat VAR cPedFat			ITEMS {"Nao Faturado/Parcial","Faturado Total","Todos"}				SIZE 060, 013 OF oFiltro COLORS 0, 16777215 PIXEL
 
	@ 145, 018 SAY oSay18	VAR "Origem Pedido " 			SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 144, 070 MSCOMBOBOX oOrigem VAR cOrigem 			ITEMS {"Todos","B2B","B2C","ItMobile","Manual"}		SIZE 060, 013 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 145, 170 SAY oSay19 	VAR "Status Pedido " 			SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 144, 232 MSCOMBOBOX oStatus VAR cStatus			ITEMS {"Liberados","Bloqueados","Todos"}				SIZE 060, 013 OF oFiltro COLORS 0, 16777215 PIXEL

	@ 160, 018 SAY oSay20	VAR "Valor Total" 				SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 159, 070 MSCOMBOBOX oTpValTot VAR cTpValTot			ITEMS {"Perdida","Entregue","a Entregar","Pre Fat","Todas"}		SIZE 060, 013 OF oFiltro COLORS 0, 16777215 PIXEL

	@ 180, 018 SAY oSay21	VAR "Ordenar por" 				SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
	@ 180, 070 MSCOMBOBOX oOdem VAR cOrdem 				ITEMS {"Data de Emiss�o","Data de Entrega","Pedido"}	SIZE 060, 013 OF oFiltro COLORS 0, 16777215 PIXEL
    
	@ 180, 150 BUTTON oButton1 PROMPT "Limpar Filtro" 		SIZE 040, 012 ACTION (LimpaTela(1)) OF oFiltro PIXEL
	@ 180, 200 BUTTON oButton1 PROMPT "Filtrar" 			SIZE 040, 012 ACTION (Close(oFiltro),Pquery(1,_xvend)) OF oFiltro PIXEL
	@ 180, 250 BUTTON oButton1 PROMPT "Cancelar"			SIZE 040, 012 ACTION (Close(oFiltro)) OF oFiltro PIXEL

	ACTIVATE MSDIALOG oFiltro CENTERED

Return


Static Function LimpaTela()

	cDtTipo			:= "Emiss�o"
	cDtDe			:= Ctod("01/01/00")
	cDtAte			:= Ctod("31/12/49")
	cFili			:= Space(04)
	cPed			:= Space(06)
	cTpOpera		:= Space(02)
	cProduto		:= Space(15)
	cCliente		:= Space(06)
	cCliNome		:= Space(20)
	cRepres 		:= Space(06)
	cPedMill 		:= Space(20)
	cPedCli 		:= Space(20)
	cRegiao			:= Space(10)
	cMicRegiao		:= Space(10)
	cPolcom			:= Space(03)
	cTpPed			:= Space(03)
	cTabPreco		:= Space(03)
	cOrigem			:= "Todos"
	cStatus			:= "Todos"
	cTpValTot		:= "Todas"
	cQuant 			:= 0
	oFiltro:Refresh()

Return

Static Function Pquery(_vez,_xvend)
	
	Processa( {|| Fquery(_vez,_xvend) }, "Processando...", "Carregando defini��o dos campos...")

Return

Static Function Pquery1(_vez,_xvend)
	
	Processa( {|| Fquery1(_vez,_xvend) }, "Processando...", "Carregando defini��o dos campos...")

Return

Static Function Fquery(_vez,_xvend)

	Local cEOL			:= +Chr(13)+Chr(10)
	Local _xHuLbCom		:= AllTrim(GetMV("MV_HULBCOM")) //000000/000088  //Usu�rios com permiss�o para todas as Macro Regi�es
	Local _xMacReg		:= ""
	Local _qry			:= ""
	Local nx 			:= 0

	Private oGreen   	:= LoadBitmap( GetResources(), "BR_VERDE")			//Pedido Bloqueado
	Private oBlue		:= LoadBitmap( GetResources(), "BR_AZUL")			//Pedido Bloqueado
	Private oOrange  	:= LoadBitmap( GetResources(), "BR_LARANJA")		//Pedido Faturado / Parcial
	Private oRed    	:= LoadBitmap( GetResources(), "BR_VERMELHO")		//Pedido Faturado / Total
	Private oBlack		:= LoadBitmap( GetResources(), "BR_PRETO")			//Pedido com Perda
	Private oWhite	  	:= LoadBitmap( GetResources(), "BR_BRANCO")			//Pedido com erro
	    
	If Select("TMPSC5") > 0
		TMPSC5->(DbCloseArea())
	EndIf
	
	_qry := " SELECT T.STATUS, T.PEDIDO, T.PEDMIL, T.C5_DESCONT, T.DTEMISSAO,T.DTENTREGA,T.QTD_PED,T.QTD_PREFAT,T.QTD_ENTREGUE,  "
	_qry += " T.QTD_PERDA,T.QTD_SALDO,T.NFISCAL,T.DTFAT,T.VALPED,T.PRECO_TOTAL,CASE WHEN ROUND(T.VALPED - T.PRECO_TOTAL,2) > '0' THEN ROUND(T.VALPED - T.PRECO_TOTAL,2) ELSE '0' END AS DESCONTO,T.VALPREFAT,T.VALENTREGUE,T.VALPERDA, "
	_qry += " T.VALSALDO,T.COD_CLIENTE,T.NOME_CLIENTE,T.CLI_MIL,T.PEDRAK,T.COND_PAGTO,T.DESCRI_COND_PAGTO,T.PEDCLI,T.ORIGEM, " 
	_qry += " T.CANAL,T.POLCOM,T.DESCPOL,T.TBPRECO,T.DESCTBPRECO,T.COD_REPRES,T.REPRES,T.FILIAL,T.ENDERECO,T.BAIRRO,T.CIDADE, " 
	_qry += " T.ESTADO,T.TPPED,T.[MACRO REGIAO]  FROM ( " 
	
	If lPassou
		_qry += " SELECT C5_XBLQ as 'STATUS', "+cEOL
	Else
		_qry += " SELECT TOP 0 C5_XBLQ as 'STATUS', "+cEOL
		lPassou := .T.
	End
	_qry += " 		C5_NUM AS PEDIDO, C5_XPEDMIL AS PEDMIL,C5_DESCONT ,C5_EMISSAO AS DTEMISSAO, "+cEOL
	_qry += " 		 CASE WHEN C5_FECENT<>'' THEN C5_FECENT ELSE (SELECT MAX(SC6X.C6_ENTREG) "+cEOL
	_qry += " 		 			FROM "+RetSqlName("SC6")+" AS SC6X  (NOLOCK) WHERE SC6X.C6_NUM=C5_NUM) END AS DTENTREGA, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_QTDVEN) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS QTD_PED, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(ZJ_QTDLIB) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "							INNER JOIN "+RetSqlName("SZJ")+" SZJ  (NOLOCK) ON SZJ.D_E_L_E_T_= '' AND ZJ_FILIAL=C5_FILIAL "+cEOL
	_qry += "											AND ZJ_PEDIDO=C6_NUM AND ZJ_ITEM=C6_ITEM AND ZJ_PRODUTO=C6_PRODUTO AND ZJ_DOC='' "+cEOL
	_qry += " 				 	WHERE SC6.D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS QTD_PREFAT, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS QTD_ENTREGUE, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ='R' AND C6_NUM=C5_NUM),0) AS QTD_PERDA, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ<>'R' AND C6_NUM=C5_NUM),0) AS QTD_SALDO, "+cEOL
	_qry += " 		ISNULL(CONVERT(VARCHAR(1000),(SELECT CONVERT(VARCHAR(9),C9_NFISCAL) + '-' AS [text()]"+cEOL
	_qry += " 				    	FROM "+RetSqlName("SC9")+" SC9  (NOLOCK) "+cEOL
	_qry += " 					    WHERE D_E_L_E_T_= '' AND C9_FILIAL=C5_FILIAL AND C9_NFISCAL<>'' AND C9_PEDIDO=C5_NUM "+cEOL
	_qry += " 					    GROUP BY C9_NFISCAL, C9_PEDIDO "+cEOL
	_qry += " 					    FOR XML AUTO)),'') AS NFISCAL, "+cEOL
	_qry += " 		ISNULL(CONVERT(VARCHAR(1000),(SELECT CONVERT(VARCHAR(10),CONVERT(CHAR, CAST(C6_DATFAT AS SMALLDATETIME),103)) + '-' AS [text()]"+cEOL
	_qry += " 				    	FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 					    WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NOTA<>'' AND C6_NUM=C5_NUM "+cEOL
	_qry += " 					    GROUP BY C6_DATFAT, C6_NUM "+cEOL
	_qry += " 					    FOR XML AUTO)),'') AS DTFAT, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_PRUNIT*C6_QTDVEN) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS VALPED, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_VALOR) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS PRECO_TOTAL, "+cEOL
	//_qry += " 	CASE WHEN  ISNULL((SELECT SUM(C6_PRUNIT*C6_QTDVEN) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) WHERE D_E_L_E_T_= ''  AND C6_FILIAL=C5_FILIAL  AND C6_NUM=C5_NUM  AND C6_LOJA = C5_LOJACLI),0)  = 0 "+cEOL
	//_qry += "	THEN 0 ELSE " +cEOL
	//_qry += "  ROUND(((ISNULL((SELECT SUM(C6_PRUNIT*C6_QTDVEN) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) " +cEOL 
    //_qry += "  WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL  AND C6_NUM=C5_NUM AND C6_CLI = C5_CLIENT AND C6_LOJA = C5_LOJACLI),0) - ISNULL((SELECT SUM(C6_VALOR) " +cEOL 
    //_qry += "  FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL  AND C6_NUM=C5_NUM AND C6_CLI = C5_CLIENT AND C6_LOJA = C5_LOJACLI),0)) * 100 ) " +cEOL 
	//_qry += "   / ISNULL((SELECT SUM(C6_QTDVEN*C6_PRUNIT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM AND C6_CLI = C5_CLIENT AND C6_LOJA = C5_LOJACLI),0),2) END AS DESCONTO, " +cEOL 
	_qry += " 	  ISNULL((SELECT SUM(C6_PRCVEN*ZJ_QTDLIB) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "	  INNER JOIN "+RetSqlName("SZJ")+" SZJ  (NOLOCK) ON SZJ.D_E_L_E_T_= '' AND ZJ_FILIAL=C5_FILIAL "+cEOL
	_qry += "	  AND ZJ_PEDIDO=C6_NUM AND ZJ_ITEM=C6_ITEM AND ZJ_PRODUTO=C6_PRODUTO AND ZJ_DOC='' "+cEOL
	_qry += " 				 	WHERE SC6.D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS VALPREFAT, "+cEOL
	_qry += "  		ISNULL((SELECT SUM(C6_PRCVEN*C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "  				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS VALENTREGUE, "+cEOL
	_qry += "  		ISNULL((SELECT SUM(C6_PRCVEN*(C6_QTDVEN-C6_QTDENT)) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "  				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ='R' AND C6_NUM=C5_NUM),0) AS VALPERDA, "+cEOL
	_qry += "  		ISNULL((SELECT SUM(C6_PRCVEN*(C6_QTDVEN-C6_QTDENT)) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "  				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ<>'R' AND C6_NUM=C5_NUM),0) AS VALSALDO, "+cEOL
	_qry += " 		C5_CLIENTE AS COD_CLIENTE, A1_NOME AS NOME_CLIENTE, A1_XCOD AS CLI_MIL, C5_XPEDWEB as PEDRAK, "+cEOL
	_qry += " 		E4_CODIGO AS COND_PAGTO, E4_DESCRI AS DESCRI_COND_PAGTO, C5_XPEDCLI AS PEDCLI, C5_ORIGEM AS ORIGEM, "+cEOL
	_qry += "		C5_XCANALD AS CANAL, C5_POLCOM AS POLCOM, C5_XDESCPO AS DESCPOL, C5_TABELA AS TBPRECO, "+cEOL
	_qry += "		C5_XDESTAB AS DESCTBPRECO, ISNULL(A3_COD,'') AS COD_REPRES, ISNULL(A3_NOME,'') AS REPRES, "+cEOL
	_qry += "		C5_FILIAL AS FILIAL, A1_END AS ENDERECO, A1_BAIRRO AS BAIRRO, A1_MUN AS CIDADE, A1_EST AS ESTADO, "+cEOL
	_qry += "		C5_TPPED AS TPPED, A1_XMICRRE AS 'MACRO REGIAO',SC5.R_E_C_N_O_ "+cEOL
	_qry += " 	FROM "+RetSqlName("SC5")+" SC5  (NOLOCK) "+cEOL
	_qry += " 	INNER JOIN "+RetSqlName("SC6")+" SC6  (NOLOCK) ON SC6.D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C5_NUM = C6_NUM "+cEOL
	_qry += " 	INNER JOIN "+RetSqlName("SA1")+" SA1  (NOLOCK) ON SA1.D_E_L_E_T_= '' AND C5_CLIENTE=A1_COD AND C5_LOJACLI=A1_LOJA "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("SE4")+" SE4  (NOLOCK) ON SE4.D_E_L_E_T_= '' AND C5_CONDPAG=E4_CODIGO "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("SA3")+" SA3  (NOLOCK) ON SA3.D_E_L_E_T_= '' AND C5_VEND1=A3_COD "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("DA0")+" DA0  (NOLOCK) ON DA0.D_E_L_E_T_= '' AND C5_TABELA=DA0_CODTAB "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("SA4")+" SA4  (NOLOCK) ON SA4.D_E_L_E_T_= '' AND C5_TRANSP=A4_COD "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("SZ1")+" SZ1  (NOLOCK) ON SZ1.D_E_L_E_T_= '' AND C5_TPPED=Z1_CODIGO "+cEOL
	_qry += " WHERE SC5.D_E_L_E_T_= '' "+cEOL


	If AllTrim(cProduto)<>''
			_qry += " 	AND C6_PRODUTO LIKE '%"+AllTrim(cProduto)+"%' "+cEOL
	End

	If cPedFat=="Nao Faturado/Parcial"
		_qry += " 	AND (C5_NOTA = '' AND C6_BLQ<>'R') "+cEOL
		_qry += " 	AND ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
		_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ<>'R' AND C6_NUM=C5_NUM),0) <> 0 "+cEOL
	ElseIf cPedFat=="Faturado Total"
		_qry += " 	AND (C5_NOTA <> '' OR C6_BLQ='R' "+cEOL
		_qry += " 	OR ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
		_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ<>'R' AND C6_NUM=C5_NUM),0) = 0 )"+cEOL
	End
  
	If cDtTipo=="Emiss�o"
		_qry += " AND C5_EMISSAO >= '"+dtos(cDtDe)+"' AND C5_EMISSAO <= '"+dtos(cDtAte)+"' "+cEOL
	ElseIf cDtTipo=="Entrega"
		_qry += " 	AND C6_ENTREG >= '"+dtos(cDtDe)+"' AND C6_ENTREG <= '"+dtos(cDtAte)+"' "+cEOL
	End

	If AllTrim(cFili)<>''
		_qry += " 	AND C5_FILIAL = '"+cFili+"' "+cEOL
	End


	If AllTrim(cPed)<>''
		_qry += " 	AND C5_NUM LIKE '"+ AllTrim(cPed)+"%' "+cEOL
	End

	If AllTrim(cTpOpera)<>''
		_qry += " 	AND Z1_TPOPER LIKE '%"+Alltrim(cTpOpera)+"%' "+cEOL
	End
 
	If AllTrim(cCliente)<>''
		_qry += "	AND C5_CLIENTE LIKE '%"+Alltrim(cCliente)+"%' "+cEOL
	End

	If AllTrim(cCliNome)<>''
		_qry += "	AND A1_NOME LIKE '%"+UPPER(AllTrim(cCliNome))+"%' "+cEOL
	End

	If AllTrim(cPedMill)<>''
		_qry += "	AND (C5_XPEDMIL LIKE '%"+Alltrim(cPedMill)+"%' OR C5_XPEDWEB LIKE '%"+Alltrim(cPedMill)+"%' )"+cEOL
	End

	If AllTrim(cPedCli)<>''
		_qry += "	AND C5_XPEDCLI LIKE '%"+Alltrim(cPedCli)+"%' "+cEOL
	End
	
	_xvend	:= alltrim(_xvend)
	If _xvend <> ''
		If AllTrim(cRepres)<>''
			_qry += "	AND C5_VEND1 = '"+cRepres+"' "+cEOL
		Else
			_qry += " AND C5_VEND1 IN ("+SubStr(_xvend,1,len(_xvend)-1)+") "+cEOL
		End
	Else
		If AllTrim(cRepres)<>''
			_qry += "	AND C5_VEND1 = '"+cRepres+"' "+cEOL
		End
	Endif
	
	If _xHuLbCom <> ''
		If !(__CUSERID $ _xHuLbCom)
			DbSelectArea("ZZ4")
			DbSetOrder(1)
			DbSeek(xfilial("ZZ4")+__CUSERID)
			While ZZ4->(!Eof()) .AND. ZZ4->ZZ4_USER==__CUSERID
				_xMacReg += "'"+AllTrim(ZZ4->ZZ4_REGIAO)+"',"
				ZZ4->(DbSkip())
			EndDo
			
			_xMacReg	:= alltrim(_xMacReg)
			If _xMacReg <> ''
				_qry += " 	AND A1_XMICRRE IN ("+SubStr(_xMacReg,1,len(_xMacReg)-1)+") "+cEOL
			Else
				_qry += " 	AND A1_XMICRRE = '' "+cEOL
			End
		End
	End

	If AllTrim(cRegiao)<>''
		_qry += " 	AND C5_XCODREG LIKE '%"+Alltrim(cRegiao)+"%' "+cEOL
	End

	If AllTrim(cMicRegiao)<>''
		_qry += " 	AND C5_XMICRRE LIKE '%"+Alltrim(cMicRegiao)+"%' "+cEOL
	End

	If AllTrim(cPolcom)<>''
		_qry += " 	AND C5_POLCOM LIKE '%"+Alltrim(cPolcom)+"%' "+cEOL
	End

	If AllTrim(cTpPed)<>''
		_qry += " 	AND C5_TPPED LIKE '%"+Alltrim(cTpPed)+"%' "+cEOL
	End

	If AllTrim(cTabPreco)<>''
		_qry += " 	AND C5_TABELA LIKE '%"+Alltrim(cTabPreco)+"%' "+cEOL
	End

	If AllTrim(cOrigem)<>'Todos'
			_qry += " 	AND C5_ORIGEM LIKE '%"+Alltrim(Upper(cOrigem))+"%' "+cEOL
	Endif
	
	If cPedFat=="Nao Faturado/Parcial"
		If Substring(cStatus,1,1)='L'
			_qry += "	AND C5_XBLQ='L' "+cEOL
		ElseIf Substring(cStatus,1,1)='B'
			_qry += "	AND C5_XBLQ='B' "+cEOL
		End
	End

	_qry += "  GROUP BY C5_XBLQ, C5_NUM,C5_DESCONT, C5_XPEDMIL, C5_XPEDWEB, C5_CLIENTE, C5_EMISSAO, C5_FECENT, "+cEOL
	_qry += "  		A1_NOME, A1_XCOD, E4_CODIGO, E4_DESCRI, C5_XPEDCLI, A3_COD, A3_NOME, C5_TABELA, C5_ORIGEM, "+cEOL
	_qry += "  		C5_XCANALD, C5_POLCOM, C5_XDESCPO, DA0_DESCRI, C5_DESC1, C5_DESC2, C5_DESC3, C5_DESC4, C5_FILIAL, C5_XDESTAB, A1_END, "+cEOL
	_qry += "  		A1_BAIRRO, A1_EST, A1_MUN, C5_TPPED, C5_NOTA, A1_XMICRRE,C5_CLIENT,C5_LOJACLI ,SC5.R_E_C_N_O_ "+cEOL
	
	_qry += " ) T  ORDER BY R_E_C_N_O_   "

	//_qry += "  ORDER BY SC5.R_E_C_N_O_ "+cEOL
	//_qry := ChangeQuery(_qry)
	MEMOWRITE("HFAP011_QRY.TXT",_qry)
	If Select("TMPSC5") > 0
       DbSelectArea("TMPSC5")
       DbCloseArea()
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSC5",.T.,.T.)
  
	DbSelectArea("TMPSC5")
	DbGoTop()
  
	_tValPed		:= 0
	_tValDesc		:= 0
	_tValTot		:= 0
	_tValPFat		:= 0
	_tValEntreg		:= 0
	_tValPerda		:= 0
	_tValSaldo		:= 0
	_tQtdPed		:= 0
	_tQtdPFat		:= 0
	_tQtdEnt		:= 0
	_tQtdPerda		:= 0
	_tQtdSaldo		:= 0
	_tVaDesc        := 0
  
	aColsEx 		:= {}
	IF _vez > 0
		oMSPedido:aCols := aClone(aColsEx)
	Endif

	If !EOF()
	  
		While !EOF()
	
			aFieldFill := {}
	
			If TMPSC5->QTD_SALDO = 0 .AND. TMPSC5->QTD_PERDA = 0  //'FATURADO / TOTAL'
				Aadd(aFieldFill, oRed)
			ElseIf TMPSC5->QTD_PED <> TMPSC5->QTD_SALDO .AND. TMPSC5->QTD_SALDO > 0 .AND. TMPSC5->QTD_PERDA = 0  //'FATURADO / PARCIAL'
				Aadd(aFieldFill, oOrange)
			ElseIf TMPSC5->QTD_PED <> TMPSC5->QTD_SALDO .AND. TMPSC5->QTD_SALDO > 0 .AND. TMPSC5->QTD_PERDA > 0  //'FATURADO / PARCIAL'
				Aadd(aFieldFill, oOrange)
			ElseIf TMPSC5->QTD_SALDO = 0 .AND. TMPSC5->QTD_PERDA <> 0  //'PERDA'
				Aadd(aFieldFill, oBlack)
			ElseIf TMPSC5->STATUS =='L' .AND. TMPSC5->QTD_SALDO <> 0 .AND. TMPSC5->QTD_PERDA = 0
				Aadd(aFieldFill, oGreen)
			ElseIf TMPSC5->STATUS =='B' .AND. TMPSC5->QTD_SALDO <> 0 .AND. TMPSC5->QTD_PERDA = 0 .OR. TMPSC5->STATUS ==' '
				Aadd(aFieldFill, oBlue)
			ElseIf TMPSC5->STATUS ==' '
				Aadd(aFieldFill, oWhite)
			Endif
	
			Aadd(aFieldFill, Stod(TMPSC5->DTEMISSAO))
			Aadd(aFieldFill, Stod(TMPSC5->DTENTREGA))
			Aadd(aFieldFill, TMPSC5->PEDIDO)
			If alltrim(TMPSC5->PEDMIL) <> ""
				Aadd(aFieldFill, TMPSC5->PEDMIL)
			Else
				Aadd(aFieldFill, TMPSC5->PEDRAK)
			Endif
			
			//			Aadd(aFieldFill, SUBSTRING(TMPSC5->NFISCAL,1,LEN(ALLTRIM(TMPSC5->NFISCAL))-1))
			cNfiscal := StrTran(AllTrim(TMPSC5->NFISCAL),'<SC9 text_x0028__x0029_="',"")
			cNfiscal := StrTran(cNfiscal,'-"/>',"-")
			cNfiscal := SUBSTRING(cNfiscal,1,LEN(cNfiscal)-1)
			Aadd(aFieldFill, SUBSTRING(cNfiscal,1,100))

			ddtfat := StrTran(AllTrim(TMPSC5->DTFAT),'<SC6 text_x0028__x0029_="',"")
			ddtfat := StrTran(ddtfat,'-"/>',"-")
			ddtfat := SUBSTRING(ddtfat,1,LEN(ddtfat)-1)
			//Aadd(aFieldFill, STOD(ddtfat))
			Aadd(aFieldFill, SUBSTRING(ddtfat,1,100))
	
			Aadd(aFieldFill, TMPSC5->COD_CLIENTE)
			Aadd(aFieldFill, TMPSC5->NOME_CLIENTE)
			//Aadd(aFieldFill, TMPSC5->CLI_MIL)
			Aadd(aFieldFill, TMPSC5->CLI_MIL)
			Aadd(aFieldFill, TMPSC5->VALPED)
			_tVaDesc := (TMPSC5->DESCONTO * 100) / TMPSC5->VALPED
			Aadd(aFieldFill, _tVaDesc)
			Aadd(aFieldFill, TMPSC5->PRECO_TOTAL - TMPSC5->C5_DESCONT )
			Aadd(aFieldFill, TMPSC5->VALPREFAT)
			Aadd(aFieldFill, TMPSC5->QTD_PED)
			Aadd(aFieldFill, TMPSC5->QTD_PREFAT)
			Aadd(aFieldFill, TMPSC5->QTD_ENTREGUE)
			Aadd(aFieldFill, TMPSC5->QTD_PERDA)
			Aadd(aFieldFill, TMPSC5->QTD_SALDO)
			Aadd(aFieldFill, TMPSC5->COND_PAGTO)
			Aadd(aFieldFill, TMPSC5->DESCRI_COND_PAGTO)
			Aadd(aFieldFill, TMPSC5->TPPED)
			Aadd(aFieldFill, Alltrim(POSICIONE("SZ1",1,xFilial("SZ1")+TMPSC5->TPPED,"Z1_DESC")))
			Aadd(aFieldFill, TMPSC5->PEDCLI)
			Aadd(aFieldFill, TMPSC5->ORIGEM)
			Aadd(aFieldFill, TMPSC5->CANAL)
			Aadd(aFieldFill, TMPSC5->TBPRECO)
			Aadd(aFieldFill, TMPSC5->DESCTBPRECO)
			Aadd(aFieldFill, TMPSC5->POLCOM)
			Aadd(aFieldFill, TMPSC5->DESCPOL)
			Aadd(aFieldFill, TMPSC5->FILIAL)
			Aadd(aFieldFill, TMPSC5->COD_REPRES)
			Aadd(aFieldFill, TMPSC5->REPRES)
			Aadd(aFieldFill, TMPSC5->ENDERECO)
			Aadd(aFieldFill, TMPSC5->BAIRRO)
			Aadd(aFieldFill, TMPSC5->CIDADE)
			Aadd(aFieldFill, TMPSC5->ESTADO)
			Aadd(aFieldFill, TMPSC5->VALPREFAT - ( TMPSC5->C5_DESCONT +((TMPSC5->VALPREFAT/100)*TMPSC5->DESCONTO)))
			Aadd(aFieldFill, TMPSC5->VALENTREGUE- (TMPSC5->C5_DESCONT + ((TMPSC5->VALENTREGUE/100)*TMPSC5->DESCONTO)))
			Aadd(aFieldFill, TMPSC5->VALPERDA- (TMPSC5->C5_DESCONT + ((TMPSC5->VALPERDA/100)*TMPSC5->DESCONTO)))
			Aadd(aFieldFill, TMPSC5->VALSALDO- (TMPSC5->C5_DESCONT + ((TMPSC5->VALSALDO/100)*TMPSC5->DESCONTO)))
			Aadd(aFieldFill, .F.)
			Aadd(aColsEx, aFieldFill)
	  		 
			_tValPed		+= TMPSC5->VALPED
			_tValDesc		+= TMPSC5->DESCONTO //TMPSC5->C5_DESCONT + ((TMPSC5->VALPED/100) * TMPSC5->DESCONTO) 2
			_tValTot		+= TMPSC5->PRECO_TOTAL - TMPSC5->C5_DESCONT
			_tValPFat		+= TMPSC5->VALPREFAT
			_tValEntreg		+= TMPSC5->VALENTREGUE
			_tValPerda		+= TMPSC5->VALPERDA
			_tValSaldo		+= TMPSC5->VALSALDO
			_tQtdPed		+= TMPSC5->QTD_PED
			_tQtdPFat		+= TMPSC5->QTD_PREFAT
			_tQtdEnt		+= TMPSC5->QTD_ENTREGUE
			_tQtdPerda		+= TMPSC5->QTD_PERDA
			_tQtdSaldo		+= TMPSC5->QTD_SALDO
	 
			DbSelectArea("TMPSC5")
			DbSkip()
		End
	  
	Else
  		
		aFieldFill := {}
		
		For nX := 1 to 36
			Aadd(aFieldFill, "")
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
  		
	End

	cTotValPed			:= _tValPed
	cTotValDesc			:= _tValDesc
	cTotQtdPed			:= _tQtdPed
	cTotQtdPFat			:= _tQtdPFat
	cTotQtdEnt			:= _tQtdEnt
	cTotQtdPerda		:= _tQtdPerda
	cTotQtdSaldo		:= _tQtdSaldo

	If cTpValTot == "Pre Fat"
		cTotValTot		:= _tValPFat
	ElseIf cTpValTot == "Perdida"
		cTotValTot		:= _tValPerda
	ElseIf cTpValTot == "Entregue"
		cTotValTot		:= _tValEntreg
	ElseIf cTpValTot == "a Entregar"
		cTotValTot		:= _tValSaldo
	Else
		cTotValTot		:= _tValTot
	End

	TMPSC5->(DbCloseArea())

	If _vez > 0
		oMSPedido:aCols := aClone(aColsEx)//aColsEx
		//oMSPedido:Refresh()
	Endif

Return

Static Function Fquery1(_vez,_xvend)

	Local cEOL			:= +Chr(13)+Chr(10)
	Local _xHuLbCom		:= AllTrim(GetMV("MV_HULBCOM")) //000000/000088  //Usu�rios com permiss�o para todas as Macro Regi�es
	Local _xMacReg		:= ""
	Local _qry			:= ""
	Local cRegiao       := Space(10)
	Local nx			:= 0

	Private oGreen   	:= LoadBitmap( GetResources(), "BR_VERDE")			//Pedido Bloqueado
	Private oBlue		:= LoadBitmap( GetResources(), "BR_AZUL")			//Pedido Bloqueado
	Private oOrange  	:= LoadBitmap( GetResources(), "BR_LARANJA")		//Pedido Faturado / Parcial
	Private oRed    	:= LoadBitmap( GetResources(), "BR_VERMELHO")		//Pedido Faturado / Total
	Private oBlack		:= LoadBitmap( GetResources(), "BR_PRETO")			//Pedido com Perda
	Private oWhite	  	:= LoadBitmap( GetResources(), "BR_BRANCO")			//Pedido com erro
	    
	If Select("TMPSC5") > 0
		TMPSC5->(DbCloseArea())
	EndIf
	
	
	_qry := " SELECT T.STATUS, T.PEDIDO, T.PEDMIL, T.C5_DESCONT, T.DTEMISSAO,T.DTENTREGA,T.QTD_PED,T.QTD_PREFAT,T.QTD_ENTREGUE,  "
	_qry += " T.QTD_PERDA,T.QTD_SALDO,T.NFISCAL,T.DTFAT,T.VALPED,T.PRECO_TOTAL,CASE WHEN ROUND(T.VALPED - T.PRECO_TOTAL,2) > '0' THEN ROUND(T.VALPED - T.PRECO_TOTAL,2) ELSE '0' END AS DESCONTO,T.VALPREFAT,T.VALENTREGUE,T.VALPERDA, "
	_qry += " T.VALSALDO,T.COD_CLIENTE,T.NOME_CLIENTE,T.CLI_MIL,T.PEDRAK,T.COND_PAGTO,T.DESCRI_COND_PAGTO,T.PEDCLI,T.ORIGEM, " 
	_qry += " T.CANAL,T.POLCOM,T.DESCPOL,T.TBPRECO,T.DESCTBPRECO,T.COD_REPRES,T.REPRES,T.FILIAL,T.ENDERECO,T.BAIRRO,T.CIDADE, " 
	_qry += " T.ESTADO,T.TPPED,T.[MACRO REGIAO]  FROM ( " 
	
	If lPassou
		_qry += " SELECT C5_XBLQ as 'STATUS', "+cEOL
	Else
		_qry += " SELECT TOP 0 C5_XBLQ as 'STATUS', "+cEOL
		lPassou := .T.
	End
	_qry += " 		C5_NUM AS PEDIDO, C5_XPEDMIL AS PEDMIL,C5_DESCONT ,C5_EMISSAO AS DTEMISSAO, "+cEOL
	_qry += " 		 CASE WHEN C5_FECENT<>'' THEN C5_FECENT ELSE (SELECT MAX(SC6X.C6_ENTREG) "+cEOL
	_qry += " 		 			FROM "+RetSqlName("SC6")+" AS SC6X  (NOLOCK) WHERE SC6X.C6_NUM=C5_NUM) END AS DTENTREGA, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_QTDVEN) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS QTD_PED, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(ZJ_QTDLIB) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "							INNER JOIN "+RetSqlName("SZJ")+" SZJ  (NOLOCK) ON SZJ.D_E_L_E_T_= '' AND ZJ_FILIAL=C5_FILIAL "+cEOL
	_qry += "											AND ZJ_PEDIDO=C6_NUM AND ZJ_ITEM=C6_ITEM AND ZJ_PRODUTO=C6_PRODUTO AND ZJ_DOC='' "+cEOL
	_qry += " 				 	WHERE SC6.D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS QTD_PREFAT, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS QTD_ENTREGUE, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ='R' AND C6_NUM=C5_NUM),0) AS QTD_PERDA, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ<>'R' AND C6_NUM=C5_NUM),0) AS QTD_SALDO, "+cEOL
	_qry += " 		ISNULL(CONVERT(VARCHAR(1000),(SELECT CONVERT(VARCHAR(9),C9_NFISCAL) + '-' AS [text()]"+cEOL
	_qry += " 				    	FROM "+RetSqlName("SC9")+" SC9  (NOLOCK) "+cEOL
	_qry += " 					    WHERE D_E_L_E_T_= '' AND C9_FILIAL=C5_FILIAL AND C9_NFISCAL<>'' AND C9_PEDIDO=C5_NUM "+cEOL
	_qry += " 					    GROUP BY C9_NFISCAL, C9_PEDIDO "+cEOL
	_qry += " 					    FOR XML AUTO)),'') AS NFISCAL, "+cEOL
	_qry += " 		ISNULL(CONVERT(VARCHAR(1000),(SELECT CONVERT(VARCHAR(10),CONVERT(CHAR, CAST(C6_DATFAT AS SMALLDATETIME),103))+ '-' AS [text()]"+cEOL
	_qry += " 				    	FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 					    WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NOTA<>'' AND C6_NUM=C5_NUM "+cEOL
	_qry += " 					    GROUP BY C6_DATFAT, C6_NUM "+cEOL
	_qry += " 					    FOR XML AUTO)),'') AS DTFAT, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_PRUNIT*C6_QTDVEN) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS VALPED, "+cEOL
	_qry += " 		ISNULL((SELECT SUM(C6_VALOR) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS PRECO_TOTAL, "+cEOL
	//_qry += " CASE WHEN  ISNULL((SELECT SUM(C6_PRUNIT*C6_QTDVEN) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) WHERE D_E_L_E_T_= ''  AND C6_FILIAL=C5_FILIAL  AND C6_NUM=C5_NUM),0)  = 0 "+cEOL
	//_qry += " THEN 0 ELSE "+cEOL 	
	//_qry += "  ROUND(((ISNULL((SELECT SUM(C6_PRUNIT*C6_QTDVEN) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) " +cEOL 
    //_qry += "  WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL  AND C6_NUM=C5_NUM AND C6_CLI = C5_CLIENT AND C6_LOJA = C5_LOJACLI),0) - ISNULL((SELECT SUM(C6_VALOR) " +cEOL 
    //_qry += "  FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL  AND C6_NUM=C5_NUM AND C6_CLI = C5_CLIENT AND C6_LOJA = C5_LOJACLI),0)) * 100 ) " +cEOL 
	//_qry += "   / ISNULL((SELECT SUM(C6_PRUNIT*C6_QTDVEN) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM AND C6_CLI = C5_CLIENT AND C6_LOJA = C5_LOJACLI),0),2) END AS DESCONTO, " +cEOL 
	_qry += " 		ISNULL((SELECT SUM(C6_PRCVEN*ZJ_QTDLIB) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "							INNER JOIN "+RetSqlName("SZJ")+" SZJ  (NOLOCK) ON SZJ.D_E_L_E_T_= '' AND ZJ_FILIAL=C5_FILIAL "+cEOL
	_qry += "											AND ZJ_PEDIDO=C6_NUM AND ZJ_ITEM=C6_ITEM AND ZJ_PRODUTO=C6_PRODUTO AND ZJ_DOC='' "+cEOL
	_qry += " 				 	WHERE SC6.D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS VALPREFAT, "+cEOL
	_qry += "  		ISNULL((SELECT SUM(C6_PRCVEN*C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "  				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM),0) AS VALENTREGUE, "+cEOL
	_qry += "  		ISNULL((SELECT SUM(C6_PRCVEN*(C6_QTDVEN-C6_QTDENT)) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "  				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ='R' AND C6_NUM=C5_NUM),0) AS VALPERDA, "+cEOL
	_qry += "  		ISNULL((SELECT SUM(C6_PRCVEN*(C6_QTDVEN-C6_QTDENT)) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
	_qry += "  				WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ<>'R' AND C6_NUM=C5_NUM),0) AS VALSALDO, "+cEOL
	_qry += " 		C5_CLIENTE AS COD_CLIENTE, A1_NOME AS NOME_CLIENTE, A1_XCOD AS CLI_MIL, C5_XPEDWEB as PEDRAK, "+cEOL
	_qry += " 		E4_CODIGO AS COND_PAGTO, E4_DESCRI AS DESCRI_COND_PAGTO, C5_XPEDCLI AS PEDCLI, C5_ORIGEM AS ORIGEM, "+cEOL
	_qry += "		C5_XCANALD AS CANAL, C5_POLCOM AS POLCOM, C5_XDESCPO AS DESCPOL, C5_TABELA AS TBPRECO, "+cEOL
	_qry += "		C5_XDESTAB AS DESCTBPRECO, ISNULL(A3_COD,'') AS COD_REPRES, ISNULL(A3_NOME,'') AS REPRES, "+cEOL
	_qry += "		C5_FILIAL AS FILIAL, A1_END AS ENDERECO, A1_BAIRRO AS BAIRRO, A1_MUN AS CIDADE, A1_EST AS ESTADO, "+cEOL
	_qry += "		C5_TPPED AS TPPED, A1_XMICRRE AS 'MACRO REGIAO', SC5.R_E_C_N_O_  "+cEOL
	_qry += " FROM "+RetSqlName("SC5")+" SC5  (NOLOCK) "+cEOL
	_qry += " 	INNER JOIN "+RetSqlName("SC6")+" SC6  (NOLOCK) ON SC6.D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C5_NUM = C6_NUM "+cEOL
	_qry += " 	INNER JOIN "+RetSqlName("SA1")+" SA1  (NOLOCK) ON SA1.D_E_L_E_T_= '' AND C5_CLIENTE=A1_COD AND C5_LOJACLI=A1_LOJA "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("SE4")+" SE4  (NOLOCK) ON SE4.D_E_L_E_T_= '' AND C5_CONDPAG=E4_CODIGO "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("SA3")+" SA3  (NOLOCK) ON SA3.D_E_L_E_T_= '' AND C5_VEND1=A3_COD "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("DA0")+" DA0  (NOLOCK) ON DA0.D_E_L_E_T_= '' AND C5_TABELA=DA0_CODTAB "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("SA4")+" SA4  (NOLOCK) ON SA4.D_E_L_E_T_= '' AND C5_TRANSP=A4_COD "+cEOL
	_qry += " 	 FULL JOIN "+RetSqlName("SZ1")+" SZ1  (NOLOCK) ON SZ1.D_E_L_E_T_= '' AND C5_TPPED=Z1_CODIGO "+cEOL
	_qry += " WHERE SC5.D_E_L_E_T_= '' "+cEOL


	If cPedFat=="Nao Faturado/Parcial"
		_qry += " 	AND (C5_NOTA = '' AND C6_BLQ<>'R') "+cEOL
		_qry += " 	AND ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
		_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ<>'R' AND C6_NUM=C5_NUM),0) <> 0 "+cEOL
	ElseIf cPedFat=="Faturado Total"
		_qry += " 	AND (C5_NOTA <> '' OR C6_BLQ='R' "+cEOL
		_qry += " 	OR ISNULL((SELECT SUM(C6_QTDVEN-C6_QTDENT) FROM "+RetSqlName("SC6")+" SC6  (NOLOCK) "+cEOL
		_qry += " 				 WHERE D_E_L_E_T_= '' AND C6_FILIAL=C5_FILIAL AND C6_BLQ<>'R' AND C6_NUM=C5_NUM),0) = 0 )"+cEOL
	End
  
	If cDtTipo=="Emiss�o"
		_qry += " AND C5_EMISSAO >= '"+dtos(cDtDe)+"' AND C5_EMISSAO <= '"+dtos(cDtAte)+"' "+cEOL
	ElseIf cDtTipo=="Entrega"
		_qry += " 	AND C6_ENTREG >= '"+dtos(cDtDe)+"' AND C6_ENTREG <= '"+dtos(cDtAte)+"' "+cEOL
	End

	If AllTrim(cFili)<>''
		_qry += " 	AND C5_FILIAL = '"+cFili+"' "+cEOL
	End


	If AllTrim(cPed)<>''
		_qry += " 	AND C5_NUM LIKE '"+ AllTrim(cPed)+"%' "+cEOL
	End

	If AllTrim(cTpOpera)<>''
		_qry += " 	AND Z1_TPOPER LIKE '%"+Alltrim(cTpOpera)+"%' "+cEOL
	End
 
	If AllTrim(cCliente)<>''
		_qry += "	AND C5_CLIENTE LIKE '%"+Alltrim(cCliente)+"%' "+cEOL
	End

	If AllTrim(cCliNome)<>''
		_qry += "	AND A1_NOME LIKE '%"+UPPER(AllTrim(cCliNome))+"%' "+cEOL
	End

	If AllTrim(cPedMill)<>''
		_qry += "	AND (C5_XPEDMIL LIKE '%"+Alltrim(cPedMill)+"%' OR C5_XPEDWEB LIKE '%"+Alltrim(cPedMill)+"%' )"+cEOL
	End

	If AllTrim(cPedCli)<>''
		_qry += "	AND C5_XPEDCLI LIKE '%"+Alltrim(cPedCli)+"%' "+cEOL
	End
	
	_xvend	:= alltrim(_xvend)
	If _xvend <> ''
		If AllTrim(cRepres)<>''
			_qry += "	AND C5_VEND1 = '"+cRepres+"' "+cEOL
		Else
			_qry += " AND C5_VEND1 IN ("+SubStr(_xvend,1,len(_xvend)-1)+") "+cEOL
		End
	Else
		If AllTrim(cRepres)<>''
			_qry += "	AND C5_VEND1 = '"+cRepres+"' "+cEOL
		End
	Endif
	
	If _xHuLbCom <> ''
		If !(__CUSERID $ _xHuLbCom)
			DbSelectArea("ZZ4")
			DbSetOrder(1)
			DbSeek(xfilial("ZZ4")+__CUSERID)
			While ZZ4->(!Eof()) .AND. ZZ4->ZZ4_USER==__CUSERID
				_xMacReg += "'"+AllTrim(ZZ4->ZZ4_REGIAO)+"',"
				ZZ4->(DbSkip())
			EndDo
			
			_xMacReg	:= alltrim(_xMacReg)
			If _xMacReg <> ''
				_qry += " 	AND A1_XMICRRE IN ("+SubStr(_xMacReg,1,len(_xMacReg)-1)+") "+cEOL
			Else
				_qry += " 	AND A1_XMICRRE = '' "+cEOL
			End
		End
	End

	If AllTrim(cRegiao)<>''
		_qry += " 	AND C5_XCODREG LIKE '%"+Alltrim(cRegiao)+"%' "+cEOL
	End

	If AllTrim(cMicRegiao)<>''
		_qry += " 	AND C5_XMICRRE LIKE '%"+Alltrim(cMicRegiao)+"%' "+cEOL
	End

	If AllTrim(cPolcom)<>''
		_qry += " 	AND C5_POLCOM LIKE '%"+Alltrim(cPolcom)+"%' "+cEOL
	End

	If AllTrim(cTpPed)<>''
		_qry += " 	AND C5_TPPED LIKE '%"+Alltrim(cTpPed)+"%' "+cEOL
	End

	If AllTrim(cTabPreco)<>''
		_qry += " 	AND C5_TABELA LIKE '%"+Alltrim(cTabPreco)+"%' "+cEOL
	End

	If cPedFat=="Nao Faturado/Parcial"
		If Substring(cStatus,1,1)='L'
			_qry += "	AND C5_XBLQ='L' "+cEOL
		ElseIf Substring(cStatus,1,1)='B'
			_qry += "	AND C5_XBLQ='B' "+cEOL
		End
	End

	_qry += "  GROUP BY C5_XBLQ, C5_NUM, C5_XPEDMIL, C5_XPEDWEB, C5_CLIENTE, C5_EMISSAO, C5_FECENT, "+cEOL
	_qry += "  		A1_NOME, A1_XCOD, E4_CODIGO, E4_DESCRI, C5_XPEDCLI, A3_COD, A3_NOME, C5_TABELA, C5_ORIGEM, "+cEOL
	_qry += "  		C5_XCANALD, C5_POLCOM, C5_XDESCPO, DA0_DESCRI,C5_DESCONT , C5_DESC1, C5_DESC2, C5_DESC3, C5_DESC4, C5_FILIAL, C5_XDESTAB, A1_END, "+cEOL
	_qry += "  		A1_BAIRRO, A1_EST, A1_MUN, C5_TPPED, C5_NOTA, A1_XMICRRE,SC5.R_E_C_N_O_,C5_CLIENT,C5_LOJACLI "+cEOL

	//_qry += "  ORDER BY SC5.R_E_C_N_O_ "+cEOL
	
	_qry += " ) T ORDER BY R_E_C_N_O_ " 
	//_qry := ChangeQuery(_qry)
	MEMOWRITE("HFAP011_QRY.TXT",_qry)
	If Select("TMPSC5") > 0
       DbSelectArea("TMPSC5")
       DbCloseArea()
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSC5",.T.,.T.)
  
	DbSelectArea("TMPSC5")
	DbGoTop()
  
	_tValPed		:= 0
	_tValDesc		:= 0
	_tValTot		:= 0
	_tValPFat		:= 0
	_tValEntreg		:= 0
	_tValPerda		:= 0
	_tValSaldo		:= 0
	_tQtdPed		:= 0
	_tQtdPFat		:= 0
	_tQtdEnt		:= 0
	_tQtdPerda		:= 0
	_tQtdSaldo		:= 0
	_tValCort       := 0
	_tVaDesc        := 0
  
	aColsEx 		:= {}
	IF _vez > 0
		oMSPedido:aCols := {}
	Endif

	If !EOF()
	  
		While !EOF()
	
			aFieldFill := {}
	
			If TMPSC5->QTD_SALDO = 0 .AND. TMPSC5->QTD_PERDA = 0  //'FATURADO / TOTAL'
				Aadd(aFieldFill, oRed)
			ElseIf TMPSC5->QTD_PED <> TMPSC5->QTD_SALDO .AND. TMPSC5->QTD_SALDO > 0 .AND. TMPSC5->QTD_PERDA = 0  //'FATURADO / PARCIAL'
				Aadd(aFieldFill, oOrange)
			ElseIf TMPSC5->QTD_PED <> TMPSC5->QTD_SALDO .AND. TMPSC5->QTD_SALDO > 0 .AND. TMPSC5->QTD_PERDA > 0  //'FATURADO / PARCIAL'
				Aadd(aFieldFill, oOrange)
			ElseIf TMPSC5->QTD_SALDO = 0 .AND. TMPSC5->QTD_PERDA <> 0  //'PERDA'
				Aadd(aFieldFill, oBlack)
			ElseIf TMPSC5->STATUS =='L' .AND. TMPSC5->QTD_SALDO <> 0 .AND. TMPSC5->QTD_PERDA = 0
				Aadd(aFieldFill, oGreen)
			ElseIf TMPSC5->STATUS =='B' .AND. TMPSC5->QTD_SALDO <> 0 .AND. TMPSC5->QTD_PERDA = 0 .OR. TMPSC5->STATUS ==' '
				Aadd(aFieldFill, oBlue)
			ElseIf TMPSC5->STATUS ==' '// .AND. TMPSC5->QTD_SALDO <> 0 .AND. TMPSC5->QTD_PERDA = 0
				Aadd(aFieldFill, oWhite)
			Endif
	
			Aadd(aFieldFill, Stod(TMPSC5->DTEMISSAO))
			Aadd(aFieldFill, Stod(TMPSC5->DTENTREGA))
			Aadd(aFieldFill, TMPSC5->PEDIDO)
			If alltrim(TMPSC5->PEDMIL) <> ""
				Aadd(aFieldFill, TMPSC5->PEDMIL)
			Else
				Aadd(aFieldFill, TMPSC5->PEDRAK)
			Endif

//			Aadd(aFieldFill, SUBSTRING(TMPSC5->NFISCAL,1,LEN(ALLTRIM(TMPSC5->NFISCAL))-1))
			cNfiscal := StrTran(AllTrim(TMPSC5->NFISCAL),'<SC9 text_x0028__x0029_="',"")
			cNfiscal := StrTran(cNfiscal,'-"/>',"-")
			cNfiscal := SUBSTRING(cNfiscal,1,LEN(cNfiscal)-1)
			Aadd(aFieldFill, SUBSTRING(cNfiscal,1,100))

			ddtfat := StrTran(AllTrim(TMPSC5->DTFAT),'<SC6 text_x0028__x0029_="',"")
			ddtfat := StrTran(ddtfat,'-"/>',"-")
			ddtfat := SUBSTRING(ddtfat,1,LEN(ddtfat)-1)
			//Aadd(aFieldFill, STOD(ddtfat)
			Aadd(aFieldFill, SUBSTRING(ddtfat,1,100))
			
			Aadd(aFieldFill, TMPSC5->COD_CLIENTE)
			Aadd(aFieldFill, TMPSC5->NOME_CLIENTE)
			Aadd(aFieldFill, TMPSC5->CLI_MIL)
			Aadd(aFieldFill, TMPSC5->VALPED)
			//Aadd(aFieldFill, TMPSC5->C5_DESCONT + ((TMPSC5->VALPED/100) * TMPSC5->DESCONTO))1
			Aadd(aFieldFill, TMPSC5->DESCONTO)
			Aadd(aFieldFill, TMPSC5->PRECO_TOTAL - TMPSC5->C5_DESCONT)//-((TMPSC5->PRECO_TOTAL/100)*TMPSC5->DESCONTO))
			Aadd(aFieldFill, TMPSC5->QTD_PED)
			Aadd(aFieldFill, TMPSC5->QTD_PREFAT)
			Aadd(aFieldFill, TMPSC5->QTD_ENTREGUE)
			Aadd(aFieldFill, TMPSC5->QTD_PERDA)
			Aadd(aFieldFill, TMPSC5->QTD_SALDO)
			Aadd(aFieldFill, TMPSC5->COND_PAGTO)
			Aadd(aFieldFill, TMPSC5->DESCRI_COND_PAGTO)
			Aadd(aFieldFill, TMPSC5->TPPED)
			Aadd(aFieldFill, Alltrim(POSICIONE("SZ1",1,xFilial("SZ1")+TMPSC5->TPPED,"Z1_DESC")))
			Aadd(aFieldFill, TMPSC5->PEDCLI)
			Aadd(aFieldFill, TMPSC5->ORIGEM)
			Aadd(aFieldFill, TMPSC5->CANAL)
			Aadd(aFieldFill, TMPSC5->TBPRECO)
			Aadd(aFieldFill, TMPSC5->DESCTBPRECO)
			Aadd(aFieldFill, TMPSC5->POLCOM)
			Aadd(aFieldFill, TMPSC5->DESCPOL)
			Aadd(aFieldFill, TMPSC5->FILIAL)
			Aadd(aFieldFill, TMPSC5->COD_REPRES)
			Aadd(aFieldFill, TMPSC5->REPRES)
			Aadd(aFieldFill, TMPSC5->ENDERECO)
			Aadd(aFieldFill, TMPSC5->BAIRRO)
			Aadd(aFieldFill, TMPSC5->CIDADE)
			Aadd(aFieldFill, TMPSC5->ESTADO)
			Aadd(aFieldFill, TMPSC5->VALPREFAT- (TMPSC5->C5_DESCONT + ((TMPSC5->VALPREFAT/100)*TMPSC5->DESCONTO)))
			Aadd(aFieldFill, TMPSC5->VALENTREGUE- (TMPSC5->C5_DESCONT + ((TMPSC5->VALENTREGUE/100)*TMPSC5->DESCONTO)))
			Aadd(aFieldFill, TMPSC5->VALPERDA - (TMPSC5->C5_DESCONT + ((TMPSC5->VALPERDA/100)*TMPSC5->DESCONTO)))
			Aadd(aFieldFill, TMPSC5->VALSALDO - (TMPSC5->C5_DESCONT + ((TMPSC5->VALSALDO/100)*TMPSC5->DESCONTO)))
			Aadd(aFieldFill, .F.)
			Aadd(aColsEx, aFieldFill)
	  		 
			_tValPed		+= TMPSC5->VALPED
			_tValDesc		+= TMPSC5->DESCONTO //TMPSC5->C5_DESCONT + ((TMPSC5->VALPED/100) * TMPSC5->DESCONTO)
			_tValTot		+= TMPSC5->PRECO_TOTAL - TMPSC5->C5_DESCONT
			_tValPFat		+= TMPSC5->VALPREFAT
			//_tVaDesc        += (_tValDesc * 100) /  _tValPed
			_tValEntreg		+= TMPSC5->VALENTREGUE
			_tValPerda		+= TMPSC5->VALPERDA
			_tValSaldo		+= TMPSC5->VALSALDO
			_tQtdPed		+= TMPSC5->QTD_PED
			_tQtdPFat		+= TMPSC5->QTD_PREFAT
			_tQtdEnt		+= TMPSC5->QTD_ENTREGUE
			_tQtdPerda		+= TMPSC5->QTD_PERDA
			_tQtdSaldo		+= TMPSC5->QTD_SALDO
	 
			DbSelectArea("TMPSC5")
			DbSkip()
		End
	  
	Else
  		
		aFieldFill := {}
		
		For nX := 1 to 35   //Len(aFields)
			Aadd(aFieldFill, "")
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
  		
	End

	cTotValPed			:= _tValPed
	cTotValDesc			:= _tValDesc
	cTotQtdPed			:= _tQtdPed
	cTotQtdPFat			:= _tQtdPFat
	cTotQtdEnt			:= _tQtdEnt
	cTotQtdPerda		:= _tQtdPerda
	cTotQtdSaldo		:= _tQtdSaldo

	If cTpValTot == "Pre Fat"
		cTotValTot		:= _tValPFat
	ElseIf cTpValTot == "Perdida"
		cTotValTot		:= _tValPerda
	ElseIf cTpValTot == "Entregue"
		cTotValTot		:= _tValEntreg
	ElseIf cTpValTot == "a Entregar"
		cTotValTot		:= _tValSaldo
	Else
		cTotValTot		:= _tValTot
	End

	TMPSC5->(DbCloseArea())

	If _vez > 0
		oMSPedido:aCols := aColsEx
		oMSPedido:Refresh()
	Endif

Return


Static Function ManPed(_tp)

	Private Inclui    := .F.
	Private Altera    := .T.
	Private nOpca     := 1
	Private cCadastro := "Pedido de Vendas"
	Private aRotina := {}
	        
	DbSelectArea("SC5")
	DbSetOrder(1)
	DbSeek(xfilial("SC5")+oMSPedido:acols[oMSPedido:nAt,4])

	If _tp = 1
		If AllTrim(oMSPedido:acols[oMSPedido:nAt,4]) <> ""
			Altera := .T.
			Inclui := .F.
			MatA410(Nil, Nil, Nil, Nil, "A410Altera")
		Else
			MessageBox("N�o h� Pedidos no para Alterar","Aten��o",16)
		End
	ElseIf _tp = 2
		Altera := .F.
		Inclui := .T.
		MatA410(Nil, Nil, Nil, Nil, "A410Inclui")
	Else
		If AllTrim(oMSPedido:acols[oMSPedido:nAt,4]) <> ""
			Altera := .F.
			Inclui := .F.
			MatA410(Nil, Nil, Nil, Nil, "A410Visual")
		Else
			MessageBox("N�o h� Pedidos para Visualizar","Aten��o",16)
		End
	Endif

	oMSPedido:Refresh()
  	
Return


User Function PolCo3(_tp)

	Local oButton1
	Local oButton2
	Local oGet1
	Local oGet2
	Local oSay1
	Local oSay2
	local cAlias

	Private oFolder1
	Private cGet1	:= Space(6)
	Private cGet2	:= Space(1)
	Private cGetFilial	:= oMSPedido:acols[oMSPedido:nAt,31]
	private lChkSel	:= .F.

	If AllTrim(oMSPedido:acols[oMSPedido:nAt,4]) <> ""

		Static oDlg
	
		DbSelectArea("SZY")
		DbSetOrder(1)
		DbSeek(xfilial("SZY")+__CUSERID)
	
		If 	__CUSERID <> SZY->ZY_CODUSU
			MessageBox("Usu�rio sem acesso a Rotina!","Aten��o",16)
			Return
		End
	
		If _tp <> 1
			cGet1 		:= oMSPedido:acols[oMSPedido:nAt,4]
			cGet2	 	:= oMSPedido:acols[oMSPedido:nAt,2]
		Endif
		DEFINE MSDIALOG oDlg TITLE "Libera��o de Pedidos" FROM 000, 000  TO 500, 900 COLORS 0, 16777215 PIXEL
	
		@ 012, 015 SAY oSay1 PROMPT "Pedido" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 011, 049 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 012, 130 SAY oSay2 PROMPT "Data" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 011, 166 MSGET oGet2 VAR cGet2 SIZE 259, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

		fMSNewGe1(_tp)
		
		@233,015 checkbox oChk var lChkSel PROMPT "Inverte Sele��o" size 60,07 on CLICK seleciona(lChkSel)
		
		If _tp == 2
			@ 233, 388 BUTTON oButton1 PROMPT "Liberar" SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
		ElseIf _tp == 3
			@ 233, 388 BUTTON oButton1 PROMPT "Estornar" SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
		End
		@ 233, 344 BUTTON oButton2 PROMPT "Cancelar" SIZE 037, 012 ACTION oDlg:End()  OF oDlg PIXEL
	
		ACTIVATE MSDIALOG oDlg CENTERED

	Else
		MessageBox("N�o h� Pedidos para o Processo de Libera��o","Aten��o",16)
	End

	oMSPedido:Refresh()
  	
Return


//------------------------------------------------ 
Static Function fMSNewGe1(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"OK","ZZ1_BLQ","ZZ1_DESCBL","ZZ1_DTLIB","ZZ1_USRLIB"}
	Local aAlterFields := {"OK"}
	Private aHeader1 := {}
	Private aCols1 := {}

	Static oMSNewGe1

  	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If aFields[nX] $ "OK"
			Aadd(aHeader1, {aFields[nX], "OK",'@BMP', 2, 0, ,	,"C" ,   , "V",  ,   ,'seleciona','V','S'})
		ElseIf SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

	DbSelectArea("ZZ1")
	DbSetOrder(1)
	DbSeek(cGetFilial+cGet1)	//DbSeek(xfilial("ZZ1")+cGet1)
	//DbSeek(xfilial("ZZ1")+cGet1)

	While !EOF() .and. ZZ1->ZZ1_PEDIDO == cGet1
		Aadd(aCols1,{'',ZZ1->ZZ1_BLQ,ZZ1->ZZ1_DESCBL,ZZ1->ZZ1_DTLIB,ZZ1->ZZ1_USRLIB,.F.})
		DbSkip()
	End
  
	oMSNewGe1 := MsNewGetDados():New( 030, 010, 225, 443, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", , aHeader1, aCols1)

	//quando clicado duas vezes sobre o aCols[oMSNewGe1:nAt,1], ou seja, onde ficar� a coluna com o checkbox, ele ir� alternar de LBOK para LBNO e vice versa
	oMSNewGe1:oBrowse:bLDblClick := {|| oMSNewGe1:EditCell(), oMSNewGe1:aCols[oMSNewGe1:nAt,1] := iif(oMSNewGe1:aCols[oMSNewGe1:nAt,1] == 'LBOK','LBNO','LBOK')}

Return


static function seleciona(lChkSel)

Local i := 0

	for i := 1 to len(oMSNewGe1:aCols)

		If oMSNewGe1:aCOLS[i,1] == 'LBOK'
			oMSNewGe1:aCOLS[i,1] := 'LBNO'
		Else
			oMSNewGe1:aCOLS[i,1] := 'LBOK'
		End
	next

	oMSNewGe1:oBrowse:Refresh()
	oDlg:Refresh()

return


Static Function Save(_tp)

	Local I
	Local cEOL		:= +Chr(13)+Chr(10)

	If _tp == 2

		For i := 1 to Len(OMSNEWGE1:ACOLS)
			
			If OMSNEWGE1:ACOLS[i,1] == "LBOK"
			
				DbSelectArea("ZZ1")
				DbSetOrder(1)
				DbSeek(cGetFilial+cGet1+OMSNEWGE1:ACOLS[i,2])	//DbSeek(xfilial("ZZ1")+cGet1+OMSNEWGE1:ACOLS[i,2])
		
				//"TA"	"T�tulos em Atraso"							//Titulos em Atraso (em Aberto Vecidos)
				If OMSNEWGE1:ACOLS[i,2]="TA" .and. SZY->ZY_TITATRA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB		with 	 Date()
					Replace ZZ1_USRLIB		with	 cUsername
					MsUnLock()
				End

				//"PA"	"Pago em Atrazo"								//Titulos em Atraso (Pagos com Atrazo)
				If OMSNEWGE1:ACOLS[i,2]="PA" .and. SZY->ZY_TITATRP="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB		with	 Date()
					Replace ZZ1_USRLIB		with	 cUsername
					MsUnLock()
				End

				//"CI"	"Cliente Inativo"								//Cliente Inativo
				If OMSNEWGE1:ACOLS[i,2]="CI" .and. SZY->ZY_CLIINAT="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End

				//"CN"	"Cliente Novo"								//Cliente Novo
				If OMSNEWGE1:ACOLS[i,2]="CN" .and. SZY->ZY_CLINOVO="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB		with	 Date()
					Replace ZZ1_USRLIB		with	 cUsername
					MsUnLock()
				End

				//"CP"	"Cliente Incompleto"							//Dados Incompletos
				If OMSNEWGE1:ACOLS[i,2]="CP"
					
					_qry := " SELECT A1_COD, A1_NOME, A1_PESSOA, A1_END, A1_BAIRRO, A1_EST, A1_CEP, A1_MUN, A1_PAIS, "+cEOL
					_qry += " 			A1_CGC, A1_INSCR, A1_EMAIL, A1_XGRUPO, A1_XCANAL, A1_XCODREG "+cEOL
					_qry += " FROM "+RetSqlName("SA1")+" SA1  (NOLOCK) "+cEOL
					_qry += " 	INNER JOIN "+RetSqlName("SC5")+" SC5  (NOLOCK) ON C5_CLIENTE=A1_COD AND C5_LOJAENT=A1_LOJA AND C5_NUM='"+cGet1+"' AND SC5.D_E_L_E_T_= '' "+cEOL
					_qry += " WHERE SA1.D_E_L_E_T_ <> '*' AND A1_FILIAL = '"+xfilial("SA1")+"' "+cEOL
					_qry += " 		AND (A1_NOME='' OR (A1_PESSOA='' OR (A1_PESSOA='J' AND A1_INSCR='')) OR A1_END='' OR A1_BAIRRO='' OR A1_EST='' "+cEOL
					_qry += "				OR A1_CEP='' OR A1_MUN=''OR A1_PAIS='' OR A1_CGC='' OR (A1_EMAIL='' OR A1_EMAIL NOT LIKE '%@%') "+cEOL
					_qry += "				OR A1_XGRUPO='' OR A1_XCANAL='' OR A1_XCODREG='') "+cEOL
			
					If Select("TMPSA1") > 0
						TMPSA1->(DbCloseArea())
					EndIf
	
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSA1",.T.,.T.)
					DbSelectArea("TMPSA1")
					DbGoTop()
				
					If !EOF()
						If SZY->ZY_DINCOMP="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
							RecLock("ZZ1",.F.)
							Replace ZZ1_DTLIB	with	 Date()
							Replace ZZ1_USRLIB	with	 cUsername
							MsUnLock()
						End
					Else
						RecLock("ZZ1",.F.)
						Replace ZZ1_DTLIB	with	 Date()
						Replace ZZ1_USRLIB	with	 "CORRIGIDO"
						MsUnLock()
					End
					
				End

				//"OB"	"Observacao Cliente"							//Observa��o do Pedido
				If OMSNEWGE1:ACOLS[i,2]="OB" .and. SZY->ZY_OSBPED="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End
		
				//"CH"	"Cheque Devolvido"							//Cheque Devolvido
				If OMSNEWGE1:ACOLS[i,2]="CH" .and. SZY->ZY_CHEDEV="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End

				//"PI"	"Produto Inativo"								//Produto inativo
				If OMSNEWGE1:ACOLS[i,2]="PI" .and. SZY->ZY_PRODINA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with	 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End

				//"LM"	"Limite Excedido"								//Limite Ultrapassado
				If OMSNEWGE1:ACOLS[i,2] $ "LM|QL" .and. SZY->ZY_LMULTRA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End

				//"LM"	"Limite Excedido"								//Limite Ultrapassado
				If OMSNEWGE1:ACOLS[i,2] $ "LM|QL" .and. SZY->ZY_LMULTRA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with	 Date()
					Replace ZZ1_USRLIB	with	 cUsername
					MsUnLock()
				End
		
				//"ND"	"Notas de Debito"								//Nota Debito
				If OMSNEWGE1:ACOLS[i,2]="ND" .and. SZY->ZY_NOTADEB="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End
		
				//"PR"	"T�tulos Protesto"							//Titulos em Protesto
				If OMSNEWGE1:ACOLS[i,2]="PR" .and. SZY->ZY_TITPROT="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End
		
				//"PE"	"Perdas (Conta a Receber)"					//Conta Perdas
				If OMSNEWGE1:ACOLS[i,2]="PE" .and. SZY->ZY_CTPERDA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End
	
				//"VP"	"Valor Minimo Pedido"						//Valor do Pedido
				If OMSNEWGE1:ACOLS[i,2]="VP" .and. SZY->ZY_VALPED="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End
		
				//"BC"	"Bloqueio por Cliente"						//Cliente
				If OMSNEWGE1:ACOLS[i,2]="BC" .and. SZY->ZY_CLIENTE="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End
							
				//"PG"	"Bloqueio por Tabela Cond. Pagamento"		//Condi��o de Pagamento
				If OMSNEWGE1:ACOLS[i,2]="PG" .and. SZY->ZY_CONDPAG="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End
	
				//"PC"	"Bloqueio por Tabela de Pre�os"				//SZY-> 	//Tabela de Pre�o
				If OMSNEWGE1:ACOLS[i,2]="PC" .and. SZY->ZY_TBPRECO="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End
		
				//"PD"	"Bloqueio por Tipo de Pedido"				//SZZ
				If OMSNEWGE1:ACOLS[i,2]="PD" .and. AllTrim(OMSNEWGE1:ACOLS[i,5])=""
					DbSelectArea("SZZ")
					DbSetOrder(1)
					DbSeek(cGetFilial+__CUSERID)		//DbSeek(xfilial("ZZ1")+__CUSERID)
				
					_qry := " SELECT COUNT(C5_TPPED) AS CONT FROM "+RetSqlName("ZZ1")+" ZZ1  (NOLOCK) "+cEOL
					_qry += " 		INNER JOIN "+RetSqlName("SC5")+" SC5  (NOLOCK) ON SC5.D_E_L_E_T_= '' AND C5_NUM=ZZ1_PEDIDO "+cEOL
					_qry += " 		INNER JOIN "+RetSqlName("SZZ")+" SZZ  (NOLOCK) ON SZZ.D_E_L_E_T_= '' AND ZZ_USUARIO='"+__CUSERID+"' AND ZZ_TPPED=C5_TPPED "+cEOL
					_qry += " WHERE ZZ1.D_E_L_E_T_= '' "+cEOL
					_qry += " 		AND ZZ1_PEDIDO='"+ZZ1->ZZ1_PEDIDO+"' "+cEOL
					_qry += " 	AND ZZ1_BLQ='PD' "+cEOL
		
					If Select("TMPZZ1") > 0
						TMPZZ1->(DbCloseArea())
					EndIf
		
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPZZ1",.T.,.T.)
					DbSelectArea("TMPZZ1")
					DbGoTop()
					If TMPZZ1->CONT > 0
						RecLock("ZZ1",.F.)
						Replace ZZ1_DTLIB	with		 Date()
						Replace ZZ1_USRLIB	with		 cUsername
						MsUnLock()
					End
				
					TMPZZ1->(DbCloseArea())
	
				End

				//"BB"	"Bloqueio do Canal B2B
				If OMSNEWGE1:ACOLS[i,2]="BB" .and. SZY->ZY_PEDB2B="S"
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB	with		 Date()
					Replace ZZ1_USRLIB	with		 cUsername
					MsUnLock()
				End
	
				//"LA"	"Libera��o Automatica (se por algum motivo o pedido liberado automatico estiver bloqueado ele refaz a libera��o)
				If OMSNEWGE1:ACOLS[i,2]="LA"
					RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB		with	 Date()
					Replace ZZ1_USRLIB		with	 cUsername
					MsUnLock()
				End
			End
		Next

		DbSelectArea("ZZ1")
		DbSetOrder(1)
		DbSeek(cGetFilial+cGet1)				//DbSeek(xfilial("ZZ1")+cGet1)

		_blq := "L"
	
		While !EOF() .and. ZZ1->ZZ1_PEDIDO = cGet1 .and. _blq = "L"
			If alltrim(ZZ1->ZZ1_USRLIB) = ""
				_blq := "B"
			Endif
			DbSelectArea("ZZ1")
			Dbskip()
		End

		If _blq = "L"
			DbSelectArea("ZZ3")
			DbSetOrder(1)
			If DbSeek(cGetFilial+cGet1)			//DbSeek(xfilial("ZZ3")+cGet1)
				RecLock("ZZ3",.F.)
				Replace ZZ3_STATUS  with _blq
				MsUnLock()
			Endif
			
			DbSelectArea("SC5")
			DbSetOrder(1)
			If DbSeek(cGetFilial+cGet1)			//DbSeek(xfilial("SC5")+cGet1)
				RecLock("SC5",.F.)
				Replace C5_XBLQ with  _blq
				MsUnLock()
			Endif

			DbSelectArea("SC9")
			DbSetOrder(1)
			If DbSeek(cGetFilial+cGet1)			//DbSeek(xfilial("SC9")+cGet1)
			
				While !EOF() .and. SC9->C9_PEDIDO = cGet1
					RecLock("SC9",.F.)
					Replace C9_XBLQ with  _blq
					MsUnLock()
				
					DbSelectArea("SC9")
					DbSkip()
				End
			Endif
		Endif

	ElseIf _tp == 3
	
		_qry := " SELECT * FROM "+RetSqlName("SZJ")+"  (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZJ_PEDIDO = '"+cValToChar(cGet1)+"' "
	
		If Select("TMPSZJ") > 0
			TMPSZJ->(DbCloseArea())
		EndIf
	
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZJ",.T.,.T.)
		DbSelectArea("TMPSZJ")
		DbGoTop()
	
		If EOF()
			For i := 1 to Len(OMSNEWGE1:ACOLS)
				If OMSNEWGE1:ACOLS[i,1] == "LBOK"
					DbSelectArea("ZZ1")
					DbSetOrder(1)
					DbSeek(cGetFilial+cGet1+OMSNEWGE1:ACOLS[i,2])	//DbSeek(xfilial("ZZ1")+cGet1+OMSNEWGE1:ACOLS[i,2])
		
					If OMSNEWGE1:ACOLS[i,5]=SUBSTRING(cUsername,1,10)
						RecLock("ZZ1",.F.)
						Replace ZZ1_DTLIB with	 ctod("  /  /  ")
						Replace ZZ1_USRLIB with	 ""
						MsUnLock()
					End
				End
			Next
	
			DbSelectArea("ZZ3")
			DbSetOrder(1)
			DbSeek(cGetFilial+cGet1)		//DbSeek(xfilial("ZZ3")+cGet1)
			
			RecLock("ZZ3",.F.)
			Replace ZZ3_STATUS with "B"
			MsUnLock()
			
			DbSelectArea("SC5")
			DbSetOrder(1)
			DbSeek(cGetFilial+cGet1)		//DbSeek(xfilial("SC5")+cGet1)
			
			RecLock("SC5",.F.)
			Replace C5_XBLQ with "B"
			MsUnLock()
		Else
			MessageBox("Pedido com Libera��o do DAP. N�o poder� ser estornado!","Aten��o",16)
		Endif
		DbClosearea()
	Endif

	oMSPedido:Refresh()

Return


Static Function RpExcell(TpRel)
	
	Local _w
	Local _cArq			:= ""
	Local _nHdl			:= 0
	Local _cTempPath	:= Alltrim(GetTempPath())
	Local _cDirDocs		:= MsDocPath()
	Local _style		:= ""
	Local _cCabec		:= ""
	Local _cHtml		:= ""
	Local _cRodape		:= ""
	Local nx 			:= 0

	_style := "<html>"
	_style += "<meta http-equiv='Content-Type' content='application/vnd.ms-excel;charset=iso-8859-1'>"
	_style += "<style type='text/css'>"
	_style += "td.Cabec1{ "
	_style += 		"font-size: 12px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"text-align: Center;"
	_style += 		"vertical-align: middle; "
	_style +=  		"border-style: dotted; "
	_style +=  		"border-color: gray; "
	_style += 		"color: '#000066'; "
	_style += "} "
	_style += "td.tabela1{ "
	_style += 		"text-align: center; "
	_style += 		"font-size: 10px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"border-width: 1px; "
	_style += 		"vertical-align: middle; "
	_style += 		"padding: 0px; "
	_style += 		"border-style: dotted; "
	_style += 		"border-color: gray; "
	_style += 		"-moz-border-radius: ; "
	_style += "} "
	_style += "td.tabela2{ "
	_style += 		"font-size: 10px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"border-width: 1px; "
	_style += 		"vertical-align: middle; "
	_style += 		"padding: 0px; "
	_style += 		"border-style: dotted; "
	_style += 		"border-color: gray; "
	_style += 		"-moz-border-radius: ; "
	_style += "} "
	_style += "td.codigo{ "
	_style += 		'mso-number-format:"\@"; '
	_style += 		"text-align: center; "
	_style +=  		"font-size: 10px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"border-width: 1px; "
	_style += 		"vertical-align: middle;"
	_style +=  		"padding: 0px; "
	_style +=  		"border-style: dotted; "
	_style +=  		"border-color: gray; "
	_style +=  		"-moz-border-radius: ; "
	_style += "} "
	_style += " td.valor{"
	_style += 		'mso-number-format:"\#\,\#\#0\.00"; '
	_style += 		"text-align: right; "
	_style += 		"font-size: 10px; "
	_style += 		"font-family: verdana,tahoma,arial,sans-serif; "
	_style += 		"border-width: 1px; "
	_style += 		"vertical-align: middle;"
	_style += 		"padding: 0px; "
	_style += 		"border-style: dotted; "
	_style += 		"border-color: gray; "
	_style += 		"-moz-border-radius: ; "
	_style += "} "
	_style += "</style>"

	If TpRel == 1
		_cCabec := "<body>"
		_cCabec += "<table>"
		_cCabec += "<tr><td class=Cabec1 Colspan='"+AllTrim(Str(len(oMSPedido:aHeader)))+"'><b>" +SM0->(UPPER(AllTrim(M0_NOME))) + " / " + SM0->(UPPER(AllTrim(M0_FILIAL)))+ "</b></td></tr>"
		_cCabec += "<tr><td class=Cabec1 Colspan='"+AllTrim(Str(len(oMSPedido:aHeader)))+"'><b>RELATORIO DE PEDIDOS DE VENDA</b></td></tr>"
	
		_cCabec += "<tr>"
		For _w := 1 to len(oMSPedido:aHeader)
			_cCabec += "<td class=tabela1><b>"+oMSPedido:aHeader[_w][1]+"</b></td>"
		Next
		_cCabec += "</tr>"
	
		_cArq := CriaTrab(,.f.)
		_nHdl := nHdl := FCreate(_cDirDocs + "\" + _cArq + ".xls")
	
		fWrite(_nHdl,_style,Len(_style))
		fWrite(_nHdl,_cCabec,Len(_cCabec))
	
		For nX := 1 to len(oMSPedido:ACOLS)
	
			_cHtml := "<tr>"
			For _w := 1 to len(oMSPedido:aHeader)
				If _w = 1
					If oMSPedido:ACOLS[nX][_w]:CNAME = "BR_AZUL"
						_cHtml += "<td class=tabela1>Pedido Bloqueado</td>"
					ElseIf oMSPedido:ACOLS[nX][_w]:CNAME = "BR_VERDE"
						_cHtml += "<td class=tabela1>Pedido Liberado</td>"
					ElseIf oMSPedido:ACOLS[nX][_w]:CNAME = "BR_LARANJA"
						_cHtml += "<td class=tabela1>Pedido Faturado / Parcial</td>"
					ElseIf oMSPedido:ACOLS[nX][_w]:CNAME = "BR_VERMELHO"
						_cHtml += "<td class=tabela1>Pedido Faturado / Total</td>"
					ElseIf oMSPedido:ACOLS[nX][_w]:CNAME = "BR_PRETO"
						_cHtml += "<td class=tabela1>Pedido com Perda</td>"
					ElseIf oMSPedido:ACOLS[nX][_w]:CNAME = "BR_WHITE"
						_cHtml += "<td class=tabela1>Pedido com erro</td>"
					End
				Else
					If Alltrim(strzero(_w,2)) $ "04/05/07/09/20/22/24/25/27/29/31/32"
						_cHtml += "<td class=codigo>"+oMSPedido:ACOLS[nX][_w]+"</td>"
					ElseIf oMSPedido:aHeader[_w][8]=="C"
						If SubString(oMSPedido:ACOLS[nX][_w],1,1)=="0"
							_cHtml += "<td class=codigo>"+oMSPedido:ACOLS[nX][_w]+"</td>"
						Else
							_cHtml += "<td class=tabela2>"+oMSPedido:ACOLS[nX][_w]+"</td>"
						End
					ElseIf oMSPedido:aHeader[_w][8]=="D"
						_cHtml += "<td class=tabela2>"+DToC(oMSPedido:ACOLS[nX][_w])+"</td>"
					ElseIf oMSPedido:aHeader[_w][8]=="N"
						_cHtml += "<td class=valor>"+AllTrim(TRANSFORM(oMSPedido:ACOLS[nX][_w], "@E 999,999,999.99"))+"</td>"
					Else
						_cHtml += "<td class=tabela2>"+oMSPedido:ACOLS[nX][_w]+"</td>"
					End
				End
			Next
			_cHtml += "</tr>"
		
			fWrite(_nHdl,_cHtml,Len(_cHtml))
			
		Next
	
	ElseIf TpRel == 2
	
		_cCabec := "<body>"
		_cCabec += "<table>"
		_cCabec += "<tr><td class=Cabec1 Colspan='"+AllTrim(Str(len(oMSNewGe2:aHeader)))+"'><b>" + UPPER(AllTrim(SM0->M0_NOME)) + " / " + UPPER(AllTrim(SM0->M0_FILIAL))+ "</b></td></tr>"
		_cCabec += "<tr><td class=Cabec1 Colspan='"+AllTrim(Str(len(oMSNewGe2:aHeader)))+"'><b>RELATORIO DE SALDOS DE PEDIDOS DE VENDA</b></td></tr>"
		_cCabec += "<tr><td class=Cabec1 Colspan='"+AllTrim(Str(len(oMSNewGe2:aHeader)))+"'><b>PC: "+oMSPedido:acols[oMSPedido:nAt,4]+" - CLIENTE: "+oMSPedido:acols[oMSPedido:nAt,7]+" - "+AllTrim(oMSPedido:acols[oMSPedido:nAt,8])+"</b></td></tr>"
	
		_cCabec += "<tr>"
		For _w := 1 to len(oMSNewGe2:aHeader)
			_cCabec += "<td class=tabela1><b>"+oMSNewGe2:aHeader[_w][1]+"</b></td>"
		Next
		_cCabec += "</tr>"
	
		_cArq := CriaTrab(,.f.)
		_nHdl := nHdl := FCreate(_cDirDocs + "\" + _cArq + ".xls")
	
		fWrite(_nHdl,_style,Len(_style))
		fWrite(_nHdl,_cCabec,Len(_cCabec))
	
		For nX := 1 to len(oMSNewGe2:ACOLS)
	
			_cHtml := "<tr>"
			For _w := 1 to len(oMSNewGe2:aHeader)
				If oMSNewGe2:aHeader[_w][8]=="C"
					If SubString(oMSNewGe2:ACOLS[nX][_w],1,1)$ "1|2|3|4|5|6|7|8|9|0"
						_cHtml += "<td class=codigo>"+oMSNewGe2:ACOLS[nX][_w]+"</td>"
					Else
						_cHtml += "<td class=tabela2>"+oMSNewGe2:ACOLS[nX][_w]+"</td>"
					End
				ElseIf oMSNewGe2:aHeader[_w][8]=="D"
					_cHtml += "<td class=tabela2>"+DToC(oMSNewGe2:ACOLS[nX][_w])+"</td>"
				ElseIf oMSNewGe2:aHeader[_w][8]=="N"
					If _w == 8
						_cHtml += "<td class=valor>"+AllTrim(TRANSFORM(oMSNewGe2:ACOLS[nX][_w], "@E 9,999,999,999.99"))+"</td>"
					Else
						_cHtml += "<td class=valor>"+AllTrim(TRANSFORM(oMSNewGe2:ACOLS[nX][_w], "@E 999,999,999.9999"))+"</td>"
					End
				Else
					_cHtml += "<td class=tabela2>"+oMSNewGe2:ACOLS[nX][_w]+"</td>"
				End
			Next
			_cHtml += "</tr>"
		
			fWrite(_nHdl,_cHtml,Len(_cHtml))
			
		Next
		
		_cRodape += "<tr>"
		_cRodape += "<td class=Cabec1 Colspan='2'><b>TOTAL</b></td>"
		_cRodape += "<td class=valor><b>"+AllTrim(TRANSFORM(_txQtdPed, "@E 999,999,999.9999"))+"</b></td>"
		_cRodape += "<td class=valor><b>"+AllTrim(TRANSFORM(_txQtdPFat, "@E 999,999,999.9999"))+"</b></td>"
		_cRodape += "<td class=valor><b>"+AllTrim(TRANSFORM(_txQtdEnt, "@E 999,999,999.9999"))+"</b></td>"
		_cRodape += "<td class=valor><b>"+AllTrim(TRANSFORM(_txQtdPerda, "@E 999,999,999.9999"))+"</b></td>"
		_cRodape += "<td class=valor><b>"+AllTrim(TRANSFORM(_txQtdSaldo, "@E 999,999,999.9999"))+"</b></td>"
		_cRodape += "<td class=valor><b>"+AllTrim(TRANSFORM(_txValSaldo, "@E 9,999,999,999.99"))+"</b></td>"
		_cRodape += "</tr>"
		
		fWrite(_nHdl,_cRodape,Len(_cRodape))

	End
		
	FClose(_nHdl)

	If !Empty(_cArq) .and. _cHtml !=""
		CpyS2T(_cDirDocs + "\" + _cArq + ".xls", _cTempPath, .T.)
		Ferase(_cDirDocs + "\" + _cArq + ".xls")
		ShellExecute("open",_cTempPath + "\" + _cArq + ".xls","","",SW_SHOW)
	Else
		MessageBox("N�o existem registros para gerar o arqquivo!","Aten��o",16)
	EndIf

Return


Static Function Legenda()

	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5

	Static oDlg

	DEFINE MSDIALOG oDlg TITLE "Legenda" FROM 000, 000  TO 210, 400 COLORS 0, 16777215 PIXEL

	@ 015, 040 BITMAP oBlue RESNAME "BR_AZUL" 					SIZE 16,16 NOBORDER PIXEL
	@ 015, 053 SAY oSay1 PROMPT "Pedido Bloqueado" 				SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL

	@ 030, 040 BITMAP oGreen RESNAME "BR_VERDE" 					SIZE 16,16 NOBORDER PIXEL
	@ 030, 053 SAY oSay2 PROMPT "Pedido Liberado" 				SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
    
	@ 045, 040 BITMAP oOrange RESNAME "BR_LARANJA" 				SIZE 16,16 NOBORDER PIXEL
	@ 045, 053 SAY oSay3 PROMPT "Pedido Faturado / Parcial" 	SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
    
	@ 060, 040 BITMAP oRed RESNAME "BR_VERMELHO" 					SIZE 16,16 NOBORDER PIXEL
	@ 060, 053 SAY oSay4 PROMPT "Pedido Faturado / Total" 		SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
    
	@ 075, 040 BITMAP oBlack RESNAME "BR_PRETO"	 				SIZE 16,16 NOBORDER PIXEL
	@ 075, 053 SAY oSay5 PROMPT "Pedido com Perda" 				SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL

	@ 090, 040 BITMAP oWhite RESNAME "BR_WHITE"	 				SIZE 16,16 NOBORDER PIXEL
	@ 090, 053 SAY oSay5 PROMPT "Pedido com erro" 				SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL

	ACTIVATE MSDIALOG oDlg CENTERED

Return


User Function PegaDesc(_cli,_lj,_desc2)

	_area := GetArea()
	_desc := _desc2

	DbSelectArea("SA1")
	DbSetOrder(1)
	DbSeek(xfilial("SA1")+_cli+_lj)

	If SA1->A1_XTPDE01 = "N"
		_desc += SA1->A1_XDESC01*(1-(_desc/100))
	Endif
	If SA1->A1_XTPDE02 = "N"
		_desc += SA1->A1_XDESC02*(1-(_desc/100))
	Endif
	If SA1->A1_XTPDE03 = "N"
		_desc += SA1->A1_XDESC03*(1-(_desc/100))
	Endif
	If SA1->A1_XTPDE04 = "N"
		_desc += SA1->A1_XDESC04*(1-(_desc/100))
	Endif
	If SA1->A1_XTPDE05 = "N"
		_desc += SA1->A1_XDESC05*(1-(_desc/100))
	Endif
	If SA1->A1_XTPDE06 = "N"
		_desc += SA1->A1_XDESC06*(1-(_desc/100))
	Endif
	If SA1->A1_XTPDE07 = "N"
		_desc += SA1->A1_XDESC07*(1-(_desc/100))
	Endif
	If SA1->A1_XTPDE08 = "N"
		_desc += SA1->A1_XDESC08*(1-(_desc/100))
	Endif
	If SA1->A1_XTPDE09 = "N"
		_desc += SA1->A1_XDESC09*(1-(_desc/100))
	Endif
	If SA1->A1_XTPDE10 = "N"
		_desc += SA1->A1_XDESC10*(1-(_desc/100))
	Endif

	RestArea(_area)
Return(_desc)


Static Function HCOPNF()
	IF Pergunte("COPNF",.T.)
		DbSelectArea( "SF2" )
		DbSetOrder(1)
		If DbSeek( xFilial("SF2") + MV_PAR01+MV_PAR02)
			cNroPed := GetSX8Num("SC5","C5_NUM",,1)
			DbSelectArea("SC5")
			DbSetOrder(1)
			While DbSeek(xfilial("SC5")+cNroPed)
				ConfirmSX8()
				cNroPed := SOMA1(cNroPed)
			End

			DbSelectArea("SD2")
			DbSetOrder(3)
			DbSeek(xFilial("SD2")+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA)

			DbSelectArea("SC5")
			DbSetOrder(1)
			DbSeek(xfilial("SC5")+SD2->D2_PEDIDO)
			
			aCabec := {}
			aadd(aCabec,{"C5_NUM"    ,cNroPed,Nil})
			aadd(aCabec,{"C5_TIPO"   ,"N",Nil})
			aadd(aCabec,{"C5_CLIENTE",SF2->F2_CLIENTE,Nil})
			aadd(aCabec,{"C5_LOJACLI",SF2->F2_LOJA,Nil})
			aadd(aCabec,{"C5_CLIENT" ,SF2->F2_CLIENTE,Nil})
			aadd(aCabec,{"C5_LOJAENT",SF2->F2_LOJA,Nil})
			aadd(aCabec,{"C5_EMISSAO",DDATABASE,Nil})
			aadd(aCabec,{"C5_POLCOM" ,SC5->C5_POLCOM,Nil})
			aadd(aCabec,{"C5_TPPED"  ,SC5->C5_TPPED,Nil})
			aadd(aCabec,{"C5_TABELA" ,SC5->C5_TABELA,Nil})
			aadd(aCabec,{"C5_CONDPAG",SC5->C5_CONDPAG,Nil})
			aadd(aCabec,{"C5_NATUREZ",SC5->C5_NATUREZ,Nil})
			aadd(aCabec,{"C5_XPEDRAK",SC5->C5_XPEDRAK,Nil})
			aadd(aCabec,{"C5_DESC1"  ,SC5->C5_DESC1,Nil})	//aadd(aCabec,{"C5_DESC1"  ,TMPPED->DESCONTO,Nil})
						
			/*
			aadd(aCabec,{"C5_XOBSINT",SC5->C5_XOBSINT,Nil})
			aadd(aCabec,{"C5_XCANAL" ,SC5->C5_XCANAL,Nil})
			aadd(aCabec,{"C5_XCANALD",SC5->C5_XCANALD,Nil})
			aadd(aCabec,{"C5_XCODREG",SC5->C5_XCODREG,Nil})
			aadd(aCabec,{"C5_XDESREG",SC5->C5_XDESREG,Nil})
			aadd(aCabec,{"C5_XMICRRE",SC5->C5_XMICRRE,Nil})
			aadd(aCabec,{"C5_XMICRDE",SC5->C5_XMICRDE,Nil})
			aadd(aCabec,{"C5_PRAZO"  ,SC5->C5_PRAZO,Nil})
			aadd(aCabec,{"C5_VEND1"  ,SC5->C5_VEND1,Nil})
			aadd(aCabec,{"C5_COMIS1" ,SC5->C5_COMIS1,Nil})
			aadd(aCabec,{"C5_VEND2"  ,SC5->C5_VEND2,Nil})
			aadd(aCabec,{"C5_COMIS2" ,SC5->C5_COMIS2,Nil})
			aadd(aCabec,{"C5_VEND3"  ,SC5->C5_VEND3,Nil})
			aadd(aCabec,{"C5_COMIS3" ,SC5->C5_COMIS2,Nil})	
			aadd(aCabec,{"C5_FECENT" ,SC5->C5_FECENT,Nil})
			aadd(aCabec,{"C5_ORIGEM" ,'COPIANF',Nil}) 
			aadd(aCabec,{"C5_XCDINST",SC5->C5_XCDINST,Nil})
			aadd(aCabec,{"C5_XINSTRU",SC5->C5_XINSTRU,Nil})
*/
			DbSelectArea("SD2")
			aItens := {}
			cItem := "01"
			While SD2->(!EOF()) .And. SD2->D2_DOC+SD2->D2_SERIE = SF2->F2_DOC+SF2->F2_SERIE
				aLinha := {}
				aadd(aLinha,{"C6_ITEM",cItem,Nil})
				aadd(aLinha,{"C6_PRODUTO",SD2->D2_COD,Nil})
				aadd(aLinha,{"C6_QTDVEN",SD2->D2_QUANT,Nil})
				aadd(aLinha,{"C6_PRCVEN",SD2->D2_PRCVEN,Nil})			//aadd(aLinha,{"C6_PRCVEN",Round(TMPITEM->PRECO_VENDA*(1-(TMPPED->DESCONTO/100)),6),Nil})
				aadd(aLinha,{"C6_PRUNIT",SD2->D2_PRCVEN,Nil})
				aadd(aLinha,{"C6_TES",SD2->D2_TES,Nil})
				aadd(aLinha,{"C6_LOCAL",SD2->D2_LOCAL,Nil})
				aadd(aLinha,{"C6_VALOR",SD2->D2_TOTAL,Nil})
				aadd(aLinha,{"C6_COMIS1",SD2->D2_COMIS1,NIL})
				aadd(aLinha,{"C6_COMIS2",SD2->D2_COMIS2,NIL})
				aadd(aLinha,{"C6_COMIS3",SD2->D2_COMIS3,NIL})
				aadd(aLinha,{"C6_COMIS4",SD2->D2_COMIS4,NIL})
				aadd(aLinha,{"C6_COMIS5",SD2->D2_COMIS5,NIL})
				//		aadd(aLinha,{"C6_GRADE","S",Nil})
				//		aadd(aLinha,{"C6_ITEMGRD",strzero(val(cItem),3),Nil})

				aadd(aItens,aLinha)
				cItem := SOMA1(cItem)

				DbSelectArea("SD2")
				DbSkip()
			End

			lMsErroAuto := .F.

			If Len(aCabec) > 0 .and. Len(aItens) > 0
				MATA410(aCabec,aItens,3)

				If lMsErroAuto
					Mostraerro()
				Else
					MsgInfo("Pedido "+cNroPed+" inclu�do com sucesso!","Pedido Inclu�do")
					ConfirmSX8()
				EndIf
			Endif
		Endif
	Endif

	oMSPedido:Refresh()

Return


Static Function ConsCli(cPAr)
	Local aArea    := GetArea()
	Local cCliente := oMSPedido:acols[oMSPedido:nAt,8]
	U_CONSCLIF("",cCLiente,cPAr)
	RestArea(aArea)
Return

/*
User Function _PolCo3(_tp)
	Local aArea    := GetArea()
	Local oButton1
	Local oButton2
	Local oGet1
	Local oGet2
	Local oSay1
	Local oSay2
	local cAlias
	MsgStop("Pedido = "+oMSPedido:acols[oMSPedido:nAt,4])
	Private oFolder1
	Private cGet1 := Space(6)
	Private cGet2 := Space(1)

	If AllTrim(oMSPedido:acols[oMSPedido:nAt,4]) <> ""

		Static oDlg
	
		DbSelectArea("SZY")
		DbSetOrder(1)
		DbSeek(xfilial("SZY")+__CUSERID)
	
		If 	__CUSERID <> SZY->ZY_CODUSU
			Alert ("Usu�rio sem acesso a Rotina!")
			Return
		End
	
		If _tp <> 1
			cGet1 := oMSPedido:acols[oMSPedido:nAt,4]
			cGet2 := oMSPedido:acols[oMSPedido:nAt,2]
		Endif
		DEFINE MSDIALOG oDlg TITLE "Libera��o de Pedidos" FROM 000, 000  TO 500, 900 COLORS 0, 16777215 PIXEL
	
		@ 012, 015 SAY oSay1 PROMPT "Pedido" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 011, 049 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 012, 130 SAY oSay2 PROMPT "Data" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 011, 166 MSGET oGet2 VAR cGet2 SIZE 259, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		fMSNewGe1(_tp)
		If _tp == 2
			@ 233, 388 BUTTON oButton1 PROMPT "Liberar" SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
		ElseIf _tp == 3
			@ 233, 388 BUTTON oButton1 PROMPT "Estornar" SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
		End
		@ 233, 344 BUTTON oButton2 PROMPT "Cancelar" SIZE 037, 012 ACTION oDlg:End()  OF oDlg PIXEL
	
		ACTIVATE MSDIALOG oDlg CENTERED

	Else
		Alert ("N�o h� Pedidos para o Processo de Libera��o")
	End

	oMSPedido:Refresh()
	RestArea(aArea)
Return
*/

User Function HFAT01(_data)
	local i
	Local _i
	Local nPosPrd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_ENTREG'})
	Local aAreaSF4:= SF4->(GetArea())


	For _i := 1 to len(aCols)
		aCols[_i,nPosPrd] := _data
	Next

	RestArea(aAreaSF4)

Return(_data)


User Function HCCANCP()

	_qry := "Select * from "+RetSqlName("SZJ")+" (NOLOCK) where D_E_L_E_T_ = '' and ZJ_PEDIDO = '"+oMSPedido:acols[oMSPedido:nAt,4]+"' and ZJ_DOC = '' "

	If Select("TMPSZJ") > 0
		TMPSZJ->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZJ",.T.,.T.)
	DbSelectArea("TMPSZJ")
	DbGoTop()

	If !EOF()
		MessageBox("Pedido "+oMSPedido:acols[oMSPedido:nAt,4]+" possui DAP e n�o pode ter o res�duo eliminado!","Aten��o",48)
	Else

		If aColsEx[oMSPedido:nat,18] == 0	//TMPSC5->QTD_PERDA

			cPedido := oMSPedido:acols[oMSPedido:nAt,4]

			If ( Pergunte("MTA500",.T.) )

				If Motivo(cPedido)

					cQry := " UPDATE SC6010 SET C6_BLQ = 'R' "
					cQry += " FROM SC6010 SC6 (NOLOCK) , SC5010 SC5 (NOLOCK) "
					cQry += " WHERE SC6.D_E_L_E_T_ = ' ' AND "
					cQry += " SC5.C5_FILIAL='"+XFILIAL("SC5")+"' AND "
					cQry += " SC5.C5_NUM=SC6.C6_NUM AND "
					cQry += " SC5.C5_EMISSAO>='"+DTOS(MV_PAR02)+"' AND "
					cQry += " SC5.C5_EMISSAO<='"+DTOS(MV_PAR03)+"' AND "
					cQry += " SC5.D_E_L_E_T_ = ' ' AND "
					cQry += " ((SC6.C6_QTDVEN=0 AND SC5.C5_NOTA<>'         ') OR "
					cQry += " (100-((SC6.C6_QTDENT+SC6.C6_QTDEMP)/SC6.C6_QTDVEN*100)<="+Str(MV_PAR01,6,2)+")) "
					cQry += " AND C6_NUM = '"+MV_PAR04+"' "

					TCSQLEXEC(cQry)

					MsgInfo("Residuo Eliminado","HOPE")
					ElimRes(cPedido)

				Else

					MessageBox("Residuo n�o eliminado!","HOPE",48)

				endif

				//Ma410Resid(SC5,138432,4)

			ENDIF

			//MATA500()
		Else
			DbSelectArea("SC6")
			DbSetOrder(1)
			DbSeek(xfilial("SC6")+oMSPedido:acols[oMSPedido:nAt,4])

			While !EOF() .and. SC6->C6_NUM = oMSPedido:acols[oMSPedido:nAt,4]

				IF SC6->C6_BLQ = "R"

					RecLock("SC6",.F.)
					SC6->C6_BLQ := ""
					MsUnLock()

					DbSelectArea("SF4")
					DbSetOrder(1)
					DbSeek(xfilial("SF4")+SC6->C6_TES)

					If ( SF4->F4_ESTOQUE=="S" )
						dbSelectArea("SB2")
						dbSetOrder(1)
						MsSeek(xFilial("SB2")+SC6->C6_PRODUTO+SC6->C6_LOCAL)

						_qry := " UPDATE SB2010 SET B2_QPEDVEN = "+cValToChar(Max(SC6->C6_QTDVEN-SC6->C6_QTDEMP-SC6->C6_QTDENT,0))+" WHERE B2_LOCAL = '"+SC6->C6_LOCAL+"'  AND B2_COD = '"+SC6->C6_PRODUTO+"' AND D_E_L_E_T_ = '' "
						TcSqlExec(_qry)


						//RecLock("SB2",.F.)
						//SB2->B2_QPEDVEN += Max(SC6->C6_QTDVEN-SC6->C6_QTDEMP-SC6->C6_QTDENT,0)
						//MsUnLock()
					Endif
				Endif

				//SF4->(DbCloseArea())

				DbselectArea("SC6")
				SC6->(DbSkip())

			EndDo

			DbSelectArea("SC5")
			DbSetOrder(1)
			DbSeek(xfilial("SC5")+oMSPedido:acols[oMSPedido:nAt,4])

			RecLock("SC5",.F.)
			SC5->C5_NOTA := ""
			SC5->C5_XMOTRES := ""
			SC5->C5_XUSERCA := ""
			MsUnLock()

			DbselectArea("SC5")

			MessageBox("Estorno efetuado!","OK",0)

		Endif

		oMSPedido:Refresh()

	Endif
	//RestArea(aAreaSF4)
Return


Static Function ConsPed()

	Local oButton1
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oGet6
	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Private aColsEx2	:= {}
	Private _cxPedido 	:= oMSPedido:acols[oMSPedido:nAt,4]
	Private _txQtdPed	:= 0
	Private _txQtdPFat	:= 0
	Private _txQtdEnt	:= 0
	Private _txQtdPerda	:= 0
	Private _txQtdSaldo	:= 0
	Private _txValSaldo	:= 0

	Static oDlg2

	DEFINE MSDIALOG oDlg2 TITLE "Consulta de Saldos de Produtos" FROM 000, 000  TO 475, 1000 COLORS 0, 16777215 PIXEL

	@ 007, 007 SAY oSay1 PROMPT "Pedido: "					SIZE 025, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
	@ 005, 036 MSGET oGet1 VAR _cxPedido					SIZE 060, 010 OF oDlg2 COLORS 0, 16777215 PIXEL
	fMSNewGe2()
	@ 210, 007 SAY oSay2 PROMPT "Total Qtd Pedido" 			SIZE 050, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
	@ 220, 007 MSGET oGet2 VAR _txQtdPed 					SIZE 055, 010 OF oDlg2 PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL
	@ 210, 072 SAY oSay3 PROMPT "Total Qtd Pre-fat."	 	SIZE 050, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
	@ 220, 072 MSGET oGet3 VAR _txQtdPFat 					SIZE 055, 010 OF oDlg2 PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL
	@ 210, 137 SAY oSay4 PROMPT "Total Qtd Entregue" 		SIZE 050, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
	@ 220, 137 MSGET oGet4 VAR _txQtdEnt 					SIZE 055, 010 OF oDlg2 PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL
	@ 210, 202 SAY oSay5 PROMPT "Total Qtd Perda"			SIZE 050, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
	@ 220, 202 MSGET oGet5 VAR _txQtdPerda 					SIZE 055, 010 OF oDlg2 PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL
	@ 210, 267 SAY oSay6 PROMPT "Total Qtd Saldo" 			SIZE 050, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
	@ 220, 267 MSGET oGet6 VAR _txQtdSaldo 					SIZE 055, 010 OF oDlg2 PICTURE "@E 999,999,999.9999" COLORS 0, 16777215 PIXEL
	@ 210, 332 SAY oSay6 PROMPT "Valor Saldo"	 			SIZE 050, 007 OF oDlg2 COLORS 0, 16777215 PIXEL
	@ 220, 332 MSGET oGet6 VAR _txValSaldo 					SIZE 060, 010 OF oDlg2 PICTURE "@E 9,999,999,999.99" COLORS 0, 16777215 PIXEL

	@ 212, 400 BUTTON oButton1 PROMPT "Exporta XLS"			SIZE 042, 017 OF oDlg2 ACTION RpExcell(2) PIXEL
	@ 212, 448 BUTTON oButton1 PROMPT "Fechar"				SIZE 042, 017 OF oDlg2 ACTION Close(oDlg2) PIXEL

	ACTIVATE MSDIALOG oDlg2 CENTERED

Return


Static Function fMSNewGe2()

	Local nX
	Local aHeaderEx		:= {}
	Local aFieldFill	:= {}
	Local aFields		:=  {"C6_ITEM"	,"C6_PRODUTO"	,"C6_QTDVEN"	,"C6_QTDENT"		,"C6_QTDENT"		,"C6_QTDENT"	,"C6_QTDENT",	"C6_VALOR"}
	Local aFields2		:= {"Item"  	,"Produto"		,"Quant.Pedido"	,"Quant.Prefat."	,"Quant.Entregue"	,"Quant.Perda"	,"Quant.Saldo", "Val.Saldo"}
	Local aAlterFields	:= {}
	Static oMSPedido

	Static oMSNewGe2

	aColsEx2	:= ConsPedDB()

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))

	For nX := 1 to Len(aFields)
		If aFields[nX] $ "C6_QTDVEN|C6_QTDENT"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,"@E 999,999,999.9999",16,4,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		Else
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Endif
		End
	Next nX

	oMSNewGe2 := MsNewGetDados():New( 020, 007, 204, 491, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", ,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg2, aHeaderEx, aColsEx2)

Return


Static Function ConsPedDB()

	Local cQuery1		:= ""
	Local cEOL			:= chr(13) + chr(10)
	Local aColsEx2		:= {}
	Local aFieldFill	:= {}

	cQuery1 := "SELECT C6_NUM AS PEDIDO, C6_ITEM AS ITEM, C6_PRODUTO AS PRODUTO, C6_QTDVEN AS QTD_PED, "+cEOL
	cQuery1 += "	ISNULL((SELECT ZJ_QTDLIB FROM "+RetSQLName("SZJ")+" SZJ  (NOLOCK) "+cEOL
	cQuery1 += "			WHERE D_E_L_E_T_= '' AND ZJ_FILIAL=C6_FILIAL AND ZJ_DOC='' "+cEOL
	cQuery1 += "					AND ZJ_PEDIDO=C6_NUM AND ZJ_PRODUTO=C6_PRODUTO),0) AS QTD_PREFAT, "+cEOL
	cQuery1 += "	C6_QTDENT AS QTD_ENTREGUE, "+cEOL
	cQuery1 += "	CASE WHEN C6_BLQ='R' THEN C6_QTDVEN-C6_QTDENT ELSE '' END AS QTD_PERDA, "+cEOL
	cQuery1 += "	CASE WHEN C6_BLQ<>'R' THEN C6_QTDVEN-C6_QTDENT ELSE '' END AS QTD_SALDO, "+cEOL
	cQuery1 += "	ISNULL((SELECT SC61.C6_PRCVEN*(SC61.C6_QTDVEN-SC61.C6_QTDENT) FROM SC6010 SC61  (NOLOCK) "+cEOL
	cQuery1 += "			WHERE SC61.D_E_L_E_T_= '' AND SC61.C6_FILIAL=SC6.C6_FILIAL AND SC61.C6_ITEM=SC6.C6_ITEM "+cEOL
	cQuery1 += "					AND SC61.C6_PRODUTO=SC6.C6_PRODUTO AND SC61.C6_BLQ<>'R' AND SC61.C6_NUM=SC6.C6_NUM),0) AS VALSALDO "+cEOL
	cQuery1 += " FROM "+RetSQLName("SC6")+" SC6  (NOLOCK) "+cEOL
	cQuery1 += " WHERE D_E_L_E_T_= '' "+cEOL
	cQuery1 += "	AND (C6_QTDVEN-C6_QTDENT)>0 "+cEOL
	cQuery1 += "	AND C6_NUM='"+oMSPedido:acols[oMSPedido:nAt,4]+"' "+cEOL
	cQuery1 += "ORDER BY 2 "+cEOL

	If Select("TMPSC6") > 0
		TMPSC6->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSC6",.T.,.T.)

	While !EOF()

		Aadd(aFieldFill, TMPSC6->ITEM)
		Aadd(aFieldFill, TMPSC6->PRODUTO)
		Aadd(aFieldFill, TMPSC6->QTD_PED)
		Aadd(aFieldFill, TMPSC6->QTD_PREFAT)
		Aadd(aFieldFill, TMPSC6->QTD_ENTREGUE)
		Aadd(aFieldFill, TMPSC6->QTD_PERDA)
		Aadd(aFieldFill, TMPSC6->QTD_SALDO)
		Aadd(aFieldFill, TMPSC6->VALSALDO)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx2, aFieldFill)
		aFieldFill := {}

		_txQtdPed		+= TMPSC6->QTD_PED
		_txQtdPFat		+= TMPSC6->QTD_PREFAT
		_txQtdEnt		+= TMPSC6->QTD_ENTREGUE
		_txQtdPerda		+= TMPSC6->QTD_PERDA
		_txQtdSaldo		+= TMPSC6->QTD_SALDO
		_txValSaldo		+= TMPSC6->VALSALDO

		DbSkip()

	EndDo

Return (aColsEx2)


Static Function AjustaPrc()

	Local oButton1
	Local oButton2
	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Local oSay7
	Local oSay8
	Local oSay9
	Local oSay10
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oGet6
	Local oGet7
	Local oGet8
	Local oGet9 
	Local oGet10 := CTOD("")
	Local oMultiGe1
	Local cUsers		:= ""
	Local nOpcA			:= 0
	Local dGetDtEmPe	:= oMSPedido:acols[oMSPedido:nAt,2]
	Private cGetFilial	:= oMSPedido:acols[oMSPedido:nAt,31]
	
	Private cGetPed		:= oMSPedido:acols[oMSPedido:nAt,4]
	Private cGetCliCod	:= oMSPedido:acols[oMSPedido:nAt,7]
	Private cGetCliNom	:= AllTrim(oMSPedido:acols[oMSPedido:nAt,8])
	Private cGetTabAtu	:= Posicione("SC5",1,xFilial("SC5")+cGetPed,"C5_TABELA")
	Private cGetTabNew	:= "   "
	Private cGetCondAtu	:= Posicione("SC5",1,xFilial("SC5")+cGetPed,"C5_CONDPAG")
	Private cGetTpPrAtu := Posicione("SC5",1,xFilial("SC5")+cGetPed,"C5_PRAZO")
	Private cGetDtEAtu 	:= Posicione("SC5",1,xFilial("SC5")+cGetPed,"C5_FECENT")
	Private cGetCondNew	:= "   "
	Private cGetTpPrNew := "   "
	Private cGetDtENew := CTOD("") //DATE()
	Private cPolCom		:= oMSPedido:acols[oMSPedido:nAt,28]
	Private cCodPol		:= oMSPedido:acols[oMSPedido:nAt,29]
	Private cMultiGe1	:= Posicione("SC5",1,xFilial("SC5")+cGetPed,"C5_XOBSINT")
	Private oProcesso 	:= Nil


	cUsers		:= AllTrim(GetMv("MV_HDIGPRC"))
	cUsers		+= "|" + AllTrim(SuperGetMV("MV_HDIGPR2",.F.,"000000"))
//	cUsers		+= "000088"
//	cCodUsr   	:= AllTrim(SuperGetMV("MV_XUSRALT",.F.,"000000"))	//Usuarios que poderao alterar pedido independente de ja ter gerado DAP.                                                    

	If __CUSERID $ cUsers

//		If !(__cUserId $ cCodUsr)
//			MsgYesNo("Esse pedido possui DAP gerado. N�o poder� ser alterado!", "A T E N � � O")
//		Else
//			If !MsgYesNo("Este pedido j� foi liberado pelo DAP. Confirma Altera��o?","A T E N � � O")

		Static oDlg

		DEFINE MSDIALOG oDlg TITLE "Ajusta Tabela de Pre�os e Condi��o de Pagamento" FROM 000, 000  TO 400, 700 COLORS 0, 16777215 PIXEL

		@ 008, 007 SAY oSay1 PROMPT "Pedido: " 					SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 006, 030 MSGET oGet1 VAR cGetPed 						SIZE 053, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 008, 104 SAY oSay2 PROMPT "Cliente:" 					SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 006, 126 MSGET oGet2 VAR cGetCliNom					SIZE 214, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

		@ 031, 025 SAY oSay3 PROMPT "Tabela de Pre�o Atual:" 	SIZE 066, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 029, 097 MSGET oGet3 VAR cGetTabAtu					SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 031, 180 SAY oSay4 PROMPT "Tabela de Pre�os Nova:" 	SIZE 066, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 029, 254 MSGET oGet4 VAR cGetTabNew 					SIZE 060, 010 OF oDlg VALID !VAZIO() .AND. GatAjust(2) COLORS 0, 16777215 F3 "SZ5" PIXEL
//		@ 029, 254 MSGET oGet4 VAR cGetTabNew 					SIZE 060, 010 OF oDlg VALID !VAZIO() .AND. GatAjust(2) COLORS 0, 16777215 F3 "DA0-02" PIXEL

		@ 048, 025 SAY oSay5 PROMPT "Condi��o Pagamento Atual:"	SIZE 066, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 046, 097 MSGET oGet5 VAR cGetCondAtu 					SIZE 060, 010 OF oDlg  COLORS 0, 16777215 READONLY PIXEL
		@ 048, 180 SAY oSay6 PROMPT "Condi��o Pagamento Nova:" 	SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 046, 254 MSGET oGet6 VAR cGetCondNew 					SIZE 060, 010 OF oDlg VALID !VAZIO() .AND. GatAjust(1) COLORS 0, 16777215 F3 "SE402 " PIXEL

		@ 065, 025 SAY oSay7 PROMPT "Tipo de prazo Atual:"		SIZE 066, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 063, 097 MSGET oGet7 VAR cGetTpPrAtu 					SIZE 060, 010 OF oDlg  COLORS 0, 16777215 READONLY PIXEL
		@ 065, 180 SAY oSay8 PROMPT "Tipo de prazo Nova:" 		SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 063, 254 MSGET oGet8 VAR cGetTpPrNew 					SIZE 060, 010 OF oDlg VALID !VAZIO() COLORS 0, 16777215 F3 "SZ6" PIXEL

		@ 082, 025 SAY oSay9 VAR "Data de Entrega Atual:"	SIZE 066, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 080, 097 MSGET oGet9 VAR cGetDtEAtu 					SIZE 060, 010 OF oDlg  COLORS 0, 16777215 READONLY PIXEL 
		@ 082, 180 SAY oSay10  VAR "Data de Entrega Nova:" 	SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 080, 254 MSGET oGet10 VAR cGetDtENew 					SIZE 060, 010 OF oDlg VALID Dtnew(cGetPed,cGetDtENew,dGetDtEmPe) COLORS 0, 16777215 PIXEL 
//					SAY oSay4	VAR "Data At� " 				SIZE 070, 010 OF oFiltro COLORS 0, 16777215 PIXEL
		@ 097, 025 GET oMultiGe1 VAR cMultiGe1 		OF oDlg MULTILINE SIZE 289, 045 COLORS 0, 16777215 HSCROLL READONLY PIXEL

		@ 170, 259 BUTTON oButton1 PROMPT "Confirma" 			SIZE 037, 012 ACTION (nOpcA:=1,Close(oDlg)) 	OF oDlg PIXEL
		@ 170, 302 BUTTON oButton2 PROMPT "Cancela" 			SIZE 037, 012 ACTION Close(oDlg)				OF oDlg PIXEL

		ACTIVATE MSDIALOG oDlg CENTERED
//			EndIf
//		EndIf				
	Else
		MessageBox("Usu�rio sem permiss�o para utiliza��o da rotina.","MV_HDIGPR2",16)
	EndIf

	If nOpcA == 1 //Confirmacao de processamento

		DbSelectArea("SC9")
		DbSetOrder(1)
		DbSeek(xfilial("SC9")+cGetPed)

		IF Select("TEMP") > 0
			TEMP->(dbCloseArea())
		Endif

		cQuery := " SELECT C9_NFISCAL, C9_PEDIDO FROM "+RetSqlName('SC9')+" WITH(NOLOCK)  WHERE C9_PEDIDO = '"+cGetPed+"' AND D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_FILIAL = '0101' "

		TCQUERY cQuery NEW ALIAS TEMP

		IF Empty(TEMP->C9_NFISCAL) .AND. !EMPTY(TEMP->C9_PEDIDO) //Valida��o de libera��o Faturamento. Ronaldo Pereira 17/06/20
			MessageBox("Pedido j� liberado para faturamento, n�o pode ser alterado!", "A T E N � � O",48)
			AjustaPrc()
		Else
			If AllTrim(cGetTabNew) <> '' .and. AllTrim(cGetCondNew) <> ''
				oProcesso := MsNewProcess():New({|lEnd| AjusPrc() },"Atualiza��o de Registros ...")//Funcao de leitura do arquivo que transforma o conteudo lido em Array
				oProcesso:Activate()
			Else
				MessageBox("Favor preencher a Nova Tabela de Pre�os e a Nova Condi��o de Pagamento.","Aten��o",16)
				AjustaPrc()
			End
		EndIf

	EndIf

Return

/*/{Protheus.doc} Dtnew
	(long_description)
	@type  Static Function
	@author Geyson Albano
	@since 08/12/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	@example
	(examples)
	@see (links_or_references)
/*/
Static Function Dtnew(cGetPed,cGetDtENew,dGetDtEmPe)
Local cNewData := cGetDtENew
Local lAlt := .F.

If dGetDtEmPe > cNewData
	MessageBox("Data de entrega n�o pode ser menor que a data de emiss�o!!! ","Dtnew",16)
Else
	lAlt := .T.
EndIf

	
Return lAlt

Static Function GatAjust(_tp)

	If _tp == 1
		If __CUSERID $ GetMV("MV_XUSRCOM")
			ExistCpo("SE4",cGetCondNew)
		Else
			If !(ExistCpo("SZ3", cPolCom+cGetCondNew))
				MessageBox("Condi��o de Pagamento n�o esta diponivel para a Politica Comercial.","Aten��o",16)
			End
		EndIf

	ElseIf _tp == 2

		cQuery := " SELECT COUNT(DA0_CODTAB) AS COUNT_TAB FROM DA0010 "
		cQuery += " WHERE D_E_L_E_T_= '' AND DA0_CODTAB='"+cGetTabNew+"' "
		cQuery += " AND DA0_DATDE<='"+dToS(dDataBase)+"' AND DA0_DATATE>='"+dToS(dDataBase)+"' "

		TCQuery cQuery New Alias "QRYDA0"
		DbSelectArea("QRYDA0")
		QRYDA0->(DbGotop())
		vCodCount	:= QRYDA0->COUNT_TAB
		QRYDA0->(DBCloseArea())

		If vCodCount = 0
			MessageBox("Tabela de pre�os inexistente ou fora da vig�ncia.","Aten��o",16)
			oGet4:SetFocus()
		End
	/*
	ElseIf _tp == 3

		If !(ExistCpo("SZ6", cCodPol+cGetTpPrNew))
			Alert("Esse Prazo n�o esta diponivel para a Politica Comercial.")
		End
	*/
	EndIf
Return

Static Function AjusPrc()

	If Select("TRBDA1") > 0
		DbSelectArea("TRBDA1")
		DbCloseArea()
	EndIf
	_qry := "Select DA1_PRCVEN, C6_PRODUTO from "+RetSqlName("SC6")+" SC6 "
	_qry += "left join "+RetSqlName("DA1")+" DA1 on DA1.D_E_L_E_T_ = '' and DA1_CODTAB = '"+cGetTabNew+"' and DA1_CODPRO = C6_PRODUTO "
	_qry += "where SC6.D_E_L_E_T_ = '' and C6_NUM = '"+cGetPed+"' "
	_qry += "and (DA1_PRCVEN = 0 or DA1_PRCVEN is null) "
	_qry += "order by C6_PRODUTO "

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,_Qry),"TRBDA1", .F., .T.)

	_areaT := GetArea()
	DbSelectArea("TRBDA1")
	DbGoTop()

	If !EOF()
		cErro := "Os seguintes produtos n�o possuem pre�o na tabela informada"+chr(13)+ Chr(10)
		While !EOF()
			cErro += "- "+ TRBDA1->C6_PRODUTO+chr(13)+ Chr(10)
			DbSelectArea("TRBDA1")
			DbSkip()
		End
		DbCloseArea()
		EECView(cErro)
		Restarea(_areaT)
		Return()
	Endif
	Restarea(_areaT)

	nPven		:= 0
	nPVdesc		:= 0
	nPVdesc6	:= 0
	cDesc		:= POSICIONE("SE4",1,xFilial("SE4")+cGetCondNew,"E4_DESCFIN")
	cDescTab	:= AllTrim(Posicione("DA0",1,xFilial('DA0')+cGetTabNew,"DA0_DESCRI"))
	cDescCond	:= AllTrim(Posicione("SE4",1,xFilial('SE4')+cGetCondNew,"E4_DESCRI"))


	nDescFin	:= u_pegadesc(SC5->C5_CLIENTE,SC5->C5_LOJACLI,cDesc)

	oProcesso:SetRegua1( 2 )

	oProcesso:IncRegua1("Atualizando Pedido : "+cGetPed )

	dbSelectArea("SC5")
	dbSetOrder(1)
	dbSeek(xFilial("SC5")+cGetPed)

	If ( !Eof() .And. SC5->C5_FILIAL == cGetFilial .And. SC5->C5_NUM == cGetPed )

		RecLock("SC5",.F.)
		SC5->C5_TABELA	:= cGetTabNew
		SC5->C5_CONDPAG	:= cGetCondNew
		SC5->C5_DESC1	:= nDescFin
		SC5->C5_XDESTAB	:= AllTrim(Posicione("DA0",1,xFilial('DA0')+cGetTabNew,"DA0_DESCRI"))
		SC5->C5_XDESCOD := AllTrim(Posicione("SE4",1,xFilial('SE4')+cGetCondNew,"E4_DESCRI"))
		SC5->C5_PRAZO 	:= cGetTpPrNew
		SC5->C5_XDPRAZO	:= AllTrim(Posicione("SZ6",1,xFilial('SZ6')+cCodPol+cGetTpPrNew,"Z6_DESC"))

		If !Empty(cGetDtENew)
		SC5->C5_FECENT 	:= cGetDtENew
		EndIf
		MsUnLock()

		dbSelectArea("SC6")
		dbSetOrder(1)
		dbSeek(xFilial("SC6")+cGetPed)

		While ( !Eof() .And. SC6->C6_FILIAL == xFilial('SC5') .And. SC6->C6_NUM == cGetPed )

			oProcesso:IncRegua2("Ajustando Item "+SC6->C6_ITEM+" Produto "+SC6->C6_PRODUTO)

			nPven	  := Posicione("DA1",1,xFilial("DA1")+cGetTabNew+SC6->C6_PRODUTO,"DA1_PRCVEN")
			nPVdesc   := Round(nPven*(1-(nDescFin/100)),2)
			nPVdesc6  := Round(nPven*(1-(nDescFin/100)),6)

			RecLock("SC6",.F.)
			SC6->C6_PRUNIT	:= nPven
			SC6->C6_PRCVEN	:= nPVdesc6
			SC6->C6_VALOR	:= NoRound((SC6->C6_QTDVEN*round(nPVdesc,2)),2)
			MsUnLock()

			DbSkip()

		EndDo

	EndIf

Return

Static Function Motivo(cPedido)

	Local lRet := .T.
	Local oDlg2
	Private cMotivo := CriaVar("C5_XMOTRES",.T.)
	Private lMotivo := .F.
	Private oFont1  := TFont():New("Arial",,18,,.F.,,,,,.F.)

	DbSelectArea("SC5")
	DbSetOrder(1)
	If DbSeek(xFilial("SC5")+cPedido)
		If Empty(Alltrim(SC5->C5_XMOTRES))
			//While !lMotivo

			DEFINE MSDIALOG oDlg2 TITLE "Motivo Elimina��o de Res�duo" FROM 000,000 TO 130,500 PIXEL //Style DS_MODALFRAME

			@005,005 TO 045,250 OF oDlg2 PIXEL
			@008,010 SAY "Pedido:"             SIZE 020,007 OF oDlg2 PIXEL
			@007,035 SAY cPedido          	   SIZE 030,010 OF oDlg2 FONT oFont1 PIXEL
			@008,075 SAY "Usu�rio:"            SIZE 020,007 OF oDlg2 PIXEL
			@007,100 SAY UsrRetName(__cUserId) SIZE 070,010 OF oDlg2 FONT oFont1 PIXEL
			@020,010 SAY "Motivo da Elimina��o de Res�duo:" SIZE 120,007 OF oDlg2 PIXEL
			@028,010 MSGET cMotivo SIZE 235,011 OF oDlg2 PIXEL VALID ValidMot(cMotivo)

			DEFINE SBUTTON FROM 050,223 TYPE 1 ACTION (GravaMot(SC5->C5_NUM,cMotivo),oDlg2:End()) ENABLE OF oDlg2
			//DEFINE SBUTTON FROM 050,150 TYPE 2 ACTION  oDlg2:End()  ENABLE OF oDlg2

			ACTIVATE MSDIALOG oDlg2 CENTERED

			//EndDo
		EndIf
	EndIf

Return lRet

Static Function ValidMot(cTexto)
	Local lRet := .T.

	If Empty(cTexto)
		MessageBox("Favor digitar um motivo para a elimina��o de res�duo.","ATEN��O",48)
		lRet := .F.
	EndIf

Return lRet

Static Function GravaMot(cPedido,cTexto)
	Local lRet := .T.

	If Empty(Alltrim(cTexto))
		MessageBox("Favor digitar um motivo para a elimina��o de res�duo.","ATEN��O",48)
		Return .F.
	Else
		RecLock("SC5",.F.)
		SC5->C5_XMOTRES := Alltrim(cTexto)
		SC5->C5_XUSERCA := UsrRetName(__cUserId)
		MsUnlock()
		lMotivo := .T.
	EndIf

Return lRet

Static Function ElimRes(_ped)

Local lResd := .F.
Local aItRes := {}
	nVlrDep := 0

	Begin Transaction
	
//	Elimina residuo de itens e atualiza cabecalho do pedido.
		DbSelectArea("SC6")
		DbSetOrder(1)
		DbSeek(xfilial("SC6")+_ped)

		While !EOF() .and. SC6->C6_NUM = _ped
			If (SC6->C6_QTDVEN - SC6->C6_QTDENT) > 0 .and. Alltrim(SC6->C6_BLQ) == 'R'
				cMsg := "- Produtos n�o faturados [ " + Alltrim( SC6->C6_PRODUTO ) + " ] Qtd: "+CValToChar(SC6->C6_QTDVEN - SC6->C6_QTDENT)+""
				AADD( aItRes, cMsg )
				lResd:= .T.
			Endif

				DbSelectArea("SC6")
				DbSkip()
			EndDo
			
			If lResd
				DbSelectArea("SC5")
				DbSetOrder(1)
				If DbSeek(xfilial("SC5")+_ped)
				cRepres := SC5->C5_VEND1
				cPedWeb := SC5->C5_XPEDWEB
				cCliPed	:= Alltrim(SC5->C5_CLIENTE +" - "+ SC5->C5_XNOMCLI )
				cMotRes	:= SC5->C5_XMOTRES
				EndIf
				If !Empty(cRepres)
					NotifRES(_ped,aItRes,cRepres,cPedWeb,cCliPed,cMotRes)
				EndIf
			EndIf

	End Transaction
Return
/*/
	------------------------------------------------------------------
	{Protheus.doc} NotifRES()
	Notificacao do Log de Erro Pedidos BLOQUEIO DE DEMANDA

	@author Geyson Albano
	@since Nov/2020
	@version 1.00
	------------------------------------------------------------------
/*/
Static Function NotifRES(_ped,aItRes,cRepres,cPedWeb,cCliPed,cMotRes)

	Local cAssunto	:= "Res�duo gerado para o pedido:  "+_ped 
	Local cMailB	:= GetMv("HP_EMAIRES") 
	
	cemailrep := Alltrim(POSICIONE("SA3",1,xFilial("SA3")+Alltrim(cRepres),"A3_EMAIL"))
	If !Empty(cemailrep)
		cMailB += ";"+cemailrep
	Endif
	//U_SendMail( cMailB, cAssunto, getBody( "", "6", aItRes ))

	U_SendMail( "ronaldo.ti@hopelingerie.com.br", cAssunto, getBody( "", "6", aItRes,cPedWeb,cCliPed,_ped,cMotRes ))
Return

/*/
	------------------------------------------------------------------
	{Protheus.doc} getBody
	Retorna o Texto do Body do Email

	@author Sergio S. Fuzinaka
	@since Jul/2020
	@version 1.00
	------------------------------------------------------------------
/*/
Static Function getBody( cMsg, cTp, aLog,cPedWeb,cCliPed,_ped,cMotRes )

	Local nX		:= 0
	Local cHtml 	:= ""

	cHtml := '<!DOCTYPE HTML>'
	cHtml += '<html lang="pt-br">'
	cHtml += '<head>'
	cHtml += '<meta charset="utf-8">'
	cHtml += '</head>'
	cHtml += '<body>'

	cHtml += '<h3>Log gera��o de Res�duo para o Pedido Externo: '+cPedWeb+' e no Protheus de num: '+_ped+'  </h3>'
	cHtml += '<h3>Cliente: '+cCliPed+' </h3>'
	cHtml += '<h3>Motivo: '+cMotRes+' </h3>'

		
	cHtml += '<ul>'
		For nX := 1 To Len( aLog )
			cHtml += '<br>' + aLog[nX] + '</b>'
		Next
		cHtml += '</ul>'

	cHtml += '</body>'
	cHtml += '</html>'

Return( cHtml )
