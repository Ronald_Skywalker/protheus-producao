#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"
#INCLUDE "TOPCONN.CH"
#Include "TOTVS.CH"

User function HFATA013()

public cPedWeb  := ""
Processa({|| U_HFAT13IT(cPedWeb)},"Processando...","Processando Pedidos Intermidia...")

//U_HFAT13IT()

Return

User Function HFAT13IT()

Local nx,ny    	:= 0
Local cNroPed  	:= ""
Local cCGC    	:= ""
Local cCodCli  	:= ""
Local cLojCli  	:= ""
Local cNovCli  	:= ""
Local cTipPes  	:= ""
Local cNomCli  	:= ""
Local nPosP    	:= 0
Local cProd    	:= ""
Local cRef     	:= ""
Local cCor     	:= ""
Local cTam     	:= ""
Local nErro    	:= 0
Local nGerados 	:= 0
Local cCanal   	:= ""
Local cPlB2BV  	:= ""
Local cTpB2BV  	:= ""
Local cPlB2BF  	:= ""
Local cTpB2BF  	:= ""
Local cPlB2BH  	:= ""
Local cTpB2BH  	:= ""
Local cPlB2BL  	:= ""
Local cTpB2BL  	:= ""
Local cPlB2B1  	:= ""
Local cTpB2B1  	:= ""
Local cNature  	:= ""
Local cPolitc  	:= ""
Local cTipPed  	:= ""
Local cCndPag  	:= ""
Local cCdEnd   	:= ""
Local cCdMun   	:= ""
Local cCdEnt   	:= ""
Local cCdHop   	:= ""
Local cCdGer   	:= ""
Local cCdTip   	:= ""
Local cCdGrp   	:= ""
Local cCdDiv   	:= ""
Local cCdReg   	:= ""
Local cCdMac   	:= ""
Local cNtB2C   	:= ""
Local cNmCan   	:= ""
Local cNmTip   	:= ""
Local cNmGrp   	:= ""
Local cNmDiv   	:= ""
Local cNmReg   	:= ""
Local cNmMac   	:= ""
Local cEndere  	:= ""
Local cComple  	:= ""
Local cBairro  	:= ""
Local cEstado  	:= ""
Local cCEP     	:= ""
Local cMunici  	:= ""
Local cEmail   	:= ""
Local cDDDTel  	:= ""
Local cNumTel  	:= ""
Local cDDDCel  	:= ""
Local cNumCel  	:= ""
Local cTime    	:= ""
Local cDtTime  	:= ""
Local cTpOper  	:= "01"
Local cTES     	:= ""
Local cCodPed  	:= ""
Local nParce   	:= 0
Local cCdPrz   	:= ""
Local cItem     := "01"
Local aItens    :={}
Local aLinha    :={}
Local aCabec    :={}
Local cTipoSZ4:= ''
Local cNextAlias := GetNextAlias()
Local nXX	:=  0
Private cAutVdSLD	:= .F.
Private	cAutVdSTA	:= .F.
Private	cErroSLD	:= .F.
Private	cErroSTA	:= .F.
Private	lMsErroAuto	:= .F.
Private lMsHelpAuto	:= .T.    
Private lAutoErrNoFile := .T.

Prepare Environment Empresa "01" Filial "0101"

//While .T.
nx			:= 0
ny    		:= 0
cNroPed  	:= ""
cCGC    	:= ""
cCodCli  	:= ""
cLojCli  	:= ""
cNovCli  	:= ""
cTipPes  	:= ""
cNomCli  	:= ""
nPosP    	:= 0
cProd    	:= ""
cRef     	:= ""
cCor     	:= ""
cTam     	:= ""
nErro    	:= 0
nGerados 	:= 0
cCanal   	:= ""
cPlB2BV  	:= ""
cTpB2BV  	:= ""
cPlB2BF  	:= ""
cTpB2BF  	:= ""
cPlB2BH  	:= ""
cTpB2BH  	:= ""
cPlB2BL  	:= ""
cTpB2BL  	:= ""
cPlB2B1  	:= ""
cTpB2B1  	:= ""
cNature  	:= ""
cPolitc  	:= ""
cTipPed  	:= ""
cCndPag  	:= ""
cOrdemCom   := ""
cInstru     := ""
cCdEnd   	:= ""
cCdMun   	:= ""
cCdEnt   	:= ""
cCdHop   	:= ""
cCdGer   	:= ""
cCdTip   	:= ""
cCdGrp   	:= ""
cCdDiv   	:= ""
cCdReg   	:= ""
cCdMac   	:= ""
cNtB2C   	:= ""
cNmCan   	:= ""
cNmTip   	:= ""
cNmGrp   	:= ""
cNmDiv   	:= ""
cNmReg   	:= ""
cNmMac   	:= ""
cEndere  	:= ""
cComple  	:= ""
cBairro  	:= ""
cEstado  	:= ""
cCEP     	:= ""
cMunici  	:= ""
cEmail   	:= ""
cDDDTel  	:= ""
cNumTel  	:= ""
cDDDCel  	:= ""
cNumCel  	:= ""
cTime    	:= ""
cDtTime  	:= ""
cTpOper  	:= "01"
cTES     	:= ""
cCodPed  	:= ""
Parce   	:= 0
cCdPrz   	:= ""
dDatEntr  	:= Date()
cChvA1		:= ""
cChvA2		:= ""
cLog		:= ""
cAutVdSLD	:= .F.
cAutVdSTA	:= .F.
cErroSLD	:= .F.
cErroSTA	:= .F.
lMsErroAuto	:= .F.



cBloqSLD	:= SuperGetMV("MV_HBLQSLD",.F.,".F.")
cBloqSTA	:= SuperGetMV("MV_HBLQSTA",.F.,".F.")
lBloqDEM	:= SuperGetMV("MV_HBLQDEM",.F.,".T.")

cTime   	:= Time()
cDtTime 	:= DTOS(date()) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

//Volta para a fila os pedidos com falha na integra��o 
//cUpdITM  := " UPDATE [IDB_MOBILIVENDAS].[dbo].[TBT_CARRINHO] SET  CodSituacaoPedido = '3' WHERE CodSituacaoPedido = '99'"
//TcSqlExec(cUpdITM)
//Return

GravaLog("Inicio do processamento...","INICIO")

IF Select("TMP") > 1
	TMP->(dbclosearea())
ENDIF


/*_qry := " SELECT REPLACE(CodCarrinho,'.','') AS CARRINHO,CodTabelaPreco as COD_TAB_PRECO,ValorTotal AS VALOR,CodInstrucao, "
_qry += " REPLACE(CONVERT(VARCHAR, CONVERT(DATETIME, DataEntrega,103),102),'.','') AS DT_ENTREGA,"
_qry += " REPLACE(CONVERT(VARCHAR, CONVERT(DATETIME, DataEmissao,103),102),'.','') AS DT_EMISSAO,"
_qry += " REPLACE(CONVERT(VARCHAR, CONVERT(DATETIME, CtrlDataOperacao,103),102),'.','') AS DT_PED, * FROM [IDB_MOBILIVENDAS].[dbo].[TBT_CARRINHO] WHERE CodSituacaoPedido  = '98'  "
*/

_Qry := "  SELECT TOP 1 REPLACE(CodCarrinho,'.','') AS CARRINHO,CodTabelaPreco as COD_TAB_PRECO,ValorTotal AS VALOR,CodInstrucao, "
_Qry += "  REPLACE(CONVERT(VARCHAR, CONVERT(DATETIME, DataEntrega,103),102),'.','') AS DT_ENTREGA, "
_Qry += "  REPLACE(CONVERT(VARCHAR, CONVERT(DATETIME, DataEmissao,103),102),'.','') AS DT_EMISSAO, "
_Qry += "  REPLACE(CONVERT(VARCHAR, CONVERT(DATETIME, CtrlDataOperacao,103),102),'.','') AS DT_PED, * FROM [IDB_MOBILIVENDAS].[dbo].[TBT_CARRINHO] WHERE CodSituacaoPedido  = '3' "
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMP",.T.,.T.)

DbSelectArea("TMP")
DbGoTop()
ProcRegua(RecCount())

//WHILE TMP->(!EOF())
	
	cNroPed := GetSX8Num("SC5","C5_NUM",,1)
	DbSelectArea("SC5")
	DbSetOrder(1)
	
	While SC5->(DbSeek(xfilial("SC5")+cNroPed))
		ConfirmSX8()
		cNroPed := SOMA1(cNroPed)
	End

	IncProc()
	
	cCodCarr := ALLTRIM(TMP->CARRINHO)
	cPedWeb :=  ALLTRIM(TMP->CODCARRINHO)
	
	GravaLog("Iniciando a importa��o do pedido: "+cPedWeb)
	
	DbSelectArea("SA1")
	DbSetOrder(1)
	DbSeek(xFilial("SA1")+Alltrim(TMP->CodPessoaCliente))
	cCodCli := SA1->A1_COD
	cLojCli := SA1->A1_LOJA
	
	//Inserido por Robson - Verifica se o cliente est� bloqueado para efetuar pedidos atraves do campo A1_XBLQPED
	If SA1->A1_XBLQPED == "S"
		//Altera Status do Pedido para 99 - Erro de Integra��o
		cNroPed:= ""
		cUpdITM  := " UPDATE [IDB_MOBILIVENDAS].[dbo].[TBT_CARRINHO] SET CodSituacaoPedido = '99' , CodPedido = '"+cNroPed+"' WHERE  CodCarrinho = '"+cPedWeb+"' "  "
		TcSqlExec(cUpdITM)
		
		//Insere um registro na tabela de criticas para que o representante enxergue o problema com o pedido.
		cERROR:= "Cliente bloqueado para efetuar pedidos."
		cUpd2ITM := "INSERT INTO [IDB_MOBILIVENDAS].[dbo].[TBT_CRITICA_CARRINHO] (CodCarrinho,SeqCritica,Critica) VALUES "
		cUpd2ITM += "('"+cPedWeb+"',(SELECT ISNULL(MAX(SeqCritica),0)+1 FROM [IDB_MOBILIVENDAS].[dbo].[TBT_CRITICA_CARRINHO] WHERE  CodCarrinho = '"+cPedWeb+"' ),'"+cERROR+"' )"
		TCSQLExec(cUpd2ITM)
		
		//Restaura o Ambiente
		//Reset Environment
		Return
	Endif
	
	GravaLog("Cliente "+cCodCli+"-"+cLojCli+" encontrado no Protheus. Pedido: "+cPedWeb)
	
	cNature := ""
	
	If !Empty(SA1->A1_NATUREZ)
		cNature := SA1->A1_NATUREZ
	Endif
	
	
	cCanal 		:= AllTrim(SA1->A1_XCANAL)
	cCndPag 	:= Alltrim(TMP->CodCondicaoPagamento)
	nDesconto	:= POSICIONE("SE4",1,xFilial("ZE4")+cCndPag,"E4_DESCFIN")
	cCdPrz 		:= Alltrim(TMP->TipoPrazo)
	cDescPrz    := POSICIONE("SZ7",1,xFilial("SZ7")+alltrim(cCdPrz),"Z7_DESC")
	//dDtEntrega	:= ddatabase
	//cData       := TMP->DT_PED // Comentado por Robson para que os pedidos entrem com a data da emissao, para que o mesmo nao sofra alteracao por conta de falhas de integracao
	cDataEmi	:=  ALLTRIM(TMP->DT_PED) //TMP->DT_EMISSAO // Altera��o da data para considerar a data da transmiss�o(Alinhado com Patricia e Marjorie). Weskley Silva 28.08.2019
	cPolitc		:= Alltrim(TMP->CodPoliticaComercial)
	cTiposDesc  := ""
	cCodRep:= Iif(SubStr(TMP->CodPessoaRepresentante,1,1) $ 'R,G',SubStr(TMP->CodPessoaRepresentante,2,6),Alltrim(TMP->CodPessoaRepresentante))
	cPercRepre	:= POSICIONE("SA3",1,xFilial("SA3")+cCodRep,"A3_COMIS") // ADICIONADO POR ROBSON - CORRE��O COMISS�O
	//cPercRepre	:= POSICIONE("SA3",1,xFilial("SA3")+Alltrim(TMP->CodPessoaRepresentante),"A3_COMIS") // COMENTADO POR ROBSON - CORRE��O COMISS�O
	cGerente	:= POSICIONE("ZA4",1,xFilial("ZA4")+SA1->A1_XMICRRE,"ZA4_GEREN")
	cPercGer	:= POSICIONE("SA3",1,xFilial("ZA3")+cGerente,"A3_COMIS2")
	cSuperv		:= POSICIONE("ZA4",1,xFilial("ZA4")+SA1->A1_XMICRRE,"ZA4_SUPERV")
	cPercSuper	:= POSICIONE("SA3",1,xFilial("ZA3")+cSuperv,"A3_COMIS3")
	cTipoSZ4    := POSICIONE("SZ4",1,xFilial("SZ4")+cPolitc,"Z4_TPPED") 
	cTpPed		:= IIF(EMPTY(Alltrim(TMP->TipoPedido)),cTipoSZ4,Alltrim(TMP->TipoPedido))
	cTpOper		:= POSICIONE("SZ1",1,xFilial("SZ1")+cTpPed,"Z1_TPOPER")
	cDataITM    := TMP->DT_ENTREGA
	cOrdemCom   := ALLTRIM(TMP->OrdemCompra)
	cInstru     := POSICIONE("SX5",1,XFILIAL("SX5")+'Z4'+Alltrim(TMP->CodInstrucao),"X5_DESCRI")
	cDataITM    := Iif(STOD(Alltrim(cDataITM))<ddatabase,DTOS(ddatabase), Alltrim(cDataITM) )

	If Empty(cTpOper)
		cTpOper := '01'
	Endif

	/*For nx := 1 to len(TMP->INSTRUCAO) step 3
	cAux2 := SubStr(TMP->INSTRUCAO,nx,2)
	If alltrim(cAux2) <> ""
	cTiposDesc += Alltrim(POSICIONE("SX5",1,xFilial("SX5")+'Z4'+cAux2,"X5_DESCRI"))+"|"
	Endif
	Next
	*/
	
	_qry := " SELECT SUM(B.Qtd *  A.ValorUnitario) AS VALOR  FROM [IDB_MOBILIVENDAS].[dbo].[TBT_ITEM_CARRINHO] A "
	_qry +=	" JOIN [IDB_MOBILIVENDAS].[dbo].[TBT_GRADE_ITEM_CARRINHO] B ON A.CodCarrinho  = B.CodCarrinho AND A.CodItemCarrinho = B.CodItemCarrinho "
	_qry += " WHERE A.CodCarrinho = '"+cPedWeb+"' "
	
	If Select("TPP") > 0
		TPP->(dbclosearea())
	Endif
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TPP",.T.,.T.)
	
	If TPP->VALOR != TMP->VALOR
		_qry1 := " UPDATE [IDB_MOBILIVENDAS].[dbo].[TBT_CARRINHO] SET CodSituacaoPedido = '99' , CodPedido = '"+cNroPed+"' WHERE  CodCarrinho = '"+cPedWeb+"' "  "
		TcSqlExec(_qry1)
		
		
		cSql2 := "INSERT INTO [IDB_MOBILIVENDAS].[dbo].[TBT_CRITICA_CARRINHO] (CodCarrinho,SeqCritica,Critica) VALUES "
		cSql2 += "('"+cPedWeb+"',(SELECT ISNULL(MAX(SeqCritica),0)+1 FROM [IDB_MOBILIVENDAS].[dbo].[TBT_CRITICA_CARRINHO] WHERE  CodCarrinho = '"+cPedWeb+"' ),'Divergencia de Valores entre cabe�alho e Item.' )"
		TCSQLExec(cSql2)
		
		return
	Endif
	
	cTiposDesc := SubStr(cTiposDesc,1,Len(cTiposDesc)-1)
	
	nPercDesc 	:= u_pegadesc(cCodCli,cLojCli,nDesconto)
	
	aCabec := {}
	
	aadd(aCabec,{"C5_NUM"    	, cNroPed,Nil})
	aadd(aCabec,{"C5_TIPO"   	, "N",Nil})
	aadd(aCabec,{"C5_CLIENTE"	, ALLTRIM(cCodCli),Nil})//ALLTRIM("cCodCli"),Nil})//
	aadd(aCabec,{"C5_LOJACLI"	, ALLTRIM(cLojCli),Nil})
	aadd(aCabec,{"C5_CLIENT" 	, ALLTRIM(cCodCli),Nil})
	aadd(aCabec,{"C5_LOJAENT"	, ALLTRIM(cLojCli),Nil})
	aadd(aCabec,{"C5_EMISSAO"	, STOD(cDataEmi), Nil})  //STOD(cDataEmi),Nil})
	aadd(aCabec,{"C5_POLCOM" 	, ALLTRIM(cPolitc),Nil})
	aadd(aCabec,{"C5_TPPED"  	, ALLTRIM(cTpPed),Nil})
	aadd(aCabec,{"C5_TABELA" 	, ALLTRIM(TMP->COD_TAB_PRECO),Nil})
	aadd(aCabec,{"C5_CONDPAG"	, ALLTRIM(cCndPag),Nil})
	aadd(aCabec,{"C5_NATUREZ"	, ALLTRIM(cNature),Nil})
	aadd(aCabec,{"C5_XPEDWEB"	, ALLTRIM(cCodCarr),Nil})
	aadd(aCabec,{"C5_DESC1"		, nPercDesc,Nil})
	aadd(aCabec,{"C5_XOBSINT"	, ALLTRIM(TMP->Observacoes),Nil})
	aadd(aCabec,{"C5_XCANAL" 	, ALLTRIM(SA1->A1_XCANAL),Nil})
	aadd(aCabec,{"C5_XCANALD"	, ALLTRIM(SA1->A1_XCANALD),Nil})
	aadd(aCabec,{"C5_XCODREG"	, ALLTRIM(SA1->A1_XCODREG),Nil})
	aadd(aCabec,{"C5_XDESREG"	, ALLTRIM(SA1->A1_XDESREG),Nil})
	aadd(aCabec,{"C5_XMICRRE"	, ALLTRIM(SA1->A1_XMICRRE),Nil})
	aadd(aCabec,{"C5_XMICRDE"	, ALLTRIM(SA1->A1_XMICRDE),Nil})
	//+ Robson _ Adicionado a pedido do Ronaldo, pois hoje muitos chamados geram demanda para ajuste deste campo.
	aadd(aCabec,{"C5_TIPOCLI"	, ALLTRIM(SA1->A1_TIPO),Nil})
	//+ Robson 
	
	aadd(aCabec,{"C5_PRAZO"  	, cCdPrz,Nil})
	aadd(aCabec,{"C5_XDPRAZO"   , cDescPrz,Nil})
	//aadd(aCabec,{"C5_VEND1"  	, cCodRep,Nil})
	aadd(aCabec,{"C5_COMIS1" 	, cPercRepre,Nil})
	aadd(aCabec,{"C5_VEND2"  	, cGerente,Nil})
	aadd(aCabec,{"C5_COMIS2" 	, cPercGer,Nil})
	aadd(aCabec,{"C5_VEND3"  	, cSuperv,Nil})
	aadd(aCabec,{"C5_COMIS3" 	, cPercSuper,Nil})
	aadd(aCabec,{"C5_XPEDCLI" 	, cOrdemCom,Nil})
	aadd(aCabec,{"C5_XINSTRU" 	, cInstru,Nil})
	
	If Alltrim(cPolitc) $ GETMV("HP_POLITM")
		aadd(aCabec,{"C5_FECENT" 	, STOD(cDataITM),Nil})
	Else
		If cCdPrz = '001'
			//aadd(aCabec,{"C5_FECENT" 	, dDtEntrega+1,Nil})//--Alterado pro Marcio Frade, nova tratativa para verificar se a data imediata ser� a data do processamento (solicitado por Rosemeire).
			aadd(aCabec,{"C5_FECENT" 	, STOD(cDataITM),Nil})
			
		Elseif cCdPrz = '002'
			//aadd(aCabec,{"C5_FECENT" 	, dDtEntrega+15,Nil})//--Alterado pro William, nova tratativa para verificar se a data programada � superior a regra da Hope.
			If STOD(cDataITM) >= (ddatabase+15)
				aadd(aCabec,{"C5_FECENT" 	, STOD(cDataITM),Nil})
			Else
				aadd(aCabec,{"C5_FECENT" 	, ddatabase+15,Nil})
			Endif
		Else
			//aadd(aCabec,{"C5_FECENT" 	, dDtEntrega,Nil})
			//---- Alterado por Marcio Frade onde ao inv�s de gravar a database, estaremos gravando a data do Pedido Interm�dia.
			aadd(aCabec,{"C5_FECENT" 	, STOD(cDataITM),Nil})
		Endif
	Endif
	aadd(aCabec,{"C5_ORIGEM" 	, 'ITM',Nil})
	
	// TODO ITENS
	_qry := "  SELECT A.CodCarrinho,A.CodProduto +''+ REPLICATE('0', 4 - LEN(CodDerivacao)) + RTrim(CodDerivacao)  AS SKU, SUM(B.Qtd) AS QTD, A.ValorUnitario FROM [IDB_MOBILIVENDAS].[dbo].[TBT_ITEM_CARRINHO] A "
	_qry += " JOIN [IDB_MOBILIVENDAS].[dbo].[TBT_GRADE_ITEM_CARRINHO] B ON A.CodCarrinho  = B.CodCarrinho AND A.CodItemCarrinho = B.CodItemCarrinho "
	_qry += " WHERE A.CodCarrinho = '"+cPedWeb+"' "
	_qry += " GROUP BY A.CodCarrinho,A.CodProduto , B.CodDerivacao, A.ValorUnitario  "
	
	
	If Select("TMPITEM1") > 0
		TMPITEM1->(dbclosearea())
	Endif
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPITEM1",.T.,.T.)
	
	DbSelectArea("TMPITEM1")
	DbGoTop()
	
	While TMPITEM1->(!EOF())
		
		cProd := Alltrim(TMPITEM1->SKU)
		nQtdPrd:= TMPITEM1->QTD
		//Chama rotina que faz a troca do produto Nacional para Importado
		aChkEstoq:= U_fTradeNacImp(cProd, nQtdPrd)
		
		If lQbrPed:= aChkEstoq[1]
			//Caso tenha sido feita a quebra de quantidade em produtos diferentes por falta de estoque
			cProd:= aChkEstoq[2]
			nQtdPrd:= aChkEstoq[3]
			cProd2:= aChkEstoq[4]
			nQtd2Prd:= aChkEstoq[5]
		Else
			If lSit := aChkEstoq[6]
				cProd:= aChkEstoq[2]
				nQtdPrd:= aChkEstoq[3]
			Endif
		Endif
		
		lQbrPed := .F.

		nQtPr:= Iif(lQbrPed,2,1)
		
		For nXX:= 1 to nQtPr
			
			If nXX > 1
				//Informa a quantidade e codigo do item 2 que foi dividido por falta de estoque do item 1
				cProd  := cProd2
				nQtdPrd:= nQtd2Prd
			Endif
			
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xFilial("SB1")+cProd)
			
			cDescri := GetAdvFVal("SB4","B4_DESC",xFilial("SB4")+SubStr(SB1->B1_COD,1,8),1,"")
			
			//TODO Ajuste de TES Inteligente - Ronaldo pereira 16/03/20
			cTES  := MaTesInt(2,cTpOper,cCodCli,cLojCli,"C",SB1->B1_COD,'',SA1->A1_TIPO)
			If alltrim(cTES) = ""
				cTES := "501"
			Endif
			
			nPrUnit	 := TMPITEM1->ValorUnitario
			nPrecVend:= TMPITEM1->ValorUnitario
			
			
			If !Empty(TMP->COD_TAB_PRECO)
				DbSelectArea("DA1")
				DbSetOrder(1)
				If DbSeek(xFilial("DA1")+ALLTRIM(TMP->COD_TAB_PRECO) + AllTrim(SB1->B1_COD))
					nPrUnit := DA1->DA1_PRCVEN
				Endif
			Endif
			
			
			If nPercDesc > 0
				nPrecVend := Round(TMPITEM1->ValorUnitario - ((nPercDesc/100)*TMPITEM1->ValorUnitario),2)
			Endif
			
			aLinha := {}
			aadd(aLinha,{"C6_ITEM"		, cItem,Nil})
			aadd(aLinha,{"C6_PRODUTO"	, AllTrim(SB1->B1_COD),Nil})
			aadd(aLinha,{"C6_QTDVEN"	, nQtdPrd,Nil})
			aadd(aLinha,{"C6_PRCVEN"	, nPrecVend,Nil})
			aadd(aLinha,{"C6_PRUNIT"	, nPrUnit,Nil})
			aadd(aLinha,{"C6_VALOR",    Round((nQtdPrd * nPrecVend),2),Nil})
			aadd(aLinha,{"C6_TES"		, cTES,Nil})
			aadd(aLinha,{"C6_LOCAL"		, "E0",Nil})
			If !Empty(cDescri)
				aadd(aLinha,{"C6_DESCRI",cDescri,Nil})
			Endif
			aadd(aLinha,{"C6_OPER"	   ,cTpOper,Nil})
			
			If Alltrim(cPolitc) $ GETMV("HP_POLITM")
				
				aadd(aLinha,{"C6_ENTREG" , STOD(cDataITM) ,Nil})
			Else
				If cCdPrz = '001'
					aadd(aLinha,{"C6_ENTREG" , dDatEntr ,Nil}) //Alterado por Marcio Frade - 	aadd(aLinha,{"C6_ENTREG" , dDatEntr+1 ,Nil})
				Elseif cCdPrz = '002'
					//aadd(aCabec,{"C5_FECENT" 	, dDtEntrega+15,Nil})//--Alterado pro William, nova tratativa para verificar se a data programada � superior a regra da Hope.
					If STOD(cDataITM) >= (ddatabase+15)
						aadd(aLinha,{"C6_ENTREG" 	, STOD(cDataITM),Nil})
					Else
						aadd(aLinha,{"C6_ENTREG" 	, ddatabase+15,Nil})
					Endif
				Else
					//aadd(aLinha,{"C6_ENTREG" , dDatEntr,Nil})
					//---- Alterado por Marcio Frade onde ao inv�s de gravar a database, estaremos gravando a data do Pedido Intermidia.
					aadd(aLinha,{"C6_ENTREG" , STOD(cDataITM),Nil})
				Endif
			Endif
			aadd(aItens,aLinha)
			
			cItem := SOMA1(cItem)
		Next nXX
		
		
		DbSelectArea("TMPITEM1")
		TMPITEM1->(DbSkip())
	EndDo
	
	BEGIN TRANSACTION
	
	lMsErroAuto := .F.
	
	// Troca a DataBase
	dDtAtu := dDataBase
	dDataBase := STOD(cDataEmi)
	lAutoErrNoFile:= .t.
	
	MATA410(aCabec,aItens,3)
	
	//Restaura a DatBase
	dDataBase := dDtAtu
	
	If lMsErroAuto
		
		//Pega o erro gerado no Execauto e joga em um array
		aErros 	:= {}
		aErros := GetAutoGRLog()
		
		//PEga a mensagem de erro do execauto e restaura a quebra de linhas
		cError:= StrTran( aErros[1], Chr(13)+Chr(10), " - " )
		
		//Retorna o pedido com Status 99 - Erro de Inclus�o de Pedido
		_qry1 := " UPDATE [IDB_MOBILIVENDAS].[dbo].[TBT_CARRINHO] SET CodSituacaoPedido = '99' , CodPedido = '"+cNroPed+"' WHERE  CodCarrinho = '"+cPedWeb+"' "  "
		TcSqlExec(_qry1)
		
		//Grava o retono do Execauto na tabela de criticas do ITM
		cSql2 := "INSERT INTO [IDB_MOBILIVENDAS].[dbo].[TBT_CRITICA_CARRINHO] (CodCarrinho,SeqCritica,Critica) VALUES "
		cSql2 += "('"+cPedWeb+"',(SELECT ISNULL(MAX(SeqCritica),0)+1 FROM [IDB_MOBILIVENDAS].[dbo].[TBT_CRITICA_CARRINHO] WHERE  CodCarrinho = '"+cPedWeb+"' ),'"+cERROR+"' )"
		TCSQLExec(cSql2)
		
	Else
		//Retorna para o ITM  o status 98 - pedido integrado
		_qry1 := " UPDATE [IDB_MOBILIVENDAS].[dbo].[TBT_CARRINHO] SET CodSituacaoPedido = '98' , CodPedido = '"+cNroPed+"' WHERE  CodCarrinho = '"+cPedWeb+"' "  "
		
		TcSqlExec(_qry1)
		
	Endif
	
	END TRANSACTION
	
	//TMP->(dbSkip())
//END
//END
Reset Environment
return

Static Function GravaLog(cMsg,cNome)

Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(date()) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cMsg)
cLog += CRLF+cDtHora+": HFATA013 - "+cMsg

Return
/*
Static Function fTradeNacImp(cProd, nQtdPrd)
Local cPrd1   := cProd
Local nQtdOrig   := nQtdPrd
Local cPrd2   := ""
Local nQtd2   := 0
Local aRet    := {}
Local lBlqDem := .f.
Local cPV	  := ""
Local lConEst := .t. //Consulta Estoque
Local aAreaSB1:= SB1->(GetArea())
Local aAreaSB2:= SB2->(GetArea())
Local aAreaZB5:= ZB5->(GetArea())
Local cCodSeq := ''
Local cCodPrio:= ''
Local cPriori := ''
Local cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local cAmzPul := AllTrim(SuperGetMV("MV_XAMZPUL",.F.,"E1")) //Armazem padrao pulmao

dbSelectArea("ZB5")
dbSetOrder(1)
ZB5->(dbGoTop())
//Verifica se existe priorizacao de produtos, caso nao exista o pedido segue igual.
If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
	cPriori:= ZB5->ZB5_PROIRI
	cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
	//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
	cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
Else
	dbSelectArea("ZB5")
	dbSetOrder(2)
	ZB5->(dbGoTop())
	If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
		cPriori:= ZB5->ZB5_PROIRI
		cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
		//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
		cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
		//Retorna pedido original pois nao existe priorizacao cadastrada na ZB5
	Else
		aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2 , .t.}
		RestArea(aAreaSB1)
		RestArea(aAreaSB2)
		Return (aRet)
	Endif
Endif

//Verifica aqui o Bloqueio por Demanda
cBlqDem:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YBLQDEM")
lBlqDem:= Iif(Alltrim(cBlqDem)== "S",.T.,.F.)

If !lBlqDem
	//Verifica aqui a Situacao do Produto onde:
	//PV|PNV - Nao consulta estoque, venda liberada
	//NPV|PVR|NPVO - Consulta estoque, venda so acontece com disponibilidade do produto
	
	cPVSec:= POSICIONE("SB1",1,xFilial("SB1")+cCodSeq,"B1_YSITUAC")
	cPV:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YSITUAC")
	
	lConEst:= Iif(Alltrim(cPV)== "PV" .OR. Alltrim(cPV) == "PNV" .or. Alltrim(cPV) == "PVFO"  ,.F.,.T.)
	
	If lConEst
		dbSelectArea("SB2")
		dbSetOrder(1)
		SB2->(dbGoTop())
		
		nSldPik := HFASALDO(cCodPrio,cAmzPik)
		nSldPul := HFASALDO(cCodPrio,cAmzPul)
		
		nSldSecPik := HFASALDO(cCodSeq,cAmzPik)
		nSldSecPul := HFASALDO(cCodSeq,cAmzPul)
		
		If nQtdOrig<= nSldPik+nSldPul
			aRet:= { .F., cCodPrio, nQtdOrig, cPrd2, nQtd2 ,.T.}
		Else
			nPriQatu:= nSldPik + nSldPul
			nSecQatu:= nSldSecPik + nSldSecPul
			nSldAtu := nQtdOrig - nPriQatu
			
			If Alltrim(cPVSec) == "PV" .OR. Alltrim(cPVSec) == "PNV" .OR. Alltrim(cPVSec) == "PVFO"
				aRet:= { .T., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .T. }
			Else
				If nSecQatu <= nSldAtu
					//nQtd2:= nSecQatu - nSldAtu
					aRet:= { .T., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .F. }
				Else
					cPvSeq := POSICIONE("SB1",1,xFilial("SB1")+cCodSeq,"B1_YSITUAC") 
					if Alltrim(cPvSeq) == "PV" .OR.  Alltrim(cPvSeq) == "PNV" .OR. Alltrim(cPvSeq) == "PVFO" 
						aRet:= { .T., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .T. } 
					else	 
						aRet:= { .F., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .T. } 
					endif	
					RestArea(aAreaSB1)
					RestArea(aAreaSB2)
					RestArea(aAreaZB5)
					Return(aRet)
					//Produtos (Nac+Imp) juntos nao contem a quantidade total necessaria do pedido.
				Endif
			Endif
		Endif
	Else
		//Retornar aqui quantidade original + produto priorizado
		//If !empty(cCodSeq)
		//	aRet:= { .T., cCodPrio, nPriQatu, cCodSeq, nSldAtu, .T. }
		//Else
		aRet:= { .F., cCodPrio, nQtdOrig, cPrd2, nQtd2 ,.T.}
		//Endif
	Endif
Else
	Conout('Produto: '+cCodPrio+' com bloqueio de demanda.')
	aRet:= { .F., cPrd1, nQtdOrig, cPrd2, nQtd2, .T. }
	//Retornar aqui pedido original - o pedido nao podera ser feito por conta do bloqueio por demanda
Endif


RestArea(aAreaSB1)
RestArea(aAreaSB2)
RestArea(aAreaZB5)

Return (aRet)
*/

Static Function HFASALDO(cProduto,cArmazem)
Local nRet := 0

DbSelectArea("SB2")
DbSetOrder(1)
If SB2->(DbSeek(xFilial("SB2")+cProduto+cArmazem))
	nRet := SB2->B2_QATU-SB2->B2_RESERVA-SB2->B2_QEMP-SB2->B2_XRESERV //SaldoSb2()
Endif

Return nRet