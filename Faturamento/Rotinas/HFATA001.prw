#Include 'Protheus.ch'
#Include 'FwMVCDef.ch'
#INCLUDE "TBICONN.CH"
 
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA001  �Autor  �Bruno Parreira      � Data �  08/11/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa para geracao dos pedidos de venda de distribuicao  ���
���          �de material do departamento de marketing                    ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATA001()
Local _astru   := {}
Local _afields := {}     
Local _carq    := ""         
Local cQuery   := ""
Private lMarcar  := .F.

Private oMark
Private cPerg     := "HFATA001"
Private aRotina   := {} 
Private lContinua := .F.
Private cMark     := GetMark() 

AjustaSX1(cPerg)

If !Pergunte(cPerg,.T.)
	Return
EndIf

cQuery := "select D1_DOC,D1_SERIE,D1_ITEM,D1_COD,B1_DESC,D1_QUANT,D1_VUNIT,D1_TOTAL "
cQuery += CRLF + "from "+RetSqlName("SD1")+" SD1 "
cQuery += CRLF + "inner join "+RetSqlName("SB1")+" SB1 " 
cQuery += CRLF + "on B1_COD = D1_COD "
cQuery += CRLF + "and SB1.D_E_L_E_T_   = '' "
cQuery += CRLF + "and B1_FILIAL        = '"+xFilial("SB1")+"' "
cQuery += CRLF + "where SD1.D_E_L_E_T_ = '' "
cQuery += CRLF + "and D1_FILIAL  = '"+cFilAnt+"' "         
cQuery += CRLF + "and D1_DOC     between '"+mv_par01+"' and '"+mv_par03+"' "
cQuery += CRLF + "and D1_SERIE   between '"+mv_par02+"' and '"+mv_par04+"' "
cQuery += CRLF + "and D1_FORNECE between '"+mv_par05+"' and '"+mv_par07+"' "
cQuery += CRLF + "and D1_LOJA    between '"+mv_par06+"' and '"+mv_par08+"' "
cQuery += CRLF + "and D1_COD     between '"+mv_par09+"' and '"+mv_par10+"' "
cQuery += CRLF + "and D1_EMISSAO between '"+DtoS(mv_par11)+"' and '"+DtoS(mv_par12)+"' "
cQuery += CRLF + "order by D1_DOC,D1_SERIE,D1_ITEM "
    
MemoWrite("HFATA001_1.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)       
                         
//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"   ,"C",2,0})
AADD(_astru,{"D1_DOC"  ,"C",9,0})
AADD(_astru,{"D1_SERIE","C",3,0})
AADD(_astru,{"D1_ITEM" ,"C",4,0})
AADD(_astru,{"D1_COD"  ,"C",15,0})
AADD(_astru,{"B1_DESC" ,"C",40,0})
AADD(_astru,{"D1_QUANT","N",16,6})
AADD(_astru,{"D1_VUNIT","N",14,6})
AADD(_astru,{"D1_TOTAL","N",14,2})

cArqTrab  := CriaTrab(_astru)
dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )

//Atribui a tabela temporaria ao alias TRB
Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		DbSelectArea("TRB")        
		RecLock("TRB",.T.) 
		TRB->D1_DOC   := TMP->D1_DOC                
		TRB->D1_SERIE := TMP->D1_SERIE
		TRB->D1_ITEM  := TMP->D1_ITEM
		TRB->D1_COD   := TMP->D1_COD
		TRB->B1_DESC  := TMP->B1_DESC
		TRB->D1_QUANT := TMP->D1_QUANT
		TRB->D1_VUNIT := TMP->D1_VUNIT
		TRB->D1_TOTAL := TMP->D1_TOTAL
		MsUnlock()        
		TMP->(DbSkip())
	EndDo
EndIf	
     
//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('TRB')        

//define as colunas para o browse
aColunas := {;
{"Nota Fiscal","D1_DOC"  ,"C",9,0,"@!"},;
{"Serie"      ,"D1_SERIE","C",3,0,"@!"},;
{"Item"       ,"D1_ITEM" ,"C",4,0,"@!"},;
{"Produto"    ,"D1_COD"  ,"C",15,0,"@!"},;
{"Descricao"  ,"B1_DESC" ,"C",40,0,"@!"},;
{"Qtde."      ,"D1_QUANT","N",16,6,"@E 99,999,999.999999"},;
{"Vlr. Unit." ,"D1_VUNIT","N",14,6,"@E 99,999,999.999999"},;
{"Vlr. Total" ,"D1_TOTAL","N",14,2,"@E 999,999,999.99"}}

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.F.)
oMark:SetDescription('Pedidos MKT')
oMark:SetFieldMark('MK_OK')
                              
oMark:AddButton("Selecionar Canais","U_HFAT01PROX(oMark,1)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_HFAT01INVE(oMark:Mark())",,2,0)
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
oMark:SetAllMark({ || oMark:MarkRec()})

oMark:SetTemporary()

//Ativando a janela
oMark:Activate()
     
aItensNF := {}
aItensNF := HFAT01NOTA(oMark)    //Monta Array com Notas Selecionadas
    
//Limpar o arquivo tempor�rio
If !Empty(cArqTrab)
    Ferase(cArqTrab+GetDBExtension())
    Ferase(cArqTrab+OrdBagExt())
    cArqTrab := ""
    TRB->(DbCloseArea())
Endif

TMP->(DbCloseArea())    

If lContinua	
	HFAT01CANA(oMark)         //Monta tela para selecionar os canais
	aCanais := {}
	aCanais := HFAT01SLCN(oMark)    //Monta Array com Canais Selecionados
	
	If !Empty(cArqTrab)
	    Ferase(cArqTrab+GetDBExtension())
	    Ferase(cArqTrab+OrdBagExt())
	    cArqTrab := ""
	    TRB->(DbCloseArea())
	Endif
	
	TMP->(DbCloseArea())
	
	If lContinua
		HFAT01CLIE(oMark,aCanais)   //Monta tela para selecionar os clientes dos canais selecionados 
		aClientes := {}
		aClientes := HFAT01SLCL(oMark)   //Monta Array com Clientes Selecionados
		
		If !Empty(cArqTrab)
		    Ferase(cArqTrab+GetDBExtension())
		    Ferase(cArqTrab+OrdBagExt())
		    cArqTrab := ""
		    TRB->(DbCloseArea())
		Endif
		
		TMP->(DbCloseArea())
		
		If lContinua
			HFAT01PEDI(oMark,aItensNF,aClientes)   //Criacao dos pedidos
		EndIf
	EndIf 
EndIf 

Return Nil

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01NOTA�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria array contendo os itens das NFs selecionados.          ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFAT01NOTA(oMark)
Local aRet := {} 
Local aAreaTRB := GetArea()                 

TRB->(DbGoTop())
While TRB->(!Eof())
    If !Empty(TRB->MK_OK) //Se diferente de vazio, foi marcado
        aAdd(aRet,{TRB->D1_COD,TRB->D1_QUANT,TRB->D1_VUNIT,TRB->D1_DOC,TRB->D1_SERIE,TRB->D1_ITEM})
    Endif
    TRB->( dbSkip() )
EndDo

RestArea(aAreaTRB)

Return aRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01SLCN�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria array contendo os canais selecionados.                 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

Static Function HFAT01SLCN(oMark)
Local aRet := {} 
Local aAreaTRB := GetArea()                 

TRB->(DbGoTop())
While TRB->(!Eof())
    If !Empty(TRB->MK_OK) //Se diferente de vazio, foi marcado
        aAdd(aRet,{TRB->ZA3_CODIGO,TRB->ZA3_DESCRICAO})
    Endif
    TRB->( dbSkip() )
EndDo

RestArea(aAreaTRB)

Return aRet      

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01SLCL�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria array contendo os clientes selecionados.               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFAT01SLCL(oMark)
Local aRet := {} 
Local aAreaTRB := GetArea()                 

TRB->(DbGoTop())
While TRB->(!Eof())
    If !Empty(TRB->MK_OK) //Se diferente de vazio, foi marcado
        aAdd(aRet,{TRB->A1_COD,TRB->A1_LOJA})
    Endif
    TRB->( dbSkip() )
EndDo

RestArea(aAreaTRB)

Return aRet 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01INVE�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Marca ou desmarca todos os itens da FWMarkBrowse.           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFAT01INVE(cMarca)
Local aAreaTRB  := TRB->(GetArea())

lMarcar := !lMarcar 
 
dbSelectArea("TRB")
TRB->(dbGoTop())
While !TRB->(Eof())
    RecLock("TRB",.F.)
    TRB->MK_OK := IIf(lMarcar,cMarca,'  ')
    MsUnlock()
    TRB->(dbSkip())
EndDo

RestArea( aAreaTRB )

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01PROX�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Funcao para controlar a transicao de uma tela para outra.   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

User Function HFAT01PROX(oMark,nTela)
Local nCont := 0 
Local aAreaTRB := GetArea()                 

TRB->(DbGoTop())
While TRB->(!Eof())
    If !Empty(TRB->MK_OK) //Se diferente de vazio, foi marcado
        nCont++
    Endif
    TRB->( dbSkip() )
EndDo
   	
If nCont == 0
    Alert("Selecione pelo menos um registro.")
    RestArea(aAreaTRB)
    lContinua := .F.
    Return
Endif

If nTela = 3
	If !MsgYesNo("Confirma a gera��o dos pedidos de venda para os clientes selecionados?","Confirma��o")
		RestArea(aAreaTRB)
    	lContinua := .F.
    	Return
    EndIf
EndIf

CloseBrowse()
oMark:DeActivate()

lContinua := .T.       

RestArea(aAreaTRB)

Return  

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01CANA�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta a tela para selecao dos canais de venda.              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFAT01CANA(oMark)
Local _astru   := {}
Local _afields := {}     
Local _carq    := ""         
Local cQuery   := ""

lContinua := .F.

lMarcar := .F.   

cQuery := "select ZA3_CODIGO,ZA3_DESCRI "
cQuery += CRLF + "from "+RetSqlName("ZA3")+" ZA3 "
cQuery += CRLF + "where ZA3.D_E_L_E_T_ = '' " 
cQuery += CRLF + "and ZA3_FILIAL = '"+xFilial("ZA3")+"' "
cQuery += CRLF + "order by ZA3_CODIGO "
    
MemoWrite("HFATA001_2.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)       
                         
//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"     ,"C", 2,0})
AADD(_astru,{"ZA3_CODIGO","C", 3,0})
AADD(_astru,{"ZA3_DESCRI","C",30,0})

cArqTrab  := CriaTrab(_astru)
dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )

Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		DbSelectArea("TRB")        
		RecLock("TRB",.T.) 
		TRB->ZA3_CODIGO := TMP->ZA3_CODIGO                
		TRB->ZA3_DESCRI := TMP->ZA3_DESCRI
		MsUnlock()        
		TMP->(DbSkip())
	EndDo
EndIf	
     
//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('TRB')        

//define as colunas para o browse
aColunas := {;
{"Codigo"   ,"ZA3_CODIGO","C",3 ,0,"@!"},;
{"Descricao","ZA3_DESCRI","C",30,0,"@!"}}

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.T.)
oMark:SetDescription('Seleciona Canais')    
oMark:SetFieldMark('MK_OK')
                              
oMark:AddButton("Selecionar Clientes","U_HFAT01PROX(oMark,2)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_HFAT01INVE(oMark:Mark())",,2,0)
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || HFAT01INVE(oMark:Mark(),lMarcar := !lMarcar ),oMark:Refresh(.T.)})

oMark:SetTemporary()
   
//Ativando a janela
oMark:Activate() 
                                                    
Return Nil 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01CLIE�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta a tela para selecao dos clientes.                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFAT01CLIE(oMark,aCanais)
Local _astru   := {}
Local _afields := {}     
Local _carq    := ""         
Local cQuery   := ""  
Local cCanais  := ""
Local nx := 0

lContinua := .F. 

lMarcar := .F.

For nx := 1 to Len(aCanais)
	cCanais += "'"+aCanais[nx][1]+"',"
Next                                   

cCanais := SubStr(cCanais,1,Len(cCanais)-1)

cQuery := "select A1_COD,A1_LOJA,A1_CGC,A1_NOME,A1_NREDUZ "
cQuery += CRLF + "from "+RetSqlName("SA1")+" SA1 "
cQuery += CRLF + "where SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "and A1_FILIAL = '"+xFilial("SA1")+"' "
cQuery += CRLF + "and A1_XCANAL IN ("+cCanais+") "
cQuery += CRLF + "and A1_MSBLQL <> '1' "
cQuery += CRLF + "order by A1_COD,A1_LOJA "
    
MemoWrite("HFATA001_3.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)       
                         
//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"     ,"C", 2,0})
AADD(_astru,{"A1_COD"    ,"C", 6,0})
AADD(_astru,{"A1_LOJA"   ,"C", 4,0})
AADD(_astru,{"A1_CGC"    ,"C",18,0})
AADD(_astru,{"A1_NOME"   ,"C",40,0}) 
AADD(_astru,{"A1_NREDUZ" ,"C",40,0})

cArqTrab  := CriaTrab(_astru)
dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )

Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		DbSelectArea("TRB")        
		RecLock("TRB",.T.) 
		TRB->A1_COD    := TMP->A1_COD                
		TRB->A1_LOJA   := TMP->A1_LOJA 
		TRB->A1_CGC    := TMP->A1_CGC                
		TRB->A1_NOME   := TMP->A1_NOME
		TRB->A1_NREDUZ := TMP->A1_NREDUZ 
		MsUnlock()        
		TMP->(DbSkip())
	EndDo
EndIf	
     
//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('TRB')        

//define as colunas para o browse
aColunas := {;
{"Cliente"      ,"A1_COD"   ,"C",6 ,0,"@!"},; 
{"Loja"         ,"A1_LOJA"  ,"C",4 ,0,"@!"},;
{"CNPJ"         ,"A1_CGC"   ,"C",18,0,"@!"},;
{"Razao Social" ,"A1_NOME"  ,"C",40,0,"@!"},;
{"Nome Fantasia","A1_NREDUZ","C",40,0,"@!"}}

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.T.)
oMark:SetDescription('Seleciona Clientes')    
oMark:SetFieldMark('MK_OK')
                              
oMark:AddButton("Gera Pedidos","U_HFAT01PROX(oMark,3)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_HFAT01INVE(oMark:Mark())",,2,0)
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || HFAT01INVE(oMark:Mark(),lMarcar := !lMarcar ),oMark:Refresh(.T.)})

oMark:SetTemporary()
   
//Ativando a janela
oMark:Activate() 
                                                    
Return Nil 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFAT01CLIE�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Gera PVs para os clientes selecionados.                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFAT01PEDI(oMark,aItensNF,aClientes)
Local nCont     := 0 
Local nx,ny     := 0               
Local cPrd      := ""
Local aAux      := {}
Local nSoma     := 0
Local nQuant    := 0
Local nVunit    := 0
Local cNroPed   := "" 
Local cNumPVde  := "" 
Local cNumPVate := ""
Local cNatureza := "" 
Local cTES      := ""
Local cUM       := ""
Local aCabPV    := {}
Local aItemPV   := {}
Local aErro     := {}
Local aGerados  := {}
Local nErro     := 0
Local nGerados  := 0   
Local i
PRIVATE lMsErroAuto := .F.
     
//Agrupa os itens selecionados somando as qtdes de produtos comuns e ja divide pela qtde de clientes selecionados.

aSort(aItensNF)
cPrd  := aItensNF[1][1]    //Cod. Produto

nCont := Len(aClientes)

For nx := 1 to Len(aItensNF)
	nQuant := aItensNF[nx][2] //Qtde. do Item
	If cPrd <> aItensNF[nx][1]
		aAdd(aAux,{cPrd,nSoma/nCont,nVunit})
		nSoma := nQuant
	Else
	    nSoma += nQuant
	EndIf
	cPrd   := aItensNF[nx][1]
	nVunit := aItensNF[nx][3]	 //Valor Unitario do Item
Next 
aAdd(aAux,{cPrd,nSoma/nCont,nVunit}) 

ProcRegua(Len(aClientes))

If !Empty(mv_par14)
	cNatureza := mv_par14
Else
	cNatureza := "21314305" //Natureza de Material Promocional Mkt
EndIf 

cNumPVde := GetMV("MV_XNMPVMK")	

For nx := 1 to Len(aClientes)
    IncProc()  
     
    DbSelectArea("SC5")
    
    cNroPed := GetMV("MV_XNMPVMK")
    
    aCabec := {}
    aadd(aCabec,{"C5_NUM"   ,cNroPed,Nil})
	aadd(aCabec,{"C5_TIPO"   ,"N",Nil})
	aadd(aCabec,{"C5_CLIENTE",aClientes[nx][1],Nil})		
	aadd(aCabec,{"C5_LOJACLI",aClientes[nx][2],Nil})		
	aadd(aCabec,{"C5_LOJAENT",aClientes[nx][2],Nil})		
	aadd(aCabec,{"C5_POLCOM" ,MV_PAR15,Nil})
	aadd(aCabec,{"C5_TPPED"  ,MV_PAR17,Nil})
	aadd(aCabec,{"C5_CONDPAG",MV_PAR16,Nil})
	aadd(aCabec,{"C5_NATUREZ",cNatureza,Nil})
	    
	aItens := {}
	
    For ny := 1 to Len(aAux)
    	
    	DbSelectArea("SB1")
    	DbSeek(xFilial("SB1")+aAux[ny][1])
    	
    	DbSelectArea("SF4")
    	If DbSeek(xFilial("SF4")+IF(!EMPTY(MV_PAR13),MV_PAR13,"501"))
    		cTES := SF4->F4_CODIGO
    	Else
    		cTES := "501"
    	EndIf

		aLinha := {}
		aadd(aLinha,{"C6_ITEM",StrZero(nY,2),Nil})			
		aadd(aLinha,{"C6_PRODUTO",SB1->B1_COD,Nil})			
		aadd(aLinha,{"C6_QTDVEN",aAux[ny][2],Nil})			
		aadd(aLinha,{"C6_PRCVEN",aAux[ny][3],Nil})			
		aadd(aLinha,{"C6_PRUNIT",aAux[ny][3],Nil})			
		aadd(aLinha,{"C6_VALOR",ROUND(aAux[ny][2]*aAux[ny][3],2),Nil})			
		aadd(aLinha,{"C6_TES",SF4->F4_CODIGO,Nil})
		aadd(aLinha,{"C6_NUM",cNroPed,Nil})
		aadd(aItens,aLinha)		
    Next
    
    BEGIN TRANSACTION

	MATA410(aCabec,aItens,3)
		
	If lMsErroAuto
		MostraErro()
		nErro++
	Else 
		nGerados++
		cNumPVate := cNroPed
		cNroPed := SOMA1(SubStr(cNroPed,3,4))
		PutMV("MV_XNMPVMK","MK"+cNroPed)	
	EndIf
	
	END TRANSACTION	
        
Next  

If nErro > 0
	MsgInfo("Encontrado erro na gera��o de "+StrZero(nErro,4)+" pedidos.","Aviso")
EndIf

If nGerados > 0	
	MsgInfo("Foram gerados "+StrZero(nGerados,4)+" pedidos com sucesso. N�: "+cNumPVde+" ao "+cNumPVAte,"Aviso")
EndIf

CloseBrowse()	

Return 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )
  
PutSx1(cPerg,"01","Da Nota Fiscal ?    ","Da Nota Fiscal ?    ","Da Nota Fiscal ?    ","Mv_ch1",TAMSX3("D1_DOC"    )[3],TAMSX3("D1_DOC"    )[1],TAMSX3("D1_DOC"    )[2],0,"G","","SF1XSR","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o numero da nota fiscal.        ",""},{""},{""},"")
PutSx1(cPerg,"02","Da S�rie ?          ","Da S�rie ?          ","Da S�rie ?          ","Mv_ch2",TAMSX3("D1_SERIE"  )[3],TAMSX3("D1_SERIE"  )[1],TAMSX3("D1_SERIE"  )[2],0,"G","",      "","","N","Mv_par02","","","","","","","","","","","","","","","","",{"Informe a s�rie da nota fiscal.         ",""},{""},{""},"")
PutSx1(cPerg,"03","At� Nota Fiscal ?   ","At� Nota Fiscal ?   ","At� Nota Fiscal ?   ","Mv_ch3",TAMSX3("D1_DOC"    )[3],TAMSX3("D1_DOC"    )[1],TAMSX3("D1_DOC"    )[2],0,"G","","SF1XSR","","N","Mv_par03","","","","","","","","","","","","","","","","",{"Informe o numero da nota fiscal.        ",""},{""},{""},"")
PutSx1(cPerg,"04","At� S�rie ?         ","At� S�rie ?         ","At� S�rie ?         ","Mv_ch4",TAMSX3("D1_SERIE"  )[3],TAMSX3("D1_SERIE"  )[1],TAMSX3("D1_SERIE"  )[2],0,"G","",      "","","N","Mv_par04","","","","","","","","","","","","","","","","",{"Informe a s�rie da nota fiscal.         ",""},{""},{""},"")
PutSx1(cPerg,"05","De Fornecedor ?     ","De Fornecedor ?     ","De Fornecedor ?     ","Mv_ch5",TAMSX3("D1_FORNECE")[3],TAMSX3("D1_FORNECE")[1],TAMSX3("D1_FORNECE")[2],0,"G","",  "SA2A","","N","Mv_par05","","","","","","","","","","","","","","","","",{"Informe o c�digo do fornecedor.         ",""},{""},{""},"")
PutSx1(cPerg,"06","De Loja ?           ","De Loja ?           ","De Loja ?           ","Mv_ch6",TAMSX3("D1_LOJA"   )[3],TAMSX3("D1_LOJA"   )[1],TAMSX3("D1_LOJA"   )[2],0,"G","",      "","","N","Mv_par06","","","","","","","","","","","","","","","","",{"Informe a loja do fornecedor.           ",""},{""},{""},"") 
PutSx1(cPerg,"07","At� Fornecedor ?    ","At� Fornecedor ?    ","At� Fornecedor ?    ","Mv_ch7",TAMSX3("D1_FORNECE")[3],TAMSX3("D1_FORNECE")[1],TAMSX3("D1_FORNECE")[2],0,"G","",  "SA2A","","N","Mv_par07","","","","","","","","","","","","","","","","",{"Informe o c�digo do fornecedor.         ",""},{""},{""},"")
PutSx1(cPerg,"08","At� Loja ?          ","At� Loja ?          ","At� Loja ?          ","Mv_ch8",TAMSX3("D1_LOJA"   )[3],TAMSX3("D1_LOJA"   )[1],TAMSX3("D1_LOJA"   )[2],0,"G","",      "","","N","Mv_par08","","","","","","","","","","","","","","","","",{"Informe a loja do fornecedor.           ",""},{""},{""},"") 
PutSx1(cPerg,"09","Do Produto ?        ","Do Produto ?        ","Do Produto ?        ","Mv_ch9",TAMSX3("D1_COD"    )[3],TAMSX3("D1_COD"    )[1],TAMSX3("D1_COD"    )[2],0,"G","",   "SB1","","N","Mv_par09","","","","","","","","","","","","","","","","",{"Informe o c�digo do produto.            ",""},{""},{""},"")
PutSx1(cPerg,"10","At� Produto ?       ","At� Produto ?       ","At� Produto ?       ","Mv_chA",TAMSX3("D1_COD"    )[3],TAMSX3("D1_COD"    )[1],TAMSX3("D1_COD"    )[2],0,"G","",   "SB1","","N","Mv_par10","","","","","","","","","","","","","","","","",{"Informe o c�digo do produto.            ",""},{""},{""},"")
PutSx1(cPerg,"11","Da Emiss�o ?        ","Da Emiss�o ?        ","Da Emiss�o ?        ","Mv_chB",TAMSX3("D1_EMISSAO")[3],TAMSX3("D1_EMISSAO")[1],TAMSX3("D1_EMISSAO")[2],0,"G","",      "","","N","Mv_par11","","","","","","","","","","","","","","","","",{"Informe a data de emiss�o.              ",""},{""},{""},"")
PutSx1(cPerg,"12","At� Emiss�o ?       ","At� Emiss�o ?       ","At� Emiss�o ?       ","Mv_chC",TAMSX3("D1_EMISSAO")[3],TAMSX3("D1_EMISSAO")[1],TAMSX3("D1_EMISSAO")[2],0,"G","",      "","","N","Mv_par12","","","","","","","","","","","","","","","","",{"Informe a data de emiss�o.              ",""},{""},{""},"")
PutSx1(cPerg,"13","TES ?               ","TES ?               ","TES ?               ","Mv_chD",TAMSX3("D1_TES"    )[3],TAMSX3("D1_TES"    )[1],TAMSX3("D1_TES"    )[2],0,"G","",   "SF4","","N","Mv_par13","","","","","","","","","","","","","","","","",{"Informe a TES dos pedidos de venda.     ",""},{""},{""},"")
PutSx1(cPerg,"14","Natureza ?          ","Natureza ?          ","Natureza ?          ","Mv_chE",TAMSX3("ED_CODIGO" )[3],TAMSX3("ED_CODIGO" )[1],TAMSX3("ED_CODIGO" )[2],0,"G","",   "SED","","N","Mv_par14","","","","","","","","","","","","","","","","",{"Informe a natureza dos pedidos de venda.",""},{""},{""},"")
PutSx1(cPerg,"15","Politica Comercial ?","Politica Comercial ?","Politica Comercial ?","Mv_chF",TAMSX3("C5_POLCOM" )[3],TAMSX3("C5_POLCOM" )[1],TAMSX3("C5_POLCOM" )[2],0,"G","",   "SZ2","","N","Mv_par15","","","","","","","","","","","","","","","","",{"Informe a Pol. Com. dos pedidos de venda",""},{""},{""},"")
PutSx1(cPerg,"16","Cond. Pgto. ?       ","Cond. Pgto. ?       ","Cond. Pgto. ?       ","Mv_chG",TAMSX3("C5_CONDPAG")[3],TAMSX3("C5_CONDPAG")[1],TAMSX3("C5_CONDPAG")[2],0,"G","", "SZ3_2","","N","Mv_par16","","","","","","","","","","","","","","","","",{"Informe a Cond Pag. dos pedidos de venda",""},{""},{""},"")
PutSx1(cPerg,"17","Tipo Pedido ?       ","Tipo Pedido ?       ","Tipo Pedido ?       ","Mv_chH",TAMSX3("C5_TPPED"  )[3],TAMSX3("C5_TPPED"  )[1],TAMSX3("C5_TPPED"  )[2],0,"G","", "SZ4_2","","N","Mv_par17","","","","","","","","","","","","","","","","",{"Informe o Tipo dos pedidos de venda.    ",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)