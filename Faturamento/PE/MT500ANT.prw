#INCLUDE "protheus.ch" 
#INCLUDE "rwmake.ch" 

#define DS_MODALFRAME   128

User Function MT500ANT()
Local aArea := GetArea()
Local lRet := .T.
Local cMotivo := CriaVar("C5_XMOTRES",.T.)

Private lMotivo := .F.
Private oDlg
Private oFont1  := TFont():New("Arial",,18,,.F.,,,,,.F.)

DbSelectArea("SC5")
DbSetOrder(1)
If DbSeek(xFilial("SC5")+SC6->C6_NUM)
	If Empty(SC5->C5_XMOTRES)
		While !lMotivo
		
			DEFINE MSDIALOG oDlg TITLE "Motivo Elimina��o de Res�duo" FROM 000,000 TO 130,500 PIXEL Style DS_MODALFRAME
		
			@005,005 TO 045,250 OF oDlg PIXEL
			@008,010 SAY "Pedido:"             SIZE 020,007 OF oDlg PIXEL
	    	@007,035 SAY SC6->C6_NUM           SIZE 025,010 OF oDlg FONT oFont1 PIXEL
	    	@008,075 SAY "Usu�rio:"            SIZE 020,007 OF oDlg PIXEL
	    	@007,100 SAY UsrRetName(__cUserId) SIZE 070,010 OF oDlg FONT oFont1 PIXEL
			@020,010 SAY "Motivo da Elimina��o de Res�duo:" SIZE 120,007 OF oDlg PIXEL 
			@028,010 MSGET cMotivo SIZE 235,011 OF oDlg PIXEL VALID ValidMot(cMotivo,)
		
			DEFINE SBUTTON FROM 050,223 TYPE 1 ACTION (GravaMot(SC5->C5_NUM,cMotivo),oDlg:End()) ENABLE OF oDlg
		
			ACTIVATE MSDIALOG oDlg CENTERED

		EndDo
	EndIf
EndIf

RestArea(aArea)

Return lRet

Static Function ValidMot(cTexto)
Local lRet := .T.

If Empty(cTexto)
	MsgAlert("Favor digitar um motivo para a elimina��o de res�duo.","ATEN��O")
	lRet := .F.
EndIf

Return lRet

Static Function GravaMot(cPedido,cTexto)
Local lRet := .T.

If Empty(cTexto)
	MsgAlert("Favor digitar um motivo para a elimina��o de res�duo.","ATEN��O")
	Return .F.
Else
	RecLock("SC5",.F.)
	SC5->C5_XMOTRES := cTexto
	SC5->C5_XUSERCA := UsrRetName(__cUserId)
	MsUnlock()
	lMotivo := .T.
EndIf

Return lRet