#include 'totvs.ch'
#include "topconn.ch"
#Include "PROTHEUS.CH"
#include "TbiConn.ch"
#include "TOTVS.ch"

User Function MTA410T()
Local _i
Local lRet		:= .T.
Local cMsg    	:= ""				// Mensagem de alerta
Local cEOL		:= +Chr(13)+Chr(10)
_area := GetArea()

//query para selecionar a regra de liberacao de pedidos de venda 
_qry := " SELECT TOP 1 * FROM "+RetSqlName("SZU")+" ZSU WITH (NOLOCK)"+cEOL
_qry += " WHERE D_E_L_E_T_ <> '*' ORDER BY ZU_DATA DESC,ZU_CODIGO DESC "+cEOL

If Select("TMPSZU") > 0 
	TMPSZU->(DbCloseArea()) 
EndIf 

dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZU",.T.,.T.)

_qry2 := " SELECT SUBSTRING(C5_XALTPED,1,6) AS CLIOLD, SUBSTRING(C5_XALTPED,8,3) AS POLOLD, SUBSTRING(C5_XALTPED,12,3) AS CONDOLD, "+cEOL
_qry2 += " 		SUBSTRING(C5_XALTPED,16,3) AS TABOLD, SUBSTRING(C5_XALTPED,20,3) AS TPPEDOLD, SUBSTRING(C5_XALTPED,24,1) AS ALTOBS "+cEOL
_qry2 += " FROM "+RetSqlName("SC5")+" SC5 WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' "+cEOL
_qry2 += " 		AND C5_FILIAL = '"+M->C5_FILIAL+"' AND C5_NUM = '"+M->C5_NUM+"' "+cEOL

If Select("TMPSC5OLD") > 0 
	TMPSC5OLD->(DbCloseArea()) 
EndIf

dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry2),"TMPSC5OLD",.T.,.T.)

If AllTrim(TMPSC5OLD->CLIOLD) <> ""
	If M->C5_XBLQ == "B"
		lRet	:= .F.
	Else
		lRet	:= .T.
	EndIf
EndIf
 
_qry3 := " SELECT TOP 1 * FROM "+RetSqlName("SC5")+" WITH(NOLOCK)  WHERE C5_XPEDWEB  ='"+ Alltrim(M->C5_XPEDWEB) +"' AND D_E_L_E_T_ = '' ORDER BY R_E_C_N_O_  "
 
If Select("TMP") > 0 
	TMP->(DbCloseArea()) 
EndIf

dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry3),"TMP",.T.,.T.)

IF TMP->C5_ORIGEM != "MANUAL" 
 	IF TMP->C5_NUM != M->C5_NUM 	
 		_qry4 := " UPDATE SC5010 SET C5_XPEDWEB = '', C5_XPEDRAK = ''  WHERE C5_NUM = '"+M->C5_NUM+"' AND C5_XPEDWEB = '"+M->C5_XPEDWEB+"' AND D_E_L_E_T_ = '' "
 		TcSqlExec(_qry4)	
 	ENDIF
 ENDIF
 
 TMP->(DbCloseArea())
 
	IF M->C5_TPPED = '027'
		_qry5 := " UPDATE SC6010 SET C6_LOCAL = 'TR' , C6_LOCALIZ = 'DEV' WHERE C6_NUM = '"+M->C5_NUM+"' AND C6_FILIAL = '"+xfilial("SC6")+"' AND D_E_L_E_T_ = '' AND C6_CLI = '"+M->C5_CLIENT+"' "
		TcSqlExec(_qry5) 
	ENDIF

  if !EMPTY(M->C5_FECENT)	 
	_qry2 := " UPDATE SC6010 SET C6_ENTREG = '"+ DTOS(M->C5_FECENT) +"'  WHERE D_E_L_E_T_ = '' AND C6_FILIAL ='"+xfilial("SC6")+"' AND C6_NUM ='"+M->C5_NUM+"' AND C6_CLI = '"+M->C5_CLIENTE+"' AND C6_LOJA = '"+M->C5_LOJACLI+"'  "
	TcSqlExec(_qry2)
  endif
				
		
/*  
Legenda de Bloqueios
"BC"	"Bloqueio por Cliente"									//Regra ok	
"PD"	"Bloqueio por Tipo de Pedido"							//Regra ok
"PG"	"Bloqueio por Tabela Cond. Pagamento"					//Regra ok
"PC"	"Bloqueio por Tabela de Pre�os"							//Regra ok
"TA"	"T�tulos em Atraso"										//Regra ok
"PA"	"Pago em Atrazo"										//Regra ok
"CI"	"Cliente Inativo"										//Regra ok
"CN"	"Cliente Novo"											//Regra ok
"CP"	"Cliente Incompleto"									//Regra ok
"OB"	"Observacao Cliente"									//Regra ok
"CH"	"Cheque Devolvido"										//Regra ok
"PI"	"Produto Inativo"										//Regra ok
"LM"	"Limite Excedido"										//Regra ok
"QL"	"Quantidade de vezes que exedeu o Limite"				//Regra ok
"ND"	"Notas de Debito"										//Regra ok
"PR"	"T�tulos Protesto"										//Regra ok
"PE"	"Perdas (Conta a Receber)"								//Veriificar a carteira a ser utilizada (estou usando acordo)
"VP"	"Valor Minimo Pedido"
"BB"	"Pedidos B2C sempre bloqueados"
"LA"	"Liberado Automaticamente"
*/

If !( Alltrim( Upper( M->C5_ORIGEM ) ) $ "B2C/B2W" )
	If !(M->C5_POLCOM $ GetMV("MV_HPOLCOM")) 			//Libera��o de politicas
		If  !(M->C5_TPPED $ GetMV("MV_HTPPED"))			//Libera��o gde Tipos de Pedidos
			If !(M->C5_TIPO$'B/D') .and. alltrim(M->C5_XNSU) = ""
				
				//Verificao de Bloqueio por Cliente (tabela adicional)
				tabCli	:= .T.
				
				If tabCli .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
				
					_qry := " SELECT TOP 1 * FROM "+RetSqlName("ZZ2")+" ZZ2 WITH (NOLOCK) WHERE D_E_L_E_T_<>'*' "+cEOL
					_qry += "  	AND ZZ2_CODSZU='"+TMPSZU->ZU_CODIGO+"' AND ZZ2_CODCLI='"+M->C5_CLIENTE+"' AND ZZ2_LJCLI='"+M->C5_LOJACLI+"' "+cEOL
				 
					If Select("TMPZZ2") > 0 
						TMPZZ2->(DbCloseArea()) 
					EndIf
					
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPZZ2",.T.,.T.)
					DbSelectArea("TMPZZ2")
					DbGoTop()

					//If !EOF() .OR. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
					If !EOF() .OR. (TMPSC5OLD->CLIOLD <> M->C5_CLIENTE .and. alltrim(TMPSC5OLD->CLIOLD) <> "")
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"BC")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "BC"
								Replace ZZ1_DESCBL			with "Bloqueio por Cliente"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA			with ddatabase
							MsUnLock()
						Endif
					Endif
					
					TMPZZ2->(DbCloseArea()) 
				Endif
				
				//Verificao de Bloqueio por Tipo de Pedido (tabela adicional)
				tabTpPed	:= .T.

				If AllTrim(TMPSC5OLD->TPPEDOLD) <> ""
					If TMPSC5OLD->TPPEDOLD <> M->C5_TPPED 
						If !(M->C5_TPPED $ GetMV("MV_HTPDBLK"))
							tabTpPed	:= .F.
						EndIf
					Else
						tabTpPed	:= .F.
					EndIf
				EndIf

				If tabTpPed
				
					_qry := " SELECT * FROM "+RetSqlName("SZX")+" SZX WITH (NOLOCK) WHERE D_E_L_E_T_<>'*' AND ZX_CODSZU='"+TMPSZU->ZU_CODIGO+"' AND ZX_TPPED='"+M->C5_TPPED+"' "
				 
					If Select("TMPSZX") > 0 
						TMPSZX->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZX",.T.,.T.)
					DbSelectArea("TMPSZX")
					DbGoTop()
					
					If !EOF() .OR. (TMPSC5OLD->TPPEDOLD <> M->C5_TPPED .and. alltrim(TMPSC5OLD->TPPEDOLD) <> "")
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"PD")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "PD"
								Replace ZZ1_DESCBL			with "Bloqueio por Tipo de Pedido"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA			with ddatabase
							MsUnLock()
						Endif
					Endif
					
					TMPSZX->(DbCloseArea()) 
				Endif
				
				//Verificao de Bloqueio por Tabela de Condi��o de Pagamentos (tabela adicional)
				tabCondPag	:= .T.

				If AllTrim(TMPSC5OLD->CONDOLD) <> ""
					If TMPSC5OLD->CONDOLD <> M->C5_CONDPAG 
						If !(M->C5_CONDPAG $ GetMV("MV_HCPGBLK"))
							tabCondPag	:= .F.
						EndIf
					Else
						tabCondPag	:= .F.
					EndIf
				EndIf

				If tabCondPag
				
					_qry := " SELECT * FROM "+RetSqlName("SZW")+" SZW WITH (NOLOCK) WHERE D_E_L_E_T_<>'*' AND ZW_CODSZU='"+TMPSZU->ZU_CODIGO+"' AND ZW_CONDPAG='"+M->C5_CONDPAG+"' "
				
					If Select("TMPSZW") > 0 
						TMPSZW->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZW",.T.,.T.)
					DbSelectArea("TMPSZW")
					DbGoTop()
					
					If !EOF() .OR. (TMPSC5OLD->CONDOLD <> M->C5_CONDPAG .and. alltrim(TMPSC5OLD->CONDOLD) <> "")
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"PG")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "PG"
								Replace ZZ1_DESCBL			with "Bloqueio por Tabela Cond. Pagamento"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					
					TMPSZW->(DbCloseArea()) 
				Endif
				
				//Verificao de Bloqueio por Tabela de Pre�os (tabela adicional)
				tabTbPreco	:= .T.
				If tabTbPreco .AND. AllTrim(TMPSC5OLD->TABOLD) = ""
				
					_qry := " SELECT * FROM "+RetSqlName("SZV")+" SZV WITH (NOLOCK) WHERE D_E_L_E_T_<>'*' AND ZV_CODSZU='"+TMPSZU->ZU_CODIGO+"' AND ZV_CODTAB='"+M->C5_TABELA+"' "
				
					If Select("TMPSZV") > 0 
						TMPSZV->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZV",.T.,.T.)
					DbSelectArea("TMPSZV")
					DbGoTop()
					
					If !EOF()
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"PC")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "PC"
								Replace ZZ1_DESCBL			with "Bloqueio por Tabela de Pre�os"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					
					TMPSZV->(DbCloseArea()) 
				Endif
				
				//Verificao de T�tulos Atrasado
				If TMPSZU->ZU_TITATRA = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
				
					_qry := " SELECT * FROM "+RetSqlName("SE1")+" SE1 WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' AND E1_VENCREA <= '"+dtos(ddatabase-TMPSZU->ZU_VENCDIA)+"' AND E1_SALDO > 0 and E1_TIPO <> 'NCC' "+cEOL
					_qry += " AND E1_CLIENTE = '"+M->C5_CLIENTE+"' AND E1_LOJA = '"+M->C5_LOJACLI+"' AND E1_FILIAL = '"+xfilial("SE1")+"' "+cEOL
				
					If Select("TMPSE1") > 0 
						TMPSE1->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSE1",.T.,.T.)
					DbSelectArea("TMPSE1")
					DbGoTop()
					
					If !EOF()
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"TA")
						If !Found()
//							RecLock("ZZ1",.F.)
//								Replace ZZ1_DTLIB with ctod("  /  /  ")
//								Replace ZZ1_USRLIB with ""
//							MsUnLock()		
//						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "TA"
								Replace ZZ1_DESCBL			with "T�tulos em Atraso"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSE1->(DbCloseArea()) 
				Endif
				
				//Verificao de Pago em Atraso
				If TMPSZU->ZU_TITATRA = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
				
					_qry := " SELECT E1_VENCORI,E1_BAIXA, * FROM "+RetSqlName("SE1")+" SE1 WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' "+cEOL 
					_qry += " AND (E1_VENCREA < E1_BAIXA AND E1_BAIXA<>'') "+cEOL
					_qry += " AND E1_CLIENTE = '"+M->C5_CLIENTE+"' AND E1_LOJA = '"+M->C5_LOJACLI+"' AND E1_FILIAL = '"+xfilial("SE1")+"' "+cEOL
				
					If Select("TMPSE1") > 0 
						TMPSE1->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSE1",.T.,.T.)
					DbSelectArea("TMPSE1")
					DbGoTop()
					_PA := .F.
					
					While TMPSE1->(!EOF()) .and. _PA = .F.
						If (Stod(TMPSE1->E1_BAIXA) - Stod(TMPSE1->E1_VENCORI)) > TMPSZU->ZU_ATRAZO
							lRet := .F.	
							_PA := .T.
							DbSelectArea("ZZ1")
							DbSetOrder(1)
							DbSeek(xfilial("ZZ1")+M->C5_NUM+"PA")
							If Found()
								RecLock("ZZ1",.F.)
									Replace ZZ1_DTLIB with ctod("  /  /  ")
									Replace ZZ1_USRLIB with ""
								MsUnLock()		
							Else
								RecLock("ZZ1",.T.)
									Replace ZZ1_FILIAL			with xfilial("ZZ1")
									Replace ZZ1_PEDIDO			with M->C5_NUM
									Replace ZZ1_BLQ				with "PA"
									Replace ZZ1_DESCBL			with "Pago em Atrazo"
									Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
									Replace ZZ1_DATA				with ddatabase
								MsUnLock()
							Endif
						Endif
						TMPSE1->(DbSkip())
					End
					
					TMPSE1->(DbCloseArea()) 
				Endif
				
				//Verificao de Cliente Inativo
				If TMPSZU->ZU_CLIINAT = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
				
					_qry := " SELECT MAX(E1_EMIS1) AS E1_EMIS1, A1_MSBLQL FROM "+RetSqlName("SA1")+" SA1 WITH (NOLOCK) "+cEOL
					_qry += " 		INNER JOIN "+RetSqlName("SE1")+" SE1 WITH (NOLOCK) ON SE1.D_E_L_E_T_ <> '*' AND E1_CLIENTE=A1_COD AND E1_LOJA=A1_LOJA "+cEOL
					_qry += " WHERE SA1.D_E_L_E_T_ <> '*' AND A1_FILIAL = '"+xfilial("SA1")+"' "+cEOL
					_qry += " 		AND A1_COD = '"+M->C5_CLIENTE+"' AND A1_LOJA = '"+M->C5_LOJACLI+"' "+cEOL
					_qry += " GROUP BY A1_MSBLQL "+cEOL
				
					If Select("TMPSA1") > 0 
						TMPSA1->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSA1",.T.,.T.)
					DbSelectArea("TMPSA1")
					DbGoTop()
					
					If sToD(TMPSA1->E1_EMIS1) < (ddatabase - TMPSZU->ZU_INATIVO) .OR. TMPSA1->A1_MSBLQL="1"
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"CI")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "CI"
								Replace ZZ1_DESCBL			with "Cliente Inativo"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSA1->(DbCloseArea()) 
				Endif
				
				//Verificao de Clientes Novos (quantidade de pedidos faturados)
				If TMPSZU->ZU_CLINOVO = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
				
					_qry := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSqlName("SC5")+" SC5 WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' "+cEOL 
					_qry += " AND C5_NOTA<>'' AND C5_CLIENTE='"+M->C5_CLIENTE+"' AND C5_LOJACLI = '"+M->C5_LOJACLI+"' "+cEOL
				
					If Select("TMPSC5") > 0 
						TMPSC5->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC5",.T.,.T.)
					DbSelectArea("TMPSC5")
					DbGoTop()
					
					If TMPSC5->COUNT < 1
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"CN")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "CN"
								Replace ZZ1_DESCBL			with "Cliente Novo"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSC5->(DbCloseArea()) 
				Endif
				
				//Verificao de Dados Cliente Incompleto
				If TMPSZU->ZU_CLIINCO = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
				
					_qry := " SELECT A1_COD, A1_NOME, A1_PESSOA, A1_END, A1_BAIRRO, A1_EST, A1_CEP, A1_MUN, A1_PAIS, "+cEOL 
					_qry += " 			A1_CGC, A1_INSCR, A1_EMAIL, A1_XGRUPO, A1_XCANAL, A1_XCODREG "+cEOL
					_qry += " FROM "+RetSqlName("SA1")+" SA1 WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' AND A1_FILIAL = '"+xfilial("SA1")+"' "+cEOL 
					_qry += " 		AND (A1_NOME='' OR (A1_PESSOA='' OR (A1_PESSOA='J' AND A1_INSCR='')) OR A1_END='' OR A1_BAIRRO='' OR A1_EST='' "+cEOL   
					_qry += "				OR A1_CEP='' OR A1_MUN=''OR A1_PAIS='' OR (A1_CGC='' AND A1_EST<>'EX') OR (A1_EMAIL='' OR A1_EMAIL NOT LIKE '%@%') "+cEOL
					_qry += "				OR A1_XGRUPO='' OR A1_XCANAL='' OR A1_XCODREG='') "+cEOL
					_qry += " 		AND A1_COD = '"+M->C5_CLIENTE+"' AND A1_LOJA = '"+M->C5_LOJACLI+"' "+cEOL
				
					If Select("TMPSA1") > 0 
						TMPSA1->(DbCloseArea()) 
					EndIf
	
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSA1",.T.,.T.)
					DbSelectArea("TMPSA1")
					DbGoTop()
					
					_cCampoBloc := ""
					
					If ALLTRIM(TMPSA1->A1_NOME)=''
						_cCampoBloc += " NOME"
					End
					
					If ALLTRIM(TMPSA1->A1_PESSOA)=''
						_cCampoBloc += " TP.PESSOA"
					End
					
					If ALLTRIM(TMPSA1->A1_INSCR)=''
						_cCampoBloc += " INSC.EST"
					End
					
					If ALLTRIM(TMPSA1->A1_END)=''
						_cCampoBloc += " END."
					End
					
					If ALLTRIM(TMPSA1->A1_BAIRRO)=''
						_cCampoBloc += " BAIRRO"
					End
					
					If ALLTRIM(TMPSA1->A1_EST)=''
						_cCampoBloc += " EST."
					End
					
					If ALLTRIM(TMPSA1->A1_CEP)=''
						_cCampoBloc += " CEP"
					End
					
					If ALLTRIM(TMPSA1->A1_MUN)=''
						_cCampoBloc += " MUN"
					End
					
					If ALLTRIM(TMPSA1->A1_PAIS)=''
						_cCampoBloc += " PAIS"
					End
					
					If ALLTRIM(TMPSA1->A1_CGC)='' .and. TMPSA1->A1_EST <> "EX"
						_cCampoBloc += " CGC"
					End
					
					If ALLTRIM(TMPSA1->A1_XGRUPO)=''
						_cCampoBloc += " XGRUPO"
					End
					
					If ALLTRIM(TMPSA1->A1_XCANAL)=''
						_cCampoBloc += " XCANAL"
					End
					
					If ALLTRIM(TMPSA1->A1_XCODREG)=''
						_cCampoBloc += " XCODREG"
					End
					
					If ALLTRIM(TMPSA1->A1_EMAIL)='' .OR. !ALLTRIM(TMPSA1->A1_EMAIL) $ '@' 
						_cCampoBloc += " EMAIL"
					End
					
					_cCampoBloc := alltrim(_cCampoBloc)
					
					If !EOF()
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"CP")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "CP"
								Replace ZZ1_DESCBL			with "Cliente Incompleto ("+_cCampoBloc+")"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA			with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSA1->(DbCloseArea()) 
				Endif
				
				//Verificao de Cheque Devolvido
				If TMPSZU->ZU_CHEQDEV = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
				
					_qry := " SELECT * FROM "+RetSqlName("SEF")+" SEF WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' AND EF_FILIAL = '"+xfilial("SEF")+"' "+cEOL 
					_qry += " AND EF_DTCOMP='' AND (EF_ALINEA1<>'' OR EF_ALINEA2<>'') "+cEOL 
					_qry += " AND EF_CLIENTE = '"+M->C5_CLIENTE+"' AND EF_LOJACLI = '"+M->C5_LOJACLI+"' "+cEOL
				
					If Select("TMPSEF") > 0 
						TMPSEF->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSEF",.T.,.T.)
					DbSelectArea("TMPSEF")
					DbGoTop()
					
					If !EOF()
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"CH")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "CH"
								Replace ZZ1_DESCBL			with "Cheque Devolvido"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSEF->(DbCloseArea()) 
				Endif
				
				//Verificao de Produto Inativo
				If TMPSZU->ZU_PRODINA = "S" .AND. C5_XBLQ<>"L"
				
					_qry := " SELECT TOP 1 C6_NUM,B1_MSBLQL,B1_YDATLIB,B1_YDATENC, * FROM "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) "+cEOL
					_qry += "	 	INNER JOIN "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) ON SC6.D_E_L_E_T_<>'*' AND C6_PRODUTO=B1_COD AND C6_NUM='"+M->C5_NUM+"' "+cEOL
					_qry += " WHERE SB1.D_E_L_E_T_<>'*' AND B1_YDATENC<>'' AND B1_YDATENC<'"+dtos(ddatabase)+"' OR B1_YDATLIB>'"+dtos(ddatabase)+"' OR B1_MSBLQL='1' "+cEOL
				
					If Select("TMPSB1") > 0 
						TMPSB1->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSB1",.T.,.T.)
					DbSelectArea("TMPSB1")
					DbGoTop()
					
					If !EOF()
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"PI")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "PI"
								Replace ZZ1_DESCBL			with "Produto Inativo"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSB1->(DbCloseArea()) 
				Endif
				
				//Verificao de Limite Excedido
				If TMPSZU->ZU_CLILIMT = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
					
					cLimit := .F.
				
					_qry := " SELECT ISNULL(SUM(E1_SALDO),0) SALDO, "+cEOL
					_qry += "		(SELECT ISNULL(A1_LC,0) LIMITE FROM "+RetSqlName("SA1")+" SA1 WITH (NOLOCK) WHERE D_E_L_E_T_<>'*' "+cEOL  
					_qry += "			AND A1_COD='"+M->C5_CLIENTE+"' AND A1_LOJA='"+M->C5_LOJACLI+"') AS LIMITE, "+cEOL
					_qry += "		(SELECT ISNULL(SUM((C6_QTDVEN-C6_QTDENT)*C6_PRCVEN),0) AS PEDIDO FROM "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) "+cEOL 
					_qry += "			WHERE D_E_L_E_T_<>'*' AND C6_NOTA='' AND C6_CLI='"+M->C5_CLIENTE+"' AND C6_LOJA='"+M->C5_LOJACLI+"') AS PEDIDO "+cEOL
					_qry += "	FROM "+RetSqlName("SE1")+" SE1 WITH (NOLOCK) "+cEOL
					_qry += "	WHERE D_E_L_E_T_<>'*' AND E1_CLIENTE='"+M->C5_CLIENTE+"' AND E1_LOJA='"+M->C5_LOJACLI+"' AND E1_BAIXA='' AND E1_XAUTNSU='' "+cEOL 
				
					If Select("TMPSE1") > 0 
						TMPSE1->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSE1",.T.,.T.)
					DbSelectArea("TMPSE1")
					DbGoTop()
				
					If (TMPSE1->SALDO + TMPSE1->PEDIDO) > TMPSE1->LIMITE+((TMPSE1->LIMITE/100)*TMPSZU->ZU_LIMITE)
							cLimit := .T.
					End
					
					If cLimit
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"LM")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "LM"
								Replace ZZ1_DESCBL			with "Limite Excedido"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSE1->(DbCloseArea()) 
				Endif
				
				//Verificao de Vezes que o cliente ecedeu o Limite
				If TMPSZU->ZU_LIMITE > 0 .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
					
					cLimit := .F.
				
			   		_qry := "Select count(ZZ1_PEDIDO) AS COUNT from (SELECT ZZ1_PEDIDO FROM "+RetSqlName("ZZ1")+" ZZ1 WITH (NOLOCK) "+cEOL
			   		_qry += "WHERE D_E_L_E_T_<>'*' AND ZZ1_CLIENT= '"+M->C5_CLIENTE+M->C5_LOJACLI+"' "+cEOL
					_qry += "and ZZ1_BLQ = 'LM' group by ZZ1_PEDIDO) as X "

//					_qry := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSqlName("ZZ1")+" ZZ1 WITH (NOLOCK) "+cEOL
//					_qry += " WHERE D_E_L_E_T_<>'*' AND ZZ1_CLIENT= '"+M->C5_CLIENTE+M->C5_LOJACLI+"' "+cEOL
				
					If Select("TMPZZ1") > 0 
						TMPZZ1->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPZZ1",.T.,.T.)
					DbSelectArea("TMPZZ1")
					DbGoTop()
					
					If TMPZZ1->COUNT >= TMPSZU->ZU_LIMITE
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"QL")
						If Found() 
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "QL"
								Replace ZZ1_DESCBL			with "Quantidade de vezes que exedeu o Limite"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPZZ1->(DbCloseArea()) 
				Endif
				
				//Verificao de Notas de Debito
				If TMPSZU->ZU_NOTDEV = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
				
					_qry := " SELECT * FROM "+RetSqlName("SE1")+" SE1 WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' AND E1_SALDO > 0 AND E1_TIPO ='NDC' "+cEOL
					_qry += " AND E1_CLIENTE = '"+M->C5_CLIENTE+"' AND E1_LOJA = '"+M->C5_LOJACLI+"' AND E1_FILIAL = '"+xfilial("SE1")+"' "+cEOL
					_qry += " AND E1_VENCREA <= '"+dtos(ddatabase-TMPSZU->ZU_VENCDIA)+"'
				
					If Select("TMPSE1") > 0 
						TMPSE1->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSE1",.T.,.T.)
					DbSelectArea("TMPSE1")
					DbGoTop()
					
					If !EOF()
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"ND")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "ND"
								Replace ZZ1_DESCBL			with "Notas de Debito"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSE1->(DbCloseArea()) 
				Endif
				
/*
				// Solicitado retirar via email enviado pelo Sr Marc�lio em 21/08/2018
				//Verificao de T�tulos Protesto
				If TMPSZU->ZU_TITPROT = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
				
					_qry := " SELECT * FROM "+RetSqlName("SE1")+" SE1 WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' AND E1_SALDO > 0 AND E1_SITUACA IN ('F','H') "+cEOL
					_qry += " AND E1_CLIENTE = '"+M->C5_CLIENTE+"' AND E1_LOJA = '"+M->C5_LOJACLI+"' AND E1_FILIAL = '"+xfilial("SE1")+"' "+cEOL
				
					If Select("TMPSE1") > 0 
						TMPSE1->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSE1",.T.,.T.)
					DbSelectArea("TMPSE1")
					DbGoTop()
					
					If !EOF()
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"PR")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "PR"
								Replace ZZ1_DESCBL			with "T�tulo em Protesto"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSE1->(DbCloseArea()) 
				Endif
				
				//Verificao de Perdas (Conta)
				If TMPSZU->ZU_PERDAS = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
					
					_qry := " SELECT * FROM "+RetSqlName("SE1")+" SE1 WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' AND E1_SITUACA IN ('G') "+cEOL
					_qry += " AND E1_CLIENTE = '"+M->C5_CLIENTE+"' AND E1_LOJA = '"+M->C5_LOJACLI+"' AND E1_FILIAL = '"+xfilial("SE1")+"' "+cEOL
				
					If Select("TMPSE1") > 0 
						TMPSE1->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSE1",.T.,.T.)
					DbSelectArea("TMPSE1")
					DbGoTop()
					
					If !EOF()
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"PE")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "PE"
								Replace ZZ1_DESCBL			with "Perdas (Conta a Receber)"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSE1->(DbCloseArea()) 
				Endif
*/				
				//Valor Minimo Pedido
				If TMPSZU->ZU_VALPED = "S" .AND. TMPSC5OLD->CLIOLD <> M->C5_CLIENTE
					
					_qry := " SELECT SUM((C6_QTDVEN-C6_QTDENT)*C6_PRCVEN) AS VAL_MIN "+cEOL 
					_qry += " FROM "+RetSqlName("SC5")+" SC5 WITH (NOLOCK) "+cEOL
					_qry += " 		INNER JOIN "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) ON SC6.D_E_L_E_T_<>'*' AND C5_NUM=C6_NUM "+cEOL
					_qry += " WHERE SC5.D_E_L_E_T_<>'*' "+cEOL
					_qry += " 		AND C5_NUM='"+M->C5_NUM+"' "+cEOL
				
					If Select("TMPSZ2") > 0 
						TMPSZ2->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZ2",.T.,.T.)
					DbSelectArea("TMPSZ2")
					DbGoTop()
					
					If TMPSZ2->VAL_MIN < TMPSZU->ZU_VLMINPD
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"VP")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "VP"
								Replace ZZ1_DESCBL			with "Valor Minimo Pedido"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSZ2->(DbCloseArea()) 
				Endif
				
				//Verificao do Campo de Observa��o do Cliente
				If TMPSZU->ZU_OBSERV = "S" .AND. TMPSC5OLD->ALTOBS <> "N"
				
					_qry := " SELECT ISNULL(CONVERT(VARCHAR(2047), C5_XOBSINT),'') AS C5_XOBSINT, * "+cEOL
					_qry += " FROM "+RetSqlName("SC5")+" SC5 WITH (NOLOCK) WHERE D_E_L_E_T_ <> '*' AND C5_FILIAL = '"+xfilial("SC5")+"' "+cEOL
					_qry += " 		AND C5_NUM = '"+M->C5_NUM+"' "+cEOL
				
					If Select("TMPSC5") > 0 
						TMPSC5->(DbCloseArea()) 
					EndIf 
				
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC5",.T.,.T.)
					DbSelectArea("TMPSC5")
					DbGoTop()
					
					If FwCutOff(AllTrim(TMPSC5->C5_XOBSINT), .F.) <> '' 
						lRet := .F.
						DbSelectArea("ZZ1")
						DbSetOrder(1)
						DbSeek(xfilial("ZZ1")+M->C5_NUM+"OB")
						If Found()
							RecLock("ZZ1",.F.)
								Replace ZZ1_DTLIB with ctod("  /  /  ")
								Replace ZZ1_USRLIB with ""
							MsUnLock()		
						Else
							RecLock("ZZ1",.T.)
								Replace ZZ1_FILIAL			with xfilial("ZZ1")
								Replace ZZ1_PEDIDO			with M->C5_NUM
								Replace ZZ1_BLQ				with "OB"
								Replace ZZ1_DESCBL			with "Observacao Cliente"
								Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
								Replace ZZ1_DATA				with ddatabase
							MsUnLock()
						Endif
					Endif
					TMPSC5->(DbCloseArea()) 
				Endif

/*
				// Retirado conforme solicita��o por email do Sr Marcilio em 21/08/2018				
				//Pedidos B2C sempre bloqueados
				If TMPSZU->ZU_PEDB2B='S' .and. Alltrim(M->C5_ORIGEM)=='B2B' .AND. C5_XBLQ <> "L"
					lRet := .F.
					DbSelectArea("ZZ1")
					DbSetOrder(1)
					DbSeek(xfilial("ZZ1")+M->C5_NUM+"BB")
					If Found()
						RecLock("ZZ1",.F.)
							Replace ZZ1_DTLIB with ctod("  /  /  ")
							Replace ZZ1_USRLIB with ""
						MsUnLock()		
					Else
						RecLock("ZZ1",.T.)
							Replace ZZ1_FILIAL			with xfilial("ZZ1")
							Replace ZZ1_PEDIDO			with M->C5_NUM
							Replace ZZ1_BLQ				with "BB"
							Replace ZZ1_DESCBL			with "Pedidos B2B sempre Bloqueados"
							Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
							Replace ZZ1_DATA			with ddatabase
						MsUnLock()
					Endif
				Endif
*/				
			Else
				lRet := .T.
			Endif
		Else
			lRet := .T.
		Endif
	Else
		lRet := .T.
	Endif
Else
	lRet := .T.
End

DbSelectArea("SC5")
DbSetOrder(1)
DbSeek(xfilial("SC5")+M->C5_NUM)

DbSelectArea("ZZ3")
DbSetOrder(1)
DbSeek(xfilial("ZZ3")+M->C5_NUM)

If !Found()
		RecLock("ZZ3",.T.)
			Replace ZZ3_FILIAL		with xfilial("ZZ3")
			Replace ZZ3_PEDIDO		with M->C5_NUM
			Replace ZZ3_CLIENT		with M->C5_CLIENTE
			Replace ZZ3_LOJA			with M->C5_LOJACLI
			Replace ZZ3_NOME			with Substring(M->C5_XNOMCLI,1,45)
			Replace ZZ3_DATA			with ddatabase
		MsUnLock()
End
		
IF lRet = .F.
//	For _i := 1 to len(aCols)
//		aCols[_i,nPosBlq] := "X"
//	Next
	If Altera
		M->C5_XBLQ := "B"
		RecLock("SC5",.F.)
			Replace C5_XBLQ with "B"
		MsUnLock()

		RecLock("ZZ3",.F.)
			Replace ZZ3_STATUS with "B"
		MsUnLock()
		
	Else
		RecLock("SC5",.F.)
			Replace C5_XBLQ with "B"
		MsUnLock()
		
		RecLock("ZZ3",.F.)
			Replace ZZ3_STATUS with "B"
		MsUnLock()

	Endif
Else
	If Altera
		RecLock("SC5",.F.)
			Replace C5_XBLQ with "L"
		MsUnLock()

		RecLock("ZZ3",.F.)
			Replace ZZ3_STATUS with "L"
		MsUnLock()

		DbSelectArea("ZZ1")
		DbSetOrder(1)
		DbSeek(xfilial("ZZ1")+M->C5_NUM+"LA")

		IF !Found()
			RecLock("ZZ1",.T.)
				Replace ZZ1_FILIAL			with xfilial("ZZ1")
				Replace ZZ1_PEDIDO			with M->C5_NUM
				Replace ZZ1_BLQ				with "LA"
				Replace ZZ1_DESCBL			with "Liberado Automaticamente"
				Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
				Replace ZZ1_DATA			with ddatabase
			MsUnLock()
		Endif
	Else
		M->C5_XBLQ := "L"
		RecLock("SC5",.F.)
			Replace C5_XBLQ with "L"
		MsUnLock()

		RecLock("ZZ3",.F.)
			Replace ZZ3_STATUS with "L"
		MsUnLock()

		RecLock("ZZ1",.T.)
			Replace ZZ1_FILIAL			with xfilial("ZZ1")
			Replace ZZ1_PEDIDO			with M->C5_NUM
			Replace ZZ1_BLQ				with "LA"
			Replace ZZ1_DESCBL			with "Liberado Automaticamente"
			Replace ZZ1_CLIENT			with M->C5_CLIENTE+M->C5_LOJACLI
			Replace ZZ1_DATA			with ddatabase
		MsUnLock()

	Endif
Endif

//TODO Libera��o do pedido de venda na altera��o. Weskley Silva [14/08/2018]
DbSelectArea("SC6")
DbSetOrder(1)

IF M->C5_FILIAL == '0104'
if DBSeek(xFilial("SC6")+M->C5_NUM)
	   While SC6->C6_NUM == M->C5_NUM .AND. SC6->(!EOF())
	   	MaLibDoFat(SC6->(RecNo()),SC6->C6_QTDVEN,.F.,.F.,.T.,1,.T.,.F.,,,)
	   	SC6->(dBSkip())
	   enddo
	
	RecLock("SC5",.F.)
			Replace C5_LIBEROK  with "S"
	MsUnLock()   
	   
endif
endif
		
//Libera��o de pedidos habilitada temporariamente. 
//	RecLock("SC5",.F.)
//		Replace C5_XBLQ with "L"
//	MsUnLock()

TMPSZU->(DbCloseArea()) 

RestArea(_area)

Return()
