#include 'protheus.ch'
#include 'parmtype.ch'
#include "RWMAKE.CH"

/*/{Protheus.doc} Ponto de Entrada
	(Valida��o da reserva customizada na Transfer�ncia Simples)
	@type  Function
	@author Ronaldo pereira
	@since 14/04/2020
	@version version 1.0
	@return lRet (logico) 
/*/
User Function MT260TOK
Local aArea := GetArea()
Local lRet  := .T.
Local nDisp := 0
cLoteDigiD  := PARAMIXB[1]	   

//TODO Valida a reserva customizada XRESERV.
If AllTrim(CLOCORIG) $ "E0/QL"
	DbSelectArea("SB2")
	DbSetOrder(1)
	If DbSeek(xFilial("SB2")+CCODORIG+CLOCORIG)
		nDisp := SB2->B2_QATU-(SB2->B2_RESERVA+SB2->B2_XRESERV)		
		If NQUANT260 > nDisp
			MsgAlert("A quantidade a transferir � maior que a quantidade dispon�vel.","ATEN��O")
			lRet := .F.
		EndIf  
	EndIf
EndIf	

//TODO Valida a reserva customizada XRES das Opera��es sem OP.
If AllTrim(CLOCORIG) $ "A1" .And. AllTrim(CLOCDEST) <> "A1"
	DbSelectArea("SB2")
	DbSetOrder(1)
	If DbSeek(xFilial("SB2")+CCODORIG+CLOCORIG)
		nDisp := SB2->B2_QATU-(SB2->B2_RESERVA+SB2->B2_XRES)		
		If NQUANT260 > nDisp
			MsgAlert("A quantidade a transferir � maior que a quantidade dispon�vel.","ATEN��O")
			lRet := .F.
		EndIf  
	EndIf
EndIf

RestArea(aArea)

Return lRet