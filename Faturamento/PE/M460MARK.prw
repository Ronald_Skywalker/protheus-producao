#include "rwmake.ch"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
#include 'parmtype.ch'


User Function M460MARK()

	Private oFontN    := TFont():New("Arial",,20,,.t.,,,,,.f.)
	Private oFont2    := TFont():New("Arial",,12,,.t.,,,,,.f.)
	lret := .T.
	_nPedido         	:= SC9->C9_PEDIDO
	_nFilial         	:= SC9->C9_FILIAL
	cQry := " "

	If SC9->C9_XBLQ != "B"

		_cQtdNF				:= QtdPed(_nFilial,_nPedido,1)
		_cVlrNF				:= QtdPed(_nFilial,_nPedido,2)
		_cQtdCF				:= QtdPed(_nFilial,_nPedido,3)

		@ 156,367 To 400,934 Dialog mkwdlg Title OemToAnsi("Confer�ncia da Quantidade do Pedido")
		n_linha	:= 015
		n_latu		:= 013

		@ n_latu  ,018	Say OemToAnsi("Pedido:") Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
		@ n_latu  ,063	say SC9->C9_PEDIDO Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
		n_latu +=  n_linha

		@ n_latu  ,018	Say OemToAnsi("Cliente:") Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
		@ n_latu  ,063	Say SC9->C9_CLIENTE Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
		n_latu +=  n_linha //+010

		DbSelectArea("SA1")
		DbSetOrder(1)
		DbSeek(xfilial("SA1")+SC9->C9_CLIENTE+SC9->C9_LOJA)

		@ n_latu  ,018	Say OemToAnsi("Nome:") Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
		@ n_latu  ,063	Say SA1->A1_NOME Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
		n_latu +=  n_linha //+010

		@ n_latu+8,018	Say OemToAnsi("Total de Itens Liberados: "+ cValToChar(_cQtdNF) +" pe�as") Size 200,120 COLOR CLR_BLACK FONT oFontN PIXEL OF mkwdlg
		n_latu +=  n_linha+010

		@ n_latu+8,018	Say OemToAnsi("Vr Total de Itens Liberados: R$ "+ cValToChar(_cVlrNF) +" ") Size 200,120 COLOR CLR_BLACK FONT oFontN PIXEL OF mkwdlg
		n_latu +=  n_linha+010

		@ n_latu,035 BUTTON oButton2  PROMPT "Cancelar"    SIZE 036, 016 OF mkwdlg ACTION (lRet:=.F.,mkwdlg:End()) PIXEL MESSAGE "Cancelar"
		@ n_latu,075 BUTTON oButton2  PROMPT "Continuar"    SIZE 036, 016 OF mkwdlg ACTION (lRet:=.T.,mkwdlg:End()) PIXEL MESSAGE "Continuar"

		Activate Dialog mkwdlg CENTERED

		IF !EMPTY(SC9->C9_XNUMPF)
			If _cQtdNF <> _cQtdCF
				MessageBox("Este pedido possui diferen�a entre as quantidades aptas a faturar e a confer�ncia do DAP. O mesmo n�o poder� ser faturado!","Aten��o",16)
				lRet := .F.
			Endif
		ENDIF
	Else
		MessageBox("Pedido com bloqueio comercial, favor procurar o setor comercial! ", "Aten��o", 48)
		lRet := .F.
	EndIf

    //TODO verificar o saldo disponivel no armaz�m considerando a reserva customizada. Ronaldo Pereira 13/05/20
    If AllTrim(SC9->C9_LOCAL) = "A1"

		cQry := " SELECT C9_LOCAL, C9_PRODUTO, C9_QTDLIB "
		cQry += " FROM SC9010 (NOLOCK) WHERE D_E_L_E_T_ = '' AND C9_FILIAL = '"+_nFilial+"' AND C9_LOCAL = 'A1' AND C9_PEDIDO = '"+_nPedido+"' "

		If Select("TMPA") > 0
			TMPA->(DbCloseArea())
		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPA",.T.,.T.)
		DbSelectArea("TMPA")
		DbGoTop()

		While TMPA->(!EOF())  
		DbSelectArea("SB2")
		DbSetOrder(1)
			If DbSeek(xFilial("SB2")+TMPA->C9_PRODUTO+TMPA->C9_LOCAL)
				If SB2->B2_XRES > 0 				    
					//nDisp := SB2->B2_QATU-(SB2->B2_RESERVA+SB2->B2_XRES)		
					If TMPA->C9_QTDLIB > (SB2->B2_QATU - SB2->B2_XRES) //TMPA->C9_QTDLIB > nDisp
						MsgAlert("Quantidade do "+TMPA->C9_PRODUTO+" � maior que o saldo dispon�vel no A1!","ATEN��O")
						lRet := .F.
						TMPA->(DbSkip())
						Loop
					EndIf	
				EndIf  
			EndIf
			TMPA->(DbSkip())
		EndDo	
	EndIf

Return(lRet)

Static Function QtdPed(nFilial,nPedido,_tp)

	If _tp = 1
		_qry := " SELECT ISNULL(SUM(C9_QTDLIB),0) AS QTDNF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
		_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+nFilial+"' AND C9_PEDIDO='"+nPedido+"' and C9_NFISCAL = '' and C9_XBLQ = 'L' and C9_BLEST = '' "
	ElseIf _tp = 2
		_qry := " SELECT ISNULL(SUM(C9_QTDLIB*C9_PRCVEN),0) AS QTDNF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
		_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+nFilial+"' AND C9_PEDIDO='"+nPedido+"' and C9_NFISCAL = '' and C9_XBLQ = 'L' and C9_BLEST = '' "
	Else
		_qry := " Select Sum(ZJ_QTDLIB) as QTDNF from "+RetSQLName("SZJ")+" SZJ (NOLOCK) where SZJ.D_E_L_E_T_ = '' and ZJ_CONF = 'S' and ZJ_DOC = '' and ZJ_NUMPF in ( "
		_qry += " SELECT C9_XNUMPF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
		_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+nFilial+"' AND C9_PEDIDO='"+nPedido+"' and C9_NFISCAL = '' and C9_XBLQ = 'L' "
		_qry += " group by C9_XNUMPF) "
	Endif

	If Select("TMPSFT") > 0
		TMPSFT->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSFT",.T.,.T.)
	DbSelectArea("TMPSFT")
	DbGoTop()

	cQtdNF	:= TMPSFT->QTDNF

Return(cQtdNF)
