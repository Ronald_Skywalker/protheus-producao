#INCLUDE "TOTVS.CH"

/*/{Protheus.doc} M410TIP9

	Calculo das Parcelas para Multiplas Formas de Pagamento

	@author 	Milton J. dos Santos
	@since		01/06/2021
	@version	1.0
	@type function
/*/

User Function M410TIP9
Local lRet	 	:= .F.
Local nTotal   	:= 0
Local nTotLib9 	:= 0
Local nTot9    	:= 0
Local nQtdVen	:= 0
Local nQtdLib	:= 0
Local nValor 	:= 0
Local nX	 	:= 0
Local nY	 	:= 0
Local nTotParc	:= 0
Local nParcelas := 0
Local nLimParc	:= SuperGetMV("MV_NUMPARC",.F.,36)	// Verificacao do limite de parcelas no parametro (MFP = 36)
Local cDHIni    := Time()

If M->C5_CONDPAG <> "MFP"
	Return( .T. )
Endif

For nX := 1 to Len(aCols)
	If !aCols[nx][Len(aCols[nx])]
		For ny := 1 to Len(aHeader)
			If Trim(aHeader[ny][2]) == "C6_QTDVEN"
				nQtdVen := aCols[nx][ny]
			ElseIf Trim(aHeader[ny][2]) == "C6_QTDLIB"
				nQtdLib := aCols[nx][ny]
			ElseIf Trim(aHeader[ny][2]) == "C6_VALOR"
				nValor := aCols[nx][ny]
			EndIf
		Next ny
		
		nTotal   +=  nValor
		nTotLib9 +=  nQtdLib
		nTot9    +=  nQtdVen
	EndIf
Next nX
	
nTotal	 := nTotal + M->C5_FRETE + M->C5_DESPESA + M->C5_SEGURO + M->C5_FRETAUT - M->C5_DESCONT

cOrdParc := "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0"
nParcelas:= 0
nTotParc := 0

For nX := 1 to nLimParc
	nParc := &("M->C5_PARC"+Substr(cOrdParc,nx,1))
	dParc := &("M->C5_DATA"+Substr(cOrdParc,nx,1))
	If nParc > 0 .And. ! Empty(dParc)
		nParcelas++
		nTotParc += nParc
	EndIf
Next nX

If nParcelas == 0
	Help("Nao conseguiu calcular as parcelas",1,"A410TIPO9")		
ElseIf nParcelas > nLimParc
	Help("Numero de parcelas nao pode ser maior que " + Str(nLimParc,2),1,"A410TIPO9")		
Else
	If nTotal == nTotParc
		lRet := .T.
	Else
		Help("O VALOR TOTAL CONFERE COM O TOTAL DE PARCELAS",1,"A410TIPO9")		
	EndiF
Endif
conout("M410TIP9 - Inicio: " + cDHIni + " - Fim: " + Time() )

Return( lRet )
