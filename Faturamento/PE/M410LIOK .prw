#Include "Protheus.ch"
/*�����������������������������������������������������������������������������������
�������������������������������������������������������������������������������������
���������������������������������������������������������������������������������Ŀ��
���Programa | M410LIOK.PRW     � Autor Geyson Albano            � Data � 22/10/21 ���
���������������������������������������������������������������������������������Ĵ��
���Descricao |                                                                    ���
���          � M410LIOK => Ponto de Entrada para validacao de linha do PV.        ���
���������������������������������������������������������������������������������Ĵ��
���Parametros� Nenhum.                                                            ���
���          �                                                                    ���
���������������������������������������������������������������������������������Ĵ��
���Retorno   � lRet => .T. ou .F.                                                 ���
���          �                                                                    ���
���������������������������������������������������������������������������������Ĵ��
���Analista Resp.� Data � Manutencao Efetuada                                   ���
����������������������������������������������������������������������������������ٱ�
���              |        �                                                       ���
���              |        |                                                       ���
����������������������������������������������������������������������������������ٱ�
�������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������*/


User Function M410LIOK()
    
Local lRet      := .T.
Local nNewQtd       := 0
Local nDifQtd       := 0
Local cProdb5       := ""
//Local nPos      := 0
       
If SC5->C5_XPEDLAN == "S"
    nNewQtd := aCols[n][5]
    cProdb5 := Alltrim(aCols[n][2])
  
   If nNewQtd > SC6->C6_QTDVEN
       nDifQtd:= nNewQtd - SC6->C6_QTDVEN
       	
        DbSelectArea("SB5")
		DbSetOrder(1) 
		If DbSeek(xFilial("SB5")+cProdb5)
        
        RecLock("SB5",.F.)
            SB5->B5_XSALDOW := IIf(SB5->B5_XSALDOW-nDifQtd<= 0,0,SB5->B5_XSALDOW-nDifQtd)
        MsUnlock()
       EndIf 
    ElseIf nNewQtd < SC6->C6_QTDVEN
        nDifQtd:=  SC6->C6_QTDVEN - nNewQtd 

        DbSelectArea("SB5")
		DbSetOrder(1) 
		If DbSeek(xFilial("SB5")+cProdb5)
        
        RecLock("SB5",.F.)
            SB5->B5_XSALDOW := IIf(SB5->B5_XSALDOW-nDifQtd<= 0,0,SB5->B5_XSALDOW-nDifQtd)
        MsUnlock()
       EndIf 
   EndIf
EndIf

Return lRet
