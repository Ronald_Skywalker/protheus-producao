#INCLUDE "rwmake.ch"

STATIC cMailB		:= "bruna.cristina@hopelingerie.com.br;geyson.albano@hopelingerie.com.br;robson.vieira@hopelingerie.com.br"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � SF2460I  �Autor  �Daniel R. Melo      � Data �  23/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     � Ponto de Entrada para chamar a fun��o que atualiza os dados���
���Desc.     � da expedicao da nota fiscal                                ���
���          � ex: peso liquido, volume, transportadora..                 ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico HOPE - www.actualtrend.com.br                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function SF2460I()
Local aArea    := GetArea()
Local nxTaxa   := 0
Local nxVLPIX  := 0
Local _XVLTXCC := 0 // Round((SE1->E1_VALOR*nxTaxa)/100,2)
Local _XVLRLIQ := 0 // SE1->E1_VALOR-Round((SE1->E1_VALOR*nxTaxa)/100,2)
Local nParce   := 0 // n�mero de parcelas na condi��o de pagamento
Local _i	   := 0
Local nCupom   := 0
Local cNSU	   := ""
Local cAutCar  := ""
Local cCartID  := ""
Local cTID	   := ""
Local cTpPag   := ""
Local dVencto  := CtoD( " " )
Local cHistor  := ""
Local cDHIni	:= Time()
	

Private aCupom := {}
Private aTitulo:= {}

//Grava o NSU no registro SE1

	_desc := 0

	DbSelectArea("SA1")
	DbSetOrder(1)
	If DbSeek(xfilial("SA1")+SF2->F2_CLIENTE+SF2->F2_LOJA)

		If SA1->A1_XTPDE01 = "F"
			_desc += SA1->A1_XDESC01*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE02 = "F"
			_desc += SA1->A1_XDESC02*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE03 = "F"
			_desc += SA1->A1_XDESC03*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE04 = "F"
			_desc += SA1->A1_XDESC04*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE05 = "F"
			_desc += SA1->A1_XDESC05*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE06 = "F"
			_desc += SA1->A1_XDESC06*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE07 = "F"
			_desc += SA1->A1_XDESC07*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE08 = "F"
			_desc += SA1->A1_XDESC08*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE09 = "F"
			_desc += SA1->A1_XDESC09*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE10 = "F"
			_desc += SA1->A1_XDESC10*(1-(_desc/100))
		Endif
	EndIf  
//	Autoriza��o do Cart�o quando salva no pedido
	cNSU	:= SC5->C5_XNSU
	cAutCar := SC5->C5_XAUTCAR
	cCartID	:= SC5->C5_XCARTID
	cTpPag	:= SC5->C5_XTPPAG

	DbSelectarea("SE1")
	DbSetOrder(1)
	DbSeek(xfilial("SE1")+SF2->(F2_SERIE+F2_DOC))

	While !EOF() .and. SF2->F2_SERIE == SE1->E1_PREFIXO .and. SF2->F2_DOC == SE1->E1_NUM

		If SF2->(F2_SERIE+F2_DOC) == SE1->(E1_PREFIXO+E1_NUM)
			nxTaxa := Posicione("SE4",1,xFilial("SE4")+SF2->F2_COND,"E4_XTXCC")
			nxVLPIX:= Posicione("SE4",1,xFilial("SE4")+SF2->F2_COND,"E4_XVLPIX")
			nParce := Posicione("SE4",1,xFilial("SE4")+SF2->F2_COND,"E4_XNUMPAR")
			nCredRK:= Iif(SC5->C5_XCREDRK > 0, SC5->C5_XCREDRK, 1 )
			dVencto:= SE1->E1_VENCTO
			cHistor:= SE1->E1_HIST
			If SC5->C5_CONDPAG == "MFP"
				CargaZBE( @cNSU, @cAutCar, @cCartID, @cTPPAG, @nxTaxa, @nxVLPIX, @nParce, @dVencto, @cHistor, @cTID )
			Endif
			If nParce == '0'
			   nParce := 1
			Else
			   nParce := val(nParce)  
			Endif   
			If nxTaxa == 0
				_XVLTXCC  := 0	// Round((SE1->E1_VALOR*nxTaxa)/100,2)
				_XVLRLIQ  := 0	// SE1->E1_VALOR-Round((SE1->E1_VALOR*nxTaxa)/100,2)
			Else
				_XVLTXCC  := Round(((SE1->E1_VALOR-SC5->C5_XCREDRK)*nxTaxa)/100,2)
				_XVLRLIQ  := SE1->E1_VALOR-Round(((SE1->E1_VALOR-(SC5->C5_XCREDRK/nParce))*nxTaxa)/100,2)
			Endif
			DbSelectArea("SE1")
			RecLock("SE1",.F.)
			SE1->E1_DEBITO  := Posicione("SED",1,xFilial("SED")+SE1->E1_NATUREZ,"ED_CONTA")
			SE1->E1_NOMCLI  := SA1->A1_NOME
			SE1->E1_XNSU	:= cNSU		
			SE1->E1_XAUTNSU	:= cAutCar
			SE1->E1_XTPPAG  := cTPPAG 
			SE1->E1_XPEDWEB	:= SC5->C5_XPEDWEB
			SE1->E1_XPEDRAK	:= SC5->C5_XPEDRAK
			SE1->E1_XFORREC	:= SC5->C5_XDESCOD
			SE1->E1_XCARTID	:= cCartID
			SE1->E1_XTID	:= cTID                                
			SE1->E1_DESCFIN := _DESC
			SE1->E1_XTXCC   := nxTaxa
			SE1->E1_XVLPIX  := nxVLPIX
			SE1->E1_VLTXCC  := _XVLTXCC
			SE1->E1_SDDECRE := _XVLTXCC + nxVLPIX
			SE1->E1_VLRLIQ  := _XVLRLIQ - nxVLPIX
			SE1->E1_XCODDIV := SA1->A1_XCODDIV
			SE1->E1_XNONDIV := SA1->A1_XNONDIV
			SE1->E1_XCANAL  := SA1->A1_XCANAL
			SE1->E1_XCANALD := SA1->A1_XCANALD
			SE1->E1_XCODREG := SA1->A1_XCODREG
			SE1->E1_XDESREG := SA1->A1_XDESREG
			If SC5->C5_CONDPAG == "MFP"
				SE1->E1_VENCTO  := dVencto
				SE1->E1_VENCREA := dVencto
				SE1->E1_HIST	:= cHistor                                
			Endif
			SE1->( MSUNLOCK() )
		Endif

		SE1->( DbSkip() )

	Enddo
	conout("SF2460I + Atualiza SE1 - Inicio: " + cDHIni + " - Fim: " + Time() )

	cDHIni	:= Time()
	For nCupom := 1 to Len( aCupom )
		U_BxaCUPOM(aCupom[nCupom], aTitulo, .F. )
	NEXT
	conout("SF2460I + BxaCUPOM - Inicio: " + cDHIni + " - Fim: " + Time() )
	cDHIni	:= Time()

	_qry := "update "+RetSqlName("SZJ")+" set ZJ_DOC = D2_DOC, ZJ_SERIE = D2_SERIE from "+RetSqlName("SZJ")+" SZJ "
	_qry += "inner join "+RetSqlName("SD2")+" SD2 on SD2.D_E_L_E_T_ = '' and ZJ_PEDIDO = D2_PEDIDO and ZJ_ITEM = D2_ITEMPV "
	_qry += "where SZJ.D_E_L_E_T_ = '' and D2_DOC = '"+SF2->F2_DOC+"' and D2_SERIE = '"+SF2->F2_SERIE+"' and ZJ_CONF = 'S' and ZJ_DOC = '' "
	TcSqlExec(_qry)

// Chamada da rotina para atualizar os dados da expedi��o da NF.
// Alterado para o Ponto de Entrada M460FIM 
// U_HFT003EXP(.T.)

	DbSelectArea("SD2")
	DbSetOrder(3)
	aRegSC5 := {}
	If DbSeek(xFilial("SD2")+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA)
		While SD2->(!EOF()) .And. SD2->D2_DOC+SD2->D2_SERIE = SF2->F2_DOC+SF2->F2_SERIE
			nPos := aScan( aRegSC5, {|x| alltrim(x) == alltrim(SD2->D2_PEDIDO)})
			IF nPos = 0
				aAdd(aRegSC5, SD2->D2_PEDIDO)
			Endif

			DbSelectArea("SD2")
			DbSkip()
		End
	Endif
	For _i := 1 to len(aRegSC5) 
		DbSelectArea("SC5")
		DbSetOrder(1)
		DbSeek(xfilial("SC5")+aRegSC5[_i])
		
		If alltrim(SC5->C5_XCONDOR) <> ""
			RecLock("SC5",.F.)
				Replace C5_CONDPAG	with C5_XCONDOR
				Replace C5_XCONDOR	with ""
			MsUnLock()
		Endif
		u_ElimRes(aRegSC5[_i])
	Next
	conout("SF2460I + ElimRes - Inicio: " + cDHIni + " - Fim: " + Time() )

	RestArea(aArea)

Return NIL

User Function ElimRes(_ped)

Local lResd := .F.
Local aItRes := {}
	nVlrDep := 0

	Begin Transaction
//		Elimina residuo de itens e atualiza cabecalho do pedido.
		_qry := "Select sum((C6_QTDVEN-C6_QTDENT)*C6_PRUNIT) as SALDO from "+RetSqlName("SC6")+" SC6 where SC6.D_E_L_E_T_ = '' and C6_BLQ <> 'R' "
		_qry += "and C6_NUM = '"+_ped+"' "

		If Select("TMPSC6") > 0
			TMPSC6->(DbCloseArea())
		EndIf

		cQuery := ChangeQuery(_qry)
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC6",.T.,.T.)

		//IF TMPSC6->SALDO < 300
		IF TMPSC6->SALDO < GetMv("HP_VALRESI")
			DbSelectArea("SC6")
			DbSetOrder(1)
			DbSeek(xfilial("SC6")+_ped)

			While !EOF() .and. SC6->C6_NUM = _ped
				If (SC6->C6_QTDVEN - SC6->C6_QTDENT) > 0 .and. SC6->C6_BLQ <> 'R'
					MaResDoFat(nil, .T., .F., @nVlrDep)
				cMsg := "- Produtos n�o faturados [ " + Alltrim( SC6->C6_PRODUTO ) + " ] Qtd: "+CValToChar(SC6->C6_QTDVEN - SC6->C6_QTDENT)+""
				AADD( aItRes, cMsg )
					lResd:= .T.
				Endif

				DbSelectArea("SC6")
				DbSkip()
			EndDo
			If lResd
				DbSelectArea("SC5")
				DbSetOrder(1)
				If DbSeek(xfilial("SC5")+_ped)
				cRepres := SC5->C5_VEND1
				cPedWeb := SC5->C5_XPEDWEB
				cCliPed	:= Alltrim(SC5->C5_CLIENTE +" - "+ SC5->C5_XNOMCLI )
				EndIf
				If !Empty(cRepres)
					NotifRES(_ped,aItRes,cRepres,cPedWeb,cCliPed)
				EndIf
			EndIf
			DbSelectArea("SC5")
			DbSetOrder(1)
			DbSeek(xfilial("SC5")+_ped)
			MaLiberOk({ SC5->C5_NUM }, .T.)
		Endif

		DbSelectarea("TMPSC6")
		DbCloseArea()

	End Transaction
Return

Static Function CargaZBE( cNSU, cAutCar, cCartID, cTPPAG, nxTaxa, nxVLPIX, nParce, dVencto, cHistor, cTID )
Local cCondPag	:= ""
//Local cParcela 	:= Chr(Asc(GetMV("MV_1DUP"))-1)
//Local nTamParc 	:= TAMSX3("E1_PARCELA")[1]
//Local cNumSC5   := Space( TAMSX3("ZBE_NUMSC5")[1] )
//Local nTamPfx 	:= TAMSX3("E1_PREFIXO")[1]

DbSelectArea("SE4")
DbSetOrder(1)
DbSelectArea("ZBE")
DbSetOrder(1)
If Dbseek( xFilial("ZBE") + SC5->C5_NUM + SE1->E1_PARCELA )
	If ZBE->ZBE_TIPO $ "345"
		aAdd( aCupom,  PADR(ZBE->ZBE_CARDID, TamSX3("ZBF_IDVTEX")[1] ))
		aAdd( aTitulo, {SE1->E1_CLIENTE,SE1->E1_LOJA,SE1->E1_PREFIXO,SE1->E1_NUM,SE1->E1_PARCELA,SE1->E1_TIPO,SE1->E1_VALOR})
	Endif
	Do Case
		Case ZBE->ZBE_TIPO == "1"	// Cartao de Credito
			cTPPAG	:= "CARTAO"
		Case ZBE->ZBE_TIPO == "2"	// Boleto
			cTPPAG	:= "BOLETO"
		Case ZBE->ZBE_TIPO == "3"	// Vale Compra
			cTPPAG	:= "A VISTA"
			cHistor	:= "VINCULADO AO CUPOM: [ " + Alltrim( ZBE->ZBE_CARDID ) + " ] (VALE COMPRA)"
		Case ZBE->ZBE_TIPO == "4"	// Vale Presente
			cTPPAG	:= "A VISTA"
			cHistor	:= "VINCULADO AO CUPOM: [ " + Alltrim( ZBE->ZBE_CARDID ) + " ] (VALE PRESENTE)"
		Case ZBE->ZBE_TIPO == "5"	// Bonificacao
			cTPPAG	:= "A VISTA"
			cHistor	:= "VINCULADO AO CUPOM: [ " + Alltrim( ZBE->ZBE_CARDID ) + " ] (BONIFICACAO)"
		Case ZBE->ZBE_TIPO == "6"	// Deposito
			cTPPAG	:= "A VISTA"
		Case ZBE->ZBE_TIPO == "7"	// Debito
			cTPPAG	:= "A VISTA"
		Case ZBE->ZBE_TIPO == "8"	// PIX
			cTPPAG	:= "PIX"
	EndCase
	cNSU 	:= ZBE->ZBE_NSU
	cAutCar := ZBE->ZBE_AUTCAR
	cCartID	:= ZBE->ZBE_CARDID
	cTID	:= ZBE->ZBE_TID
	cCondPag:= ZBE->ZBE_CONDPA
	dVencto := ( SE1->E1_EMISSAO + ZBE->ZBE_PRAZO )
	DbSelectArea("SE4")
	If DbSeek( xFilial("SE4") + cCondPag )
		nxTaxa	:= SE4->E4_XTXCC
		nxVLPIX	:= SE4->E4_XVLPIX
		nParce	:= SE4->E4_XNUMPAR
	Endif
	DbSelectArea("ZBE")
	Reclock("ZBE",.F.)
	ZBE->ZBE_PREFIX := SE1->E1_PREFIXO
	ZBE->ZBE_NUMTIT := SE1->E1_NUM
	ZBE->ZBE_TIPTIT := SE1->E1_TIPO
	ZBE->( MSUNLOCK() )
Endif
Return

User Function BxaCUPOM( _cCUPOM, _aTitulo, _lMsg )
Local aArea		:= GetArea()
Local lRet		:= .F.
Local nTaxaCM	:= 0
Local aTxMoeda	:= {}
//Local cPortador := "001"
//Local aVetor	:= {}
Local cCliente	:= PadR( _aTitulo[1,1], TamSX3("E1_CLIENTE")[1] )
Local cLoja 	:= PadR( _aTitulo[1,2], TamSX3("E1_LOJA")[1] 	)
Local cPrefixo	:= PadR( _aTitulo[1,3], TamSX3("E1_PREFIXO")[1] )
Local cNumDoc 	:= PadR( _aTitulo[1,4], TamSX3("E1_NUM")[1] 	)
Local cParcela	:= PadR( _aTitulo[1,5], TamSX3("E1_PARCELA")[1] )
Local cTipoDoc	:= PadR( _aTitulo[1,6], TamSX3("E1_TIPO")[1] 	)
Local nSaldo	:= _aTitulo[1,7]	// Valor a ser compensado (Caso seja parcial Pode ser parcial) - Compensar apenas R$ 50,00

Private nRecnoNDF
Private nRecnoE1

DbSelectArea("ZBF")
DbSetOrder(4)
If DbSeek( _cCUPOM )
	If ZBF->ZBF_OPCAO == "4" .and. ZBF->ZBF_STATUS $ "56"
		lRet := .T.
	Else
		If _lMsg
			MsgStop( "CUPOM nao autorizado!")
		Else
			ConOut( "CUPOM [" + _cCUPOM + "] nao autorizado!")
		Endif
	Endif
Endif
If lRet
	dbSelectArea("SE1")
	DbSetOrder(2) // E1_FILIAL, E1_CLIENTE, E1_LOJA, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, R_E_C_N_O_, D_E_L_E_T_
	If SE1->( DbSeek(xFilial("SE1") + ZBF->( ZBF_CLIORI + ZBF_LOJORI + ZBF_TITPFX + ZBF_TITNUM  + ZBF_TITPAR + ZBF_TITTIP ) ))
		nRecnoRA := RECNO()

		If SE1->( DbSeek( xFilial("SE1") + cCliente + cLoja + cPrefixo + cNumDoc  + cParcela + cTipoDoc ) )
			nRecnoE1 := RECNO()

			PERGUNTE("AFI340",.F.)
			lContabiliza:= MV_PAR11 == 1
			lAglutina   := MV_PAR08 == 1
			lDigita		:= MV_PAR09 == 1

			nTaxaCM := RecMoeda(dDataBase,SE1->E1_MOEDA)

			aAdd(aTxMoeda, {1, 1} )

			aAdd(aTxMoeda, {2, nTaxaCM} )

			SE1->(dbSetOrder(1)) //E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO+E1_FORNECE+E1_LOJA
	
			aRecRA	:= { nRecnoRA }
			aRecSE1 := { nRecnoE1 }
	
			If !MaIntBxCR(3,aRecSE1,,aRecRA,,{lContabiliza,lAglutina,lDigita,.F.,.F.,.F.}, , , , , nSaldo)
				Help("XAFCMPAD",1,"HELP","XAFCMPAD","N�o foi poss�vel a compensa��o"+CRLF+" do titulo do adiantamento",1,0)
				lRet := .F.
			ENDIF
		ENDIF
	ENDIF
Endif
If lRet 
	DbSelectArea("ZBF")
	Reclock("ZBF",.F.)
	ZBF->ZBF_STATUS := "6"
	ZBF->ZBF_DTCOMP	:= dDataBase 
	ZBF->ZBF_VLPED	:= ZBF->ZBF_VLPED - nSaldo
	ZBF->ZBF_VLFAT	:= ZBF->ZBF_VLFAT + nSaldo
	ZBF->(MsUnlock()) 
Endif
RestArea(aArea)
Return( lRet )

/*/
	------------------------------------------------------------------
	{Protheus.doc} NotifRES()
	Notificacao do Log de Erro Pedidos BLOQUEIO DE DEMANDA

	@author Geyson Albano
	@since Nov/2020
	@version 1.00
	------------------------------------------------------------------
/*/
Static Function NotifRES(_ped,aItRes,cRepres,cPedWeb,cCliPed)

	Local cAssunto	:= "Res�duo gerado para o pedido:  "+_ped 

	cemailrep := Alltrim(POSICIONE("SA3",1,xFilial("SA3")+Alltrim(cRepres),"A3_EMAIL"))
	If !Empty(cemailrep)
		cMailB += ";"+cemailrep
	Endif
	//U_SendMail( cMailB, cAssunto, getBody( "", "6", aItRes ))

	//U_SendMail( "geyson.albano@hopelingerie.com.br", cAssunto, getBody( "", "6", aItRes,cPedWeb,cCliPed,_ped ))
	U_SendMail( cMailB, cAssunto, getBody( "", "6", aItRes,cPedWeb,cCliPed,_ped ))

Return

/*/
	------------------------------------------------------------------
	{Protheus.doc} getBody
	Retorna o Texto do Body do Email

	@author Sergio S. Fuzinaka
	@since Jul/2020
	@version 1.00
	------------------------------------------------------------------
/*/
Static Function getBody( cMsg, cTp, aLog,cPedWeb,cCliPed,_ped )

	Local nX		:= 0
	Local cHtml 	:= ""
	Local cMotRes := "Faturamento restante menor que R$ 300,00 "

	cHtml := '<!DOCTYPE HTML>'
	cHtml += '<html lang="pt-br">'
	cHtml += '<head>'
	cHtml += '<meta charset="utf-8">'
	cHtml += '</head>'
	cHtml += '<body>'

	cHtml += '<h3>Log gera��o de Res�duo para o Pedido Externo: '+cPedWeb+' e no Protheus de num: '+_ped+'  </h3>'
	cHtml += '<h3>Cliente: '+cCliPed+' </h3>'
	cHtml += '<h3>Motivo: '+cMotRes+' </h3>'
		
	cHtml += '<ul>'
		For nX := 1 To Len( aLog )
			cHtml += '<br>' + aLog[nX] + '</b>'
		Next
		cHtml += '</ul>'

	cHtml += '</body>'
	cHtml += '</html>'

Return( cHtml )
