#Include "PROTHEUS.CH"
#include "TbiConn.ch"
#include "TOTVS.ch"
#include "topconn.ch"

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �A410EXC  �Autor  �Ronaldo Pereira      � Data �  03/05/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �Valida pedido de venda na Exclus�o.                         ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/
user function A410EXC()
Local l_Ret      := .T.  // Conteudo de retorno
Local _aAreaOLD := GetArea()                            
Local a_SC9		:= SC9->(GetArea())
Local a_SC6		:= SC6->(GetArea())
Local a_SC5		:= SC5->(GetArea())

lVer := .F.

	//TODO Validar pr�-faturamento em aberto na exclus�o de pedido. Ronaldo pereira 03/05/19
	If SC5->C5_TIPO = "N"
		DbSelectArea("SZJ")
		DbSetOrder(2) 
		If DbSeek(xFilial("SZJ")+SC5->C5_NUM)
			While SZJ->(!EOF()) .And. SZJ->ZJ_PEDIDO = SC5->C5_NUM
				If Empty(SZJ->ZJ_DOC)
					lVer  := .T.
				EndIf	
				SZJ->(DbSkip())
			EndDo
			If lVer
				MsgAlert("Pedido com Pr�-Faturamento em Aberto, Pedido n�o pode ser exclu�do!","A T E N � � O!!!")
				l_Ret := .F.
			EndIf
		EndIf				
	EndIf

If 	l_Ret .AND. Alltrim(SC5->C5_ORIGEM) == "WISESALE" .AND. SC5->C5_XPEDLAN == "S"
	U_HATUSB5W(SC5->C5_NUM,2) // ROTINA PARA SOMAR O SALDO DO PEDIDO EXCLU�DO QUANDO FOR LAN�AMENTO DA WISE
EndIf

RestArea(a_SC9)
RestArea(a_SC6)
RestArea(a_SC5)
RestArea(_aAreaOLD)
	
return(l_Ret)
