#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} Ponto de Entrada Estorno da Transfer�ncia Simples
	(Valida��o da reserva customizada)
	@type  Function
	@author Ronaldo Pereira
	@since 09/09/2021
	@version version 1.0
	@return lRet (logico) 
/*/
user function MA260EST()

Local aArea := GetArea()
Local lRet  := .T.
Local nDisp := 0 
Local nEstono := 0

//TODO Valida a reserva customizada XRESERV.
If AllTrim(CLOCDEST) $ "E0"
	DbSelectArea("SB2")
	DbSetOrder(1)
	If DbSeek(xFilial("SB2")+CCODORIG+CLOCDEST)
		nDisp := SB2->B2_QATU-(SB2->B2_RESERVA+SB2->B2_XRESERV)		
		nEstono := nDisp - NQUANT260 
		If nEstono < 0
			MsgAlert("N�o � poss�vel estornar, a RESERVA do produto ficara maior que o saldo em estoque.(MA260EST)","ATEN��O")
			lRet := .F.
		EndIf  
	EndIf
EndIf	

RestArea(aArea) 

Return lRet
	