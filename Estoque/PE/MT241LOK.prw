#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} Ponto de Entrada
	(Valida��o da reserva customizada na Movimenta��o Multiplas)
	@type  Function
	@author Ronaldo pereira
	@since 12/04/2020
	@version version 1.0
	@return lRet (logico) 
/*/
User Function MT241LOK()

Local aArea  := GetArea()
Local lRet   := .T.
Local nTM    := VAL(M->CTM)
Local nLinha := PARAMIXB[1]  // numero da linha do aCols
Local cProd  := aCols[nLinha][1]
Local cAmz   := aCols[nLinha][6]
Local nQtde  := aCols[nLinha][3]
Local nDisp  := 0

If nTM > 499	        
	If AllTrim(cAmz) $ "E0/QL/A1"
		DbSelectArea("SB2")
		DbSetOrder(1)
		If DbSeek(xFilial("SB2")+cProd+cAmz)
			nDisp := SB2->B2_QATU-(SB2->B2_RESERVA+SB2->B2_XRESERV+SB2->B2_XRES)			
			If nQtde > nDisp
				MsgAlert("A quantidade a transferir � maior que a quantidade dispon�vel!","ATEN��O")
				lRet := .F.
			EndIf  
		EndIf
	EndIf	
EndIf

RestArea(aArea)

Return lRet