#INCLUDE "PROTHEUS.CH"

User Function M261BCHOI()

	Local aButtons := {}

	Aadd(aButtons,{'PRODUTO',{||U_M261SELOP()},OemToAnsi('1o.Nivel'),OemToAnsi('1o.Nivel')}) //"Explode 1o nivel estrutura"
	Aadd(aButtons,{'PRODUTO',{||U_MARCTRA()},OemToAnsi('Marcar Transf'),OemToAnsi('Marcar Transf')}) //"Marca Op's como transferidas"
	Aadd(aButtons,{'PRODUTO',{||U_ESTOTRA()},OemToAnsi('Estorn Transf'),OemToAnsi('Estorn Transf')}) //"Marca Op's como Liberadas"

Return(aButtons)

/*����������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun��o    �M241SeleEs� Autor �Rodrigo de A Sartorio   � Data �21.10.2004���
��������������������������������������������������������������������������Ĵ��
���Descri��o �Seleciona estrutura como base para preenchimento automatico  ���
���          �de produtos a serem requisitados.                            ���
��������������������������������������������������������������������������Ĵ��
��� Uso      � MATA241                                                     ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
����������������������������������������������������������������������������*/
User Function M261SELOP()
Local aEstrutura   := {}
Local i            := 0
Local nx           := 0
Local oDlg
Local lOk          := .T.
//Local aAreaSD4     := SD4->(GetArea())
//Local aAreaSDC     := SDC->(GetArea())
Local nPercOP      := 0
Local lSugSemSld   := .F.
Local lRetorno     := .F.
Local aColsPE      := {}
Local cLocaliz     := "PADRAO" //Criavar("D3_LOCALIZ",.F.)
Local cNumSeri     := Criavar("D3_NUMSERI",.F.)
Local nPosOP    := aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_XOP'})

Private nQtdOrigEs := 1
Private cProdEst   := Criavar("D3_COD",.F.)
Private cOpEst     := Criavar("D3_OP",.F.)
Private nEstru     := 0   

Private nPosCod    := 1

//Private cGrupo    := ""

//�����������������������������������������������������������������Ŀ
//� Funcao utilizada para verificar a ultima versao dos fontes      �
//� SIGACUS.PRW, SIGACUSA.PRX e SIGACUSB.PRX, aplicados no rpo do   |
//| cliente, assim verificando a necessidade de uma atualizacao     |
//| nestes fontes. NAO REMOVER !!!							        �
//�������������������������������������������������������������������
If !(FindFunction("SIGACUS_V") .and. SIGACUS_V() >= 20050512)
	Aviso("Aten��o","Atualizar patch do programa "+"SIGACUS.PRW !!!",{"Ok"})
	Return
EndIf
If !(FindFunction("SIGACUSA_V") .and. SIGACUSA_V() >= 20091015)
	Aviso("Aten��o","Atualizar patch do programa "+"SIGACUSA.PRX !!!",{"Ok"})
	Return
EndIf
If !(FindFunction("SIGACUSB_V") .and. SIGACUSB_V() >= 20091015)
	Aviso("Aten��o","Atualizar patch do programa "+"SIGACUSB.PRX !!!",{"Ok"})
	Return
EndIf

DEFINE MSDIALOG oDlg FROM  140,000 TO 300,400 TITLE "Informe produto com estrutura" PIXEL //"Informe produto com estrutura"
@ 10,15 TO 63,185 LABEL Alltrim(RetTitle("D3_OP"))+" - "+Alltrim(RetTitle("D3_COD"))+" - "+Alltrim(RetTitle("D3_QUANT")) OF oDlg PIXEL	
@ 20,20 MSGET cOpEst F3 "SC2" Picture PesqPict("SD3","D3_OP") When(oSugerSld:lVisible := .T.,SysRefresh(),.T.) Valid ((Vazio() .Or. ExistCpo("SC2",cOpEst)) .And. A261IniOP()) SIZE 70,9 OF oDlg PIXEL
@ 35,20 MSGET cProdEst F3 "SB1" Picture PesqPict("SD3","D3_COD") When( lSugSemSld := If(Empty(cOpEst),.F.,lSugSemSld),oSugerSld:lVisible:=!Empty(cOpEst),SysRefresh(),.T.) Valid (NaoVazio() .And. ExistCpo("SB1",cProdEst)) SIZE 70,9 OF oDlg PIXEL
@ 35,95 MSGET nQtdOrigEs Picture PesqPict("SD3","D3_QUANT") Valid (Positivo() .And. NaoVazio()) SIZE 60,9 OF oDlg PIXEL
@ 50,20 CHECKBOX oSugerSld Var lSugSemSld PROMPT "Sugere itens sem saldo"	SIZE 150,10 OF oDlg PIXEL
@ 50,120 CHECKBOX oRetorno  Var lRetorno   PROMPT OemtoAnsi("Retorno?")	SIZE 150,10 OF oDlg PIXEL
DEFINE SBUTTON FROM 67,131 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg
DEFINE SBUTTON FROM 67,158 TYPE 2 ACTION (oDlg:End(),lOk:=.F.) ENABLE OF oDlg
ACTIVATE MSDIALOG oDlg CENTERED ON INIT(oSugerSld:lVisible := .T.)
                        
//cGrupo  := GETMV("MV_XGRUPOT") //Grupo de Tecido

If lOk .And. !Empty(cProdEst) .And. nQtdOrigEs > 0
	// Le os empenhos caso a op esteja preenchida
	If !Empty(cOPEst)
		dbSelectArea("SC2")
		dbSetOrder(1)
		dbSeek(xFilial()+left(cOpEst,6))
		// Calcula qual o percentual da baixa de insumos
		nPercOp:=nQtdOrigEs/SC2->C2_QUANT
		//dbSelectArea("SDC")
		//dbSetOrder(2)
		dbSelectArea("SD4")
		dbSetOrder(2)
		dbSeek(xFilial()+LEFT(cOpEst,14))
		While SD4->(!Eof()) .And. SD4->D4_FILIAL+SubStr(SD4->D4_OP,1,11)==xFilial("SD4")+SubStr(cOpEst,1,11)
			If !lSugSemSld .AND. QtdComp(SD4->D4_QUANT)==QtdComp(0)
				SD4->(dbSkip())
				Loop
			EndIf
			//����������������������������������������������������������������������������������Ŀ
			//| Produtos de apropriacao indireta, quando ja possuem saldo no armazem de processo |
			//| a baixa e automatica pela producao, quando ainda nao possuem saldo no armazem de |
			//| de processo devera ser feita requisicao manual do armazem padrao.                |
			//������������������������������������������������������������������������������������
			If A260ApropI(SD4->D4_COD)
				SD4->(dbSkip())
				Loop
			EndIf
			/*If cTm > "500"
				If SDC->(dbSeek(xFilial("SDC")+SD4->D4_COD+SD4->D4_LOCAL+SD4->D4_OP+SD4->D4_TRT+SD4->D4_LOTECTL+SD4->D4_NUMLOTE))
					While !SDC->(Eof()) .And. xFilial("SDC")+SD4->D4_COD+SD4->D4_LOCAL+SD4->D4_OP+SD4->D4_TRT+SD4->D4_LOTECTL+SD4->D4_NUMLOTE == SDC->(DC_FILIAL+DC_PRODUTO+DC_LOCAL+DC_OP+DC_TRT+DC_LOTECTL+DC_NUMLOTE)
						AADD(aEstrutura,{NIL,NIL,SD4->D4_COD,Min(SDC->DC_QUANT,(SDC->DC_QTDORIG*nPercOp)),SDC->DC_TRT,SDC->DC_LOTECTL,SDC->DC_NUMLOTE,SDC->DC_LOCALIZ,SDC->DC_NUMSERI,SD4->D4_LOCAL,SD4->D4_DTVALID})
						SDC->(dbSkip())
					End
				Else
					AADD(aEstrutura,{NIL,NIL,SD4->D4_COD,Min(SD4->D4_QUANT,(SD4->D4_QTDEORI*nPercOp)),SD4->D4_TRT,SD4->D4_LOTECTL,SD4->D4_NUMLOTE,cLocaliz,cNumSeri,SD4->D4_LOCAL,SD4->D4_DTVALID})	
				EndIf
			Else*/
				//AADD(aEstrutura,{NIL,NIL,SD4->D4_COD,Min(SD4->D4_QTDEORI*nPercOp,SD4->D4_QTDEORI-SD4->D4_QUANT),SD4->D4_TRT,SD4->D4_LOTECTL,SD4->D4_NUMLOTE,cLocaliz,cNumSeri,SD4->D4_LOCAL,SD4->D4_DTVALID})
			
			DbSelectArea("SB1")
			SB1->(DbSetOrder(1))
			SB1->(DbSeek(xFilial("SB1")+SD4->D4_COD))
			
//			If !(AllTrim(SB1->B1_GRUPO) $ cGrupo)
//				SD4->(dbSkip())
//				Loop
//			EndIf
			
			AADD(aEstrutura,{NIL,NIL,SD4->D4_COD,SD4->D4_QTDEORI,SD4->D4_TRT,SD4->D4_LOTECTL,SD4->D4_NUMLOTE,cLocaliz,cNumSeri,SD4->D4_LOCAL,SD4->D4_DTVALID,SB1->B1_DESC})

			//EndIf
			//dbSelectArea("SD4")
			SD4->(dbSkip())
		End
		//RestArea(aAreaSD4)
		//RestArea(aAreaSDC)
	// Se nao preencheu OP le somente a estrutura
	Else
		// Explode o 1 nivel da estrutura
		A261Explod(cProdEst,nQtdOrigEs,@aEstrutura)
	EndIf
	// Le somente os itens de primeiro nivel
	_n := 0
	aEstrutura := aSort( aEstrutura,,, { |x,y| x[12] < y[12] } )
	
	For i:=1 to Len(aEstrutura)
		// Se a primeira linha esta em branco remove a mesma do acols
		If Empty(aCols[1,nPosCod])
			ADEL(aCols,1)
			ASIZE(aCols,Len(aCols)-1)
		EndIf
		// Adiciona item no acols
		_loc := aScan(aCols,{|x| AllTrim(x[nPosCod]) == alltrim(aEstrutura[i,3])})
		
		If _loc = 0
			AADD(aCols,Array(Len(aHeader)+1))
			_n++
			// Preenche conteudo do acols
			For nx:=1 to Len(aHeader)
				cCampo:=Alltrim(aHeader[nx,2])
				If IsHeadRec(cCampo)
					aCols[Len(aCols)][nx] := 0
				ElseIf IsHeadAlias(cCampo)
					aCols[Len(aCols)][nx] := "SD3"			
				Else
					aCols[Len(aCols)][nx] := CriaVar(cCampo,.F.)
				EndIf
			Next nx
			aCOLS[Len(aCols)][Len(aHeader)+1] := .F.
			_loc := i
			// Preenche campos especificos
			SB1->(dbSetOrder(1))
			SB1->(dbSeek(xFilial("SB1")+aEstrutura[i,3]))
		
			aCols[_n,1] := aEstrutura[i,3]
			aCols[_n,2] := SB1->B1_DESC
			aCols[_n,3] := SB1->B1_UM
			aCols[_n,4] := If(!lRetorno,RetFldProd(SB1->B1_COD,"B1_LOCPAD"),aEstrutura[i,10])
			aCols[_n,6] := aEstrutura[i,3]
			aCols[_n,7] := SB1->B1_DESC
			aCols[_n,8] := SB1->B1_UM
			aCols[_n,9] := If(!lRetorno,aEstrutura[i,10],RetFldProd(SB1->B1_COD,"B1_LOCPAD"))
			aCols[_n,16] := aEstrutura[i,4]
			aCols[_n,10] := "PADRAO"
			aCols[_n,nPosOP] := cOpEst
		Else
			aCols[_loc,16] += aEstrutura[i,4]
		Endif
		
		
		/*
		GDFieldPut("D3_COD",aEstrutura[i,3],Len(aCols))
		GDFieldPut("D3_DESCRI",SB1->B1_DESC,Len(aCols))
		GDFieldPut("D3_UM",SB1->B1_UM,Len(aCols))
		//GDFieldPut("D3_SEGUM",SB1->B1_SEGUM,Len(aCols))
		GDFieldPut("D3_LOCAL",RetFldProd(SB1->B1_COD,"B1_LOCPAD"),Len(aCols))
		//GDFieldPut("D3_QUANT",aEstrutura[i,4],Len(aCols))
		//GDFieldPut("D3_CONTA",SB1->B1_CONTA,Len(aCols))
		//GDFieldPut("D3_ITEMCTA",SB1->B1_ITEMCC,Len(aCols))  
		GDFieldPut("D3_LOCALIZ",Space(15),Len(aCols))
		
		//Fazer de novo
		GDFieldPut("D3_COD",aEstrutura[i,3],Len(aCols))
		GDFieldPut("D3_DESCRI",SB1->B1_DESC,Len(aCols))
		GDFieldPut("D3_UM",SB1->B1_UM,Len(aCols)) 
		GDFieldPut("D3_LOCAL",aEstrutura[i,10],Len(aCols))
		GDFieldPut("D3_LOCALIZ",Space(15),Len(aCols))
		
		GDFieldPut("D3_QUANT",aEstrutura[i,4],Len(aCols))
				              */
	// Preenche o numero da OP
	//If !Empty(cOpEst)
	//	If aEstrutura[i,10]# NIL .AND. !Empty(aEstrutura[i,10])
	//			GDFieldPut("D3_LOCAL",aEstrutura[i,10],Len(aCols))
	//		EndIf
	//		GDFieldPut("D3_OP",cOpEst,Len(aCols))
//			GDFieldPut("D3_TRT",aEstrutura[i,5],Len(aCols))
	//GDFieldPut("D3_LOTECTL",aEstrutura[i,6],Len(aCols))
	//GDFieldPut("D3_NUMLOTE",aEstrutura[i,7],Len(aCols))
	//GDFieldPut("D3_DTVALID",aEstrutura[i,11],Len(aCols))
	//If ! Empty(cTM) .and. SF5->F5_TIPO=="D"
	//GDFieldPut("D3_LOCALIZ",Space(15),Len(aCols))
	//GDFieldPut("D3_NUMSERI",Space(20),Len(aCols))
	//Else
	//	GDFieldPut("D3_LOCALIZ",aEstrutura[i,8],Len(aCols))
	//	GDFieldPut("D3_NUMSERI",aEstrutura[i,9],Len(aCols))
	//EndIf
	//EndIf
	// Executa gatilhos e validacoes
	If ExistTrigger("D3_COD")
		RunTrigger(2,Len(aCols),,"D3_COD")
	EndIf
	If ExistTrigger("D3_QUANT")
		RunTrigger(2,Len(aCols),,"D3_QUANT")
	EndIf
	If ExistTrigger("D3_LOCAL")
		RunTrigger(2,Len(aCols),,"D3_LOCAL")
	EndIf
	If ExistTrigger("D3_OP")
		RunTrigger(2,Len(aCols),,"D3_OP")
	EndIf
Next i
EndIf

If ExistBlock('MT241SE')
	aColsPE := Execblock('MT241SE', .F., .F., aCols)
	If Valtype(aColsPE) == 'A'
		aCols := aClone(aColsPE)
	EndIf
EndIf

Return

/*����������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun��o    �A241IniOP � Autor �Rodrigo de A Sartorio   � Data �21.10.2004���
��������������������������������������������������������������������������Ĵ��
���Descri��o �Inicializa informacoes da Ordem de Producao apos digitar OP  ���
��������������������������������������������������������������������������Ĵ��
��� Uso      � MATA241                                                     ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
����������������������������������������������������������������������������*/
Static Function A261IniOP()
// Inicializa produto e quantidade
If !Empty(cOPEst) .And. SC2->(MsSeek(xFilial("SC2")+cOpEst))
	nQtdOrigEs:=SC2->C2_QUANT-SC2->C2_QUJE
	cProdEst:=SC2->C2_PRODUTO
EndIf
Return

Static Function A261Explod(cProduto,nQuant,aNewStruct)
Local nX		     := 0
Local aAreaAnt	 := GetArea()
Local aArrayAux  := {}

Default cProduto := ""
Default nQuant	 := 0

//Variavel publica declarada na funcao M241SeleEs()
nEstru := 0

aArrayAux := Estrut(cProduto,nQuant,.T.)

dbSelectArea("SB1")
dbSetOrder(1)

//���������������������������������������������������������Ŀ
//| Processa todos os componentes do 1 nivel da estrutura,  |
//| verificando a existencia de produtos fantasmas.         |
//�����������������������������������������������������������
For nX := 1 to Len(aArrayAux)
	If dbSeek(xFilial("SB1")+aArrayAux[nx,3]) //Filial+Componente
		If SB1->B1_FANTASM $ "S"
			A261Explod(aArrayAux[nx,3],aArrayAux[nx,4],aNewStruct) //Componente+Qtde
		Else
			aAdd(aNewStruct,aArrayAux[nx])
		EndIf
	EndIf
Next nX

RestArea(aAreaAnt)
Return Nil


User Function MOVDAP()

Local nx := 0

	_qry := "Select B2_COD, B2_XRESERV-B2_QATU, " 
	_qry += "(Select Top 1 BF_LOCALIZ from SBF010 SBF where SBF.D_E_L_E_T_ = '' and BF_PRODUTO = B2_COD and BF_LOCAL = 'E1' order by BF_NUMLOTE, BF_LOCALIZ) as ENDE, "
	_qry += "(Select BE_LOCALIZ from SBE010 SBE where SBE.D_E_L_E_T_ = '' and BE_CODPRO = B2_COD and BE_LOCAL = 'E0') as ENDDEST, "
	_qry += "(Select Top 1 BF_NUMLOTE from SBF010 SBF where SBF.D_E_L_E_T_ = '' and BF_PRODUTO = B2_COD and BF_LOCAL = 'E1' order by BF_NUMLOTE, BF_LOCALIZ) as SLOTE, "
	_qry += "(Select Top 1 B8_DTVALID from SB8010 SB8 where SB8.D_E_L_E_T_ = '' and B8_PRODUTO = B2_COD and B8_LOCAL = 'E1' and B8_LOTECTL = '000000' and B8_NUMLOTE = (Select Top 1 BF_NUMLOTE from SBF010 SBF where SBF.D_E_L_E_T_ = '' and BF_PRODUTO = B2_COD and BF_LOCAL = 'E1' order by BF_NUMLOTE, BF_LOCALIZ) ) as VALID, "
	_qry += "(Select Top 1 BF_QUANT from SBF010 SBF where SBF.D_E_L_E_T_ = '' and BF_PRODUTO = B2_COD and BF_LOCAL = 'E1' order by BF_NUMLOTE, BF_LOCALIZ) as QUANT "
	_qry += "from SB2010 SB2 WITH (NOLOCK) "
	_qry += "where SB2.D_E_L_E_T_ = '' and B2_XRESERV > B2_QATU and "
	_qry += "(Select Top 1 BF_QUANT from SBF010 SBF where SBF.D_E_L_E_T_ = '' and BF_PRODUTO = B2_COD and BF_LOCAL = 'E1' order by BF_NUMLOTE, BF_LOCALIZ)  is not null "

	_qry := ChangeQuery(_qry)
	If Select("TMPX") > 0 
		TMPX->(DbCloseArea()) 
  	EndIf 
					
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPX",.T.,.T.)
	DbSelectArea("TMPX")
	DbGoTop()

	_n := 0
	While !EOF()
		_n++
		// Se a primeira linha esta em branco remove a mesma do acols
		If Empty(aCols[1,1])
			ADEL(aCols,1)
			ASIZE(aCols,Len(aCols)-1)
		EndIf
		// Adiciona item no acols

		AADD(aCols,Array(Len(aHeader)+1))

			// Preenche conteudo do acols
		For nx:=1 to Len(aHeader)
			cCampo:=Alltrim(aHeader[nx,2])
			If IsHeadRec(cCampo)
				aCols[Len(aCols)][nx] := 0
			ElseIf IsHeadAlias(cCampo)
				aCols[Len(aCols)][nx] := "SD3"			
			Else
				aCols[Len(aCols)][nx] := CriaVar(cCampo,.F.)
			EndIf
		Next nx
		aCOLS[Len(aCols)][Len(aHeader)+1] := .F.

		// Preenche campos especificos
		SB1->(dbSetOrder(1))
		SB1->(dbSeek(xFilial("SB1")+TMPX->B2_COD))
		
		aCols[_n,1] := TMPX->B2_COD
		aCols[_n,2] := SB1->B1_DESC
		aCols[_n,3] := SB1->B1_UM
		aCols[_n,4] := 'E1'
		aCols[_n,5] := TMPX->ENDE
		aCols[_n,6] := TMPX->B2_COD
		aCols[_n,7] := SB1->B1_DESC
		aCols[_n,8] := SB1->B1_UM
		aCols[_n,9] := 'E0'
		aCols[_n,10]:= TMPX->ENDDEST
		aCols[_n,12]:= "000000"
		aCols[_n,13]:= TMPX->SLOTE
		aCols[_n,14]:= stod(TMPX->VALID)
		aCols[_n,16] := TMPX->QUANT
		aCols[_n,20] := "000000"
		aCols[_n,21] := stod(TMPX->VALID)
		
		
		If ExistTrigger("D3_COD")
			RunTrigger(2,Len(aCols),,"D3_COD")
		EndIf
		If ExistTrigger("D3_QUANT")
			RunTrigger(2,Len(aCols),,"D3_QUANT")
		EndIf
		If ExistTrigger("D3_LOCAL")
			RunTrigger(2,Len(aCols),,"D3_LOCAL")
		EndIf
		If ExistTrigger("D3_OP")
			RunTrigger(2,Len(aCols),,"D3_OP")
		EndIf
		If ExistTrigger("D3_LOTECTL")
			RunTrigger(2,Len(aCols),,"D3_LOTECLT")
		EndIf

		
		DbSelectArea("TMPX")
		DbSkip()
	End

Return         

/*/{Protheus.doc} MARCTRA
	(long_description)
	@type  Function
	@author user
	@since 02/08/2021
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	@example
	(examples)
	@see (links_or_references)
	/*/
User Function MARCTRA()

Local _qry := ""
Local _i := 0
Local nPosOP    := aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_XOP'})
Local lAlt := .F.
Local nCount := 0


cTpLib  := AllTrim(SuperGetMV("MV_XTPNLB",.F.,"MI|PI|SV")) // Tipo de Produtos que n�o devem ser considerados na libera��o da OP
xTpLib := StrTran(cTpLib,"|","','")

If !Empty(aCols[1][1])

	For _i := 1 to len(aCols) 
		If  ACOLS[_i,len(AHEADER)+1] == .F.
			nCount++
		EndIf
		cSequen := Right(Left(aCols[_i,nPosOP],11),3)	
	Next

		If POSICIONE("SC2",1,xFilial("SC2")+alltrim(aCols[1,nPosOP]),"C2_XRES") == "T"
			Alert("Ordem j� transferida !!!", "TOTVS")
		ElseIf	POSICIONE("SC2",1,xFilial("SC2")+alltrim(aCols[1,nPosOP]),"C2_XRES") == " "
				Alert("Ordem n�o liberada !!!", "TOTVS")
		ElseIf	POSICIONE("SC2",1,xFilial("SC2")+alltrim(aCols[1,nPosOP]),"C2_XRES") == "L"		
			If ncount == 0 .AND. !Empty(aCols[1][1])
				If MsgYesNo("Deseja realmente marcar a sequ�ncia "+cSequen+" como transferida ? ","MARCTRA")
					_qry := "update "+RetSqlName("SC2")+" set C2_XRES = 'T' where D_E_L_E_T_ = '' and C2_FILIAL = '"+xfilial("SC2")+"' and C2_NUM+C2_ITEM+C2_SEQUEN = '"+left(aCols[1,nPosOP],11)+"' "
					TcSqlExec(_qry)
					lAlt := .T.
				Else
					Msgalert("Cancelado pelo usuario","Hope")
				Endif	
			EndIf

			If lAlt
				MsgAlert("Sequ�ncia marcada como transferida com sucesso !!!", "TOTVS")
			ElseIf ncount > 0 .AND. !Empty(aCols[1][1])
				Alert("Existem itens a serem transferidos !!!", "TOTVS")
			EndIf
		EndIf	
Else
	Alert("N�o foram selecionados itens para transferir !!!", "TOTVS")
EndIf

Return

User Function ESTOTRA()

Local _qry := ""
Local _i := 0
Local nPosOP    := aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_XOP'})
Local lAlt := .F.
Local nCount := 0

cTpLib  := AllTrim(SuperGetMV("MV_XTPNLB",.F.,"MI|PI|SV")) // Tipo de Produtos que n�o devem ser considerados na libera��o da OP
xTpLib := StrTran(cTpLib,"|","','")

If !Empty(aCols[1][1])

	For _i := 1 to len(aCols) 
		If  ACOLS[_i,len(AHEADER)+1] == .F.
			nCount++
		EndIf
		cSequen := Right(Left(aCols[_i,nPosOP],11),3)	

	Next

		If POSICIONE("SC2",1,xFilial("SC2")+alltrim(aCols[1,nPosOP]),"C2_XRES") == "L"
			Alert("Ordem n�o transferida !!!", "TOTVS")
		ElseIf	POSICIONE("SC2",1,xFilial("SC2")+alltrim(aCols[1,nPosOP]),"C2_XRES") == " "
				Alert("Ordem n�o liberada !!!", "TOTVS")
		ElseIf	POSICIONE("SC2",1,xFilial("SC2")+alltrim(aCols[1,nPosOP]),"C2_XRES") == "T"		
			If ncount == 0 .AND. !Empty(aCols[1][1])
				If MsgYesNo("Deseja realmente estornar a sequ�ncia "+cSequen+"  ? ","ESTOTRA")
					_qry := "update "+RetSqlName("SC2")+" set C2_XRES = 'L' where D_E_L_E_T_ = '' and C2_FILIAL = '"+xfilial("SC2")+"' and C2_NUM+C2_ITEM+C2_SEQUEN = '"+left(aCols[1,nPosOP],11)+"' "
					TcSqlExec(_qry)
					lAlt := .T.
				Else
					Msgalert("Cancelado pelo usuario","Hope")
				Endif	
			EndIf

			If lAlt
				MsgAlert("Sequ�ncia estornada com sucesso !!!", "TOTVS")
			ElseIf ncount > 0 .AND. !Empty(aCols[1][1])
				Alert("Existem itens a serem transferidos !!!", "TOTVS")
			EndIf
		EndIf	
Else
	Alert("N�o foram selecionados itens para estornar !!!", "TOTVS")
EndIf

Return

