#include "protheus.ch"
#include "rwmake.ch"


User Function A200BOK()

aRegs := PARAMIXB[1]
nOpcX := PARAMIXB[3]
lret  :=    .T.

if nOpcX == 4 .AND. __cuserid $ GETMV("HP_USERGRA")
    lret := .T.
else
    MsgAlert("Apenas o setor de produtos, pode realizar altera��es na estrutura","Hope")
    lret := .F.
endif

if nOpcX == 3
    Msginfo("Toda inclus�o deve ser feita pela equipe de produtos na rotina de cadastro de grades","Hope")
    lret := .F.
endif

Return(lret) // aceito as alteracoes. Para nao aceitar, retornar .F.