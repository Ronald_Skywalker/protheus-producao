#Include "RwMake.ch"
#Include "Protheus.ch"
#Include "Topconn.ch"
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MVIEWSALDO� Autor �Ricardo A. Batagella� Data �  01/05/14   ���
�������������������������������������������������������������������������͹��
���Desc.     �Ponto de entrada para correcao de valores de saldo:         ���
���          � consulta pedidos em aberto e  corrige valor empenhado      ���
���          � Saldo atual corrigido com valor empenhado                  ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico RARITUBOS.                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function MVIEWSALDO()
Local aArea   := GetArea()
Local cProd   := PARAMIXB[1]
Local cLocal  := PARAMIXB[2]
Local aSaldo  := PARAMIXB[3]
Local aRet    := {}
Local nQtdRes := 0
Local nQtdDis := 0

DbSelectArea("SB2")
DbSetOrder(2)
If DbSeek(xFilial("SB2")+cLocal+cProd)
	nQtdRes := SB2->B2_RESERVA + SB2->B2_XRESERV  //Qtde Reservada
	nQtdDis := SB2->B2_QATU - SB2->B2_QEMP - nQtdRes 
EndIf

/*
PARAMIXB3[1,1] -> Qtd. Disponivel (SaldoSB2())
PARAMIXB3[1,2] -> Saldo Atual (B2_QATU)
PARAMIXB3[1,3] -> Qtd. Pedido de Vendas (B2_QPEDVEN)
PARAMIXB3[1,4] -> Qtd. Empenhada (B2_QEMP)
PARAMIXB3[1,5] -> Qtd. Prevista Entrada (B2_SALPEDI)
PARAMIXB3[1,6] -> Qtd. Empenhada S.A. (B2_QEMPSA)
PARAMIXB3[1,7] -> Qtd. Reservada (B2_RESERVA)
PARAMIXB3[1,8] -> Qtd. Ter.Ns.Pd. (B2_QTNP)
PARAMIXB3[1,9] -> Qtd. Ns.Pd.Ter (B2_QNPT)
PARAMIXB3[1,10] -> Saldo Poder 3 (B2_QTER)
PARAMIXB3[1,11] -> Qtd. Emp. NF (B2_QEMPN)
PARAMIXB3[1,12] -> Qdt. a Enderecar (B2_QACLASS)
*/

If Len(aSaldo) > 0 
	aSaldo[1,7] := nQtdRes
	aSaldo[1,1] := nQtdDis
    aAdd(aRet,{	aSaldo[1,01],;
				aSaldo[1,02],;
				aSaldo[1,03],;
				aSaldo[1,04],;
				aSaldo[1,05],;
				aSaldo[1,06],;
				aSaldo[1,07],;
				aSaldo[1,08],;
				aSaldo[1,09],;
				aSaldo[1,10],;
				aSaldo[1,11],;
				aSaldo[1,12],;
				aSaldo[1,13],;
				aSaldo[1,14]})
Endif

RestArea(aArea)

Return(aRet)