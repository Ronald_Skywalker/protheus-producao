#include 'protheus.ch'
#include 'parmtype.ch'

//Ponto de Entrada - Amarra��o com o Picking do produto no cadastro de Endere�os.
user function MT015TOK()

Local cQuery  := " "
Local lRet    := .T.
Local cCodPro := M->BE_CODPRO
Local cLocal  := M->BE_LOCAL

	cQuery := " SELECT BE_CODPRO FROM "+RetSqlName("SBE")+" WHERE BE_CODPRO = '"+Alltrim(cCodPro)+"' AND BE_LOCAL = '"+Alltrim(cLocal)+"' AND D_E_L_E_T_ = '' "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP1",.T.,.T.)
	DbSelectArea("TMP1")

	IF !EMPTY(TMP1->BE_CODPRO)		
		MsgAlert("Produto j� possui endere�o no "+Alltrim(cLocal)+"!","HOPE")
		lRet := .F.		
	ELSE
		lRet := .T.		
	ENDIF

	TMP1->(DbCloseArea())
	
return lRet 