#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} Ponto de Entrada
	(Valida��o da reserva customizada na Movimenta��o Simples)
	@type  Function
	@author Ronaldo pereira
	@since 12/04/2020
	@version version 1.0
	@return lRet (logico) 
/*/
user function MT240TOK()

Local aArea := GetArea()
Local lRet  := .T.
Local nDisp := 0
Local nTM   := VAL(M->D3_TM)	
Local cProd := M->D3_COD
Local cLocal := M->D3_LOCAL
Local nQuant := M->D3_QUANT 

If nTM > 499
	If AllTrim(cLocal) $ "E0/QL/A1"
		DbSelectArea("SB2")
		DbSetOrder(1)
		If DbSeek(xFilial("SB2")+cProd+cLocal)
			nDisp := SB2->B2_QATU-(SB2->B2_RESERVA+SB2->B2_XRESERV+SB2->B2_XRES)			
			If nQuant > nDisp
				MsgAlert("A quantidade a transferir � maior que a quantidade dispon�vel!","ATEN��O")
				lRet := .F.
			EndIf  
		EndIf
	EndIf
EndIf	

RestArea(aArea) 

Return lRet
	
