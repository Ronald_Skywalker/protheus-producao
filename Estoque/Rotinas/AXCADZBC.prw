#include 'protheus.ch'
#include 'parmtype.ch'

#Define NXTLINE		Chr(13)+Chr(10)
/*---------------------------------------------------------------------*
| Func:  AXCADZBC                                                      |
| Autor: Robson Melo                                                   |
| Data:  02/06/2021                                                    |
| Desc:  Rotina de Cadastro de Stopper                                 |
*---------------------------------------------------------------------*/
User Function AXCADZBC()

Local aArea    := GetArea()
Local aAreaZBC := ZBC->(GetArea())
Local cDelOk   := "U_HPDELTOP()"
Local cFunTOk  := "U_HPALTSTP()"
Private aRotina := {}

//AADD(aRotina, {"Visualizar","AxVisual",0,2})
//AADD(aRotina, {"Incluir","AxInclui",0,3})
//AADD(aRotina, {"Alterar","U_HPALTSTP()",0,4})
//AADD(aRotina, {"Alterar","AxAltera",0,4})
//AADD(aRotina, {"Excluir","U_HPDELTOP()",0,5})
//AADD(aRotina, { "Importa Stopper", "U_HPSTOPPER()" , 0, 3})

If !(__cuserid) $ GETMV("HP_USERGRA") 
	Alert("Apenas o setor de produtos, pode realizar importa��o de Stopper!!")
	return()
EndIf

//Chamando a tela de cadastros
AxCadastro('ZBC', 'Stopper', cDelOk, cFunTOk)
	
RestArea(aAreaZBC)
RestArea(aArea)


Return

/*---------------------------------------------------------------------*
| Func:  AXCADZBD                                                      |
| Autor: Robson Melo                                                   |
| Data:  02/06/2021                                                    |
| Desc:  Rotina de Cadastro de Stopper                                 |
*---------------------------------------------------------------------*/
User Function AXCADZBD()

    Local aArea    := GetArea()
    Local aAreaZBD := ZBD->(GetArea())
    Local cDelOk   := ".T."
    Local cFunTOk  := ".T."
    Private aRotina := {}

    AADD(aRotina, {"Pesquisar","AxPesqui",0,1})
    AADD(aRotina, {"Visualizar","AxVisual",0,2})
    AADD(aRotina, {"Incluir","AxInclui",0,3})
    AADD(aRotina, {"Alterar","AxAltera",0,4})
    AADD(aRotina, {"Excluir","AxDeleta",0,5})
    AADD(aRotina, { "Importa Stopper", "U_HPSTOPPER()" , 0, 3})

    If !(__cuserid) $ GETMV("HP_USERGRA") 
        Alert("Apenas o setor de produtos, pode realizar importa��o de Stopper!!")
        return()
    EndIf

    //Chamando a tela de cadastros
    AxCadastro('ZBD', 'Stopper', cDelOk, cFunTOk)
     
    RestArea(aAreaZBD)
    RestArea(aArea)

Return
/*---------------------------------------------------------------------*
| Func:  HPSTOPPER                                                     |
| Autor: Robson Melo                                                   |
| Data:  02/06/2021                                                    |
| Desc:  Rotina de importa��o de Stopper                               |
*---------------------------------------------------------------------*/

User Function HPSTOPPER() 

Local cDiret  := ""
Local cLinha  := ""
Local cStopper:= ""
Local cStopNulo  := ""
Local cProdNulo  := ""
Local cDupli     := ""
Local lPrimlin   := .T.
Local lImport1	 := .F.
Local aCampos := {}
Local aDados  := {}
Local nR
Local nI
Local nX 
Local lRec

Private aErro := {}
 

If !(__cuserid) $ GETMV("HP_USERGRA") 
	Alert("Apenas o setor de produtos, pode realizar importa��o de Stopper!!")
	return()
EndIf

cPerg := "HPSTOPPER"   

/*--------------------------*
| Layout?  = MV_PAR01       |                    
*--------------------------*/

Pergunte(cPerg, .T.)

cDiret :=  cGetFile( 'Arquito CSV|*.csv| Arquivo TXT|*.txt| Arquivo XML|*.xml',; //[ cMascara], 
                         'Selecao de Arquivos',;                  //[ cTitulo], 
                         0,;                                      //[ nMascpadrao], 
                         'C:\',;                                  //[ cDirinicial], 
                         .F.,;                                    //[ lSalvar], 
                         GETF_LOCALHARD  + GETF_NETWORKDRIVE,;    //[ nOpcoes], 
                         .T.)     

                        // cDiret := cGetFile( '*.*' , 'Arquivos', 1, 'C:\', .F., nOR( GETF_LOCALHARD, GETF_LOCALFLOPPY, GETF_RETDIRECTORY ),.T., .T. ) 
If Empty( cDiret )
    MsgInfo( 'Arquivo Vazio!', 'VAZIO' )
    Return
Endif

FT_FUSE(cDiret)
ProcRegua(FT_FLASTREC())
FT_FGOTOP()

While !FT_FEOF()
 
	IncProc("Lendo arquivo...")
 
	cLinha := FT_FREADLN()
 
	If lPrimlin
		aCampos := Separa(cLinha,";",.T.)
		lPrimlin := .F.
	Else
		AADD(aDados,Separa(cLinha,";",.T.))
	EndIf
 
	FT_FSKIP()

EndDo
 
Begin Transaction

	ProcRegua(Len(aDados))

	For nI:=1 to Len(aDados)

		lStpVazio:= .F.
		cStopper:= ""
		cDscStop:= ""
 
		IncProc("Importando Registros...")
 
		dbSelectArea("ZBD")
		dbSetOrder(1)
		dbGoTop()

		DbSelectArea("SB1")
		DbSetOrder(1)
		If SB1->(DbSeek(xfilial("SB1")+PADL(aDados[nI,1],11)))
				
			If ZBD->(dbSeek(xFilial("ZBD")+aDados[nI,1]))
				lRec:= .F.
			Else
				lRec:= .T.
			EndIf

			aStopper:=  Separa(aDados[nI,2],"-",.T.) 
			dbSelectArea("ZBC")
			dbSetOrder(1)

			cDupli:= fVldDup(aStopper)

			If !Empty(cDupli)  .AND. (MV_PAR01 == 1 .or. MV_PAR01 == 3)
				Alert("Stopper(s) " + cDupli+ ' duplicados.')
				Exit
			Endif

			For nR:= 1 to Len(aStopper)
				ZBC->(dbGoTop())
				If ZBC->(DbSeek(xfilial("ZBC")+aStopper[nR])) .and. !Empty(aStopper[nR])
					cStopper:= cStopper + aStopper[nR] + '-'
					cZbcDsc:= POSICIONE("ZBC",1,xFilial("ZBC")+aStopper[nR],"ZBC_DESCRI") 
					cDscStop += Iif(Empty(cZbcDsc),'',cZbcDsc + NXTLINE)
				else
					If !Empty(aStopper[nR])
						cStopNulo+=  Alltrim(aStopper[nR])	 + '/'
					Endif
				Endif
			Next nR

			If Empty(cStopper) .AND. (MV_PAR01 == 1 .or. MV_PAR01 == 3)
				lStpVazio:= .T.
			Endif

			If !lStpVazio
				Reclock("ZBD",lRec)

					lImport1:= .T.

					ZBD->ZBD_FILIAL := xFilial("ZBD")

					For nX:=1 to Len(aCampos)

						cCampo  := "ZBD->" + aCampos[nX]

						If aCampos[nX] == 'ZBD_STOPPE'


							If MV_PAR01 == 1 .or. MV_PAR01 == 3
								&cCampo := cStopper
								ZBD->ZBD_DESC := cDscStop
							Endif

						Endif
						
						If nX == 1 
							&cCampo := Alltrim(aDados[nI,nX])	   
						Elseif nX == 3
							If MV_PAR01 == 2 .or. MV_PAR01 == 3
								&cCampo := StrTran(StrTran(Alltrim(aDados[nI,nX]),'|', NXTLINE),'"','')
							Endif
						Endif
					Next nX

				ZBD->(MsUnlock())
			Endif
		else
			cProdNulo+= aDados[nI,1] + '/'
		Endif
	Next nI

End Transaction



If  MV_PAR01 == 2
	If Empty(cProdNulo) 
		ApMsgInfo("Importa��o finalizada com sucesso!","Sucesso!")
	Else
		ApMsgInfo("Os produtos: "+cProdNulo+" n�o foram encontrados." + NXTLINE + 'Cadastre os c�digos e importe novamente',"TOTVS")
	Endif
Else
	If !Empty(cProdNulo)
		ApMsgInfo("Os produtos: "+cProdNulo+" n�o foram encontrados." + NXTLINE + 'Cadastre os c�digos e importe novamente',"TOTVS")
	ENDIF
	If !Empty(cStopNulo)
		ApMsgInfo("Os c�digos de stopper: "+cStopNulo+" n�o foram encontrados." + NXTLINE + 'Cadastre os c�digos e importe novamente',"TOTVS")		
	Endif

	If Empty(cProdNulo) .and. Empty(cStopNulo)
		ApMsgInfo("Importa��o finalizada com sucesso!","Sucesso!")	
	Endif
Endif 

/*


If Empty(cStopNulo) .and. Empty(cProdNulo) 

	ApMsgInfo("Importa��o conclu�da com sucesso!","Sucesso!")

Elseif  MV_PAR01 == 1 .or. MV_PAR01 == 3 

	If !Empty(cStopNulo)
	Endif

Else
	If Empty(cProdNulo) 
		ApMsgInfo("Importa��o conclu�da, por�m os c�digos de stopper: "+cStopNulo+" n�o foram encontrados." + NXTLINE + 'Cadastre os c�digos e importe novamente',"Sucesso!")
	Else		
		ApMsgInfo("Importa��o conclu�da, por�m os c�digos de stopper: "+cStopNulo+" n�o foram encontrados." + NXTLINE + "Produtos: "+cProdNulo+" n�o foram encontrados." + NXTLINE + 'Cadastre os c�digos e importe novamente',"Sucesso!")
	Endif
Endif*/

Return

/*---------------------------------------------------------------------*
| Func:  SXBEZBC                                                       |
| Autor: Robson Melo                                                   |
| Data:  11/06/2021                                                    |
| Desc:  Consulta Espec�fica de Stopper (SXB)                          |
*---------------------------------------------------------------------*/
User Function SXBEZBC(l1Elem,lTipoRet)

	Local MvPar
	Local aArea  := GetArea()
	Local cTitulo := ""
	Local lRet  := .T.
	Local aBox  := {}
	Local nTam  := 6
	Local MvParDef := ""
    Local cDscStop := ''
	Public pStopper := ""

	MvPar:=&(Alltrim(ReadVar()))   // Carrega Nome da Variavel do Get em Questao
	mvRet:=Alltrim(ReadVar())      // Iguala Nome da Variavel ao Nome variavel de Retorno

	dbSelectArea("ZBC")
	DbSetOrder(1)
	DbGoTop()
	cTitulo := "Stopper"
	While !Eof()
		Aadd(aBox,ZBC->ZBC_CODIGO + "- " + Alltrim(ZBC->ZBC_DESCRI))
		MvParDef += SubStr(ZBC->ZBC_CODIGO,1,5)+"-"
		dbSkip()
	EndDo

	If f_Opcoes( @MvPar,;  // uVarRet
		cTitulo,;  // cTitulo
		@aBox,;   // aOpcoes
		MvParDef,;  // cOpcoes
		,;    // nLin1
		,;    // nCol1
		,;    // l1Elem
		nTam,;    // nTam
		Len( aBox ),; // nElemRet
		,;    // lMultSelect
		,;    // lComboBox
		,;    // cCampo
		,;    // lNotOrdena
		,;    // NotPesq
		,;   // ForceRetArr
		)    // F3

		&MvRet := mvpar  // Devolve Resultado
	EndIf

	pStopper := StrTran(mvpar,'*','')

	RestArea(aArea)

Return lRet

/*---------------------------------------------------------------------*
| Func:  HPGATZBC                                                      |
| Autor: Robson Melo                                                   |
| Data:  11/06/2021                                                    |
| Desc:  Gatilha descri��o do Stopper no campo memo                    |
*---------------------------------------------------------------------*/
User Function HPGATZBC()

Local cDscStop:= ''
Local cStopper:= Alltrim(M->ZBD_STOPPE)
Local nX	  := 1
Local aStopper:= {}

If !Empty(cStopper) 
    cStopper	:= STRTRAN(cStopper,"*","")
    If Empty(cStopper)
        cDscStop	:= ''
    Else
        aStopper:= Separa(cStopper,"-",.T.)
        For nX := 1 to Len(aStopper)
			If !Empty(aStopper[nX])
            	cDscStop	+= POSICIONE("ZBC",1,xFilial("ZBC")+aStopper[nX],"ZBC_DESCRI") + NXTLINE
			Endif
        Next
    EndIf
Endif

Return cDscStop

/*---------------------------------------------------------------------*
| Func:  HPDELTOP                                                      |
| Autor: Robson Melo                                                   |
| Data:  11/06/2021                                                    |
| Desc:  Valida se o stopper j� foi utilizado para barrar a exclus�o   |
*---------------------------------------------------------------------*/
User Function HPDELTOP()
Local cAlias := GetNextAlias()
Local lRet   := .T.

cQuery := ""
cQuery += " SELECT * "
cQuery += "   FROM "+ RetSqlName("ZBD") +" (NOLOCK) "
cQuery += "  WHERE ZBD_FILIAL  =  '"+ xFilial("ZBD") +"' "
cQuery += "    AND ZBD_STOPPE LIKE '%"+ZBC->ZBC_CODIGO+"%' "
cQuery += "    AND D_E_L_E_T_ =  ' ' "
DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)

If !(cAlias)->( Eof())
	lRet:= .F.
EndIf

If lRet
	Reclock("ZBC",.F.)
	dbDelete()
	MsUnlock("ZBC")
	//axDeleta("ZBC",ZBC->(RecNo()),5)
	MsgInfo("Stopper exclu�do com sucesso!")
Else
	Alert("Stoppers j� vinculados a um produto n�o poder�o ser exclu�dos!")
Endif

(cAlias)->( DbCloseArea() )

Return lRet



/*---------------------------------------------------------------------*
| Func:  STOPPEROK                                                     |
| Autor: Robson Melo                                                   |
| Data:  30/06/2021                                                    |
| Desc:  Valida se o c�digo do Stopper foi preenchido corretamente     |
*---------------------------------------------------------------------*/
User Function STOPPEROK()
Local lRet := .T.
Local nR   := 0
Local nZ   := 0
Local nU   := 0
Local cStopNulo	:= ''
Local cStpAtu	:= ''
Local cStopRep	:= ''

aStopper:=  Separa(M->ZBD_STOPPE,"-",.T.) 

dbSelectArea("ZBC")
dbSetOrder(1)

For nR:= 1 to Len(aStopper)
	ZBC->(dbGoTop())
	If !ZBC->(DbSeek(xfilial("ZBC")+aStopper[nR])) .and. !Empty(aStopper[nR])
		cStopNulo+= Alltrim(aStopper[nR]) + '/'
		lRet := .F.
	Endif
Next nR

If !lRet
	Alert("Stopper(s) " + cStopNulo+ ' n�o cadastrado(s)')
Endif

cDupli:= fVldDup(aStopper)

IF !Empty(cDupli)
	lRet:= .F.
	Alert("Stopper(s) " + cDupli+ ' duplicados.')
Endif

Return lRet

/*---------------------------------------------------------------------*
| Func:  fVldDup                                                       |
| Autor: Robson Melo                                                   |
| Data:  01/07/2021                                                    |
| Desc:  Valida se o c�digo do Stopper foi duplicado                   |
*---------------------------------------------------------------------*/
Static Function fVldDup(aStopper)
Local lRet := .T.
Local nZ   := 0
Local nU   := 0
Local cStpAtu	:= ''
Local cStopRep	:= ''

For nZ:= 1 to Len(aStopper)
	cStpAtu:= aStopper[nZ]
	For nU:= 1 to Len(aStopper)
		If Alltrim(cStpAtu) == aStopper[nU] .AND. nU <> nZ
			cStopRep+= Alltrim(cStpAtu) + '/'
			lRet := .F.
		Endif
	Next nU
Next nZ

Return cStopRep

/*---------------------------------------------------------------------*
| Func:  STPPRDOK                                                      |
| Autor: Robson Melo                                                   |
| Data:  30/06/2021                                                    |
| Desc:  Valida se o c�digo do Stopper foi preenchido corretamente     |
*---------------------------------------------------------------------*/
User Function STPPRDOK()
Local lRet := .T.

DbSelectArea("SB1")
DbSetOrder(1)

If !SB1->(DbSeek(xfilial("SB1")+PADL(M->ZBD_PRODUT,11)))
	lRet := .F.	
	Alert("Produto " + M->ZBD_PRODUT+ ' n�o cadastrado')	
Endif

Return lRet




/*---------------------------------------------------------------------*
| Func:  HPALTSTP                                                      |
| Autor: Robson Melo                                                   |
| Data:  11/06/2021                                                    |
| Desc:  Altera todas as descri��es    |
*---------------------------------------------------------------------*/
User Function HPALTSTP()
Local cAlias := GetNextAlias()
Local lRet   := .T.

If ALTERA
	cQuery := ""
	cQuery += " SELECT R_E_C_N_O_ AS RECZBD, * "
	cQuery += "   FROM "+ RetSqlName("ZBD") +" (NOLOCK) "
	cQuery += "  WHERE ZBD_FILIAL  =  '"+ xFilial("ZBD") +"' "
	cQuery += "    AND ZBD_STOPPE LIKE '%"+ZBC->ZBC_CODIGO+"%' "
	cQuery += "    AND D_E_L_E_T_ =  ' ' "
	DbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .F., .T.)

	If (cAlias)->( Eof())
		Reclock("ZBC",.F.)
		ZBC->ZBC_DESCRI:= M->ZBC_DESCRI
		ZBC->(MsUnlock())
		Return lRet
	EndIf

	While (cAlias)->(!Eof())

		dbSelectArea("ZBD")
		dbSetOrder(1)
		ZBD->(dbGoTo((cAlias)->RECZBD))

		cDescAnt:= ZBC->ZBC_DESCRI
		cDescComp:= ZBD->ZBD_DESC

		cNewDesc:= STRTRAN(cDescComp,cDescAnt,M->ZBC_DESCRI)
		
		Reclock("ZBD",.F.)
		ZBD->ZBD_DESC:= cNewDesc
		ZBD->(MsUnlock())

		(cAlias)->(DbSkip())
	EndDo

Endif

Return lRet
