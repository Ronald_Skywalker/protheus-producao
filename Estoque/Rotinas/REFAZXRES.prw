#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

#Define CRLF  CHR(13)+CHR(10)

User Function REFAZXRES()
Local oButton1
Local oButton2
Local oGet1
Local oGroup1
Local oSay1
Local oSay2
Local oRadMenu1
private cGetpro := Space(15)
private nRadLocal := 4
Private oFont14n    := TFont():New("Arial",,14,,.t.,,,,,.f.)	
Static oDlg

  DEFINE MSDIALOG oDlg TITLE "Refaz Saldo Reserva Customizada" FROM 000, 000  TO 300, 380 COLORS 0, 16777215 PIXEL

    @ 011, 007 SAY oSay2 PROMPT "Este programa ir� atualizar os campos B2_XRESERV e B2_XRES com o saldo correto do DAP ou OPs geradas" SIZE 175, 019 OF oDlg COLORS 16711680, 16777215 FONT oFont14n PIXEL

    @ 045, 009 SAY oSay1 PROMPT "Produto: " SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 044, 034 MSGET oGet1 VAR cGetPro SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    
    @ 063, 008 GROUP oGroup1 TO 121, 184 PROMPT "  Escolher o Local: " OF oDlg COLOR 0, 16777215 PIXEL
    @ 077, 013 RADIO oRadMenu1 VAR nRadLocal ITEMS "A1","E0","QL","TODOS" SIZE 050, 037 OF oDlg COLOR 0, 16777215 PIXEL

	@ 128, 101 BUTTON oButton1 PROMPT "OK" SIZE 037, 012 OF oDlg ACTION (ProcSaldo(),Close(oDlg)) PIXEL
    @ 128, 144 BUTTON oButton2 PROMPT "Cancela" SIZE 037, 012 OF oDlg ACTION Close(oDlg) PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED   

Return

Static Function ProcSaldo()

	If MsgYesNo("Deseja realmente executar o recalculo?","Confima��o")
		Processa( {|| REFAZSALDO() }, "Aguarde...", "Processando...",.F.)
	EndIf

Return

Static Function REFAZSALDO()

	Local cQuery   := ""
    Local cQry     := ""


	cGrpLib := AllTrim(SuperGetMV("MV_XGRPNLB",.F.,"MP05|MP16|MP17|MP91|MP92|MP30|MP98")) //Grupo de Produtos que n�o devem ser considerados para libera��o de OP
	cTpLib  := AllTrim(SuperGetMV("MV_XTPNLB",.F.,"MI|PI|SV")) // Tipo de Produtos que n�o devem ser considerados na libera��o da OP
	cArmLib := AllTrim(SuperGetMV("MV_XARMLIB",.F.,"A1")) // Armaz�m que deve ser considerado para a Libera��o da OP
	xGrpLib := StrTran(cGrpLib,"|","','")
	xTpLib := StrTran(cTpLib,"|","','")

    If nRadLocal = 1 .OR. nRadLocal = 4
		
		If !Empty(Alltrim(cGetpro))
			cQuery := "UPDATE "+RetSqlName("SB2")+" SET B2_XRES = 0 WHERE D_E_L_E_T_ = '' AND B2_XRES <> 0 AND B2_COD = '"+Alltrim(cGetpro)+"' "
			TcSqlExec(cQuery)
		Else
			cQuery := "UPDATE "+RetSqlName("SB2")+" SET B2_XRES = 0 WHERE D_E_L_E_T_ = '' AND B2_XRES <> 0 "
			TcSqlExec(cQuery)	
        EndIf

		cQuery := "UPDATE SB2010 SET B2_XRES =  "
		cQuery += "(SELECT ISNULL(SUM(D4_XRES),0) from "+RetSqlName("SD4")+" SD4 "
		cQuery += "inner join "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' and C2_NUM = left(D4_OP,6) and C2_ITEM = Substring(D4_OP,7,2) and C2_SEQUEN = Substring(D4_OP,9,3) and C2_ITEMGRD = SubString(D4_OP,12,3) "
		cQuery += "inner join "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = D4_COD "
		cQuery += "where SD4.D_E_L_E_T_ = '' and C2_XRES in ('L') and Substring(D4_COD,1,1) <> 'B' and D4_FILIAL = '"+XfILIAL("SD4")+"' "
		cQuery += " AND SB1.B1_TIPO NOT IN ('"+xTpLib+"') AND SB1.B1_GRUPO NOT IN ('"+xGrpLib+"')  "
        If !Empty(Alltrim(cGetpro))
			cQuery += "AND SD4.D4_COD = '"+Alltrim(cGetpro)+"' "
        EndIf
		cQuery += "and D4_COD = B2_COD) "
		cQuery += "FROM "+RetSqlName("SB2")+" SB2 WHERE SB2.D_E_L_E_T_ = '' AND B2_LOCAL = 'A1' AND B2_COD IN ( "
		cQuery += "SELECT D4_COD from "+RetSqlName("SD4")+" SD4 "
		cQuery += "inner join "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' and C2_NUM = left(D4_OP,6) and C2_ITEM = Substring(D4_OP,7,2) and C2_SEQUEN = Substring(D4_OP,9,3) and C2_ITEMGRD = SubString(D4_OP,12,3) "
		cQuery += "inner join "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = D4_COD "
		cQuery += "where SD4.D_E_L_E_T_ = '' and C2_XRES in ('L') and Substring(D4_COD,1,1) <> 'B' and D4_FILIAL = '"+XFILIAL("SD4")+"' "
		cQuery += " AND SB1.B1_TIPO NOT IN ('"+xTpLib+"') AND SB1.B1_GRUPO NOT IN ('"+xGrpLib+"')  "
		If !Empty(Alltrim(cGetpro))
			cQuery += "AND SD4.D4_COD = '"+Alltrim(cGetpro)+"' "
        EndIf
		cQuery += "group by D4_COD) "

		TcSqlExec(cQuery)
        
		If nRadLocal = 1
			MsgInfo("Rec�lculo realizado com sucesso.","Aviso")
		Endif	

	Endif

	If nRadLocal = 2 .OR. nRadLocal = 3 .OR. nRadLocal = 4

		If Select("TRB") > 0
				TRB->(DbCloseArea())
		EndIf

		cQuery := "SELECT B2_LOCAL, "
		cQuery += "       B2_COD, "
		cQuery += "       B2_RESERVA, "
		cQuery += "       B2_XRESERV "
		cQuery += "FROM "+RetSqlName("SB2")+" AS SB2 (NOLOCK) "
		cQuery += "INNER JOIN "+RetSqlName("SB1")+" AS SB1 (NOLOCK) ON B1_COD = B2_COD "
		cQuery += "AND SB1.D_E_L_E_T_ = '' "
		cQuery += "AND B1_GRUPO NOT IN ('"+xGrpLib+"') "
		cQuery += "LEFT JOIN "+RetSqlName("SZJ")+" AS SZJ  (NOLOCK) ON ZJ_PRODUTO = B2_COD "
		cQuery += "AND ZJ_LOCAL = B2_LOCAL "
		cQuery += "AND SZJ.D_E_L_E_T_ = '' "
		cQuery += "AND ZJ_DOC = '' "
		cQuery += "AND (ZJ_CONF <> 'S' OR ZJ_BLEST = '02') "
		cQuery += "AND ZJ_FILIAL = B2_FILIAL "
		cQuery += "WHERE SB2.D_E_L_E_T_ = '' "
		cQuery += "  AND B2_FILIAL = '"+xFilial("SB2")+"' "
		cQuery += "  AND B2_QATU > 0 "

		If !Empty(Alltrim(cGetpro))
			cQuery += "AND B2_COD = '"+Alltrim(cGetpro)+"' "
        ENDIF

		If nRadLocal = 2 
			cQuery += "  AND B2_LOCAL = 'E0' "
		elseif nRadLocal = 3 
			cQuery += "  AND B2_LOCAL = 'QL' "
		elseif nRadLocal = 4 
		    cQuery += "  AND B2_LOCAL IN ('E0','QL') "				
		Endif

		cQuery += "GROUP BY B2_LOCAL, "
		cQuery += "         B2_COD, "
		cQuery += "         B2_RESERVA, "
		cQuery += "         B2_XRESERV "
		cQuery += "ORDER BY B2_LOCAL, B2_COD "


		MemoWrite("REFAZXRES.txt",cQuery)

		cQuery := ChangeQuery(cQuery)

		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)

		DbSelectArea("TRB")

			nCont := 0
			If TRB->(!EOF())
				While TRB->(!EOF())
					nCont++
					TRB->(DbSkip())
				EndDo
			EndIf
			ProcRegua(nCont)

			TRB->(DbGoTop())
			If TRB->(!EOF())
				While TRB->(!EOF())
				IncProc()
				DbSelectArea("SB2")
				DbSetOrder(2)
					If DbSeek(xFilial("SB2")+TRB->B2_LOCAL+TRB->B2_COD)
						If Select("TRP") > 0
							TRP->(DbCloseArea())
						EndIf

						cQry := "SELECT COALESCE(SUM(ZJ_QTDLIB), 0) AS QTDECORRETA FROM "+RetSqlName("SZJ")+" WITH (NOLOCK) 
						cQry += "WHERE D_E_L_E_T_ = ''  AND ZJ_DOC = '' AND (ZJ_CONF <> 'S' OR ZJ_BLEST = '02') "
						cQry += "AND ZJ_LOCAL = '"+Alltrim(TRB->B2_LOCAL)+"' AND ZJ_PRODUTO = '"+Alltrim(TRB->B2_COD)+"' "

						DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQry),"TRP", .F., .T.)
						DbSelectArea("TRP")

						If SB2->B2_XRESERV <> TRP->QTDECORRETA
							RecLock("SB2",.F.)
								SB2->B2_XRESERV := TRP->QTDECORRETA
							MsUnlock()
						EndIf
					EndIf
				TRB->(DbSkip())
				EndDo
				MsgInfo("Rec�lculo realizado com sucesso.","Aviso")
			Else
				MsgAlert("N�o h� produtos a serem corrigidos.","Aten��o")	
			EndIf
    Endif

Return
