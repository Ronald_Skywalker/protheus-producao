#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

User Function HPCPP013()

	Local oButton1
	Local oButton2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oGet6
	Local oGet15
	Local oGroup1
	Local oGroup2
	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Local oSay7
	Local oSay8
	Local oSay9
	Local oSay10
	Local oSay11
	Local oSay12
	Local oSay13
	Local oSay14
	Local oSay15
	Local oSay16
	Local oSay17
	Local oSay18
	Local oSay19
	Local oSay20
	Local oSay21
	Local oSay22
	Local oSay23
	Local oSay24
	Local oSay25
	Local oSay26
	Local oSay27
	Local oSay28
	Local oSay29
	Local oSay30
	Local oSay31
	Local oSay32
	Private oGet1
	Private oRadMenu1
	Private nRadMenu1 := 0
	Private nComboBo1 := "Piking"
	Private cGet1 := space(15)
	Private cGet2 := space(15)
	Private cGet3 := space(40)
	Private cGet4 := space(15)
	Private cGet5 := space(4)
	Private cGet6 := space(15)
	Private cGet7 := 0
	Private oGet2
	Private cGet8 := 0
	Private cGet10:= Space(2)
	Private cGet15:= Space(20)
	Private cGet9 := 0
	Private cEndOr:= "PADRAO"
	Private cLocOr:= "EB"

	Static oDlg

	DEFINE MSDIALOG oDlg TITLE "Tranfer�ncia Importados / Devolu��o (EB)" FROM 000, 000  TO 300, 1100 COLORS 0, 16777215 PIXEL

	@ 011, 009 SAY oSay2   PROMPT "Codigo:" 					SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 008, 041 MSGET oGet2 VAR cGet2 							SIZE 068, 010 OF oDlg VALID Atutela(1,alltrim(cGet2),0) COLORS 0, 16777215 F3 "SB1" PIXEL
	@ 011, 129 SAY oSay3   PROMPT "Descricao:" 					SIZE 030, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 008, 161 MSGET oGet3 VAR cGet3 							SIZE 188, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	@ 011, 363 SAY oSay4   PROMPT "Cor:" 						SIZE 019, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 008, 388 MSGET oGet4 VAR cGet4 							SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	@ 011, 457 SAY oSay5   PROMPT "Tamanho:" 					SIZE 028, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 008, 491 MSGET oGet5 VAR cGet5 							SIZE 047, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

	@ 027, 009 GROUP oGroup1 TO 065, 539 PROMPT "Origem " 	OF oDlg COLOR 0, 16777215 PIXEL
	@ 040, 014 SAY oSay6   PROMPT "Quantidade: " 				SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 040, 060 MSGET oGet9 VAR cGet9							SIZE 057, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

	@ 040, 200 SAY oSay6   PROMPT "Qtde de Ba�s: " 					SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 040, 249 MSGET oGet9 VAR cGet8							SIZE 057, 010 PICTURE "@E 9999" OF oDlg VALID Atutela(3,cGet8,cGet7) COLORS 0, 16777215 PIXEL

	@ 040, 350 SAY oSay8   PROMPT "Qtd Pe�as Ba�:" 				SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 040, 400 MSGET oGet5 VAR cGet7 							SIZE 057, 010 PICTURE "@E 9999" OF oDlg VALID Atutela(3,cGet8,cGet7) COLORS 0, 16777215 PIXEL

	@ 075, 010 SAY oSay20  PROMPT "Transferir Para:" 		SIZE 055, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 075, 071 MSCOMBOBOX oComboBo1 VAR nComboBo1 ITEMS {"Piking","Aereo"} SIZE 072, 010 OF oDlg VALID Atutela(2,cGet2,nComboBo1) COLORS 0, 16777215 PIXEL

	@ 075, 200 SAY oSay20  PROMPT "Ba� Devolu��o:" 		SIZE 055, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 075, 249 MSGET oGet15 VAR cGet15				SIZE 057, 010 PICTURE "@!" OF oDlg COLORS 0, 16777215 PIXEL

	@ 087, 009 GROUP oGroup2 TO 120, 539 PROMPT "Destino " 	OF oDlg COLOR 0, 16777215 PIXEL
	@ 100, 011 SAY oSay21  PROMPT "Endereco Destino:" 		SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 100, 073 MSGET oGet6 VAR cGet6 							SIZE 057, 010 OF oDlg VALID u_vldsbe(cGet6,nComboBo1,cGet2) COLORS 0, 16777215 F3 "SBE" PIXEL

	@ 100, 200 SAY oSay21  PROMPT "Armaz�m:" 		SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 100, 249 MSGET oGet6 VAR cGet10 							SIZE 057, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

//    @ 100, 350 SAY oSay1   PROMPT "Recipiente:" 				SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 100, 400 MSGET oGet1 VAR cGet1 							SIZE 057, 010 OF oDlg COLORS 0, 16777215 PIXEL

	@ 130, 011 SAY oSay31  PROMPT "Usu�rio:" 					SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 130, 052 SAY oSay32  PROMPT cUserName 					SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL

	@ 130, 495 BUTTON oButton1 PROMPT "Transferir" 			SIZE 043, 012 ACTION (SaveA(cGet2,cGet7,cLocOr,cGet10,cEndOr,cGet6,cGet8,nComboBo1)) OF oDlg PIXEL
	@ 130, 446 BUTTON oButton2 PROMPT "Cancelar" 			SIZE 043, 012 ACTION oDlg:End() OF oDlg PIXEL

	//oRadMenu1:bSetGet := {|u|Iif (Atutela(2,cGet2,nRadMenu1)==0,nRadMenu1,nRadMenu1)}
	//oRadMenu1:bSetGet := {|u|Atutela(2,cGet2,nRadMenu1)}

	ACTIVATE MSDIALOG oDlg CENTERED

Return

Static Function SaveA(_par1,_par2,_par3,_par4,_par5,_par6,_par7,_par8)
	lOCAL _I
	//lOCAL _QRY

	If _par2 > 0
		If _par8 = "Aereo"
			For _i := 1 to _par7
				_qry := "Select Max(ZS_CODRECI) as RECI from SZS010 "
				DbUseArea(.T.,'TOPCONN',TCGENQRY(,,_Qry),"QRY",.F.,.T.)
				DbSelectArea("QRY")
				DbGoTop()
				_reci := SubStr(QRY->RECI,1,1)+strzero(Val(SubStr(QRY->RECI,2,5))+1,5)

				DbCloseArea()

				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+_par1)
				_QRY := "INSERT INTO "+RETSQLNAME("SZS")+" (ZS_FILIAL, ZS_CODRECI, ZS_YQUANT, ZS_NUMEAN, ZS_DATA, R_E_C_N_O_) VALUES ('"
				_QRY += XFILIAL("SZS")+"','"+_RECI+"',"+STR(_PAR2)+",'"+SB1->B1_CODBAR+"','"+DTOS(DDATABASE)+"',(sELECT MAX(R_E_C_N_O_)+1 FROM SZS010))"
				TCSQLEXEC(_QRY)
		
		/*RecLock("SZS",.T.)
			Replace ZS_FILIAL		with xfilial("SZS")
			Replace ZS_CODRECI		with _reci
			Replace ZS_YQUANT		with _par2
			Replace ZS_NUMEAN		with SB1->B1_CODBAR
			Replace ZS_DATA			with ddatabase 
		MsUnLock()
		*/

				Save(_par1,_par2,_par3,_par4,_par5,_par6,_reci,_par8)

			Next
		Else
			_reci := 0
			Save(_par1,_par2,_par3,_par4,_par5,_par6,_reci,_par8)
		EndIf


		cGet2:= sPACE(15)
		cGet3:= SPACE(30)
		cGet4:= SPACE(3)
		cGet5:= SPACE(4)
		cGet9:= 0
		cGet8:= 0
		cGet7:= 0
		cGet6:= SPACE(20)
		nComboBo1 := "Piking"
		cGet10:=SPACE(2)
		oGet2:setfocus()

		oDlg:refresh()

	Else
		MessageBox("Qtde de pe�as n�o informada para Ba�!","Aten��o",48)
	EndIf

Return

Static Function Atutela(ntipo, Codigo,nradio)

	_RET := 0
	If !Empty(Codigo)
		If nTipo = 1

			DbselectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+Codigo)

			DbSelectArea("SB2")
			DbSetOrder(1)
			DbSeek(xfilial("SB2")+Codigo+cLocOr)

			cGet3 := SB1->B1_DESC
			cGet4 := SUBSTRING(SB1->B1_COD,9,3)
			cGet5 := SUBSTRING(SB1->B1_COD,12,4)
			cGet9 := SB2->B2_QATU

			oDlg:refresh()

		ElseIf nTipo = 2
			_RET := 0
			If nRadio = "Piking"
				DbSelectArea("SBE")
				DbSetOrder(10)
				DbSeek(xfilial("SBE")+Codigo+"E0")

				cGet10:= "E0"
				cGet6 := SBE->BE_LOCALIZ
				oDlg:Refresh()
			Else
				cGet10:= "E1"
				cGet6 := "B.U.IMPORTADO"
				oDlg:Refresh()
			Endif
		Else
			If Codigo * nradio > cGet9
				cGet8 := 0
				cGet7 := 0
				oDlg:Refresh()
				MessageBox("Quantidade informada superior ao estoque dispon�vel!","Aten��o!",16)
			Endif
		Endif
	EndIf

Return _RET

Static Function Save(cProd,nQuant,cAmzOri,cAmzDest, cEndOri, cEndDest,recip,cTipo)
	Local cDoc      := ""
	Local lContinua := .F.
	Local aLnTran   := {}
	Local aAgrupa   := {}
	Local cQry      := ""
	lOCAL _QRY
//Local cEndOri   := ""
//Local cEndDest  := ""
	Local cUM       := ""
	Local cDescri   := ""
	Local cSPadrao  := AllTrim(SuperGetMV("MV_XSUBPAD",.F.,"0"))

	Local nTransf   := 0
	Local aTransf   := {}
	Local cTime     := ""
	Local cDtTime   := ""
	Local nx,ny     := 0
	Local nOpcAuto  := 3 // Indica qual tipo de a��o ser� tomada (Inclus�o/Exclus�o)
	Local dData     := DDATABASE
	Local cHora     := SubStr(Time(),1,5)
	Local dDtValid  := StoD('20491231')
	Local cDocSeq   := ""
	Local nStatus   := 0
	Local cLote := ""
//Local cSubPad   := AllTrim(SuperGetMV("MV_XSUBPAD",.F.,"0"))

	Private lMsHelpAuto := .T.
	Private lMsErroAuto := .F.

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xFilial("SB1")+cProd)

	cQry := "select BF_PRODUTO,BF_LOCAL,BF_QUANT,BF_LOTECTL,BF_NUMLOTE,BF_LOCALIZ "
	cQry += CRLF + "from "+RetSqlName("SBF")+" SBF "
	cQry += CRLF + "where SBF.D_E_L_E_T_ = '' "
	cQry += CRLF + "and BF_PRODUTO = '"+cProd+"' "
	cQry += CRLF + "and BF_LOCAL   = '"+cAmzOri+"' "
	cQry += CRLF + "and BF_LOCALIZ = '"+cEndOri+"' "
//cQry += CRLF + "and BF_NUMLOTE = '"+Right(alltrim(cGet1),6)+"' "
	cQry += CRLF + "and BF_QUANT >= "+str(nQuant)+" "
	cQry += CRLF + "order by BF_PRODUTO,BF_NUMLOTE,BF_LOTECTL "

	MemoWrite("HPCPP010.txt",cQry)

	cQry := ChangeQuery(cQry)

	DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQry),"QRY",.F.,.T.)

	DbSelectArea("QRY")

	nTransf := nQuant//QRY->BF_QUANT
	cLote   := QRY->BF_LOTECTL


	If QRY->(!EOF())
		If cLote <> "000000"
			MsgAlert("Transfer�ncia n�o realizada! LOTE diferente de 000000.","Aten��o")
		Else
			While QRY->(!EOF())
				If nTransf > 0
					cEndOri := QRY->BF_LOCALIZ

					//                     1         2              3        4       5       6        7            8         9          10         11
					aAdd(aTransf,{QRY->BF_PRODUTO,SB1->B1_DESC,SB1->B1_UM,cAmzOri,cEndOri,cAmzDest,cEndDest,QRY->BF_LOTECTL,recip,QRY->BF_NUMLOTE,nQuant})

					nTransf := nTransf - nQuant

					lContinua := .T.
				Else
					EXIT
				EndIf

				QRY->(DbSkip())
			EndDo
		EndIf
	EndIf

//TODO Validar picking do produto. Ronaldo 06/03/20
	If Alltrim(nComboBo1) == "Piking"
		DbSelectArea("SBE")
		DbSetOrder(10)
		DbSeek(xfilial("SBE")+alltrim(cGet2)+"E0")
		If Empty(SBE->BE_LOCALIZ)
			MessageBox("Endere�o de Picking n�o cadastrado para o produto: "+alltrim(cProd)+" ","Aten��o!",48)
			lContinua := .F.
		Else
			If Found() .and. alltrim(SBE->BE_LOCALIZ) <> alltrim(cGet6)
				MessageBox("Endere�o diferente do cadastrado! Picking: "+alltrim(SBE->BE_LOCALIZ)+" ","Aten��o!",48)
				lContinua := .F.
			EndIf
		EndIf
	EndIf

//TODO Valida se o endere�o existe no armazem Aereo. Ronaldo Pereira 14/05/19
	If Alltrim(nComboBo1) == "Aereo"
		DbSelectArea("SBE")
		DbSetOrder(1)
		DbSeek(xfilial("SBE")+cAmzDest+cEndDest)
		If Empty(SBE->BE_LOCALIZ)
			MessageBox("Endere�o invalido para o Armazem Aereo: (E1)!","Aten��o",48)
			lContinua := .F.
		EndIf
	EndIf

//TODO Valida se o endere�o esta Bloqueado. Ronaldo Pereira 06/03/20
	DbSelectArea("SBE")
	DbSetOrder(1)
	DbSeek(xfilial("SBE")+cAmzDest+cEndDest)
	If SBE->BE_STATUS = '3'
		MessageBox("Endere�o Bloqueado!","Aten��o",48)
		lContinua := .F.
	EndIf

	QRY->(DbCloseArea())

	If lContinua
		cTrnDAP := GetSxENum("SD3","D3_DOC",1)
		cDoc := cTrnDAP

		For nx := 1 to Len(aTransf)
			If aTransf[nx][11] < 0
				MsgAlert("N�o � poss�vel realizar uma transfer�ncia com valor negativo.","Aten��o")
				Loop
			EndIf

			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xFilial("SB1")+aTransf[nx][1])

			cTpConv := SB1->B1_TIPCONV
			nFtConv := SB1->B1_CONV
			cSegUm  := SB1->B1_SEGUM

			DbSelectArea("SB2")
			DbSetOrder(1)
			DbSeek(xFilial("SB2")+aTransf[nx][1]+aTransf[nx][4])

			cDocSeq := PROXNUM(.F.)
			If GetMV("MV_DOCSEQ") <> cDocSeq
				PutMV("MV_DOCSEQ",cDocSeq)
			EndIf

			//Begin Transaction

			If Alltrim(nComboBo1) == "Piking"
				cLocal   := "E0"
				aTransf[nx][6] := "E0"
				cSubPad  := "0" //aTransf[nx][9]//cSPadrao
			Else
				DbSelectArea("SBE")
				DbSetOrder(10)
				DbSeek(xfilial("SBE")+alltrim(cGet2)+"E0")

				If Found() .and. alltrim(SBE->BE_LOCALIZ) = alltrim(cGet6)
					cLocal   := "E0"
					aTransf[nx][6] := "E0"
					cSubPad := "0" //aTransf[nx][9]//cSPadrao
				Else
					cLocal   := "E1"
					aTransf[nx][6] := "E1"
					cSubPad := aTransf[nx][9]
				Endif
			End

			//TODO WESKLEY/ FUN��O PARA CRIAR SALDO INICIAL CASO O PRODUTO N�O POSSUA. 14/05/2018

			DbSelectArea("SB9")
			DbSetOrder(1)
			IF !Dbseek(xFilial("SB9")+aTransf[nx][1]+aTransf[nx][6])

				cFilanterior := cfilant
				PARAMIXB1 := {}
				aadd(PARAMIXB1,{"B9_FILIAL","0101",})
				aadd(PARAMIXB1,{"B9_COD",Alltrim(aTransf[nx][1]),})
				aadd(PARAMIXB1,{"B9_LOCAL",Alltrim(aTransf[nx][6]),})
				aadd(PARAMIXB1,{"B9_QINI",0,})
				cfilant := "0101"
				MSExecAuto({|x,y| mata220(x,y)},PARAMIXB1,3)
				cfilant := cFilanterior
				If lMsErroAuto
					mostraerro()
				else

					//TODO GEYSON/ FUN��O PARA CRIAR SALDO INICIAL CASO O PRODUTO N�??O POSSUA. 13/01/2020
					DbSelectArea("SB2")
					DbSetOrder(1)
					IF !Dbseek(xFilial("SB2")+aTransf[nx][1]+aTransf[nx][6])

						RECLOCK("SB2",.T.)
						SB2->B2_FILIAL 	:= XFILIAL("SB2")
						SB2->B2_COD    	:= Alltrim(aTransf[nx][1])
						SB2->B2_LOCAL  	:= Alltrim(aTransf[nx][6])
						SB2->B2_QATU   	:= 0
						SB2->B2_QTSEGUM := 0
						MSUNLOCK()
					EndIf

					DBselectArea("SBF")
					DbSetOrder(1)
					DbSeek(xfilial("SBF")+aTransf[nx][4]+aTransf[nx][5]+aTransf[nx][1]+space(20)+aTransf[nx][8]+aTransf[nx][10])

					If !Found() .or. SBf->BF_QUANT < aTransf[nx][11]
						MsgAlert("Saldo de estoque insuficiente.","Aten��o")
						Loop
					Endif

					lTransfSBF := AtuSBF(aTransf[nx][1],aTransf[nx][4],aTransf[nx][6],aTransf[nx][8],cSubPad,aTransf[nx][11],aTransf[nx][10],aTransf[nx][5],aTransf[nx][7],cTpConv,nFtConv)

					DbSelectArea("SB2")
					DbSetOrder(2)
					DbSeek(xFilial("SB2")+aTransf[nx][4]+aTransf[nx][1])
					nSldAtu := SB2->B2_QATU
					nSldAlt := nSldAtu - aTransf[nx][11]

					nSld2 := 0
					If !Empty(cTpConv)
						If cTpConv = "M"
							nSld2 := nSldAlt * nFtConv
						Else
							nSld2 := nSldAlt / nFtConv
						EndIf
					EndIf

					RecLock("SB2",.F.)
					SB2->B2_QATU    := nSldAlt
					SB2->B2_QTSEGUM := nSld2
					MsUnlock()

					//Armazem Destino
					DbSelectArea("SB2")
					DbSetOrder(2)
					DbSeek(xFilial("SB2")+aTransf[nx][6]+aTransf[nx][1])
					nSldAtu := SB2->B2_QATU
					nSldAlt := nSldAtu + aTransf[nx][11]

					nSld2 := 0
					If !Empty(cTpConv)
						If cTpConv = "M"
							nSld2 := nSldAlt * nFtConv
						Else
							nSld2 := nSldAlt / nFtConv
						EndIf
					EndIf

					RecLock("SB2",.F.)
					SB2->B2_QATU := nSldAlt
					SB2->B2_QTSEGUM := nSld2
					MsUnlock()

					lTransfSB8 := AtuSB8(cDoc,aTransf[nx][1],aTransf[nx][4],aTransf[nx][6],aTransf[nx][8],cSubPad,aTransf[nx][11],aTransf[nx][10],cTpConv,nFtConv)

					nQtd2 := 0
					If !Empty(cTpConv)
						If cTpConv = "M"
							nQtd2 := aTransf[nx][11] * nFtConv
						Else
							nQtd2 := aTransf[nx][11] / nFtConv
						EndIf
					EndIf

					//Grava SD3 Transferencia
					//Requisicao
					DbSelectArea("SD3")
					RecLock("SD3",.T.)
					SD3->D3_FILIAL  := xFilial("SD3")
					SD3->D3_TM      := "999"
					SD3->D3_COD     := aTransf[nx][1]
					SD3->D3_UM      := aTransf[nx][3]
					SD3->D3_QUANT   := aTransf[nx][11]
					SD3->D3_QTSEGUM := nQtd2
					SD3->D3_SEGUM   := cSegUm
					SD3->D3_CF      := "RE4"
					SD3->D3_CONTA   := SB1->B1_CONTA
					SD3->D3_LOCAL   := aTransf[nx][4]
					SD3->D3_DOC     := cDoc
					SD3->D3_EMISSAO := DDATABASE
					SD3->D3_GRUPO   := SB1->B1_GRUPO
					SD3->D3_CUSTO1  := SB2->B2_CM1
					SD3->D3_NUMSEQ  := cDocseq
					SD3->D3_TIPO    := SB1->B1_TIPO
					SD3->D3_USUARIO := UsrRetName(__cUserID)
					SD3->D3_CHAVE   := "E0"
					SD3->D3_LOTECTL := aTransf[nx][8]
					SD3->D3_NUMLOTE := aTransf[nx][10]
					SD3->D3_DTVALID := dDtValid
					SD3->D3_LOCALIZ := aTransf[nx][5]
					SD3->D3_XDBAU	:= cGet15
					MsUnlock()

					//Devolucao
					RecLock("SD3",.T.)
					SD3->D3_FILIAL  := xFilial("SD3")
					SD3->D3_TM      := "499"
					SD3->D3_COD     := aTransf[nx][1]
					SD3->D3_UM      := aTransf[nx][3]
					SD3->D3_QUANT   := aTransf[nx][11]
					SD3->D3_QTSEGUM := nQtd2
					SD3->D3_SEGUM   := cSegUm
					SD3->D3_CF      := "DE4"
					SD3->D3_CONTA   := SB1->B1_CONTA
					SD3->D3_LOCAL   := cLocal
					SD3->D3_DOC     := cDoc
					SD3->D3_EMISSAO := DDATABASE
					SD3->D3_GRUPO   := SB1->B1_GRUPO
					SD3->D3_CUSTO1  := SB2->B2_CM1
					SD3->D3_NUMSEQ  := cDocSeq
					SD3->D3_TIPO    := SB1->B1_TIPO
					SD3->D3_USUARIO := UsrRetName(__cUserID)
					SD3->D3_CHAVE   := "E9"
					SD3->D3_LOTECTL := aTransf[nx][8]
					SD3->D3_NUMLOTE := cSubPad
					SD3->D3_DTVALID := dDtValid
					SD3->D3_LOCALIZ := aTransf[nx][7]
					SD3->D3_XDBAU	:= cGet15
					MsUnlock()

					cNumIDOper := GetSx8Num('SDB','DB_IDOPERA')
					ConfirmSX8()

					DbSelectArea("SDB")
					RecLock("SDB",.T.)
					SDB->DB_FILIAL  := xFilial("SDB")
					SDB->DB_ITEM    := "0001"
					SDB->DB_PRODUTO := aTransf[nx][1]
					SDB->DB_LOCAL   := aTransf[nx][4]
					SDB->DB_LOCALIZ := aTransf[nx][5]
					SDB->DB_DOC     := cDoc
					SDB->DB_TM      := "999"
					SDB->DB_ORIGEM  := "SD3"
					SDB->DB_QUANT   := aTransf[nx][11]
					SDB->DB_QTSEGUM := nQtd2
					SDB->DB_DATA    := DDATABASE
					SDB->DB_LOTECTL := aTransf[nx][8]
					SDB->DB_NUMLOTE := aTransf[nx][10]
					SDB->DB_NUMSEQ  := cDocseq
					SDB->DB_TIPO    := "M"
					SDB->DB_SERVIC  := "999"
					SDB->DB_ATIVID  := "ZZZ"
					SDB->DB_HRINI   := SubStr(Time(),1,5)
					SDB->DB_ATUEST  := "S"
					SDB->DB_STATUS  := "M"
					SDB->DB_ORDATIV := "ZZ"
					SDB->DB_IDOPERA := cNumIDOper
					MsUnlock()

					cNumIDOper := GetSx8Num('SDB','DB_IDOPERA')
					ConfirmSX8()

					RecLock("SDB",.T.)
					SDB->DB_FILIAL  := xFilial("SDB")
					SDB->DB_ITEM    := "0001"
					SDB->DB_PRODUTO := aTransf[nx][1]
					SDB->DB_LOCAL   := aTransf[nx][6]
					SDB->DB_LOCALIZ := aTransf[nx][7]
					SDB->DB_DOC     := cDoc
					SDB->DB_TM      := "499"
					SDB->DB_ORIGEM  := "SD3"
					SDB->DB_QUANT   := aTransf[nx][11]
					SDB->DB_QTSEGUM := nQtd2
					SDB->DB_DATA    := DDATABASE
					SDB->DB_LOTECTL := aTransf[nx][8]
					SDB->DB_NUMLOTE := cSubPad
					SDB->DB_NUMSEQ  := cDocseq
					SDB->DB_TIPO    := "M"
					SDB->DB_SERVIC  := "499"
					SDB->DB_ATIVID  := "ZZZ"
					SDB->DB_HRINI   := SubStr(Time(),1,5)
					SDB->DB_ATUEST  := "S"
					SDB->DB_STATUS  := "M"
					SDB->DB_ORDATIV := "ZZ"
					SDB->DB_IDOPERA := cNumIDOper
					MsUnlock()

					DbSelectArea("SD5")
					RecLock("SD5",.T.)
					SD5->D5_FILIAL  := xFilial("SD5")
					SD5->D5_PRODUTO := aTransf[nx][1]
					SD5->D5_LOCAL   := aTransf[nx][4]
					SD5->D5_DOC     := cDoc
					SD5->D5_DATA    := DDATABASE
					SD5->D5_ORIGLAN := "999"
					SD5->D5_NUMSEQ  := cDocseq
					SD5->D5_QUANT   := aTransf[nx][11]
					SD5->D5_QTSEGUM := nQtd2
					SD5->D5_LOTECTL := aTransf[nx][8]
					SD5->D5_NUMLOTE := aTransf[nx][10]
					SD5->D5_DTVALID := dDtValid
					MsUnlock()

					RecLock("SD5",.T.)
					SD5->D5_FILIAL  := xFilial("SD5")
					SD5->D5_PRODUTO := aTransf[nx][1]
					SD5->D5_LOCAL   := aTransf[nx][6]
					SD5->D5_DOC     := cDoc
					SD5->D5_DATA    := DDATABASE
					SD5->D5_ORIGLAN := "499"
					SD5->D5_NUMSEQ  := cDocseq
					SD5->D5_QUANT   := aTransf[nx][11]
					SD5->D5_QTSEGUM := nQtd2
					SD5->D5_LOTECTL := aTransf[nx][8]
					SD5->D5_NUMLOTE := cSubPad
					SD5->D5_DTVALID := dDtValid
					MsUnlock()

				endif

			else

				DBselectArea("SBF")
				DbSetOrder(1)
				DbSeek(xfilial("SBF")+aTransf[nx][4]+aTransf[nx][5]+aTransf[nx][1]+space(20)+aTransf[nx][8]+aTransf[nx][10])

				If !Found() .or. SBf->BF_QUANT < aTransf[nx][11]
					MsgAlert("Saldo de estoque insuficiente.","Aten��o")
					Loop
				Endif

				lTransfSBF := AtuSBF(aTransf[nx][1],aTransf[nx][4],aTransf[nx][6],aTransf[nx][8],cSubPad,aTransf[nx][11],aTransf[nx][10],aTransf[nx][5],aTransf[nx][7],cTpConv,nFtConv)

				DbSelectArea("SB2")
				DbSetOrder(2)
				DbSeek(xFilial("SB2")+aTransf[nx][4]+aTransf[nx][1])
				nSldAtu := SB2->B2_QATU
				nSldAlt := nSldAtu - aTransf[nx][11]

				nSld2 := 0
				If !Empty(cTpConv)
					If cTpConv = "M"
						nSld2 := nSldAlt * nFtConv
					Else
						nSld2 := nSldAlt / nFtConv
					EndIf
				EndIf

				RecLock("SB2",.F.)
				SB2->B2_QATU    := nSldAlt
				SB2->B2_QTSEGUM := nSld2
				MsUnlock()

				//Armazem Destino
				DbSelectArea("SB2")
				DbSetOrder(2)
				DbSeek(xFilial("SB2")+aTransf[nx][6]+aTransf[nx][1])
				nSldAtu := SB2->B2_QATU
				nSldAlt := nSldAtu + aTransf[nx][11]

				nSld2 := 0
				If !Empty(cTpConv)
					If cTpConv = "M"
						nSld2 := nSldAlt * nFtConv
					Else
						nSld2 := nSldAlt / nFtConv
					EndIf
				EndIf

				RecLock("SB2",.F.)
				SB2->B2_QATU := nSldAlt
				SB2->B2_QTSEGUM := nSld2
				MsUnlock()

				lTransfSB8 := AtuSB8(cDoc,aTransf[nx][1],aTransf[nx][4],aTransf[nx][6],aTransf[nx][8],cSubPad,aTransf[nx][11],aTransf[nx][10],cTpConv,nFtConv)

				nQtd2 := 0
				If !Empty(cTpConv)
					If cTpConv = "M"
						nQtd2 := aTransf[nx][11] * nFtConv
					Else
						nQtd2 := aTransf[nx][11] / nFtConv
					EndIf
				EndIf

				//Grava SD3 Transferencia
				//Requisicao
				DbSelectArea("SD3")
				RecLock("SD3",.T.)
				SD3->D3_FILIAL  := xFilial("SD3")
				SD3->D3_TM      := "999"
				SD3->D3_COD     := aTransf[nx][1]
				SD3->D3_UM      := aTransf[nx][3]
				SD3->D3_QUANT   := aTransf[nx][11]
				SD3->D3_QTSEGUM := nQtd2
				SD3->D3_SEGUM   := cSegUm
				SD3->D3_CF      := "RE4"
				SD3->D3_CONTA   := SB1->B1_CONTA
				SD3->D3_LOCAL   := aTransf[nx][4]
				SD3->D3_DOC     := cDoc
				SD3->D3_EMISSAO := DDATABASE
				SD3->D3_GRUPO   := SB1->B1_GRUPO
				SD3->D3_CUSTO1  := SB2->B2_CM1
				SD3->D3_NUMSEQ  := cDocseq
				SD3->D3_TIPO    := SB1->B1_TIPO
				SD3->D3_USUARIO := UsrRetName(__cUserID)
				SD3->D3_CHAVE   := "E0"
				SD3->D3_LOTECTL := aTransf[nx][8]
				SD3->D3_NUMLOTE := aTransf[nx][10]
				SD3->D3_DTVALID := dDtValid
				SD3->D3_LOCALIZ := aTransf[nx][5]
				SD3->D3_XDBAU	:= cGet15
				MsUnlock()

				//Devolucao
				RecLock("SD3",.T.)
				SD3->D3_FILIAL  := xFilial("SD3")
				SD3->D3_TM      := "499"
				SD3->D3_COD     := aTransf[nx][1]
				SD3->D3_UM      := aTransf[nx][3]
				SD3->D3_QUANT   := aTransf[nx][11]
				SD3->D3_QTSEGUM := nQtd2
				SD3->D3_SEGUM   := cSegUm
				SD3->D3_CF      := "DE4"
				SD3->D3_CONTA   := SB1->B1_CONTA
				SD3->D3_LOCAL   := cLocal
				SD3->D3_DOC     := cDoc
				SD3->D3_EMISSAO := DDATABASE
				SD3->D3_GRUPO   := SB1->B1_GRUPO
				SD3->D3_CUSTO1  := SB2->B2_CM1
				SD3->D3_NUMSEQ  := cDocSeq
				SD3->D3_TIPO    := SB1->B1_TIPO
				SD3->D3_USUARIO := UsrRetName(__cUserID)
				SD3->D3_CHAVE   := "E9"
				SD3->D3_LOTECTL := aTransf[nx][8]
				SD3->D3_NUMLOTE := cSubPad
				SD3->D3_DTVALID := dDtValid
				SD3->D3_LOCALIZ := aTransf[nx][7]
				SD3->D3_XDBAU	:= cGet15
				MsUnlock()

				cNumIDOper := GetSx8Num('SDB','DB_IDOPERA')
				ConfirmSX8()

				DbSelectArea("SDB")
				RecLock("SDB",.T.)
				SDB->DB_FILIAL  := xFilial("SDB")
				SDB->DB_ITEM    := "0001"
				SDB->DB_PRODUTO := aTransf[nx][1]
				SDB->DB_LOCAL   := aTransf[nx][4]
				SDB->DB_LOCALIZ := aTransf[nx][5]
				SDB->DB_DOC     := cDoc
				SDB->DB_TM      := "999"
				SDB->DB_ORIGEM  := "SD3"
				SDB->DB_QUANT   := aTransf[nx][11]
				SDB->DB_QTSEGUM := nQtd2
				SDB->DB_DATA    := DDATABASE
				SDB->DB_LOTECTL := aTransf[nx][8]
				SDB->DB_NUMLOTE := aTransf[nx][10]
				SDB->DB_NUMSEQ  := cDocseq
				SDB->DB_TIPO    := "M"
				SDB->DB_SERVIC  := "999"
				SDB->DB_ATIVID  := "ZZZ"
				SDB->DB_HRINI   := SubStr(Time(),1,5)
				SDB->DB_ATUEST  := "S"
				SDB->DB_STATUS  := "M"
				SDB->DB_ORDATIV := "ZZ"
				SDB->DB_IDOPERA := cNumIDOper
				MsUnlock()

				cNumIDOper := GetSx8Num('SDB','DB_IDOPERA')
				ConfirmSX8()

				RecLock("SDB",.T.)
				SDB->DB_FILIAL  := xFilial("SDB")
				SDB->DB_ITEM    := "0001"
				SDB->DB_PRODUTO := aTransf[nx][1]
				SDB->DB_LOCAL   := aTransf[nx][6]
				SDB->DB_LOCALIZ := aTransf[nx][7]
				SDB->DB_DOC     := cDoc
				SDB->DB_TM      := "499"
				SDB->DB_ORIGEM  := "SD3"
				SDB->DB_QUANT   := aTransf[nx][11]
				SDB->DB_QTSEGUM := nQtd2
				SDB->DB_DATA    := DDATABASE
				SDB->DB_LOTECTL := aTransf[nx][8]
				SDB->DB_NUMLOTE := cSubPad
				SDB->DB_NUMSEQ  := cDocseq
				SDB->DB_TIPO    := "M"
				SDB->DB_SERVIC  := "499"
				SDB->DB_ATIVID  := "ZZZ"
				SDB->DB_HRINI   := SubStr(Time(),1,5)
				SDB->DB_ATUEST  := "S"
				SDB->DB_STATUS  := "M"
				SDB->DB_ORDATIV := "ZZ"
				SDB->DB_IDOPERA := cNumIDOper
				MsUnlock()

				DbSelectArea("SD5")
				RecLock("SD5",.T.)
				SD5->D5_FILIAL  := xFilial("SD5")
				SD5->D5_PRODUTO := aTransf[nx][1]
				SD5->D5_LOCAL   := aTransf[nx][4]
				SD5->D5_DOC     := cDoc
				SD5->D5_DATA    := DDATABASE
				SD5->D5_ORIGLAN := "999"
				SD5->D5_NUMSEQ  := cDocseq
				SD5->D5_QUANT   := aTransf[nx][11]
				SD5->D5_QTSEGUM := nQtd2
				SD5->D5_LOTECTL := aTransf[nx][8]
				SD5->D5_NUMLOTE := aTransf[nx][10]
				SD5->D5_DTVALID := dDtValid
				MsUnlock()

				RecLock("SD5",.T.)
				SD5->D5_FILIAL  := xFilial("SD5")
				SD5->D5_PRODUTO := aTransf[nx][1]
				SD5->D5_LOCAL   := aTransf[nx][6]
				SD5->D5_DOC     := cDoc
				SD5->D5_DATA    := DDATABASE
				SD5->D5_ORIGLAN := "499"
				SD5->D5_NUMSEQ  := cDocseq
				SD5->D5_QUANT   := aTransf[nx][11]
				SD5->D5_QTSEGUM := nQtd2
				SD5->D5_LOTECTL := aTransf[nx][8]
				SD5->D5_NUMLOTE := cSubPad
				SD5->D5_DTVALID := dDtValid
				MsUnlock()

			endif
			//End Transaction

		Next

		cPrxTrn := SOMA1(cTrnDAP,6)
	Else
		MessageBox("SBF - Dados n�o encontrados.","Aten��o",48)
	EndIf

/*
cGet1 := space(15)
cGet2 := space(15)
cGet3 := space(40)
cGet4 := space(15)
cGet5 := space(3)
cGet6 := space(15)
cGet7 := 0
cGet8 := space(2)
cGet10:= Space(2)
cGet9 := Space(15)
//nComboBo1 := 0
oGet1:setfocus()
oDlg:refresh()
*/
Return

Static Function AtuSBF(cPrdSBF,cAOriSBF,cADesSBF,cLotSBF,cSubSBF,nQtdSBF,cSubOri,cESBFOrig,cESBFDest,cFator,nFator)
	Local cQuery  := ""
	Local nRecNo  := 0
	Local nSldSBF := 0
	Local lRet    := .T.

//Atualiza Saldo do amazem de origem
	cQuery := "select BF_PRODUTO,BF_LOCAL,BF_QUANT,BF_LOTECTL,BF_NUMLOTE,R_E_C_N_O_ "
	cQuery += CRLF + "from "+RetSqlName("SBF")+" SBF "
	cQuery += CRLF + "where BF_FILIAL  = '"+xFilial("SBF")+"' "
	cQuery += CRLF + "and   BF_PRODUTO = '"+cPrdSBF+"' "
	cQuery += CRLF + "and   BF_LOCAL   = '"+cAOriSBF+"' "
	cQuery += CRLF + "and   BF_LOTECTL = '"+cLotSBF+"' "
	cQuery += CRLF + "and   BF_NUMLOTE = '"+cSubOri+"' "
	cQuery += CRLF + "and   BF_LOCALIZ = '"+cESBFOrig+"' "
	cQuery += CRLF + "and   D_E_L_E_T_ = '' "

	MemoWrite("HFATA010_AtuSBF_Orig.txt",cQuery)

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSBF", .F., .T.)

	DbSelectArea("TMPSBF")

	If TMPSBF->(!EOF())
		nRecNo  := TMPSBF->R_E_C_N_O_
		nSldSBF := TMPSBF->BF_QUANT - nQtdSBF
		//nSldSBF := IF(nSldSBF < 0,0,nSldSBF)

		nSldSBF2 := 0
		If !Empty(cFator)
			If cFator = "M"
				nSldSBF2 := nSldSBF * nFator
			Else
				nSldSBF2 := nSldSBF / nFator
			EndIf
		EndIf

		cUpdate := "UPDATE "+RetSqlName("SBF")+" SET BF_QUANT = "+AllTrim(Str(nSldSBF))+" , BF_QTSEGUM = "+AllTrim(Str(nSldSBF2))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
		MemoWrite("HFATA010_AtuSBF_Orig_Update.txt",cUpdate)
		nStatus := TCSQLEXEC(cUpdate)

		If nSldSBF = 0
			cUpdate := "UPDATE "+RetSqlName("SBF")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_, BF_QUANT = 0 , BF_QTSEGUM = 0 WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
			MemoWrite("HFATA010_AtuSBF_Orig_Delete.txt",cUpdate)
			nStatus := TCSQLEXEC(cUpdate)
		EndIf

	EndIf

	TMPSBF->(DbCloseArea())

//Atualiza Saldo do armazem de destino
	cQuery := "select BF_PRODUTO,BF_LOCAL,BF_QUANT,BF_LOTECTL,BF_NUMLOTE,R_E_C_N_O_ "
	cQuery += CRLF + "from "+RetSqlName("SBF")+" SBF "
	cQuery += CRLF + "where BF_FILIAL  = '"+xFilial("SBF")+"' "
	cQuery += CRLF + "and   BF_PRODUTO = '"+cPrdSBF+"' "
	cQuery += CRLF + "and   BF_LOCAL   = '"+cADesSBF+"' "
	cQuery += CRLF + "and   BF_LOTECTL = '"+cLotSBF+"' "
	cQuery += CRLF + "and   BF_NUMLOTE = '"+cSubSBF+"' "
	cQuery += CRLF + "and   BF_LOCALIZ = '"+cESBFDest+"' "
	cQuery += CRLF + "and   D_E_L_E_T_ = '' "

	MemoWrite("HFATA010_AtuSBF_Dest.txt",cQuery)

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSBF", .F., .T.)

	DbSelectArea("TMPSBF")
	DbGoTop()

	If TMPSBF->(!EOF())
		nRecNo  := TMPSBF->R_E_C_N_O_
		nSldSBF := TMPSBF->BF_QUANT + nQtdSBF

		nSldSBF2 := 0
		If !Empty(cFator)
			If cFator = "M"
				nSldSBF2 := nSldSBF * nFator
			Else
				nSldSBF2 := nSldSBF / nFator
			EndIf
		EndIf

		cUpdate := "UPDATE "+RetSqlName("SBF")+" SET BF_QUANT = "+AllTrim(Str(nSldSBF))+" , BF_QTSEGUM = "+AllTrim(Str(nSldSBF2))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
		MemoWrite("HFATA010_AtuSBF_Dest_Update.txt",cUpdate)
		nStatus := TCSQLEXEC(cUpdate)
	Else
		nSldSBF2 := 0
		If !Empty(cFator)
			If cFator = "M"
				nSldSBF2 := nQtdSBF * nFator
			Else
				nSldSBF2 := nQtdSBF / nFator
			EndIf
		EndIf

		RecLock("SBF",.T.)
		SBF->BF_FILIAL  := xFilial("SBF")
		SBF->BF_PRODUTO := cPrdSBF
		SBF->BF_LOCAL   := cADesSBF
		SBF->BF_LOCALIZ := cESBFDest
		SBF->BF_LOTECTL := cLotSBF
		SBF->BF_NUMLOTE := cSubSBF
		SBF->BF_QUANT   := nQtdSBF
		SBF->BF_QTSEGUM := nSldSBF2
		MsUnlock()
	EndIf

	TMPSBF->(DbCloseArea())

Return lRet

Static Function AtuSB8(cDocument,cPrdSB8,cAOriSB8,cADesSB8,cLotSB8,cSubSB8,nQtdSB8,cSubOri,cFator,nFator)
	Local cQuery  := ""
	Local nRecNo  := 0
	Local nSldSB8 := 0
	Local lRet    := .T.

//Atualiza Saldo do amazem de origem
	cQuery := "select B8_PRODUTO,B8_LOCAL,B8_SALDO,B8_LOTECTL,B8_NUMLOTE,R_E_C_N_O_ "
	cQuery += CRLF + "from "+RetSqlName("SB8")+" SB8 "
	cQuery += CRLF + "where B8_FILIAL  = '"+xFilial("SB8")+"' "
	cQuery += CRLF + "and   B8_PRODUTO = '"+cPrdSB8+"' "
	cQuery += CRLF + "and   B8_LOCAL   = '"+cAOriSB8+"' "
	cQuery += CRLF + "and   B8_LOTECTL = '"+cLotSB8+"' "
	cQuery += CRLF + "and   B8_NUMLOTE = '"+cSubOri+"' "
	cQuery += CRLF + "and   D_E_L_E_T_ = '' "

	MemoWrite("HFATA010_AtuSB8_Orig.txt",cQuery)

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSB8", .F., .T.)

	DbSelectArea("TMPSB8")

	If TMPSB8->(!EOF())
		nRecNo  := TMPSB8->R_E_C_N_O_
		nSldSB8 := TMPSB8->B8_SALDO - nQtdSB8
		//nSldSB8 := IF(nSldSB8 < 0,0,nSldSB8)

		nSldSB82 := 0
		If !Empty(cFator)
			If cFator = "M"
				nSldSB82 := nSldSB8 * nFator
			Else
				nSldSB82 := nSldSB8 / nFator
			EndIf
		EndIf

		cUpdate := "UPDATE "+RetSqlName("SB8")+" SET B8_DOC = '"+cDocument+"' , B8_SALDO = "+AllTrim(Str(nSldSB8))+" , B8_SALDO2 = "+AllTrim(Str(nSldSB82))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
		MemoWrite("HFATA010_AtuSB8_Dest_Update.txt",cUpdate)
		nStatus := TCSQLEXEC(cUpdate)
	EndIf

	TMPSB8->(DbCloseArea())

//Atualiza Saldo do armazem de destino
	cQuery := "select B8_PRODUTO,B8_LOCAL,B8_SALDO,B8_LOTECTL,B8_NUMLOTE,R_E_C_N_O_ "
	cQuery += CRLF + "from "+RetSqlName("SB8")+" SB8 "
	cQuery += CRLF + "where B8_FILIAL  = '"+xFilial("SB8")+"' "
	cQuery += CRLF + "and   B8_PRODUTO = '"+cPrdSB8+"' "
	cQuery += CRLF + "and   B8_LOCAL   = '"+cADesSB8+"' "
	cQuery += CRLF + "and   B8_LOTECTL = '"+cLotSB8+"' "
	cQuery += CRLF + "and   B8_NUMLOTE = '"+cSubSB8+"' "
	cQuery += CRLF + "and   D_E_L_E_T_ = '' "

	MemoWrite("HFATA010_AtuSB8_Dest.txt",cQuery)

	cQuery := ChangeQuery(cQuery)

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSB8", .F., .T.)

	DbSelectArea("TMPSB8")

	If TMPSB8->(!EOF())
		nRecNo  := TMPSB8->R_E_C_N_O_
		nSldSB8 := TMPSB8->B8_SALDO + nQtdSB8

		nSldSB82 := 0
		If !Empty(cFator)
			If cFator = "M"
				nSldSB82 := nSldSB8 * nFator
			Else
				nSldSB82 := nSldSB8 / nFator
			EndIf
		EndIf

		cUpdate := "UPDATE "+RetSqlName("SB8")+" SET B8_DOC = '"+cDocument+"' , B8_SALDO = "+AllTrim(Str(nSldSB8))+" , B8_SALDO2 = "+AllTrim(Str(nSldSB82))+" , B8_QTDORI = "+AllTrim(Str(nSldSB8))+" , B8_QTDORI2 = "+AllTrim(Str(nSldSB82))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
		MemoWrite("HFATA010_AtuSB8_Dest_Update.txt",cUpdate)
		nStatus := TCSQLEXEC(cUpdate)
	Else
		nSldSB82 := 0
		If !Empty(cFator)
			If cFator = "M"
				nSldSB82 := nQtdSB8 * nFator
			Else
				nSldSB82 := nQtdSB8 / nFator
			EndIf
		EndIf

		RecLock("SB8",.T.)
		SB8->B8_FILIAL  := xFilial("SB8")
		SB8->B8_QTDORI  := nQtdSB8
		SB8->B8_QTDORI2 := nSldSB82
		SB8->B8_PRODUTO := cPrdSB8
		SB8->B8_LOCAL   := cADesSB8
		SB8->B8_DATA    := DDATABASE
		SB8->B8_DTVALID := StoD('20491231')
		SB8->B8_SALDO   := nQtdSB8
		SB8->B8_SALDO2  := nSldSB82
		SB8->B8_ORIGLAN := 'MI' //Movimento Interno
		SB8->B8_LOTECTL := cLotSB8
		SB8->B8_NUMLOTE := cSubSB8
		SB8->B8_DOC     := cDocument
		SB8->B8_DFABRIC := DDATABASE
		MsUnlock()
	EndIf

	TMPSB8->(DbCloseArea())

Return lRet

User Function VldSBE(_end,_loc,cCod)

	lret := .T.

	if _Loc="Piking"
		_qry := "Select * from "+RetSqlName("SBE")+" where BE_LOCALIZ = '"+_end+"' and BE_LOCAL = 'E0' and BE_STATUS <> '3' "
	Else
		_qry := "Select * from "+RetSqlName("SBE")+" where BE_LOCALIZ = '"+_end+"' and BE_LOCAL = 'E1' and BE_STATUS <> '3' "
	Endif

	If Select("QRY") > 0
		QRY->(dbCloseArea())
	EndIf

	DbUseArea(.T.,'TOPCONN',TCGENQRY(,,_Qry),"QRY",.F.,.T.)
	DbGoTop()

	/*If !Empty(cCod)
	If !EOF()
			lRet := .T.
	Else
			MessageBox("Endere�o inv�lido!","Aten��o!",48)
	Endif
EndIf*/

If !EOF()
	If _loc == "Piking" .And. !Empty(cCod)
		_Get6 := QRY->BE_LOCALIZ
		cGet6 := QRY->BE_LOCALIZ
		oDlg:Refresh()	
	Else
		cGet6 := "B.U.IMPORTADO"
	Endif
EndIf

	QRY->(dbCloseArea())

Return(lret)