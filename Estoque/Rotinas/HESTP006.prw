#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"
#include "tbiconn.ch"
#include "ap5mail.ch"
#include "xmlxfun.ch"

user function HESTP006()

	Local aCabec := {}
	Local aItens := {}
	Local aLinha := {}
	Local aColsCC := {}
	Local aCodRet := {}
	Local nX := 0
	Local nY := 0
	Local cDoc := ""
	Local lOk := .T.
	Private lMsErroAuto := .F.
	Private lMsHelpAuto := .T.


	If Select("TMP") > 0
		TMP->(DbCloseArea())
	Endif

	_cQuery := " SELECT  B6_FILIAL,B6_PRODUTO,B6_CLIFOR,B6_LOJA,B6_DOC,B6_SALDO,cast(B6_PRUNIT as DECIMAL(6,2)) AS B6_PRUNIT ,B6_IDENT,B6_LOCAL,cast(B6_SALDO * B6_PRUNIT AS DECIMAL(6,6)) AS TOTAL1 FROM "+RetSqlName("SB6")+" " 
	_cQuery += " JOIN "+RetSqlName("SF2")+" ON B6_DOC = F2_DOC AND B6_CLIFOR = F2_CLIENTE AND B6_LOJA = F2_LOJA AND SF2010.D_E_L_E_T_ = '' " 
	_cQuery += " WHERE SB6010.D_E_L_E_T_ = '' "  
	_cQuery += " AND B6_SALDO > 0 "
	_cQuery += " AND B6_FILIAL = '0101' " 
	_cQuery += " AND B6_TES = '760'  AND B6_CLIFOR = '000343'  " //AND B6_DOC NOT IN ('000730553','000699380','000711214','000744497','000744498') "
	_cQuery += " GROUP BY B6_FILIAL,B6_PRODUTO,B6_CLIFOR,B6_DOC,B6_SALDO,B6_PRUNIT,B6_IDENT,B6_LOJA,B6_LOCAL " 
	//_cQuery += " HAVING cast(B6_SALDO * B6_PRUNIT AS DECIMAL(6,2)) <> '0.00' " //  AND cast(B6_SALDO * B6_PRUNIT AS DECIMAL(6,2)) <> '0.01'  "
	_cQuery += " ORDER BY B6_CLIFOR "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_cQuery),"TMP",.T.,.T.)


	aadd(aCabec,{"F1_TIPO" , "N" , Nil})
	aadd(aCabec,{"F1_FORMUL" , "N" , Nil})
	aadd(aCabec,{"F1_DOC" ,"333333333", Nil})
	aadd(aCabec,{"F1_SERIE" , "1" , Nil})
	aadd(aCabec,{"F1_EMISSAO" ,dDataBase , Nil})
	aadd(aCabec,{"F1_FORNECE" ,TMP->B6_CLIFOR , Nil})
	aadd(aCabec,{"F1_LOJA" ,TMP->B6_LOJA, Nil})
	aadd(aCabec,{"F1_ESPECIE" , "SPED" , Nil})
	
		
	WHILE TMP->(!EOF())
	
	If Select("TMP2") > 0
		TMP2->(DbCloseArea())
	Endif

	_cQuy1 := " SELECT cast(SUM(D1_TOTAL) as DECIMAL(6,2)) AS TOTAL FROM "+RetSqlName("SD1")+" WHERE D1_NFORI = '"+TMP->B6_DOC+"' AND D1_COD = '"+TMP->B6_PRODUTO+"' AND D1_FORNECE = '"+TMP->B6_CLIFOR+"' AND D_E_L_E_T_ = '' " 

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_cQuy1),"TMP2",.T.,.T.)
		
	
	If Select("TMP1") > 0
		TMP1->(DbCloseArea())
	Endif

	_cQuy := " SELECT * FROM "+RetSqlName("SD2")+" WHERE D2_DOC = '"+TMP->B6_DOC+"' AND D2_CLIENTE = '"+TMP->B6_CLIFOR+"' AND D2_COD = '"+TMP->B6_PRODUTO+"' AND D2_IDENTB6 = '"+TMP->B6_IDENT+"' AND D_E_L_E_T_ = '' " 

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_cQuy),"TMP1",.T.,.T.)
	
		aLinha := {}
		
		aadd(aLinha,{"D1_COD" ,TMP->B6_PRODUTO , Nil})
		aadd(aLinha,{"D1_QUANT" , TMP->B6_SALDO , Nil})
		if (TMP1->D2_TOTAL - TMP2->TOTAL) <= 0
		aadd(aLinha,{"D1_VUNIT" , TMP->TOTAL1 / TMP->B6_SALDO , Nil}) //TMP->B6_PRUNIT , Nil})
		aadd(aLinha,{"D1_TOTAL" , TMP->TOTAL1 , Nil})
		else
		aadd(aLinha,{"D1_VUNIT" , (TMP1->D2_TOTAL - TMP2->TOTAL) / TMP->B6_SALDO , Nil}) //TMP->B6_PRUNIT , Nil})
		aadd(aLinha,{"D1_TOTAL" , TMP1->D2_TOTAL - TMP2->TOTAL , Nil})
		endif
		aadd(aLinha,{"D1_TES" , "318" , Nil})
		aadd(aLinha,{"D1_LOCAL" , TMP->B6_LOCAL , Nil})
		aadd(aLinha,{"D1_NFORI" ,ALLTRIM(TMP->B6_DOC) , Nil})
		aadd(aLinha,{"D1_SERIORI" ,"2", Nil})
		aadd(aLinha,{"D1_IDENTB6" ,TMP->B6_IDENT, Nil})
		aadd(aLinha,{"D1_ITEMORI" ,TMP1->D2_ITEM, Nil})
		aadd(aLinha,{"D1_CLASFIS" ,TMP1->D2_CLASFIS, Nil})
		aadd(aItens,aLinha)
		
		TMP->(DBSKIP())
	END

	MATA103(aCabec,aItens,3) //ExpN1 - Op��o desejada: 3-Inclus�o; 4-Altera��o ; 5-Exclus�o
	If !lMsErroAuto
		ConOut("Incluido com sucesso! "+cDoc)
	Else
		MostraErro()
		ConOut("Erro na inclusao!")
	EndIf

Return