#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} AXCADZAD
Fun��o de Teste
@Desc PROGRAMA PARA MONTAR TELA DE CADASTRO DE OCASIOES
@type function
@author R. Melo 
@since 07/10/2019
@version 1.0
/*/

user function AXCADZAD()
    Local aArea    := GetArea()
    Local aAreaZAD := ZAD->(GetArea())
    Local cDelOk   := ".T."
    Local cFunTOk  := ".T."
     
    //Chamando a tela de cadastros
    AxCadastro('ZAD', 'Cadastro de Ocasi�es', cDelOk, cFunTOk)
     
    RestArea(aAreaZAD)
    RestArea(aArea)
Return
