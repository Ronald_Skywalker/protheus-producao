#include 'protheus.ch'
#include 'parmtype.ch'

//--------------------------------------------------------------
/*/{Protheus.doc} MyFunction: HESTP011 - Cadastro de Adensamento

@param xParam Parameter Description
@return xRet Return Description
@author Ronaldo Pereira -
@since 19/11/2020
/*/
//--------------------------------------------------------------
User Function HESTP009()
Local oButton1
Local oButton2
Local oButton3
Local oCheckBo1
Local oGet2
Local oGet3
Local oGet4
Local oGet5
Local oGet6
Local oGet7
Local oSay1
Local oSay2
Local oSay3
Local oSay4
Local oSay5
Local oSay6
Local oSay7
Local oGroup1

Private oGet1	
Private cGetPrd1 	:= SPACE(8)
Private cGetPrd2 	:= SPACE(8)
Private cGetCor		:= SPACE(4)
Private cGetTam1 	:= SPACE(4)
Private cGetTam2 	:= SPACE(4)
Private nGetQtd1 	:= 0
Private nGetQtd2 	:= 0
Private lCheckBo1	:= .F.

Static oDlg   
  
  DEFINE MSDIALOG oDlg TITLE "Cadastro Adensamento" FROM 000, 000  TO 550, 800 COLORS 0, 16777215 PIXEL

    @ 011, 007 SAY oSay1 PROMPT "Refer�ncia de:" SIZE 036, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 009, 044 MSGET oGet1 VAR cGetPrd1 SIZE 037, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 011, 086 SAY oSay2 PROMPT "At�:" SIZE 013, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 009, 099 MSGET oGet2 VAR cGetPrd2 SIZE 037, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 011, 150 SAY oSay3 PROMPT "Cor:" SIZE 012, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 010, 163 MSGET oGet3 VAR cGetCor SIZE 022, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 011, 198 SAY oSay4 PROMPT "Tamanho de:" SIZE 033, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 009, 230 MSGET oGet4 VAR cGetTam1 SIZE 022, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 011, 260 SAY oSay5 PROMPT "At�:" SIZE 013, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 009, 272 MSGET oGet5 VAR cGetTam2 SIZE 022, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 010, 308 CHECKBOX oCheckBo1 VAR lCheckBo1 PROMPT "Zerado" SIZE 030, 008 OF oDlg COLORS 0, 16777215 PIXEL 
    @ 008, 349 BUTTON oButton1 PROMPT "Consultar" SIZE 037, 012 OF oDlg ACTION Progresso() PIXEL
    @ 025, 004 GROUP oGroup1 TO 060, 397 PROMPT " Adensamento:  " OF oDlg COLOR 0, 16777215 PIXEL
    fMSNewGe1()
    @ 040, 075 SAY oSay6 PROMPT "Qtde Maior:" SIZE 030, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 038, 104 MSGET oGet6 VAR nGetQtd1 SIZE 025, 010 OF oDlg PICTURE "@R 9999" COLORS 0, 16777215 PIXEL
    @ 040, 159 SAY oSay7 PROMPT "Qtde Menor:" SIZE 032, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 038, 191 MSGET oGet7 VAR nGetQtd2 SIZE 025, 010 OF oDlg PICTURE "@R 9999" COLORS 0, 16777215 PIXEL
    @ 038, 350 BUTTON oButton2 PROMPT "Cadastrar" SIZE 037, 012 OF oDlg ACTION Salvar() PIXEL
    @ 258, 351 BUTTON oButton3 PROMPT "Sair" SIZE 037, 012 OF oDlg ACTION (oDlg:End()) PIXEL 

  ACTIVATE MSDIALOG oDlg CENTERED

Return

Static Function Progresso()
		Processa( {|| fMSNewGe1() }, "Aguarde...", "Processando...",.F.)
Return

//------------------------------------------------ 
Static Function fMSNewGe1()
//------------------------------------------------ 
Local nX
Local aHeaderEx := {}
Local aColsEx := {}
Local aFieldFill := {}
Local aFields   := {"B1_COD"  ,"B1_COD"  	,"B4_GRUPO"  ,"B4_GRUPO" ,"B5_QE1"     ,"B5_QE1"     ,"B1_DESC"}
Local aFields2 	:= {"PRODUTO" ,"REFERENCIA" ,"COR"	     ,"TAM"	     ,"QTDE_MAIOR" ,"QTDE_MENOR" ,"DESCRICAO"}
Local aAlterFields := {}
Static oMSNewGe1

  aColsEx := Consulta()
   
  // Define field properties
  DbSelectArea("SX3")
  SX3->(DbSetOrder(2))
  For nX := 1 to Len(aFields)
    If SX3->(DbSeek(aFields[nX]))
      Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
                       SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
    Endif
  Next nX

  // Define field values
  For nX := 1 to Len(aFields)
    If DbSeek(aFields[nX])
      Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
    Endif
  Next nX
  Aadd(aFieldFill, .F.)
  Aadd(aColsEx, aFieldFill)

  oMSNewGe1 := MsNewGetDados():New( 064, 004, 254, 396, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx) 
	
return


Static Function Consulta()
local cQry      := ""
Local aColsEx2  := {}
local cProd1    := alltrim(cGetPrd1)
local cProd2    := alltrim(cGetPrd2)
local cCor		:= alltrim(cGetCor)			
local cTam1		:= alltrim(cGetTam1)
local cTam2		:= alltrim(cGetTam2)
	

	If !Empty(cProd1) .OR. lCheckBo1 = .T.
	
		cQry := " SELECT * FROM ( "
		cQry += " SELECT B1.B1_COD AS PRODUTO, "         
		cQry += " 	   B4.B4_COD AS REFERENCIA, " 
		cQry += " 	   SUBSTRING(B1.B1_COD,9,3) AS COR, " 
		cQry += " 	   CASE WHEN RIGHT(B1_COD,4) LIKE '000%' THEN REPLACE(RIGHT(B1_COD,4),'000','') " 
		cQry += " 			WHEN RIGHT(B1_COD,4) LIKE '00%' THEN REPLACE(RIGHT(B1_COD,4),'00','') " 
		cQry += " 			WHEN RIGHT(B1_COD,4) LIKE '0%' THEN SUBSTRING(RIGHT(B1_COD,4),2,4) " 
		cQry += " 	   ELSE REPLACE(RIGHT(B1_COD,4),'00','') END TAMANHO, " 
		cQry += " 	   ISNULL(B5.B5_QE1,0) AS QTDE_MAIOR, " 
		cQry += " 	   ISNULL(B5.B5_QE2,0) AS QTDE_MENOR, "
		cQry += " 	   B1.B1_DESC AS DESCRICAO "
		cQry += " FROM "+RetSqlName("SB1")+" AS B1 WITH (NOLOCK) " 
		cQry += " INNER JOIN "+RetSqlName("SB4")+" AS B4 WITH (NOLOCK) ON (B4.B4_COD = LEFT(B1.B1_COD,8) AND B4.D_E_L_E_T_ = '') "
		cQry += " LEFT  JOIN "+RetSqlName("SB5")+" AS B5 WITH (NOLOCK) ON (B5.B5_COD = B1.B1_COD AND B5.D_E_L_E_T_ = '') "
		cQry += " WHERE B1.D_E_L_E_T_ =	'' AND B1.B1_TIPO IN ('PF','KT','PI','ME') "
		
		If !Empty(cProd1)
			cQry += " AND B4_COD BETWEEN '"+cProd1+"' AND '"+cProd2+"' "
		EndIf
		
		If !Empty(cCor)
			cQry += " AND SUBSTRING(B1_COD,9,3) = '"+cCor+"' "
		EndIf
		
		If lCheckBo1 = .T.
			cQry += " AND (B5.B5_QE1 = 0 OR B5.B5_QE1 IS NULL) "
		EndIf	
			
			cQry += " ) T " 
		
		If !Empty(cTam1)
			cQry += " WHERE T.TAMANHO BETWEEN '"+cTam1+"' AND '"+cTam2+"' "
		EndIf
		
		If Select("TMPRD") > 0
	        TMPRD->(DbCloseArea())
	    EndIf
	
	    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPRD",.T.,.T.)
	    DbSelectArea("TMPRD")
	    DbGoTop()
	
	    While TMPRD->(!EOF()) 
	        aAdd(aColsEx2,{TMPRD->PRODUTO,TMPRD->REFERENCIA,TMPRD->COR,TMPRD->TAMANHO,TMPRD->QTDE_MAIOR,TMPRD->QTDE_MENOR,TMPRD->DESCRICAO,.F.})
	        TMPRD->(DbSkip())
	    EndDo		
		
	EndIf 
	
Return (aColsEx2)


Static Function Salvar()
Local nX
Local aColSKU := {}

    aColSKU := Consulta()
        
	If !Empty(aColSKU) .And. nGetQtd1 <> 0 .And. nGetQtd2 <> 0
		nRet3 := MessageBox("Confirmar cadastro?","Confirma��o",4)
	    If  nRet3 == 6     	
	     	DbselectArea("SB5")
	    	DbSetOrder(1)
	    	For nX := 1 to Len(aColSKU)
	    		If DbSeek(xfilial("SB5")+aColSKU[nX][1])	    			
	    			RECLOCK("SB5",.F.)
		    		SB5->B5_QE1 := nGetQtd1 
		    		SB5->B5_QE2 := nGetQtd2
		    		MSUNLOCK()	
		    	Else
		    	   	RECLOCK("SB5",.T.)
		    	   	SB5->B5_COD := aColSKU[nX][1]
		    	   	SB5->B5_CEME:= aColSKU[nX][7]
		    		SB5->B5_QE1 := nGetQtd1 
		    		SB5->B5_QE2 := nGetQtd2
		    		MSUNLOCK()			
	    		Endif
	    	Next nX		
				MessageBox("Cadastro realizado com Sucesso!","Aviso",0)
				
				oDlg:refresh()
				oGet1:setfocus()
			    fMSNewGe1()
		Else
	        MessageBox("Cancelado pelo usu�rio!","Aten��o",16)
	        oGet1:setfocus()
	    EndIf
	Else
	   MessageBox("Consulte um Produto!","Aten��o",16)
	   oGet1:setfocus()
	EndIf    

Return