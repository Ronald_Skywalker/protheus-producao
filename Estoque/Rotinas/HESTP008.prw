#include 'protheus.ch'
#include 'parmtype.ch'

//--------------------------------------------------------------
/*/{Protheus.doc} Cadastro do Vi�s
Cadastrar dados dos produtos que envolve o calculo do vi�s na OP                                                     
                                                                
@param xParam Parameter Description                             
@return xRet Return Description                                 
@author Ronaldo Pereira -                                               
@since 02/07/2020                                                   
/*/                                                             
//--------------------------------------------------------------
User Function HESTP008()                        

Local oButton1
Local oButton2
Local oButton3
Local oComboBo1

Local oGet2
Local oGet3
Local oGet4
Local oGet5
Local oGet6
Local oGet8
Local oGet9
Local oGet10
Local oGet11
Local oGroup1
Local oGroup2
Local oSay1
Local oSay2
Local oSay3
Local oSay4
Local oSay5
Local oSay6
Local oSay9
Local oSay10
Local oSay11
Local oSay12
Local oSay15
Local cDescr1  := ""
local cDescr2  := ""

Private oGet1
Private oGet7
Private cCodRef 	:= SPACE(8)
Private nCos 		:= 0
Private nCava 		:= 0
Private nDecote 	:= 0
Private nAlca 		:= 0
Private nComp		:= 0
Private cCodVies	:= SPACE(8)
Private cGetCor 	:= SPACE(3)
Private cTipoVies	:= 0
Private nLrgVies	:= 0
Private nLrgTec 	:= 0
private cData1 		:= SPACE(10)

Static oDlg

  DEFINE MSDIALOG oDlg TITLE "CADASTRO DADOS DO VI�S" FROM 000, 000  TO 550, 660 COLORS 0, 16777215 PIXEL

    @ 007, 004 GROUP oGroup1 TO 075, 327 PROMPT " REFER�NCIA:  " OF oDlg COLOR 0, 16777215 PIXEL   
    @ 024, 016 SAY oSay1 PROMPT "C�digo:" SIZE 019, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 022, 036 MSGET oGet1 VAR cCodRef SIZE 037, 010 OF oDlg VALID cDescr1 := DadosRef(alltrim(cCodRef)) PICTURE "@!" COLORS 0, 16777215 PIXEL
    @ 024, 081 SAY oSay8 PROMPT "Data Atualiza��o:" SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 022, 125 MSGET oGet11 VAR cData1 SIZE 037, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL 
    @ 038, 016 SAY cDescr1 SIZE 303, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 055, 016 SAY oSay2 PROMPT "C�S:" SIZE 014, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 053, 030 MSGET oGet2 VAR nCos SIZE 025, 010 OF oDlg PICTURE "@E 99.99" COLORS 0, 16777215 PIXEL
    @ 055, 059 SAY oSay3 PROMPT "CAVA:" SIZE 018, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 053, 077 MSGET oGet3 VAR nCava SIZE 025, 010 OF oDlg PICTURE "@E 99.99" COLORS 0, 16777215 PIXEL
    @ 055, 106 SAY oSay4 PROMPT "DECOTE:" SIZE 024, 006 OF oDlg COLORS 0, 16777215 PIXEL
    @ 053, 131 MSGET oGet4 VAR nDecote SIZE 025, 010 OF oDlg PICTURE "@E 99.99" COLORS 0, 16777215 PIXEL
    @ 055, 161 SAY oSay5 PROMPT "AL�A:" SIZE 017, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 053, 177 MSGET oGet5 VAR nAlca SIZE 025, 010 OF oDlg PICTURE "@E 99.99" COLORS 0, 16777215 PIXEL
    @ 055, 206 SAY oSay6 PROMPT "COMPLEMENTO:" SIZE 044, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 053, 250 MSGET oGet6 VAR nComp SIZE 025, 010 OF oDlg PICTURE "@E 99.99" COLORS 0, 16777215 PIXEL
    @ 052, 284 BUTTON oButton1 PROMPT "Atualizar" SIZE 037, 012 OF oDlg ACTION AtualizRef(cCodRef,nCos,nCava,nDecote,nAlca,nComp) PIXEL
    
    @ 079, 004 GROUP oGroup2 TO 256, 327 PROMPT " PRODUTO VI�S: " OF oDlg COLOR 0, 16777215 PIXEL                   
    @ 094, 016 SAY oSay9 PROMPT "C�digo:" SIZE 020, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 093, 037 MSGET oGet7 VAR cCodVies SIZE 037, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 PIXEL 
    @ 092, 125 BUTTON oButton4 PROMPT "Consultar" SIZE 037, 012 OF oDlg ACTION cDescr2 := DadosVies(alltrim(cCodVies)) PIXEL
    @ 094, 082 SAY oSay15 PROMPT "Cor:" SIZE 011, 007 OF oDlg COLORS 0, 16777215 PIXEL 
    @ 093, 095 MSGET oGet8 VAR cGetCor SIZE 022, 010 OF oDlg VALID DadosVies(alltrim(cCodVies)) PICTURE "@!" COLORS 0, 16777215 PIXEL 
    @ 109, 016 SAY cDescr2 SIZE 303, 007 OF oDlg COLORS 0, 16777215 PIXEL   
    @ 125, 016 SAY oSay10 PROMPT "Lagura do Vi�s:" SIZE 040, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 124, 059 MSGET oGet9 VAR nLrgVies SIZE 027, 010 OF oDlg PICTURE "@E 99.9999" COLORS 0, 16777215 PIXEL
    @ 125, 100 SAY oSay11 PROMPT "Largura do Tecido:" SIZE 047, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 124, 149 MSGET oGet10 VAR nLrgTec SIZE 025, 010 OF oDlg PICTURE "@E 99.99" COLORS 0, 16777215 PIXEL
    @ 125, 184 SAY oSay12 PROMPT "Tipo do Vi�s:" SIZE 032, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 124, 218 MSCOMBOBOX oComboBo1 VAR cTipoVies ITEMS {"Esticando","Travando"} SIZE 046, 010 OF oDlg COLORS 0, 16777215 PIXEL
    fMSNewGe1()
    @ 123, 283 BUTTON oButton2 PROMPT "Atualizar" SIZE 037, 012 OF oDlg ACTION AtualizVies(cTipoVies,nLrgVies,nLrgTec) PIXEL    
    @ 260, 284 BUTTON oButton3 PROMPT "Sair" SIZE 037, 012 OF oDlg ACTION (oDlg:End()) PIXEL
              
  ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------
Static Function fMSNewGe1()
//------------------------------------------------
Local nX
Local aHeaderEx 	:= {}
Local aColsEx 		:= {}
Local aFieldFill 	:= {}
Local aFields   	:= {"B1_COD"  ,"B1_COD"  	,"B4_GRUPO" ,"B1_XLRGVIE"   ,"B1_XLRGTEC"     ,"B1_XTPVIES","B1_XDTATUL"}
Local aFields2 		:= {"PRODUTO" ,"REFERENCIA" ,"COR"	    ,"LARGURA VIES" ,"LARGURA TECIDO" ,"TIPO VIES" ,"DT ATUALIZACAO" }
Local aAlterFields 	:= {}
Static oMSNewGe1

  aColsEx := ConsultVies()

  // Define field properties
  DbSelectArea("SX3")
  SX3->(DbSetOrder(2))
  For nX := 1 to Len(aFields)
    If SX3->(DbSeek(aFields[nX]))
      Aadd(aHeaderEx, {aFields2[nX],SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
                       SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
    Endif
  Next nX

  // Define field values
  For nX := 1 to Len(aFields)
    If DbSeek(aFields[nX])
      Aadd(aFieldFill, "")
    Endif
  Next nX
  Aadd(aFieldFill, .F.)
  Aadd(aColsEx, aFieldFill)

  oMSNewGe1 := MsNewGetDados():New( 141, 009, 250, 321, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
 
Return


Static Function DadosRef(cCodRef)
local cDesc := ""

	If !Empty(cCodRef)
		DbselectArea("SB4")
		DbSetOrder(1)
		IF DbSeek(xfilial("SB4")+cCodRef)	    
		    cData1		:= SB4->B4_YDTATUL		    	
			nCos    	:= SB4->B4_YTAMCOS
			nCava		:= SB4->B4_YTAMCAV
			nDecote		:= SB4->B4_YTAMDEC
			nAlca		:= SB4->B4_YTAMALC
			nComp		:= SB4->B4_YTAMCOM
			cDesc  		:= Alltrim(SB4->B4_DESC)		
			oDlg:refresh()
			DbCloseArea()	
		Else
			MessageBox("Refer�ncia Inexistente!","Aten��o",48)
			oGet1:setfocus()
		EndIf
	EndIf	

Return (cDesc)


Static Function AtualizRef(cCodRef,nCos,nCava,nDecote,nAlca,nComp)
	
	nRet3 := MessageBox("Confirmar atualiza��o da Refer�ncia?","Confirma��o",4)
    If  nRet3 == 6
		DbselectArea("SB4")
		DbSetOrder(1)
		IF DbSeek(xfilial("SB4")+cCodRef)		
			RECLOCK("SB4",.F.)
			SB4->B4_YDTATUL := DDATABASE 
			SB4->B4_YTAMCOS := nCos
			SB4->B4_YTAMCAV := nCava
			SB4->B4_YTAMDEC := nDecote
			SB4->B4_YTAMALC := nAlca
			SB4->B4_YTAMCOM := nComp
			MSUNLOCK()			
				
			MessageBox("Atualiza��o realizada com Sucesso!","Aviso",0)
			
			oDlg:refresh()
			oGet1:setfocus()
		Else
			MessageBox("Refer�ncia Inexistente!","Aten��o",48)
			oGet1:setfocus()
		EndIf
	Else
        MessageBox("Cancelado pelo usu�rio!","Aten��o",16)
        oGet1:setfocus()
    EndIf
    
Return


Static Function DadosVies(cCodVies)
local cDesc   := ""
local cQuery  := ""
local cProd   := alltrim(cCodVies)

	If !Empty(cProd) 
		cQuery := " SELECT B1_COD AS COD, "
		cQuery += " 	   B1_DESC AS DESCRICAO "
		cQuery += " FROM "+RetSqlName("SB1")+" AS B1 WITH (NOLOCK) "
		cQuery += " INNER JOIN "+RetSqlName("SB4")+" AS B4 WITH (NOLOCK) ON (B4.B4_COD = LEFT(B1.B1_COD, 8) "
		cQuery += "                                           AND B4.D_E_L_E_T_='') "
		cQuery += " WHERE B1.D_E_L_E_T_ = '' "
		cQuery += "   AND B1_GRUPO IN ('MP08', "
		cQuery += "                    'MP11') "
		cQuery += "   AND B4_COD = '"+cProd+"' "		
        
		If Select("TMVIE") > 0
			TMVIE->(DbCloseArea())
		EndIf
	
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMVIE",.T.,.T.)
		DbSelectArea("TMVIE")
		DbGoTop()	        
    
		IF TMVIE->(!EOF())	  
			cDesc := alltrim(TMVIE->DESCRICAO)							
		Else
			MessageBox("Produto Inexistente!","Aten��o",48)
			oGet7:setfocus()
		EndIf
		fMSNewGe1()
		oDlg:refresh()	
	EndIf    
    
Return (cDesc)


Static Function ConsultVies()
local cQry      := ""
Local aColsEx2  := {}
local cProd     := alltrim(cCodVies)
local cCor		:= alltrim(cGetCor)	
local cTpVies   := ""		
				
	If !Empty(cProd) 		
		cQry := " SELECT B1_COD AS PRODUTO, "
	    cQry += "        B4_COD AS REFERENCIA, "
	    cQry += "        SUBSTRING(B1.B1_COD, 9, 3) AS COR, "
	    cQry += "        B1_XLRGVIE AS LARGURA_VIES, "
	    cQry += "        B1_XLRGTEC AS LARGURA_TECIDO, "
	    cQry += "        B1_XTPVIES AS TIPO_VIES, "
	    cQry += "        CASE "
        cQry += "           WHEN B1_XDTATUL <> '' THEN CONVERT(CHAR, CAST(B1_XDTATUL AS SMALLDATETIME), 103) "
        cQry += "           ELSE '' "
        cQry += "        END DT_ATUALIZ "
	    cQry += " FROM "+RetSqlName("SB1")+" AS B1 WITH (NOLOCK) "
	    cQry += " INNER JOIN "+RetSqlName("SB4")+" AS B4 WITH (NOLOCK) ON (B4.B4_COD = LEFT(B1.B1_COD, 8) "
	    cQry += "                                           AND B4.D_E_L_E_T_='') "
	    cQry += " WHERE B1.D_E_L_E_T_ = '' "
	    cQry += "   AND B1_GRUPO IN ('MP08', "
	    cQry += "                    'MP11') "
	    cQry += "   AND B4_COD = '"+cProd+"' "	    
	    
	    If !Empty(cCor)
	    	cQry += " AND SUBSTRING(B1.B1_COD, 9, 3) = '"+cCor+"' "
	    EndIf	    
	    	
	    If Select("TMPRD") > 0
	        TMPRD->(DbCloseArea())
	    EndIf
	
	    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPRD",.T.,.T.)
	    DbSelectArea("TMPRD")
	    DbGoTop()
	
	    While TMPRD->(!EOF()) 
	        If TMPRD->TIPO_VIES = "E"
	        	cTpVies := "Esticando"
	        ElseIf TMPRD->TIPO_VIES = "T"
	        	cTpVies := "Travando"
	        Else
	            cTpVies := ""	
	        EndIf
	        aAdd(aColsEx2,{TMPRD->PRODUTO,TMPRD->REFERENCIA,TMPRD->COR,TMPRD->LARGURA_VIES,TMPRD->LARGURA_TECIDO,cTpVies,TMPRD->DT_ATUALIZ,.F.})
	        TMPRD->(DbSkip())	        
	    EndDo	    
	    oDlg:refresh()
	    
	EndIf      
    
return (aColsEx2)


Static Function AtualizVies(cTipoVies,nLrgVies,nLrgTec)
Local nX
Local aColSKU := {}

    aColSKU := ConsultVies()

	nRet3 := MessageBox("Confirmar atualiza��o do Produto?","Confirma��o",4)
    If  nRet3 == 6
		DbselectArea("SB1")
		DbSetOrder(1)
		For nX := 1 to Len(aColSKU)
			IF DbSeek(xfilial("SB1")+aColSKU[nX][1])		
				RECLOCK("SB1",.F.)
				SB1->B1_XDTATUL := DDATABASE
				If cTipoVies = "Travando"
					SB1->B1_XTPVIES = "T"
				ElseIf cTipoVies = "Esticando"
					SB1->B1_XTPVIES = "E"
				EndIf	
				SB1->B1_XLRGVIE := nLrgVies
				SB1->B1_XLRGTEC := nLrgTec
				MSUNLOCK()	
			Else
				MessageBox("Produto Inexistente!","Aten��o",48)
				oGet7:setfocus()
			EndIf		
		Next nX		
			MessageBox("Atualiza��o realizada com Sucesso!","Aviso",0)
			
			/*cCodVies 	:= SPACE(8)
			cGetCor     := SPACE(4)
			cTipoVies   := 0
			nLrgVies	:= 0
			nLrgTec		:= 0*/
			oDlg:refresh()
			oGet7:setfocus()
			fMSNewGe1()		
	Else
        MessageBox("Cancelado pelo usu�rio!","Aten��o",16)
        oGet7:setfocus()
    EndIf

Return   