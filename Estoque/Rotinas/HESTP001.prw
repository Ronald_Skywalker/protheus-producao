#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
//#include "apwebsrv.ch" 
//#include "apwebex.ch"
//#include "ap5mail.ch"
#include "TOTVS.CH"

// Retornos poss�veis do MessageBox
#Define IDOK			    1
#Define IDCANCEL		    2
#Define IDYES			    6
#Define IDNO			    7

#define DS_MODALFRAME   128
#Define CRLF  CHR(13)+CHR(10)

// HP_USRPROD - Us�rios com o bot�o OK habilitado
// HP_USERGRA - Acesso a tela de grade

/*
�����������������������������������������������������������������������������
?????????????????????????????????????????????????????????????????????????????
??������������������������������������������������������������������������???
???Programa  ?HESTP001  ?Autor  ? Actual Trend       ? Data ?  13/02/17   ???
??������������������������������������������������������������������������???
???Desc.     ?Rotina de cadastro de Produtos e Estrutura por Grade        ???
??������������������������������������������������������������������������???
?????????????????????????????????????????????????????????????????????????????
�����������������������������������������������������������������������������
*/

User Function HESTP001()

	local cVldAlt	:= ".T."
	local cVldExc	:= ".T."
	local cAliasd

	cAlias	:= "SB4"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	private cCadastro := "Cadastro de Grade de Produtos"
	Private cLinha		:= "COR"
	Private wperg := 0
	Private aEST := {}
	Private aEST2:= {}
	Private aEST3:= {}
	Private aEST4:= {}
	Private cTabela		:= Space(03)
	Private lLmpCor	:= .F.
	Private lLmpTam	:= .F.
	Private lLmpCon	:= .F.
	Private lCodBar	:= .F.
	Private cMvEstru := Alltrim(SuperGetMV("HP_VLDESTR",.F.,'MIAL,MIRO,MIVI,MIEE,MIEC,MIHD,MIRB'))


	aRotina := {;
		{ "Pesquisar" 				, "AxPesqui"			, 0, 1},;
		{ "Vis. Grade"				, "U_GrdProd(4)"		, 0, 2},;
		{ "Inc. Grade"				, "U_GrdProd(1)"		, 0, 3},;
		{ "Alt. Grade"				, "U_GrdProd(2)"		, 0, 4},;
		{ "Inc. Ficha"				, "U_GrdProd(11)"		, 0, 2},;
		{ "Alt. Ficha"				, "U_GrdProd(12)"		, 0, 3},;
		{ "Alt. Roterio"			, "U_GrdProd(13)"		, 0, 3},;
		{ "Cadastro de Tempos"		, "U_GrdProd(14)"		, 0, 3},;
		{ "Inc. Opcionais"			, "U_GrdProd(15)"		, 0, 2},;
		{ "Alt. Opcionais"			, "U_GrdProd(16)"		, 0, 3},;
		{ "Cod.Barras"				, "U_GrdProd(7)"		, 0, 3},;
		{ "Inc. Mix  "				, "U_GrdProd(8)"		, 0, 3},;
		{ "Alt. Mix  "				, "U_GrdProd(9)"		, 0, 3},;
		{ "Alt.Status"				, "U_GrdProd(10)"		, 0, 3},;
		{ "Inc. Pre�o"				, "U_PrecPro(1)"		, 0, 3},;
		{ "Vis. Pre�o"				, "U_PrecPro(3)"		, 0, 3},;
		{ "Ajusta Cor"				, "U_AjuCor()"			, 0, 3},;
		{ "Estrutura" 				, "U_EstPro1(1)"		, 0, 3},;
		{ "Limpa Registros"			, "U_Exclui()"			, 0, 3},;
		{ "Relat. Ficha T�cnica"	, "U_RHEST001(.T.)"		, 0, 3},;
		{ "Relat. Medid/Tama"		, "U_RHEST002(.T.)"		, 0, 3},;
		{ "Cad ProdxFornece"		, "U_HPROXFO()"			, 0, 3},;
		{ "Cad Peso e Medidas PA"	, "U_HESTP012()"		, 0, 3},;
		{ "Cadastro Stopper"        , "U_AXCADZBC()"		, 0, 3},;
		{ "Produto x Stopper"       , "U_AXCADZBD()"		, 0, 3},;
		{ "Importa��o Stopper"      , "U_HPSTOPPER()"		, 0, 3}}

	dbSelectArea(cAlias)
	mBrowse( 6, 1, 22, 75, cAlias)

return

User Function GrdProd(_tp)

// Esta rotina � a respons�vel pelo cadastro da grade de produto, criando SB4 (Grade de Produtos) e SB1 (Produtos)
// SX5 criado: Z3
// Consulta Padr�o: SBM-02/SX5-Z3

	Local cCabecalho

	Local oSay01
	Local oSay02
	Local oSay03
	Local oSay04
	Local oSay05
	Local oSay06
	Local oSay07
	Local oSay08
	Local oSay09
	Local oSay10
	Local oSay11
	Local oSay12
	Local oSay13
	Local oSay14
	Local oSay15
	Local oSay16
	Local oSay17
	Local oSay18
	Local oSay19
	Local oSay20
	Local oSay21
	Local oSay22
	Local oSay23
	Local oSay24
	Local oSay25
	Local oSay26
	Local oSay27
	Local oSay28
	Local oSay29
	Local oSay30
	Local oSay31
	Local oSay32
	Local oSay33
	Local oSay34
	Local oSay35
	Local oSay36
	Local oSay37
	Local oSay38
	Local oSay39
	Local oSay40
	Local oSay41
	Local oSay42
	Local oSay43
	Local oSay44
	Local oSay45
	Local oSay46

	Local oTipoDesc
	Local oOrigemDesc
	Local oGrupoDesc
	Local oCodigoDesc
	Local oUM
	Local oMarcaDesc
	Local oCicloDesc
	Local oGpProd
	Local oGpProdDesc
	Local oSGrupo
	Local oSGrupoDesc
	Local oColecao
	Local oColecaoDesc
	Local oSubCol
	Local oSubColDesc
	Local oTpProd
	Local oTpProdDesc
	Local oCatProd
	Local oCatProdDesc
	Local oOcasiao
	Local oOcasiaoDesc
	Local oTecido
	Local oRastro
	Local oEnd
	Local oLinha
	Local oPeso
	Local oDtCad
	Local oWEB
	Local oDescriPTB
	Local oDescriENG
	Local oDescriSPA
	Local oObsProd
	Local oFoto

	Local oButton1
	Local oButton2
	Local oButton3
	Local lSave			:= .F.

	Private oFolder1
	Private nLin
	Private oTipo
	Private oOrigem
	Private oGrupo
	Private oCodigo
	Private oMarca
	Private oCiclo
	Private oTipo
	Private oGpProd
	Private oSGrupo
	Private oColecao
	Private oSubCol
	Private oTpProd
	Private oCatProd
	Private oOcasiao
	Private oArm
	Private oColuna
	Private oPosIPI
	Private oTempo
	Private cTipo			:= Space(02)
	Private cTipoDesc		:= Space(60)
	Private cClass			:= Space(05)
//	Private cClassDesc		:= Space(60)
	Private cOrigem			:= Space(1)
	Private cOrigemDesc		:= POSICIONE("SX5",1,xFilial("SX5")+'S0'+cOrigem,"X5_DESCRI")
	Private cGrupo			:= Space(04)
	Private cGrupoDesc		:= Space(60)
	Private cCodigo			:= Space(26)
	Private cCodigoDesc		:= Space(120)
	Private cUM				:= Space(02)
	Private cMarca			:= Space(05)
	Private cMarcaDesc		:= Space(60)
	Private cCiclo			:= Space(05)
	Private cCicloDesc		:= Space(60)
	Private cGpProd			:= Space(05)
	Private cGpProdDesc		:= Space(60)
	Private cSGrupo			:= Space(05)
	Private cSGrupoDesc		:= Space(60)
	Private cColecao		:= Space(05)
	Private cColecaoDesc	:= Space(60)
	Private cSubCol			:= Space(05)
	Private cSubColDesc		:= Space(60)
	Private cTpProd			:= Space(05)
	Private cTpProdDesc		:= Space(60)
	Private cCatProd		:= Space(05)
	Private cCatProdDesc	:= Space(60)
	Private cOcasiao		:= Space(200)
	Private cOcasiaoDesc	:= Space(60)
	Private cTecido			:= Space(20)
	Private cArm			:= Space(02)
//	Private cRastro			:= 2
	Private cRastro			:= "SubLote"
//	Private cEnd			:= 1
	Private cEnd			:= "Sim"
	Private cColuna			:= Space(03)
	Private cColunx			:= Space(03)
	Private cPosIPI			:= Space(10)
	Private nPeso			:= 0
	Private dDtCad			:= Date()
	Private cWEB			:= 2
	Private cTempo			:= 0					//Daniel 21/03/2018		//Private cTempo	:= "0"
	Private cTempRef		:= 0
	Private cTrib 			:= " "
	Private cDescriPTB		:= ""
	Private cDescriENG		:= ""
	Private cDescriSPA		:= ""
	Private cObsProd		:= ""
	Private Foto			:= "LGRL01.bmp"
	Private cCodBarras		:= 0

	Static oDlg

	If _tp = 1 .or. _tp = 2 .or. _tp = 4
		SetKey( VK_F11, { || hCompo() } )
		SetKey( VK_F12, { || hLavage()  } )
	EndIf

	If _tp = 1 .or. _tp = 2
		// TO DO  INICIO RETIRADO TRAVA DE OPCIONAIS CHAMADO 14553 GEYSON ALBANO 17/09/2020
		/*
		DbSelectArea("ZAU")
		DbSetOrder(1) // ZAU_FILIAL+ZAU_CODREF+ZAU_GRUPO+ZAU_COR+ZAU_ITEM+ZAU_PRODUT     
		DbSeek(xfilial("ZAU")+SB4->B4_COD)

		If Found()
			Alert("Este produto possui cadastro de Opcionais e n�o pode ser alterado por esta rotina!")
			Return()
		EndIf
		*/ 
		// TO DO  FIM RETIRADO TRAVA DE OPCIONAIS CHAMADO 14553 GEYSON ALBANO 17/09/2020

		If !(__cuserid) $ GETMV("HP_USERGRA") .AND. (_tp = 1 .or. _tp = 2 )
			Alert("Apenas o setor de produtos, pode realizar altera��es na grade!!")
			return()
		EndIf

	EndIf
	If _tp = 7 .AND. !SB4->B4_TIPO $ "PF|PL|KT|ME|PI"
		Alert("Este produto n�o possui C�digo de Barras!")
		Return()
	EndIf

	If _tp = 2 .or. _tp = 4 .or. _tp = 7 .or. _tp = 8 .or. _tp = 9 .or. _tp = 10 .or. _tp = 11 .or. _tp = 12 .or. _tp = 13 .or. _tp = 14 .or. _tp = 15 .or. _tp = 16
		cTipo			:= SB4->B4_TIPO
		cTipoDesc		:= POSICIONE("SX5",1,xFilial("SX5")+'02'+cTipo,"X5_DESCRI")
//		cClass			:= SB4->B4_YCLASPR
//		cClassDesc		:= POSICIONE("ZAE",1,xFilial("ZAE")+cClass,"ZAE_DESCRI")
		cOrigem			:= If(Alltrim(SB4->B4_YORIGEM)<>"",SB4->B4_YORIGEM,"0")
		cOrigemDesc		:= POSICIONE("SX5",1,xFilial("SX5")+'S0'+cOrigem,"X5_DESCRI")
		cGrupo			:= SB4->B4_GRUPO
		cGrupoDesc		:= POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_DESC")
		cCodigo 		:= SB4->B4_COD
		cCodigoDesc		:= SB4->B4_DESC
		cUM				:= SB4->B4_UM
		cMarca			:= SB4->B4_YMARCA
		cMarcaDesc		:= SB4->B4_YNOMARC
		cCiclo			:= SubString(SB4->B4_YCICLO,1,5)
		cCicloDesc		:= SB4->B4_YNCICLO
		cGpProd			:= SubString(SB4->B4_YGPRODU,1,5)
		cGpProdDesc		:= SB4->B4_YNGPCOM
		cSGrupo			:= SB4->B4_YSUBGRP
		cSGrupoDesc		:= SB4->B4_YNSUBGR
		cColecao		:= SB4->B4_YCOLECA
		cColecaoDesc	:= SB4->B4_YNCOLEC
		cSubCol			:= SB4->B4_YSUBCOL
		cSubColDesc		:= SB4->B4_YNSUBCO
		cTpProd			:= SB4->B4_YTPPROD
		cTpProdDesc		:= SB4->B4_YNTPCOM
		cCatProd		:= SB4->B4_YCATEGO
		cCatProdDesc	:= SB4->B4_YNCATEG
		cOcasiao		:= SB4->B4_YOCASIA //SubString(SB4->B4_YOCASIA,1,5)
		If alltrim(cOcasiao) <> ""
			xocasiao := STRTRAN(cOcasiao,"*","")
			If alltrim(xocasiao) = ""
				cOcasiaoDesc	:= ""
			Else
				cOcasiaoDesc	:= POSICIONE("ZAD",1,xFilial("ZAD")+AllTrim(SubStr(xOcasiao,1,5)),"ZAD_DESCRI")
			EndIf
		EndIf
		cTecido			:= SB4->B4_YTECIDO
		cArm 			:= SB4->B4_LOCPAD
		cRastro 		:= If(SB4->B4_RASTRO="N",3,If(SB4->B4_RASTRO="L",1,2))
		cEnd 			:= If(SB4->B4_LOCALIZ="S",1,2)
		cLinha 			:= SB4->B4_LINHA
		cColuna 		:= SB4->B4_COLUNA
		nPeso 			:= SB4->B4_PESO
		cPosIPI 		:= SB4->B4_POSIPI
		dDtCad 			:= SB4->B4_YDATCAD
		cWeb 			:= If(SB4->B4_DWEB="S",1,2)
		cTempRef		:= SB4->B4_YTEMPO
		cTrib			:= SB4->B4_SITTRIB
		cDescriPTB 		:= SB4->B4_YDESCPT
		cDescriSPA 		:= SB4->B4_YDESCES
		cDescriENG 		:= SB4->B4_YDESCIE
		cObsProd		:= SB4->B4_YOBS
	EndIf

	If _tp == 1
		cCabecalho := "Inclus�o no Cadastro de Grades"
	ElseIf _tp == 2
		cCabecalho := "Altera��o do Cadastro de Grades"
	ElseIf _tp == 4
		cCabecalho := "Visualizar Cadastro de Grades"
	ElseIf _tp == 7
		cCabecalho := "Gera��o de codigo de Barras"
	ElseIf _tp == 8
		cCabecalho := "Inclus�o de Cadastro de Mix para OP"
	ElseIf _tp == 9
		cCabecalho := "Altera��o de Cadastro de Mix para OP"
	ElseIf _tp == 10
		cCabecalho := "Ajuste de Status"
	ElseIf _tp == 11
		cCabecalho := "Inclus�o de Ficha T�cnica"
	ElseIf _tp == 12
		cCabecalho := "Altera��o de Ficha T�cnica"
	ElseIf _tp == 13
		cCabecalho := "Visualiza��o de Roteiro"
	ElseIf _tp == 14
		cCabecalho := "Cadastro de Tempos"
	ElseIf _tp == 15
		cCabecalho := "Inclus�o de Opcionais"
	ElseIf _tp == 16
		cCabecalho := "Altera��o de Opcionais"
	EndIf

	DEFINE MSDIALOG oDlg TITLE cCabecalho FROM 000, 000  TO 600, 1250 COLORS 0, 16777215 PIXEL

	@ 004, 004 FOLDER oFolder1 SIZE 620, 181 OF oDlg ITEMS "Cadastro","Outros" COLORS 0, 16777215 PIXEL

	//FOLDER 01
	nLin	:= 004
	If _tp == 1
		@ nLin	, 005 SAY oSay01 PROMPT "Tipo" 							SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oTipo VAR cTipo			 					SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. GatTela(1,_tp) PICTURE "@!" COLORS 0, 16777215 F3 "02" 	PIXEL
		@ nLin	, 045 SAY oSay02 PROMPT "Desc. do Tipo" 				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oTipoDesc VAR cTipoDesc 					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
	Else
		GatTela(0,_tp)
	EndIf

	//FOLDER 02
	nLin	:= 004
	@ nLin	, 005 SAY oSay40 PROMPT "Foto"							SIZE 055, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 005 BITMAP oFoto	 									SIZE 112, 116 OF oFolder1:aDialogs[2] PIXEL
	@ nLin	, 130 SAY oSay41 PROMPT "Descri��o Portugu�s"			SIZE 055, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 130 GET oDescriPTB VAR cDescriPTB			MULTILINE 	SIZE 235, 050 OF oFolder1:aDialogs[2] COLORS 0, 16777215 HSCROLL 		PIXEL
	@ nLin	, 378 SAY oSay42 PROMPT "Descri��o Espanhol"			SIZE 055, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 378 GET oDescriSPA VAR cDescriSPA			MULTILINE 	SIZE 235, 050 OF oFolder1:aDialogs[2] COLORS 0, 16777215 HSCROLL 		PIXEL

	nLin	:= nLin+65
	@ nLin	, 130 SAY oSay43 PROMPT "Descri��o Ingl�s"				SIZE 055, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 130 GET oDescriENG VAR cDescriENG			MULTILINE 	SIZE 235, 050 OF oFolder1:aDialogs[2] COLORS 0, 16777215 HSCROLL 		PIXEL
	@ nLin	, 378 SAY oSay44 PROMPT "Observa�oes do Produto"		SIZE 065, 007 OF oFolder1:aDialogs[2] COLORS 0, 16777215 PIXEL
	@ nLin+8, 378 GET oObsProd 	VAR cObsProd			MULTILINE 	SIZE 235, 050 OF oFolder1:aDialogs[2] COLORS 0, 16777215 HSCROLL 		PIXEL

// Rotinas para cria��o dos acols para v�rias op��es do menu
	If _tp = 7
		fMSCBar(_tp, cColuna)
	ElseIf _tp = 8 .or. _tp = 9
		fMSMix(_tp, cColuna)
	ElseIf _tp = 10
		fMSSta(_tp, cColuna)
	ElseIf _tp = 11 .or. _tp = 12
		fMSFicha(_tp, cColuna)
	ElseIf _tp = 13
		fMSFich1(_tp, cColuna)
	ElseIf _tp = 14
		fMSFich2(_tp, cColuna)
	ElseIf _tp = 15 .or. _tp = 16
		fMSOPC(_tp, cColuna)
	Else
		fMSGrade(_tp, cColuna)
	EndIf

	If _tp = 1 .or. _tp = 2 .or. _tp = 4
		@ 279, 005 SAY oSay45 PROMPT "F11 - Composi��o"						SIZE 100, 008 OF oDlg COLORS 0, 16777215 PIXEL
		@ 287, 005 SAY oSay46 PROMPT "F12 - Procedimentos de Lavagem"		SIZE 100, 008 OF oDlg COLORS 0, 16777215 PIXEL

	ElseIf _tp = 14 .or. _tp = 13
		/*
		@ 283, 005 SAY oSay38 PROMPT "Tempo Estimado de Produ��o"			SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 280, 085 MSGET oTempo VAR cTempo									SIZE 055, 010 OF oDlg VALID !VAZIO() PICTURE "9999.99" COLORS 0, 16777215 PIXEL
	*/
		@ 283, 005 BUTTON oButton1 PROMPT "Rep. Tempo" 									SIZE 040, 012 ACTION ReplTmp() OF oDlg PIXEL

	EndIf
	If _tp = 1 .or. _tp = 2
		@ 282, 450 BUTTON oButton3 PROMPT "Replicar"						SIZE 040, 012 ACTION ReplyLn(_tp) OF oDlg PIXEL
	ElseIf _tp = 7
		@ 282, 450 BUTTON oButton3 PROMPT "Gera C.Barras"					SIZE 040, 012 ACTION CriacBar(_tp) OF oDlg PIXEL
	EndIf
	@ 282, 500 BUTTON oButton1 PROMPT "Ok" 									SIZE 040, 012 ACTION Save(_tp) OF oDlg PIXEL

//	@ 282, 550 BUTTON oButton2 PROMPT "Cancelar" 							SIZE 040, 012 ACTION Close(oDlg) OF oDlg PIXEL
	@ 282, 550 BUTTON oButton2 PROMPT "Cancelar" 							SIZE 040, 012 ACTION (IIf(ValCodB(),(oDlg:End(),oDlg:End()),))  OF oDlg PIXEL


	ACTIVATE MSDIALOG oDlg CENTERED

	If _tp = 1 .or. _tp = 2 .or. _tp = 4
		SET Key VK_F11 To
		SET Key VK_F12 To
	EndIf

Return

Static Function ValCodB()
// Fun��o para verificar se foi gerado Cod. de barras.
// Geyson Albano 09/08/21
	Local lCodOk := .T.

	If lCodBar
		MessageBox("Foram gerados c�d de barras, favor sair com o OK para que as altera��es sejam salvas","ValCodB",16)
		lCodOk := .F.
	EndIf

Return lCodOk

Static Function Gatilhos(cCodGat)
// Rotina para preenchimento dos campos de descri��o
	If cCodGat==2
		cOrigemDesc		:= POSICIONE("SX5",1,xFilial("SX5")+'S0'+cOrigem,"X5_DESCRI")
		If Alltrim(cOrigemDesc)==""
			oOrigem:SetFocus()
		EndIf
	ElseIf cCodGat==3
		cMarcaDesc		:= POSICIONE("ZAI",1,xFilial("ZAI")+cMarca,"ZAI_MARCA")
		If Alltrim(cMarcaDesc)==""
			oMarca:SetFocus()
		EndIf
	ElseIf cCodGat==4
		cCicloDesc		:= POSICIONE("ZAJ",1,xFilial("ZAJ")+cCiclo,"ZAJ_CICLO")
		If Alltrim(cCicloDesc)==""
			oCiclo:SetFocus()
		EndIf
	ElseIf cCodGat==5
		cGpProdDesc		:= POSICIONE("ZAF",1,xFilial("ZAF")+cGpProd,"ZAF_DESCRI")
		If Alltrim(cGpProdDesc)==""
			oGpProd:SetFocus()
		EndIf
	ElseIf cCodGat==6
		cSGrupoDesc		:= POSICIONE("ZAG",1,xFilial("ZAG")+cSGrupo,"ZAG_DESCRI")
		If Alltrim(cSGrupoDesc)==""
			oSGrupo:SetFocus()
		EndIf
	ElseIf cCodGat==7
		cColecaoDesc	:= POSICIONE("ZAA",1,xFilial("ZAA")+cColecao,"ZAA_DESCRI")
		If Alltrim(cColecaoDesc)==""
			oColecao:SetFocus()
		EndIf
	ElseIf cCodGat==8
		cSubColDesc		:= POSICIONE("ZAH",1,xFilial("ZAH")+cSubCol,"ZAH_DESCRI")
		If Alltrim(cSubColDesc)==""
			oSubCol:SetFocus()
		EndIf
	ElseIf cCodGat==9
		cTpProdDesc		:= POSICIONE("ZAB",1,xFilial("ZAB")+cTpProd,"ZAB_DESCRI")
		If Alltrim(cTpProdDesc)==""
			oTpProd:SetFocus()
		EndIf
	ElseIf cCodGat==10
		cCatProdDesc	:= POSICIONE("ZAC",1,xFilial("ZAC")+cCatProd,"ZAC_DESCRI")
		If Alltrim(cCatProdDesc)==""
			oCatProd:SetFocus()
		EndIf
	ElseIf cCodGat==11
		If Alltrim(cOcasiao) <> ""
			xocasiao 	:= STRTRAN(cOcasiao,"*","")
			If alltrim(xocasiao) = ""
				cOcasiaoDesc	:= ""
			Else
				cOcasiaoDesc	:= POSICIONE("ZAD",1,xFilial("ZAD")+SubStr(xOcasiao,1,5),"ZAD_DESCRI")
			EndIf
//		If Alltrim(cOcasiaoDesc)=="" 
//			oOcasiao:SetFocus()
//		End
		EndIf
	ElseIf cCodGat==12
		If !EXISTCPO("NNR",cArm)
			oArm:SetFocus()
		EndIf
	ElseIf cCodGat==13
		cTipo		:= Upper(cTipo)
		cTipoDesc	:= POSICIONE("SX5",1,xFilial("SX5")+'02'+cTipo,"X5_DESCRI")
		If Alltrim(cTipoDesc)==""
			oTipo:SetFocus()
		EndIf
	EndIf

Return

Static Function GatTela(cCodGat,_tp)
// Rotina para tratamento de habilitar campos para produtos do tipo PF

	If cCodGat==1
		//TODO Valida��o para inclus�o de servi�o {Weskley Silva 25/06/2018}
		If cTipo == "SV" .AND. !(__cUserID $ GETMV("HP_INCPROD"))
			MsgAlert("Usuario sem permiss�o")
			return
		EndIf

		cTipoDesc	:= POSICIONE("SX5",1,xFilial("SX5")+'02'+cTipo,"X5_DESCRI")
		If Alltrim(cTipoDesc)==""
			oTipo:SetFocus()
		Else
			cArm 		:= If(cTipo$"KT|PF|PL|ME","E1",If(cTipo$"MP|MB","A1",If(cTipo$"PI|MI","AP","  ")))
			cUM			:= If(cTipo$GetMv("HP_TPPROUN"),"UN",Space(02))
			cWEB		:= If(cTipo$"KT|ME|PF|PL",1,2)
		EndIf
	Else
		@ nLin	, 005 SAY oSay01 PROMPT "Tipo" 								SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oTipo VAR cTipo 								SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(13) COLORS 0, 16777215 F3 "02"  PIXEL
	EndIf
	If cTipo $ "MP-PF-ME-KT-MB-PL"
		//cEnd := 1
		cEnd := "Sim"
	Else
		//cEnd := 2
		cEnd := "N�o"
	EndIf

	If cTipo $ "PF-PL-ME-KT"
		//cRastro := 1
		cRastro := "SubLote"
	Else
		//cRastro := 3
		cRastro := "Nao Utiliza"
	EndIf


	If cTipo $ "PF|KT|ME|PL"
		nLin	:= 004
		@ nLin	, 045 SAY oSay02 PROMPT "Desc. do Tipo" 				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oTipoDesc VAR cTipoDesc 					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay03 PROMPT "Origem Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oOrigem VAR cOrigem 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(2) COLORS 0, 16777215 F3 "S0" 	PIXEL
		@ nLin	, 252 SAY oSay04 PROMPT "Desc. Origem Prod."			SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oOrigemDesc VAR cOrigemDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay05 PROMPT "Grupo" 						SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oGrupo VAR cGrupo							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. VldCod() PICTURE "@!" COLORS 0, 16777215 F3 "SBM-02" 	PIXEL
		@ nLin	, 459 SAY oSay06 PROMPT "Desc. Grupo"					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oGrupoDesc VAR cGrupoDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL

		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay07 PROMPT "C�digo" 						SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		If _tp = 2 .OR. _tp = 4
			@ nLin+8, 005 MSGET oCodigo VAR cCodigo 					SIZE 050, 010 OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 READONLY 		PIXEL
		Else
			@ nLin+8, 005 MSGET oCodigo VAR cCodigo 					SIZE 050, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. VldCod2(cCodigo) PICTURE "@!" COLORS 0, 16777215 PIXEL
		EndIf
		@ nLin	, 060 SAY oSay08 PROMPT "Descri��o"						SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
//	    If _tp = 2 .OR. _tp = 4
//	       @ nLin+8, 060 MSGET oCodigoDesc VAR cCodigoDesc				SIZE 300, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
//	    Else
		@ nLin+8, 060 MSGET oCodigoDesc VAR cCodigoDesc					SIZE 300, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() PICTURE "@!" COLORS 0, 16777215 PIXEL
//	    End
		@ nLin	, 372 SAY oSay09 PROMPT "Unid.Med."						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		//@ nLin+8, 372 MSGET oUM VAR cUM 								SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() COLORS 0, 16777215 F3 "SAH" 		PIXEL
		@ nLin+8, 372 MSGET oUM VAR cUM             	                SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. ExistCpo("SX5", "62" + cUM) COLORS 0, 16777215 F3 "62" PIXEL
		@ nLin	, 419 SAY oSay10 PROMPT "Marca Produto"					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oMarca VAR cMarca							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(3) COLORS 0, 16777215 F3 "ZAI" 	PIXEL
		@ nLin	, 459 SAY oSay11 PROMPT "Desc. Marca Prod."				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oMarcaDesc VAR cMarcaDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL

		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay12 PROMPT "Ciclo" 						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oCiclo VAR cCiclo 							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(4) COLORS 0, 16777215 F3 "ZAJ" 	PIXEL
		@ nLin	, 045 SAY oSay13 PROMPT "Desc. Ciclo"					SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oCicloDesc VAR cCicloDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay14 PROMPT "Grupo Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oGpProd VAR cGpProd							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(5) COLORS 0, 16777215 F3 "ZAF" 	PIXEL
		@ nLin	, 252 SAY oSay15 PROMPT "Desc. Grupo Prod."				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oGpProdDesc VAR cGpProdDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay16 PROMPT "Sub Grupo"	 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oSGrupo VAR cSGrupo 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(6) COLORS 0, 16777215 F3 "ZAG" 	PIXEL
		@ nLin	, 459 SAY oSay17 PROMPT "Desc. Sub Grupo"				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oSGrupoDesc VAR cSGrupoDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL

		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay18 PROMPT "Cole��o" 						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oColecao VAR cColecao 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(7) COLORS 0, 16777215 F3 "ZAA" 	PIXEL
		@ nLin	, 045 SAY oSay19 PROMPT "Desc. Cole��o"					SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oColecaoDesc VAR cColecaoDesc				SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay20 PROMPT "Sub Cole��o" 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oSubCol VAR cSubCol							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(8) COLORS 0, 16777215 F3 "ZAH" 	PIXEL
		@ nLin	, 252 SAY oSay21 PROMPT "Desc. Sub Cole��o"				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oSubColDesc VAR cSubColDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay22 PROMPT "Tipo Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oTpProd VAR cTpProd							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(9) COLORS 0, 16777215 F3 "ZAB" 	PIXEL
		@ nLin	, 459 SAY oSay23 PROMPT "Desc. Tipo Prod."				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oTpProdDesc VAR cTpProdDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL

		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay24 PROMPT "Cat.Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oCatProd VAR cCatProd 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(10) COLORS 0, 16777215 F3 "ZAC" 	PIXEL
		@ nLin	, 045 SAY oSay25 PROMPT "Desc. Cat.Prod"				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oCatProdDesc VAR cCatProdDesc				SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay26 PROMPT "Ocasi�o" 						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oOcasiao VAR cOcasiao 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID Gatilhos(11) COLORS 0, 16777215 F3 "ZAD" 	PIXEL
		@ nLin	, 252 SAY oSay27 PROMPT "Desc. Ocasi�o"					SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oOcasiaoDesc VAR cOcasiaoDesc				SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay28 PROMPT "Tecido" 						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oTecido VAR cTecido 						SIZE 195, 010 OF oFolder1:aDialogs[1] VALID PICTURE "@!" COLORS 0, 16777215 PIXEL

		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay30 PROMPT "Armaz�m" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oArm VAR cArm 								SIZE 055, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(12) PICTURE "@!" COLORS 0, 16777215 F3 "NNR" 		PIXEL
		@ nLin	, 075 SAY oSay31 PROMPT "Lote/SubLote"					SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 075 MSCOMBOBOX oRastro VAR cRastro		ITEMS {"Lote","SubLote","Nao Utiliza"}		SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin	, 145 SAY oSay32 PROMPT "Endere�o" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 145 MSCOMBOBOX oEnd VAR cEnd 				ITEMS {"Sim","N�o"}							SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin	, 215 SAY oSay33 PROMPT "Linha" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 215 MSGET oLinha VAR cLinha 							SIZE 055, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY			PIXEL
		@ nLin	, 285 SAY oSay34 PROMPT "Coluna" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		If _tp == 1
			@ nLin+8, 285 MSGET oColuna VAR cColuna							SIZE 055, 010 VALID !VAZIO() .AND. U_valacols(_tp, cColuna) OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 F3 "SX5-Z3" PIXEL
		Else
			@ nLin+8, 285 MSGET oColuna VAR cColuna							SIZE 055, 010 VALID !VAZIO() .AND. U_valacols(_tp, cColuna) WHEN .F. OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 PIXEL
		EndIf
		If _tp != 14
			@ nLin	, 355 SAY oSay35 PROMPT "Peso Liquido"					SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
			@ nLin+8, 355 MSGET oPeso VAR nPeso								SIZE 055, 010 PICTURE "@E 999,999.9999" OF oFolder1:aDialogs[1] VALID !VAZIO() COLORS 0, 16777215 PIXEL
		Else
			@ nLin	, 355 SAY oSay35 PROMPT "Tempo Padr�o"					SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
			@ nLin+8, 355 MSGET oPeso VAR cTempRef							SIZE 055, 010 PICTURE "99:99" OF oFolder1:aDialogs[1] VALID !VAZIO() COLORS 0, 16777215 PIXEL
		EndIf

		@ nLin	, 425 SAY oSay36 PROMPT "Pos. IPI/NCM"					SIZE 040, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 425 MSGET oPosIPI VAR cPosIPI	 						SIZE 050, 010 PICTURE "@R 9999.99.99" OF oFolder1:aDialogs[1] VALID U_ExistCod(1,cPosIPI) COLORS 0, 16777215 F3 "SYD" PIXEL
		@ nLin	, 490 SAY oSay37 PROMPT "Data de cadastro"				SIZE 055, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 490 MSGET oDtCad VAR dDtCad 							SIZE 055, 010 PICTURE "@D 99/99/9999" OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin	, 559 SAY oSay39 PROMPT "Disponivel na WEB"				SIZE 055, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 559 MSCOMBOBOX oWeb VAR cWeb				ITEMS {"Sim","N�o"}							SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL

	Else

		nLin	:= 004
		@ nLin	, 045 SAY oSay02 PROMPT "Desc. do Tipo" 				SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 045 MSGET oTipoDesc VAR cTipoDesc 					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 212 SAY oSay03 PROMPT "Origem Prod." 					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 212 MSGET oOrigem VAR cOrigem 						SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(2) COLORS 0, 16777215 F3 "S0" 	PIXEL
		@ nLin	, 252 SAY oSay04 PROMPT "Desc. Origem Prod."			SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 252 MSGET oOrigemDesc VAR cOrigemDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
		@ nLin	, 419 SAY oSay05 PROMPT "Grupo" 						SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 419 MSGET oGrupo VAR cGrupo							SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. VldCod() PICTURE "@!" COLORS 0, 16777215 F3 "SBM-02" 	PIXEL
		@ nLin	, 459 SAY oSay06 PROMPT "Desc. Grupo"					SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 459 MSGET oGrupoDesc VAR cGrupoDesc					SIZE 155, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL

		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay07 PROMPT "C�digo" 						SIZE 025, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		If _tp = 2 .OR. _tp = 4
			@ nLin+8, 005 MSGET oCodigo VAR cCodigo 					SIZE 050, 010 OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 READONLY 		PIXEL
		Else
			@ nLin+8, 005 MSGET oCodigo VAR cCodigo 					SIZE 050, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() PICTURE "@!"  COLORS 0, 16777215 PIXEL
		EndIf
		@ nLin	, 060 SAY oSay08 PROMPT "Descri��o"						SIZE 050, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
//	    If _tp = 2 .OR. _tp = 4
//	       @ nLin+8, 060 MSGET oCodigoDesc VAR cCodigoDesc				SIZE 300, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY 		PIXEL
//	    Else
		@ nLin+8, 060 MSGET oCodigoDesc VAR cCodigoDesc					SIZE 300, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() PICTURE "@!" COLORS 0, 16777215 PIXEL
//	    End
		@ nLin	, 372 SAY oSay09 PROMPT "Unid.Med."						SIZE 035, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 372 MSGET oUM VAR cUM 								SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() COLORS 0, 16777215 F3 "SAH" 		PIXEL
		@ nLin+8, 372 MSGET oUM VAR cUM                                 SIZE 035, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. ExistCpo("SX5", "62" + cUM) COLORS 0, 16777215 F3 "62" PIXEL
		nLin	:= nLin+27
		@ nLin	, 005 SAY oSay30 PROMPT "Armaz�m" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 005 MSGET oArm VAR cArm 								SIZE 055, 010 OF oFolder1:aDialogs[1] VALID !VAZIO() .AND. Gatilhos(12) PICTURE "@!" COLORS 0, 16777215 F3 "NNR" PIXEL
		@ nLin	, 075 SAY oSay31 PROMPT "Lote/SubLote"					SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 075 MSCOMBOBOX oRastro VAR cRastro		ITEMS {"Lote","SubLote","Nao Utiliza"}		SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		If _tp = 2 .OR. _tp = 4
			If SB4->B4_LOCALIZ != "S"  //VALIDACAO PARA PEGAR O CONTEUDO DE MEMORIA NA HORA DA VISUALIZA��O/ALTERA��O
				cEnd := 2
			Else
				cEnd := 1
			EndIf
			@ nLin	, 145 SAY oSay32 PROMPT "Endere�o" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
			@ nLin+8, 145 MSCOMBOBOX oEnd VAR cEnd 				ITEMS {"Sim","N�o"}		SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		Else
			@ nLin	, 145 SAY oSay32 PROMPT "Endere�o" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
			@ nLin+8, 145 MSCOMBOBOX oEnd VAR cEnd 				ITEMS {"Sim","N�o"}		SIZE 055, 013 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		EndIf
		@ nLin	, 215 SAY oSay33 PROMPT "Linha" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 215 MSGET oLinha VAR cLinha 							SIZE 055, 010 OF oFolder1:aDialogs[1] COLORS 0, 16777215 READONLY			PIXEL
		@ nLin	, 285 SAY oSay34 PROMPT "Coluna" 						SIZE 045, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		If _tp == 1
			@ nLin+8, 285 MSGET oColuna VAR cColuna		 				SIZE 055, 010 VALID !VAZIO() .AND. U_valacols(_tp, cColuna) OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 F3 "SX5-Z3" PIXEL
		Else
			//@ nLin+8, 285 MSGET oColuna VAR cColuna		 				SIZE 055, 010 VALID !VAZIO() .AND. U_valacols(_tp, cColuna) OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215 F3 "SX5-Z3" READONLY PIXEL
			@ nLin+8, 285 MSGET oColuna VAR cColuna							SIZE 055, 010 VALID !VAZIO() .AND. U_valacols(_tp, cColuna) WHEN .F. OF oFolder1:aDialogs[1] PICTURE "@!" COLORS 0, 16777215  PIXEL

		EndIf
		@ nLin	, 425 SAY oSay36 PROMPT "Pos. IPI/NCM"					SIZE 040, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 425 MSGET oPosIPI VAR cPosIPI	 						SIZE 050, 010 PICTURE "@R 9999.99.99" OF oFolder1:aDialogs[1] VALID U_ExistCod(1,cPosIPI) COLORS 0, 16777215 F3 "SYD" PIXEL
		@ nLin	, 490 SAY oSay37 PROMPT "Data de cadastro"				SIZE 055, 007 OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL
		@ nLin+8, 490 MSGET oDtCad VAR dDtCad 							SIZE 055, 010 PICTURE "@D 99/99/9999" OF oFolder1:aDialogs[1] COLORS 0, 16777215 PIXEL

	EndIf

	oDlg:refresh()

Return

//Carrega a grade da tabela
//------------------------------------------------
Static Function fMSGrade(_tp, cColuna)
//------------------------------------------------
// Monta acols para cadastro da Grade de Produtos
// Tabelas envolvidas: SZD

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZD_COR","ZD_DESCRIC","ZD_COMPOS","ZD_DESCCP2","ZD_DESCLAV","ZD_LAVAGE"} //"ZD_DTLIB","ZD_SITUAC",
	Local aAlterFields := {"ZD_COR","ZD_DESCRIC","ZD_COMPOS","ZD_DESCCP2","ZD_LAVAGE"}
	Static oMSGrade

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZD_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf
	If SX3->(DbSeek("ZD_DESCRIC"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_DESCRIC","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf
	If SX3->(DbSeek("ZD_COMPOS"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_COMPOS","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf
	If SX3->(DbSeek("ZD_DESCCP2"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_DESCCP2","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If alltrim(cColuna) <> ""
		// Busca os tamanhos dispon�veis selecionados na tabela SBV

		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		cqryc := "SELECT COUNT(*)+1 AS QTD FROM " +RetSQLName("SBV") + "(NOLOCK) WHERE BV_XCAMPO <> '' AND D_E_L_E_T_ ='' AND BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' "

		If SELECT("TMPSBVC") > 0
			TMPSBVC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cqryc),"TMPSBVC",.T.,.T.)

		nzd:= TMPSBVC->QTD

		While TMPSBV->(!EOF())
			If Empty(TMPSBV->BV_XCAMPO)
				DbSelectArea("SBV")
				DbSetOrder(1)
				If DbSeek(xfilial("SBV")+TMPSBV->BV_TABELA+TMPSBV->BV_CHAVE)
					RecLock("SBV",.F.)
					SBV->BV_XCAMPO := "ZD_TAM"+Padl(cvaltochar(nzd),3,"0")
					MsUnLock()
					nzd ++
				EndIf
			EndIf
			TMPSBV->(DbSkip())
		EndDo

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")
			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY BV_CHAVE,R_E_C_N_O_"

			cQuery := ChangeQuery(cQuery)

			If SELECT("TMPSBV") > 0
				TMPSBV->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		EndIf

		//Conta total e registros
		Count To nRecTMPSBV
		If nRecTMPSBV == 0
			oColuna:SetFocus()
		EndIf

		DbSelectArea("TMPSBV")
		DbGoTop()

		While !EOF() .AND. TMPSBV->BV_TABELA == cColuna
			DbSelectArea("SX3")
			// Habilita as colunas com os tamanhos no Acols utilizando a tabela SZD at� o tamanho m�ximo de 200
			_campo := "ZD_TAM"+StrZero(len(aFields)-5,3)
			Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
			Aadd(aAlterFields,_campo)

			SX3->(DbSetOrder(2))
			If SX3->(DbSeek(_campo))
				_campo3 :="M->"+"ZD_TAM"+StrZero(len(aFields)-5,3)

				//Aadd(aHeaderEx, {alltrim(TMPSBV->BV_DESCRI)+Space(4),SX3->X3_CAMPO,"@!",1,0,"",SX3->X3_USADO,"C","","R","",""})
				//Aadd(aHeaderEx, {alltrim(TMPSBV->BV_DESCRI)+Space(4),Alltrim(SX3->X3_CAMPO),"@!",1,0,"U_ExistCod(6,M->ZD_COR)",,SX3->X3_USADO,"C","","R","",""})
				Aadd(aHeaderEx, {Alltrim(TMPSBV->BV_DESCRI)+Space(4),Alltrim(TMPSBV->BV_XCAMPO),"@!",1,0,"U_ExistCod(6,M->ZD_COR)",,SX3->X3_USADO,"C","","R","",""})

			EndIf

			DbSelectArea("TMPSBV")
			DbSkip()

		EndDo
		DbClosearea()
	Else
		// Caso n�o tenha um tamanho cadastrado, habilita um tamanho �nico
		Aadd(aFields,"ZD_TAM001")
		Aadd(aAlterFields,"ZD_TAM001")

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		If SX3->(DbSeek("ZD_TAM001"))
			Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,"@!",1,0,"",SX3->X3_USADO,"C","","R","",""})
			//Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,"@!",1,0,"U_ExistCod(6,M->ZD_TAM001)",SX3->X3_USADO,"C","","R","",""})
		EndIf
	EndIf
	If SX3->(DbSeek("ZD_DESCLAV"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo())+Space(4),"ZD_DESCLAV",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZD_LAVAGE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo())+Space(4),"ZD_LAVAGE",SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	// Define field values
	If _tp = 1
		For nX := 1 to Len(aFields)
			If nX == 1
				Aadd(aFieldFill, Space(3))
			ElseIf nX == 2 .or. nX == 3
				Aadd(aFieldFill, Space(20))
			ElseIf nX == Len(aFields)
				Aadd(aFieldFill, Space(0))
			Else
				Aadd(aFieldFill, Space(1))
			EndIf
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+SubString(SB4->B4_COD,1,15))

		While !EOF() .AND. SubString(SZD->ZD_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)

			Aadd(aFieldFill, SZD->ZD_COR)
			Aadd(aFieldFill, SZD->ZD_DESCRIC)
			Aadd(aFieldFill, SZD->ZD_COMPOS)
			Aadd(aFieldFill, SZD->ZD_DESCCP2)
			For nX := 6 to (Len(aFields)-1)

				Aadd(aFieldFill,"")
			Next
			Aadd(aFieldFill, SZD->ZD_DESCLAV)
			Aadd(aFieldFill, SZD->ZD_LAVAGE)
			For nX := 6 to (Len(aFields)-1)
				_campo := "SZD->ZD_TAM"+StrZero(nX-5,3)
				_cfor := "ZD_TAM"+StrZero(nX-5,3)
				npos:= Ascan(aheaderex,{|x| x[2] == Substring(_campo,6,(Len(_campo)))})

				If npos > 0
					aFieldFill[nPos]:= &_campo
				EndIf
			Next

			Aadd(aFieldFill, .F.)
			Aadd(aColsEx, aFieldFill)
			aFieldFill :={}
			DbSkip()
		EndDo
	EndIf

	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return


/*
�����������������������������������������������������������������������������
?????????????????????????????????????????????????????????????????????????????
??������������������������������������������������������������������������???
???Programa  ? hLavage  ?Autor  ?Daniel R. Melo      ? Data ? 27/02/2017  ???
??������������������������������������������������������������������������???
???Desc.     ?                                                            ???
???          ?                                                            ???
??������������������������������������������������������������������������???
???Uso       ?                                                            ???
??������������������������������������������������������������������������???
?????????????????????????????????????????????????????????????????????????????
�����������������������������������������������������������������������������
*/
Static Function hLavage()

	Local oBitmap1
	Local oBitmap2
	Local oBitmap3
	Local oBitmap4
	Local oBitmap5
	Local oBitmap6
	Local oBitmap7
	Local oBitmap8
	Local oBitmap9
	Local oBitmap10
	Local oBitmap11
	Local oBitmap12
	Local oBitmap14
	Local oBitmap15
	Local oBitmap16
	Local oBitmap17
	Local oBitmap18
	Local oBitmap19
	Local oBitmap20
	Local oBitmap21
	Local oBitmap22
	Local oBitmap23
	Local oBitmap24
	Local oBitmap25
	Local oBitmap26
	Local oBitmap27
	Local oBitmap28
	Local oBitmap29
	Local oBitmap30
	Local oBitmap31
	Local oBitmap32
	Local oBitmap33
	Local oBitmap34
	Local oBitmap35
	Local oBitmap36
	Local oBitmap37
	Local oBitmap38
	Local oBitmap39
	Local oBitmap41
	Local oBitmap42
	Local oBitmap43
	Local oBitmap44
	Local oBitmap45
	Local oBitmap46
	Local oBitmap47
	Local oBitmap48
	Local oBitmap49
	Local oBitmap50
	Local oBitmap51
	Local oBitmap52
	Local oBitmap53
	Local oBitmap54
	Local oBitmap55

	Local oCheckBo1
	Local oCheckBo2
	Local oCheckBo3
	Local oCheckBo4
	Local oCheckBo5
	Local oCheckBo6
	Local oCheckBo7
	Local oCheckBo8
	Local oCheckBo9
	Local oCheckBo10
	Local oCheckBo11
	Local oCheckBo12
	Local oCheckBo13
	Local oCheckBo14
	Local oCheckBo15
	Local oCheckBo16
	Local oCheckBo17
	Local oCheckBo18
	Local oCheckBo19
	Local oCheckBo20
	Local oCheckBo21
	Local oCheckBo22
	Local oCheckBo23
	Local oCheckBo24
	Local oCheckBo25
	Local oCheckBo26
	Local oCheckBo27
	Local oCheckBo28
	Local oCheckBo29
	Local oCheckBo30
	Local oCheckBo31
	Local oCheckBo32
	Local oCheckBo33
	Local oCheckBo34
	Local oCheckBo35
	Local oCheckBo36
	Local oCheckBo37
	Local oCheckBo38
	Local oCheckBo39
	Local oCheckBo40
	Local oCheckBo41
	Local oCheckBo42
	Local oCheckBo43
	Local oCheckBo44
	Local oCheckBo45
	Local oCheckBo46
	Local oCheckBo47
	Local oCheckBo48
	Local oCheckBo49
	Local oCheckBo50
	Local oCheckBo51
	Local oCheckBo52
	Local oCheckBo53
	Local oCheckBo54
	Local oCheckBo55

	Local oButton1
	Local oButton2
	Local oButton3
	Local nx

	Private lCheckBo1 := .F.
	Private lCheckBo2 := .F.
	Private lCheckBo3 := .F.
	Private lCheckBo4 := .F.
	Private lCheckBo5 := .F.
	Private lCheckBo6 := .F.
	Private lCheckBo7 := .F.
	Private lCheckBo8 := .F.
	Private lCheckBo9 := .F.
	Private lCheckBo10 := .F.
	Private lCheckBo11 := .F.
	Private lCheckBo12 := .F.
	Private lCheckBo13 := .F.
	Private lCheckBo14 := .F.
	Private lCheckBo15 := .F.
	Private lCheckBo16 := .F.
	Private lCheckBo17 := .F.
	Private lCheckBo18 := .F.
	Private lCheckBo19 := .F.
	Private lCheckBo20 := .F.
	Private lCheckBo21 := .F.
	Private lCheckBo22 := .F.
	Private lCheckBo23 := .F.
	Private lCheckBo24 := .F.
	Private lCheckBo25 := .F.
	Private lCheckBo26 := .F.
	Private lCheckBo27 := .F.
	Private lCheckBo28 := .F.
	Private lCheckBo29 := .F.
	Private lCheckBo30 := .F.
	Private lCheckBo31 := .F.
	Private lCheckBo32 := .F.
	Private lCheckBo33 := .F.
	Private lCheckBo34 := .F.
	Private lCheckBo35 := .F.
	Private lCheckBo36 := .F.
	Private lCheckBo37 := .F.
	Private lCheckBo38 := .F.
	Private lCheckBo39 := .F.
	Private lCheckBo40 := .F.
	Private lCheckBo41 := .F.
	Private lCheckBo42 := .F.
	Private lCheckBo43 := .F.
	Private lCheckBo44 := .F.
	Private lCheckBo45 := .F.
	Private lCheckBo46 := .F.
	Private lCheckBo47 := .F.
	Private lCheckBo48 := .F.
	Private lCheckBo49 := .F.
	Private lCheckBo50 := .F.
	Private lCheckBo51 := .F.
	Private lCheckBo52 := .F.
	Private lCheckBo53 := .F.
	Private lCheckBo54 := .F.
	Private lCheckBo55 := .F.

	Private oSay1
	Private oSay2
	Private oSay3
	Private oSay4
	Private oSay5
	Private oSay6
	Private oSay7
	Private oSay8
	Private oSay9
	Private oSay10
	Private oSay11
	Private oSay12
	Private oSay13
	Private oSay14
	Private oSay15
	Private oSay16
	Private oSay17
	Private oSay18
	Private oSay19
	Private oSay20
	Private oSay21
	Private oSay22
	Private oSay23
	Private oSay24
	Private oSay25
	Private oSay26
	Private oSay27
	Private oSay28
	Private oSay29
	Private oSay30
	Private oSay31
	Private oSay32
	Private oSay33
	Private oSay34
	Private oSay35
	Private oSay36
	Private oSay37
	Private oSay38
	Private oSay39
	Private oSay40
	Private oSay41
	Private oSay42
	Private oSay43
	Private oSay44
	Private oSay45
	Private oSay46
	Private oSay47
	Private oSay48
	Private oSay49
	Private oSay50
	Private oSay51
	Private oSay52
	Private oSay53
	Private oSay54
	Private oSay55

	Private MvParCod := Alltrim(oMSGrade:aCols[oMSGrade:nat,len(OMSGrade:aHeader)])+","   // Carrega Nome da Variavel do Get em Questao
	Private MvParTxt := ""

	If MvParCod <> ","
		For nx := 1 to len(MvParCod) step 3
			cAux2 	:= SubStr(MvParCod,nx,2)
			cCheck		:= "lCheckBo"+cValToChar(Val(cAux2))
			&cCheck		:= .T.
		Next
	Else
		MvParCod := ""
	EndIf

	Static oDlgLav

	DEFINE FONT oFont1 NAME "Arial" SIZE 0,12

	DEFINE MSDIALOG oDlgLav TITLE "New Dialog" FROM 000, 000  TO 520, 1300 COLORS 0, 16777215 PIXEL

	nCol := 7
	@ 007, nCol BITMAP oBitmap1  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\01.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap2  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\02.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap3  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\03.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap4  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\04.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap5  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\05.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap6  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\06.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap7  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\07.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap8  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\08.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap9  SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\09.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap10 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\10.jpg" NOBORDER PIXEL
	@ 232, nCol BITMAP oBitmap11 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\11.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo1  VAR lCheckBo1  PROMPT "" SIZE 010, 008 VALID SelCheck(1)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo2  VAR lCheckBo2  PROMPT "" SIZE 010, 008 VALID SelCheck(2)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo3  VAR lCheckBo3  PROMPT "" SIZE 010, 008 VALID SelCheck(3)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo4  VAR lCheckBo4  PROMPT "" SIZE 010, 008 VALID SelCheck(4)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo5  VAR lCheckBo5  PROMPT "" SIZE 010, 008 VALID SelCheck(5)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo6  VAR lCheckBo6  PROMPT "" SIZE 010, 008 VALID SelCheck(6)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo7  VAR lCheckBo7  PROMPT "" SIZE 010, 008 VALID SelCheck(7)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo8  VAR lCheckBo8  PROMPT "" SIZE 010, 008 VALID SelCheck(8)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo9  VAR lCheckBo9  PROMPT "" SIZE 010, 008 VALID SelCheck(9)  OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo10 VAR lCheckBo10 PROMPT "" SIZE 010, 008 VALID SelCheck(10) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 232, nCol CHECKBOX oCheckBo11 VAR lCheckBo11 PROMPT "" SIZE 010, 008 VALID SelCheck(11) OF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay1  PROMPT "LAVAGEM NORMAL" 						   					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay2  PROMPT "TEMP.MAXIMA 95?C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay3  PROMPT "TEMP.MAXIMA 90?C - NORMAL" 			  	  				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay4  PROMPT "TEMP.MAXIMA 90?C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay5  PROMPT "TEMP.MAXIMA 70?C - NORMAL" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay6  PROMPT "TEMP.MAXIMA 60?C - NORMAL" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay7  PROMPT "TEMP.MAXIMA 60?C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay8  PROMPT "TEMP.MAXIMA 50?C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay9  PROMPT "TEMP.MAXIMA 40?C - NORMAL" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay10 PROMPT "TEMP.MAXIMA 40?C - TRATATAMENTO BRANDO" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 232, nCol SAY oSay11 PROMPT "TEMP.MAXIMA 40?C - TRATATAMENTO MUITO BRANDO" 			FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	nCol := nCol+95
	@ 007, nCol BITMAP oBitmap12 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\12.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap13 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\13.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap14 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\14.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap15 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\15.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap16 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\16.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap17 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\17.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap18 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\18.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap19 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\19.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap20 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\20.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap21 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\21.jpg" NOBORDER PIXEL
	@ 232, nCol BITMAP oBitmap22 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\22.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo12 VAR lCheckBo12 PROMPT "" SIZE 010, 008 VALID SelCheck(12) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo13 VAR lCheckBo13 PROMPT "" SIZE 010, 008 VALID SelCheck(13) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo14 VAR lCheckBo14 PROMPT "" SIZE 010, 008 VALID SelCheck(14) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo15 VAR lCheckBo15 PROMPT "" SIZE 010, 008 VALID SelCheck(15) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo16 VAR lCheckBo16 PROMPT "" SIZE 010, 008 VALID SelCheck(16) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo17 VAR lCheckBo17 PROMPT "" SIZE 010, 008 VALID SelCheck(17) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo18 VAR lCheckBo18 PROMPT "" SIZE 010, 008 VALID SelCheck(18) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo19 VAR lCheckBo19 PROMPT "" SIZE 010, 008 VALID SelCheck(19) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo20 VAR lCheckBo20 PROMPT "" SIZE 010, 008 VALID SelCheck(20) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo21 VAR lCheckBo21 PROMPT "" SIZE 010, 008 VALID SelCheck(21) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 232, nCol CHECKBOX oCheckBo22 VAR lCheckBo22 PROMPT "" SIZE 010, 008 VALID SelCheck(22) OF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay12 PROMPT "TEMP.MAXIMA 30?C - PROCESSO SUAVE" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay13 PROMPT "SOMENTE LAVAGEM MANUAL" 									FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay14 PROMPT "NAO LAVAR" 												FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay15 PROMPT "ALVEJAMENTO A BASE DE CLORO" 						   	FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay16 PROMPT "PERMITIDO ALVEJAMENTO COM CLORO DILUIDO"	 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay17 PROMPT "NAO ALVEJAR" 											FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay18 PROMPT "SECAGEM" 												FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay19 PROMPT "SECAGEM NA HORIZONTAL A SOMBRA" 							FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay20 PROMPT "PERMITIDO TAMBOR ROTATIVO COM TEMP.MAXIMA" 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay21 PROMPT "PERMITIDO TAMBOR ROTATIVO COM TEMP.MINIMA"	 			FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 232, nCol SAY oSay22 PROMPT "NAO SECAR EM TAMBOR" 									FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	nCol := nCol+95
	@ 007, nCol BITMAP oBitmap23 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\23.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap24 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\24.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap25 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\25.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap26 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\26.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap27 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\27.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap28 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\28.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap29 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\29.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap30 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\30.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap31 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\31.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap32 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\32.jpg" NOBORDER PIXEL
//    @ 232, nCol BITMAP oBitmap33 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\??.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo23 VAR lCheckBo23 PROMPT "" SIZE 010, 008 VALID SelCheck(23) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo24 VAR lCheckBo24 PROMPT "" SIZE 010, 008 VALID SelCheck(24) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo25 VAR lCheckBo25 PROMPT "" SIZE 010, 008 VALID SelCheck(25) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo26 VAR lCheckBo26 PROMPT "" SIZE 010, 008 VALID SelCheck(26) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo27 VAR lCheckBo27 PROMPT "" SIZE 010, 008 VALID SelCheck(27) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo28 VAR lCheckBo28 PROMPT "" SIZE 010, 008 VALID SelCheck(28) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo29 VAR lCheckBo29 PROMPT "" SIZE 010, 008 VALID SelCheck(29) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo30 VAR lCheckBo30 PROMPT "" SIZE 010, 008 VALID SelCheck(30) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo31 VAR lCheckBo31 PROMPT "" SIZE 010, 008 VALID SelCheck(31) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo32 VAR lCheckBo32 PROMPT "" SIZE 010, 008 VALID SelCheck(32) OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 232, nCol CHECKBOX oCheckBo33 VAR lCheckBo33 PROMPT "" SIZE 010, 008 VALID SelCheck(33) OF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay23 PROMPT "PERMITIDO SECAR NA VERTICAL" 							FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay24 PROMPT "PERMITIDO NA VERTICAL SEM TORCER" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay25 PROMPT "SECAGEM NA HORIZONTAL" 									FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay26 PROMPT "PASSADORIA A FERRO" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay27 PROMPT "CHAPA DA BASE DO FERRO ATE 200?C" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay28 PROMPT "CHAPA DA BASE DO FERRO ATE 150?C" 	 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay29 PROMPT "TEMPERATURA MAXIMA BASE DO FERRO 110?C SEM VAPOR" 		FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay30 PROMPT "NAO PASSAR OU UTILIZAR VAPORIZACAO" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay31 PROMPT "LIMPEZA A SECO" 											FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay32 PROMPT "LIMPEZA A SECO COM TODOS OS SOLVENTES" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 232, nCol SAY oSay33 PROMPT "" 														FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	nCol := nCol+95
	@ 007, nCol BITMAP oBitmap34 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\33.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap35 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\34.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap36 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\35.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap37 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\36.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap38 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\37.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap39 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\38.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap40 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\39.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap41 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\40.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap42 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\41.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap43 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\42.jpg" NOBORDER PIXEL
//    @ 232, nCol BITMAP oBitmap44 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\??.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo34 VAR lCheckBo34 PROMPT "" SIZE 010, 008 VALID SelCheck(34) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo35 VAR lCheckBo35 PROMPT "" SIZE 010, 008 VALID SelCheck(35) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo36 VAR lCheckBo36 PROMPT "" SIZE 010, 008 VALID SelCheck(36) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo37 VAR lCheckBo37 PROMPT "" SIZE 010, 008 VALID SelCheck(37) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo38 VAR lCheckBo38 PROMPT "" SIZE 010, 008 VALID SelCheck(38) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo39 VAR lCheckBo39 PROMPT "" SIZE 010, 008 VALID SelCheck(39) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo40 VAR lCheckBo40 PROMPT "" SIZE 010, 008 VALID SelCheck(40) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo41 VAR lCheckBo41 PROMPT "" SIZE 010, 008 VALID SelCheck(41) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo42 VAR lCheckBo42 PROMPT "" SIZE 010, 008 VALID SelCheck(42) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo43 VAR lCheckBo43 PROMPT "" SIZE 010, 008 VALID SelCheck(43) OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 232, nCol CHECKBOX oCheckBo44 VAR lCheckBo44 PROMPT "" SIZE 010, 008 VALID SelCheck(44) OF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay34 PROMPT "LIMPEZA A SECO COM TRICLOROETILENO" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay35 PROMPT "LIMPEZA A SECO TRICLOROETILENO SEM AGUA/ACAO MECANICA" 	FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay36 PROMPT "LIMPEZA A SECO COM TRIFLUORTRICLOROETANO" 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay37 PROMPT "LIMPEZA A SECO COM TRIFLUORTRICLOROETANO SEM AGUA" 		FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay38 PROMPT "NAO LIMPAR A SECO" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay39 PROMPT "NAO PERMITIDO" 											FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay40 PROMPT "TRATAMENTO BRANDO" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay41 PROMPT "TRATAMENTO MUITO BRANDO" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay42 PROMPT "LAVAGEM A MAO" 											FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay43 PROMPT "SECAGEM EM VARAL" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 232, nCol SAY oSay44 PROMPT "" 														FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	nCol := nCol+95
	@ 007, nCol BITMAP oBitmap45 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\43.jpg" NOBORDER PIXEL
	@ 030, nCol BITMAP oBitmap46 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\44.jpg" NOBORDER PIXEL
	@ 052, nCol BITMAP oBitmap47 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\45.jpg" NOBORDER PIXEL
	@ 075, nCol BITMAP oBitmap48 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\46.jpg" NOBORDER PIXEL
	@ 097, nCol BITMAP oBitmap49 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\47.jpg" NOBORDER PIXEL
	@ 120, nCol BITMAP oBitmap50 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\48.jpg" NOBORDER PIXEL
	@ 142, nCol BITMAP oBitmap51 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\49.jpg" NOBORDER PIXEL
	@ 165, nCol BITMAP oBitmap52 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\50.jpg" NOBORDER PIXEL
	@ 187, nCol BITMAP oBitmap53 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\51.jpg" NOBORDER PIXEL
	@ 210, nCol BITMAP oBitmap54 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\52.jpg" NOBORDER PIXEL
//    @ 232, nCol BITMAP oBitmap55 SIZE 017, 017 OF oDlgLav FILENAME "\system\Imagens\??.jpg" NOBORDER PIXEL
	nCol := nCol+22
	@ 007, nCol CHECKBOX oCheckBo45 VAR lCheckBo45 PROMPT "" SIZE 010, 008 VALID SelCheck(45) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol CHECKBOX oCheckBo46 VAR lCheckBo46 PROMPT "" SIZE 010, 008 VALID SelCheck(46) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol CHECKBOX oCheckBo47 VAR lCheckBo47 PROMPT "" SIZE 010, 008 VALID SelCheck(47) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol CHECKBOX oCheckBo48 VAR lCheckBo48 PROMPT "" SIZE 010, 008 VALID SelCheck(48) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol CHECKBOX oCheckBo49 VAR lCheckBo49 PROMPT "" SIZE 010, 008 VALID SelCheck(49) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol CHECKBOX oCheckBo50 VAR lCheckBo50 PROMPT "" SIZE 010, 008 VALID SelCheck(50) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol CHECKBOX oCheckBo51 VAR lCheckBo51 PROMPT "" SIZE 010, 008 VALID SelCheck(51) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol CHECKBOX oCheckBo52 VAR lCheckBo52 PROMPT "" SIZE 010, 008 VALID SelCheck(52) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol CHECKBOX oCheckBo53 VAR lCheckBo53 PROMPT "" SIZE 010, 008 VALID SelCheck(53) OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol CHECKBOX oCheckBo54 VAR lCheckBo54 PROMPT "" SIZE 010, 008 VALID SelCheck(54) OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 210, nCol CHECKBOX oCheckBo55 VAR lCheckBo55 PROMPT "" SIZE 010, 008 OVALID SelCheck(55) OFF oDlgLav COLORS 0, 16777215 PIXEL
	nCol := nCol+11
	@ 007, nCol SAY oSay45 PROMPT "SECAGEM EM VARAL POR GOTEJAMENTO" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 030, nCol SAY oSay46 PROMPT "SECAGEM A SOMBRA" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 052, nCol SAY oSay47 PROMPT "SECAGEM NA HORIZONTAL" 									FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 075, nCol SAY oSay48 PROMPT "LIMPEZA A UMIDO PRO-NORMAL" 								FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 097, nCol SAY oSay49 PROMPT "LIMPEZA A UMIDO PRO-SUAVE" 						   		FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 120, nCol SAY oSay50 PROMPT "LIMPEZA A UMIDO PRO-MUITO SUAVE" 						FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 142, nCol SAY oSay51 PROMPT "NAO LIMPAR A UMIDO" 										FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 165, nCol SAY oSay52 PROMPT "SECAGEM EM VARAL POR GOTEJAMENTO A SOMBRA" 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 187, nCol SAY oSay53 PROMPT "PERMITIDO ALVEJAMENTO SOMENTE COM OXIGENIO" 				FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
	@ 210, nCol SAY oSay54 PROMPT "SECAR PENDURADO, A SOMBRA E SEM TORCER" 					FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL
//    @ 210, nCol SAY oSay55 PROMPT "" 														FONT oFont1 SIZE 085, 016 OF oDlgLav COLORS 0, 16777215 PIXEL

	@ 240, 496 BUTTON oButton3 PROMPT "Limpar" 		SIZE 043, 012 ACTION LimpaLav() OF oDlgLav PIXEL
	@ 240, 548 BUTTON oButton2 PROMPT "Cancelar" 	SIZE 043, 012 ACTION Close(oDlgLav) OF oDlgLav PIXEL
	@ 240, 599 BUTTON oButton1 PROMPT "Gravar" 		SIZE 043, 012 ACTION SalvaLav() OF oDlgLav PIXEL

	ACTIVATE MSDIALOG oDlgLav CENTERED

Return

/*
�����������������������������������������������������������������������������
?????????????????????????????????????????????????????????????????????????????
??������������������������������������������������������������������������???
???Programa  ? LimpaLav ?Autor  ?Daniel R. Melo      ? Data ? 27/02/2017  ???
??������������������������������������������������������������������������???
???Desc.     ? Limpar marcados.	                                          ???
??������������������������������������������������������������������������???
???Uso       ? HOPE                                                       ???
??������������������������������������������������������������������������???
?????????????????????????????????????????????????????????????????????????????
�����������������������������������������������������������������������������
*/
Static Function LimpaLav()

	Local nX

	For nX := 1 to 55
		cCheck		:= "lCheckBo"+cValToChar(nX)
		&cCheck		:= .F.
	Next
	MvParCod	:= ""		//CODIGO
	MvParTxt	:= ""		//TEXTO
	oDlgLav:Refresh()

Return()

/*
�����������������������������������������������������������������������������
?????????????????????????????????????????????????????????????????????????????
??������������������������������������������������������������������������???
???Programa  ? SelCheck ?Autor  ?Daniel R. Melo      ? Data ? 27/02/2017  ???
??������������������������������������������������������������������������???
???Desc.     ? SELECIONA CHECKBOX E GRAVA NO MULTIGET                     ???
??������������������������������������������������������������������������???
???Uso       ? HOPE                                                       ???
??������������������������������������������������������������������������???
?????????????????????????????????????????????????????????????????????????????
�����������������������������������������������������������������������������
*/
Static Function SelCheck(nNum)

	cCheck	:= "lCheckBo"+cValToChar(nNum)
	cSay	:= "oSay"+cValToChar(nNum)

	If &cCheck == .T.
		MvParCod	:= MvParCod+StrZero(nNum,2)+","		//CODIGO
	EndIf

	If &cCheck == .F. .AND. AT(StrZero(nNum,2),MvParCod)>0
		MvParCod  := StrTran(MvParCod,StrZero(nNum,2)+",","")
	EndIf

	oDlgLav:Refresh()

Return()

/*
�����������������������������������������������������������������������������
?????????????????????????????????????????????????????????????????????????????
??������������������������������������������������������������������������???
???Programa  ? SalvaLav ?Autor  ?Daniel R. Melo      ? Data ? 27/02/2017  ???
??������������������������������������������������������������������������???
???Desc.     ? SALVA O PROCESSO DE LAVAGEM                                ???
??������������������������������������������������������������������������???
???Uso       ? HOPE                                                       ???
??������������������������������������������������������������������������???
?????????????????????????????????????????????????????????????????????????????
�����������������������������������������������������������������������������
*/
Static Function SalvaLav()

	Local nx

	MvParTxt2 := ""

	If MvParCod <> ""

		For nx := 1 to len(MvParCod) step 3
			cAux2 	:= SubStr(MvParCod,nx,2)
			cSay2	:= "oSay"+cValToChar(Val(cAux2))
			MvParTxt2	:= MvParTxt2+&cSay2:CCAPTION+" |"
		Next

		MvParCod	:= SubStr(MvParCod,1,len(MvParCod)-1)	//CODIGO
		MvParTxt	:= SubStr(MvParTxt2,1,len(MvParTxt2)-2)	//TEXTO

	ElseIf MvParCod == ""
		Alert("Processo de Lavagem em Branco!")

		MvParCod	:= ""
		MvParTxt	:= ""

	EndIf

	oMSGrade:aCols[oMSGrade:nat,len(OMSGrade:aHeader)]	:= MvParCod
	oMSGrade:aCols[oMSGrade:nat,len(OMSGrade:aHeader)-1]	:= MvParTxt
	Close(oDlgLav)
	oMsGrade:refresh()

Return()

Return()


Static Function ReplyLn(_tp)
// Rotina para replica��o das linhas da grade para outras cores escolhidas

	Local nX
	Local nX2

	If _tp = 2 .OR. _tp = 1
		If MsgYesNo("Ser�o replicadas as informa��es da Cor: "+oMSGrade:aCols[oMSGrade:nat,1]+" para as outras cores?")
			For nX := 1 to len(oMSGrade:aCols)
				for nX2 := 1 to len(oMSGrade:aCols[oMSGrade:nat])-1
					If nX <> oMSGrade:nat .AND. !AllTrim(Str(nX2)) $ "1|2|" .AND. alltrim(oMSGrade:aCols[nX,nX2])==""
						oMSGrade:aCols[nX,nX2] := oMSGrade:aCols[oMSGrade:nat,nX2]
					EndIf

				Next nX2
			Next nX
		EndIf
	EndIf

	oMsGrade:refresh()

Return


//------------------------------------------------
Static Function fMSMix(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de Mix para regra de aglutina��o de Ordem de Produ��o
// Tabela criada: SZH

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {}
	Local aAlterFields	:= {}
	Local cAchou			:= .F.
	Local _i
	Static oMSGrade

	// Define field properties
	If alltrim(cColuna) <> ""
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
		DbSelectArea("TMPSBV")
		//  inicio ajuste mix

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")
			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY BV_CHAVE,R_E_C_N_O_"

			cQuery := ChangeQuery(cQuery)

			If SELECT("TMPSBV") > 0
				TMPSBV->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		EndIf

		cQuery2  := " SELECT * FROM "+RetSQLName("SZD")+" (nolock) WHERE D_E_L_E_T_ = ' ' AND ZD_FILIAL = '"+xFilial("SZD")+"' AND ZD_PRODUTO='"+SubString(SB4->B4_COD,1,15)+"'"

		If SELECT("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)

		DbSelectArea("TMPSZD")
		TMPSZD->(DbGoTop())

		While TMPSZD->(!EOF())
			For _i := 1 to 200
				If &("TMPSZD->ZD_TAM"+StrZero(_i,3))=="X"
					_campo	:= "ZF_TAM"+StrZero(_i,3)
					_campbv	:= "ZD_TAM"+StrZero(_i,3)

					cQdes := " SELECT * FROM "+RetSQLName("SBV")+" (NOLOCK) WHERE BV_TABELA = '"+cColuna+"' AND D_E_L_E_T_ = '' AND BV_XCAMPO = '"+_campbv+"' "

					If Select("TMPQDES") > 0
						TMPQDES->(DbCloseArea())
					EndIf
					dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQdes),"TMPQDES",.T.,.T.)

					_desc	:= TMPQDES->BV_DESCRI
					_chave	:= TMPQDES->BV_CHAVE

					If Alltrim(cColuna) $ GetMv("HP_COLUNA")
						_chavebv := Alltrim(TMPQDES->BV_CHAVE)
					Else
						_chavebv := TMPQDES->R_E_C_N_O_
					EndIf

					DbSelectArea("SX3")
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo))
						nX := ASCAN( aFields, {|x| alltrim(x) == alltrim(_campo)})
						If nX = 0
							Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
							Aadd(aAlterFields,_campo)
							Aadd(aHeaderEx, {Space(5)+ alltrim(_desc)+ Space(5),SX3->X3_CAMPO,"@E 99",2,0,"",SX3->X3_USADO,"N","","R","","",_chavebv})
						EndIf

					EndIf
				EndIf
			Next
			TMPSZD->(DbSkip())
		EndDo

		aSort(aFields)
		aSort(aAlterFields)
		ASORT(aHeaderEx, , , { |x,y| x[13] < y[13] } )

	Else
		Aadd(aFields,"ZH_TAM001")
		Aadd(aAlterFields,"ZH_TAM001")

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		If SX3->(DbSeek("ZH_TAM001"))
			Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,"@E 99",2,0,"",SX3->X3_USADO,"N","","R","",""})
		EndIf

	EndIf

	If _tp = 8 	//Inclusao
		DbSelectArea("SZH")
		DbSetOrder(1)
		DbSeek(xfilial("SZH")+SubString(SB4->B4_COD,1,15))

		While !EOF() .AND. SubString(SZH->ZH_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)

			// Define field values
			If !Found()
				For nX := 1 to Len(aHeaderEx)
					DbSelectArea("SX3")
					SX3->(DbSetOrder(2))
					If DbSeek(aFields[nX])
						Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
					EndIf
				Next nX
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill :={}

			ElseIf Found()
				Alert("Favor clicar em alterar, o produto em quest�o j� tem MIX cadastrado!")
				TMPSZD->(DbCloseArea())
				Close(oDlg)
				Return()
			EndIf
			DbSkip()
		EndDo
	EndIf

	If _tp = 9 	//Altera��o
		DbSelectArea("SZH")
		DbSetOrder(1)
		DbSeek(xfilial("SZH")+SubString(SB4->B4_COD,1,15))

		While !EOF() .AND. SubString(SZH->ZH_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)

			If Found()
				For nX := 1 to Len(aHeaderEx)
					_campo := aHeaderEx[Nx,2]//"SZH->ZH_TAM"+StrZero(nX,3)
					Aadd(aFieldFill,&_campo) //TMPSBV->BV_CHAVE)
				Next
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill	:={}
				cAchou		:= .T.
			EndIf
			DbSkip()
		EndDo

		If !Found() .AND. cAchou == .F.
			Alert("Favor clicar em incluir, o produto em quest�o n�o tem MIX cadastrado!")
			TMPSZD->(DbCloseArea())
			Close(oDlg)
			Return()
		EndIf
	EndIf

	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

	TMPSZD->(DbCloseArea())

Return


//------------------------------------------------
Static Function fMSFicha(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de Ficha T�cnica
// Tabela criada: SZN

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {}
	Local aAlterFields	:= {}
	Local cAchou			:= .F.
	Static oMSGrade

	// Define field properties
	Aadd(aFields,"ZN_PARTES") //TMPSBV->BV_CHAVE)
	Aadd(aAlterFields,"ZN_PARTES")

	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZN_PARTES"))
		Aadd(aHeaderEx, {alltrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO-70,SX3->X3_DECIMAL,"",SX3->X3_USADO,"C","","R","",""})
	EndIf

	If alltrim(cColuna) <> ""
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)
		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
		DbSelectArea("TMPSBV")

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")
			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY BV_CHAVE,R_E_C_N_O_"

			cQuery := ChangeQuery(cQuery)

			If SELECT("TMPSBV") > 0
				TMPSBV->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		EndIf

		cQuery2  := " SELECT * FROM "+RetSQLName("SZD")+" (nolock) WHERE D_E_L_E_T_ = ' ' AND ZD_FILIAL = '"+xFilial("SZD")+"' AND ZD_PRODUTO='"+SubString(SB4->B4_COD,1,8)+"'"
		If SELECT("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)
		DbSelectArea("TMPSZD")

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")
			While !EOF() .AND. TMPSBV->BV_TABELA == cColuna
				DbSelectArea("SX3")
				_campo		:= "ZN_"+Alltrim(SUBSTRING(TMPSBV->BV_XCAMPO,4,8))
				cZdVerif	:= 0

				Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
				Aadd(aAlterFields,_campo)

				TMPSZD->(DbGoTop())

				While TMPSZD->(!EOF())
					//If &("TMPSZD->ZD_TAM"+StrZero(len(aFields)-1,3))=="X"
					If &("TMPSZD->"+SUBSTRING(TMPSBV->BV_XCAMPO,1,9))=="X"
						cZdVerif += 1

						SX3->(DbSetOrder(2))
						If SX3->(DbSeek(_campo)) .AND. cZdVerif==1
							Aadd(aHeaderEx, {alltrim(TMPSBV->BV_DESCRI),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO-40,SX3->X3_DECIMAL,"",SX3->X3_USADO,"C","","R","",""})
						EndIf
					EndIf

					TMPSZD->(DbSkip())
				EndDo


				DbSelectArea("TMPSBV")
				DbSkip()
			EndDo
			TMPSZD->(DbCloseArea())
		Else
			While !EOF() .AND. TMPSBV->BV_TABELA == cColuna
				DbSelectArea("SX3")
				_campo		:= "ZN_TAM"+StrZero(len(aFields),3)
				cZdVerif	:= 0

				Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
				Aadd(aAlterFields,_campo)

				TMPSZD->(DbGoTop())

				While TMPSZD->(!EOF())
					If &("TMPSZD->ZD_TAM"+StrZero(len(aFields)-1,3))=="X"
						cZdVerif += 1

						SX3->(DbSetOrder(2))
						If SX3->(DbSeek(_campo)) .AND. cZdVerif==1
							Aadd(aHeaderEx, {alltrim(TMPSBV->BV_DESCRI),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO-40,SX3->X3_DECIMAL,"",SX3->X3_USADO,"C","","R","",""})
						EndIf
					EndIf

					TMPSZD->(DbSkip())
				EndDo

				DbSelectArea("TMPSBV")
				DbSkip()
			EndDo
			TMPSZD->(DbCloseArea())
		EndIf
		DbClosearea()

	Else
		Aadd(aFields,"ZN_TAM001")
		Aadd(aAlterFields,"ZN_TAM001")

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		If SX3->(DbSeek("ZN_TAM001"))
			Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,"N","","R","",""})
		EndIf

	EndIf

	If _tp = 11 	//Inclusao
		DbSelectArea("SZN")
		DbSetOrder(1)
		DbSeek(xfilial("SZN")+PADR(SB4->B4_COD,15))

		If Found()
			Alert("Favor clicar em alterar, o produto em quest�o j� tem Ficha cadastrada!")
			Close(oDlg)
			Return()
		EndIf

		For nX := 1 to Len(aHeaderEx)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			EndIf
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}

	EndIf

	If _tp = 12 	//Altera��o
		DbSelectArea("SZN")
		DbSetOrder(1)
		DbSeek(xfilial("SZN")+PADR(SB4->B4_COD,15))

		If !Found()
			Alert("Favor clicar em incluir, o produto em quest�o n�o tem Ficha cadastrado!")
			Close(oDlg)
			Return()
		Else
			While !EOF() .AND. SubString(SZN->ZN_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)

				For nX := 1 to Len(aHeaderEx)
					_campo := "SZN->"+aHeaderEx[nX,2]
					Aadd(aFieldFill,&_campo) //TMPSBV->BV_CHAVE)
				Next
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill	:={}
				DbSkip()
			EndDo
		EndIf
	EndIf

	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return

User Function VALIDT()
	Local lok := .T.
	If M->ZD_TEMPO == 0
		MessageBox("Favor inserir um valor de tempo","tempo",16)
		lok := .F.
	EndIf
Return lok

//------------------------------------------------
Static Function fMSFich1(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de Ficha T�cnica
// Tabela criada: SZN

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {"ZO_TEMPO","ZO_DESCRI","ZO_OBS","ZO_OBSMAQ","ZO_TEMPO1","ZO_TEMPO2","ZO_TEMPO3","ZO_USRALT1","ZO_USRALT2","ZO_USRALT3"}
	Local aAlterFields 		:= {"ZO_TEMPO","ZO_DESCRI","ZO_OBS","ZO_OBSMAQ","ZO_TEMPO1","ZO_TEMPO2","ZO_TEMPO3","ZO_USRALT1","ZO_USRALT2","ZO_USRALT3"}
	Local cAchou			:= .F.
//	Local _nTpTotal			:= 0
	Static oMSGrade


	SX3->(DbSetOrder(2))
	For nX := 1 to len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			If aFields[nX] = "ZO_DESCRI"
				Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"ZAO","R","",""})
			ElseIf aFields[nX] = "ZO_OBSMAQ"
				Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"ZAP","R","",""})
			Else
				Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_VALIDT(M->ZO_TEMPO)",SX3->X3_USADO,SX3->X3_TIPO,"","R","",""})
			EndIf
		EndIf
	Next
/*
	If _tp = 13 	//Inclusao
		DbSelectArea("SZO")
		DbSetOrder(1)
		DbSeek(xfilial("SZO")+PADR(SB4->B4_COD,15))
		
		If Found()
			Alert("Favor clicar em alterar, o produto em quest�o j� tem Ficha cadastrada!")
			Close(oDlg)
			Return()
		EndIf
		
		For nX := 1 to Len(aHeaderEx)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			EndIf
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}

	EndIf
	*/
	If _tp = 13 	//Altera��o
		DbSelectArea("SZO")
		DbSetOrder(1)
		DbSeek(xfilial("SZO")+PADR(SB4->B4_COD,15))

		If Found()
			While !EOF() .AND. SubString(SZO->ZO_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)

				Aadd(aFieldFill,SZO->ZO_TEMPO)
				Aadd(aFieldFill,SZO->ZO_DESCRI)
				Aadd(aFieldFill,SZO->ZO_OBS)
				Aadd(aFieldFill,SZO->ZO_OBSMAQ)
				Aadd(aFieldFill,SZO->ZO_TEMPO1)
				Aadd(aFieldFill,SZO->ZO_TEMPO2)
				Aadd(aFieldFill,SZO->ZO_TEMPO3)
				Aadd(aFieldFill,SZO->ZO_USRALT1)
				Aadd(aFieldFill,SZO->ZO_USRALT2)
				Aadd(aFieldFill,SZO->ZO_USRALT3)
				Aadd(aFieldFill,SZO->(RECNO()))
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill	:={}
				DbSkip()
			EndDo
		Else
			Alert("Favor inserir tempo pela nova rotina Cadastro de Tempos, o produto em quest�o n�o tem tempo cadastrado!")
			Close(oDlg)
			Return()
		EndIf
	EndIf

	//oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624,, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

	//oMSGrade:bchange	:= {|| U_AtuTempo() }

Return

//------------------------------------------------
Static Function fMSFich2(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de Ficha T�cnica
// Tabela criada: SZD

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZD_COR","ZD_DESCRIC","ZD_TEMPO","ZD_TEMPO1","ZD_TEMPO2","ZD_TEMPO3","ZD_USRALT1","ZD_USRALT2","ZD_USRALT3"} //"ZD_DTLIB","ZD_SITUAC",
	Local aAlterFields := {"ZD_TEMPO"}
	Static oMSGrade



	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZD_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf
	If SX3->(DbSeek("ZD_DESCRIC"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_DESCRIC","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If SX3->(DbSeek("ZD_TEMPO"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_TEMPO",SX3->X3_PICTURE,20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If SX3->(DbSeek("ZD_TEMPO1"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_TEMPO1",SX3->X3_PICTURE,20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If SX3->(DbSeek("ZD_USRALT1"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_USRALT1",SX3->X3_PICTURE,20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If SX3->(DbSeek("ZD_TEMPO2"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_TEMPO2",SX3->X3_PICTURE,20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If SX3->(DbSeek("ZD_USRALT2"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_USRALT2",SX3->X3_PICTURE,20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If SX3->(DbSeek("ZD_TEMPO3"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_TEMPO3",SX3->X3_PICTURE,20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If SX3->(DbSeek("ZD_USRALT3"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_USRALT3",SX3->X3_PICTURE,20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If _tp = 14 	//Altera��o
		_qry := "SELECT COUNT(*) QTD FROM "+RetSqlName("SG1")+" SG1 "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = G1_COMP "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(G1_COD,8) = '"+alltrim(cCodigo)+"' AND B1_TIPO = 'BN' "

		If SELECT("TMPQTD") > 0
			TMPQTD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPQTD",.T.,.T.)

		If TMPQTD->QTD == 0
			Alert("Refer�ncia n�o tem tempo cadastrado, favor verificar com o Time de Cadastro !")
			Close(oDlg)
			Return()
		Else

			DbSelectArea("SZD")
			DbSetOrder(1)
			DbSeek(xfilial("SZD")+PADR(SB4->B4_COD,15))

			While !EOF() .AND. SubString(SZD->ZD_PRODUTO,1,15) = SubString(SB4->B4_COD,1,15)

				Aadd(aFieldFill,SZD->ZD_COR)
				Aadd(aFieldFill,SZD->ZD_DESCRIC)
				Aadd(aFieldFill,SZD->ZD_TEMPO)
				Aadd(aFieldFill,SZD->ZD_TEMPO1)
				Aadd(aFieldFill,SZD->ZD_USRALT1)
				Aadd(aFieldFill,SZD->ZD_TEMPO2)
				Aadd(aFieldFill,SZD->ZD_USRALT2)
				Aadd(aFieldFill,SZD->ZD_TEMPO3)
				Aadd(aFieldFill,SZD->ZD_USRALT3)
				Aadd(aFieldFill,SZD->(RECNO()))
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill	:={}
				DbSkip()
			EndDo

			oMSGrade := MsNewGetDados():New( 189, 005, 275, 624,+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

		EndIf
	EndIf
Return

Static Function ReplTmp()
// Replica as quantidades por linha

	Local nX
	Local nX2

	If MsgYesNo("Ser�o replicadas as informa��es da Cor: "+oMSGrade:aCols[oMSGrade:nat,1]+" para as outras cores?")
		For nX := 1 to len(oMSGrade:aCols)
			for nX2 := 3 to len(oMSGrade:aCols[oMSGrade:nat])-1
				If nX <> oMSGrade:nat
					oMSGrade:aCols[nX,3] := oMSGrade:aCols[oMSGrade:nat,3]
				EndIf
			Next nX2
		Next nX
	EndIf

	oMSGrade:refresh()

Return



User Function AtuTempo()

	Local _i
	cTempo		:= 0

	For _i := 1 to len(oMSGrade:aCols)

		If len(oMSGrade:aCols[_i]) == 6
			If oMSGrade:aCols[_i,1] <> 0 .AND. !oMSGrade:aCols[_i,6]
				cTempo	+= oMSGrade:aCols[_i,1]
			EndIf
		Else
			If oMSGrade:aCols[_i,1] <> 0 .AND. !oMSGrade:aCols[_i,5]
				cTempo	+= oMSGrade:aCols[_i,1]
			EndIf
		EndIf

	Next

	oMSGrade:Refresh()
	oTempo:refresh()
	oDlg:refresh()

Return

//------------------------------------------------
Static Function fMSOPC(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de Ficha T�cnica
// Tabela criada: SZN

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {}
	Local aAlterFields	:= {}
	Local cAchou			:= .F.
	Static oMSOPC

	// Define field properties
	aFields := {"ZAU_COR","ZAU_PRODUT","ZAU_GRUPO","ZAU_ITEM"}
	aAlterFields := {"ZAU_COR","ZAU_PRODUT","ZAU_GRUPO","ZAU_ITEM"}

	SX3->(DbSetOrder(2))
	For nX := 1 to len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(5),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",SX3->X3_USADO,SX3->X3_TIPO,"","R","",""})
		EndIf
	Next

	If _tp = 15 	//Inclusao
		DbSelectArea("ZAU")
		DbSetOrder(1)
		DbSeek(xfilial("ZAU")+LEFT(SB4->B4_COD,8))

		If Found()
			Alert("Favor clicar em alterar, o produto em quest�o j� tem Opcionais!")
			Close(oDlg)
			Return()
		EndIf

		For nX := 1 to Len(aHeaderEx)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			EndIf
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}

	EndIf

	If _tp = 16 	//Altera��o
		DbSelectArea("ZAU")
		DbSetOrder(1)
		DbSeek(xfilial("ZAU")+left(SB4->B4_COD,8))

		If !Found()
			Alert("Favor clicar em incluir, o produto em quest�o n�o tem Opcionais cadastrado!")
			Close(oDlg)
			Return()
		Else
			While !EOF() .AND. SubString(ZAU->ZAU_CODREF,1,8) = SubString(SB4->B4_COD,1,8)

				Aadd(aFieldFill,ZAU->ZAU_COR)
				Aadd(aFieldFill,ZAU->ZAU_PRODUT)
				Aadd(aFieldFill,ZAU->ZAU_GRUPO)
				Aadd(aFieldFill,ZAU->ZAU_ITEM)
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)
				aFieldFill	:={}
				DbSkip()
			EndDo
		EndIf
	EndIf

	oMSOPC := MsNewGetDados():New( 189, 005, 275, 624, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

	oDlg:refresh()

Return

//------------------------------------------------
Static Function fMSSta(_tp, cColuna)
//------------------------------------------------
// Rotina para manute��o de cadastro de Status do Produto
// Tabela criada: SZL

	Local nX
	Local aHeaderEx		:= {}
	Local aColsEx			:= {}
	Local aFieldFill		:= {}
	Local aFields			:= {"ZL_PRODUTO","ZL_COR","BV_DESCRI","ZL_TAM","ZL_BLQDEM","ZL_STATUS","ZL_DTLIBER","ZL_DTENCER"}
	Local aAlterFields	    := {"ZL_BLQDEM" ,"ZL_STATUS","ZL_DTLIBER","ZL_DTENCER"}

	Local aCicEstFields		:= {"ZL_PRODUTO","ZL_COR","BV_DESCRI","ZL_TAM","ZL_BLQDEM","ZL_STATUS","ZL_DTLIBER","ZL_DTENCER","ZL_CODCIC","ZL_DSCCIC","ZL_CODEST","ZL_DSCEST"}
	Local aAlterCicEst	    := {"ZL_BLQDEM" ,"ZL_STATUS","ZL_DTLIBER","ZL_DTENCER","ZL_CODCIC","ZL_CODEST"}


	Public nCountSta		:= 0
	Static oMSSta

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZL_PRODUTO"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZL_COR"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("BV_DESCRI"))
		Aadd(aHeaderEx, {Space(40)+AllTrim(X3Titulo())+ Space(40),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZL_TAM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZL_BLQDEM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZL_STATUS"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(4,M->ZL_STATUS)",;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZL_DTLIBER"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,; // "U_AltStat(nCountSta,M->ZL_STATUS,M->ZL_DTLIBER,M->ZL_DTENCER)",;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZL_DTENCER"))
		Aadd(aHeaderEx, {Space(16)+AllTrim(X3Titulo())+ Space(16),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf


	// Define field values
	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+SubString(cCodigo,1,8))
	aFieldFill :={}
	//Adiciona no aHeader os campos de Cliclo x Esta��o apenas para os tipos solicitados pela equipe de Produtos
	If SB1->B1_TIPO $ ("PF|KT|ME")
		If SX3->(DbSeek("ZL_CODCIC"))
			Aadd(aHeaderEx, {Space(3)+AllTrim(X3Titulo())+ Space(3),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		EndIf
		If SX3->(DbSeek("ZL_DSCCIC"))
			Aadd(aHeaderEx, {Space(30)+AllTrim(X3Titulo())+ Space(30),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		EndIf
		If SX3->(DbSeek("ZL_CODEST"))
			Aadd(aHeaderEx, {Space(6)+AllTrim(X3Titulo())+ Space(6),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		EndIf
		If SX3->(DbSeek("ZL_DSCEST"))
			Aadd(aHeaderEx, {Space(30)+AllTrim(X3Titulo())+ Space(30),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		EndIf

		aAlterFields:= aClone(aAlterCicEst)
	EndIf

	While !EOF() .AND. SubString(SB1->B1_COD,1,8) = SubString(cCodigo,1,8)

		Aadd(aFieldFill, SubStr(SB1->B1_COD,1,8))
		Aadd(aFieldFill, SubStr(SB1->B1_COD,9,3))
		Aadd(aFieldFill, POSICIONE("SBV",1,xFilial("SBV")+'COR'+SubStr(SB1->B1_COD,9,3),"BV_DESCRI"))
		Aadd(aFieldFill, SubStr(SB1->B1_COD,12,4))
		Aadd(aFieldFill, SB1->B1_YBLQDEM)
		Aadd(aFieldFill, SB1->B1_YSITUAC)
		Aadd(aFieldFill, SB1->B1_YDATLIB)
		Aadd(aFieldFill, SB1->B1_YDATENC)
		If SB1->B1_TIPO $ ("PF|KT|ME")
			Aadd(aFieldFill, SB1->B1_XCODCIC)
			Aadd(aFieldFill, SB1->B1_XDSCCIC)
			Aadd(aFieldFill, SB1->B1_XCODEST)
			Aadd(aFieldFill, SB1->B1_XDSCEST)
		EndIf
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo

	oMSSta := MsNewGetDados():New( 189, 005, 275, 624, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return


//------------------------------------------------
Static Function fMSCBar(_tp, cColuna)
//------------------------------------------------
// Rotina para cadastro de C�dito de Barras EAN13
// Par�metro criado: MV_HCODBAR

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZD_COR","ZD_DESCRIC"}
	Local aAlterFields := {}
	Static oMSGrade

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZD_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf
	If SX3->(DbSeek("ZD_DESCRIC"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_DESCRIC","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If alltrim(cColuna) <> ""
		// Busca os tamanhos dispon�veis selecionados na tabela SBV

		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		cqryc := "SELECT COUNT(*)+1 AS QTD FROM " +RetSQLName("SBV") + "(NOLOCK) WHERE BV_XCAMPO <> '' AND D_E_L_E_T_ ='' AND BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' "

		If SELECT("TMPSBVC") > 0
			TMPSBVC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cqryc),"TMPSBVC",.T.,.T.)

		nzd:= TMPSBVC->QTD

		While TMPSBV->(!EOF())
			If Empty(TMPSBV->BV_XCAMPO)
				DbSelectArea("SBV")
				DbSetOrder(1)
				If DbSeek(xfilial("SBV")+TMPSBV->BV_TABELA+TMPSBV->BV_CHAVE)
					RecLock("SBV",.F.)
					SBV->BV_XCAMPO := "ZD_TAM"+Padl(cvaltochar(nzd),3,"0")
					MsUnLock()
					nzd ++
				EndIf
			EndIf
			TMPSBV->(DbSkip())
		EndDo

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")
			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY BV_CHAVE,R_E_C_N_O_"

		Else
			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY R_E_C_N_O_"
		EndIf

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)


		/*
		//Conta total e registros
		Count To nRecTMPSBV
		If nRecTMPSBV == 0
			oColuna:SetFocus()
		EndIf
		*/


		_YTAMZ:={}
		While TMPSBV->(!EOF()) .AND. alltrim(TMPSBV->BV_TABELA) == alltrim(cColuna)
			Aadd(_YTAMZ,{TMPSBV->BV_XCAMPO,TMPSBV->BV_CHAVE})
			DbSkip()
		EndDo

		DbSelectArea("TMPSBV")
		DbGoTop()

		While !EOF() .AND. TMPSBV->BV_TABELA == cColuna
			DbSelectArea("SX3")
			If 	Alltrim(cColuna) $ GetMv("HP_COLUNA")
				_campo		:= "ZH_TAM"+SUBSTRING(TMPSBV->BV_XCAMPO,7,LEN(TMPSBV->BV_XCAMPO))
				cZdVerif	:= 0
			Else
				_campo		:= "ZH_TAM"+StrZero(len(aFields)-1,3)
				cZdVerif	:= 0
			EndIf

			Aadd(aFields,_campo)
			Aadd(aAlterFields,_campo)

			cZdVerif += 1

			SX3->(DbSetOrder(2))
			If SX3->(DbSeek(_campo)) .AND. cZdVerif==1
				Aadd(aHeaderEx, {alltrim(TMPSBV->BV_DESCRI)+Space(4),SX3->X3_CAMPO,"@!",12,0,"",SX3->X3_USADO,"C","","R","","",alltrim(TMPSBV->BV_CHAVE),alltrim(TMPSBV->BV_XCAMPO)})
			EndIf

			DbSelectArea("TMPSBV")
			DbSkip()
		EndDo
		/*
		If 	Alltrim(cColuna) $ GetMv("HP_COLUNA")
			ASORT(aHeaderEx, , , { |x,y| x[13] < y[13] } )
		EndIf
		*/
		DbClosearea()


	Else
		Aadd(aFields,"ZD_TAM001")
		Aadd(aAlterFields,"ZD_TAM001")

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		If SX3->(DbSeek("ZD_TAM001"))
			Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,"@!",12,0,"",SX3->X3_USADO,"C","","R","",""})
		EndIf
	EndIf
	// Define field values

	DbSelectArea("SZD")
	DbSetOrder(1)
	DbSeek(xfilial("SZD")+SubString(SB4->B4_COD,1,15))

	While !EOF() .AND. AllTrim(SZD->ZD_PRODUTO) = AllTrim(SB4->B4_COD)

		Aadd(aFieldFill, SZD->ZD_COR)
		Aadd(aFieldFill, SZD->ZD_DESCRIC)
		For nX := 3 to Len(aheaderex)
			If Alltrim(cColuna) $ GetMv("HP_COLUNA")
				_campo := "ZD_TAM"+substring(aheaderex[nx,2],7,len(aheaderex[nx,2]))
				_tam := aheaderex[nx,13]
			Else
				_campo := "ZD_TAM"+StrZero(nX-2,3)
				_tam := rettam(nx-2,cColuna,"C")
			EndIf

			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+AllTrim(SB4->B4_COD)+SZD->ZD_COR+_tam)
			If Found() .AND. alltrim(SB1->B1_CODBAR)<>""
				_campx := SubStr(SB1->B1_CODBAR,1,13)
				Aadd(aFieldFill,_campx)
			Else

				Aadd(aFieldFill,"")
			EndIf
		Next
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSelectArea("SZD")
		DbSkip()
	EndDo

	oMSGrade := MsNewGetDados():New( 189, 005, 275, 624, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return

User Function valacols(_tp, cColuna)
// Rotina para atualiza��o de Acols com base no grupo de tamanhos selecionado na tabela SBV

	cColuna	:= UPPER(cColuna)
	fMSGrade(_tp, cColuna)
	oMSGrade:Refresh()
	oDlg:refresh()

Return

Static Function CHKTAM(cCodigo,cColuna)

	Local cEOL	     := +Chr(13)+Chr(10)
	Local cQuery1
	Local cQuery2
	Local aFields   := {}
	Local aHeaderEx := {}
	Local _x := 0
	Local _i := 0
	Local lOk := .T.
	Local cProduto := cCodigo

//	Define field properties
	If alltrim(cColuna) <> ""
		cQuery1 := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI "+cEOL
		cQuery1 += "FROM "+RetSQLName("SBV")+" "+cEOL
		cQuery1 += "WHERE D_E_L_E_T_ = '' "+cEOL
		cQuery1 += "		AND BV_FILIAL = '" +xFilial("SBV") +"' "+cEOL
		cQuery1 += "		AND BV_TABELA = '"+cColuna+"' "+cEOL
		cQuery1 += "ORDER BY R_E_C_N_O_ "+cEOL

		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSBV",.T.,.T.)

		cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" "+cEOL
		cQuery2  += "WHERE D_E_L_E_T_ = ' ' "+cEOL
		cQuery2  += "		AND ZD_FILIAL = '"+xFilial("SZD")+"' "+cEOL
		cQuery2  += "		AND ZD_PRODUTO='"+AllTrim(cProduto)+"' "+cEOL

		If Select("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)
		DbSelectArea("TMPSZD")
		TMPSZD->(DbGoTop())

		While TMPSZD->(!EOF())
			For _i := 1 to 200
				If &("TMPSZD->ZD_TAM"+StrZero(_i,3))=="X"
					_campo	:= "ZD_TAM"+StrZero(_i,3)
					_corxtam := TMPSZD->ZD_COR

					DbSelectArea("TMPSBV")
					TMPSBV->(DbGoTop())
					For _x := 1 to _i
						_chave	:= TMPSBV->BV_CHAVE
						TMPSBV->(DbSkip())
					Next nX
					DbSelectArea("SX3")
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo))
						nX := ASCAN( aFields, {|x| alltrim(x) == alltrim(_campo)})
						If nX = 0
							Aadd(aFields,_campo)
							Aadd(aHeaderEx, {alltrim(_corxtam)+ Space(5),SX3->X3_CAMPO,"@E 99",2,0,"",SX3->X3_USADO,"N","","R","",""})
						Endif
					Endif
				Endif
			Next
			TMPSZD->(DbSkip())
		EndDo
	EndIf
	_tem := ASCAN( aHeaderEx, {|x| ALLTRIM(x[1]) == ALLTRIM(cMpT)})
	If _tem = 0
		lOk := .F.
		MessageBox(" A MP "+AllTrim(cMp)+" n�o possui esse tamanho cadastrado na cor " +Alltrim(cMpC)+ " !","SEM-TAM",16)
		M->ZK_TAMNV := ""
	Endif

Return lOk

Static Function Save(_tp)
// Rotina para saltar a grade de produtos no SB4 e no SB1, Mix de Produto, C�digo de Barras e Status do Produto

	Local I,W

	Local cParCicPad:= AllTrim(SuperGetMV("HP_CICPAD",.F.,"002"))
	Local cDscCicPad:= POSICIONE("ZB7",1,xFilial("ZB7")+cParCicPad,"ZB7_DSCCIC")
	Local ccodP := ""
	Local cdescP := ""
	Local lAtutmp := .F.

	If alltrim(cColuna) = ""
		Alert("Campo Coluna n�o preenchido!")
		Return()
	EndIf

	If SubString(cValToChar(cRastro),1,1)="L" .or. SubString(cValToChar(cRastro),1,1)="1"
		cRastroBD	:= "L"
	ElseIf SubString(cValToChar(cRastro),1,1)="S" .or. SubString(cValToChar(cRastro),1,1)="2"
		cRastroBD	:= "S"
	ElseIf SubString(cValToChar(cRastro),1,1)="N" .or. SubString(cValToChar(cRastro),1,1)="3"
		cRastroBD	:= "N"
	EndIf

	If SubString(cValToChar(cEnd),1,1)="S" .or. SubString(cValToChar(cEnd),1,1)="1"
		cEndBD		:= "S"
	ElseIf SubString(cValToChar(cEnd),1,1)="N" .or. SubString(cValToChar(cEnd),1,1)="2"
		cEndBD		:= "N"
	EndIf

	If cTipo $ "KT|ME|PF|PL"
		If SubString(cValToChar(cWeb),1,1)="S" .or. SubString(cValToChar(cWeb),1,1)="1"
			cWebBD		:= "S"
		ElseIf SubString(cValToChar(cWeb),1,1)="N" .or. SubString(cValToChar(cWeb),1,1)="2"
			cWebBD		:= "N"
		EndIf
	Else
		cWebBD		:= "N"
	EndIf

	If _tp == 1
		If AllTrim(cTipo)=""
			Alert("Favor preencher a Tipo do Produto!")
			return(.F.)
		EndIf
		If AllTrim(cOrigem)=""
			Alert("Favor preencher a Origem do Produto!")
			return(.F.)
		EndIf
		If AllTrim(cGrupo)=""
			Alert("Favor preencher o Grupo do Produto!")
			return(.F.)
		EndIf
		If AllTrim(cCodigo)=""
			Alert("Favor preencher o C�digo do Produto!")
			return(.F.)
		EndIf
		If AllTrim(cCodigoDesc)=""
			Alert("Favor preencher o Descri��o do Produto!")
			return(.F.)
		EndIf
		If AllTrim(cUM)=""
			Alert("Favor preencher a Unidade de Medida do Produto!")
			return(.F.)
		EndIf
		If cTipo $ "PF|KT|ME|PL"
			If AllTrim(cMarca)=""
				Alert("Favor preencher a Marca do Produto!")
				return(.F.)
			EndIf
			If AllTrim(cCiclo)=""
				Alert("Favor preencher o Ciclo do Produto!")
				return(.F.)
			EndIf
			If AllTrim(cGpProd)=""
				Alert("Favor preencher o Grupo do Produto!")
				return(.F.)
			EndIf
			If AllTrim(cSGrupo)=""
				Alert("Favor preencher o Sub Grupo do Produto!")
				return(.F.)
			EndIf
			If AllTrim(cColecao)=""
				Alert("Favor preencher a Cole��o do Produto!")
				return(.F.)
			EndIf
			If AllTrim(cSubCol)=""
				Alert("Favor preencher a Sub Cole��o do Produto!")
				return(.F.)
			EndIf
			If AllTrim(cTpProd)=""
				Alert("Favor preencher o Tipo do Produto!")
				return(.F.)
			EndIf
			If AllTrim(cCatProd)=""
				Alert("Favor preencher a Categoria do Produto!")
				return(.F.)
			EndIf
			If nPeso <= 0
				Alert("Favor preencher o Peso Liquido do Produto!")
				return(.F.)
			EndIf
		EndIf
		If AllTrim(cArm)=""
			Alert("Favor preencher o Armaz�m do Produto!")
			return(.F.)
		EndIf
		If AllTrim(cColuna)=""
			Alert("Favor preencher o campo Coluna do Produto!")
			return(.F.)
		EndIf
		If 	Len(OMSGrade:ACOLS)<=1 .AND. AllTrim(OMSGrade:ACOLS[1,1])==""
			Alert("Favor incluir as Cores e Tamanhos para Produto!")
			return(.F.)
		EndIf
		EMail(cCodigo)
		Begin Transaction
			RecLock("SB4",.T.)
			Replace B4_FILIAL 		with xfilial("SB4")
			Replace B4_COD			with Alltrim(cCodigo)
			Replace B4_TIPO			with cTipo
			Replace B4_YORIGEM		with cOrigem
			Replace B4_YCLASPR		with cClass
			Replace B4_GRUPO		with cGrupo
			Replace B4_DESC			with cCodigoDesc
			Replace B4_UM			with cUM
			Replace B4_YMARCA		with cMarca
			Replace B4_YNOMARC		with cMarcaDesc
			Replace B4_YCICLO		with cCiclo
			Replace B4_YNCICLO		with cCicloDesc
			Replace B4_YGPRODU		with cGpProd
			Replace B4_YNGPCOM		with cGpProdDesc
			Replace B4_YSUBGRP		with cSGrupo
			Replace B4_YNSUBGR		with cSGrupoDesc
			Replace B4_YCOLECA		with cColecao
			Replace B4_YNCOLEC		with cColecaoDesc
			Replace B4_YSUBCOL		with cSubCol
			Replace B4_YNSUBCO		with cSubColDesc
			Replace B4_YTPPROD		with cTpProd
			Replace B4_YNTPCOM		with cTpProdDesc
			Replace B4_YCATEGO		with cCatProd
			Replace B4_YNCATEG		with cCatProdDesc
			Replace B4_YOCASIA		with cOcasiao
			Replace B4_YTECIDO		with cTecido
			Replace B4_LOCPAD		with cArm
			Replace B4_RASTRO		with cRastroBD
			Replace B4_LOCALIZ		with cEndBD
			Replace B4_LINHA		with cLinha
			Replace B4_COLUNA		with cColuna
			Replace B4_PESO			with nPeso
			Replace B4_POSIPI		with cPosIPI
			Replace B4_YDATCAD		with dDtCad
			Replace B4_DWEB			with cWebBD
			//Replace B4_YTEMPO		with cTempRef
			Replace B4_SITTRIB		with cTrib
			Replace B4_YDESCPT		with cDescriPTB
			Replace B4_YDESCES		with cDescriSPA
			Replace B4_YDESCIE		with cDescriENG
			Replace B4_YOBS			with cObsProd
			MsUnLock()

			For i := 1 to Len(OMSGrade:ACOLS)
				If OMSGrade:ACOLS[i,len(OMSGrade:AHEADER)+1] == .F. .AND. AllTrim(OMSGrade:ACOLS[i,1])<>""
					DbSelectArea("SZD")
					DbSetOrder(1)
					DbSeek(xfilial("SZD")+SubString(cCodigo,1,15)+OMSGrade:ACOLS[i,1])

					If !Found() .AND. AllTrim(OMSGrade:ACOLS[i,1])<>""
						RecLock("SZD",.T.)
						Replace ZD_FILIAL	with xfilial("SZD")
						Replace ZD_PRODUTO		with SubString(Alltrim(cCodigo),1,15)
						Replace ZD_COR			with OMSGrade:ACOLS[i,1]
						Replace ZD_DESCRIC		with OMSGrade:ACOLS[i,2]
						Replace ZD_COMPOS			with OMSGrade:ACOLS[i,3]
						Replace ZD_DESCCP2		with OMSGrade:ACOLS[i,4]
						For w := 5 to len(OMSGrade:aHeader)-2
							//_cpo := "ZD_TAM"+StrZero((w-4),3)
							_cpo := "SZD->"+OMSGrade:aHeader[w][2]
							Replace &(_cpo) with OMSGrade:ACOLS[i,w]
						Next
//					Replace ZD_DTLIB			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//					Replace ZD_SITUAC			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
						Replace ZD_DESCLAV		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-1]
						Replace ZD_LAVAGE			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
						MsUnLock()
					Else
						RecLock("SZD",.F.)
						Replace ZD_DESCRIC		with OMSGrade:ACOLS[i,2]
						Replace ZD_COMPOS			with OMSGrade:ACOLS[i,3]
						Replace ZD_DESCCP2		with OMSGrade:ACOLS[i,4]
						For w := 5 to len(OMSGrade:aHeader)-2
							//_cpo := "ZD_TAM"+StrZero((w-4),3)
							_cpo := "SZD->"+OMSGrade:aHeader[w][2]
							Replace &(_cpo) with OMSGrade:ACOLS[i,w]
						Next
//					Replace ZD_DTLIB			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//					Replace ZD_SITUAC			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
						Replace ZD_DESCLAV		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-1]
						Replace ZD_LAVAGE			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
						MsUnLock()
					EndIf

					For w := 5 to len(OMSGrade:aHeader)-2
						// O que estiver com a quantidade marcada com o X, cria o produto
						If OMSGrade:ACOLS[i,w] == "X"
							//_tam := PADL(Alltrim(OMSGrade:aHeader[w][1]),4,"0") //rettam(w-4,cColuna,"C")
							_tam := rettam5(cColuna,Alltrim(OMSGrade:aHeader[w][1]))
							_Dtam := OMSGrade:aHeader[w][1] //rettam(w-4,cColuna,"D")

							DbSelectArea("SB1")
							DbSetOrder(1)
							DbSeek(xfilial("SB1")+AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)
							If !Found()
								RecLock("SB1",.T.)
								Replace B1_FILIAL		with xfilial("SB1")
								Replace B1_COD			with AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam
								Replace B1_DESC			with AllTrim(cCodigoDesc)+" - "+AllTrim(OMSGrade:ACOLS[i,2])+" - "+_Dtam
								Replace B1_TIPO			with cTipo
								Replace B1_GRUPO		with cGrupo
								Replace B1_UM			with cUM
								Replace B1_LOCPAD		with cArm
								Replace B1_SEGUM		with cUM
								Replace B1_CONV			with 1
								Replace B1_TIPCONV		with "M"
								Replace B1_PESO			with nPeso
								Replace B1_TIPODEC		with "N"
								Replace B1_ORIGEM		with cOrigem
								Replace B1_REVATU		with "001"
								Replace B1_POSIPI		with cPosIPI
								Replace B1_GRADE		with "S"
								Replace B1_RASTRO		with cRastroBD
								Replace B1_LOCALIZ		with cEndBD
								Replace B1_VEREAN		with "13"
								Replace B1_TIPOBN		with cTipo
								Replace B1_GARANT		with "N"
								Replace B1_YMARCA		with cMarca
								Replace B1_XDESCMA		with cMarcaDesc
								Replace B1_YCICLO		with cCiclo
								Replace B1_YDESCIC		with cCicloDesc
								Replace B1_YGPRODU		with cGpProd
								Replace B1_YSUBGRP		with cSGrupo
								Replace B1_YDESSUB		with cSGrupoDesc
								Replace B1_YCOLECA		with cColecao
								Replace B1_YDESCOL		with cColecaoDesc
								Replace B1_YSUBCOL		with cSubCol
								//Replace B1_YDESCOL   	with POSICIONE("ZAA",1,xFilial("ZAA")+cColecao,"ZAA_DESCRI")
								Replace B1_YDESSCO   	with cSubColDesc //POSICIONE("ZAH",1,xFilial("ZAH")+cSubCol,"ZAH_DESCRI")
								Replace B1_YTPPROD		with cTpProd
								Replace B1_YCATEGO		with cCatProd
								Replace B1_YDESCAT		with cCatProdDesc
								Replace B1_YOCASIA		with cOcasiao
								Replace B1_YTECIDO		with cTecido
								Replace B1_YDATCAD		with Date() //dDtCad
								Replace B1_YDWEB		with cWebBD
//							Replace B1_YTEMPO		with cTempo
								Replace B1_YCLASPR		with cClass
								Replace B1_SITTRIB		with cTrib
								Replace B1_YDESCPT		with cDescriPTB
								Replace B1_YDESCES		with cDescriSPA
								Replace B1_YDESCIE		with cDescriENG
								Replace B1_YOBS			with cObsProd
//							Replace B1_YDATLIB		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
								If Alltrim(cTipo) $ GetMv("HP_TIPOPRD")
									Replace B1_YSITUAC		with "PDSV" // Na inclusao grava Como produto em desenvolvimento
								Else
									Replace B1_YSITUAC		with "PNV"
								EndIf

								Replace B1_YLAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
								Replace B1_OPERPAD		with "01"
								Replace B1_MSBLQL		with "2"

								If cTipo = "ME" .AND. cOrigem = "0"
									Replace B1_GRTRIB with "006"
								ElseIf cOrigem = "1"
									Replace B1_GRTRIB with "006"
								ElseIf cOrigem = "0" .AND. cTipo <> "ME"
									Replace B1_GRTRIB with "007"
								EndIf

								If cTipo = "ME"
									Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
								Else
									Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTACUS")
								EndIf
								Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")

								If cTipo $ "MP|MI|PF|PI|KT|ME|PL"
									Replace B1_MRP			with "S"
								EndIf

								If cTipo $ ("PF|KT|ME")
									Replace B1_XCODCIC		with cParCicPad
									Replace B1_XDSCCIC		with cDscCicPad
								EndIf

								MsUnLock()
								U_HPDISPLJ(SB1->B1_COD)


							Else
								RecLock("SB1",.F.)
								Replace B1_FILIAL		with xfilial("SB1")
								Replace B1_DESC			with AllTrim(cCodigoDesc)+" - "+AllTrim(OMSGrade:ACOLS[i,2])+" - "+_Dtam
								Replace B1_TIPO			with cTipo
								Replace B1_GRUPO		with cGrupo
								Replace B1_UM			with cUM
								Replace B1_LOCPAD		with cArm
//							Replace B1_SEGUM		with cUM
//							Replace B1_CONV			with 1
								Replace B1_TIPCONV		with "M"
								Replace B1_PESO			with nPeso
								Replace B1_TIPODEC		with "N"
								Replace B1_ORIGEM		with cOrigem
								Replace B1_POSIPI		with cPosIPI
								Replace B1_GRADE		with "S"
								Replace B1_RASTRO		with cRastroBD
								Replace B1_LOCALIZ		with cEndBD
								Replace B1_VEREAN		with "13"
								Replace B1_TIPOBN		with cTipo
								Replace B1_GARANT		with "N"
								Replace B1_YMARCA		with cMarca
								Replace B1_XDESCMA		with cMarcaDesc
								Replace B1_YCICLO		with cCiclo
								Replace B1_YDESCIC		with cCicloDesc
								Replace B1_YGPRODU		with cGpProd
								Replace B1_YSUBGRP		with cSGrupo
								Replace B1_YDESSUB		with cSGrupoDesc
								Replace B1_YCOLECA		with cColecao
								Replace B1_YDESCOL		with cColecaoDesc
								Replace B1_YSUBCOL		with cSubCol
								//Replace B1_YDESCOL   	with POSICIONE("ZAA",1,xFilial("ZAA")+cColecao,"ZAA_DESCRI")
								Replace B1_YDESSCO   	with cSubColDesc //POSICIONE("ZAH",1,xFilial("ZAH")+cSubCol,"ZAH_DESCRI")
								Replace B1_YTPPROD		with cTpProd
								Replace B1_YCATEGO		with cCatProd
								Replace B1_YDESCAT		with cCatProdDesc
								Replace B1_YOCASIA		with cOcasiao
								Replace B1_YTECIDO		with cTecido
								//Replace B1_YDATCAD		with Date() //dDtCad
								Replace B1_YDWEB		with cWebBD
//							Replace B1_YTEMPO		with cTempo
								Replace B1_YCLASPR		with cClass
								Replace B1_SITTRIB		with cTrib
								Replace B1_YDESCPT		with cDescriPTB
								Replace B1_YDESCES		with cDescriSPA
								Replace B1_YDESCIE		with cDescriENG
								Replace B1_YOBS			with cObsProd
//							Replace B1_YDATLIB		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//							Replace B1_YSITUAC		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
								Replace B1_YLAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]

								If cTipo = "ME" .AND. cOrigem = "0"
									Replace B1_GRTRIB with "006"
								ElseIf cOrigem = "1"
									Replace B1_GRTRIB with "006"
								ElseIf cOrigem = "0" .AND. cTipo <> "ME"
									Replace B1_GRTRIB with "007"
								EndIf

								If cTipo = "ME"
									Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
								Else
									Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTACUS")
								EndIf
								Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")

								If cTipo $ "MP|MI|PF|PL|PI|KT|ME"
									Replace B1_MRP			with "S"
								EndIf

								//Replace B1_XCODCIC		with oMSSta:ACOLS[i,9]
								//Replace B1_XDSCCIC		with oMSSta:ACOLS[i,10]

								MsUnLock()
								U_HPDISPLJ(SB1->B1_COD)
							EndIf
						EndIf
					Next
				EndIf
			Next
		End Transaction
	ElseIf _tp == 2

		//CHKTAM(cCodigo,cColuna)

		Begin Transaction
			DbSelectArea("SB4")
			DbSetOrder(1)
			DbSeek(xfilial("SB4")+cCodigo)

			If Found()
				RecLock("SB4",.F.)
				Replace B4_FILIAL 		with xfilial("SB4")
				Replace B4_COD			with Alltrim(cCodigo)
				Replace B4_DESC			with AllTrim(cCodigoDesc)
				Replace B4_TIPO			with cTipo
				Replace B4_YORIGEM		with cOrigem
				Replace B4_YCLASPR		with cClass
				Replace B4_GRUPO		with cGrupo
				Replace B4_DESC			with cCodigoDesc
				Replace B4_UM			with cUM
				Replace B4_YMARCA		with cMarca
				Replace B4_YNOMARC		with cMarcaDesc
				Replace B4_YCICLO		with cCiclo
				Replace B4_YNCICLO		with cCicloDesc
				Replace B4_YGPRODU		with cGpProd
				Replace B4_YNGPCOM		with cGpProdDesc
				Replace B4_YSUBGRP		with cSGrupo
				Replace B4_YNSUBGR		with cSGrupoDesc
				Replace B4_YCOLECA		with cColecao
				Replace B4_YNCOLEC		with cColecaoDesc
				Replace B4_YSUBCOL		with cSubCol
				Replace B4_YNSUBCO		with cSubColDesc
				Replace B4_YTPPROD		with cTpProd
				Replace B4_YNTPCOM		with cTpProdDesc
				Replace B4_YCATEGO		with cCatProd
				Replace B4_YNCATEG		with cCatProdDesc
				Replace B4_YOCASIA		with cOcasiao
				Replace B4_YTECIDO		with cTecido
				Replace B4_LOCPAD		with cArm
				Replace B4_RASTRO		with cRastroBD
				Replace B4_LOCALIZ		with cEndBD
				Replace B4_LINHA		with cLinha
				Replace B4_COLUNA		with cColuna
				Replace B4_PESO			with nPeso
				Replace B4_POSIPI		with cPosIPI
				Replace B4_YDATCAD		with dDtCad
				Replace B4_DWEB			with cWebBD
//			Replace B4_YTEMPO		with cTempo
				Replace B4_SITTRIB		with cTrib
				Replace B4_YDESCPT		with cDescriPTB
				Replace B4_YDESCES		with cDescriSPA
				Replace B4_YDESCIE		with cDescriENG
				Replace B4_YOBS			with cObsProd
				MsUnLock()

				For i := 1 to Len(OMSGrade:ACOLS)
					If OMSGrade:ACOLS[i,len(OMSGrade:AHEADER)+1] == .F. .AND. AllTrim(OMSGrade:ACOLS[i,1])<>""
						DbSelectArea("SZD")
						DbSetOrder(1)
						DbSeek(xfilial("SZD")+SubString(cCodigo,1,15)+OMSGrade:ACOLS[i,1])

						If !Found()
							RecLock("SZD",.T.)
							Replace ZD_FILIAL	with xfilial("SZD")
							Replace ZD_PRODUTO		with SubString(Alltrim(cCodigo),1,15)
							Replace ZD_COR			with OMSGrade:ACOLS[i,1]
							Replace ZD_DESCRIC		with OMSGrade:ACOLS[i,2]
							Replace ZD_COMPOS		with OMSGrade:ACOLS[i,3]
							Replace ZD_DESCCP2		with OMSGrade:ACOLS[i,4]
							For w := 5 to len(OMSGrade:aHeader)-2
								//_cpo := "ZD_TAM"+StrZero((w-4),3)
								_cpo := "SZD->"+OMSGrade:aHeader[w][2]
								Replace &(_cpo) with OMSGrade:ACOLS[i,w]
							Next
//						Replace ZD_DTLIB			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//						Replace ZD_SITUAC			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
							Replace ZD_DESCLAV			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-1]
							Replace ZD_LAVAGE			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
							MsUnLock()
						Else
							RecLock("SZD",.F.)
							Replace ZD_COMPOS			with OMSGrade:ACOLS[i,3]
							Replace ZD_DESCCP2			with OMSGrade:ACOLS[i,4]
							For w := 5 to len(OMSGrade:aHeader)-2
								//	_cpo := "SZD->ZD_TAM"+StrZero((w-4),3)
								_cpo := "SZD->"+OMSGrade:aHeader[w][2]
								Replace &(_cpo) with OMSGrade:ACOLS[i,w]
								If OMSGrade:ACOLS[i,w] = " " .AND. &(_cpo) = "X"
									_tam := PADL(Alltrim(OMSGrade:aHeader[w][1]),4,"0") //rettam(w-4,cColuna,"C")
									_Dtam := OMSGrade:aHeader[w][1] //rettam(w-4,cColuna,"D")
									If SELECT("TMPSG1") > 0
										TMPSG1->(DbCloseArea())
									EndIf

									cQry := " SELECT TOP 1 * FROM "+RetSqlName("SG1")+" WITH(NOLOCK) WHERE G1_COD = '"+Alltrim(AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)+"'  AND D_E_L_E_T_ = '' "
									cQry += " UNION "
									cQry += " SELECT TOP 1 * FROM "+RetSqlName("SG1")+" WITH(NOLOCK) WHERE G1_COMP = '"+Alltrim(AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)+"'  AND D_E_L_E_T_ = '' "

									dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPSG1",.T.,.T.)
									dbSelectArea("TMPSG1")

									If !EMPTY(TMPSG1->G1_COD)
										Msginfo("O Produto "+Alltrim(AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)+" n�o poder� ter seu tamanho "+Alltrim(_tam)+"  excluido da Grade, pois existe Estrutura ","HOPE")
									Else
										Replace &(_cpo) with OMSGrade:ACOLS[i,w]
									EndIf

								EndIf
							Next x
//						Replace ZD_DTLIB			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//						Replace ZD_SITUAC			with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
							Replace SZD->ZD_DESCLAV		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-1]
							Replace SZD->ZD_LAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
							MsUnLock()
						EndIf

						For w := 5 to len(OMSGrade:aHeader)-2
							If OMSGrade:ACOLS[i,w] == "X"
								//_tam := PADL(Alltrim(OMSGrade:aHeader[w][1]),4,"0") //rettam(w-4,cColuna,"C")
								_tam := rettam5(cColuna,Alltrim(OMSGrade:aHeader[w][1]))

								_Dtam := OMSGrade:aHeader[w][1] //rettam(w-4,cColuna,"D")
								DbSelectArea("SB1")
								DbSetOrder(1)
								DbSeek(xfilial("SB1")+AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)

								If !Found()
									RecLock("SB1",.T.)
									Replace B1_FILIAL			with xfilial("SB1")
									Replace B1_COD			with AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam
									Replace B1_DESC			with AllTrim(cCodigoDesc)+" - "+AllTrim(OMSGrade:ACOLS[i,2])+" - "+_Dtam
									Replace B1_TIPO			with cTipo
									Replace B1_GRUPO		with cGrupo
									Replace B1_UM			with cUM
									Replace B1_LOCPAD		with cArm
									Replace B1_SEGUM		with cUM
									Replace B1_CONV			with 1
									Replace B1_TIPCONV		with "M"
									Replace B1_PESO			with nPeso
									Replace B1_TIPODEC		with "N"
									Replace B1_ORIGEM		with cOrigem
									Replace B1_REVATU       with "001"
									Replace B1_POSIPI		with cPosIPI
									Replace B1_GRADE		with "S"
									Replace B1_RASTRO		with cRastroBD
									Replace B1_LOCALIZ		with cEndBD
									Replace B1_VEREAN		with "13"
									Replace B1_TIPOBN		with cTipo
									Replace B1_GARANT		with "N"
									Replace B1_YMARCA		with cMarca
									Replace B1_XDESCMA		with cMarcaDesc
									Replace B1_YCICLO		with cCiclo
									Replace B1_YDESCIC		with cCicloDesc
									Replace B1_YGPRODU		with cGpProd
									Replace B1_YSUBGRP		with cSGrupo
									Replace B1_YDESSUB		with cSGrupoDesc
									Replace B1_YCOLECA		with cColecao
									Replace B1_YDESCOL		with cColecaoDesc
									Replace B1_YSUBCOL		with cSubCol
									Replace B1_YDESSCO		with cSubColDesc
									Replace B1_YTPPROD		with cTpProd
									Replace B1_YCATEGO		with cCatProd
									Replace B1_YDESCAT		with cCatProdDesc
									Replace B1_YOCASIA		with cOcasiao
									Replace B1_YTECIDO		with cTecido
									Replace B1_YDATCAD		with Date() //dDtCad
									Replace B1_YDWEB		with cWebBD
//								Replace B1_YTEMPO		with cTempo
									Replace B1_YCLASPR		with cClass
									Replace B1_SITTRIB		with cTrib
									Replace B1_YDESCPT		with cDescriPTB
									Replace B1_YDESCES		with cDescriSPA
									Replace B1_YDESCIE		with cDescriENG
									Replace B1_YOBS			with cObsProd
//								Replace B1_YDATLIB		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
									If Alltrim(cTipo) $ GetMv("HP_TIPOPRD")
										Replace B1_YSITUAC		with "PDSV" // Na inclusao grava Como produto em desenvolvimento
									Else
										Replace B1_YSITUAC		with "PNV"
									EndIf

									Replace B1_YLAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]
									Replace B1_OPERPAD		with "01"
									Replace B1_MSBLQL		with "2"

									If cTipo = "ME" .AND. cOrigem = "0"
										Replace B1_GRTRIB with "006"
									ElseIf cOrigem = "1"
										Replace B1_GRTRIB with "006"
									ElseIf cOrigem = "0" .AND. cTipo <> "ME"
										Replace B1_GRTRIB with "007"
									EndIf

									If cTipo = "ME"
										Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
									Else
										Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTACUS")
									EndIf

									Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")

									If cTipo $ "MP|MI|PF|PL|PI|KT|ME"
										Replace B1_MRP			with "S"
									EndIf

									If cTipo $ ("PF|KT|ME")
										Replace B1_XCODCIC		with cParCicPad
										Replace B1_XDSCCIC		with cDscCicPad
									EndIf

									MsUnLock()
									U_HPDISPLJ(SB1->B1_COD)
								Else
									RecLock("SB1",.F.)
									Replace B1_FILIAL		with xfilial("SB1")
									Replace B1_DESC			with AllTrim(cCodigoDesc)+" - "+AllTrim(OMSGrade:ACOLS[i,2])+" - "+_dtam
									Replace B1_TIPO			with cTipo
									Replace B1_GRUPO		with cGrupo
									Replace B1_UM			with cUM
									Replace B1_LOCPAD		with cArm
//								Replace B1_SEGUM		with cUM
//								Replace B1_CONV			with 1
									Replace B1_TIPCONV		with "M"
									Replace B1_PESO			with nPeso
									Replace B1_TIPODEC		with "N"
									Replace B1_ORIGEM		with cOrigem
									Replace B1_POSIPI		with cPosIPI
									Replace B1_GRADE		with "S"
									Replace B1_RASTRO		with cRastroBD
									Replace B1_LOCALIZ		with cEndBD
									Replace B1_VEREAN		with "13"
									Replace B1_TIPOBN		with cTipo
									Replace B1_GARANT		with "N"
									Replace B1_YMARCA		with cMarca
									Replace B1_XDESCMA		with cMarcaDesc
									Replace B1_YCICLO		with cCiclo
									Replace B1_YDESCIC		with cCicloDesc
									Replace B1_YGPRODU		with cGpProd
									Replace B1_YSUBGRP		with cSGrupo
									Replace B1_YDESSUB		with cSGrupoDesc
									Replace B1_YCOLECA		with cColecao
									Replace B1_YDESCOL		with cColecaoDesc
									Replace B1_YSUBCOL		with cSubCol
									Replace B1_YDESSCO		with cSubColDesc
									Replace B1_YTPPROD		with cTpProd
									Replace B1_YCATEGO		with cCatProd
									Replace B1_YDESCAT		with cCatProdDesc
									Replace B1_YOCASIA		with cOcasiao
									Replace B1_YTECIDO		with cTecido
									//Replace B1_YDATCAD		with dDtCad
									Replace B1_YDWEB		with cWebBD
//								Replace B1_YTEMPO		with cTempo
									Replace B1_YCLASPR		with cClass
									Replace B1_SITTRIB		with cTrib
									Replace B1_YDESCPT		with cDescriPTB
									Replace B1_YDESCES		with cDescriSPA
									Replace B1_YDESCIE		with cDescriENG
									Replace B1_YOBS			with cObsProd
//								Replace B1_YDATLIB		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-3]
//								Replace B1_YSITUAC		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)-2]
									Replace B1_YLAVAGE		with OMSGrade:ACOLS[i,len(OMSGrade:aHeader)]

									If cTipo = "ME" .AND. cOrigem = "0"
										Replace B1_GRTRIB with "006"
									ElseIf cOrigem = "1"
										Replace B1_GRTRIB with "006"
									ElseIf cOrigem = "0" .AND. cTipo <> "ME"
										Replace B1_GRTRIB with "007"
									EndIf

									If cTipo = "ME"
										Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")
									Else
										Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTACUS")
									EndIf

									Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_CTADES")

									If cTipo $ "MP|MI|PF|PL|PI|KT|ME"
										Replace B1_MRP			with "S"
									EndIf

									//Replace B1_XCODCIC		with cParCicPad
									//Replace B1_XDSCCIC		with cDscCicPad

									MsUnLock()

									// ATUALIZAR DESCRI��O CAD PRODXFOR INICIO
									ccodP := Alltrim(SB1->B1_COD)
									cdescP := Alltrim(SB1->B1_DESC)

									AdesProd(ccodP,cdescP)

									ccodP := ""
									cdescP := ""
									// ATUALIZAR DESCRI��O CAD PRODXFOR FIM

									U_HPDISPLJ(SB1->B1_COD)
								EndIf
							EndIf
						Next
					Else
						DbSelectArea("SZD")
						DbSetOrder(1)
						DbSeek(xfilial("SZD")+SubString(cCodigo,1,15)+OMSGrade:ACOLS[i,1])
						// TODO Valida��o se a referencia existe na estrutura. Weskley Silva 29.01.2019
						If SELECT("TMPSG1") > 0
							TMPSG1->(DbCloseArea())
						EndIf

						cQry := " SELECT TOP 1 * FROM "+RetSqlName("SG1")+" WITH(NOLOCK) WHERE LEFT(G1_COD,11) = '"+SubString(alltrim(cCodigo),1,15)+OMSGrade:ACOLS[i,1]+"'  AND D_E_L_E_T_ = '' "
						cQry += " UNION "
						cQry += " SELECT TOP 1 * FROM "+RetSqlName("SG1")+" WITH(NOLOCK) WHERE LEFT(G1_COMP,11) = '"+SubString(alltrim(cCodigo),1,15)+OMSGrade:ACOLS[i,1]+"'  AND D_E_L_E_T_ = '' "

						dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPSG1",.T.,.T.)
						dbSelectArea("TMPSG1")

						If !EMPTY(TMPSG1->G1_COD)
							Msginfo(" A refencia "+SubString(alltrim(cCodigo),1,15)+" na cor "+OMSGrade:ACOLS[i,1]+" possui estrutura e n�o poder� ser excluida!! ","HOPE")
						Else

							If Found()
								RecLock("SZD",.F.)
								DbDelete()
								MsUnLock()
							EndIf
						EndIf
					EndIf
				Next
			EndIf
		End Transaction
	ElseIf _tp == 7
		cCodBar	:= ""
		cCodBarMV	:= ""
		For i := 1 to Len(OMSGrade:ACOLS)
			If OMSGrade:ACOLS[i,len(OMSGrade:AHEADER)+1] == .F.
				For w := 3 to len(OMSGrade:aHeader)
					If alltrim(OMSGrade:ACOLS[i,w]) <> ""
						_tam := rettam2(AllTrim(OMSGrade:AHEADER[w,1]),cColuna,"C")
						DbSelectArea("SB1")
						DbSetOrder(1)
						DbSeek(xfilial("SB1")+AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam)

						_codbar := alltrim(OMSGrade:ACOLS[i,w]) + cValToChar(dig13(alltrim(OMSGrade:ACOLS[i,w])))

						If Found() .AND. alltrim(SB1->B1_CODBAR) == ""
							DbSelectArea("SB1")
							DbSetOrder(5)								// VALIDACAO ANTI-DUPLICIDADE DO COD DE BARRAS 12/08/21 GEYSON ALBANO
							If !DbSeek(xfilial("SB1")+AllTrim(_codbar)) // VALIDO SE O C�DIGO DE BARRAS J� EXISTE NO BANCO

								DbSelectArea("SB1")
								DbSetOrder(1)
								DbSeek(xfilial("SB1")+AllTrim(cCodigo)+OMSGrade:ACOLS[i,1]+_tam) // SE N�O EXISTIR POSICIONO NOVAMENTE NO REGISTRO A SER ALTERADO

								RecLock("SB1",.F.)
								Replace B1_CODBAR		with _codbar
								MsUnLock()
							Else
								MessageBox("C�digo de barras j� existente, favor refa�a a opera��o!!!","EXISTCODBAR",16)
								lCodBar := .F.
								Return .F. // CASO NO MOMENTO DA GRAVA��O ELE ECONTRE ALGUM COD DE BARRAS EXISTENTE ELE PARA E PEDE PRO USU�RIO REFAZER A OPERA��O.
							EndIf

							If cCodBarMV < alltrim(OMSGrade:ACOLS[i,w])
								cCodBarMV := alltrim(OMSGrade:ACOLS[i,w])
							EndIf
						EndIf
					EndIf
				Next
			EndIf
		Next
		/*
		If cCodBarMV <> ""
			PutMv ("MV_HCODBAR",cCodBarMV)
		EndIf
		*/
		lCodBar := .F.
	ElseIf _tp = 8 .or. _tp = 9
		For i := 1 to Len(oMSGrade:ACOLS)
			If oMSGrade:ACOLS[i,len(oMSGrade:AHEADER)+1] == .F.
				DbSelectArea("SZH")
				DbSetOrder(1)
				DbSeek(xfilial("SZH")+SubString(cCodigo,1,15))

				If !Found()
					RecLock("SZH",.T.)
					Replace ZH_FILIAL			with 	xfilial("SZD")
					Replace ZH_PRODUTO		with SubString(cCodigo,1,15)
				Else
					RecLock("SZH",.F.)
				EndIf
				For w := 1 to len(oMSGrade:aHeader)
					_cpo := oMSGrade:AHEADER[w,2]
					Replace &(_cpo)		with oMSGrade:ACOLS[i,w]
				Next
				MsUnLock()
			EndIf
		Next
	ElseIf _tp = 11 .or. _tp = 12
		For i := 1 to Len(oMSGrade:ACOLS)
			If oMSGrade:ACOLS[i,len(oMSGrade:AHEADER)+1] == .F.
				DbSelectArea("SZN")
				DbSetOrder(1)
				DbSeek(xfilial("SZN")+PADR(cCodigo,15)+oMSGrade:ACOLS[i,1])

				If !Found()
					RecLock("SZN",.T.)
					Replace ZN_FILIAL		with xfilial("SZN")
					Replace ZN_PRODUTO		with SubString(cCodigo,1,15)
					Replace ZN_PARTES		with oMSGrade:ACOLS[i,1]
				Else
					RecLock("SZN",.F.)
					Replace ZN_PARTES		with oMSGrade:ACOLS[i,1]
				EndIf
				For w := 2 to len(oMSGrade:aHeader)
					_cpo := oMSGrade:AHEADER[w,2]
					Replace &(_cpo)		with oMSGrade:ACOLS[i,w]
				Next
				MsUnLock()
			Else
				DbSelectArea("SZN")
				DbSetOrder(1)
				DbSeek(xfilial("SZN")+PADR(cCodigo,15)+oMSGrade:ACOLS[i,1])

				If Found()
					Reclock("SZN")
					DbDelete()
					MsUnLock()
				EndIf
			EndIf
		Next
	ElseIf _tp = 14

		DbSelectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+cCodigo)

		If Found()
			RecLock("SB4",.F.)
			Replace B4_YTEMPO			with ALLtRIM(cTempRef)
		EndIf

		For i := 1 to Len(oMSGrade:ACOLS)

			If len(oMSGrade:ACOLS[i]) == 11
				If oMSGrade:ACOLS[i,len(oMSGrade:AHEADER)+2] == .F.

					DbSelectArea("SZD")
					DbSetOrder(1)
					DbSeek(xfilial("SZD")+PADR(cCodigo,15)+oMSGrade:ACOLS[i,1])

					RecLock("SZD",.F.)
					If SZD->ZD_TEMPO2 > 0 .AND. SZD->ZD_TEMPO3 != ZD_TEMPO2	.AND. SZD->ZD_TEMPO != oMSGrade:ACOLS[i,3]
						SZD->ZD_TEMPO3 	 :=			ZD_TEMPO2
						SZD->ZD_USRALT3  :=			ZD_USRALT2
					EndIf
					If SZD->ZD_TEMPO1 > 0 .AND. SZD->ZD_TEMPO2 != ZD_TEMPO1	.AND. SZD->ZD_TEMPO != oMSGrade:ACOLS[i,3]
						SZD->ZD_TEMPO2 	 :=			ZD_TEMPO1
						SZD->ZD_USRALT2	 :=			ZD_USRALT1
					EndIf
					If SZD->ZD_TEMPO > 0 .AND. SZD->ZD_TEMPO != oMSGrade:ACOLS[i,3]
						SZD->ZD_TEMPO1	 :=			ZD_TEMPO
						SZD->ZD_USRALT1	 :=			Alltrim(SUBSTR(UsrRetName(__cUserID),1,20))
					EndIf
					If SZD->ZD_TEMPO != oMSGrade:ACOLS[i,3]
						Replace ZD_TEMPO 			with oMSGrade:ACOLS[i,3]
						lAtutmp := .T.
					EndIf
					MsUnLock()
				EndIf

			EndIf

			If lAtutmp .AND. oMSGrade:ACOLS[i,3] != 0 // S� IRA EXECUTAR UPDATE NO BANCO DE REALMENTE TIVER SIDO ALTERADO O TEMPO OU TEMPO N�O FOR 0

				_qry := "UPDATE "+RetSqlName("SG1")+" SET G1_QUANT = '"+ALLtRIM(STR(oMSGrade:ACOLS[i,3]))+"' "
				_qry += "FROM "+RetSqlName("SG1")+" SG1 "
				_qry += "	INNER JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = G1_COMP "
				_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(G1_COD,11) = '"+alltrim(cCodigo)+oMSGrade:ACOLS[i,1]+"' AND B1_TIPO = 'BN' "

				nStatus := TcSqlExec(_qry)

				If (nStatus < 0)
					MsgBox(TCSQLError(), "Erro no comando", "Stop")
				EndIf

				DbSelectArea("SG1")
				DbSetOrder(1)
				DbGoTop()
				DbSeek(xfilial("SG1")+ALLTRIM(cCodigo)+ALLTRIM(oMSGrade:ACOLS[i,1]))

				While !EOF() .AND. SubStr(SG1->G1_COD,1,11) == ALLTRIM(cCodigo)+ALLTRIM(oMSGrade:ACOLS[i,1])
					DbSelectArea("SB1")
					DbSetOrder(1)
					Dbseek(xfilial("SB1")+SG1->G1_COMP)

					If SB1->B1_TIPO = "BN"

						DbSelectArea("SB4")
						DbSetOrder(1)
						Dbseek(xfilial("SB4")+SubStr(SG1->G1_COD,1,8))

						_wtam := rettam3(SB4->B4_COLUNA,SubStr(SG1->G1_COD,12,4))
						_campo:= "ZF_TAM"+_wtam
						_campo2:= "ZB1_TAM"+_wtam // TABELA DE BKP ESTRUTURA

						DbSelectarea("SG5")
						DbSetOrder(1)
						DbSeek(xfilial("SG5")+alltrim(SB4->B4_COD))

						If Found()
							_cod := SG5->G5_PRODUTO
							While !EOF() .AND. SG5->G5_PRODUTO == _cod
								DbSelectArea("SZF")
								DbSetOrder(2)
								DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG5->G5_REVISAO+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))

								If Found() .AND. &_campo <> SG1->G1_QUANT
									RecLock("SZF",.F.)
									Replace &_campo			with SG1->G1_QUANT
									MsUnLock()
								EndIf
								DbSelectArea("SG5")
								DbSkip()
							EndDo
						Else
							DbSelectArea("SZF")
							DbSetOrder(2)
							DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))

							If Found() .AND. &_campo <> SG1->G1_QUANT
								RecLock("SZF",.F.)
								Replace &_campo			with SG1->G1_QUANT
								MsUnLock()
							EndIf

						EndIf
						// ATUALIZACAO TABELA BKP INICIO
						If Found()
							_cod := SG5->G5_PRODUTO
							While !EOF() .AND. SG5->G5_PRODUTO == _cod
								DbSelectArea("ZB1")
								DbSetOrder(2)
								DbSeek(xfilial("ZB1")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
								If Found() .AND. &_campo2 <> SG1->G1_QUANT
									RecLock("ZB1",.F.)
									Replace &_campo2			with SG1->G1_QUANT
									MsUnLock()
								EndIf
								DbSelectArea("SG5")
								DbSkip()
							EndDo
						Else
							DbSelectArea("ZB1")
							DbSetOrder(2)
							DbSeek(xfilial("ZB1")+Padr(SubStr(SG1->G1_COD,1,8),15)+"001"+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))

							If Found() .AND. &_campo2 <> SG1->G1_QUANT
								RecLock("ZB1",.F.)
								Replace &_campo2			with SG1->G1_QUANT
								MsUnLock()
							EndIf

						EndIf
						//ATUALIZACAO TABELA BKP FIM
					EndIf
					DbSelectArea("SG1")
					DbSkip()
				EndDo
			EndIf
		Next
	ElseIf _tp = 15 .or. _tp = 16 // TODO SE FOR OPCIONAIS INICIO
		For i := 1 to Len(oMSOPC:ACOLS)
			If oMSOPC:ACOLS[i,len(oMSOPC:AHEADER)+1] == .F.
				DbSelectArea("ZAU")
				DbSetOrder(1) //ZAU_FILIAL+ZAU_CODREF+ZAU_GRUPO+ZAU_COR+ZAU_ITEM+ZAU_PRODUT
				DbSeek(xfilial("ZAU")+alltrim(cCodigo)+oMSOPC:ACOLS[i,3]+oMSOPC:ACOLS[i,1]+oMSOPC:ACOLS[i,4]+oMSOPC:ACOLS[i,2])
				If !Found()
					RecLock("ZAU",.T.)
					Replace ZAU_FILIAL			with xfilial("ZAU")
					Replace ZAU_CODREF			with ALLTRIM(cCodigo)
					Replace ZAU_COR				with oMSOPC:ACOLS[i,1]
					Replace ZAU_PRODUT			with oMSOPC:ACOLS[i,2]
					Replace ZAU_GRUPO			with oMSOPC:ACOLS[i,3]
					Replace ZAU_ITEM			with oMSOPC:ACOLS[i,4]
				Else
					RecLock("ZAU",.F.)
					Replace ZAU_COR				with oMSOPC:ACOLS[i,1]
					Replace ZAU_PRODUT			with oMSOPC:ACOLS[i,2]
					Replace ZAU_GRUPO			with oMSOPC:ACOLS[i,3]
					Replace ZAU_ITEM			with oMSOPC:ACOLS[i,4]
				EndIf
				MsUnLock()

				cProGA:= PADR(SubStr(oMSOPC:ACOLS[i,2],1,8)+" - "+SubStr(oMSOPC:ACOLS[i,2],9,LEN(oMSOPC:ACOLS[i,2])),30,"")

				DbSelectArea("SGA")
				DbSetOrder(2) // GA_FILIAL+GA_GROPC+GA_DESCGRP+GA_OPC+GA_DESCOPC
				DbSeek(xfilial("SGA")+oMSOPC:ACOLS[i,3]+PadR(cCodigo,30,"")+oMSOPC:ACOLS[i,4]+cProGA)

				If !Found()
					RecLock("SGA",.T.)
					Replace GA_FILIAL		with xfilial("SGA")
					Replace GA_GROPC		with oMSOPC:ACOLS[i,3]
					Replace GA_DESCGRP		with ALLTRIM(cCodigo)
					Replace GA_OPC			with oMSOPC:ACOLS[i,4]
					Replace GA_DESCOPC		with SubStr(oMSOPC:ACOLS[i,2],1,8)+" - "+SubStr(oMSOPC:ACOLS[i,2],9,3)
					MsUnLock()
				EndIf

				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+ALLTRIM(cCodigo)+oMSOPC:ACOLS[i,1])

				While !EOF() .AND. SubStr(SB1->B1_COD,1,11) == ALLTRIM(cCodigo)+oMSOPC:ACOLS[i,1]
					DbSelectArea("SG1")
					DbSetOrder(1)
					DbSeek(Xfilial("SG1")+SB1->B1_COD+SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4)+oMSOPC:ACOLS[i,3])

					If !Found() .AND. len(alltrim(SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4))) = 15

						cqB1:= "SELECT * FROM "+RetSqlName("SB1")+" (NOLOCK) WHERE B1_COD = '"+SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4)+"' "
						cqB1+= " AND D_E_L_E_T_ = '' "

						If SELECT("TMPSB1") > 0
							TMPSB1->(DbCloseArea())
						EndIf
						dbUseArea(.T.,"TOPCONN",TcGenQry(,,cqB1),"TMPSB1",.T.,.T.)

						If TMPSB1->(!Eof())

							_qry := "Insert into "+RetSqlName("SG1")+" (G1_FILIAL, G1_COD, G1_COMP, G1_TRT, G1_QUANT, G1_INI, G1_FIM, G1_NIV, G1_NIVINV, G1_REVINI,"
							_qry += "G1_REVFIM, G1_GROPC, G1_OPC, R_E_C_N_O_) Values ('"+xfilial("SG1")+"','"+SB1->B1_COD+"','"
							_qry += SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4)+"','"+oMSOPC:ACOLS[i,3]+"',1,'19500101',"
							_qry += "'20491231', '01', '99', '001', '001','"+oMSOPC:ACOLS[i,3]+"','"+oMSOPC:ACOLS[i,4]+"',(SELECT max(R_E_C_N_O_)+1 FROM SG1010))"

							nStatus := TcSqlExec(_qry)

							If (nStatus < 0)
								MsgBox(TCSQLError(), "Erro no comando", "Stop")
							EndIf
						Else
							MessageBox("O produto "+SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4)+ " n�o est� cadastrado na tabela de produtos !","SEMB1",16)
							return (.F.)
						EndIf

					EndIf
					SB1->(DbSkip())
				EndDo
			Else // DELETANDO OPCIONAIS
				DbSelectArea("ZAU")
				DbSetOrder(1) //ZAU_FILIAL+ZAU_CODREF+ZAU_GRUPO+ZAU_COR+ZAU_ITEM+ZAU_PRODUT
				DbSeek(xfilial("ZAU")+alltrim(cCodigo)+oMSOPC:ACOLS[i,3]+oMSOPC:ACOLS[i,1]+oMSOPC:ACOLS[i,4]+oMSOPC:ACOLS[i,2])
				If Found()
					RecLock("ZAU",.F.)
					DBDELETE()
					MsUnLock()
				EndIf

				DbSelectArea("SGA")
				DbSetOrder(1)
				DbSeek(xfilial("SGA")+oMSOPC:ACOLS[i,3]+oMSOPC:ACOLS[i,4])

				If Found()
					RecLock("SGA",.F.)
					DBDELETE()
					MsUnLock()
				EndIf

				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+ALLTRIM(cCodigo)+oMSOPC:ACOLS[i,1])

				While !EOF() .AND. SubStr(SB1->B1_COD,1,11) = ALLTRIM(cCodigo)+oMSOPC:ACOLS[i,1]

					DbSelectArea("SG1")
					DbSetOrder(1)
					DbSeek(Xfilial("SG1")+SB1->B1_COD+SubStr(oMSOPC:ACOLS[i,2],1,11)+SubStr(SB1->B1_COD,12,4)+oMSOPC:ACOLS[i,3])

					If Found()
						RecLock("SG1",.F.)
						DBDELETE()
						MsUnLock()
					EndIf
					DbSelectArea("SB1")
					DbSkip()
				EndDo
			EndIf
		Next
		lLmpOpc := .F.
		// TODO SE FOR OPCIONAIS FIM
	ElseIf _tp = 10
		For i := 1 to Len(oMSSta:ACOLS)
			If oMSSta:ACOLS[i,len(oMSSta:AHEADER)+1] == .F.
				DbSelectArea("SZL")
				DbSetOrder(1)
				DbSeek(xfilial("SZL")+oMSSta:ACOLS[i,1]+oMSSta:ACOLS[i,2]+oMSSta:ACOLS[i,4])

				If !Found()
					RecLock("SZL",.T.)
					Replace ZL_FILIAL		with xfilial("SB1")
					Replace ZL_PRODUTO		with oMSSta:ACOLS[i,1]
					Replace ZL_COR			with oMSSta:ACOLS[i,2]
					Replace ZL_TAM			with oMSSta:ACOLS[i,4]
					Replace ZL_BLQDEM		with oMSSta:ACOLS[i,5]
					Replace ZL_STATUS		with ALLTRIM(oMSSta:ACOLS[i,6])
					Replace ZL_DTLIBER		with oMSSta:ACOLS[i,7]
					Replace ZL_DTENCER		with oMSSta:ACOLS[i,8]
					If cTipo $ ("PF|KT|ME")
						Replace ZL_CODCIC		with cParCicPad //oMSSta:ACOLS[i,9]
						Replace ZL_DSCCIC		with cDscCicPad //oMSSta:ACOLS[i,10]
						Replace ZL_CODEST		with oMSSta:ACOLS[i,11]
						Replace ZL_DSCEST		with oMSSta:ACOLS[i,12]
					EndIf
				Else
					RecLock("SZL",.F.)
					Replace ZL_BLQDEM		with oMSSta:ACOLS[i,5]
					Replace ZL_STATUS		with ALLTRIM(oMSSta:ACOLS[i,6])
					Replace ZL_DTLIBER		with oMSSta:ACOLS[i,7]
					Replace ZL_DTENCER		with oMSSta:ACOLS[i,8]
					If cTipo $ ("PF|KT|ME")
						Replace ZL_CODCIC		with oMSSta:ACOLS[i,9]
						Replace ZL_DSCCIC		with oMSSta:ACOLS[i,10]
						Replace ZL_CODEST		with oMSSta:ACOLS[i,11]
						Replace ZL_DSCEST		with oMSSta:ACOLS[i,12]
					EndIf
				EndIf
				MsUnLock()

				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+oMSSta:ACOLS[i,1]+oMSSta:ACOLS[i,2]+oMSSta:ACOLS[i,4])
				If Found()
					RecLock("SB1",.F.)
					Replace B1_YBLQDEM		with oMSSta:ACOLS[i,5]
					Replace B1_YSITUAC		with ALLTRIM(oMSSta:ACOLS[i,6])
					Replace B1_YDATLIB		with oMSSta:ACOLS[i,7]
					Replace B1_YDATENC		with oMSSta:ACOLS[i,8]
					If cTipo $ ("PF|KT|ME")
						Replace B1_XCODCIC		with oMSSta:ACOLS[i,9]
						Replace B1_XDSCCIC		with oMSSta:ACOLS[i,10]
						Replace B1_XCODEST		with oMSSta:ACOLS[i,11]
						Replace B1_XDSCEST		with oMSSta:ACOLS[i,12]
					EndIf
					MsUnLock()
					U_HPDISPLJ(SB1->B1_COD)
				EndIf
			EndIf
		Next
		U_HPCPP016()
	EndIf

	oDlg:End()

Return

Static Function rettam(ordtam,xcoluna,_tp)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
	_BVTAM := ""

	cQuery  := "SELECT TOP "+AllTrim(str(ordtam))+" BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
	cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' "
	cQuery += "ORDER BY R_E_C_N_O_"

	cQuery := ChangeQuery(cQuery)
	If SELECT("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	DbSelectArea("TMPSBV")
	DbGoTop()
	While !EOF()
		If _TP == "C"
			_BVTAM := TMPSBV->BV_CHAVE
		Else
			_BVTAM := TMPSBV->BV_DESCRI
		EndIf
		DbSkip()
	EndDo

	DbCloseArea()

Return(_BVTAM)


Static Function rettam2(xTamSbv,xcoluna,_tp)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
	_BVTAM := ""

	cQuery  := "SELECT TOP 1 BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
	cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' AND BV_DESCRI='"+xTamSbv+"'"
	cQuery += "ORDER BY R_E_C_N_O_"

	cQuery := ChangeQuery(cQuery)
	If SELECT("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	DbSelectArea("TMPSBV")
	DbGoTop()
	While !EOF()
		If _TP == "C"
			_BVTAM := TMPSBV->BV_CHAVE
		Else
			_BVTAM := TMPSBV->BV_DESCRI
		EndIf
		DbSkip()
	EndDo

	DbCloseArea()

Return(_BVTAM)


Static Function VerCodBar(codBar)
// Rotina para verificar se o ultimo numero de codigo de barras e o correto.
//_codBar := ""

	cQuery  := "SELECT COUNT(B1_CODBAR) AS COUNT FROM "+RetSQLName("SB1")+" (nolock) "
	cQuery  += "WHERE D_E_L_E_T_='' AND B1_CODBAR LIKE '"+AllTrim(STR(codBar))+"%'"

	cQuery := ChangeQuery(cQuery)
	If SELECT("TMPSB1a") > 0
		TMPSB1a->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSB1a",.T.,.T.)
	DbSelectArea("TMPSB1a")

	If TMPSB1a->COUNT > 0

		cQuery  := " SELECT TOP 1 B1_CODBAR AS CODBAR FROM "+RetSQLName("SB1")+" (nolock) "
		cQuery  += " WHERE D_E_L_E_T_='' AND B1_CODBAR <>'' "
		cQuery  += " ORDER BY 1 DESC "

		cQuery := ChangeQuery(cQuery)
		If SELECT("TMPSB1b") > 0
			TMPSB1b->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSB1b",.T.,.T.)
		DbSelectArea("TMPSB1b")

		lRet := VAL(SubStr(AllTrim(TMPSB1b->CODBAR),1,12))+1

		TMPSB1b->(DbCloseArea())

	Else

		lRet := codBar

	EndIf

	TMPSB1a->(DbCloseArea())

Return(lRet)


Static Function hCompo()
//Criar e Visualizar composi��o

	Local oButton1
	Local oButton2
	Local xopc		:= 0
	Local cSoma	:= 0
	Local cRet		:= ""
	Local cRet2	:= ""
	Local nX
	Static oDlx

	DEFINE MSDIALOG oDlx TITLE "Composi��o" FROM 000, 000  TO 350, 700 COLORS 0, 16777215 PIXEL

	fCompo()
	@ 143, 305 BUTTON oButton1 PROMPT "Ok" SIZE 037, 012 ACTION (xopc := 1,oDlx:End()) OF oDlx PIXEL
	@ 143, 263 BUTTON oButton2 PROMPT "Cancelar" SIZE 037, 012 ACTION (xopc := 0,oDlx:End()) OF oDlx PIXEL

	ACTIVATE MSDIALOG oDlx CENTERED

	If xopc == 1
		For nX := 1 to Len(oMSCompo:aCols)
			If OMSCompo:ACOLS[nX,len(OMSCompo:AHEADER)+1] == .F. .AND. AllTrim(oMSCompo:aCols[nX,1])<>''
				If nx<>1
					cRet	+= ";"
					cRet2	+= ";"
				EndIf

				cRet  += oMSCompo:aCols[nX,1]+"-"+strzero(val(oMSCompo:aCols[nX,3]),3)
				cRet2 += AllTrim(oMSCompo:aCols[nX,2])+"-"+strzero(val(oMSCompo:aCols[nX,3]),3)
			EndIf
		Next
	ElseIf xopc == 0
		cRet	:= AllTrim(oMSGrade:aCols[oMSGrade:nat,3])
		cRet2	:= AllTrim(oMSGrade:aCols[oMSGrade:nat,4])
	EndIf

	If AllTrim(cRet) == "-000"
		cRet	:=""
		cRet2	:=""
	EndIf

/*	//retirado porque nao deve calcular o valor total da porcentagem
  	cSoma	:= 0	
	For nX := 1 to Len(oMSCompo:aCols)
		cSoma := cSoma + val(oMSCompo:aCols[nX,3])
		If cSoma > 100
			Alert("Revisar percentuais a soma ultrapassa 100%")
			Return(.F.)
		End
	Next
*/

	oMSGrade:aCols[oMSGrade:nat,3] := cRet
	oMSGrade:aCols[oMSGrade:nat,4] := cRet2
	oMsGrade:refresh()

Return

//------------------------------------------------
Static Function fCompo()
//------------------------------------------------
// Acols para composi��o
// ZAQ
// Utiliza estes campos (SF4) para n�o precisar criar uma tabela espec�fica para isso

	Local nX
	Local aHeaderEx 		:= {}
	Local aColsEx 		:= {}
	Local aFieldFill 		:= {}
	Local aFields 		:= {"F4_CODIGO","F4_TEXTO","F4_CFEXT"}
	Local aAlterFields 	:= {"F4_CODIGO","F4_CFEXT"}
	Static oMSCompo

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			If nX == 1
				Aadd(aHeaderEx, {"C�digo",SX3->X3_CAMPO,SX3->X3_PICTURE,/*TAMANHO*/5,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,"ZAQ",SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf nX == 2
				Aadd(aHeaderEx, {"Descri��o",SX3->X3_CAMPO,SX3->X3_PICTURE,/*TAMANHO*/50,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,"",SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			Else
				Aadd(aHeaderEx, {"Percentual",SX3->X3_CAMPO,SX3->X3_PICTURE,/*TAMANHO*/3,SX3->X3_DECIMAL,"VAL(M->F4_CFEXT) <= 100",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		EndIf
	Next nX

	// Define field values
	If oMSGrade:aCols[oMSGrade:nat,3] = ""
		For nX := 1 to Len(aFields)
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			EndIf
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
	Else
		For nX := 1 to len(alltrim(oMSGrade:aCols[oMSGrade:nat,3]))
			Aadd(aFieldFill, SubStr(oMSGrade:aCols[oMSGrade:nat,3],nx,5))
			Aadd(aFieldFill, POSICIONE("ZAQ",1,xFilial("ZAQ")+SubStr(oMSGrade:aCols[oMSGrade:nat,3],nx,5),"ZAQ_DESC"))
			Aadd(aFieldFill, Strzero(Val(SubStr(oMSGrade:aCols[oMSGrade:nat,3],nx+6,3)),3))
			Aadd(aFieldFill, .F.)

			Aadd(aColsEx, aFieldFill)
			aFieldFill := {}
			nX += 9
		Next
	EndIf

	oMSCompo := MsNewGetDados():New( 008, 006, 130, 344, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlx, aHeaderEx, aColsEx)

Return


Static function VldCod()
// Rotina para cria��o de c�digo autom�tico do produto

	If Alltrim(cCodigo) == ""
		DbSelectArea("SBM")
		DbSetOrder(1)
		DbSeek(xfilial("SBM")+cGrupo)

		_cod := Space(26)

		If AllTrim(cGrupo)<>""
			_ini := cTipo+SBM->BM_YCODPRO
		Else
			_ini := Space(4)
		EndIf

		If !SubStr(_ini,1,2) $ "KT|PF|PL|ME|PI|SV|MO|  "

			cQuery := "SELECT TOP 1 B4_COD FROM "+RetSqlName("SB4")+" SB4 (nolock) "
			cQuery += "WHERE SB4.D_E_L_E_T_ = '' "
			cQuery += "AND B4_COD like '"+alltrim(_ini)+"%' "
			cQuery += "order by B4_COD desc "

			cQuery := ChangeQuery(cQuery)

			If SELECT("TMP3") > 0
				TMP3->(dbclosearea())
			EndIf

			DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),"TMP3",.F.,.F.)

			DbSelectArea("TMP3")
			DbGoTop()
			If EOF()
				_cod := _ini+"0001"
			Else
				_cod := _ini+Soma1(SubStr(TMP3->B4_COD,5,4))
			EndIf

			TMP3->(dbclosearea())
		EndIf
	Else
		_cod := cCodigo
	EndIf

	cGrupoDesc		:= POSICIONE("SBM",1,xFilial("SBM")+cGrupo,"BM_DESC")
	cCodigo 		:= _cod

	If Alltrim(cGrupoDesc)==""
		oGrupo:SetFocus()
	EndIf

	oDlg:Refresh()
return()

Static function VldCod2(vCod)
// Rotina para cria��o de c�digo autom�tico do produto

	cQuery := "SELECT COUNT(B1_COD) AS COUNT_COD FROM SB1010 (nolock) WHERE D_E_L_E_T_<>'*' AND B1_COD LIKE '"+Alltrim(vCod)+"%' "

	TCQuery cQuery New Alias "QRY_08"
	DbSelectArea("QRY_08")
	QRY_08->(DbGotop())
	vCodCount	:= QRY_08->COUNT_COD
	QRY_08->(DBCloseArea())

	If vCodCount >= 1
		Alert("Este codigo de produto j� foi utilizado, favor alterar")
	ElseIf len(AllTrim(vCod)) <> 8
		Alert("Corrigir quantidade de caracteres no codigo Codigo.")
		oCodigo:SetFocus()
	EndIf

return()


Static Function dig13(_codb)
// C�lculo do d�gito verificador do C�digo de Barras EAN13

	_dig := ""
	_calc := Val(SubStr(_codb,1,1))*1 + Val(SubStr(_codb,2,1))*3 + Val(SubStr(_codb,3,1))*1 + Val(SubStr(_codb,4,1))*3 + Val(SubStr(_codb,5,1))*1 + Val(SubStr(_codb,6,1))*3 + Val(SubStr(_codb,7,1))*1 + Val(SubStr(_codb,8,1))*3 + Val(SubStr(_codb,9,1))*1 + Val(SubStr(_codb,10,1))*3 + Val(SubStr(_codb,11,1))*1 + Val(SubStr(_codb,12,1))*3
	_dig := ((Int(_calc/10)+1)*10)-_calc

	If _dig =10
		_dig :=0
	EndIf

Return(_dig)

User Function PrecPro(_tp)
// Rotina para forma��o de pre�o
	aEST := {}
	aEST2:= {}
	aEST3:= {}
	aEST4:= {}

	_xpar01 := ""
	If wperg == 0
		_xret := U_perg1()
		_xpar01 := SubStr(_xret,4,len(alltrim(_xret))-3)
		cTabela := SubStr(_xret,1,3)
		_wperg := 1
		U_prec1(_tp,_xpar01)
	Else
		U_prec1(_tp,_xpar01)
	EndIf

Return

User Function prec1(_tp,_sta)

	Local cCabecalho

	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Local oSay7
	Local oSay8

	Local oButton1
	Local oButton2

	Local oCodigo
	Local oDesc
	Local oTabela
	Local odescTab
	Local oDtDe
	Local oDtAte
	Local oVrMP
	Local oVrMO
	Local oVrMPA
	Local oVrMOA
	Local _i

	Private cCodigo 		:= SubString(SB4->B4_COD,1,15)
	Private cDesc 		:= SB4->B4_DESC
	Private cdescTab		:= Space(60)
	Private cDtDe			:= ddatabase
	Private cTipo		:= Space(10)
	Private cVrMP			:= 0
	Private cVrGeral		:= 0
	Private cVrGerAnt		:= 0
	Private cVrMO			:= 0
	Private cVrMPA		:= 0
	Private cVrMOA		:= 0
	Private cCusto		:= 0
	Private cCusA			:= 0

	Private cStatus		:= ""
	Private cStatu2		:= ""
	Private nAjuste 		:= 0
	Static oDlg1

	If _tp == 1
//		AjuSX1("HPRECO")
		_areaa := GetArea()
		_xpar01 := _sta
		For _i:=1 to len(_xPAR01) step 4
			If SubStr(_xPAR01,_i,1) <> "*"
				cStatus += "'"+SubStr(_xPAR01,_i,4)+"',"
				cStatu2 += SubStr(_xPAR01,_i,4)+"#"
			EndIf
		Next

		RestArea(_areaa)
		cStatus := SubStr(cStatus,1,len(alltrim(cStatus))-1)
		cCabecalho := "Inclus�o da Forma��o de Pre�o de Produtos"
	ElseIf  _tp == 2
		cCabecalho := "Altera��o da Forma��o de Pre�o de Produtos"
	ElseIf  _tp == 3
		cCabecalho := "Visualizar da Forma��o de Pre�o de Produtos"
	EndIf

	DEFINE MSDIALOG oDlg1 TITLE cCabecalho FROM 000, 000  TO 550, 1250 COLORS 0, 16777215 PIXEL

	@ 010, 012 SAY oSay1 PROMPT "C�digo" 				SIZE 021, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 008, 043 MSGET oCodigo VAR cCodigo 				SIZE 077, 010 OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL

	@ 010, 135 SAY oSay2 PROMPT "Descri��o" 			SIZE 025, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 008, 170 MSGET oDesc VAR cDesc 					SIZE 300, 010 OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 010, 532 SAY oSay7 PROMPT "Ajuste"				SIZE 025, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 008, 560 MSGET oAju VAR nAjuste 					SIZE 060, 010 PICTURE "@E 99.9" OF odlg1 COLORS 0, 16777215 		 	PIXEL

	@ 032, 012 SAY oSay3 PROMPT "Tabela" 				SIZE 021, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 030, 040 MSGET oTabela VAR cTabela 				SIZE 060, 010 OF odlg1 VALID !VAZIO() .AND. valTab(_tp, cTabela) COLORS 0, 16777215 	F3 "DA0" 	PIXEL

	@ 032, 115 SAY oSay4 PROMPT "Descri��o Tabela" 	SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 030, 162 MSGET odescTab VAR cdescTab 			SIZE 242, 010 OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL

	@ 032, 429 SAY oSay5 PROMPT "Data De" 			SIZE 021, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 030, 457 MSGET oDtDe VAR cDtDe	 				SIZE 060, 010 OF odlg1 VALID !VAZIO() COLORS 0, 16777215 			PIXEL
	@ 032, 532 SAY oSay6 PROMPT "Tipo" 			SIZE 021, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 030, 560 MSGET oDtAte VAR cTipo 				SIZE 060, 010 OF odlg1 VALID !VAZIO() COLORS 0, 16777215 		 	PIXEL


	@ 235, 012 SAY oSay7 PROMPT "Totais Ant:"			SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 255, 012 SAY oSay7 PROMPT "Totais Atu:"			SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 235, 044 SAY oSay8 PROMPT "MP:"					SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL

	@ 233, 065 MSGET oVrMPA VAR cVrMPA	 				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 253, 065 MSGET oVrMP VAR cVrMP	 				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 235, 155 SAY oSay8 PROMPT "MO:"					SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 233, 166 MSGET oVrMOA VAR cVrMOA 				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 253, 166 MSGET oVrMO VAR cVrMO 					SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 235, 260 SAY oSay9 PROMPT "Ajuste:"				SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL

	@ 233, 279 MSGET oCustA VAR cCusA 					SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 253, 279 MSGET oCusto VAR cCusto 				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL

//    @ 255, 279 SAY oSay10 PROMPT "MO:"				SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 235, 410 SAY oSay11 PROMPT "Total:"				SIZE 050, 007 OF odlg1 COLORS 0, 16777215 			PIXEL
	@ 233, 432 MSGET oVrGerA VAR cVrGerAnt			SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL
	@ 253, 432 MSGET oVrGer VAR cVrGeral				SIZE 060, 010 PICTURE "@E 999,999.9999" OF odlg1 COLORS 0, 16777215 READONLY 	PIXEL

	fMSPreco(_tp)
	@ 253, 520 BUTTON oButton1 PROMPT "Ok" 			SIZE 045, 012 ACTION (SavePrc(_tp),oDlg1:End()) 		OF oDlg1	PIXEL
	@ 253, 575 BUTTON oButton2 PROMPT "Cancelar" 	SIZE 045, 012 ACTION Close(oDlg1) OF oDlg1 	PIXEL

	ACTIVATE MSDIALOG oDlg1 CENTERED

Return

USer Function perg1()

	Pergunte("HPRECO")

	lret := MV_PAR02+MV_PAR01

Return(lret)
//------------------------------------------------
Static Function fMSPreco(_tp)
//------------------------------------------------
// Acols para forma��o de pre�o
// Tabela: SZI

	Local nX
	Local aHeader1 		:= {}
	Local aCols1 			:= {}
	Local aFieldFill 	:= {}
	Local aFields 		:= {"ZI_COMP","ZI_DESCCOM","ZI_UM","ZI_QTD","ZI_CANT","ZI_CATU","ZI_TOTAL","ZI_TOTANT"}
	Local aAlterFields 	:= {"ZI_CATU"}
	Local _vrmo  := 0
	Local _vrmp  := 0
	Local _vrmoa := 0
	Local _vrmpa := 0
	Local _i
	Local _t	:= 0
	Static oMSPreco

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Else
			If aFields[nX]=="ZI_DESCCOM" .AND. SX3->(DbSeek("B1_DESC"))
				Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,70,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf aFields[nX]=="ZI_UM" .AND. SX3->(DbSeek("B1_UM"))
				Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf aFields[nX]=="ZI_QTD" .AND. SX3->(DbSeek("G1_QUANT"))
				Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf aFields[nX]=="ZI_TOTAL" .AND. SX3->(DbSeek("ZI_CANT"))
				Aadd(aHeader1, {"Total ",SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			ElseIf aFields[nX]=="ZI_TOTANT" .AND. SX3->(DbSeek("ZI_CANT"))
				Aadd(aHeader1, {"Tot Ant",SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		EndIf
	Next nX

	If _tp = 1
		// Query para c�lculo de m�dia de quantidades de Mat�ria Prima por grade

		_akit := {}
		_qtdprc := 0

		If SB4->B4_TIPO = "KT"
/*		
			_xcod := "in ('"
			DbSelectArea("SG1")
			DbSetOrder(1)
			DbSeek(xfilial("SG1")+SubStr(SB4->B4_COD,1,8))
			While !EOF() .AND. SubStr(SG1->G1_COD,1,8) = SubStr(SB4->B4_COD,1,8)
				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+SG1->G1_COD)
				If SB1->B1_YSITUAC $ cStatu2
					_tem := ASCAN( _aKit, {|x| x[1] == SubStr(SG1->G1_COMP,1,8)})
					If _tem = 0
						aadd(_akit,{SubStr(SG1->G1_COMP,1,8)})
					EndIf
				EndIf
				DbSelectArea("SG1")
				DbSkip()
			End
			For _i := 1 to len(_akit)
				_xcod += _akit[_i,1]+"','"
			Next
			_xcod := SubStr(_xcod,1,len(_xcod)-2)+")"
*/
			_qry := "SELECT Count(QTD) as QTD FROM ( "
			_qry += "SELECT SB1.B1_COD as QTD FROM "+RetSqlName("SB1")+" (NOLOCK) SB1 "
			_qry += "inner join "+RetSqlName("SG1")+"(NOLOCK) SG1 on SG1.D_E_L_E_T_ = '' AND G1_COD = B1_COD AND SG1.G1_FILIAL = '"+xfilial("SG1")+"' "
			_qry += "inner join "+RetSqlName("SB1")+" (NOLOCK) SB1A on SB1A.D_E_L_E_T_ = '' AND G1_COMP = SB1A.B1_COD AND SB1A.B1_YSITUAC in ("+cStatus+") AND SB1A.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "inner join "+RetSqlName("SG1")+" (NOLOCK) SG1A on SG1A.D_E_L_E_T_ = '' AND SG1A.G1_COD = SB1A.B1_COD AND SG1A.G1_FILIAL = '"+xfilial("SG1")+"' "
			_qry += "WHERE SB1.D_E_L_E_T_ = '' AND left(SB1.B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' AND SB1.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "group by SB1.B1_COD) as X "

			If SELECT("TMPPRC") > 0
				TMPPRC->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
			DbSelectArea("TMPPRC")
			DbGoTop()
			_qtdprc := TMPPRC->QTD
			TMPPRC->(DbCloseArea())

			_qry := "SELECT SB1.B1_REVATU, SB1.B1_COD, SG1A.G1_COMP, SG1A.G1_QUANT*SG1.G1_QUANT as G1_QUANT FROM "+RetSqlName("SB1")+" (NOLOCK) SB1 "
			_qry += "inner join "+RetSqlName("SG1")+" (NOLOCK) SG1 on SG1.D_E_L_E_T_ = '' AND G1_COD = B1_COD AND SG1.G1_FILIAL = '"+xfilial("SG1")+"' AND G1_REVINI = B1_REVATU "
			_qry += "inner join "+RetSqlName("SB1")+" (NOLOCK) SB1A on SB1A.D_E_L_E_T_ = '' AND G1_COMP = SB1A.B1_COD AND SB1A.B1_YSITUAC in ("+cStatus+") AND SB1A.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "inner join "+RetSqlName("SG1")+" (NOLOCK) SG1A on SG1A.D_E_L_E_T_ = '' AND SG1A.G1_COD = SG1.G1_COMP AND SG1A.G1_FILIAL = '"+xfilial("SG1")+"' "
			_qry += "inner join "+RetSqlName("SB1")+" (NOLOCK) SB1B on SB1B.D_E_L_E_T_ = '' AND SB1B.B1_COD = SG1A.G1_COMP AND SB1B.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "WHERE SB1.D_E_L_E_T_ = '' AND left(SB1.B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' AND SB1.B1_FILIAL = '"+xfilial("SB1")+"' "

			If SELECT("TMPPRC") > 0
				TMPPRC->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
			DbSelectArea("TMPPRC")
			DbGoTop()

			While !EOF()
				dbSelectArea('SG1')
				DbSetOrder(1)
				MsSeek(xFilial('SG1')+TMPPRC->G1_COMP)

				If Found()
					MR225ExplG(TMPPRC->G1_COMP,TMPPRC->G1_QUANT,TMPPRC->B1_REVATU)
				Else
					DbSelectArea("DA1")
					DbSetOrder(1)
					DbSeek(xfilial("DA1")+cTabela+TMPPRC->G1_COMP)

					If !Found() .or. DA1->DA1_PRCVEN = 0
						MsgInfo("Produto "+TMPPRC->G1_COMP+" n�o possui pre�o cadastrado na tabela informada!","Diverg�ncia de Pre�o")
					EndIf

					_tem := ASCAN( aEST, {|x| ALLTRIM(x[1]) == SubStr(ALLTRIM(TMPPRC->G1_COMP),1,8)})
					If _tem = 0
						Aadd(aEST, {substr(ALLTRIM(TMPPRC->G1_COMP),1,8),TMPPRC->G1_QUANT})
					Else
						aEST[_tem,2] += TMPPRC->G1_QUANT
					EndIf
					Aadd(aEST2, {TMPPRC->G1_COMP,TMPPRC->B1_COD, TMPPRC->G1_QUANT})
				EndIf

				DbSelectArea("TMPPRC")
				DbSkip()
			EndDo

			TMPPRC->(DbCloseArea())

		Else
//			_xcod := "= '"+SubStr(SB4->B4_COD,1,8)+"'"
//			_qry := "SELECT count(B1_COD) as QTD FROM "+RetSqlName("SB1")+" SB1 WHERE SB1.D_E_L_E_T_ = '' AND left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' AND B1_YSITUAC in ("+cStatus+") AND SB1.B1_FILIAL = '"+xfilial("SB1")+"'"
			_qry := "SELECT Count(B1_COD) as QTD FROM (SELECT B1_COD FROM "+RetSqlName("SB1")+" (NOLOCK) SB1 "
			_qry += "inner join "+RetSqlName("SG1")+" (NOLOCK) SG1 on SG1.D_E_L_E_T_ = '' AND G1_COD = B1_COD AND SG1.G1_FILIAL = '"+xfilial("SG1")+"' AND G1_REVINI = B1_REVATU "
			_qry += "WHERE SB1.D_E_L_E_T_ = '' AND left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' "
			If alltrim(cstatus) <> ""
				_qry += "AND B1_YSITUAC in ("+cStatus+") "
			EndIf
			_qry += "AND SB1.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "group by B1_COD) as x "

			If SELECT("TMPPRC") > 0
				TMPPRC->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
			DbSelectArea("TMPPRC")
			DbGoTop()
			_qtdprc := TMPPRC->QTD
			TMPPRC->(DbCloseArea())

			_qry := "SELECT B1_REVATU, G1_COD, G1_COMP, G1_QUANT FROM "+RetSqlName("SG1")+" (NOLOCK) SG1 "
			_qry += "inner join "+RetSqlName("SB1")+" (NOLOCK) SB1 on SB1.D_E_L_E_T_ = '' AND B1_COD = G1_COMP AND SB1.B1_FILIAL = '"+xfilial("SB1")+"' "
			_qry += "WHERE SG1.D_E_L_E_T_ = '' AND SG1.G1_FILIAL = '"+xfilial("SG1")+"' AND G1_REVINI+G1_COD in ( "
			_qry += "SELECT B1_REVATU+B1_COD FROM "+RetSqlName("SB1")+" (NOLOCK) SB1 WHERE SB1.D_E_L_E_T_ = '' AND left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' "
			If alltrim(cSTATUS) <> ""
				_qry += "AND B1_YSITUAC in ("+cStatus+") "
			EndIf
			_qry += "AND SB1.B1_FILIAL = '"+xfilial("SB1")+"' )"
//			_qry += "AND B1_TIPO not in ('MO','BN') "

			If SELECT("TMPPRC") > 0
				TMPPRC->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
			DbSelectArea("TMPPRC")
			DbGoTop()

			While !EOF()
				dbSelectArea('SG1')
				DbSetOrder(1)
				MsSeek(xFilial('SG1')+TMPPRC->G1_COMP)

				If Found()
					MR225ExplG(TMPPRC->G1_COMP,TMPPRC->G1_QUANT,TMPPRC->B1_REVATU)
				Else
					DbSelectArea("DA1")
					DbSetOrder(1)
					DbSeek(xfilial("DA1")+cTabela+TMPPRC->G1_COMP)

					If !Found() .or. DA1->DA1_PRCVEN = 0
						MsgInfo("Produto "+TMPPRC->G1_COMP+" n�o possui pre�o cadastrado na tabela informada!","Diverg�ncia de Pre�o")
					EndIf

					_tem := ASCAN( aEST, {|x| x[1] == SubStr(TMPPRC->G1_COMP,1,8)})
					If _tem = 0
						Aadd(aEST, {substr(TMPPRC->G1_COMP,1,8),TMPPRC->G1_QUANT})
					Else
						aEST[_tem,2] += TMPPRC->G1_QUANT
					EndIf
					Aadd(aEST2, {TMPPRC->G1_COMP,TMPPRC->G1_COD,TMPPRC->G1_QUANT})
				EndIf

				DbSelectArea("TMPPRC")
				DbSkip()
			EndDo

			TMPPRC->(DbCloseArea())
		EndIf

/*
		If SB4->B4_TIPO <> "KT"

			_qry := "SELECT G1_COMP, sum(TOT) as TOT, B4_DESC, B4_UM FROM ( " 
			_qry += "SELECT G1_COMP AS G1_COMP, AVG(TOT) AS TOT FROM "
			_qry += "( SELECT left(SG1A.G1_COD,8) as COD, left(G1_COMP,8) as G1_COMP, Sum(G1_QUANT) /(SELECT Count(*) as Tot " 
			_qry += "FROM ( SELECT SG1D.G1_COD FROM "+RetSqlName("SG1")+" SG1D (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1B (nolock) on SB1B.D_E_L_E_T_ = '' AND SB1B.B1_COD = SG1D.G1_COD " 
			If SB4->B4_TIPO <> "MI"
				_qry += "AND B1_YSITUAC in ("+cStatus+") "
			EndIf
			_qry += "WHERE lEFT(SG1D.G1_COD,8) = LEFT(SG1A.G1_COD,8) AND SG1D.D_E_L_E_T_ = '' " 
			_qry += "group by SG1D.G1_COD) as X) AS TOT " 
			_qry += "FROM "+RetSqlName("SG1")+" SG1A (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1 (nolock) on SB1.D_E_L_E_T_ = '' AND SB1.B1_COD = SG1A.G1_COD "
			If SB4->B4_TIPO <> "MI"
				_qry += "AND B1_YSITUAC in ("+cStatus+") "
			EndIf
			_qry += "inner join "+RetSqlName("SB1")+" SB1X (nolock) on SB1X.D_E_L_E_T_ = '' AND SB1X.B1_COD = SG1A.G1_COMP "
			_qry += "WHERE LEFT(G1_COD,8) "+_xcod+" AND SG1A.D_E_L_E_T_ = '' AND SB1X.B1_TIPO not in ('PI','MI') " 
			_qry += "Group by left(G1_COD,8), left(G1_COMP,8)) AS W " 
			_qry += "GROUP BY G1_COMP " 
			_qry += "union all "
			_qry += "SELECT G1_COMP, SUM(TOT) FROM ( "
			_qry += "SELECT left(X.G1_COMP,8) G1_COMP, (X.QTD/ "
			_qry += "(SELECT count(*) FROM ( "
			_qry += "SELECT SG1I.G1_COD "
			_qry += "FROM "+RetSqlName("SG1")+" SG1I (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1W (nolock) on SB1W.D_E_L_E_T_ = '' AND SB1W.B1_COD = SG1I.G1_COD "
			If SB4->B4_TIPO <> "MI"
				_qry += "AND SB1W.B1_YSITUAC IN ("+cStatus+") "
			EndIf
			_qry += "WHERE SG1I.D_E_L_E_T_ = '' AND LEFT(SG1I.G1_COD,8) "+_xcod+" GROUP BY SG1I.G1_COD ) AS T)) AS TOT "
			_qry += "FROM ( "
			If SB4->B4_TIPO = "MI"
				_qry += "SELECT G1_COD, SG1A.G1_COMP, (SELECT TOP 1 SG1X.G1_QUANT FROM "+RetSqlName("SG1")+" SG1X (nolock) WHERE SG1X.D_E_L_E_T_ = '' AND SG1X.G1_COMP = SG1A.G1_COD) * G1_QUANT as QTD "
			Else
				_qry += "SELECT G1_COD, SG1A.G1_COMP, G1_QUANT as QTD "
			EndIf
			_qry += "FROM "+RetSqlName("SG1")+" SG1A (nolock) "
			_qry += "WHERE SG1A.D_E_L_E_T_ = '' AND SG1A.G1_COD in ( SELECT SG1B.G1_COMP FROM "+RetSqlName("SG1")+" SG1B (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1 (nolock) on SB1.D_E_L_E_T_ = '' AND SB1.B1_COD = SG1B.G1_COD "
			If SB4->B4_TIPO <> "MI"
				_qry += "AND B1_YSITUAC in ("+cStatus+") "
			EndIf
			_qry += "inner join "+RetSqlName("SB1")+" SB1X (nolock) on SB1X.D_E_L_E_T_ = '' AND SB1X.B1_COD = SG1B.G1_COMP " 
			_qry += "WHERE "
			_qry += "SB1X.B1_TIPO in ('PI','MI') AND "
			_qry += "SG1B.D_E_L_E_T_ = '' AND LEFT(SG1B.G1_COD,8) "+_xcod+" " 
			_qry += "group by SG1B.G1_COMP) " 
			_qry += ") as X "
			_qry += "inner Join "+RetSqlName("SG1")+" SG1T (nolock) on SG1T.D_E_L_E_T_ = '' AND X.G1_COD = SG1T.G1_COMP AND LEFT(SG1T.G1_COD,8) "+_xcod+" " 
			_qry += "inner join "+RetSqlName("SB1")+" SB1Z (nolock) on SB1Z.D_E_L_E_T_ = '' AND SB1Z.B1_COD = SG1T.G1_COD "
			If SB4->B4_TIPO <> "MI"
				_qry += "AND SB1Z.B1_YSITUAC IN ("+cStatus+") "
			EndIf
			_qry += ") AS Q "
			_qry += "group by Q.G1_COMP "
			_qry += ") as P "
			_qry += "inner join "+RetSqlName("SB4")+" SB4 (nolock) on SB4.D_E_L_E_T_ = '' AND left(G1_COMP,8) = B4_COD " 
			_qry += "GROUP BY G1_COMP, B4_DESC, B4_UM " 
			_qry += "order by G1_COMP " 			
		Else
			_qry := "SELECT G1_COMP, sum(TOT) as TOT, B4_DESC, B4_UM FROM ( "
			_qry += "SELECT G1_COMP AS G1_COMP, sum(TOT) AS TOT FROM ( " 
			_qry += "SELECT left(SG1A.G1_COD,8) as COD, " 
			_qry += "left(G1_COMP,8) as G1_COMP "
			_qry += ",avg(G1_QUANT) as TOT "
			_qry += "FROM "+RetSqlName("SG1")+" SG1A (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1 (nolock) on SB1.D_E_L_E_T_ = '' AND SB1.B1_COD = '"+SubStr(SB4->B4_COD,1,8)+"'+Right(SG1A.G1_COD,7) AND SB1.B1_YSITUAC in ("+cStatus+") "
			_qry += "inner join "+RetSqlName("SB1")+" SB1X (nolock) on SB1X.D_E_L_E_T_ = '' AND SB1X.B1_COD = SG1A.G1_COMP WHERE LEFT(G1_COD,8) "+_xcod+" AND "
			_qry += "SG1A.D_E_L_E_T_ = '' AND SB1X.B1_TIPO not in ('PI','MI') "
			_qry += "Group by left(G1_COD,8), left(G1_COMP,8)) AS W GROUP BY G1_COMP " 
			_qry += "union all "
			_qry += "SELECT Q.G1_COMP, sum(TOTAL) as TOT FROM ( "
			_qry += "SELECT Left(X.G1_COD,8) as G1_COD, Left(X.G1_COMP,8) as G1_COMP, " 
			_qry += "avg(TOT) as TOTAL "
			_qry += "FROM "
			_qry += "(SELECT SG1A.G1_COD, SG1A.G1_COMP, SG1A.G1_QUANT "
			_qry += "*(SELECT X.QTD FROM ( "
			_qry += "SELECT TOP 1 right(G1_COD,7) as COD ,count(*) as QTD FROM "+RetSqlName("SG1")+" SG1Z (nolock) WHERE SG1Z.D_E_L_E_T_ = '' AND " 
			_qry += "left(SG1Z.G1_COD,8) "+_xcod+" AND left(SG1Z.G1_COMP,8) = left(SG1A.G1_COD,8) "
			_qry += "group by right(G1_COD,7)) as X) as TOT "
			_qry += "FROM "+RetSqlName("SG1")+" SG1A (nolock) WHERE SG1A.D_E_L_E_T_ = '' AND SG1A.G1_COD in ( " 
			_qry += "SELECT SG1B.G1_COMP FROM "+RetSqlName("SG1")+" SG1B (nolock) "
			_qry += "inner join "+RetSqlName("SB1")+" SB1 (nolock) on SB1.D_E_L_E_T_ = '' AND SB1.B1_COD = '"+SubStr(SB4->B4_COD,1,8)+"'+Right(SG1B.G1_COD,7) AND SB1.B1_YSITUAC in ("+cStatus+") "
			_qry += "inner join "+RetSqlName("SB1")+" SB1X (nolock) on SB1X.D_E_L_E_T_ = '' AND SB1X.B1_COD = SG1B.G1_COMP WHERE SB1X.B1_TIPO in ('PI','MI') AND SG1B.D_E_L_E_T_ = '' AND " 
			_qry += "LEFT(SG1B.G1_COD,8) "+_xcod+" " 
			_qry += "group by SG1B.G1_COMP) " 
			_qry += ") as X "
			_qry += "group by left(X.G1_COD,8), Left(X.G1_COMP,8) " 
			_qry += ") as Q group by Q.G1_COMP "
			_qry += ") as P "
			_qry += "inner join "+RetSqlName("SB4")+" SB4 (nolock) on SB4.D_E_L_E_T_ = '' AND left(G1_COMP,8) = B4_COD GROUP BY G1_COMP, B4_DESC, B4_UM order by G1_COMP" 		
		EndIf
		
		MemoWrite("HESTP001_preco0.txt",_Qry)
		If SELECT("TMPSG1") > 0
			TMPSG1->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSG1",.T.,.T.)

		DbSelectArea("TMPSG1")
		DbGoTop()

		While !EOF()
		
			DbSelectArea("SG1")
			DbSetOrder(1)
			DbSeek(xfilial("SG1")+SubStr(TMPSG1->G1_COMP,1,8))

			If Found()
				While !EOF() .AND. SubStr(SG1->G1_COD,1,8) = SubStr(TMPSG1->G1_COMP,1,8)
					_ultprc := pegprg1(ccodigo,SubStr(SG1->G1_COMP,1,8),1,_tp,cDtDe,cTipo)
					_antprc := pegprg1(ccodigo,SubStr(SG1->G1_COMP,1,8),2,_tp,cDtDe,cTipo)
					DbSelectArea("SB4")
					DbSetOrder(1)
					DbSeek(xfilial("SB4")+SubStr(SG1->G1_COMP,1,8))

					_i := ASCAN( aCols1, {|x| SubStr(x[1],1,8) == SubStr(SG1->G1_COMP,1,8)})
	    		
					If _i = 0
						If _tp == 1
							Aadd(aCols1, {SubStr(SG1->G1_COMP,1,8),SB4->B4_DESC,SB4->B4_UM,TMPSG1->TOT*SG1->G1_QUANT,_ultprc,0,0,0,.F.})
						Else
							Aadd(aCols1, {SubStr(SG1->G1_COMP,1,8),SB4->B4_DESC,SB4->B4_UM,TMPSG1->TOT*SG1->G1_QUANT,_antprc,_ultprc,(TMPSG1->TOT*SG1->G1_QUANT)*_ultprc,(TMPSG1->TOT*SG1->G1_QUANT)*_antprc,.F.})
						EndIf
					EndIf
				
					DbSelectArea("SG1")
					DbSkip()
				End
			Else
*/

		DbSelectArea("SB4")
		_areaB4 := GetArea()

		For _t := 1 to len(aEst)
			DbSelectArea("SB4")
			DbSetOrder(1)
			DbSeek(xfilial("SB4")+aEst[_t,1])

			_ultprc := pegprg1(ccodigo,aEst[_t,1],1,_tp,cDtDe,cTipo)
			_antprc := pegprg1(ccodigo,aEst[_t,1],2,_tp,cDtDe,cTipo)
			If _tp == 1
				Aadd(aCols1, {aEst[_t,1],SB4->B4_DESC,SB4->B4_UM,round(aEst[_t,2]/_qtdprc,6),_ultprc,0,0,0,.F.})
			Else
				Aadd(aCols1, {aEst[_t,1],SB4->B4_DESC,SB4->B4_UM,round(aEst[_t,2]/_qtdprc,6),_antprc,_ultprc,round(aEst[_t,2]/_qtdprc,6)*_ultprc,round(aEst[_t,2]/_qtdprc,6)*_antprc,.F.})
			EndIf
		Next

		RestArea(_areaB4)
//			DbSelectArea("TMPSG1")
//			DbSkip()
//		End
//		TMPSG1->(dbCloseArea())

	Else
		AjuSX1()
		Pergunte("HESTC1",.T.)
		_qry := "SELECT TOP 1 * FROM "+RetSqlName("SZI")+" (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZI_PRODUTO = '"+SB4->B4_COD+"' AND ZI_TIPO = '"+MV_PAR01+"' order by ZI_DTDE desc "

		If SELECT("TMPSZI") > 0
			TMPSZI->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZI",.T.,.T.)
		DbSelectArea("TMPSZI")
		DbGoTop()

		_dtde := TMPSZI->ZI_DTDE
		DbCloseArea()

		_qry := " SELECT * FROM "+RetSqlName("SZI")+" (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZI_PRODUTO = '"+SB4->B4_COD+"' AND ZI_TIPO = '"+MV_PAR01+"' AND ZI_DTDE = '"+_dtde+"' order by ZI_PRODUTO "

		If SELECT("TMPSZI") > 0
			TMPSZI->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZI",.T.,.T.)
		DbSelectArea("TMPSZI")
		DbGoTop()
		_tipo := ""
		_ctab  := ""
		While !EOF()
			_tipo := TMPSZI->ZI_TIPO
			_ctab  := TMPSZI->ZI_TABELA
			DbSelectArea("SB4")
			_areaB4 := GetArea()
			DbSetOrder(1)
			DbSeek(Xfilial("SB4")+alltrim(TMPSZI->ZI_COMP))

			Aadd(aCols1, {TMPSZI->ZI_COMP,SB4->B4_DESC,SB4->B4_UM,TMPSZI->ZI_QTD,TMPSZI->ZI_CANT,TMPSZI->ZI_CATU,TMPSZI->ZI_QTD*TMPSZI->ZI_CATU,TMPSZI->ZI_QTD*TMPSZI->ZI_CANT,.F.})

			If SB4->B4_TIPO == "MO" .or. SB4->B4_TIPO == "BN"
				_vrmo += ROUND(TMPSZI->ZI_QTD*TMPSZI->ZI_CATU,6)
				_vrmoa+= ROUND(TMPSZI->ZI_QTD*TMPSZI->ZI_CANT,6)
			Else
				_vrmp += ROUND(TMPSZI->ZI_QTD*TMPSZI->ZI_CATU,6)
				_vrmpa+= ROUND(TMPSZI->ZI_QTD*TMPSZI->ZI_CANT,6)
			EndIf
			RestArea(_areaB4)
			DbSelectArea("TMPSZI")
			DbSkip()
		EndDo
		TMPSZI->(DbCloseArea())

		cVrMO := _vrmo		//oDlg:cVrMO := str(_vrmo)
		cVrMP := _vrmp		//oDlg:cVrMP := str(_vrmp)
		cVrMOA:= _vrmoa		//oDlg:cVrMO := str(_vrmo)
		cVrMPA:= _vrmpa		//oDlg:cVrMP := str(_vrmp)
		cCusto := ROUND((_vrmo*(najuste/100))+(_vrmP*(najuste/100)),6)		//oDlg:cVrMO := str(_vrmo)
		cCusA  := ROUND((_vrmoa*(najuste/100))+(_vrmPa*(najuste/100)),6)
		cVrGeral:= _vrmo+_vrmp+cCusto
		cVrGerAnt:= cVrMOA+cVrMPA+cCusA
		cTabela			:= _ctab
		cdescTab		:= POSICIONE("DA0",1,xFilial("DA0")+_ctab,"DA0_DESCRI")
		cTipo			:= _tipo
		cDtDe			:= stod(_dtde)
		oDlg1:refresh()

	EndIf

	oMSPreco := MsNewGetDados():New( 050, 005, 225, 624, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg1, aHeader1, aCols1)

Return

Static Function pegprg1(ccodigo,_COMP,_tpprc,_tp,cDtDe,cTipo)
// Busca o Pre�o dos produtos para forma��o de pre�o

	_prc	:= 0

	// Pega o �ltimo pre�o
	_qry := "SELECT TOP 1 * FROM  "+RetSqlName("SZI")+" SZI (nolock) WHERE SZI.D_E_L_E_T_ = '' AND "
	_qry += "ZI_COMP = '"+_COMP+"' AND ZI_PRODUTO = '"+cCodigo+"' " //AND ZI_TABELA = '"+cTabela+"' "
//  If _tp <> 1
//  	_qry += "AND ZI_DTDE <= '"+Dtos(Date())+"' AND ZI_DTATE >= '"+Dtos(Date())+"' " 
//  EndIf
	_qry += "order by R_E_C_N_O_ desc "

	If SELECT("TMPSZI") > 0
		TMPSZI->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSZI",.T.,.T.)
	DbSelectArea("TMPSZI")
	DbGoTop()

	If _tpprc == 1
		_prc := TMPSZI->ZI_CATU
	Else
		_prc := TMPSZI->ZI_CANT
	EndIf

	TMPSZI->(dbCloseArea())

Return(_prc)

Static Function valTab(_tp, cTabela)
// Valida tabela selecionada e atualiza os pre�os

	Local _i	:= 0
	Local _vrmo	:= 0
	Local _vrmp	:= 0
	Local _vrmoa:= 0
	Local _vrmpa:= 0
	Local _i

	DbSelectArea("SB4")
	DbSetOrder(1)
	DbSeek(xfilial("SB4")+SubSt(cCodigo,1,8))

	If SB4->B4_TIPO = "KT"
		_xcod := "in ('"
		DbSelectArea("SG1")
		DbSetOrder(1)
		DbSeek(xfilial("SG1")+SubStr(SB4->B4_COD,1,8))
		_akit := {}
		While !EOF() .AND. SubStr(SG1->G1_COD,1,8) = SubStr(SB4->B4_COD,1,8)
			_tem := ASCAN( _aKit, {|x| x[1] == SubStr(SG1->G1_COMP,1,8)})
			If _tem = 0
				aadd(_akit,{SubStr(SG1->G1_COMP,1,8)})
			EndIf
			DbSelectArea("SG1")
			DbSkip()
		EndDo
		For _i := 1 to len(_akit)
			_xcod += _akit[_i,1]+"','"
		Next
		_xcod := SubStr(_xcod,1,len(_xcod)-2)+")"
	Else
		_xcod := "= '"+SubStr(SB4->B4_COD,1,8)+"'"
	EndIf

	If SB4->B4_TIPO <> "KT"

		_qry := " SELECT * FROM (  "
		_qry += "SELECT LEFT(G1_COMP,8) as PRODUTO, AVG(DA1_PRCVEN) as PRECO FROM "+RetSqlName("SG1")+" SG1 (nolock) "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 (nolock) ON SB1.D_E_L_E_T_ = '' AND SB1.B1_COD = G1_COD "
		If alltrim(cStatus) <> ""
			_qry += "AND SB1.B1_YSITUAC in ("+cStatus+") "
		EndIf
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1A (nolock) ON SB1A.D_E_L_E_T_ = '' AND SB1A.B1_COD = G1_COMP AND SB1A.B1_TIPO NOT in ('PI','MI') "
		_qry += "INNER JOIN "+RetSqlName("DA1")+" DA1 (nolock) ON DA1.D_E_L_E_T_ = '' AND DA1_CODTAB = '"+cTabela+"' AND DA1_CODPRO = G1_COMP "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(G1_COD,8) "+_xcod+" "
		_qry += "GROUP BY LEFT(SG1.G1_COMP,8) "
		_qry += "UNION ALL "
		_qry += "SELECT LEFT(SG1A.G1_COMP,8) as PRODUTO, AVG(DA1_PRCVEN) as PRECO FROM "+RetSqlName("SG1")+" SG1 (nolock) "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1 (nolock) ON SB1.D_E_L_E_T_ = '' AND SB1.B1_COD = SG1.G1_COD "
		If alltrim(cStatus) <> ""
			_qry += "AND SB1.B1_YSITUAC in ("+cStatus+") "
		EndIf
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1A (nolock) ON SB1A.D_E_L_E_T_ = '' AND SB1A.B1_COD = SG1.G1_COMP AND SB1A.B1_TIPO in ('PI','MI') "
		_qry += "INNER JOIN "+RetSqlName("SG1")+" SG1A (nolock) ON SG1A.D_E_L_E_T_ = '' AND SG1.G1_COMP = SG1A.G1_COD "
		_qry += "INNER JOIN "+RetSqlName("DA1")+" DA1 (nolock) ON DA1.D_E_L_E_T_ = '' AND DA1_CODTAB = '"+cTabela+"' AND DA1_CODPRO = SG1A.G1_COMP "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(SG1.G1_COD,8) "+_xcod+" "
		_qry += "GROUP BY LEFT(SG1A.G1_COMP,8) "
		_qry += ") AS X "
		_qry += "ORDER BY PRODUTO "
/*
		_qry := "SELECT Count(QTD) as QTD FROM ( "
		_qry += "SELECT SB1.B1_COD as QTD FROM "+RetSqlName("SB1")+" SB1 " 
		_qry += "inner join "+RetSqlName("SG1")+" SG1 on SG1.D_E_L_E_T_ = '' AND G1_COD = B1_COD AND SG1.G1_FILIAL = '"+xfilial("SG1")+"' "
		_qry += "inner join "+RetSqlName("SB1")+" SB1A on SB1A.D_E_L_E_T_ = '' AND G1_COMP = SB1A.B1_COD AND SB1A.B1_YSITUAC in ("+cStatus+") AND SB1A.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "WHERE SB1.D_E_L_E_T_ = '' AND left(SB1.B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' AND SB1.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "group by SB1.B1_COD) as X "

		If SELECT("TMPPRC") > 0
			TMPPRC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
		DbSelectArea("TMPPRC")
		DbGoTop()
		_qtdprc := TMPPRC->QTD
		TMPPRC->(DbCloseArea())

		_qry := "SELECT SB1.B1_COD, SG1A.G1_COMP, SG1A.G1_QUANT*SG1.G1_QUANT as G1_QUANT FROM "+RetSqlName("SB1")+" SB1 " 
		_qry += "inner join "+RetSqlName("SG1")+" SG1 on SG1.D_E_L_E_T_ = '' AND G1_COD = B1_COD AND SG1.G1_FILIAL = '"+xfilial("SG1")+"' "
		_qry += "inner join "+RetSqlName("SB1")+" SB1A on SB1A.D_E_L_E_T_ = '' AND G1_COMP = SB1A.B1_COD AND SB1A.B1_YSITUAC in ("+cStatus+") AND SB1A.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "inner join "+RetSqlName("SG1")+" SG1A on SG1A.D_E_L_E_T_ = '' AND SG1A.G1_COD = SG1.G1_COMP AND SG1A.G1_FILIAL = '"+xfilial("SG1")+"' "
		_qry += "inner join "+RetSqlName("SB1")+" SB1B on SB1B.D_E_L_E_T_ = '' AND SB1B.B1_COD = SG1A.G1_COMP AND SB1B.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "WHERE SB1.D_E_L_E_T_ = '' AND left(SB1.B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' AND SB1.B1_FILIAL = '"+xfilial("SB1")+"' "

		If SELECT("TMPPRC") > 0
			TMPPRC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
		DbSelectArea("TMPPRC")
		DbGoTop()

		While !EOF()
			dbSelectArea('SG1')
			DbSetOrder(1)
			MsSeek(xFilial('SG1')+TMPPRC->G1_COMP)
			
			If Found()
				MP225ExplG(TMPPRC->G1_COMP,TMPPRC->G1_QUANT)
			Else
				_tem := ASCAN( aEST3, {|x| x[1] == substr(TMPPRC->G1_COMP,1,8) })
				If _tem = 0
					Aadd(aEST3, {substr(TMPPRC->G1_COMP,1,8),POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")})
				Else
					aEST3[_tem,2] += POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")
				EndIf
				Aadd(aEST4, {TMPPRC->G1_COMP,TMPPRC->B1_COD, POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")})
					
			EndIf
			
			DbSelectArea("TMPPRC")
			DbSkip()
		End
			
		TMPPRC->(DbCloseArea())
*/
	Else

		_qry := "SELECT left(G1_COMP,8) as PRODUTO, Preco2 as PRECO FROM ( "
		_qry += "SELECT Left(G1_COMP,8) as G1_COMP, Avg(Preco2) as Preco2 FROM ( "
		_qry += "SELECT isnull(DA1_PRCVEN,0) as precoa, G1_QUANT, isnull(DA1_PRCVEN,0)*G1_QUANT as Preco, G1_COMP, "
		_qry += "(isnull(DA1_PRCVEN,0)) "
		_qry += "as Preco2 "
		_qry += "FROM "+RetSqlName("SG1")+" SG1 WITH (NOLOCK) "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1X WITH (NOLOCK) ON SB1X.D_E_L_E_T_ = '' AND SG1.G1_COD = SB1X.B1_COD " // AND SB1X.B1_YSITUAC in ("+cStatus+") "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1W WITH (NOLOCK) ON SB1W.D_E_L_E_T_ = '' AND SG1.G1_COMP = SB1W.B1_COD "
		_qry += "left join "+RetSqlName("DA1")+" DA1 WITH (NOLOCK) on DA1.D_E_L_E_T_ = '' AND DA1_CODTAB = '"+cTabela+"' AND DA1_CODPRO = G1_COMP "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(G1_COD,8) "+_xcod+" AND SB1W.B1_TIPO NOT in ('PI','MI') "
		_qry += ") as X "
		_qry += "group by left(G1_COMP,8) "
		_qry += ") as w "
		_qry += "group by left(G1_COMP,8), Preco2 "
		_qry += "union all "
		_qry += "SELECT left(G1_COMP,8), avg(Preco2) FROM ( "
		_qry += "SELECT (isnull(DA1_PRCVEN,0)) as Preco2, "
		_qry += "G1_COMP FROM ( "
		_qry += "SELECT "
		_qry += "(SELECT Count(*) FROM "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) WHERE SB1.D_E_L_E_T_ = '' " // AND B1_YSITUAC in ("+cStatus+") "
		_qry += "AND LEFT(B1_COD,8) "+_xcod+"  AND (SELECT TOP 1 G1_COD FROM "+RetSqlName("SG1")+" SG1D WITH (NOLOCK) WHERE SG1D.D_E_L_E_T_ = '' AND SG1D.G1_COD = B1_COD) is not null) as qtditem "
		_qry += ",G1_COMP, "
		_qry += "(SELECT TOP 1 SG1B.G1_QUANT FROM "+RetSqlName("SG1")+" SG1B WITH (NOLOCK) WHERE SG1B.D_E_L_E_T_ = '' AND SG1B.G1_COMP = SG1A.G1_COD ) * G1_QUANT as Quant "
		_qry += "FROM "+RetSqlName("SG1")+" SG1A WITH (NOLOCK) "
		_qry += "WHERE SG1A.D_E_L_E_T_ = '' AND SG1A.G1_COD in ( "
		_qry += "SELECT G1_COMP "
		_qry += "FROM "+RetSqlName("SG1")+" SG1 WITH (NOLOCK) "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1X WITH (NOLOCK) ON SB1X.D_E_L_E_T_ = '' AND SG1.G1_COD = SB1X.B1_COD " //AND SB1X.B1_YSITUAC in ("+cStatus+") "
		_qry += "INNER JOIN "+RetSqlName("SB1")+" SB1W WITH (NOLOCK) ON SB1W.D_E_L_E_T_ = '' AND SG1.G1_COMP = SB1W.B1_COD "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND LEFT(G1_COD,8) "+_xcod+" AND SB1W.B1_TIPO in ('PI','MI') ) "
		_qry += ") as w "
		_qry += "left join "+RetSqlName("DA1")+" DA1 WITH (NOLOCK) on DA1.D_E_L_E_T_ = '' AND DA1_CODTAB = '"+cTabela+"' AND DA1_CODPRO = G1_COMP "
		_qry += ") as t "
		_qry += "group by Left(G1_COMP,8) order by left(G1_COMP,8) "
/*		
		_qry := "SELECT count(B1_COD) as QTD FROM "+RetSqlName("SB1")+" SB1 WHERE SB1.D_E_L_E_T_ = '' AND left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' AND B1_YSITUAC in ("+cStatus+") AND SB1.B1_FILIAL = '"+xfilial("SB1")+"'"

		If SELECT("TMPPRC") > 0
			TMPPRC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
		DbSelectArea("TMPPRC")
		DbGoTop()
		_qtdprc := TMPPRC->QTD
		TMPPRC->(DbCloseArea())
			
		_qry := "SELECT G1_COD, G1_COMP, G1_QUANT FROM "+RetSqlName("SG1")+" SG1 " 
		_qry += "inner join "+RetSqlName("SB1")+" SB1 on SB1.D_E_L_E_T_ = '' AND B1_COD = G1_COMP AND SB1.B1_FILIAL = '"+xfilial("SB1")+"' "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND SG1.G1_FILIAL = '"+xfilial("SG1")+"' AND G1_COD in ( "
		_qry += "SELECT B1_COD FROM "+RetSqlName("SB1")+" SB1 WHERE SB1.D_E_L_E_T_ = '' AND left(B1_COD,8) = '"+SubStr(SB4->B4_COD,1,8)+"' AND B1_YSITUAC in ("+cStatus+")) AND SB1.B1_FILIAL = '"+xfilial("SB1")+"'"

		If SELECT("TMPPRC") > 0
			TMPPRC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)
		DbSelectArea("TMPPRC")
		DbGoTop()

		While !EOF()
			dbSelectArea('SG1')
			DbSetOrder(1)
			MsSeek(xFilial('SG1')+TMPPRC->G1_COMP)
			
			If Found()
				MP225ExplG(TMPPRC->G1_COMP,TMPPRC->G1_QUANT)
			Else
				_tem := ASCAN( aEST3, {|x| x[1] == SubStr(TMPPRC->G1_COMP,1,8)})
				If _tem = 0
					Aadd(aEST3, {substr(TMPPRC->G1_COMP,1,8),POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")})
				Else
					aEST3[_tem,2] += POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")
				EndIf
				Aadd(aEST4, {TMPPRC->G1_COMP,TMPPRC->G1_COD,POSICIONE("DA1",1,xFilial("DA1")+cTabela+TMPPRC->G1_COMP,"DA1_PRCVEN")})
			EndIf
			
			DbSelectArea("TMPPRC")
			DbSkip()
		End
			
		TMPPRC->(DbCloseArea())
*/
	EndIf
	cQuery := ChangeQuery(_qry)

	MemoWrite("HESTP001_preco1.txt",cQuery)
	If SELECT("TMPPRC") > 0
		TMPPRC->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPPRC",.T.,.T.)

	cdescTab		:= POSICIONE("DA0",1,xFilial("DA0")+cTabela,"DA0_DESCRI")

	DbSelectArea("TMPPRC")
	DbGoTop()
	While !EOF()

//	For _t := 1 to len(aEST3)
//		_i := ASCAN( OMSPreco:ACOLS, {|x| SubStr(x[1],1,8) == SubStr(aEST3[_t,1],1,8)})
		_i := ASCAN( OMSPreco:ACOLS, {|x| SubStr(x[1],1,8) == SubStr(TMPPRC->PRODUTO,1,8)})
		_prc := 0

		If _i > 0
//			_prc := round(aEst3[_t,2] / _qtdprc,6) 
			_prc := TMPPRC->PRECO

			If _tp <> 1
				OMSPreco:ACols[_i,5] := pegprg1(ccodigo,OMSPreco:ACOLS[_i,1],1,_tp,Dtos(Date()),Dtos(Date()))
				OMSPreco:ACols[_i,6] := _prc //Posicione("DA1",1,xfilial("DA1")+cTabela+OMSPreco:aCols[_i,1],"DA1_PRCVEN")
//				OMSPreco:ACols[_i,7] := ROUND(Posicione("DA1",1,xfilial("DA1")+cTabela+OMSPreco:aCols[_i,1],"DA1_PRCVEN")*OMSPreco:aCols[_i,4],6)
				OMSPreco:ACols[_i,7] := ROUND(_prc*OMSPreco:aCols[_i,4],6)
				OMSPreco:ACols[_i,8] := ROUND(OMSPreco:ACols[_i,5]*OMSPreco:aCols[_i,4],6)
			Else
				//OMSPreco:ACols[_i,5] := 0
				OMSPreco:ACols[_i,6] := _prc
				OMSPreco:ACols[_i,7] := ROUND(_prc*OMSPreco:aCols[_i,4],6)
				OMSPreco:ACols[_i,8] := ROUND(OMSPreco:aCols[_i,5]*OMSPreco:aCols[_i,4],6)
			EndIf

			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+OMSPreco:aCols[_i,1])

			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmo += OMSPreco:ACols[_i,7]
			Else
				_vrmp += OMSPreco:ACols[_i,7]
			EndIf
			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmoa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			Else
				_vrmpa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			EndIf
		EndIf

		DbSelectArea("TMPPRC")
		DbSkip()
	EndDo
//	Next
	DbSelectArea("TMPPRC")
	DbCloseArea()
	_vrmo := 0
	_vrmp := 0
	_vrmoa:= 0
	_vrmpa:= 0

	For _i := 1 to len(OMSPreco:ACols)

		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+OMSPreco:aCols[_i,1])

		If OMSPreco:ACols[_i,6] = 0
			_prc := Posicione("DA1",1,xfilial("DA1")+cTabela+OMSPreco:aCols[_i,1],"DA1_PRCVEN")
			OMSPreco:ACols[_i,6] := _prc
			OMSPreco:ACols[_i,7] := ROUND(_prc*OMSPreco:aCols[_i,4],6)
			OMSPreco:ACols[_i,8] := ROUND(OMSPreco:aCols[_i,5]*OMSPreco:aCols[_i,4],6)

			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmo += OMSPreco:ACols[_i,7]
			Else
				_vrmp += OMSPreco:ACols[_i,7]
			EndIf
			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmoa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			Else
				_vrmpa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			EndIf
		Else
			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmo += OMSPreco:ACols[_i,7]
			Else
				_vrmp += OMSPreco:ACols[_i,7]
			EndIf
			If SB1->B1_TIPO == "MO" .or. SB1->B1_TIPO == "BN"
				_vrmoa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			Else
				_vrmpa+= ROUND(OMSPreco:ACols[_i,4]*OMSPreco:ACols[_i,5],6)
			EndIf
		EndIf
	Next

	oMSPreco:Refresh()

	cVrMO := _vrmo		//oDlg:cVrMO := str(_vrmo)
	cVrMP := _vrmp		//oDlg:cVrMP := str(_vrmp)
	cVrMOA:= _vrmoa		//oDlg:cVrMO := str(_vrmo)
	cVrMPA:= _vrmpa		//oDlg:cVrMP := str(_vrmp)
	cCusto := ROUND((_vrmo*(najuste/100))+(_vrmP*(najuste/100)),6)		//oDlg:cVrMO := str(_vrmo)
	cCusA  := ROUND((_vrmoa*(najuste/100))+(_vrmPa*(najuste/100)),6)
	cVrGeral:= _vrmo+_vrmp+cCusto
	cVrGerAnt:= cVrMOA+cVrMPA+cCusA


//cVrMO := _vrmo*(1+(najuste/100))		//oDlg:cVrMO := str(_vrmo)
//cVrMP := _vrmp*(1+(najuste/100))		//oDlg:cVrMP := str(_vrmp)
//cVrMOA:= _vrmoa		//oDlg:cVrMO := str(_vrmo)
//cVrMPA:= _vrmpa		//oDlg:cVrMP := str(_vrmp)
//cVrGeral:= cVrMO+cVrMP
	oDlg1:refresh()

Return


Static Function SavePrc(_tp)
// Salva forma��o de pre�o cadastrada

	Local I

	If Alltrim(cTabela)<>"" .AND. (cDtDe <= Date() .AND. cDtDe <> ctod("  /  /    "))
		_custd := MsgYesNo("Atualiza Custo Standard? ","Atualiza Custo Standard")
		_custo := 0
		If _tp == 1
			For i := 1 to Len(OMSPreco:ACOLS)
				If OMSPreco:ACOLS[i,len(OMSPreco:AHEADER)+1] == .F.
					_custo += OMSPreco:ACOLS[i,4] * OMSPreco:ACOLS[i,6]
					DbSelectArea("SZI")
					DbSetOrder(1)
					DbSeek(xfilial("SZI")+cCodigo+DTOS(cDtDe)+PADR(cTipo,10)+cTabela+OMSPreco:ACOLS[i,1])

					If !Found()
						RecLock("SZI",.T.)
						Replace ZI_FILIAL			with xfilial("SZI")
						Replace ZI_PRODUTO		with cCodigo
						Replace ZI_DTDE			with cDtDe
						Replace ZI_TIPO			with cTipo
						Replace ZI_TABELA			with cTabela
						Replace ZI_COMP			with OMSPreco:ACOLS[i,1]
						Replace ZI_QTD			with OMSPreco:ACOLS[i,4]
						Replace ZI_CANT			with OMSPreco:ACOLS[i,5]
						Replace ZI_CATU			with OMSPreco:ACOLS[i,6]
						MsUnLock()
					Else
						RecLock("SZI",.F.)
						Replace ZI_TIPO			with cTipo
						Replace ZI_QTD			with OMSPreco:ACOLS[i,4]
						Replace ZI_CANT			with OMSPreco:ACOLS[i,5]
						Replace ZI_CATU			with OMSPreco:ACOLS[i,6]
						MsUnLock()
					EndIf
				EndIf
			Next
		ElseIf _tp == 2
			For i := 1 to Len(OMSPreco:ACOLS)
				If OMSPreco:ACOLS[i,len(OMSPreco:AHEADER)+1] == .F.
					_custo += OMSPreco:ACOLS[i,4] * OMSPreco:ACOLS[i,6]
					DbSelectArea("SZI")
					DbSetOrder(1)
					DbSeek(xfilial("SZI")+cCodigo+DTOS(cDtDe)+PADR(cTipo,10)+cTabela+OMSPreco:ACOLS[i,1])

					If !Found()
						RecLock("SZI",.T.)
						Replace ZI_FILIAL			with xfilial("SZE")
						Replace ZI_PRODUTO		with cCodigo
						Replace ZI_DTDE			with cDtDe
						Replace ZI_TIPO			with ctipo
						Replace ZI_TABELA			with cTabela
						Replace ZI_COMP			with OMSPreco:ACOLS[i,1]
						Replace ZI_QTD			with OMSPreco:ACOLS[i,4]
						Replace ZI_CANT			with OMSPreco:ACOLS[i,5]
						Replace ZI_CATU			with OMSPreco:ACOLS[i,6]
						MsUnLock()
					Else
						RecLock("SZI",.F.)
						Replace ZI_TIPO			with cTipo
						Replace ZI_QTD			with OMSPreco:ACOLS[i,4]
						Replace ZI_CANT			with OMSPreco:ACOLS[i,5]
						Replace ZI_CATU			with OMSPreco:ACOLS[i,6]
						MsUnLock()
					EndIf
				EndIf
			Next
		EndIf
		If _custd
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+alltrim(cCodigo))

			While !EOF() .AND. SubStr(SB1->B1_COD,1,8) == SubStr(cCodigo,1,8)
				If SB1->B1_YSITUAC $ cStatu2 .or. SB1->B1_TIPO = "MI"
					RecLock("SB1",.f.)
					Replace B1_CUSTD 	with cVrGeral //_custo
					If SB1->B1_TIPO = "MI"
						Replace B1_PRV1 	with cVrGeral //_custo
					EndIf
					MsUnLock()
					U_HPDISPLJ(SB1->B1_COD)
				EndIf
				DbSelectArea("SB1")
				DbSkip()
			EndDo
		EndIf
	Else
		If Alltrim(cTabela)==""
			Alert("Favor incluir o codigo da tabela a ser utilizada!")
			//return(oDlg1)
			return()
		EndIf
		If 	cDtDe > Date() .or. cDtDe == ctod("  /  /    ")
			Alert("Data De deve ser inferior a "+ dtoc(Date()) +" !")
			//return(oDlg1)
			return()
		EndIf
	EndIf

Return

User Function EstPro1(_tp)
// Cadastro de estrutura de produtos - Controle de vers�o

	Local oCancela
	Local oCodProd
	Local oDescProd
	Local cDescProd := SB4->B4_DESC
	Local oOk
	Local oSay1
	Private cCodProd := SB4->B4_COD
	Private choradel := Time()
	Private ddatadel := dtos(date())
	Private ccodusr := retcodusr()
	Private cnomusr := Alltrim(SUBSTR(UsrFullName(),1,18))
	Static oDlg

	Public cColuna		:= Space(03)
	Public cColunx		:= Space(03)
	Public cCor			:= Space(500)
	Public OMSRevisao

	If cfilant == "0101"

		cColuna := SB4->B4_COLUNA
		DEFINE MSDIALOG oDlg TITLE "Controle de Vers�o" FROM 000, 000  TO 300, 800 COLORS 0, 16777215 PIXEL

		fMSRevisao()
		@ 011, 007 SAY oSay1 PROMPT "Produto" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 010, 042 MSGET oCodProd VAR cCodProd SIZE 066, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 010, 113 MSGET oDescProd VAR cDescProd SIZE 278, 010 OF oDlg COLORS 0, 16777215 PIXEL
		@ 124, 306 BUTTON oOk PROMPT "Visualiza" SIZE 037, 012 ACTION Visualiza(_tp, OMSRevisao:ACOLS[OMSRevisao:nat,1]) OF oDlg PIXEL
		@ 124, 263 BUTTON oCancela PROMPT "Cancela" SIZE 037, 012 ACTION oDlg:End() OF oDlg PIXEL
		If __cuserid $ GETMV("HP_USRPROD")
			@ 124, 220 BUTTON oInclui PROMPT "Salva" SIZE 037, 012 ACTION Salva() OF oDlg PIXEL
			@ 124, 360 BUTTON oOk PROMPT "Seleciona" SIZE 037, 012 ACTION Seleciona(_tp, OMSRevisao:ACOLS[OMSRevisao:nat,1]) OF oDlg PIXEL
		EndIf

		ACTIVATE MSDIALOG oDlg CENTERED
	Else
		MSGALERT("Para verificar a estrutura, favor logar na filial 0101 ! ","Filial" )
	EndIf

Return

//------------------------------------------------ 
Static Function fMSRevisao()
//------------------------------------------------ 
// Acols da Revis�o

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"G5_REVISAO","G5_OBS","G5_STATUS"}
	Local aAlterFields := {"G5_REVISAO","G5_OBS","G5_STATUS"}

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		EndIf
	Next nX

	// Define field values
	DbSelectarea("SG5")
	DbSetOrder(1)
	DbSeek(xfilial("SG5")+alltrim(SB4->B4_COD))

	If Found()
		_cod := SG5->G5_PRODUTO
		While !EOF() .AND. SG5->G5_PRODUTO == _cod
			Aadd(aColsEx, {SG5->G5_REVISAO, SG5->G5_OBS,SG5->G5_STATUS,.f.})
			DbSelectArea("SG5")
			DbSkip()
		EndDo
	Else
		Aadd(aColsEx, {"001","REVISAO INICIAL","1",.F.})
	EndIf

	oMSRevisao := MsNewGetDados():New( 028, 003, 106, 394, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)

Return

Static Function Salva
// Salva uma nova revis�o

	Local _i

	For _i := 1 to Len(OMSRevisao:ACOLS)
		If OMSRevisao:ACOLS[_i,len(OMSRevisao:AHEADER)+1] == .F.
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+SubStr(SB4->B4_COD,1,8))

			While !EOF() .AND. SubStr(SB1->B1_COD,1,8) == SubStr(SB4->B4_COD,1,8)
				If OMSRevisao:ACOLS[_i,3] = "1"
					RecLock("SB1",.F.)
					Replace B1_REVATU 	with OMSRevisao:ACOLS[_i,1]
					MsUnLock()
				EndIf

				DbSelectArea("SG5")
				DbSetOrder(1)
				DbSeek(xfilial("SG5")+SB1->B1_COD+OMSRevisao:ACOLS[_i,1])

				If !Found()

					Reclock("SG5",.T.)
					G5_FILIAL		:= xfilial("SG5")
					G5_PRODUTO		:= SB1->B1_COD
					G5_REVISAO		:= OMSRevisao:ACOLS[_i,1]
					G5_OBS			:= OMSRevisao:ACOLS[_i,2]
					G5_USER			:= __cUserID
					G5_DATAVIG		:= dDatabase
					G5_STATUS		:= OMSRevisao:ACOLS[_i,3]
					MsUnLock()
				Else
					RecLock("SG5",.F.)
					Replace G5_OBS			with OMSRevisao:ACOLS[_i,2]
					Replace G5_STATUS		with OMSRevisao:ACOLS[_i,3]
					MsUnLock()
				EndIf
				DbSelectArea("SB1")
				DbSkip()
			EndDo


			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+"B"+SubStr(SB4->B4_COD,1,7))

			While !EOF() .AND. SubStr(SB1->B1_COD,1,8) == "B"+SubStr(SB4->B4_COD,1,7)
				If OMSRevisao:ACOLS[_i,3] = "1"
					RecLock("SB1",.F.)
					Replace B1_REVATU 	with OMSRevisao:ACOLS[_i,1]
					MsUnLock()
				EndIf

				DbSelectArea("SG5")
				DbSetOrder(1)
				DbSeek(xfilial("SG5")+SB1->B1_COD+OMSRevisao:ACOLS[_i,1])

				If !Found()

					Reclock("SG5",.T.)
					G5_FILIAL		:= xfilial("SG5")
					G5_PRODUTO		:= SB1->B1_COD
					G5_REVISAO		:= OMSRevisao:ACOLS[_i,1]
					G5_OBS			:= OMSRevisao:ACOLS[_i,2]
					G5_USER			:= __cUserID
					G5_DATAVIG		:= dDatabase
					G5_STATUS		:= OMSRevisao:ACOLS[_i,3]
					MsUnLock()
				Else
					RecLock("SG5",.F.)
					Replace G5_OBS			with OMSRevisao:ACOLS[_i,2]
					Replace G5_STATUS		with OMSRevisao:ACOLS[_i,3]
					MsUnLock()
				EndIf
				DbSelectArea("SB1")
				DbSkip()
			EndDo

		EndIf
	Next
Return()

Static Function Seleciona(_tp, _Rev)
// Seleciona a revis�o e abre a tela para cadastro de estrutura

	Local oButton1
	Local oCancela
	Local oDescProd
	Local cDescProd := SB4->B4_DESC
	Local oProduto
	Local oRevisao
	Local oSay1
	Local oSay2
	Local _w := 0
	Local lusr := .T.
	Private cProduto := cCodProd
	Private cRevisao := _Rev
	Private _fecha := .T.
	Private cProxIndice := ""
	Static oEst

	DbSelectArea("ZAU")
	DbSetOrder(1) // ZAU_FILIAL+ZAU_CODREF+ZAU_GRUPO+ZAU_COR+ZAU_ITEM+ZAU_PRODUT
	DbSeek(xfilial("ZAU")+Alltrim(SB4->B4_COD))

	If Found()
		Alert("Este produto possui cadastro de Opcionais e n�o pode ser alterado por esta rotina!")
		Return
	EndIf

	If GETBKP()


		_salva := 0

		//	DEFINE MSDIALOG oEst TITLE "Estrutura de Produtos" FROM 000, 000  TO 600, 1300 COLORS 0, 16777215 PIXEL
		DEFINE MsDialog oEst FROM 000,000 To 600,1300 Title "Estrutura de Produtos" Pixel Style DS_MODALFRAME // Cria Dialog sem o bot�o de Fechar.

		@ 007, 012 SAY oSay1 PROMPT "Produto" SIZE 025, 007 OF oEst COLORS 0, 16777215 PIXEL
		@ 005, 045 MSGET oProduto VAR cProduto SIZE 060, 010 OF oEst COLORS 0, 16777215 READONLY PIXEL
		@ 005, 112 MSGET oDescProd VAR cDescProd SIZE 345, 010 OF oEst COLORS 0, 16777215 READONLY PIXEL
		@ 007, 467 SAY oSay2 PROMPT "Revis�o" SIZE 025, 007 OF oEst COLORS 0, 16777215 PIXEL
		@ 005, 495 MSGET oRevisao VAR cRevisao SIZE 029, 010 OF oEst COLORS 0, 16777215 READONLY PIXEL
		fMSProduto(_tp)
		fMSCor()
		fMSQtd()
		fMSTam()
		If __cuserid $ GETMV("HP_USRPROD")
			@ 280, 530 BUTTON oButton1 PROMPT "Ok" 						SIZE 050, 012 ACTION (EstSav(),Iif(_fecha,(oEst:End(),oDlg:End()),MessageBox("Tamanhos ou Cores n�o cadastrados!","ATEN��O",16))) OF oEst PIXEL
		EndIf
		//@ 280, 590 BUTTON oCancela PROMPT "Cancela" 				SIZE 050, 012 ACTION (oEst:End(),oDlg:End()) OF oEst PIXEL //Aguardando Valida��o Marly
		If __cuserid $ GETMV("HP_USRPROD")
			//@ 280, 590 BUTTON oCancela PROMPT "Cancela"                 SIZE 050, 012 ACTION (IIf(RESBKP(),(oEst:End(),oDlg:End()),MessageBox("Refer�ncia n�o possui Backup salvo, favor sair da tela clicando no OK, para salvar o Backup da mesma!","ATEN��O",16))) OF oEst PIXEL
			//	@ 280, 590 BUTTON oCancela PROMPT "Cancela"                 SIZE 050, 012 ACTION (IIf(RESBKP(),(oEst:End(),oDlg:End()),)) OF oEst PIXEL
		Else
			@ 280, 590 BUTTON oCancela PROMPT "Sair"                 SIZE 050, 012 ACTION (IIf(.T.,(oEst:End(),oDlg:End()),)) OF oEst PIXEL
		EndIf
		//@ 280, 210 BUTTON oButton6 PROMPT "Limpa Cor"				SIZE 050, 012 ACTION LimpaCor() OF oEst PIXEL
		@ 280, 270 BUTTON oButton3 PROMPT "Repl.Cor"				SIZE 050, 012 ACTION ReplyCor() OF oEst PIXEL
		@ 280, 330 BUTTON oButton4 PROMPT "Repl.Tam"				SIZE 050, 012 ACTION ReplyTam() OF oEst PIXEL
		@ 280, 390 BUTTON oButton5 PROMPT "Repl.Qtd x Grade"		SIZE 050, 012 ACTION ReplyQtdLn() OF oEst PIXEL
		@ 280, 450 BUTTON oButton6 PROMPT "Repl.Qtd x Linha"		SIZE 050, 012 ACTION ReplyQtdCl() OF oEst PIXEL

		ACTIVATE MSDIALOG oEst CENTERED

	Endif
Return

Static Function Visualiza(_tp, _Rev)
// Seleciona a revis�o e abre a tela para cadastro de estrutura

	Local oButton1
	Local oCancela
	Local oDescProd
	Local cDescProd := SB4->B4_DESC
	Local oProduto
	Local oRevisao
	Local oSay1
	Local oSay2
	Local _w := 0
	Local lusr := .T.
	Private cProduto := cCodProd
	Private cRevisao := _Rev
	Private _fecha := .T.
	Private cProxIndice := ""


	Static oEst




	_salva := 0

	DEFINE MsDialog oEst FROM 000,000 To 600,1300 Title "Estrutura de Produtos" Pixel Style DS_MODALFRAME // Cria Dialog sem o bot�o de Fechar.

	@ 007, 012 SAY oSay1 PROMPT "Produto" SIZE 025, 007 OF oEst COLORS 0, 16777215 PIXEL
	@ 005, 045 MSGET oProduto VAR cProduto SIZE 060, 010 OF oEst COLORS 0, 16777215 READONLY PIXEL
	@ 005, 112 MSGET oDescProd VAR cDescProd SIZE 345, 010 OF oEst COLORS 0, 16777215 READONLY PIXEL
	@ 007, 467 SAY oSay2 PROMPT "Revis�o" SIZE 025, 007 OF oEst COLORS 0, 16777215 PIXEL
	@ 005, 495 MSGET oRevisao VAR cRevisao SIZE 029, 010 OF oEst COLORS 0, 16777215 READONLY PIXEL
	fMSProdV(_tp)
	fMSCorV()
	fMSQtdV()
	fMSTamV()
	@ 280, 590 BUTTON oCancela PROMPT "Sair"                 SIZE 050, 012 ACTION (IIf(.T.,(oEst:End(),oDlg:End()),)) OF oEst PIXEL

	ACTIVATE MSDIALOG oEst CENTERED


Return

STATIC FUNCTION RESBKP

	Local _w := 0
	Local lOk := .T.
	Local lRef := .T.
	Local lret1 := .T.

	If !lLmpCon
		If !lLmpCor
			// TO DO INICIO VALIDA��O PARA VERIFICAR SE EXISTE REGISTRO DA REF NAS TABELAS CUSTOMIZADAS
			DbSelectArea("SZE")
			DbSetOrder(1)
			If !DbSeek(xfilial("SZE")+substr(cProduto,1,15)+cRevisao) .AND. lOk
				lRef := .F.
			EndIf
			DbSelectArea("SZF")
			DbSetOrder(1)
			If !DbSeek(xfilial("SZF")+substr(cProduto,1,15)+cRevisao) .AND. lOk
				lRef := .F.
			EndIf
			DbSelectArea("SZG")
			DbSetOrder(1)
			If !DbSeek(xfilial("SZG")+substr(cProduto,1,15)+cRevisao) .AND. lOk
				lRef := .F.
			EndIf
			DbSelectArea("SZK")
			DbSetOrder(1)
			If !DbSeek(xfilial("SZK")+substr(cProduto,1,15)+cRevisao) .AND. lOk
				lRef := .F.
			EndIf
			// TO DO FIM VALIDA��O PARA VERIFICAR SE EXISTE REGISTRO DA REF NAS TABELAS CUSTOMIZADAS
			// CASO EXISTA ELE N�O VERIFICA O BKP E PERMITE FECHA A TELA COM A REFER�NCIA VAZIA


			// TO DO INICIO VALIDA��O PARA VERIFICAR SE EXISTE BACKUP DA REFER�NCIA
			If lRef
				DbSelectArea("ZB0")
				DbSetOrder(1)
				If !DbSeek(xfilial("ZB0")+substr(cProduto,1,15)+cRevisao)
					lOk := .F.
				EndIf
				DbSelectArea("ZB1")
				DbSetOrder(1)
				If !DbSeek(xfilial("ZB1")+substr(cProduto,1,15)+cRevisao) .AND. lOk
					lOk := .F.
				EndIf
				DbSelectArea("ZB2")
				DbSetOrder(1)
				If !DbSeek(xfilial("ZB2")+substr(cProduto,1,15)+cRevisao) .AND. lOk
					lOk := .F.
				EndIf
				DbSelectArea("ZB3")
				DbSetOrder(1)
				If !DbSeek(xfilial("ZB3")+substr(cProduto,1,15)+cRevisao) .AND. lOk
					lOk := .F.
				EndIf
			Else // SE A REFER�NCIA POSSUIR REGISTRO EM TODAS TABELAS CUSTOMIZADAS (SZE,SZF,SZG ou SZK), N�O PERMITE SAIR DA TELA SEM O BACKUP SALVO
				lret1 := .T.

				_qry := "delete "+RetSqlName("SZE")+"  WHERE ZE_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' AND ZE_REVISAO = '"+cRevisao+"' AND D_E_L_E_T_ = '' "
				nStatus := TcSqlExec(_qry)

				_qry := "delete "+RetSqlName("SZF")+"  WHERE ZF_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' AND ZF_REVISAO = '"+cRevisao+"'  AND D_E_L_E_T_ = '' "
				nStatus := TcSqlExec(_qry)

				_qry := "delete "+RetSqlName("SZG")+"  WHERE ZG_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' AND ZG_REVISAO = '"+cRevisao+"'  AND D_E_L_E_T_ = '' "
				nStatus := TcSqlExec(_qry)

				_qry := "delete "+RetSqlName("SZK")+"  WHERE ZK_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' AND ZK_REVISAO = '"+cRevisao+"'  AND D_E_L_E_T_ = '' "
				nStatus := TcSqlExec(_qry)

				Return(lret1)
			EndIf
			// TO DO FIM VALIDA��O PARA VERIFICAR SE EXISTE BACKUP DA REFER�NCIA

			If lOk
				_qry := "UPDATE "+RetSqlName("SZE")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ WHERE ZE_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' AND ZE_REVISAO = '"+cRevisao+"'  "
				nStatus := TcSqlExec(_qry)

				If (nStatus < 0)
					MsgBox(TCSQLError(), "Erro no comando", "Stop")
				EndIf
				_qry := "UPDATE "+RetSqlName("SZF")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ WHERE ZF_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' AND ZF_REVISAO = '"+cRevisao+"' "
				nStatus := TcSqlExec(_qry)

				If (nStatus < 0)
					MsgBox(TCSQLError(), "Erro no comando", "Stop")
				EndIf
				_qry := "UPDATE "+RetSqlName("SZG")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ WHERE ZG_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' AND ZG_REVISAO = '"+cRevisao+"'  "
				nStatus := TcSqlExec(_qry)

				If (nStatus < 0)
					MsgBox(TCSQLError(), "Erro no comando", "Stop")
				EndIf
				_qry := "UPDATE "+RetSqlName("SZK")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ WHERE ZK_PRODUTO = '"+SUBSTR(cProduto,1,8)+"' AND ZK_REVISAO = '"+cRevisao+"' "
				nStatus := TcSqlExec(_qry)

				If (nStatus < 0)
					MsgBox(TCSQLError(), "Erro no comando", "Stop")
				EndIf

				DbSelectArea("ZB0")
				DbSetOrder(1)
				ZB0->(DbGotop())
				DbSeek(xfilial("ZB0")+substr(cProduto,1,15)+cRevisao)

				While !EOF() .AND. ZB0->ZB0_PRODUT = substr(cProduto,1,15) .AND. ZB0->ZB0_REVISA = cRevisao
					RecLock("SZE",.t.)
					Replace ZE_FILIAL	with ZB0->ZB0_FILIAL
					Replace ZE_PRODUTO	with ZB0->ZB0_PRODUT
					Replace ZE_DESCPRO	with ZB0->ZB0_DESCPR
					Replace ZE_COMP		with ZB0->ZB0_COMP
					Replace ZE_DESCCOM	with ZB0->ZB0_DESCCO
					Replace ZE_UM		with ZB0->ZB0_UM
					Replace ZE_CORTE	with ZB0->ZB0_CORTE
					Replace ZE_REVISAO	with ZB0->ZB0_REVISA
					Replace ZE_INDICE	with ZB0->ZB0_INDICE
					MsUnLock()

					DbSelectArea("ZB0")
					DbSkip()
				EndDo

				DbSelectArea("ZB1")
				DbSetOrder(1)
				DbSeek(xfilial("ZB1")+substr(cProduto,1,15)+cRevisao)

				While !EOF() .AND. ZB1->ZB1_PRODUT = substr(cProduto,1,15) .AND. ZB1->ZB1_REVISA = cRevisao
					RecLock("SZF",.t.)
					Replace ZF_FILIAL	with ZB1->ZB1_FILIAL
					Replace ZF_PRODUTO	with ZB1->ZB1_PRODUT
					Replace ZF_COMP		with ZB1->ZB1_COMP
					Replace ZF_REVISAO	with ZB1->ZB1_REVISA
					Replace ZF_INDICE	with ZB1->ZB1_INDICE
					Replace ZF_COR		with ZB1->ZB1_COR
					For _w := 1 to 200
						_campo1 := "ZF_TAM"+StrZero(_w,3)
						_campo2 := "ZB1->ZB1_TAM"+StrZero(_w,3)
						Replace &(_campo1) with &(_campo2)
					Next
					MsUnLock()

					DbSelectArea("ZB1")
					DbSkip()
				EndDo

				DbSelectArea("ZB2")
				DbSetOrder(1)
				DbSeek(xfilial("ZB2")+substr(cProduto,1,15)+cRevisao)

				While !EOF() .AND. ZB2->ZB2_PRODUT = substr(cProduto,1,15) .AND. ZB2->ZB2_REVISA = cRevisao
					RecLock("SZG",.t.)
					Replace ZG_FILIAL	with ZB2->ZB2_FILIAL
					Replace ZG_PRODUTO	with ZB2->ZB2_PRODUT
					Replace ZG_COMP		with ZB2->ZB2_COMP
					Replace ZG_REVISAO	with ZB2->ZB2_REVISA
					Replace ZG_INDICE	with ZB2->ZB2_INDICE
					Replace ZG_COR		with ZB2->ZB2_COR
					Replace ZG_CORNV	with ZB2->ZB2_CORNV
					MsUnLock()

					DbSelectArea("ZB2")
					DbSkip()
				EndDo

				DbSelectArea("ZB3")
				DbSetOrder(1)
				DbSeek(xfilial("ZB3")+substr(cProduto,1,15)+cRevisao)

				While !EOF() .AND. ZB3->ZB3_PRODUT = substr(cProduto,1,15) .AND. ZB3->ZB3_REVISA = cRevisao
					RecLock("SZK",.t.)
					Replace ZK_FILIAL	with ZB3->ZB3_FILIAL
					Replace ZK_PRODUTO	with ZB3->ZB3_PRODUT
					Replace ZK_COMP		with ZB3->ZB3_COMP
					Replace ZK_REVISAO	with ZB3->ZB3_REVISA
					Replace ZK_INDICE	with ZB3->ZB3_INDICE
					Replace ZK_TAM		with ZB3->ZB3_TAM
					Replace ZK_TAMNV	with ZB3->ZB3_TAMNV
					MsUnLock()

					DbSelectArea("ZB3")
					DbSkip()
				EndDo
				lret1 := .T.
			Else
				lret1 := .F.
			EndIf
		Else
			MessageBox("Fun��o Limpa Cor utilizada, favor sair com o OK para que as altera��es sejam salvas","LIMPACOR",16)
			lret1 := .F.
		EndIf
	Else
		MessageBox("Fun��o Limpa Consumo utilizada, favor sair com o OK para que as altera��es sejam salvas","LIMPACON",16)
		lret1 := .F.
	EndIf

	If !lret1 .and.  !lLmpCor .and.  !lLmpCon
		MessageBox("Refer�ncia n�o possui Backup salvo, favor sair da tela clicando no OK, para salvar o Backup da mesma!","ATEN��O",16)
	EndIf
Return(lret1)

//------------------------------------------------ 
Static Function fMSProduto(_tp)
//------------------------------------------------ 
// Acols para componentes das estruturas
// Tabela: SZE

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZE_COMP","ZE_DESCCOM","ZE_UM","ZE_CORTE","ZE_INDICE"}
	Local aAlterFields := {"ZE_COMP","ZE_CORTE"}
	Public oMSNewGe1

	U_ProxIndice(cProduto)

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZE_COMP"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"ExistCpo('SB4')",;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZE_DESCCOM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZE_UM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZE_CORTE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZE_INDICE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf

	// Define field values
	DbSelectArea("SZE")
	DbSetOrder(1)
	DbSeek(xfilial("SZE")+substr(cProduto,1,15)+cRevisao)

	If Found()
		While !EOF() .AND. alltrim(SZE->ZE_PRODUTO) == alltrim(cProduto) .AND. SZE->ZE_REVISAO == cRevisao
			Aadd(aColsEx, {SZE->ZE_COMP,Posicione("SB4",1,xfilial("SB4")+SZE->ZE_COMP,"B4_DESC"),Posicione("SB4",1,xfilial("SB4")+SZE->ZE_COMP,"B4_UM"),SZE->ZE_CORTE,SZE->ZE_INDICE,.F.})

			DbSelectArea("SZE")
			DbSkip()
		EndDo
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			EndIf
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
	EndIf

	If __cuserid $ GETMV("HP_USRPROD")
		oMSNewGe1 := MsNewGetDados():New( 027, 006, 185, 470, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	Else
		oMSNewGe1 := MsNewGetDados():New( 027, 006, 185, 470, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	EndIf
	// Chamada de rotina para atualiza��o de demais acols quando da mudan�a de linha no acols de componentes
	oMSNewGe1:bchange	:= {|| U_AtuAcols() }

Return

User Function CORTSIM(cProduto,cComp)

	Local cTpProd := Posicione("SB4",1,xfilial("SB4")+cProduto,"B4_TIPO")

	If cTpProd $ "PF|PI" .AND. SUBSTRING(cComp,1,4) $ GetMv("HP_MPSIMCO")
		cCorte := "S"
	Else
		cCorte := "N"
	EndIf

Return cCorte

//------------------------------------------------ 
Static Function fMSProdV(_tp)
//------------------------------------------------ 
// Acols para componentes das estruturas
// Tabela: SZE

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZE_COMP","ZE_DESCCOM","ZE_UM","ZE_CORTE","ZE_INDICE"}
	Local aAlterFields := {"ZE_COMP","ZE_CORTE"}
	Public oMSNewGe1

	//U_ProxIndice(cProduto)

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZE_COMP"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"ExistCpo('SB4')",;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZE_DESCCOM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZE_UM"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZE_CORTE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
	If SX3->(DbSeek("ZE_INDICE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf

	// Define field values
	DbSelectArea("SZE")
	DbSetOrder(1)
	DbSeek(xfilial("SZE")+substr(cProduto,1,15)+cRevisao)

	If Found()
		While !EOF() .AND. alltrim(SZE->ZE_PRODUTO) == alltrim(cProduto) .AND. SZE->ZE_REVISAO == cRevisao
			Aadd(aColsEx, {SZE->ZE_COMP,Posicione("SB4",1,xfilial("SB4")+SZE->ZE_COMP,"B4_DESC"),Posicione("SB4",1,xfilial("SB4")+SZE->ZE_COMP,"B4_UM"),SZE->ZE_CORTE,SZE->ZE_INDICE,.F.})

			DbSelectArea("SZE")
			DbSkip()
		EndDo
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			EndIf
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
	EndIf


	oMSNewGe1 := MsNewGetDados():New( 027, 006, 185, 470, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)

	oMSNewGe1:bchange	:= {|| U_AtuAcols() }

Return
//------------------------------------------------ 
Static Function fMSCorV()
//------------------------------------------------ 
// Monta acols de Cores
// Tabela: SZG

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZG_COR","ZG_CORNV","ZG_COMP","ZG_INDICE"}
	Local aAlterFields := {"ZG_CORNV"}
	Static oMSCor

	cCor := ""
	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If aFields[nX] <> "ZG_CORNV"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		Else
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(2,M->ZG_CORNV,M->ZG_COR)",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		EndIf
	Next nX

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)

			DbSelectArea("SZG")
			DbSetOrder(2)
			DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+PADR(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SZD->ZD_COR)

			If Found()

				Aadd(aColsEx, {SZG->ZG_COR,SZG->ZG_CORNV,SZG->ZG_COMP,SZG->ZG_INDICE,.F.})

			Else
				Aadd(aColsEx, {SZD->ZD_COR,"   ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
			EndIf

			DbSelectArea("SZD")
			DbSkip()
		EndDo

	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			Aadd(aColsEx, {SZD->ZD_COR,"   ",space(15),space(3),.F.})

			DbSelectArea("SZD")
			DbSkip()
		EndDo
	EndIf


	oMSCor := MsNewGetDados():New( 027, 475, 185, 645, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)

Return

//------------------------------------------------ 
Static Function fMSCor()
//------------------------------------------------ 
// Monta acols de Cores
// Tabela: SZG

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZG_COR","ZG_CORNV","ZG_COMP","ZG_INDICE"}
	Local aAlterFields := {"ZG_CORNV"}
	Static oMSCor

	cCor := ""
	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If aFields[nX] <> "ZG_CORNV"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		Else
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(2,M->ZG_CORNV)",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		EndIf
	Next nX

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)

			DbSelectArea("SZG")
			DbSetOrder(2)
			DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+PADR(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SZD->ZD_COR)

			If Found()

				Aadd(aColsEx, {SZG->ZG_COR,SZG->ZG_CORNV,SZG->ZG_COMP,SZG->ZG_INDICE,.F.})

			Else
				Aadd(aColsEx, {SZD->ZD_COR,"   ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
			EndIf

			DbSelectArea("SZD")
			DbSkip()
		EndDo

	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			Aadd(aColsEx, {SZD->ZD_COR,"   ",space(15),space(3),.F.})

			DbSelectArea("SZD")
			DbSkip()
		EndDo
	EndIf

	If __cuserid $ GETMV("HP_USRPROD")
		oMSCor := MsNewGetDados():New( 027, 475, 185, 645, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	Else
		oMSCor := MsNewGetDados():New( 027, 475, 185, 645, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	EndIf

Return
/*
//------------------------------------------------ 
Static Function fMSQtd()
//------------------------------------------------ 
// Acols para Quandidades por tamanho
// Tabela: SZF

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZF_COR","ZF_COR","ZF_INDICE"}
	Local aAlterFields := {}
	Local _i
	Static oMSQtd

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZF_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZF_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
		Aadd(aHeaderEx, {"Descri��o"+Space(4),"ZF_COR","@!",10,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	DbSelectArea("SZD")
	DbSetOrder(1)
	DbSeek(xfilial("SZD")+alltrim(cProduto))
	_XTAM := {}
	While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
		For _i := 1 to 200
			_campo := "SZD->ZD_TAM"+StrZero(_i,3)
			If &_campo == "X"
				_tem := ASCAN( _xtam, {|x| x == _i})
				If _tem == 0
					aadd(_xtam,_i)
				EndIf
			EndIf
		Next

		DbSelectarea("SZD")
		DbSkip()
	End

	aSort(_xtam)

	For _i := 1 to len(_xtam)
		DbSelectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+alltrim(cproduto))
		_desc := rettam(_xtam[_i],SB4->B4_COLUNA,"D")
		DbSelectArea("SX3")
		_campo := "ZF_TAM"+StrZero(_xtam[_i],3)
		Aadd(aFields,_campo)
		Aadd(aAlterFields,_campo)

		SX3->(DbSetOrder(2))
		If SX3->(DbSeek(_campo))
			Aadd(aHeaderEx, {alltrim(_desc)+Space(4),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(7,M->ZF_PRODUTO)",;
				SX3->X3_USADO,SX3->X3_TIPO,"",SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		EndIf

	Next

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZF_COMP"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZF_COMP","@!",15,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf
//  Aadd(aFields,"ZF_COMP") 

	If SX3->(DbSeek("ZF_INDICE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf
//  Aadd(aFields,"ZF_INDICE")

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""

		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[oMSNewGe1:nAT,1]+oMSNewGe1:aCols[oMSNewGe1:nAT,5])

		If Found()

			While !EOF() .AND. alltrim(ZF_PRODUTO) == alltrim(cProduto) .AND. alltrim(ZF_COMP) == alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) .AND. ZF_REVISAO == cRevisao .AND. ZF_INDICE = oMSNewGe1:aCols[oMSNewGe1:nAT,5]
				aFieldFill := {}
				Aadd(aFieldFill, SZF->ZF_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZF->ZF_COR,"BV_DESCRI"))
				For nX := 3 to Len(aHeaderEx)-2
					_campo := "SZF->"+aheaderEx[nX][2]//ZF_TAM"+StrZero(nX,3)
					Aadd(aFieldFill,&_campo)
				Next
				Aadd(aFieldFill, SZF->ZF_COMP)
				Aadd(aFieldFill, SZF->ZF_INDICE)
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)

				DbSelectArea("SZF")
				DbSkip()
			End
		Else
			DbSelectArea("SZD")
			DbSetOrder(1)
			DbSeek(xfilial("SZD")+alltrim(cProduto))

			While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
				aFieldFill := {}
				Aadd(aFieldFill, SZD->ZD_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

				For nX := 3 to Len(aHeaderEx)-2
					Aadd(aFieldFill,0)
				Next
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,5])
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)

				DbSelectArea("SZD")
				DbSkip()
			End
		EndIf
	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			aFieldFill := {}
			Aadd(aFieldFill, SZD->ZD_COR)
			Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

			For nX := 3 to Len(aHeaderEx)-2
				Aadd(aFieldFill,0)
			Next
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,2])
			Aadd(aFieldFill, .F.)
			Aadd(aColsEx, aFieldFill)

			DbSelectArea("SZD")
			DbSkip()
		End
	EndIf

	If __cuserid $ GETMV("HP_USRPROD")
		oMSQtd := MsNewGetDados():New( 190, 006, 275, 500, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	Else
		oMSQtd := MsNewGetDados():New( 190, 006, 275, 500, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	EndIf
Return
*/
//------------------------------------------------ 
Static Function fMSQtd()
//------------------------------------------------ 
// Acols para Quandidades por tamanho
// Tabela: SZF

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZF_COR","ZF_COR","ZF_INDICE"}
	Local aAlterFields := {}
	Local _i
	Static oMSQtd

// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZF_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZF_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
		Aadd(aHeaderEx, {"Descri��o"+Space(4),"ZF_COR","@!",10,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If alltrim(cColuna) <> ""

		// Busca os tamanhos dispon�veis selecionados na tabela SBV

		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		_XTAMZ:={}
		While TMPSBV->(!EOF()) .AND. alltrim(TMPSBV->BV_TABELA) == alltrim(cColuna)
			Aadd(_XTAMZ,TMPSBV->BV_XCAMPO)
			DbSkip()
		EndDo

		TMPSBV->(DbGoTop())

		cqryc := "SELECT COUNT(*)+1 AS QTD FROM " +RetSQLName("SBV") + "(NOLOCK) WHERE BV_XCAMPO <> '' AND D_E_L_E_T_ ='' AND BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' "

		If SELECT("TMPSBVC") > 0
			TMPSBVC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cqryc),"TMPSBVC",.T.,.T.)

		nzd:= TMPSBVC->QTD

		While TMPSBV->(!EOF())
			If Empty(TMPSBV->BV_XCAMPO)
				DbSelectArea("SBV")
				DbSetOrder(1)
				If DbSeek(xfilial("SBV")+TMPSBV->BV_TABELA+TMPSBV->BV_CHAVE)
					RecLock("SBV",.F.)
					SBV->BV_XCAMPO := "ZD_TAM"+Padl(cvaltochar(nzd),3,"0")
					MsUnLock()
					nzd ++
				EndIf
			EndIf
			TMPSBV->(DbSkip())
		EndDo

		// Busca os tamanhos dispon�veis selecionados na tabela SBV

		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO,R_E_C_N_O_ FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		_XTAMZ:={}
		While TMPSBV->(!EOF()) .AND. alltrim(TMPSBV->BV_TABELA) == alltrim(cColuna)
			Aadd(_XTAMZ,{TMPSBV->BV_XCAMPO,TMPSBV->R_E_C_N_O_})
			DbSkip()
		EndDo

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")

			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY BV_CHAVE,R_E_C_N_O_"

			cQuery := ChangeQuery(cQuery)

			If SELECT("TMPSBV") > 0
				TMPSBV->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

			_XTAMZ:={}
			While TMPSBV->(!EOF()) .AND. alltrim(TMPSBV->BV_TABELA) == alltrim(cColuna)
				Aadd(_XTAMZ,{TMPSBV->BV_XCAMPO,TMPSBV->BV_CHAVE})
				DbSkip()
			EndDo

		EndIf
	EndIf

	DbSelectArea("SZD")
	DbSetOrder(1)
	DbSeek(xfilial("SZD")+alltrim(cProduto))
	_XTAM := {}
	While !EOF() .AND. Alltrim(SZD->ZD_PRODUTO) == Alltrim(cProduto)
		For _i := 1 to LEN(_XTAMZ)
			_campo := "SZD->"+_XTAMZ[_i][1]
			_chavebv := _XTAMZ[_i][2]
			If &_campo == "X"
				_tem := Ascan(_xtam,{|x| x[1] == _campo})
				If _tem == 0
					aadd(_xtam,{_campo,_chavebv})
				EndIf
			EndIf
		Next
		DbSelectarea("SZD")
		DbSkip()
	EndDo

	ASORT(_xtam,,,{ |x,y| x[2] < y[2] } )

	For _i := 1 to len(_xtam)
		DbSelectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+alltrim(cproduto))
		_desc := rettam4(SB4->B4_COLUNA,SUBSTRING(_xtam[_i][1],6,LEN(_xtam[_i][1])))
		DbSelectArea("SX3")
		_campo := "ZF_TAM"+SUBSTRING(_xtam[_i][1],12,LEN(_xtam[_i][1]))
		Aadd(aFields,_campo)
		Aadd(aAlterFields,_campo)

		SX3->(DbSetOrder(2))
		If SX3->(DbSeek(_campo))
			Aadd(aHeaderEx, {alltrim(_desc)+Space(4),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(7,M->ZF_PRODUTO)",;
				SX3->X3_USADO,SX3->X3_TIPO,"",SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		EndIf

	Next

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZF_COMP"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZF_COMP","@!",15,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If SX3->(DbSeek("ZF_INDICE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""

		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[oMSNewGe1:nAT,1]+oMSNewGe1:aCols[oMSNewGe1:nAT,5])

		If Found()

			While !EOF() .AND. alltrim(ZF_PRODUTO) == alltrim(cProduto) .AND. alltrim(ZF_COMP) == alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) .AND. ZF_REVISAO == cRevisao .AND. ZF_INDICE = oMSNewGe1:aCols[oMSNewGe1:nAT,5]
				aFieldFill := {}
				Aadd(aFieldFill, SZF->ZF_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZF->ZF_COR,"BV_DESCRI"))
				For nX := 3 to Len(aHeaderEx)-2

					Aadd(aFieldFill,"")
				Next

				For nX := 3 to Len(aHeaderEx)-2
					_campo := "SZF->"+aheaderEx[nX][2]
					_cfor := aheaderEx[nX][2]
					npos:= Ascan(aheaderex,{|x| x[2] == _cfor})

					If npos > 0
						aFieldFill[nPos]:= &_campo
					EndIf
				Next
				Aadd(aFieldFill, SZF->ZF_COMP)
				Aadd(aFieldFill, SZF->ZF_INDICE)
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)

				DbSelectArea("SZF")
				DbSkip()
			EndDo
		Else
			DbSelectArea("SZD")
			DbSetOrder(1)
			DbSeek(xfilial("SZD")+alltrim(cProduto))

			While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
				aFieldFill := {}
				Aadd(aFieldFill, SZD->ZD_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

				For nX := 3 to Len(aHeaderEx)-2
					Aadd(aFieldFill,0)
				Next
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,5])
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)

				DbSelectArea("SZD")
				DbSkip()
			EndDo
		EndIf
	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			aFieldFill := {}
			Aadd(aFieldFill, SZD->ZD_COR)
			Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

			For nX := 3 to Len(aHeaderEx)-2
				Aadd(aFieldFill,0)
			Next
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,2])
			Aadd(aFieldFill, .F.)
			Aadd(aColsEx, aFieldFill)

			DbSelectArea("SZD")
			DbSkip()
		EndDo
	EndIf

	If __cuserid $ GETMV("HP_USRPROD")
		oMSQtd := MsNewGetDados():New( 190, 006, 275, 500, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	Else
		oMSQtd := MsNewGetDados():New( 190, 006, 275, 500, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	EndIf

Return
//------------------------------------------------ 
Static Function fMSQtdV()
//------------------------------------------------ 
// Acols para Quandidades por tamanho
// Tabela: SZF

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZF_COR","ZF_COR","ZF_INDICE"}
	Local aAlterFields := {}
	Local _i
	Static oMSQtd

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZF_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZF_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
		Aadd(aHeaderEx, {"Descri��o"+Space(4),"ZF_COR","@!",10,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If alltrim(cColuna) <> ""

		// Busca os tamanhos dispon�veis selecionados na tabela SBV

		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		_XTAMZ:={}
		While TMPSBV->(!EOF()) .AND. alltrim(TMPSBV->BV_TABELA) == alltrim(cColuna)
			Aadd(_XTAMZ,TMPSBV->BV_XCAMPO)
			DbSkip()
		EndDo

		TMPSBV->(DbGoTop())

		cqryc := "SELECT COUNT(*)+1 AS QTD FROM " +RetSQLName("SBV") + "(NOLOCK) WHERE BV_XCAMPO <> '' AND D_E_L_E_T_ ='' AND BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' "

		If SELECT("TMPSBVC") > 0
			TMPSBVC->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cqryc),"TMPSBVC",.T.,.T.)

		nzd:= TMPSBVC->QTD

		While TMPSBV->(!EOF())
			If Empty(TMPSBV->BV_XCAMPO)
				DbSelectArea("SBV")
				DbSetOrder(1)
				If DbSeek(xfilial("SBV")+TMPSBV->BV_TABELA+TMPSBV->BV_CHAVE)
					RecLock("SBV",.F.)
					SBV->BV_XCAMPO := "ZD_TAM"+Padl(cvaltochar(nzd),3,"0")
					MsUnLock()
					nzd ++
				EndIf
			EndIf
			TMPSBV->(DbSkip())
		EndDo

		// Busca os tamanhos dispon�veis selecionados na tabela SBV

		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO,R_E_C_N_O_ FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		_XTAMZ:={}
		While TMPSBV->(!EOF()) .AND. alltrim(TMPSBV->BV_TABELA) == alltrim(cColuna)
			Aadd(_XTAMZ,{TMPSBV->BV_XCAMPO,TMPSBV->R_E_C_N_O_})
			DbSkip()
		EndDo

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")

			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY BV_CHAVE,R_E_C_N_O_"

			cQuery := ChangeQuery(cQuery)

			If SELECT("TMPSBV") > 0
				TMPSBV->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

			_XTAMZ:={}
			While TMPSBV->(!EOF()) .AND. alltrim(TMPSBV->BV_TABELA) == alltrim(cColuna)
				Aadd(_XTAMZ,{TMPSBV->BV_XCAMPO,TMPSBV->BV_CHAVE})
				DbSkip()
			EndDo

		EndIf
	EndIf

	DbSelectArea("SZD")
	DbSetOrder(1)
	DbSeek(xfilial("SZD")+alltrim(cProduto))
	_XTAM := {}
	While !EOF() .AND. Alltrim(SZD->ZD_PRODUTO) == Alltrim(cProduto)
		For _i := 1 to LEN(_XTAMZ)
			_campo := "SZD->"+_XTAMZ[_i][1]
			_chavebv := _XTAMZ[_i][2]
			If &_campo == "X"
				_tem := Ascan(_xtam,{|x| x[1] == _campo})
				If _tem == 0
					aadd(_xtam,{_campo,_chavebv})
				EndIf
			EndIf
		Next
		DbSelectarea("SZD")
		DbSkip()
	EndDo

	ASORT(_xtam,,,{ |x,y| x[2] < y[2] } )

	For _i := 1 to len(_xtam)
		DbSelectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+alltrim(cproduto))
		_desc := rettam4(SB4->B4_COLUNA,SUBSTRING(_xtam[_i][1],6,LEN(_xtam[_i][1])))
		DbSelectArea("SX3")
		_campo := "ZF_TAM"+SUBSTRING(_xtam[_i][1],12,LEN(_xtam[_i][1]))
		Aadd(aFields,_campo)
		Aadd(aAlterFields,_campo)

		SX3->(DbSetOrder(2))
		If SX3->(DbSeek(_campo))
			Aadd(aHeaderEx, {alltrim(_desc)+Space(4),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"",;
				SX3->X3_USADO,SX3->X3_TIPO,"",SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		EndIf

	Next

	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZF_COMP"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZF_COMP","@!",15,0,,SX3->X3_USADO,"C","","R","",""})
	EndIf

	If SX3->(DbSeek("ZF_INDICE"))
		Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
			SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
	EndIf

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""

		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[oMSNewGe1:nAT,1]+oMSNewGe1:aCols[oMSNewGe1:nAT,5])

		If Found()

			While !EOF() .AND. alltrim(ZF_PRODUTO) == alltrim(cProduto) .AND. alltrim(ZF_COMP) == alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) .AND. ZF_REVISAO == cRevisao .AND. ZF_INDICE = oMSNewGe1:aCols[oMSNewGe1:nAT,5]
				aFieldFill := {}
				Aadd(aFieldFill, SZF->ZF_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZF->ZF_COR,"BV_DESCRI"))
				For nX := 3 to Len(aHeaderEx)-2

					Aadd(aFieldFill,"")
				Next

				For nX := 3 to Len(aHeaderEx)-2
					_campo := "SZF->"+aheaderEx[nX][2]
					_cfor := aheaderEx[nX][2]
					npos:= Ascan(aheaderex,{|x| x[2] == _cfor})

					If npos > 0
						aFieldFill[nPos]:= &_campo
					EndIf
				Next
				Aadd(aFieldFill, SZF->ZF_COMP)
				Aadd(aFieldFill, SZF->ZF_INDICE)
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)

				DbSelectArea("SZF")
				DbSkip()
			EndDo
		Else
			DbSelectArea("SZD")
			DbSetOrder(1)
			DbSeek(xfilial("SZD")+alltrim(cProduto))

			While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
				aFieldFill := {}
				Aadd(aFieldFill, SZD->ZD_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

				For nX := 3 to Len(aHeaderEx)-2
					Aadd(aFieldFill,0)
				Next
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,5])
				Aadd(aFieldFill, .F.)
				Aadd(aColsEx, aFieldFill)

				DbSelectArea("SZD")
				DbSkip()
			EndDo
		EndIf
	Else
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			aFieldFill := {}
			Aadd(aFieldFill, SZD->ZD_COR)
			Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

			For nX := 3 to Len(aHeaderEx)-2
				Aadd(aFieldFill,0)
			Next
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,2])
			Aadd(aFieldFill, .F.)
			Aadd(aColsEx, aFieldFill)

			DbSelectArea("SZD")
			DbSkip()
		EndDo
	EndIf

	oMSQtd := MsNewGetDados():New( 190, 006, 275, 500, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)

Return

//------------------------------------------------ 
Static Function fMSTam()
//------------------------------------------------ 
// Acols para Tamanho dos componentes 
// Tabela: SZK

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZK_TAM","ZK_TAMNV","ZK_COMP","ZK_INDICE"}
	Local aAlterFields := {"ZK_TAMNV"}
	Static oMSTAM

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If aFields[nX] <> "ZK_TAMNV"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		Else
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(3,M->ZK_TAMNV)",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		EndIf
	Next nX

	_ac := {}

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SubStr(cProduto,1,8))

		While !EOF() .AND. SubStr(B1_COD,1,8) == SubStr(cProduto,1,8)
			_tem := ASCAN(_aC, {|x| Alltrim(x[2]) == SubStr(SB1->B1_COD,12,4)})
			If _tem == 0
				DbSelectArea("SZK")
				DbSetOrder(2)
				DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+padr(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SubStr(SB1->B1_COD,12,4))

				If Found()
//				While !EOF() .AND. alltrim(ZK_PRODUTO) == alltrim(cProduto) .AND. alltrim(ZK_COMP) == alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) .AND. ZK_REVISAO == cRevisao
					Aadd(_aC, {0,SZK->ZK_TAM,SZK->ZK_TAMNV,oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
//				DbSelectArea("SZK")
//				DbSkip()
//				End
				Else
					DbSelectArea("SB4")
					DbSetOrder(1)
					DbSeek(xfilial("SB4")+oMSNewGe1:aCols[oMSNewGe1:nAT,1])

					DbSelectArea("SBV")
					DbSetOrder(1)
					DbSeek(xfilial("SBV")+SB4->B4_COLUNA)

					If SBV->BV_CHAVE = '00TU'
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"00TU",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					Else
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"   ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					EndIf
				EndIf
			EndIf

			DbSelectArea("SB1")
			DbSkip()
		EndDo
	Else
		Aadd(aColsEx, {Space(3),"   ",Space(15),Space(3),.F.})
	EndIf

	If Len(_ac) > 0
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna+ "' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)
		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		For nX:= 1 to len(_ac)
			DbSelectArea("TMPSBV")
			DbGoTop()
			_w := 0
			While !EOF()
				_w++
				If alltrim(TMPSBV->BV_CHAVE) == alltrim(_ac[nX,2])
					_ac[nX,1] := _w
				EndIf
				DbSkip()
			EndDo

		Next
		DbSelectArea("TMPSBV")
		DbCloseArea()

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")
			ASORT(_ac, , , { |x,y| x[1] < y[1] } )
		EndIf

		For nX:=1 to len(_ac)
			If ASCAN(aColsEx,_ac[nX,2])=0
				Aadd(aColsEx, {_ac[nX,2],_ac[nX,3],_ac[nX,4],_ac[nX,5],.F.})
			EndIf
		Next
	EndIf
	If __cuserid $ GETMV("HP_USRPROD")
		oMSTAM := MsNewGetDados():New( 190, 505, 275, 645, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	Else
		oMSTAM := MsNewGetDados():New( 190, 505, 275, 645, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)
	EndIf
Return

//------------------------------------------------ 
Static Function fMSTamV()
//------------------------------------------------ 
// Acols para Tamanho dos componentes 
// Tabela: SZK

	Local nX
	Local aHeaderEx := {}
	Local aColsEx := {}
	Local aFieldFill := {}
	Local aFields := {"ZK_TAM","ZK_TAMNV","ZK_COMP","ZK_INDICE"}
	Local aAlterFields := {"ZK_TAMNV"}
	Static oMSTAM

	// Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If aFields[nX] <> "ZK_TAMNV"
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,,SX3->X3_VALID,;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		Else
			If SX3->(DbSeek(aFields[nX]))
				Aadd(aHeaderEx, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_ExistCod(3,M->ZK_TAMNV)",;
					SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
			EndIf
		EndIf
	Next nX

	_ac := {}

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SubStr(cProduto,1,8))

		While !EOF() .AND. SubStr(B1_COD,1,8) == SubStr(cProduto,1,8)
			_tem := ASCAN(_aC, {|x| Alltrim(x[2]) == SubStr(SB1->B1_COD,12,4)})
			If _tem == 0
				DbSelectArea("SZK")
				DbSetOrder(2)
				DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+padr(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SubStr(SB1->B1_COD,12,4))

				If Found()
					Aadd(_aC, {0,SZK->ZK_TAM,SZK->ZK_TAMNV,oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
				Else
					DbSelectArea("SB4")
					DbSetOrder(1)
					DbSeek(xfilial("SB4")+oMSNewGe1:aCols[oMSNewGe1:nAT,1])

					DbSelectArea("SBV")
					DbSetOrder(1)
					DbSeek(xfilial("SBV")+SB4->B4_COLUNA)

					If SBV->BV_CHAVE = '00TU'
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"00TU",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					Else
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"   ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					EndIf
				EndIf
			EndIf

			DbSelectArea("SB1")
			DbSkip()
		EndDo
	Else
		Aadd(aColsEx, {Space(3),"   ",Space(15),Space(3),.F.})
	EndIf

	If Len(_ac) > 0
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna+ "' AND D_E_L_E_T_ = '' "
		cQuery += "ORDER BY R_E_C_N_O_"

		cQuery := ChangeQuery(cQuery)
		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		For nX:= 1 to len(_ac)
			DbSelectArea("TMPSBV")
			DbGoTop()
			_w := 0
			While !EOF()
				_w++
				If alltrim(TMPSBV->BV_CHAVE) == alltrim(_ac[nX,2])
					_ac[nX,1] := _w
				EndIf
				DbSkip()
			EndDo

		Next
		DbSelectArea("TMPSBV")
		DbCloseArea()

		//If Alltrim(cColuna) $ GetMv("HP_COLUNA")
		ASORT(_ac, , , { |x,y| x[1] < y[1] } )
		//	EndIf


		For nX:=1 to len(_ac)
			If ASCAN(aColsEx,_ac[nX,2])=0
				Aadd(aColsEx, {_ac[nX,2],_ac[nX,3],_ac[nX,4],_ac[nX,5],.F.})
			EndIf
		Next
	EndIf

	oMSTAM := MsNewGetDados():New( 190, 505, 275, 645, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oEst, aHeaderEx, aColsEx)

Return


Static Function estsav()
// Rotina para gerar a estrutura (SG1)
// Primeiro salva as tabelas espec�ficas
// Depois gera a estrutura de Produtos

	Local _i, _w, nX
	Local cprodg1 := ""
	Local ccompdg1 := ""

	If _fecha == .F.
		_fecha := .T.
	EndIf

	//Verifica a exist�ncia da estrutura em todas as tabelas envolvidas para liberar o cadastro.
	/*For nY := 1 to len(oMSNewGe1:aCols)
		If oMSNewGe1:aCols[nY,len(oMSNewGe1:aHeader)+1] = .F.
			If SubStr(Alltrim(oMSNewGe1:aCols[nY,1]),1,4) $ cMvEstru
				If !VldEstr(oMSNewGe1:aCols[nY,1],,,3) 
						Alert("Consumo da ficha composta "+Alltrim(oMSNewGe1:aCols[nY,1])+" encontra-se vazia, favor verificar.")
					_fecha := .F.
				Endif
			Endif
		Endif
	Next nY*/

	For _i := 1 to len(oMSNewGe1:aCols)
		If oMSNewGe1:aCols[_i,len(oMSNewGe1:aHeader)+1] = .F. .AND. alltrim(oMSNewGe1:aCols[_i,1]) <> ""
			DbSelectArea("SZE")
			DbSetOrder(2)
			DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[_i,5]+oMSNewGe1:aCols[_i,1])

			If alltrim(oMSNewGe1:aCols[_i,1]) <> ""
				If !Found()

					cCorte:= U_CORTSIM(Alltrim(cProduto),Alltrim(oMSNewGe1:aCols[_i,1]))
					oMSNewGE1:aCols[_i,4] := cCorte

					RecLock("SZE",.T.)
					Replace ZE_FILIAL			with xfilial("SZE")
					Replace ZE_PRODUTO			with SubStr(cProduto,1,15)
					Replace ZE_COMP				with oMSNewGe1:aCols[_i,1]
					Replace ZE_REVISAO			with cRevisao
					Replace ZE_CORTE			with oMSNewGE1:aCols[_i,4]
					Replace ZE_INDICE			with oMSNewGE1:aCols[_i,5]
					MsUnLock()
				Else
					/*
					cprodg1 := (ALLTRIM(CPRODUTO)+alltrim(OMSCOR:ACOLS[1,1]))
					ccompdg1 := SUBSTR(oMSNewGe1:aCols[_i,1],1,8)
					DbSelectArea("SG1")
					DbSetOrder(1)
					DbSeek(xfilial("SG1")+LEFT(cprodg1,11))

					While !EOF() .AND. SubStr(SG1->G1_COMP,1,8) == alltrim(oMSNewGe1:aCols[_i,1]) .AND. Alltrim(SG1->G1_TRT) == Alltrim(oMSNewGe1:aCols[_i,5])
						If SubStr(SG1->G1_COD,1,8) == SubStr(cProduto,1,8)
							RecLock("SG1",.F.)
							DbDelete()
							MsUnLock() //teste geyson 26/03
						EndIf

						DbSelectArea("SG1")
						DbSkip()
					EndDo
					*/
	If SZE->ZE_CORTE <> oMSNewGE1:aCols[_i,4]
		U_ajustac(SubStr(cProduto,1,15),cRevisao,oMSNewGe1:aCols[_i,5],oMSNewGe1:aCols[_i,1],SZE->ZE_CORTE)
	EndIf
	RecLock("SZE",.F.)
	Replace ZE_CORTE			with oMSNewGE1:aCols[_i,4]
	MsUnLock()
EndIf

EndIf
Else
	DbSelectArea("SZE")
	DbSetOrder(2)
	DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[_i,5]+oMSNewGe1:aCols[_i,1])

	If Found()
		RecLock("SZE",.F.)
		SZE->ZE_YHORDEL := choradel
		SZE->ZE_YDTDELE := DATE()
		SZE->ZE_YUSRDEL := ccodusr
		SZE->ZE_YNUSRDE := cnomusr
		DbDelete()
		SZE->(MsUnLock("SZE"))
	EndIf
EndIf
Next
DbSelectArea("SZE")



For _i := 1 to len(oMSCor:aCols)
	DbSelectArea("SZG")
	DbSetOrder(2)
	DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+oMsCor:aCols[_i,3]+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+oMsCor:aCols[_i,1])

	If alltrim(oMsCor:aCols[_i,3]) <> ""
		_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsCor:aCols[_i,3]) .AND.  Alltrim(x[5]) == alltrim(oMsCor:aCols[_i,4])})

		If Found()
			If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
				RecLock("SZG",.F.)
				Replace ZG_CORNV	with oMsCor:aCols[_i,2]
				MsUnlock()
			Else
				RecLock("SZG",.F.)
				SZG->ZG_YHORDEL := choradel
				SZG->ZG_YDTDELE := DATE()
				SZG->ZG_YUSRDEL := ccodusr
				SZG->ZG_YNUSRDE := cnomusr
				DbDelete()
				MsUnLock()
			EndIf
		Else
			If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
				RecLock("SZG",.T.)
				Replace ZG_FILIAL		with xfilial("SZG")
				Replace ZG_PRODUTO 		with cProduto
				Replace ZG_COMP			with oMsCor:aCols[_i,3]
				Replace ZG_COR			with oMsCor:aCols[_i,1]
				Replace ZG_CORNV		with oMsCor:aCols[_i,2]
				Replace ZG_REVISAO		with cRevisao
				Replace ZG_INDICE		with oMSCor:aCols[_i,4]
				MsUnlock()
			EndIf
		EndIf

	EndIf
Next

For _i := 1 to len(oMSTam:aCols)
	DbSelectArea("SZK")
	DbSetOrder(2)
	SZK->(DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+oMsTam:aCols[_i,3]+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+oMsTam:aCols[_i,1]))

	If alltrim(oMsTam:aCols[_i,3]) <> ""
		_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsTam:aCols[_i,3]) .AND. Alltrim(x[5]) == alltrim(oMsTam:aCols[_i,4])})

		If Found()
			If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
				RecLock("SZK",.F.)
				Replace ZK_TAMNV	with oMsTam:aCols[_i,2]
				MsUnlock()
			Else
				RecLock("SZK",.F.)
				SZK->ZK_YHORDEL := choradel
				SZK->ZK_YDTDELE := DATE()
				SZK->ZK_YUSRDEL := ccodusr
				SZK->ZK_YNUSRDE := cnomusr
				DbDelete()
				MsUnLock()
			EndIf
		Else
			If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
				RecLock("SZK",.T.)
				Replace ZK_FILIAL	with xfilial("SZK")
				Replace ZK_PRODUTO 	with cProduto
				Replace ZK_COMP		with oMsTam:aCols[_i,3]
				Replace ZK_TAM		with oMsTam:aCols[_i,1]
				Replace ZK_TAMNV		with oMsTam:aCols[_i,2]
				Replace ZK_REVISAO	with cRevisao
				Replace ZK_INDICE		with oMSTam:aCols[_i,4]
				MsUnlock()
			EndIf
		EndIf
	EndIf
Next

For _i := 1 to len(oMSQtd:aCols)
	DbSelectArea("SZF")
	DbSetOrder(2)
	DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMsQtd:aCols[_i,Len(oMsQtd:aHeader)-1],1,15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+alltrim(oMsQtd:aCols[_i,1]))

	_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]) .AND. Alltrim(x[5]) == alltrim(oMsQtd:aCols[_i,len(oMsQtd:aHeader)])})

	If Found()
		If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
			RecLock("SZF",.F.)
			For _w := 3 to Len(oMsQtd:aHeader)-2
				_campo := "SZF->"+alltrim(oMsQtd:aheader[_w][2])  //ZF_TAM"+StrZero(_w,3)
				Replace &(_campo) with oMsQtd:ACOLS[_i,_w]
			Next
			MsUnlock()
		Else
			RecLock("SZF",.F.)
			SZF->ZF_YHORDEL := choradel
			SZF->ZF_YDTDELE := DATE()
			SZF->ZF_YUSRDEL := ccodusr
			SZF->ZF_YNUSRDE := cnomusr
			DbDelete()
			MsUnLock()
		EndIf
	Else
		If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F. .AND. alltrim(oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]) <> ""
			RecLock("SZF",.T.)
			Replace ZF_FILIAL	with xfilial("SZF")
			Replace ZF_PRODUTO 	with cProduto
			Replace ZF_COMP		with oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]
			Replace ZF_COR		with oMsQtd:aCols[_i,1]
			Replace ZF_REVISAO	with cRevisao
			Replace ZF_INDICE		with oMSQtd:aCols[_i,len(oMsQtd:aHeader)]
			For _w := 3 to Len(oMsQtd:aHeader)-2
				_campo := "SZF->"+oMsQtd:aheader[_w][2] //ZF_TAM"+StrZero(_w,3)
				Replace &(_campo) with oMsQtd:ACOLS[_i,_w]
			Next
			MsUnlock()
		EndIf
	EndIf
Next

For _i := 1 to len(oMSNewGe1:aCols)
	If oMSNewGe1:aCols[_i,len(oMSNewGe1:aHeader)+1] = .F.
		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5])

		If found()
			While !EOF() .AND. SubStr(cProduto,1,8) == SubStr(SZF->ZF_PRODUTO,1,8) .AND. alltrim(cRevisao) == alltrim(SZF->ZF_REVISAO) .AND. SubStr(oMSNewGe1:aCols[_i,1],1,8) == SubStr(SZF->ZF_COMP,1,8) .AND. _fecha .AND. alltrim(SZF->ZF_INDICE) == alltrim(oMSNewGe1:aCols[_i,5])
				_temqtd := 0
				For _w := 3 to len(oMsQtd:aheader)-2
					_campo := "SZF->"+oMsQtd:aheader[_w][2] //ZF_TAM"+StrZero(_w,3)

					DbSelectArea("SZG")
					DbSetOrder(2)
					DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)

					If &_campo > 0

						_temqtd++
						DbSelectArea("SZG")
						DbSetOrder(2)
						DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)

						If !Found() .or. alltrim(SZG->ZG_CORNV) == ""
							alert(SubStr(oMSNewGe1:aCols[_i,1],1,15)+"-"+oMSNewGe1:aCols[_i,5]+"-"+SZF->ZF_COR)
							_fecha := .F.
						EndIf


						DbSelectArea("SZK")
						DbSetOrder(2)
						DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMsNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5])

						If Found()
							While !EOF() .AND. SubStr(cProduto,1,8) == SubStr(SZK->ZK_PRODUTO,1,8) .AND. alltrim(cRevisao) == alltrim(SZK->ZK_REVISAO) .AND. SubStr(oMSNewGe1:aCols[_i,1],1,8) == SubStr(SZK->ZK_COMP,1,8) .AND. _fecha .AND. alltrim(SZK->ZK_INDICE) == alltrim(oMSNewGe1:aCols[_i,5])
								DbSelectArea("SB4")
								DbSetOrder(1)
								DbSeek(xfilial("SB4")+SubStr(cProduto,1,8))

								If SELECT("TMPSBVB") > 0
									TMPSBV->(DbCloseArea())
								EndIf

								cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (nolock) WHERE "
								cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +SB4->B4_COLUNA +"' AND D_E_L_E_T_ = '' "
								cQuery += "ORDER BY R_E_C_N_O_"

								//cQuery := ChangeQuery(cQuery)

								dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBVB",.T.,.T.)

								DbSelectArea("TMPSBVB")
								DbGoTop()
								_xlin := 0
								While !EOF()
									_xlin++
									If alltrim(SZK->ZK_TAM) = alltrim(TMPSBVB->BV_CHAVE)
										_campbv	:= "ZF_"+Alltrim(SUBSTRING(TMPSBVB->BV_XCAMPO,4,8))
										Exit
									EndIf
									DbSkip()
								EndDo
								DbCloseArea()

								If _xlin > 0
									If SELECT("XQRY") > 0
										XQRY->(DbCloseArea())
									EndIf
									_Qry1 := "SELECT TOP 1 * FROM "+RetSqlName("SZF")+" (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZF_PRODUTO = '"+SubStr(cProduto,1,15)+"' AND ZF_COMP = '"+SubStr(oMsNewGe1:aCols[_i,1],1,15)+"' AND "+_campbv+" > 0"

									//_Qry1 := "SELECT TOP 1 * FROM "+RetSqlName("SZF")+" (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZF_PRODUTO = '"+SubStr(cProduto,1,15)+"' AND ZF_COMP = '"+SubStr(oMsNewGe1:aCols[_i,1],1,15)+"' AND ZF_TAM"+StrZero(_xlin,3)+" > 0"

									dbUseArea( .T., "TOPCONN", TcGenQry(,,_Qry1), "XQRY", .T., .T. )
									dbSelectArea("XQRY")
									dbGoTop()

									If !EOF()
										If alltrim(SZK->ZK_TAMNV) == ""

											Alert(SubStr(oMSNewGe1:aCols[_i,1],1,8)+"-"+SZK->ZK_TAM)
											_fecha := .F.
										EndIf
									EndIf
									DbCloseArea()
								Else
									_fecha := .F.
								EndIf
								DbSelectArea("SZK")
								DbSkip()
							EndDo
						Else
							_fecha := .F.
						EndIf
					EndIf
				Next
				// Obrigat�rio a ter pelo menos uma quantidade preenchida caso tenha a cor preenchida!
				If _temqtd = 0
					DbSelectArea("SZG")
					DbSetOrder(2)
					DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)

					If Found() .AND. alltrim(SZG->ZG_CORNV) != "" //.OR. _temqtd = 0
						alert(SubStr(oMSNewGe1:aCols[_i,1],1,15)+"-"+oMSNewGe1:aCols[_i,5]+"-"+SZF->ZF_COR+SZF->ZF_INDICE)
						_fecha := .F.
					EndIf
				EndIf

				DbSelectArea("SZF")
				DbSkip()
			EndDo
		Else
			_fecha := .F.
		EndIf
	EndIf
Next

If _fecha

	_qry := "UPDATE	"+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '1"+cnomusr+"' FROM "+RetSqlName("SG1")+" SG1 "
	_qry += " WHERE left(G1_COD,8) = 'B"+substr(cProduto,1,7)+"' AND SG1.D_E_L_E_T_ = ''  AND G1_INI <> '19500101' AND "
	_qry += "(SELECT TOP 1 ZE_COMP FROM "+RetSqlName("SZE")+" SZE WHERE SZE.D_E_L_E_T_ = ''  AND ZE_PRODUTO = '"+substr(cProduto,1,8)+"' AND "
	_qry += "left(ZE_COMP,8) = left(G1_COMP,8) AND ZE_CORTE = 'S' AND ZE_REVISAO = G1_REVINI AND SZE.ZE_INDICE = SG1.G1_TRT ) is null "

	TcSqlExec(_qry)

	//ADCIONADO UPDATE PARA DELETAR PRODUTO DE CORTE GEYSON
	_qry := "UPDATE "+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '2"+cnomusr+"' FROM "+RetSqlName("SG1")+" SG1 "
	_qry += " WHERE left(G1_COMP,8) = 'B"+substr(cProduto,1,7)+"' AND SG1.D_E_L_E_T_ = ''  AND G1_INI <> '19500101' AND "
	_qry += "(SELECT TOP 1 ZE_COMP FROM "+RetSqlName("SZE")+" SZE WHERE SZE.D_E_L_E_T_ = ''  AND ZE_PRODUTO = '"+substr(cProduto,1,8)+"' AND "
	_qry += "left(ZE_COMP,8) = left(G1_COMP,8) AND ZE_CORTE = 'N' AND ZE_REVISAO = G1_REVINI AND SZE.ZE_INDICE = SG1.G1_TRT) is null "

	TcSqlExec(_qry)

	_QRY3 := "SELECT COUNT(*) AS CORTE FROM "+RetSqlName("SZE")+" (NOLOCK) WHERE ZE_PRODUTO = '"+substr(cProduto,1,8)+"' AND D_E_L_E_T_ = '' AND ZE_CORTE = 'S'"

	If SELECT("TMPCOR") > 0
		TMPCOR->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_QRY3),"TMPCOR",.T.,.T.)

	If TMPCOR->CORTE = 0 // CASO N�O TENHA MAIS ZE_CORTE COM S LIMPA NA G1 O PRIMIERO NIVEL DO PRODUTO DE CORTE
		_qry4:= " UPDATE "+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*',R_E_C_D_E_L_ = G1.R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '13"+cnomusr+"' FROM "+RetSqlName("SG1")+" G1 "
		_qry4+= " WHERE G1_COD = '"+(cProduto)+"' AND  left(G1_COMP,8) = 'B"+substr(cProduto,2,15)+"'  AND G1_INI <> '19500101' AND D_E_L_E_T_ = ''
		TcSqlExec(_qry4)
	EndIf

	_qry2:= " UPDATE "+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*',R_E_C_D_E_L_ = G1.R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '3"+cnomusr+"' FROM "+RetSqlName("SG1")+" G1 INNER JOIN "+RetSqlName("SZE")+" ZE (NOLOCK)  "
	_qry2+= " ON SUBSTRING(G1_COD,2,7)= SUBSTRING(ZE_PRODUTO,1,7) AND LEFT(G1_COMP,8) = ZE.ZE_COMP AND G1_TRT = ZE_INDICE WHERE 	ZE_CORTE = 'N' AND ZE.D_E_L_E_T_ = '' "
	_qry2+= " AND G1.D_E_L_E_T_ = '' AND LEFT(G1_COD,8) = 'B"+substr(cProduto,1,7)+"' AND G1_INI <> '19500101' "

	TcSqlExec(_qry2)
	/*
		If SELECT("TMPUPD2") > 0
		TMPUPD2->(DbCloseArea())
		EndIf

	_qry := "SELECT 'UPDATE"+RetSqlName("SG1")+" SET D_E_L_E_T_ = ''*'',R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = ''"+choradel+"'',G1_YDTDELE = ''"+ddatadel+"'',G1_YUSRDEL = ''"+ccodusr+"'',G1_YNUSRDE = ''4"+cnomusr+"'' WHERE R_E_C_N_O_ in ('+ ( "
	_qry += "SELECT cast(R_E_C_N_O_ as varchar) + ',' AS [text()] "
	_qry += "FROM "+RetSqlName("SG1")+" SG1A (nolock) "
	_qry += "WHERE SG1A.D_E_L_E_T_ = '' AND SG1A.G1_REVINI = '"+cRevisao+"' AND G1_COD in "
	_qry += "(SELECT G1_COMP FROM "+RetSqlName("SG1")+" SG1 WHERE SG1.D_E_L_E_T_ = '' AND "
	_qry += "left(SG1.G1_COD,8) = '"+substr(cProduto,1,8)+"' AND left(SG1.G1_COMP,1) = 'B' "
	_qry += "group by G1_COMP) "
	_qry += "FOR XML PATH ('')) as UPD "


	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPUPD2",.T.,.T.)
	DbselectArea("TMPUPD2")
	DbGoTop()

	_QRY := SUBSTR(TMPUPD2->UPD,1,LEN(ALLTRIM(TMPUPD2->UPD))-1)+")"

		If len(alltrim(_qry)) > 10
		nStatus := TcSqlExec(_qry)

			If (nStatus < 0)
			MsgBox(TCSQLError(), "Erro no comando", "Stop")
			EndIf
		EndIf
	DbSelectArea("TMPUPD2")
	DbCloseArea()

		If SELECT("TMPUPD3") > 0
		TMPUPD3->(DbCloseArea())
		EndIf

	_qry := "SELECT 'UPDATE"+RetSqlName("SG1")+" SET D_E_L_E_T_ = ''*'', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = ''"+choradel+"'',G1_YDTDELE = ''"+ddatadel+"'',G1_YUSRDEL = ''"+ccodusr+"'',G1_YNUSRDE = ''5"+cnomusr+"'' WHERE R_E_C_N_O_ in ('+ ( "
	_qry += "SELECT cast(R_E_C_N_O_ as varchar) + ',' AS [text()] "
	_qry += "FROM "+RetSqlName("SG1")+" SG1 (nolock) "
	_qry += "WHERE D_E_L_E_T_ = '' AND G1_REVINI = '"+cRevisao+"' AND left(G1_COD,8) = '"+substr(cProduto,1,8)+"' "
	_qry += "FOR XML PATH ('')) as UPD "

	

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPUPD3",.T.,.T.)
	DbselectArea("TMPUPD3")
	DbGoTop()

	_QRY := SUBSTR(TMPUPD3->UPD,1,LEN(ALLTRIM(TMPUPD3->UPD))-1)+")"

		If len(alltrim(_qry)) > 10
		nStatus := TcSqlExec(_qry)

			If (nStatus < 0)
			MsgBox(TCSQLError(), "Erro no comando", "Stop")
			EndIf
		EndIf
	DbSelectArea("TMPUPD3")
	DbCloseArea()
*/
	DbSelectArea("SZE")
	DbSetOrder(1)
	DbSeek(xfilial("SZE")+substr(cProduto,1,15)+cRevisao)

	_seq1 := SZE->ZE_INDICE

	While !EOF() .AND. alltrim(SZE->ZE_PRODUTO) == ALLTRIM(SubStr(cProduto,1,15)) .AND. Alltrim(SZE->ZE_REVISAO) == cRevisao
		DbSelectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+SubStr(SZE->ZE_COMP,1,8))

		If !Found()
			_seq := 1
			DbSelectArea("SZF")
			DbSetOrder(2)
			DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SZE->ZE_INDICE+SZE->ZE_COMP)

			DbSelectArea("SZG")
			DbSetOrder(2)
			DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SZE->ZE_COMP+SZE->ZE_INDICE)

			If Found() .AND. SZG->ZG_CORNV <> ""
				For _i := 3 to len(oMsQtd:aHeader)-2

					_campo := "SZF->"+oMsQtd:aheader[_i][2]//ZF_TAM"+StrZero(_i,3)

					If &_campo > 0
						DbSelectArea("SB4")
						DbSetOrder(1)
						DbSeek(xfilial("SB4")+alltrim(SubStr(cProduto,1,15)))
						cColuna	:= SB4->B4_COLUNA
						_comp := SZE->ZE_COMP
						//_tam 		:= rettam(Val(SubStr(_campo,12,3)),cColuna,"C")
						_tam := PADL(ALLTRIM(oMsQtd:aheader[_i][1]),4,"0")
						_qtd  := &_campo
						_niv := "01"
						_xnv := "99"

						If SZE->ZE_CORTE == "S"
							_niv := "02"
							_xnv := "98"
							_prod := U_cadEst(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_CORNV)+_tam ,cColuna,SZE->ZE_INDICE)
							U_EstSup(PADR(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam,15),PADR(_prod,15),SZE->ZE_INDICE)
						Else
							_niv := "01"
							_xnv := "99"
							_prod := alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam
						EndIf

						DbSelectArea("SB1")
						DbSetOrder(1)
						DbSeek(xfilial("SB1")+_comp)
						If Found()

							DbSelectArea("SB1")
							DbSetOrder(1)
							DbSeek(xfilial("SB1")+_prod)

							If Found()

								DbSelectArea("SG1")
								//DbOrderNickName("SG1GRD1")
								DbSetOrder(6)
								//DbSeek(xfilial("SG1")+PADR(_prod,15)+PADR(_comp,15)+_seq1+cRevisao)
								DbSeek(xfilial("SG1")+PADR(_prod,15)+PADR(_comp,15))

								If !Found()

									If len(alltrim(_comp)) = 15

										_qry := "Insert into "+RetSqlName("SG1")+" (G1_FILIAL, G1_COD, G1_COMP, G1_TRT, G1_QUANT, G1_INI, G1_FIM, G1_NIV, G1_NIVINV, G1_REVINI,"
										_qry += "G1_REVFIM, R_E_C_N_O_) Values ('"+xfilial("SG1")+"','"+_prod+"','"
										_qry += _comp+"','"+oMSOPC:ACOLS[i,3]+"',"+alltrim(str(_qtd))+",'"+dtos(ddatabase)+"',"
										_qry += "'20491231', '"+_niv+"', '"+_xnv+"', '"+cRevisao+"', '"+cRevisao+"',(SELECT max(R_E_C_N_O_)+1 FROM SG1010))"

										nStatus := TcSqlExec(_qry)

										If (nStatus < 0)
											MsgBox(TCSQLError(), "Erro no comando", "Stop")
										EndIf

									EndIf
								Else
									DbSelectArea("SG1")
									DbSetOrder(6)
									DbSeek(xfilial("SG1")+PADR(_prod,15)+PADR(_comp,15)+_seq1+cRevisao)
									If Found()
										If SG1->G1_QUANT <> _qtd
											RecLock("SG1",.F.)
											Replace G1_QUANT with _qtd
											MsUnLock()
										EndIf
									Else
										Alert("A MP "+_comp+" do produto j� "+_prod+" est� cadastrada na grade com outro indice, Favor verifique!")
										_FECHA := .F.
									EndIf
								EndIf
							Else
								Alert("O produto "+_prod+" n�o est� cadastrado no cadastro de produtos!")
								_FECHA := .F.
							EndIf
						Else
							MessageBox("A MP "+_comp+" com o indice "+_seq1+" n�o est� cadastrada na tabela de produtos!","estsav1",16)
							_FECHA := .F.
						EndIf
					EndIf
				Next
			EndIf
		Else
			DbSelectArea("SZG")
			DbSetOrder(2)
			DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SZE->ZE_COMP+SZE->ZE_INDICE)
			_seq1 := SZE->ZE_INDICE
			_seq := 0
			While !EOF() .AND. SZG->ZG_COMP = SZE->ZE_COMP .AND. SZG->ZG_PRODUTO = SZE->ZE_PRODUTO .AND. SZG->ZG_REVISAO == cRevisao .AND. SZE->ZE_INDICE = SZG->ZG_INDICE .AND. SZG->ZG_CORNV <> ""
				DbSelectArea("SZF")
				DbSetOrder(2)
				DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SZG->ZG_COMP+SZE->ZE_INDICE+SZG->ZG_COR)

				If Found()
					_seq++

					For _i := 3 to len(oMsQtd:aHeader)-2

						_campo := "SZF->"+oMsQtd:aheader[_i][2]//ZF_TAM"+StrZero(_i,3)

						If &_campo > 0
							DbSelectArea("SB4")
							DbSetOrder(1)
							DbSeek(xfilial("SB4")+alltrim(SubStr(cProduto,1,15)))
							cColuna	:= SB4->B4_COLUNA
							//_tam 		:= rettam(Val(SubStr(_campo,12,3)),cColuna,"C")
							_tam := PADL(ALLTRIM(oMsQtd:aheader[_i][1]),4,"0")
							DbSelectArea("SZK")
							DbSetOrder(2)
							DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+SZG->ZG_COMP+SZE->ZE_INDICE+_tam)
							If Found()
								_tamcomp := SZK->ZK_TAMNV
							Else
								_tamcomp := ""
							EndIf
							_comp := alltrim(SZG->ZG_COMP)+alltrim(SZG->ZG_CORNV)+_tamcomp
							_qtd  := &_campo
							_niv := "01"
							_xnv := "99"

							If SZE->ZE_CORTE == "S"
								_niv := "02"
								_xnv := "98"
								_prod := U_cadEst(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam, ccoluna,SZE->ZE_INDICE)
							Else
								_niv := "01"
								_xnv := "99"
								_prod := alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam
							EndIf

							If PADR(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam,15) <> PADR(alltrim(_prod),15)
								U_EstSup(PADR(alltrim(SubStr(cProduto,1,15))+alltrim(SZG->ZG_COR)+_tam,15),PADR(_prod,15),SZG->ZG_INDICE)
							EndIf

							DbSelectArea("SB1")
							DbSetOrder(1)
							DbSeek(xfilial("SB1")+_comp)

							If Found()

								DbSelectArea("SB1")
								DbSetOrder(1)
								DbSeek(xfilial("SB1")+_prod)

								If Found()
									DbSelectArea("SG1")
									//DbOrderNickName("SG1GRD1")
									DbSetOrder(6)
									//DbSeek(xfilial("SG1")+PADR(_prod,15)+PADR(_comp,15)+_seq1+cRevisao)
									DbSeek(xfilial("SG1")+PADR(_prod,15)+PADR(_comp,15))

									If !Found()
										If len(alltrim(_comp)) = 15

											_qry := "Insert into "+RetSqlName("SG1")+" (G1_FILIAL, G1_COD, G1_COMP, G1_TRT, G1_QUANT, G1_INI, G1_FIM, G1_NIV, G1_NIVINV, G1_REVINI,"
											_qry += "G1_REVFIM, R_E_C_N_O_) Values ('"+xfilial("SG1")+"','"+_prod+"','"
											_qry += _comp+"','"+_seq1+"',"+alltrim(str(_qtd))+",'"+dtos(ddatabase)+"',"
											_qry += "'20491231', '"+_niv+"', '"+_xnv+"', '"+cRevisao+"', '"+cRevisao+"',(SELECT max(R_E_C_N_O_)+1 FROM SG1010))"

											nStatus := TcSqlExec(_qry)

											If (nStatus < 0)
												MsgBox(TCSQLError(), "Erro no comando", "Stop")
											EndIf

										EndIf
									Else
										DbSelectArea("SG1")
										DbSetOrder(6)
										DbSeek(xfilial("SG1")+PADR(_prod,15)+PADR(_comp,15)+_seq1+cRevisao)
										If Found()
											If SG1->G1_QUANT <> _qtd
												RecLock("SG1",.F.)
												Replace G1_QUANT with _qtd
												MsUnLock()
											EndIf
										Else
											Alert("A MP "+_comp+" do produto "+_prod+" j� est� cadastrada na grade com outro indice, Favor verifique!")
											_FECHA := .F.
										EndIf
									EndIf

								Else
									Alert("O produto "+_prod+" n�o est� cadastrado no cadastro de produtos!")
									_FECHA := .F.
								EndIf
							Else
								MessageBox("A MP "+_comp+" com o indice "+_seq1+" n�o est� cadastrada na tabela de produtos!","estsav2",16)
								_FECHA := .F.
							EndIf
						EndIf
					Next
				EndIf

				DbSelectArea("SZG")
				DbSkip()
			EndDo
		EndIf
		DbSelectArea("SZE")
		DbSkip()
	EndDo

	_qry := "UPDATE "+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '6"+cnomusr+"' FROM "+RetSqlName("SG1")+" SG1 "
	_qry += "WHERE SG1.D_E_L_E_T_ = '' AND Left(G1_COD,8) = '"+substr(cProduto,1,8)+"' "
	_qry += "AND (SELECT ZE_COMP FROM SZE010 SZE WHERE SZE.D_E_L_E_T_ = '' AND ZE_FILIAL = '"+xfilial("SZE")+"' AND "
	_qry += "ZE_PRODUTO = SubString(G1_COD,1,8) AND ZE_REVISAO = G1_REVINI AND ZE_INDICE = G1_TRT AND ZE_COMP = SubString(G1_COMP,1,8)) "
	_qry += "is null AND SubString(G1_COMP,1,1) <> 'B'  AND G1_INI <> '19500101' "

	TcSqlExec(_qry)
		/*
		_qry := "UPDATE "+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '7"+cnomusr+"' FROM "+RetSqlName("SG1")+" SG1 "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND Left(G1_COD,8) = '"+substr(cProduto,1,8)+"' "
		_qry += "AND (SELECT ZG_COMP FROM SZG010 SZG WHERE SZG.D_E_L_E_T_ = '' AND ZG_FILIAL = '"+xfilial("SZG")+"' AND "
		_qry += "ZG_PRODUTO = SubString(G1_COD,1,8) AND ZG_REVISAO = G1_REVINI AND ZG_INDICE = G1_TRT AND ZG_COMP = SubString(G1_COMP,1,8) "
		_qry += "AND ZG_CORNV = SubString(G1_COMP,9,3)) is null AND SubString(G1_COMP,1,1) <> 'B'  AND G1_INI <> '19500101' "

		TcSqlExec(_qry)
		//caio
		_qry := "UPDATE "+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '8"+cnomusr+"' FROM "+RetSqlName("SG1")+" SG1 "
		_qry += "WHERE SG1.D_E_L_E_T_ = '' AND Left(G1_COD,8) = '"+substr(cProduto,1,8)+"' "
		_qry += "AND (SELECT ZK_COMP FROM SZK010 SZK WHERE SZK.D_E_L_E_T_ = '' AND ZK_FILIAL = '"+xfilial("SZK")+"' AND "
		_qry += "ZK_PRODUTO = SubString(G1_COD,1,8) AND ZK_REVISAO = G1_REVINI AND ZK_INDICE = G1_TRT AND ZK_COMP = SubString(G1_COMP,1,8) "
		_qry += "AND ZK_TAMNV = SubString(G1_COMP,12,4)) is null AND SubString(G1_COMP,1,1) <> 'B'  AND G1_INI <> '19500101' "

		TcSqlExec(_qry)
		*/
	_qry := "UPDATE "+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '9"+cnomusr+"' FROM "+RetSqlName("SG1")+" SG1 WHERE SG1.D_E_L_E_T_ = '' AND left(G1_COD,8) = '"+substr(cProduto,1,8)+"' AND Left(G1_COMP,1) <> 'B'  AND G1_INI <> '19500101' "
	_qry += "AND (SELECT TOP 1 ZG_CORNV FROM "+RetSqlName("SZG")+" SZG WHERE SZG.D_E_L_E_T_ = '' AND Left(G1_COD,8) = left(ZG_PRODUTO,8) AND Left(G1_COMP,8) = Left(ZG_COMP,8) AND SubString(G1_COD,9,3) = ZG_COR AND SubString(G1_COMP,9,3) = ZG_CORNV) is null "

	TcSqlExec(_qry)


EndIf
If _Fecha
	U_SalvaRef()
	lLmpCor	:= .F.
	lLmpCon := .F.
EndIf
Return

User Function AtuAcols()
// Rotina para atualizar acols na mudan�a de linha de acols de componentes
// Salva cada acol antes de mudar de linha e atualizar 

	Local _i,_w, nx,_wtam

	U_ProxIndice(cProduto)

	For _i := 1 to len(oMSNewGe1:aCols)
		If oMSNewGe1:aCols[_i,len(oMSNewGe1:aHeader)+1] = .F.
			DbSelectArea("SZE")
			DbSetOrder(2)
			If Alltrim(oMSNewGe1:aCols[_i,2]) != ""
				DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[_i,5]+oMSNewGe1:aCols[_i,1])
			ElseIf Alltrim(oMSNewGe1:aCols[_i,1]) != "" .AND. Alltrim(oMSNewGe1:aCols[_i,2]) == ""


				oMSNewGe1:aCols[oMSNewGe1:nAT,5]:= cProxIndice // ADICIONANDO UMA NOVA LINHA COM O INDICE M�XIMO + 1 GEYSON ALBANO 13/12/19
				DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[_i,5]+oMSNewGe1:aCols[_i,1])
			EndIf

			If alltrim(oMSNewGe1:aCols[_i,1]) <> ""
				If !Found()

					cCorte:= U_CORTSIM(Alltrim(cProduto),Alltrim(oMSNewGe1:aCols[_i,1]))
					oMSNewGE1:aCols[_i,4] := cCorte

					RecLock("SZE",.T.)
					Replace ZE_FILIAL	  with xfilial("SZE")
					Replace ZE_PRODUTO		with SubStr(cProduto,1,15)
					Replace ZE_COMP			with oMSNewGe1:aCols[_i,1]
					Replace ZE_REVISAO		with cRevisao
					Replace ZE_CORTE		with oMSNewGE1:aCols[_i,4]
					Replace ZE_INDICE		with oMSNewGE1:aCols[_i,5]
					MsUnLock()

				Else
					If SZE->ZE_CORTE <> oMSNewGE1:aCols[_i,4]
						U_ajustac(SubStr(cProduto,1,15),cRevisao,oMSNewGe1:aCols[_i,5],oMSNewGe1:aCols[_i,1],SZE->ZE_CORTE)
					EndIf
					RecLock("SZE",.F.)
					Replace ZE_CORTE			with oMSNewGE1:aCols[_i,4]
					MsUnLock()
				EndIf
			EndIf

		Else
			DbSelectArea("SZE")
			DbSetOrder(2)
			DbSeek(xfilial("SZE")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[_i,5]+oMSNewGe1:aCols[_i,1])

			If Found()
				RecLock("SZE",.F.)
				SZE->ZE_YHORDEL := choradel
				SZE->ZE_YDTDELE := DATE()
				SZE->ZE_YUSRDEL := ccodusr
				SZE->ZE_YNUSRDE := cnomusr
				DbDelete()
				SZE->(MsUnLock("SZE"))
			EndIf

		EndIf
		DbSelectArea("SZE")
	Next

	For _i := 1 to len(oMSCor:aCols)
		DbSelectArea("SZG")
		DbSetOrder(2)
		DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+oMsCor:aCols[_i,3]+oMsCor:aCols[_i,4]+oMsCor:aCols[_i,1])

		If alltrim(oMsCor:aCols[_i,3]) <> "" .AND. alltrim(oMsCor:aCols[_i,3]) <> ""
			_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsCor:aCols[_i,3]) .AND. Alltrim(x[5]) == alltrim(oMsCor:aCols[_i,4])})

			If Found()
				// TODO Alterado a variavel _tem por _i. Para buscar no array de cores (Weskley Silva 28.08.2019)
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZG",.F.)
					Replace ZG_CORNV	with oMsCor:aCols[_i,2]
					MsUnlock()
				Else
					Reclock("SZG",.F.)
					SZG->ZG_YHORDEL := choradel
					SZG->ZG_YDTDELE := DATE()
					SZG->ZG_YUSRDEL := ccodusr
					SZG->ZG_YNUSRDE := cnomusr
					DbDelete()
					MsUnLock()
				EndIf


			Else
				// TODO Alterado a variavel _tem por _i. Para buscar no array de cores (Weskley Silva 28.08.2019)
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					Aadd(oMSCor:aCols, {oMsCor:aCols[_i,1],oMsCor:aCols[_i,2],cRevisao,oMsCor:aCols[_i,4],.F.})


					RecLock("SZG",.T.)
					Replace ZG_FILIAL	with xfilial("SZG")
					Replace ZG_PRODUTO 	with cProduto
					Replace ZG_COMP		with oMsCor:aCols[_i,3]
					Replace ZG_COR		with oMsCor:aCols[_i,1]
					Replace ZG_CORNV		with oMsCor:aCols[_i,2]
					Replace ZG_REVISAO	with cRevisao
					Replace ZG_INDICE		with oMsCor:aCols[_i,4]
					MsUnlock()

				EndIf

			EndIf

		EndIf
	Next


	For _i := 1 to len(oMSTam:aCols)
		DbSelectArea("SZK")
		DbSetOrder(2)
		DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+oMsTam:aCols[_i,3]+oMsTam:aCols[_i,4]+oMsTam:aCols[_i,1])

		If alltrim(oMsTam:aCols[_i,3]) <> ""
			_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsTam:aCols[_i,3]) .AND. Alltrim(x[5]) == alltrim(oMsTam:aCols[_i,4])})

			If Found()
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZK",.F.)
					Replace ZK_TAMNV	with oMsTam:aCols[_i,2]
					MsUnlock()
				Else
					RecLock("SZK",.F.)
					SZK->ZK_YHORDEL := choradel
					SZK->ZK_YDTDELE := DATE()
					SZK->ZK_YUSRDEL := ccodusr
					SZK->ZK_YNUSRDE := cnomusr
					DbDelete()
					MsUnLock()
				EndIf

			Else

				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZK",.T.)
					Replace ZK_FILIAL	with xfilial("SZG")
					Replace ZK_PRODUTO 	with cProduto
					Replace ZK_COMP		with oMsTam:aCols[_i,3]
					Replace ZK_TAM		with oMsTam:aCols[_i,1]
					Replace ZK_TAMNV		with oMsTam:aCols[_i,2]
					Replace ZK_REVISAO	with cRevisao
					Replace ZK_INDICE		with oMsTam:aCols[_i,4]
					MsUnlock()
				EndIf

			EndIf

		EndIf
	Next


	For _i := 1 to len(oMSQtd:aCols)
		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+oMsQtd:aCols[_i,LEN(oMsQtd:aHeader)-1]+SubStr(oMsQtd:aCols[_i,Len(oMsQtd:aHeader)],1,15)+alltrim(oMsQtd:aCols[_i,1]))

		If Alltrim(oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]) <> ""
			_tem := ASCAN( oMSNewGe1:aCols, {|x| Alltrim(x[1]) == alltrim(oMsQtd:aCols[_i,LEN(OMSQTD:AHEADER)-1]) .AND. Alltrim(x[5]) == alltrim(oMsQtd:aCols[_i,Len(oMsQtd:aHeader)])})

			If Found()
				// TODO Alterado a variavel _tem por _i. Para buscar no array de cores (Weskley Silva 28.08.2019)
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZF",.F.)
					For _w := 3 to Len(oMsQtd:aHeader)-2
						_campo := "SZF->"+oMsQtd:aheader[_w][2]
						Replace &(_campo) with oMsQtd:ACOLS[_i,_w]
					Next
					MsUnlock()
				Else
					RecLock("SZF",.F.)
					SZF->ZF_YHORDEL := choradel
					SZF->ZF_YDTDELE := DATE()
					SZF->ZF_YUSRDEL := ccodusr
					SZF->ZF_YNUSRDE := cnomusr
					DbDelete()
					MsUnLock()
				EndIf

			Else
				If oMSNewGe1:aCols[_tem,len(oMSNewGe1:aHeader)+1] = .F.
					RecLock("SZF",.T.)
					Replace ZF_FILIAL	with xfilial("SZF")
					Replace ZF_PRODUTO 	with cProduto
					Replace ZF_COMP		with oMsQtd:aCols[_i,len(oMsQtd:aHeader)-1]
					Replace ZF_COR		with oMsQtd:aCols[_i,1]
					Replace ZF_REVISAO	with cRevisao
					Replace ZF_INDICE		with oMsQtd:aCols[_i,Len(oMsQtd:aHeader)]
					For _w := 3 to Len(oMsQtd:aHeader)-2
						_campo := "SZF->"+oMsQtd:aheader[_w][2]
						Replace &(_campo) with oMsQtd:ACOLS[_i,_w]
					Next
					MsUnlock()
				EndIf

			EndIf

		EndIf
	Next

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		DbselectArea("SB4")
		DbSetOrder(1)
		DbSeek(xfilial("SB4")+alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]))
		cColunx 		:= SB4->B4_COLUNA

		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+alltrim(SB4->B4_COD))

		aCor := {}
		While !EOF() .AND. SubStr(B1_COD,1,8) == SubStr(SB4->B4_COD,1,8)

			_tem := ASCAN(aCor, {|x| x == SubStr(SB1->B1_COD,9,3)})

			If _tem == 0
				aadd(aCor, SubStr(SB1->B1_COD,9,3))
			EndIf
			DbSelectArea("SB1")
			DbSkip()
		EndDo

		For _w := 1 to len(aCor)
			cCor += aCor[_w]+"|"
		Next
	EndIf

	If alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) <> ""
		oMSCor:aCols := {}
		oMSCor:Refresh()

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)

			DbSelectArea("SZG")
			DbSetOrder(2)
			DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+PADR(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SZD->ZD_COR)

			If Found()
				Aadd(oMSCor:aCols, {SZG->ZG_COR,SZG->ZG_CORNV,SZG->ZG_COMP,SZG->ZG_INDICE,.F.})
			Else
				Aadd(oMSCor:aCols, {SZD->ZD_COR,"   ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
			EndIf
			DbSelectArea("SZD")
			DbSkip()
		EndDo

		oMSTam:Refresh()

		_ac := {}

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")

			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY BV_CHAVE,R_E_C_N_O_"

		Else
			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY R_E_C_N_O_"
		EndIf

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		_YTAMZ:={}
		While TMPSBV->(!EOF()) .AND. alltrim(TMPSBV->BV_TABELA) == alltrim(cColuna)
			Aadd(_YTAMZ,{TMPSBV->BV_XCAMPO,TMPSBV->BV_CHAVE})
			DbSkip()
		EndDo
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))
		_YTAM := {}
		While !EOF() .AND. Alltrim(SZD->ZD_PRODUTO) == Alltrim(cProduto)
			For _i := 1 to LEN(_YTAMZ)
				_campo := "SZD->"+_YTAMZ[_i][1]
				_chavebv := _YTAMZ[_i][2]
				If &_campo == "X"
					_tem := Ascan(_Ytam,{|x| x[1] == _campo})
					If _tem == 0
						aadd(_ytam,{_campo,_chavebv})
					EndIf
				EndIf
			Next
			DbSelectarea("SZD")
			DbSkip()
		EndDo
		ASORT(_ytam, , , { |x,y| x[1] < y[1] } )

		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SubStr(cProduto,1,8))
		oMSTam:aCols := {}

		While !EOF() .AND. SubStr(B1_COD,1,8) == SubStr(cProduto,1,8)
			_tem := ASCAN( _ytam, {|x| Alltrim(x[2]) == SubStr(SB1->B1_COD,12,4)})
			If _tem != 0
				DbSelectArea("SZK")
				DbSetOrder(2)
				DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+padr(oMSNewGe1:aCols[oMSNewGe1:nAT,1],15)+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SubStr(SB1->B1_COD,12,4))

				If Found()
					Aadd(_aC, {0,SZK->ZK_TAM,SZK->ZK_TAMNV,SZK->ZK_COMP,SZK->ZK_INDICE,.F.})
				Else
					DbSelectArea("SB4")
					DbSetOrder(1)
					DbSeek(xfilial("SB4")+oMSNewGe1:aCols[oMSNewGe1:nAT,1])

					DbSelectArea("SBV")
					DbSetOrder(1)
					DbSeek(xfilial("SBV")+SB4->B4_COLUNA)

					If SBV->BV_CHAVE = '00TU'
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"00TU",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					Else
						Aadd(_aC, {0,SubStr(SB1->B1_COD,12,4),"    ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
					EndIf
				EndIf
			EndIf

			DbSelectArea("SB1")
			DbSkip()
		EndDo

		If Len(_ac) > 0

			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna+ "' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY R_E_C_N_O_"

			cQuery := ChangeQuery(cQuery)
			If SELECT("TMPSBV") > 0
				TMPSBV->(DbCloseArea())
			EndIf
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

			For nX:= 1 to len(_ac)
				DbSelectArea("TMPSBV")
				DbGoTop()
				_w := 0
				While !EOF()
					_w++
					If alltrim(TMPSBV->BV_CHAVE) == alltrim(_ac[nX,2])
						_ac[nX,1] := _w
					EndIf
					DbSkip()
				EndDo

			Next
			DbSelectArea("TMPSBV")
			DbCloseArea()

			ASORT(_ac, , , { |x,y| x[1] < y[1] } )

			For nX:=1 to len(_ac)
				If ASCAN( oMSTam:aCols, {|x| alltrim(x[1]) == alltrim(_ac[nX,2])})=0
					Aadd(oMSTam:aCols, {_ac[nX,2],_ac[nX,3],_ac[nX,4],_ac[nX,5],.F.})
				EndIf
			Next

		EndIf
		If Alltrim(cColuna) $ GetMv("HP_COLUNA")
			ASORT(oMSTam:aCols, , , { |x,y| x[1] < y[1] } )
		EndIf
		oMSQtd:aCols := {}
		oMSQtd:Refresh()

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))
		oMSQtd:aCols := {}

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)

			DbSelectArea("SZF")
			DbSetOrder(2)
			DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+oMSNewGe1:aCols[oMSNewGe1:nAT,1]+oMSNewGe1:aCols[oMSNewGe1:nAT,5]+SZD->ZD_COR)

			If Found()
				aFieldFill := {}
				Aadd(aFieldFill, SZF->ZF_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZF->ZF_COR,"BV_DESCRI"))

				For nX := 3 to Len(oMsQtd:aHeader)-2
					_campo := "SZF->"+oMsQtd:aheader[nX][2]
					Aadd(aFieldFill,&_campo)
				Next
				Aadd(aFieldFill, SZF->ZF_COMP)
				Aadd(aFieldFill, SZF->ZF_INDICE)
				Aadd(aFieldFill, .F.)
				Aadd(oMsQtd:aCols, aFieldFill)
			Else
				aFieldFill := {}
				Aadd(aFieldFill, SZD->ZD_COR)
				Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

				For nX := 3 to Len(oMsQtd:aHeader)-2
					Aadd(aFieldFill,0)
				Next
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
				Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,5])
				Aadd(aFieldFill, .F.)
				Aadd(oMsQtd:aCols, aFieldFill)
			EndIf

			DbSelectArea("SZD")
			DbSkip()
		EndDo

	Else // INICIO VALIDACAO NOVA LINHA
		oMSCor:aCols := {}
		oMSCor:Refresh()

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			Aadd(oMSCor:aCols, {SZD->ZD_COR,"   ",space(15),Space(3),.F.})

			DbSelectArea("SZD")
			DbSkip()
		EndDo
		oMSCor:Refresh()

		oMSTam:aCols := {}
		oMSTam:Refresh()

		If Alltrim(cColuna) $ GetMv("HP_COLUNA")

			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY BV_CHAVE,R_E_C_N_O_"

		Else
			cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
			cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
			cQuery += "ORDER BY R_E_C_N_O_"
		EndIf

		cQuery := ChangeQuery(cQuery)

		If SELECT("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

		_YTAMZ:={}
		While TMPSBV->(!EOF()) .AND. alltrim(TMPSBV->BV_TABELA) == alltrim(cColuna)
			Aadd(_YTAMZ,{TMPSBV->BV_XCAMPO,TMPSBV->BV_CHAVE})
			DbSkip()
		EndDo
		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))
		_YTAM := {}
		While !EOF() .AND. Alltrim(SZD->ZD_PRODUTO) == Alltrim(cProduto)
			For _i := 1 to LEN(_YTAMZ)
				_campo := "SZD->"+_YTAMZ[_i][1]
				_chavebv := _YTAMZ[_i][2]
				If &_campo == "X"
					_tem := Ascan(_Ytam,{|x| x[1] == _campo})
					If _tem == 0
						aadd(_ytam,{_campo,_chavebv})
					EndIf
				EndIf
			Next
			DbSelectarea("SZD")
			DbSkip()
		EndDo
		If Alltrim(cColuna) $ GetMv("HP_COLUNA")
			ASORT(_ytam, , , { |x,y| x[2] < y[2] } )
		EndIf
		For _wtam := 1 to len(_ytam)
			Aadd(oMSTam:aCols, {Alltrim(_ytam[_wtam][2]),"    ",oMSNewGe1:aCols[oMSNewGe1:nAT,1],oMSNewGe1:aCols[oMSNewGe1:nAT,5],.F.})
		Next

		oMSTam:Refresh()

		oMSQtd:aCols := {}
		oMSQtd:Refresh()

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+alltrim(cProduto))

		While !EOF() .AND. alltrim(SZD->ZD_PRODUTO) == alltrim(cProduto)
			aFieldFill := {}
			Aadd(aFieldFill, SZD->ZD_COR)
			Aadd(aFieldFill,POSICIONE("SBV",1,xFilial("SBV")+'COR'+SZD->ZD_COR,"BV_DESCRI"))

			For nX := 3 to Len(oMsQtd:aHeader)-2
				Aadd(aFieldFill,0)
			Next
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,1])
			Aadd(aFieldFill, oMSNewGe1:aCols[oMSNewGe1:nAT,5])
			Aadd(aFieldFill, .F.)
			Aadd(oMsQtd:aCols, aFieldFill)



			DbSelectArea("SZD")
			DbSkip()
		EndDo
		oMSQtd:Refresh()

	EndIf

	oMSCor:Refresh()
	oMsQtd:Refresh()
	oMsTam:Refresh()
Return

user Function cadEst(_cod,_coluna,_ind)
// Cadastra produto de Corte para usar na estrutura de produtos

	cParCicPad:= AllTrim(SuperGetMV("HP_CICPAD",.F.,"002"))
	cDscCicPad:= POSICIONE("ZB7",1,xFilial("ZB7")+cParCicPad,"ZB7_DSCCIC")

	DbSelectArea("SB4")
	DbSetOrder(1)
	DbSeek(xfilial("SB4")+PADR("B"+SubStr(_cod,1,7),15))

	If !Found()
		EMail(PADR("B"+SubStr(_cod,1,7),15))
		RecLock("SB4",.T.)
		Replace B4_FILIAL 		with xfilial("SB4")
		Replace B4_COD			with PADR("B"+SubStr(_cod,1,7),15)
		Replace B4_TIPO			with "MI"
		Replace B4_LOCPAD		with "AP"
		Replace B4_GRUPO		with "MP13"
		Replace B4_DESC			with "CORTE TECIDO - REF "+SubString(_COD,1,8)
		Replace B4_UM			with "UN"
		Replace B4_POSIPI		With "60041092"
		Replace B4_YORIGEM		with "0"
		Replace B4_CLASFIS		with "00"
		Replace B4_RASTRO		with "N"
		Replace B4_LINHA		with "COR"
		Replace B4_COLUNA		with _coluna
		MsUnLock()
	EndIf

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbGoTop()
	DbSeek(xfilial("SB1")+PADR("B"+SubStr(_cod,1,7)+SubStr(_cod,9,7),15))

	If !Found()
		RecLock("SB1",.T.)
		Replace B1_FILIAL			with xfilial("SB1")
		Replace B1_COD 				with "B"+SubStr(_cod,1,7)+SubStr(_cod,9,7)
		Replace B1_DESC				with "CORTE TECIDO - REF "+SubString(_COD,1,15)
		Replace B1_TIPO				with "MI"
		Replace B1_GRUPO			with "MP13"
		Replace B1_UM				with "UN"
		Replace B1_LOCPAD			with "AP"
		Replace B1_SEGUM			with "UN"
		Replace B1_CONV				with 1
		Replace B1_TIPCONV			with "M"
		Replace B1_CONTA			with "101030201001"
		Replace B1_POSIPI			with "60041092"
		Replace B1_TIPODEC			with "N"
		Replace B1_ORIGEM			with "0"
		Replace B1_REVATU			with "001"
		Replace B1_CLASFIS			with "00"
		Replace B1_GRADE			with "S"
		Replace B1_LOCALIZ			with "N"
		Replace B1_RASTRO			with "N"
		Replace B1_VEREAN			with "13"
		Replace B1_TIPOBN			with "MI"
		Replace B1_GARANT			with "N"
		Replace B1_OPERPAD			with "01"
		Replace B1_GRTRIB with "007"
		Replace B1_CONTA 		with POSICIONE("SBM",1,xFilial("SBM")+"MP13","BM_CTACUS")
		Replace B1_CTADESP 		with POSICIONE("SBM",1,xFilial("SBM")+"MP13","BM_CTADES")
		Replace B1_MRP			with "S"
		Replace B1_XCODCIC		with cParCicPad
		Replace B1_XDSCCIC		with cDscCicPad
		Replace B1_MSBLQL		with "2"

		MsUnLock()
	EndIf

Return("B"+SubStr(_cod,1,7)+SubStr(_cod,9,7))

User Function EstSup(_oldprod,_nwprod,_IND)
//Cadastra estrutura de produto para produto de Corte (bcorte)

	DbSelectArea("SG1")
	DbSetOrder(1)
	DbSeek(xfilial("SG1")+PADR(_oldprod,15))

	While !EOF() .AND. SG1->G1_COD = PADR(_oldprod,15)

		If SG1->G1_REVINI = cRevisao
			_ind := SG1->G1_TRT
		EndIf
		DbSelectarea("SG1")
		DbSkip()
	EndDo

//	DbSelectArea("SG1")
//	DbOrderNickName("SG1GRD1")
//	DbSeek(xfilial("SG1")+PADR(_oldprod,15)+PADR(_nwprod,15)+_IND+cRevisao)

	_qry := "SELECT * FROM "+RetSqlName("SG1")+" (NOLOCK) WHERE D_E_L_E_T_ = '' AND G1_FILIAL = '"+xfilial("SG1")+"' AND G1_COD = '"+_oldprod+"' AND G1_COMP = '"+_nwprod+"' AND G1_REVINI = '"+cRevisao+"' "
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSG1A",.T.,.T.)

	DbSelectarea("TMPSG1A")
	DbGoTop()

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+_comp)
	If Found()

		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+_oldprod)

		If Found()

			If TMPSG1A->(EOF())

				If len(alltrim(_nwprod)) = 15

					_qry := "Insert into "+RetSqlName("SG1")+" (G1_FILIAL, G1_COD, G1_COMP, G1_TRT, G1_QUANT, G1_INI, G1_FIM, G1_NIV, G1_NIVINV, G1_REVINI,"
					_qry += "G1_REVFIM, R_E_C_N_O_) Values ('"+xfilial("SG1")+"','"+_oldprod+"','"
					_qry += _nwprod+"','"+_IND+"',1,'"+dtos(ddatabase)+"',"
					_qry += "'20491231', '01', '99', '"+cRevisao+"', '"+cRevisao+"',(SELECT max(R_E_C_N_O_)+1 FROM SG1010))"

					nStatus := TcSqlExec(_qry)

					If (nStatus < 0)
						MsgBox(TCSQLError(), "Erro no comando", "Stop")
					EndIf

				EndIf

			EndIf
		Else
			Alert("O produto "+_oldprod+" n�o est� cadastrado no cadastro de produtos!")
			_FECHA := .F.
		EndIf
	Else
		MessageBox("A MP "+_comp+" com o indice "+_seq1+" n�o est� cadastrada na tabela de produtos!","EstSup",16)
		_FECHA := .F.
	EndIf
	DbSelectarea("TMPSG1A")
	DbCloseArea()
	DbSelectArea("SG1")
Return()

Static Function LimpaCor()
// Replica tamanhos para cols de tamanho

	Local nX
	Local nX2

	If oMSNewGE1:aCols[oMSNewGE1:nat,4] <> "S"
		_qry := "UPDATE "+RetSqlName("SG1'")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '14"+cnomusr+"' WHERE D_E_L_E_T_ = '' AND "
		_qry += "G1_FILIAL = '"+xfilial("SG1")+"' AND left(G1_COD,11) = '"+SubStr(CProduto,1,8)+Alltrim(oMSCor:ACols[oMSCor:nAT,1])+"' AND "
		_qry += "left(G1_COMP,8) = '"+alltrim(oMsQtd:aCols[oMsQtd:nat,len(oMsQtd:aHeader)-1])+"' AND G1_REVINI = '"+cRevisao+"' "
		_qry += " AND G1_TRT = '"+Alltrim(oMSNewGE1:aCols[oMSNewGE1:nat,5])+"' AND G1_INI <> '19500101' "
	Else
		_qry := "UPDATE "+RetSqlName("SG1'")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '15"+cnomusr+"' WHERE D_E_L_E_T_ = '' AND "
		_qry += "G1_FILIAL = '"+xfilial("SG1")+"' AND left(G1_COD,11) = 'B"+SubStr(CProduto,1,7)+Alltrim(oMSCor:ACols[oMSCor:nAT,1])+"' AND "
		_qry += "left(G1_COMP,8) = '"+alltrim(oMsQtd:aCols[oMsQtd:nat,len(oMsQtd:aHeader)-1])+"' AND G1_REVINI = '"+cRevisao+"' "
		_qry += "AND G1_TRT = '"+Alltrim(oMSNewGE1:aCols[oMSNewGE1:nat,5])+"' AND G1_INI <> '19500101' "
	EndIf

	MemoWrite("HESTP001_limpacor.txt",_Qry)
	TcSqlExec(_qry)
	If MsgYesNo("Deseja limpar a cor: "+Alltrim(oMSCor:ACols[oMSCor:nAT,1])+" para TODOS os tamanhos?")
		For nX := 3 to len(oMsQtd:aHeader)-2
			oMSQtd:aCols[oMSCor:nat,nX] := 0
		Next nX
	EndIf
	oMsQtd:refresh()

Return

Static Function LimpaCon(_ctam)
// Replica tamanhos para cols de tamanho

	Local nX
	Local nX2

	If oMSNewGE1:aCols[oMSNewGE1:nat,4] <> "S"
		_qry := "UPDATE "+RetSqlName("SG1'")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '18"+cnomusr+"' WHERE D_E_L_E_T_ = '' AND "
		_qry += "G1_FILIAL = '"+xfilial("SG1")+"' AND left(G1_COD,11) = '"+SubStr(CProduto,1,8)+Alltrim(oMSQtd:ACols[oMSQtd:nAT,1])+"' AND "
		_qry += "left(G1_COMP,8) = '"+alltrim(oMsQtd:aCols[oMsQtd:nat,len(oMsQtd:aHeader)-1])+"' AND RIGHT(G1_COD, 4) = '"+Alltrim(_ctam)+"' AND G1_REVINI = '"+cRevisao+"' "
		_qry += " AND G1_TRT = '"+Alltrim(oMSNewGE1:aCols[oMSNewGE1:nat,5])+"' AND G1_INI <> '19500101' "
	Else
		_qry := "UPDATE "+RetSqlName("SG1'")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '19"+cnomusr+"' WHERE D_E_L_E_T_ = '' AND "
		_qry += "G1_FILIAL = '"+xfilial("SG1")+"' AND left(G1_COD,11) = 'B"+SubStr(CProduto,1,7)+Alltrim(oMSQtd:ACols[oMSQtd:nAT,1])+"' AND "
		_qry += "left(G1_COMP,8) = '"+alltrim(oMsQtd:aCols[oMsQtd:nat,len(oMsQtd:aHeader)-1])+"' AND RIGHT(G1_COD, 4) = '"+Alltrim(_ctam)+"' AND G1_REVINI = '"+cRevisao+"' "
		_qry += "AND G1_TRT = '"+Alltrim(oMSNewGE1:aCols[oMSNewGE1:nat,5])+"' AND G1_INI <> '19500101' "
	EndIf

	MemoWrite("HESTP001_limpacon.txt",_Qry)
	TcSqlExec(_qry)
	oMsQtd:refresh()

Return

Static Function LimpaTam()
// Replica tamanhos para cols de tamanho

	Local nX
	Local nX2

	If oMSNewGE1:aCols[oMSNewGE1:nat,4] <> "S"
		_qry := "UPDATE "+RetSqlName("SG1'")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '16"+cnomusr+"' WHERE D_E_L_E_T_ = '' AND "
		_qry += "G1_FILIAL = '"+xfilial("SG1")+"' AND left(G1_COD,8) = '"+SubStr(CProduto,1,8)+"' AND "
		_qry += "left(G1_COMP,8) = '"+alltrim(oMsQtd:aCols[oMsQtd:nat,len(oMsQtd:aHeader)-1])+"' AND G1_REVINI = '"+cRevisao+"' "
		_qry += " AND G1_TRT = '"+Alltrim(oMSNewGE1:aCols[oMSNewGE1:nat,5])+"' AND G1_INI <> '19500101' "
	Else
		_qry := "UPDATE "+RetSqlName("SG1'")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '17"+cnomusr+"' WHERE D_E_L_E_T_ = '' AND "
		_qry += "G1_FILIAL = '"+xfilial("SG1")+"' AND left(G1_COD,8) = 'B"+SubStr(CProduto,1,7)+"' AND "
		_qry += "left(G1_COMP,8) = '"+alltrim(oMsQtd:aCols[oMsQtd:nat,len(oMsQtd:aHeader)-1])+"' AND G1_REVINI = '"+cRevisao+"' "
		_qry += "AND G1_TRT = '"+Alltrim(oMSNewGE1:aCols[oMSNewGE1:nat,5])+"' AND G1_INI <> '19500101' "
	EndIf

	MemoWrite("HESTP001_limpatam.txt",_Qry)
	TcSqlExec(_qry)
	MessageBox("Tamanho atualizado em todas as cores! ","TAMOK",0)

	oMsQtd:refresh()

Return

Static Function ReplyCor()
// Replica a cor para acols de cor

	Local nX

	If MsgYesNo("Ser�o replicadas as informa��es da Cor: "+oMSCor:aCols[oMSCor:nat,1]+" para as outras cores?")
		For nX := 1 to len(oMSCor:aCols)
			If nX <> oMSCor:nat
				oMSCor:aCols[nX,2] := oMSCor:aCols[oMSCor:nat,2]
			EndIf
		Next nX
	EndIf

	oMsCor:refresh()

Return

Static Function ReplyTam()
// Replica tamanhos para cols de tamanho

	Local nX

	If MsgYesNo("Ser�o replicadas as informa��es do Tamanho: "+oMSTam:aCols[oMSTam:nat,1]+" para os outros tamanhos?")
		For nX := 1 to len(oMSTam:aCols)
			If nX <> oMSTam:nat
				oMSTam:aCols[nX,2] := oMSTam:aCols[oMSTam:nat,2]
			EndIf
		Next nX
	EndIf

	oMsTam:refresh()

Return

Static Function ReplyQtdLn()
// Replica as quantidades por linha

	Local nX
	Local nX2

	If MsgYesNo("Ser�o replicadas as informa��es da Cor: "+oMSQtd:aCols[oMSQtd:nat,1]+" para as outras cores?")
		For nX := 1 to len(oMSQtd:aCols)
			for nX2 := 3 to len(oMSQtd:aCols[oMSQtd:nat])-1
				If nX <> oMSQtd:nat .AND. !AllTrim(Str(nX2)) $ "1"
					oMSQtd:aCols[nX,nX2] := oMSQtd:aCols[oMSQtd:nat,nX2]
				EndIf
			Next nX2
		Next nX
	EndIf

	oMsQtd:refresh()

Return

Static Function ReplyQtdCl()
// Copia as quatidades para as colunas

	Local nX
	Local nX2

	If MsgYesNo("Ser�o replicadas as informa��es do Tamanho: "+oMSQtd:aCols[oMSQtd:nat,1]+" para outros Tamanhos?")
		for nX2 := omsqtd:obrowse:colpos+1 to len(oMSQtd:aCols[oMSQtd:nat])-3 //3
			If nX <> oMSQtd:nat
				oMSQtd:aCols[oMSQtd:nat,nX2] := oMSQtd:aCols[oMSQtd:nat,omsqtd:obrowse:colpos] //2
			EndIf
		Next nX2
	EndIf

	oMsQtd:refresh()

Return

User Function VldCor(_cor,_n)

	Local lRet := .T.
	Local _i

	lRet	:= U_ExistCod(5,_cor)

	For _i:=1 to len(oMSGrade:aCols)
		If alltrim(_cor) == alltrim(oMSGrade:aCols[_i,1]) .AND. _i <> _n
			Alert("A COR j� foi incluida no produto, selecione outra COR.")
			lRet := .F.
			Return(lRet)
		ElseIf alltrim(_cor) == ""
			Alert("N�o poder� incluir uma linha em branco.")
			lRet := .F.
			Return(lRet)
		EndIf
	Next

Return(lRet)

User Function ExistCod(nCod,cCod,cCorP)
// Rotina para preenchimento dos campos de descri��o
	Local _t := 0
	Local _c := 0

	If nCod==1   // valida��o do NCM
		If alltrim(cCod) <> ""

			cCod := Alltrim(StrTran(cCod,".",""))

			If SELECT("VLDDB") > 0
				VLDDB->(DbCloseArea())
			EndIf

			cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SYD")
			cQuery  += " (nolock) WHERE D_E_L_E_T_<>'*' AND YD_TEC='"+cCod+"'"

			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)

			If VLDDB->COUNT==0 .AND. cCod<>""
				Alert("O Codigo de NCM/POS.IPI Digitado n�o existe no cadastro.")
				oPosIPI:SetFocus()
			EndIf

			VLDDB->(DbCloseArea())
		EndIf

	ElseIf nCod==2  //valida��o de cor

		lRet 	:= .T.
		cLinha	:= If (cLinha<>"COR","COR",cLinha)

		cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SB1")
		cQuery  += " (nolock) WHERE D_E_L_E_T_<>'*' AND B1_FILIAL = '"+xFilial("SB1")+"'"
		cQuery  += " 		AND SubString(B1_COD,1,8)='"+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+"'"
		cQuery  += " 		AND SubString(B1_COD,9,3) ='"+cCod+"'"

		If SELECT("VLDDB") > 0
			VLDDB->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)

		If VLDDB->COUNT==0 .AND. AllTrim(cCod)<>""
			Alert("A COR selecionada n�o existe para o componete "+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+".")
			lRet := .F.
		EndIf

		//Verifica a exist�ncia da estrutura em todas as tabelas envolvidas para liberar o cadastro.
		If SubStr(Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]),1,4) $ cMvEstru .AND. AllTrim(cCod)<>""
			If !VldEstr(oMSNewGe1:aCols[oMSNewGe1:nAT,1],cCod,,2)
				Alert("Ficha composta "+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+" encontra-se vazia, favor verificar.")
				lRet := .F.
			Endif
		Endif
		VLDDB->(DbCloseArea())

		cMp  := Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])  // MP
		cInd := Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,5]) //INDICE
		cMpC := M->ZG_CORNV								 // MP COR
		cPaC := Alltrim(oMSCor:ACols[oMSCor:nAT,1]) 	// PA COR

		If lRet
			nopp := 1
			LmpAuto(nopp,cProduto,cMp,cMpC,cInd,cCorP)
		EndIf
		oMSCOR:refresh()
		Return(lRet)

	ElseIf nCod==3  //valida��o de Tamanho

		lRet := .T.

		cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SB1")
		cQuery  += " (nolock) WHERE D_E_L_E_T_<>'*' AND B1_FILIAL = '"+xFilial("SB1")+"'"
		cQuery  += " 		AND SubString(B1_COD,1,8)='"+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+"'"
		cQuery  += " 		AND SubString(B1_COD,12,4)='"+cCod+"'"

		If SELECT("VLDDB") > 0
			VLDDB->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)

		If VLDDB->COUNT==0 .AND. AllTrim(cCod)<>""
			Alert("O Tamanho Selecionado n�o existe para o componete "+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+".")
			lRet := .F.
		EndIf

		VLDDB->(DbCloseArea())


		cMp  := Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])  // MP
		cInd := Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,5]) // INDICE
		cMpT := M->ZK_TAMNV 							 // TAM MP
		cMpC := Alltrim(oMSCor:ACols[oMSCor:nAT,2])		//COR MP


		If SubStr(cMp,1,4) $ cMvEstru .AND. AllTrim(cCod)<>""
			If !Vldtam()
				Alert("Ficha composta "+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+" encontra-se vazia, favor verificar.")
				//lRet:= .F.
			Endif
		Endif

		If lRet
			LmpAutoT(cProduto,cMp,cMpT,cInd,cMpC)
		EndIf
		oMStAM:refresh()

		If lRet
			VERTAM(cProduto,cMp,cMpC,cInd,cMpT)
		EndIf
		oMStAM:refresh()
		Return(lRet)

	ElseIf nCod==4  //Valida Status

		lRet := .T.

		cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("ZA8")
		cQuery  += " (nolock) WHERE D_E_L_E_T_ = '' AND ZA8_FILIAL = '"+xFilial("ZA8")+"' AND ZA8_CODIGO = '"+cCod+"'"

		If SELECT("VLDDB") > 0
			VLDDB->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)

		If VLDDB->COUNT==0
			Alert("O codigo selecionado n�o existe no cadastro.")
			lRet := .F.
		EndIf

		VLDDB->(DbCloseArea())

		Return(lRet)

	ElseIf nCod==5  //Valida��o de cor

		lRet 	:= .T.
		cLinha	:= If (cLinha<>"COR","COR",cLinha)

		cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SBV")
		cQuery  += " (nolock) WHERE BV_FILIAL = '"+xFilial("SBV")+"' AND BV_TABELA = '"+cLinha+"' "
		cQuery  += " AND D_E_L_E_T_ = ''  AND BV_CHAVE='"+cCod+"'"

		If SELECT("VLDDB") > 0
			VLDDB->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)

		If VLDDB->COUNT==0
			Alert("A COR selecionada n�o existe no cadastro.")
			lRet := .F.
		EndIf

		VLDDB->(DbCloseArea())

		Return(lRet)

	ElseIf nCod==6  //valida��o de Tamanho Tela de Grade.

		lRet := .T.

		cQuery  := " SELECT * FROM "+RetSQLName("SZD")
		cQuery  += " (NOLOCK) WHERE D_E_L_E_T_= '' AND ZD_FILIAL = '"+xFilial("SZD")+"'"
		cQuery  += " AND ZD_PRODUTO = '"+Alltrim(cCodigo)+"' AND ZD_COR ='"+Alltrim(oMSGrade:aCols[oMSGrade:nAT,1])+"'"

		If SELECT("VLDDB") > 0
			VLDDB->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)

		For _t := 5 to len(oMSGrade:aHeader)
			_campo3 := Alltrim(oMSGrade:aHeader[_t][2])
			If Valtype(M->&_campo3) != "U" .AND. SUBSTRING(_CAMPO3,1,6) == "ZD_TAM"
				If M->&_campo3 != "X" .AND. VLDDB->&_campo3 == "X"
					MessageBox("N�o � poss�vel deletar um tamanho j� marcado","TOTVS",16)
					M->&_campo3 := "X"
					lRet := .F.
				EndIf
			EndIf

		Next _t
		oMSGrade:refresh()
		Return(lRet)

	ElseIf nCod==7  //valida��o de consumo Tela de Grade.

		lRet := .T.

		cQuery  := " SELECT * FROM "+RetSQLName("SZF")
		cQuery  += " (NOLOCK) WHERE D_E_L_E_T_= '' AND ZF_FILIAL = '"+xFilial("SZF")+"'"
		cQuery  += " AND ZF_PRODUTO = '"+Alltrim(cCodProd)+"' AND ZF_COR ='"+Alltrim(oMSQtd:aCols[oMSQtd:nAT,1])+"'"
		cQuery  += " AND ZF_COMP ='"+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]) +"' AND ZF_INDICE ='"+Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,5]) +"'"

		If SELECT("VLDCON") > 0
			VLDCON->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDCON",.T.,.T.)

		For _c := 2 to len(oMSQtd:aHeader)
			_campo4 := Alltrim(oMSQtd:aHeader[_c][2])
			If Valtype(M->&_campo4) != "U" .AND. SUBSTRING(_CAMPO4,1,6) == "ZF_TAM"
				If M->&_campo4 == 0 .AND. VLDCON->&_campo4 <> 0
					_ctam := Padl(Alltrim(oMSQtd:aHeader[_c][1]),4,"0")
					nret:= MessageBox("Deseja realmente zerar o consumo da MP: " +Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+ " na Cor: " +Alltrim(oMSQtd:aCols[oMSQtd:nAT,1])+" e no tamanho: "+Alltrim(Alltrim(oMSQtd:aHeader[_c][1]))+ " ? ","TOTVS",4)
					If nret == 6
						lLmpCon := .T.
						LimpaCon(_ctam)
					Else
						MessageBox("Cancelado pelo Usu�rio! ","TOTVS",48)
						M->&_campo4 := VLDCON->&_campo4
					EndIf
				EndIf
			EndIf

		Next _c
		oMSQtd:refresh()
		Return(lRet)


	EndIf

Return

Static Function VERTAM(cProduto,cMp,cMpC,cInd,cMpT)

	Local cColuna := POSICIONE("SB4",1,xFilial("SB4")+cMp,"B4_COLUNA")
	Local cEOL	     := +Chr(13)+Chr(10)
	Local cQuery1
	Local cQuery2
	Local aFields   := {}
	Local aHeaderEx := {}
	Local _x := 0
	Local _i := 0
	Local lOk := .T.

//	Define field properties
	If alltrim(cColuna) <> ""
		cQuery1 := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI "+cEOL
		cQuery1 += "FROM "+RetSQLName("SBV")+" (NOLOCK) "+cEOL
		cQuery1 += "WHERE D_E_L_E_T_ = '' "+cEOL
		cQuery1 += "		AND BV_FILIAL = '" +xFilial("SBV") +"' "+cEOL
		cQuery1 += "		AND BV_TABELA = '"+SB4->B4_COLUNA+"' "+cEOL
		cQuery1 += "ORDER BY R_E_C_N_O_ "+cEOL

		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSBV",.T.,.T.)

		cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" (NOLOCK) "+cEOL
		cQuery2  += "WHERE D_E_L_E_T_ = ' ' "+cEOL
		cQuery2  += "		AND ZD_FILIAL = '"+xFilial("SZD")+"' "+cEOL
		cQuery2  += "		AND ZD_PRODUTO='"+AllTrim(cMp)+"' AND ZD_COR = '"+Alltrim(cMpC)+"'"+cEOL

		If Select("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)
		DbSelectArea("TMPSZD")
		TMPSZD->(DbGoTop())

		While TMPSZD->(!EOF())
			For _i := 1 to 200
				If &("TMPSZD->ZD_TAM"+StrZero(_i,3))=="X"
					_campo	:= "ZF_TAM"+StrZero(_i,3)
					_campbv	:= "ZD_TAM"+StrZero(_i,3)
					For _x := 1 to _i
						cQdes := " SELECT * FROM "+RetSQLName("SBV")+" (NOLOCK) WHERE BV_TABELA = '"+cColuna+"' AND D_E_L_E_T_ = '' AND BV_XCAMPO = '"+_campbv+"' "
						If Select("TMPQDES") > 0
							TMPQDES->(DbCloseArea())
						EndIf
						dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQdes),"TMPQDES",.T.,.T.)
						_chave	:= TMPQDES->BV_CHAVE
					Next _x
					DbSelectArea("SX3")
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo))
						nX := ASCAN( aFields, {|x| alltrim(x) == alltrim(_campo)})
						If nX = 0
							Aadd(aFields,_campo)
							Aadd(aHeaderEx, {alltrim(_chave)+ Space(5),SX3->X3_CAMPO,"@E 99",2,0,"",SX3->X3_USADO,"N","","R","",""})
						Endif
					Endif
				Endif
			Next
			TMPSZD->(DbSkip())
		EndDo
	EndIf
	_tem := ASCAN( aHeaderEx, {|x| ALLTRIM(x[1]) == ALLTRIM(cMpT)})
	If _tem = 0
		lOk := .F.
		MessageBox(" A MP "+AllTrim(cMp)+" n�o possui esse tamanho cadastrado na cor " +Alltrim(cMpC)+ " !","SEM-TAM",16)
		M->ZK_TAMNV := ""
	Endif

Return lOk

Static Function LmpAutoT(cProduto,cMp,cMpT,cInd,cMpC)

	Local cQrl := ""
	Local lOk := .T.

	cMpC:= Alltrim(oMSCor:ACols[oMSCor:nAT,2]) // MP COR
	cPfTam := Alltrim(oMStam:ACols[oMStam:nAT,1]) // TAM PF

	cQrl += " SELECT * FROM "+RetSQLName("SZK")+" (NOLOCK) WHERE ZK_PRODUTO = '"+Alltrim(cProduto)+"'   "
	cQrl += " AND ZK_INDICE = '"+Alltrim(cInd)+"' AND ZK_TAM = '"+Alltrim(cPfTam)+"' AND D_E_L_E_T_ = '' "

	If SELECT("XCO") > 0
		XCO->(dbclosearea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrl),"XCO",.F.,.T.)

	If 	Alltrim(XCO->ZK_TAMNV) != Alltrim(cMpT) .AND. !Empty(Alltrim(XCO->ZK_TAMNV))
		nRet:= MessageBox("Deseja atualizar o TAMANHO na estrutura ?","TAM",4)
		If nRet== 6
			lLmpTam := .T.
			LimpaTam()
			lLmpCor	:= .T.

		Else
			M->ZK_TAMNV := Alltrim(oMStam:ACols[oMStam:nAT,2])//XCO->ZK_TAMNV
			oMStAM:refresh()
			lOk := .F.
		EndIf
	EndIf
Return lOk


Static Function LmpAuto(nopp,cProduto,cMp,cMpC,cInd,cCorP)

	Local cQrl := ""
	Local lOk := .T.


	cQrl += " SELECT * FROM "+RetSQLName("SZG")+" (NOLOCK) WHERE ZG_PRODUTO = '"+Alltrim(cProduto)+"'   "
	cQrl += " AND ZG_INDICE = '"+Alltrim(cInd)+"' AND ZG_COMP = '"+Alltrim(cMp)+"' AND ZG_COR = '"+Alltrim(cPaC)+"' AND D_E_L_E_T_ = '' "

	If SELECT("XCO") > 0
		XCO->(dbclosearea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQrl),"XCO",.F.,.T.)

	If 	Alltrim(XCO->ZG_CORNV) != Alltrim(cMpC) .AND. !Empty(XCO->ZG_CORNV)
		nRet:= MessageBox("Deseja atualizar a COR na estrutura ?","cor",4)
		If nRet== 6
			LimpaCor()
			lLmpCor	:= .T.
		Else
			M->ZG_CORNV := XCO->ZG_CORNV
			lOk := .F.
		EndIf
	EndIf


Return lOk


Static Function AjuSX1(cPerg)

	Local aAreaAtu	:= GetArea()
	Local aAreaSX1	:= SX1->( GetArea() )

	PutSx1(cPerg,"01","Status de Produtos","Status de Produtos","Status de Produtos","Mv_ch1","C",99,0,0,"G","U_HE001TP()",   "","","N","Mv_par01","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione os status de produtos.",""},{""},{""},"")
	PutSx1("HESTC1","01","Tipo","Tipo","Tipo","Mv_ch1","C",10,0,0,"G","",   "","","N","Mv_par01","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione o Tipo.",""},{""},{""},"")

	RestArea( aAreaSX1 )
	RestArea( aAreaAtu )

Return(cPerg)

User Function HE001TP() //l1Elem,lTipoRet)

	Local xvPar
	Local aArea  := GetArea()
	Local cTitulo := ""
	Local lRet  := .T.
	Local aBox  := {}
	Local nTam  := 4
	Local MvParDef := ""

	xvPar:=&(Alltrim(ReadVar()))   // Carrega Nome da Variavel do Get em Questao
	mvRet:=Alltrim(ReadVar())    // Iguala Nome da Variavel ao Nome variavel de Retorno

	dbSelectArea("ZA8")
	DbSetOrder(1)
	DbGoTop()
	cTitulo := "Status de Produtos"
	While !Eof()
		Aadd(aBox,ZA8->ZA8_CODIGO + " - " + Alltrim(ZA8->ZA8_DESCRI))
		MvParDef += SubStr(ZA8->ZA8_CODIGO,1,4)
		dbSkip()
	EndDo

	If f_Opcoes( @xvPar,;  // uVarRet
		cTitulo,;  // cTitulo
		@aBox,;   // aOpcoes
		MvParDef,;  // cOpcoes
		,;    // nLin1
		,;    // nCol1
		,;    // l1Elem
		nTam,;    // nTam
		Len( aBox ),; // nElemRet
		,;    // lMultSelect
		,;    // lComboBox
		,;    // cCampo
		,;    // lNotOrdena
		,;    // NotPesq
		,;   // ForceRetArr
		)    // F3

		&MvRet := xvpar                                                                          // Devolve Resultado
	EndIf

	RestArea(aArea)

Return(.T.)

User Function HE001OC(l1Elem,lTipoRet)

	Local MvPar
	Local aArea  := GetArea()
	Local cTitulo := ""
	Local lRet  := .T.
	Local aBox  := {}
	Local nTam  := 6
	Local MvParDef := ""
	Public hocasiao := ""

	MvPar:=&(Alltrim(ReadVar()))   // Carrega Nome da Variavel do Get em Questao
	mvRet:=Alltrim(ReadVar())    // Iguala Nome da Variavel ao Nome variavel de Retorno

	dbSelectArea("ZAD")
	DbSetOrder(1)
	DbGoTop()
	cTitulo := "Ocasiao"
	While !Eof()
		Aadd(aBox,ZAD->ZAD_CODIGO + "- " + Alltrim(ZAD->ZAD_DESCRI))
		MvParDef += SubStr(ZAD->ZAD_CODIGO,1,5)+"-"
		dbSkip()
	EndDo

	If f_Opcoes( @MvPar,;  // uVarRet
		cTitulo,;  // cTitulo
		@aBox,;   // aOpcoes
		MvParDef,;  // cOpcoes
		,;    // nLin1
		,;    // nCol1
		,;    // l1Elem
		nTam,;    // nTam
		Len( aBox ),; // nElemRet
		,;    // lMultSelect
		,;    // lComboBox
		,;    // cCampo
		,;    // lNotOrdena
		,;    // NotPesq
		,;   // ForceRetArr
		)    // F3

		&MvRet := mvpar                                                                          // Devolve Resultado
	EndIf

	hocasiao := StrTran(mvpar,'*','')

	RestArea(aArea)

Return lRet


User function buscasb1()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	Pergunte("DESCSB1",.T.)

	_qry := "SELECT B1_DESC, B1_COD FROM "+RetSqlName("SB1")+" SB1 "
	_qry += "(nolock) WHERE SB1.D_E_L_E_T_ = '' AND (B1_DESC like '%"+upper(alltrim(MV_PAR01))+"%' or B1_COD like '%"+upper(alltrim(MV_PAR01))+"%')  "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->B1_DESC,XXX->B1_COD})
		DbSkip()
	EndDo
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()

	If Len(aItems) <= 0
		Alert("Produto n�o encontrado")
		Return(lRet)
	EndIf

	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Produto" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Descri��o","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2]}}

	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
	DEFINE SBUTTON FROM 88,105 TYPE 4 ENABLE OF oDlgSu9 ACTION (AxInclui("SB1"))
	DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (U_cadsB1("V",aItems[oLbx1:nAt][2]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SB1")
		DbSetorder(1)
		DbSeek(xFilial("SB1")+aItems[nPos][2])
	EndIf

Return(lRet)


user function buscasa1()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	Pergunte("DESCSA1",.T.)

	_qry := "SELECT A1_NOME, A1_NREDUZ, A1_COD, A1_CGC FROM "+RetSqlName("SA1")+" SA1 (nolock) "
	_qry += "WHERE SA1.D_E_L_E_T_ = '' AND (A1_NOME like '%"+upper(alltrim(MV_PAR01))+"%' or A1_COD like '%"+upper(alltrim(MV_PAR01))+"%' or "
	_qry += "A1_NREDUZ like '%"+upper(alltrim(MV_PAR01))+"%' or A1_CGC like '%"+upper(alltrim(MV_PAR01))+"%' ) "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->A1_NOME,XXX->A1_NREDUZ,XXX->A1_CGC,XXX->A1_COD})
		DbSkip()
	EndDo
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()

	If Len(aItems) <= 0
		Alert("Cliente n�o encontrado")
		Return(lRet)
	EndIf

	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Cliente" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Raz�o","Fantasia","CNPJ","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2],;
		aItems[oLbx1:nAt,3],;
		aItems[oLbx1:nAt,4]}}



	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
	DEFINE SBUTTON FROM 88,105 TYPE 4 ENABLE OF oDlgSu9 ACTION (AxInclui("SA1"))
	DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (U_cadsA1("V",aItems[oLbx1:nAt][4]))
//    DEFINE SBUTTON FROM 88,140 TYPE 4 ENABLE OF oDlgSu9 ACTION (U_cadsz1("I","")) //4
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SA1")
		DbSetorder(1)
		DbSeek(xFilial("SA1")+aItems[nPos][4])
	EndIf

Return(lRet)

User Function CadSA1(_tp,_cod)

	_rarea := Getarea()
	DbSelectArea("SA1")
	cCadastro := "Cadastro de Cliente"
	DbSetOrder(1)
	DbSeek(xfilial("SA1")+_cod)
	nReg := Recno()
	AxVisual("SA1",nReg,4)
	RestArea(_rarea)

Return

User Function CadSA2(_tp,_cod)

	_rarea := Getarea()
	DbSelectArea("SA2")
	cCadastro := "Cadastro de Fornecedor"
	DbSetOrder(1)
	DbSeek(xfilial("SA2")+_cod)
	nReg := Recno()
	AxVisual("SA2",nReg,4)
	RestArea(_rarea)

Return

User Function CadSB1(_tp,_cod)

	_rarea := Getarea()
	DbSelectArea("SB1")
	cCadastro := "Cadastro de Produto"
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+_cod)
	nReg := Recno()
	AxVisual("SB1",nReg,4)
	RestArea(_rarea)

Return


user function buscasa2()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	Pergunte("DESCSA2",.T.)

	_qry := "SELECT A2_NOME, A2_NREDUZ, A2_COD, A2_CGC FROM "+RetSqlName("SA2")+" SA2 (nolock) "
	_qry += "WHERE SA2.D_E_L_E_T_ = '' AND (A2_NOME like '%"+upper(alltrim(MV_PAR01))+"%' or A2_COD like '%"+upper(alltrim(MV_PAR01))+"%' or "
	_qry += "A2_NREDUZ like '%"+upper(alltrim(MV_PAR01))+"%' or A2_CGC like '%"+upper(alltrim(MV_PAR01))+"%' ) "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->A2_NOME,XXX->A2_NREDUZ,XXX->A2_CGC,XXX->A2_COD})
		DbSkip()
	EndDo
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()

	If Len(aItems) <= 0
		Alert("Fornecedor n�o encontrado")
		Return(lRet)
	EndIf

	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Fornecedor" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Raz�o","Fantasia","CNPJ","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2],;
		aItems[oLbx1:nAt,3],;
		aItems[oLbx1:nAt,4]}}



	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
	DEFINE SBUTTON FROM 88,105 TYPE 4 ENABLE OF oDlgSu9 ACTION (AxInclui("SA2"))
	DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (U_cadsA2("V",aItems[oLbx1:nAt][4]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SA2")
		DbSetorder(1)
		DbSeek(xFilial("SA2")+aItems[nPos][4])
	EndIf

Return(lRet)



user function buscased()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	Pergunte("DESCSED",.T.)

	_qry := "SELECT ED_DESCRIC, ED_CODIGO FROM "+RetSqlName("SED")+" SED (nolock) "
	_qry += "WHERE SED.D_E_L_E_T_ = '' AND (ED_DESCRIC like '%"+upper(alltrim(MV_PAR01))+"%' or ED_CODIGO like '%"+upper(alltrim(MV_PAR01))+"%')  "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->ED_DESCRIC,XXX->ED_CODIGO})
		DbSkip()
	EndDo
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()

	If Len(aItems) <= 0
		Alert("Natureza n�o encontrada")
		Return(lRet)
	EndIf

	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Natureza" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Descri��o","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2]}}

	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (U_cadsB1("V",aItems[oLbx1:nAt][2]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SED")
		DbSetorder(1)
		DbSeek(xFilial("SED")+aItems[nPos][2])
	EndIf

Return(lRet)


user function buscacor()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	_qry := "SELECT SubString(B1_COD,9,3) as COD, BV_DESCRI FROM "+RetSqlName("SB1")+" SB1 (nolock) "
	_qry += "inner join "+RetSqlName("SBV")+" SBV (nolock) on SBV.D_E_L_E_T_ = '' AND BV_TABELA = 'COR' AND BV_CHAVE = SubString(B1_COD,9,3) "
	_qry += "WHERE SB1.D_E_L_E_T_ = '' AND B1_COD like '"+alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+"%' "
	_qry += "group by SubString(B1_COD,9,3), BV_DESCRI "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->BV_DESCRI,XXX->COD})
		DbSkip()
	EndDo
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()

	If Len(aItems) <= 0
		Alert("Cor n�o encontrada")
		Return(lRet)
	EndIf

	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Cores" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Descri��o","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2]}}

	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (U_cadsB1("V",aItems[oLbx1:nAt][2]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SBV")
		DbSetorder(1)
		DbSeek(xFilial("SBV")+"COR"+aItems[nPos][2])
	EndIf

Return(lRet)

user function buscatam()

	Local oDlgSu9														// Tela
	Local oLbx1                                                         // Listbox
	Local nPosLbx  := 0                                                 // Posicao do List
	Local aItems   := {}                                                // Array com os itens
	Local nPos     := 0                                                 // Posicao no array
	Local cAssunto := ""                                                // Descricao do Assunto
	Local lRet     := .F.                                               // Retorno da funcao

	CursorWait()

	DbSelectArea("SB4")
	DbSetOrder(1)
	DbSeek(xfilial("SB4")+alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1]))

	_qry := "SELECT SubString(B1_COD,12,4) as COD, BV_DESCRI FROM "+RetSqlName("SB1")+" SB1 (nolock) "
	_qry += "inner join "+RetSqlName("SBV")+" SBV (nolock) on SBV.D_E_L_E_T_ = '' AND BV_TABELA = '"+SB4->B4_COLUNA+"' AND BV_CHAVE = SubString(B1_COD,12,4) "
	_qry += "WHERE SB1.D_E_L_E_T_ = '' AND B1_COD like '"+alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,1])+"%' "
	_qry += "group by SubString(B1_COD,12,4), BV_DESCRI "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	DbSelectArea("XXX")
	DbGoTop()

	While !Eof()
		Aadd(aItems,{XXX->BV_DESCRI,XXX->COD})
		DbSkip()
	EndDo
	DbSelectArea("XXX")
	DbCloseArea()
	CursorArrow()

	If Len(aItems) <= 0
		Alert("Tamanho n�o encontrada")
		Return(lRet)
	EndIf

	DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Cadastro de Tamanhos" PIXEL

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
		"Descri��o","C�digo";	//"Descricao"
	SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo"
	oLbx1:SetArray(aItems)
	oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
		aItems[oLbx1:nAt,2]}}

	oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (U_cadsB1("V",aItems[oLbx1:nAt][2]))
	DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
	DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

	ACTIVATE MSDIALOG oDlgSu9 CENTERED

	If lRet
		DbSelectarea("SBV")
		DbSetorder(1)
		DbSeek(xFilial("SBV")+SB4->B4_COLUNA+aItems[nPos][2])
	EndIf

Return(lRet)

User Function AjuCor

	If Pergunte("AJUCOR",.t.)
		DbSelectArea("SBV")
		DbSetOrder(1)
		DbSeek(xfilial("SBV")+"COR"+MV_PAR01)

		If Found()

			If SELECT("TMP") > 0
				TMP->(dbclosearea())
			EndIf

			_qry := "SELECT TOP 1 ZD_DESCRIC FROM "+RETSQLNAME("SZD")+ " (NOLOCK) WHERE ZD_COR = '"+MV_PAR01+"' AND D_E_L_E_T_ = '' "
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMP",.F.,.T.)


			_QRY1 := "UPDATE "+RETSQLNAME("SB1")+" SET B1_DESC = REPLACE(B1_DESC,'"+ALLTRIM(TMP->ZD_DESCRIC)+"','"+ALLTRIM(SBV->BV_DESCRI)+"') WHERE D_E_L_E_T_ = '' AND B1_DESC LIKE '%"+ALLTRIM(TMP->ZD_DESCRIC)+"%' AND SUBSTRING(B1_COD,9,3) = '"+MV_PAR01+"'
			nSta := TCSQLEXEC(_QRY1)


			_QRY2 := "UPDATE "+RETSQLNAME("SZD")+" SET ZD_DESCRIC = '"+ALLTRIM(SBV->BV_DESCRI)+"' WHERE D_E_L_E_T_ = '' AND ZD_COR = '"+MV_PAR01+"'"
			nStatus := TCSQLEXEC(_QRY2)

			If (nStatus < 0) .OR. (nSta < 0)
				MsgBox(TCSQLError(), "Erro no comando", "Stop")
			EndIf
		EndIf

		Alert("As cores foram ajustadas para todos os Produtos!")

	EndIf

Return


User Function Exclui()

	cCodProd	:= AllTrim(SB4->B4_COD)
	cExistSB1	:= .F.
	cExistSG1	:= .F.

	cTexto1 := "Este processo ir� limpar todos os registros nas "+CRLF
	cTexto1 += " tabelas customizadas!"+CRLF

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+AllTrim(cCodProd))
	If Found()
		cExistSB1 := .T.
	EndIf

	DbSelectArea("SG1")
	DbSetOrder(1)
	DbSeek(xfilial("SG1")+AllTrim(cCodProd))
	If Found()
		cExistSG1 := .T.
	EndIf

	If MsgYesNo(cTexto1)

		If cExistSB1 == .F. .AND. cExistSG1 == .F.

			DbSelectArea("SZD")
			DbSetOrder(1)
			DbSeek(xfilial("SZD")+cCodProd)
			While !EOF() .AND. SubString(SZD->ZD_PRODUTO,1,8) = cCodProd
				RecLock("SZD",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			EndDo

			DbSelectArea("SZE")
			DbSetOrder(1)
			DbSeek(xfilial("SZE")+cCodProd)
			While !EOF() .AND. SubString(SZE->ZE_PRODUTO,1,8) = cCodProd
				RecLock("SZE",.F.)
				SZE->ZE_YHORDEL := choradel
				SZE->ZE_YDTDELE := DATE()
				SZE->ZE_YUSRDEL := ccodusr
				SZE->ZE_YNUSRDE := cnomusr
				dbdelete()
				MsUnLock()
				DbSkip()
			EndDo

			DbSelectArea("SZF")
			DbSetOrder(1)
			DbSeek(xfilial("SZF")+cCodProd)
			While !EOF() .AND. SubString(SZF->ZF_PRODUTO,1,8) = cCodProd
				RecLock("SZF",.F.)
				SZF->ZF_YHORDEL := choradel
				SZF->ZF_YDTDELE := DATE()
				SZF->ZF_YUSRDEL := ccodusr
				SZF->ZF_YNUSRDE := cnomusr
				dbdelete()
				MsUnLock()
				DbSkip()
			EndDo

			DbSelectArea("SZG")
			DbSetOrder(1)
			DbSeek(xfilial("SZG")+cCodProd)
			While !EOF() .AND. SubString(SZG->ZG_PRODUTO,1,8) = cCodProd
				RecLock("SZG",.F.)
				SZG->ZG_YHORDEL := choradel
				SZG->ZG_YDTDELE := DATE()
				SZG->ZG_YUSRDEL := ccodusr
				SZG->ZG_YNUSRDE := cnomusr
				dbdelete()
				MsUnLock()
				DbSkip()
			EndDo

			DbSelectArea("SZH")
			DbSetOrder(1)
			DbSeek(xfilial("SZH")+cCodProd)
			While !EOF() .AND. SubString(SZH->ZH_PRODUTO,1,8) = cCodProd
				RecLock("SZH",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			EndDo

			DbSelectArea("SZI")
			DbSetOrder(1)
			DbSeek(xfilial("SZI")+cCodProd)
			While !EOF() .AND. SubString(SZI->ZI_PRODUTO,1,8) = cCodProd
				RecLock("SZI",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			EndDo

			DbSelectArea("SZK")
			DbSetOrder(1)
			DbSeek(xfilial("SZK")+cCodProd)
			While !EOF() .AND. SubString(SZK->ZK_PRODUTO,1,8) = cCodProd
				RecLock("SZK",.F.)
				dbdelete()
				SZK->ZK_YHORDEL := choradel
				SZK->ZK_YDTDELE := DATE()
				SZK->ZK_YUSRDEL := ccodusr
				SZK->ZK_YNUSRDE := cnomusr
				MsUnLock()
				DbSkip()
			EndDo

			DbSelectArea("SZL")
			DbSetOrder(1)
			DbSeek(xfilial("SZL")+cCodProd)
			While !EOF() .AND. SubString(SZL->ZL_PRODUTO,1,8) = cCodProd
				RecLock("SZL",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			EndDo

			DbSelectArea("SZN")
			DbSetOrder(1)
			DbSeek(xfilial("SZN")+cCodProd)
			While !EOF() .AND. SubString(SZN->ZN_PRODUTO,1,8) = cCodProd
				RecLock("SZN",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			EndDo

			DbSelectArea("SZO")
			DbSetOrder(1)
			DbSeek(xfilial("SZO")+cCodProd)
			While !EOF() .AND. SubString(SZO->ZO_PRODUTO,1,8) = cCodProd
				RecLock("SZO",.F.)
				dbdelete()
				MsUnLock()
				DbSkip()
			EndDo

			MsgInfo("Limpeza efetuada com sucesso!","Limpeza Ok")

		Else
			If 	cExistSB1 == .T. .AND. cExistSG1 == .T.
				Alert("Favor efetuar a limpeza do Cadastro de Produto (padr�o) e Cadastro de Grades (padr�o), antes de efetuar o processo de Limpeza")
			ElseIf cExistSB1 == .T.
				Alert("Favor efetuar a limpeza do Cadastro de Produto (padr�o), antes de efetuar o processo de Limpeza")
			ElseIf cExistSG1 == .T.
				Alert("Favor efetuar a limpeza do Cadastro de Grades (padr�o), antes de efetuar o processo de Limpeza")
			EndIf
		EndIf
	EndIf

Return


Static Function EMail(_ref)

	Local cUser := "", cPass := "", cSendSrv := ""
	Local cMsg := ""
	Local nSendPort := 0, nSendSec := 0, nTimeout := 0
	Local xRet
	Local oServer, oMessage

	cUser		:= Trim(GetMV("MV_EMCONTA"))		//Trim(GetMV("MV_RELACNT"))		//define the e-mail account username
	cPass		:= Trim(GetMV("MV_EMSENHA"))		//Trim(GetMV("MV_RELPSW"))		//define the e-mail account password
	cSendSrv	:= Trim(GetMV("MV_RELSERV"))		//Trim(GetMV("MV_RELSERV"))		// define the send server
	cFromMv	:= Trim(GetMV("MV_EMCONTA"))		//Trim(GetMV("MV_RELFROM"))
	cToEnv		:= Trim(GetMV("MV_WCADPRD"))
	nTimeout	:= 60 									// define the timout to 60 seconds

//	Cria a conex�o com o server STMP ( Envio de e-mail )
	oServer := TMailManager():New()

	oServer:SetUseSSL( .F. )
	oServer:SetUseTLS( .F. )

	If nSendSec == 0
		nSendPort := 25 //default port for SMTP protocol
	elseif nSendSec == 1
		nSendPort := 465 //default port for SMTP protocol with SSL
		oServer:SetUseSSL( .T. )
	Else
		nSendPort := 587 //default port for SMTPS protocol with TLS
		oServer:SetUseTLS( .T. )
	EndIf

	xRet := oServer:Init( "", cSendSrv, cUser, cPass, , nSendPort )
	If xRet != 0
		cMsg := "Could not initialize SMTP server: " + oServer:GetErrorString( xRet )
		conout( cMsg )
		Return .F.
	EndIf

//	seta um tempo de time out com servidor
	xRet := oServer:SetSMTPTimeout( nTimeout )
	If xRet != 0
		cMsg := "Falha ao setar " + cProtocol + " timeout to " + cValToChar( nTimeout )
		conout( cMsg )
		Return .F.
	EndIf

//	realiza a conex�o SMTP
	xRet := oServer:SMTPConnect()
	If xRet <> 0
		cMsg := "Falha ao conectar: " + oServer:GetErrorString( xRet )
		conout( cMsg )
		Return .F.
	EndIf

//	authenticate on the SMTP server (If needed)
	xRet := oServer:SmtpAuth( cUser, cPass )
	If xRet <> 0
		cMsg := "Could not authenticate on SMTP server: " + oServer:GetErrorString( xRet )
		alert( cMsg )			//conout( cMsg )
		oServer:SMTPDisconnect()
		Return .F.
	EndIf

//	Apos a conex�o, cria o objeto da mensagem
	oMessage := TMailMessage():New()

//	Limpa o objeto
	oMessage:Clear()

//	Popula com os dados de envio
	oMessage:cFrom		:= cFromMv
	oMessage:cTo			:= cToEnv
//	OMessage:cCc			:= "daniel.melo@actualtrend.com.br; contato@marcoscaio.com"
//	oMessage:cBcc			:= "microsiga@microsiga.com.br"
	oMessage:cSubject		:= "Novo Cadastro de Produto"
	oMessage:cBody		:= " <p><b><font face='ARIAL' size='5'>Produto "+_ref+" cadastrado</font></b></p>"

//	Envia o e-mail
	xRet := oMessage:Send( oServer )
	If xRet <> 0
		cMsg := "Erro ao enviar o e-mail: " + oServer:GetErrorString( xRet )
		conout( cMsg )
		Return .F.
	EndIf

//	Desconecta do servidor
	xRet := oServer:SMTPDisconnect()
	If xRet <> 0
		cMsg := "Erro ao disconectar do servidor SMTP: " + oServer:GetErrorString( xRet )
		conout( cMsg )
		Return .F.
	EndIf

Return .T.

User Function AtuSG5(_linha,_status)

	Local _i := 0

	For _i := 1 to Len(OMSRevisao:ACOLS)
		If OMSRevisao:ACOLS[_i,len(OMSRevisao:AHEADER)+1] == .F.
			If _i <> _linha
				OMSRevisao:ACOLS[_i,3] := "2"
			EndIf
		EndIf
	Next
	oMSRevisao:refresh()

Return(_status)

User Function HVLDOPC(_grupo,_linha,_tp)

	Local i := 0
	lret := .t.

	DbSelectArea("SGA")
	DbSetOrder(1)
	DbSeek(xfilial("SGA")+_grupo)

	If Found()
		If _tp = 15
			lret := .f.
		Else
			lret := .f.

			For i := 1 to Len(oMSOPC:ACOLS)
				If oMSOPC:ACOLS[i,3] = _grupo .AND. i <> _linha
					lret := .t.
				EndIf
			Next
		EndIf
	EndIf

Return(lret)

User Function HEXISTP(_prod)
	lret := .t.

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+alltrim(_prod))

	If !Found()
		lret := .F.
		Alert("Produto inexistente!")
	EndIf

Return(lret)

User Function ajustac(_Produto,_Revisao,_indice,_compon,_corte)

	Local cEOL  := CHR(13)+CHR(10)

	If _corte = 'S'
		_qry := "UPDATE "+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ ,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '11"+cnomusr+"'"+cEOL
		_qry += "WHERE D_E_L_E_T_ = '' AND G1_REVINI = '"+_Revisao+"' AND SubString(G1_COD,1,8) = 'B"+SubStr(_Produto,2,7)+"' "+cEOL
		_qry += "		AND SubString(G1_COMP,1,8) = '"+alltrim(SubStr(_compon,1,8))+"' "+cEOL
	Else
		_qry := "UPDATE "+RetSqlName("SG1")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ ,G1_YHORDEL = '"+choradel+"',G1_YDTDELE = '"+ddatadel+"',G1_YUSRDEL = '"+ccodusr+"',G1_YNUSRDE = '12"+cnomusr+"'"+cEOL
		_qry += "WHERE D_E_L_E_T_ = '' AND G1_REVINI = '"+_revisao+"' AND Left(G1_COD,8) = '"+SubStr(_Produto,1,8)+"' "+cEOL
		_qry += "		AND left(G1_COMP,8) = '"+SubStr(_compon,1,8)+"' AND G1_TRT = '"+_indice+"' "+cEOL
	EndIf

	nStatus := TcSqlExec(_qry)

	If (nStatus < 0)
		MsgBox(TCSQLError(), "Erro no comando", "Stop")
	EndIf
Return

Static Function CriacBar(_tp)
// Rotina para gera��o de codigo de barras

	Local nx
	Local _campw := GetMV("MV_HCODBAR")
	Local cCodBarMV := ""
	Local _campwx := ""

	If _tp = 7
		for nx := 3 to len(oMSGrade:aCols[oMSGrade:nat])-1
			If AllTrim(oMSGrade:aCols[oMSGrade:nat,nx])=""

				_tam := rettam2(AllTrim(OMSGrade:AHEADER[nx,1]),cColuna,"C")
				DbSelectArea("SB1")
				DbSetOrder(1)
				DbSeek(xfilial("SB1")+AllTrim(cCodigo)+OMSGrade:ACOLS[oMSGrade:nat,1]+_tam)

				If Found()
					If cCodBarras = 0
						_campwx 	:= VerCodBar(_campw)
						cCodBarras	:= _campwx
					Else
						cCodBarras	:= cCodBarras+1
						_campwx		:= cCodBarras
					EndIf
					oMSGrade:aCols[oMSGrade:nat,nx] := cValToChar(ROUND(_campwx, 0))
				EndIf
			EndIf
		Next nx
	EndIf

	oMsGrade:refresh()
	If !Empty(_campwx)
		cCodBarMV := cValToChar(ROUND(_campwx+1, 0)) //oMSGrade:aCols[oMSGrade:nat,nx]

		If cCodBarMV <> ""
			PutMv ("MV_HCODBAR",cCodBarMV)
			lCodBar := .T.
		EndIf
	EndIf
Return


Static Function MR225ExplG(cProduto,nQuantPai,xrev)
	Local nReg 		  := 0
	Local cTpMp		  := ""

	dbSelectArea('SG1')
	DbSetOrder(1)
	MsSeek(xFilial('SG1')+cProduto)

	While !Eof() .AND. G1_FILIAL+G1_COD == xFilial('SG1')+cProduto
		If SG1->G1_REVINI <> xrev
			DbSkip()
			Loop
		EndIf

		nReg       := Recno()
		cTpMp:= POSICIONE("SB1",1,xFilial("SB1")+G1_COD,"B1_TIPO")
		If cTpMp != "MB"
			nQuantItem := ExplEstr(nQuantPai,,,SG1->G1_REVINI)
		Else
			nQuantItem := nQuantPai
		EndIf
		//��������������������������������������������������?
		//? Verifica se existe sub-estrutura                ?
		//���������������������������������������������������
		dbSelectArea('SG1')
		MsSeek(xFilial('SG1')+G1_COMP)
		//cTpMp:= POSICIONE("SB1",1,xFilial("SB1")+G1_COMP,"B1_TIPO")

		If Found() .AND. cTpMp != "MB"
			MR225ExplG(G1_COD,nQuantItem,G1_REVINI)
		Else
			DbSelectArea("SG1")
			dbGoto(nReg)

			DbSelectArea("DA1")
			DbSetOrder(1)
			DbSeek(xfilial("DA1")+cTabela+SG1->G1_COMP)

			If !Found() .or. DA1->DA1_PRCVEN = 0
				MsgInfo("Produto "+SG1->G1_COMP+" n�o possui pre�o cadastrado na tabela informada!","Diverg�ncia de Pre�o")
			EndIf

			_tem := ASCAN( aEST, {|x| ALLTRIM(x[1]) == SubStr(ALLTRIM(SG1->G1_COMP),1,8)})
			If _tem = 0
				Aadd(aEST, {substr(ALLTRIM(SG1->G1_COMP),1,8),nQuantItem}) //nQuantPai*SG1->G1_QUANT})
			Else
				aEST[_tem,2] += nQuantItem //nQuantPai*SG1->G1_QUANT
			EndIf
			Aadd(aEST2, {SG1->G1_COMP,SG1->G1_COD,nQuantItem}) //nQuantPai*SG1->G1_QUANT})
		EndIf

		DbSelectArea("SG1")
		dbGoto(nReg)

		dbSkip()
	EndDo
Return

Static Function MP225ExplG(cProduto,nQuantPai,xrev)
	Local nReg 		  := 0
	Local cTpMp		  := ""

	dbSelectArea('SG1')
	DbSetOrder(1)
	MsSeek(xFilial('SG1')+cProduto)

	While !Eof() .AND. G1_FILIAL+G1_COD == xFilial('SG1')+cProduto
		If SG1->G1_REVINI <> xrev
			DbSkip()
			Loop
		EndIf


		nReg       := Recno()
		cTpMp:= POSICIONE("SB1",1,xFilial("SB1")+G1_COD,"B1_TIPO")
		//nQuantItem := ExplEstr(nQuantPai,,,"001")

		//��������������������������������������������������?
		//? Verifica se existe sub-estrutura                ?
		//���������������������������������������������������
		dbSelectArea('SG1')
		MsSeek(xFilial('SG1')+G1_COMP)
		//cTpMp:= POSICIONE("SB1",1,xFilial("SB1")+G1_COMP,"B1_TIPO")

		If Found() .AND. cTpMp != "MB"
			MP225ExplG(G1_COD,G1_QUANT,G1_REVINI)
		Else
			DbSelectArea("SG1")
			dbGoto(nReg)
			DbSelectArea("DA1")
			DbSetOrder(1)
			DbSeek(xfilial("DA1")+cTabela+SG1->G1_COMP)

			If !Found() .or. DA1->DA1_PRCVEN = 0
				MsgInfo("Produto "+SG1->G1_COMP+" n�o possui pre�o cadastrado na tabela informada!","Diverg�ncia de Pre�o")
			EndIf
			_tem := ASCAN( aEST3, {|x| x[1] == SubStr(SG1->G1_COMP,1,8)})
			If _tem = 0
				Aadd(aEST3, {substr(SG1->G1_COMP,1,8),POSICIONE("DA1",1,xFilial("DA1")+cTabela+SG1->G1_COMP,"DA1_PRCVEN")}) //nQuantPai*SG1->G1_QUANT})
			Else
				aEST3[_tem,2] += POSICIONE("DA1",1,xFilial("DA1")+cTabela+SG1->G1_COMP,"DA1_PRCVEN")
			EndIf
			Aadd(aEST4, {SG1->G1_COMP,SG1->G1_COD,POSICIONE("DA1",1,xFilial("DA1")+cTabela+SG1->G1_COMP,"DA1_PRCVEN")}) //nQuantPai*SG1->G1_QUANT})
		EndIf

		DbSelectArea("SG1")
		dbGoto(nReg)

		dbSkip()
	EndDo
Return

Static Function rettam3(xcoluna,_tam)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
	_BVTAM := ""

	cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
	cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' "
	cQuery += "ORDER BY R_E_C_N_O_"

	cQuery := ChangeQuery(cQuery)
	If SELECT("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	DbSelectArea("TMPSBV")
	DbGoTop()
	_xtam := 0
	_achou := .F.
	While !EOF() .AND. !_achou
		_xtam++
		If alltrim(TMPSBV->BV_CHAVE) == alltrim(_tam)
			_achou := .T.
		EndIf
		DbSkip()
	EndDo

	DbCloseArea()
	_BVTAM := StrZero(_xtam,3)
Return(_BVTAM)

Static Function rettam4(xcoluna,_tam)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
	_BVTAM := ""

	cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI,BV_XCAMPO FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
	cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' "
	cQuery += "ORDER BY R_E_C_N_O_"

	cQuery := ChangeQuery(cQuery)
	If SELECT("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	DbSelectArea("TMPSBV")
	DbGoTop()
//	_xtam := 0
	_achou := .F.
	While !EOF() .AND. !_achou
//		_xtam++
		If alltrim(TMPSBV->BV_XCAMPO) == alltrim(_tam)
			_achou := .T.
			_BVTAM := Alltrim(TMPSBV->BV_DESCRI)
		EndIf
		DbSkip()
	EndDo

	DbCloseArea()
	//StrZero(_xtam,3)
Return(_BVTAM)

Static Function rettam5(xcoluna,_desctam)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
	Local 	_BVTAM := ""

	cQuery  := "SELECT BV_CHAVE FROM " +RetSQLName("SBV") + " (NOLOCK) WHERE "
	cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND BV_DESCRI = '" +_desctam+ "'  AND D_E_L_E_T_ = '' "
	cQuery += "ORDER BY R_E_C_N_O_"

	cQuery := ChangeQuery(cQuery)
	If SELECT("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	DbSelectArea("TMPSBV")
	DbGoTop()
	If !EOF()
		_BVTAM := Alltrim(TMPSBV->BV_CHAVE)
	EndIf

	DbCloseArea()

Return(_BVTAM)

user function vldfecha(_linha)

	Local _w := 0

	_fecha := .T.
	_i := _linha
	If oMSNewGe1:aCols[_i,len(oMSNewGe1:aHeader)+1] = .F.
		DbSelectArea("SZF")
		DbSetOrder(2)
		DbSeek(xfilial("SZF")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5])

		If found()
			While !EOF() .AND. SubStr(cProduto,1,8) == SubStr(SZF->ZF_PRODUTO,1,8) .AND. alltrim(cRevisao) == alltrim(SZF->ZF_REVISAO) .AND. SubStr(oMSNewGe1:aCols[_i,1],1,8) == SubStr(SZF->ZF_COMP,1,8) .AND. _fecha .AND. alltrim(SZF->ZF_INDICE) == alltrim(oMSNewGe1:aCols[_i,5])
				_temqtd := 0
				For _w := 3 to len(oMsQtd:aheader)-2
					_campo := "SZF->"+oMsQtd:aheader[_w][2] //ZF_TAM"+StrZero(_w,3)

					DbSelectArea("SZG")
					DbSetOrder(2)
					DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)

					If &_campo > 0
						_temqtd++
						DbSelectArea("SZG")
						DbSetOrder(2)
						DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)

						If !Found() .or. alltrim(SZG->ZG_CORNV) = ""
							alert(SubStr(oMSNewGe1:aCols[_i,1],1,15)+"-"+oMSNewGe1:aCols[_i,5]+"-"+SZF->ZF_COR)
							_fecha := .F.
						EndIf

						DbSelectArea("SZK")
						DbSetOrder(2)
						DbSeek(xfilial("SZK")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMsNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5])

						If Found()
							While !EOF() .AND. SubStr(cProduto,1,8) == SubStr(SZK->ZK_PRODUTO,1,8) .AND. alltrim(cRevisao) == alltrim(SZK->ZK_REVISAO) .AND. SubStr(oMSNewGe1:aCols[_i,1],1,8) == SubStr(SZK->ZK_COMP,1,8) .AND. _fecha .AND. alltrim(SZK->ZK_INDICE) == alltrim(oMSNewGe1:aCols[_i,5])
								DbSelectArea("SB4")
								DbSetOrder(1)
								DbSeek(xfilial("SB4")+SubStr(cProduto,1,8))

								cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " (nolock) WHERE "
								cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +SB4->B4_COLUNA +"' AND D_E_L_E_T_ = '' "
								cQuery += "ORDER BY R_E_C_N_O_"

								cQuery := ChangeQuery(cQuery)

								If SELECT("TMPSBV") > 0
									TMPSBV->(DbCloseArea())
								EndIf
								dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

								DbSelectArea("TMPSBV")
								DbGoTop()
								_xlin := 0
								While !EOF()
									_xlin++
									If alltrim(SZK->ZK_TAM) = alltrim(TMPSBV->BV_CHAVE)
										Exit
									EndIf
									DbSkip()
								EndDo
								DbCloseArea()

								If _xlin > 0
									_qry := "SELECT TOP 1 * FROM "+RetSqlName("SZF")+" (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZF_PRODUTO = '"+SubStr(cProduto,1,15)+"' AND ZF_COMP = '"+SubStr(oMsNewGe1:aCols[_i,1],1,15)+"' AND ZF_TAM"+StrZero(_xlin,3)+" > 0"
									dbUseArea( .T., "TOPCONN", TcGenQry(,,_Qry), "XQRB", .T., .F. )
									dbSelectArea("XQRB")
									dbGoTop()
									If !EOF()
										If alltrim(SZK->ZK_TAMNV) == ""

											Alert(SubStr(oMSNewGe1:aCols[_i,1],1,8)+"-"+SZK->ZK_TAM)
											_fecha := .F.
										EndIf
									EndIf
									DbCloseArea()
								Else
									_fecha := .F.
								EndIf
								DbSelectArea("SZK")
								DbSkip()
							EndDo
						Else
							_fecha := .F.
						EndIf
					EndIf
				Next
				// Obrigat�rio a ter pelo menos uma quantidade preenchida caso tenha a cor preenchida!
				If _temqtd = 0
					DbSelectArea("SZG")
					DbSetOrder(2)
					DbSeek(xfilial("SZG")+SubStr(cProduto,1,15)+cRevisao+SubStr(oMSNewGe1:aCols[_i,1],1,15)+oMSNewGe1:aCols[_i,5]+SZF->ZF_COR)

					If Found() .AND. alltrim(SZG->ZG_CORNV) <> ""
						alert(SubStr(oMSNewGe1:aCols[_i,1],1,15)+"-"+oMSNewGe1:aCols[_i,5]+"-"+SZF->ZF_COR)
						_fecha := .F.
					EndIf
				EndIf

				DbSelectArea("SZF")
				DbSkip()
			EndDo
		Else
			_fecha := .F.
		EndIf
	EndIf

	_qry := "SELECT rtrim(ltrim(ZG_COMP))+ZG_CORNV+ZK_TAMNV as SKU FROM "+RetSqlName("SZE")+" SZE WITH(NOLOCK) "
	_qry += "left join "+RetSqlName("SZG")+" SZG WITH(NOLOCK) on SZG.D_E_L_E_T_ = '' AND ZG_PRODUTO = ZE_PRODUTO AND ZG_COMP = ZE_COMP AND ZG_REVISAO = ZE_REVISAO AND ZG_INDICE = ZE_INDICE "
	_qry += "left join "+RetSqlName("SZK")+" SZK WITH(NOLOCK) on SZK.D_E_L_E_T_ = '' AND ZK_PRODUTO = ZE_PRODUTO AND ZK_COMP = ZE_COMP AND ZK_REVISAO = ZE_REVISAO AND ZK_INDICE = ZE_INDICE "
	_qry += "left join "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) on SB1.D_E_L_E_T_ = '' AND B1_COD =  rtrim(ltrim(ZG_COMP))+ZG_CORNV+ZK_TAMNV "
	_qry += "WHERE SZE.D_E_L_E_T_ = '' AND ZE_PRODUTO = '"+SubStr(cProduto,1,15)+"' AND ZE_REVISAO = '"+cRevisao+"' AND ZG_CORNV <> '' AND ZK_TAMNV <> '' AND ZE_COMP = '"+SubStr(oMsNewGe1:aCols[_linha,1],1,8)+"' "
	_qry += "AND ZE_INDICE = '"+oMsNewGe1:aCols[_linha,5]+"' "
	_qry += "AND B1_COD is null "
	_qry += "group by rtrim(ltrim(ZG_COMP))+ZG_CORNV+ZK_TAMNV "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSZ",.T.,.T.)

	DbSelectArea("TMPSZ")
	DbGoTop()
	_msg := ""

	While !EOF()
		_fecha := .F.
		_msg += TMPSZ->SKU + " - "
		DbSelectArea("TMPSZ")
		DbSkip()
	EndDo

	If alltrim(_msg) <> ""
		MsgBox(_msg, "SKUs n�o cadastrados", "Stop")
	EndIf

	DbCloseArea()

Return

/*/{Protheus.doc} User Function nomeFunction
	(long_description)
	@type  Function
	@author user
	@since 12/12/2019
	@version version
	@param param_name, param_type, param_descr
	@return return_var, return_type, return_description
	@example
	(examples)
	@see (links_or_references)
	/*/
User Function ProxIndice(cCodProd)


	Local cQuery := ""

	cQuery := " SELECT MAX(ZE_INDICE) AS ZE_INDICE "
	cQuery += " FROM "+RetSqlName("SZE")+" SZE (NOLOCK) "
	cQuery += " WHERE ZE_FILIAL = '" + xFilial("SZE") + "' "
	cQuery += " AND ZE_PRODUTO = '"+Alltrim(cCodProd)+"' "


	TCQuery cquery new alias T01I

	cProxIndice := T01I->ZE_INDICE

	cProxIndice := Soma1(cProxIndice)

	T01I->(DBCLOSEAREA())



Return cProxIndice

User Function SalvaRef()

	Local _w := 0

	DbSelectArea("SB4")
	DbSetOrder(1)
	DbSeek(xfilial("SB4")+cProduto)
	cDescProd := SB4->B4_DESC
	cColuna := SB4->B4_COLUNA

	DbSelectArea("SZE")
	DbSetOrder(1)
	DbSeek(xfilial("SZE")+substr(cProduto,1,15)+cRevisao)

	//	BEGIN TRANSACTION



	_qry := "delete "+RetSqlName("ZB0")+"  WHERE ZB0_PRODUT = '"+SUBSTR(cProduto,1,8)+"' AND ZB0_REVISA = '"+cRevisao+"'  "
	nStatus := TcSqlExec(_qry)

	If (nStatus < 0)
		MsgBox(TCSQLError(), "Erro no comando", "Stop")
	EndIf
	_qry := "delete "+RetSqlName("ZB1")+" WHERE ZB1_PRODUT = '"+SUBSTR(cProduto,1,8)+"' AND ZB1_REVISA = '"+cRevisao+"' "
	nStatus := TcSqlExec(_qry)

	If (nStatus < 0)
		MsgBox(TCSQLError(), "Erro no comando", "Stop")
	EndIf
	_qry := "delete "+RetSqlName("ZB2")+" WHERE ZB2_PRODUT = '"+SUBSTR(cProduto,1,8)+"' AND ZB2_REVISA = '"+cRevisao+"' "
	nStatus := TcSqlExec(_qry)

	If (nStatus < 0)
		MsgBox(TCSQLError(), "Erro no comando", "Stop")
	EndIf
	_qry := "delete "+RetSqlName("ZB3")+" WHERE ZB3_PRODUT = '"+SUBSTR(cProduto,1,8)+"' AND ZB3_REVISA = '"+cRevisao+"' "
	nStatus := TcSqlExec(_qry)

	If (nStatus < 0)
		MsgBox(TCSQLError(), "Erro no comando", "Stop")
	EndIf

	DbSelectArea("SZE")
	DbSetOrder(1)
	DbGoTop()
	DbSeek(xfilial("SZE")+substr(cProduto,1,8)+Space(7)+cRevisao)
	If Found()
		While !EOF() .AND. SubStr(SZE->ZE_PRODUTO,1,8) = substr(cProduto,1,8) .AND. SZE->ZE_REVISAO = cRevisao
			RecLock("ZB0",.t.)
			Replace ZB0_FILIAL	with SZE->ZE_FILIAL
			Replace ZB0_PRODUT	with SZE->ZE_PRODUTO
			Replace ZB0_DESCPR	with SZE->ZE_DESCPRO
			Replace ZB0_COMP	with SZE->ZE_COMP
			Replace ZB0_DESCCO	with SZE->ZE_DESCCOM
			Replace ZB0_UM		with SZE->ZE_UM
			Replace ZB0_CORTE	with SZE->ZE_CORTE
			Replace ZB0_REVISA	with SZE->ZE_REVISAO
			Replace ZB0_INDICE	with SZE->ZE_INDICE
			MsUnLock()

			DbSelectArea("SZE")
			DbSkip()
		EndDo

		DbSelectArea("SZF")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SZF")+substr(cProduto,1,8)+space(7)+cRevisao)

		While !EOF() .AND. SubStr(SZF->ZF_PRODUTO,1,8) = substr(cProduto,1,8) .AND. SZF->ZF_REVISAO = cRevisao
			RecLock("ZB1",.t.)
			Replace ZB1_FILIAL	with SZF->ZF_FILIAL
			Replace ZB1_PRODUT	with SZF->ZF_PRODUTO
			Replace ZB1_COMP	with SZF->ZF_COMP
			Replace ZB1_REVISA	with SZF->ZF_REVISAO
			Replace ZB1_INDICE	with SZF->ZF_INDICE
			Replace ZB1_COR		with SZF->ZF_COR
			For _w := 1 to 200
				_campo1 := "ZB1_TAM"+StrZero(_w,3)
				_campo2 := "SZF->ZF_TAM"+StrZero(_w,3)
				Replace &(_campo1) with &(_campo2)
			Next
			MsUnLock()

			DbSelectArea("SZF")
			DbSkip()
		EndDo

		DbSelectArea("SZG")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SZG")+substr(cProduto,1,8)+Space(7)+cRevisao)

		While !EOF() .AND. SubStr(SZG->ZG_PRODUTO,1,8) = substr(cProduto,1,8) .AND. SZG->ZG_REVISAO = cRevisao
			RecLock("ZB2",.t.)
			Replace ZB2_FILIAL	with SZG->ZG_FILIAL
			Replace ZB2_PRODUT	with SZG->ZG_PRODUTO
			Replace ZB2_COMP	with SZG->ZG_COMP
			Replace ZB2_REVISA	with SZG->ZG_REVISAO
			Replace ZB2_INDICE	with SZG->ZG_INDICE
			Replace ZB2_COR		with SZG->ZG_COR
			Replace ZB2_CORNV	with SZG->ZG_CORNV
			MsUnLock()

			DbSelectArea("SZG")
			DbSkip()
		EndDo

		DbSelectArea("SZK")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SZK")+substr(cProduto,1,8)+space(7)+cRevisao)

		While !EOF() .AND. substr(SZK->ZK_PRODUTO,1,8) = substr(cProduto,1,8) .AND. SZK->ZK_REVISAO = cRevisao
			RecLock("ZB3",.t.)
			Replace ZB3_FILIAL	with SZK->ZK_FILIAL
			Replace ZB3_PRODUT	with SZK->ZK_PRODUTO
			Replace ZB3_COMP	with SZK->ZK_COMP
			Replace ZB3_REVISA	with SZK->ZK_REVISAO
			Replace ZB3_INDICE	with SZK->ZK_INDICE
			Replace ZB3_TAM		with SZK->ZK_TAM
			Replace ZB3_TAMNV	with SZK->ZK_TAMNV
			MsUnLock()

			DbSelectArea("SZK")
			DbSkip()
		EndDo

	EndIf
Return

STATIC FUNCTION GETBKP()

	Local lOk := .T.

	DbSelectArea("ZB0")
	DbSetOrder(1)
	If !DbSeek(xfilial("ZB0")+substr(cProduto,1,15)+cRevisao)
		lOk := .F.
	EndIf
	DbSelectArea("ZB1")
	DbSetOrder(1)
	If !DbSeek(xfilial("ZB1")+substr(cProduto,1,15)+cRevisao) .AND. lOk
		lOk := .F.
	EndIf
	DbSelectArea("ZB2")
	DbSetOrder(1)
	If !DbSeek(xfilial("ZB2")+substr(cProduto,1,15)+cRevisao) .AND. lOk
		lOk := .F.
	EndIf
	DbSelectArea("ZB3")
	DbSetOrder(1)
	If !DbSeek(xfilial("ZB3")+substr(cProduto,1,15)+cRevisao) .AND. lOk
		lOk := .F.
	EndIf

	If !lOk
		nRet:= MessageBox("Refer�ncia n�o tem bkp cadastrado, DESEJA CONTINUAR MESMO ASSIM ? ","BKP N�O ENCONTRADO",4)
		If nRet== 6
			lOk := .T.
		Else
			MessageBox("Cancelado pelo usu�rio !","Cancelado",16)
		EndIf
	EndIf

Return lOk


Static Function AdesProd(ccodP,cdescP)


	_qry := "UPDATE "+RetSqlName("SA5")+" SET A5_NOMPROD = '"+cdescP+"' "
	_qry += "WHERE D_E_L_E_T_ = '' AND A5_PRODUTO = '"+ccodP+"' "

	nStatus := TcSqlExec(_qry)

	If (nStatus < 0)
		MsgBox(TCSQLError(), "Erro no comando", "Stop")
	EndIf

Return

/*---------------------------------------------------------------------*
 | Func:  VldEstr                                                      |
 | Autor: Robson Melo                                                  |
 | Data:  21/05/2021                                                   |
 | Desc:  Cria��o da valida��o de exist�ncia de estrutura composta     |
 *---------------------------------------------------------------------*/
Static Function VldEstr(cCodEstr,cCodCor,cTamanho,nOpcao)

Local lFindEstru:= .T.
Local cAliasSZE:= GetNextAlias()
Local cAliasSZF:= GetNextAlias()
Local cAliasSZG:= GetNextAlias()
Local cAliasSZK:= GetNextAlias()
Local nX

Default cCodCor := ''
Default cTamanho := ''

//Valida exist�ncia da cor nas estruturas
If nOpcao == 2
	cQryCor:= "SELECT * FROM " +RetSQLName("SZG") + " WHERE ZG_PRODUTO = '"+Alltrim(cCodEstr)+"' AND ZG_COR = '"+cCodCor+"' AND D_E_L_E_T_ = ''"
	cQryCor := ChangeQuery(cQryCor)
	dbUseArea(.T., "TOPCONN", TCGenQry(,,cQryCor), cAliasSZG, .F., .T.)

	If (cAliasSZG)->( Eof() )
		lFindEstru:= .F.
	Else
		While !((cAliasSZG)->( Eof() ))
			If Empty(Alltrim((cAliasSZG)->ZG_CORNV))
				lFindEstru:= .F.
			Endif
			(cAliasSZG)->(dbSkip())
		Enddo
	Endif
Endif

//Valida exist�ncia do tamanho nas estruturas
If nOpcao == 3
	If lFindEstru
		cQryTam:= "SELECT * FROM " +RetSQLName("SZF") + " WHERE ZF_PRODUTO = '"+Alltrim(cCodEstr)+"'  AND D_E_L_E_T_ = ''"
		For nX:= 1 to 100
			cQryTam	+= 'AND ZF_TAM'+StrZero(nX,3) + ' = 0 '
		Next nX

		cQryTam := ChangeQuery(cQryTam)
		dbUseArea(.T., "TOPCONN", TCGenQry(,,cQryTam), cAliasSZF, .F., .T.)

		If !(cAliasSZF)->( Eof() )
			lFindEstru:= .F.
		Endif
	Endif
Endif

Return lFindEstru

/*---------------------------------------------------------------------*
 | Func:  Vldtam                                                        |
 | Autor: Robson Melo                                                   |
 | Data:  21/05/2021                                                    |
 | Desc:  Cria��o da valida��o de exist�ncia de tamanho                 |
 *---------------------------------------------------------------------*/
Static Function Vldtam()
Local aAreaSZE   := SZE->(GetArea())
Local aAreaSZF   := SZF->(GetArea())
Local aAreaSZG   := SZG->(GetArea())
Local aAreaSZK   := SZK->(GetArea())
Local lFindEstru := .T.
Local cMatPrim   := oMSNewGe1:aCols[oMSNewGe1:nAT,1] // MP
Local cIndice    := Alltrim(oMSNewGe1:aCols[oMSNewGe1:nAT,5]) // INDICE
Local cMpTam     := M->ZK_TAMNV                               // TAM MP
Local cMpCor     := Alltrim(oMSCor:ACols[oMSCor:nAT,2])		   // COR MP

If SubStr(cMatPrim,1,4) $ cMvEstru

	DbSelectArea("SZE")
	DbSetOrder(1)
	If !DbSeek(xfilial("SZE")+substr(cMatPrim,1,15)+cRevisao) .AND. lFindEstru
		lFindEstru := .F.
	EndIf

	DbSelectArea("SZF")
	DbSetOrder(1)
	If !DbSeek(xfilial("SZF")+substr(cMatPrim,1,15)+cRevisao) .AND. lFindEstru
		lFindEstru := .F.
	EndIf

	DbSelectArea("SZG")
	DbSetOrder(1)
	If !DbSeek(xfilial("SZG")+substr(cMatPrim,1,15)+cRevisao) .AND. lFindEstru
		lFindEstru := .F.
	EndIf

	DbSelectArea("SZK")
	DbSetOrder(1)
	If !DbSeek(xfilial("SZK")+substr(cMatPrim,1,15)+cRevisao) .AND. lFindEstru
		lFindEstru := .F.
	EndIf

Endif

RestArea(aAreaSZE)
RestArea(aAreaSZF)
RestArea(aAreaSZG)
RestArea(aAreaSZK)

Return lFindEstru
