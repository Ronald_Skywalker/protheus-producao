#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'RWMAKE.CH'
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'

// Retornos poss�veis do MessageBox
#Define IDOK			    1
#Define IDCANCEL		    2
#Define IDYES			    6
#Define IDNO			    7

/*���������������������������������������������������������������������������
?????????????????????????????????????????????????????????????????????????????
??������������������������������������������������������������������������???
???Programa  ?  HPROXFO    ? Autor ?  Geyson Albano  ? Data ? 16/12/2020  ???
??������������������������������������������������������������������������???
???Locacao   ?                  ?Contato ?                                ???
??������������������������������������������������������������������������???
???Descricao ?  Tela de Cadastro   Produto/Cor X Fornecedor               ???
??������������������������������������������������������������������������???
???Parametros?                                                            ???
??������������������������������������������������������������������������???
???Retorno   ?                                                            ???
??������������������������������������������������������������������������???
???Aplicacao ?                                                            ???
??������������������������������������������������������������������������???
???Uso       ?                                                            ???
??������������������������������������������������������������������������???
???Analista Resp.?  Data  ?                                               ???
??������������������������������������������������������������������������???
???              ?  /  /  ?                                               ???
???              ?  /  /  ?                                               ???
??�������������������������������������������������������������������������??
?????????????????????????????????????????????????????????????????????????????
���������������������������������������������������������������������������*/

User Function HPROXFO()

/*�������������������������������������������������������������������������??
?? Declara��o de cVariable dos componentes                                 ??
�??������������������������������������������������������������������������*/
Local nOpc := GD_INSERT+GD_DELETE+GD_UPDATE
Local aAlterFields := {"A5_FORNECE","A5_LOJA","A5_NOMEFOR","A5_CODPRF","A5_YCORFOR","A5_YLE","A5_YQE","A5_YPE","A5_YTIPE","A5_PRODUTO","A5_YPCE","A5_YOBS"}
Private aCoBrw1     := {}
Private aHoBrw1     := {}
Private cCorr       := Space(3)
Private cForne      := Space(6)
Private cPro        := Space(8)
Private noBrw1      := 0
Private cDescr      := "" //Space(50)
Private lcont       := .T.

/*�������������������������������������������������������������������������??
?? Declara��o de Variaveis Private dos Objetos                             ??
�??������������������������������������������������������������������������*/
SetPrvt("oFont1","oFont2","oDlg1","oPanel1","oSay1","oSay2","oSay3","oSay4","oSay5","oGet1","oGet2","oGet3")
SetPrvt("oBtn1","oBtn2","oBtn3","oBtn4","oBtn5","oGet4","oBrw1" )


/*�������������������������������������������������������������������������??
?? Definicao do Dialog e todos os seus componentes.                        ??
�??������������������������������������������������������������������������*/
oFont1     := TFont():New( "Comic Sans MS",0,-16,,.F.,0,,400,.F.,.F.,,,,,, )
oFont2     := TFont():New( "Comic Sans MS",0,-11,,.F.,0,,400,.F.,.F.,,,,,, )
oDlg1      := MSDialog():New( 092,232,667,1417,"TELA DE CADASTRO PRODUTO/COR X FORNECEDOR",,,.F.,,,,,,.T.,,,.T. )
oPanel1    := TPanel():New( 000,000,"",oDlg1,,.F.,.F.,,,588,280,.T.,.F. )
oSay1      := TSay():New( 016,172,{||"TELA DE CADASTRO PRODUTO/COR X FORNECEDOR"},oPanel1,,oFont1,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,220,016)
oSay2      := TSay():New( 044,020,{||"PRODUTO"},oPanel1,,oFont2,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,029,008)
oSay3      := TSay():New( 044,202,{||"DESCRI��O"},oPanel1,,oFont2,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,038,008)
oSay4      := TSay():New( 044,087,{||"COR"},oPanel1,,oFont2,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,032,008)
oSay5      := TSay():New( 041,348,{||"FORNECEDOR EXC."},oPanel1,,oFont2,.F.,.F.,.F.,.T.,CLR_BLACK,CLR_WHITE,044,008)
oGet1      := TGet():New( 051,009,{|u| If(PCount()>0,cPro:=u,cPro)},oPanel1,060,008,'@!',{||ValidMp(cPro)},CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SB4","cPro",,)
oGet2      := TGet():New( 051,138,{|| U_GatDesc(),cDescr},oPanel1,174,008,'@!',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.T.,,.T.,.F.,"","cDescr",,,,,,,,,,,,,,)
oGet3      := TGet():New( 051,082,{|u| If(PCount()>0,cCorr:=u,cCorr)},oPanel1,042,008,'@!',{||VERTAM(cPro,cCorr)},CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"","cCorr",,)

MHoBrw1()
MCoBrw1()
oBrw1      := MsNewGetDados():New(076,008,160,600,nOpc,'AllwaysTrue()','AllwaysTrue()','',aAlterFields,0,99,'AllwaysTrue()','','AllwaysTrue()',oPanel1,aHoBrw1,aCoBrw1 )
oGet4      := TGet():New( 049,342,{|u| If(PCount()>0,cForne:=u,cForne)},oPanel1,060,008,'',,CLR_BLACK,CLR_WHITE,,,,.T.,"",,,.F.,.F.,,.F.,.F.,"SA2","cForne",,)

oBtn1      := TButton():New( 208,520,"Inc/Alterar",oPanel1,{||GravSA5()},036,012,,,,.T.,,"",,,,.F. )
oBtn3      := TButton():New( 208,452,"Excluir",oPanel1,{||ExclSA5()},037,012,,,,.T.,,"",,,,.F. )
oBtn4      := TButton():New( 045,452,"Consulta",oPanel1,{||DadosA5(cPro,cCorr,cForne)},036,012,,,,.T.,,"",,,,.F. )
oBtn5      := TButton():New( 045,500,"Limpa Consulta",oPanel1,{||LimpaDA5()},045,012,,,,.T.,,"",,,,.F. )



oDlg1:Activate(,,,.T.)

Return

/*����������������������������������������������������������������������������
Function  ? MHoBrw1() - Monta aHeader da MsNewGetDados para o Alias: SA5
����������������������������������������������������������������������������*/
Static Function MHoBrw1()

DbSelectArea("SX3")
DbSetOrder(1)
DbSeek("SA5")
	While !Eof() .and. SX3->X3_ARQUIVO == "SA5"
		//If X3Uso(SX3->X3_USADO) .and. cNivel >= SX3->X3_NIVEL .AND. !(AllTrim(SX3->X3_CAMPO)) $("A5_PRODUTO|A5_NOMPROD||A5_REFGRD|A5_DESREF|A5_DSCTAB")
		If X3Uso(SX3->X3_USADO) .and. cNivel >= SX3->X3_NIVEL .AND. (AllTrim(SX3->X3_CAMPO)) $("A5_FORNECE|A5_LOJA|A5_NOMEFOR|A5_CODPRF|A5_YCORFOR|A5_YLE|A5_YQE|A5_YPE|A5_YTIPE|A5_PRODUTO|A5_YPCE|A5_YOBS|A5_CODTAB")
      noBrw1++
      Aadd(aHoBrw1,{Trim(X3Titulo()),;
           SX3->X3_CAMPO,;
           SX3->X3_PICTURE,;
           SX3->X3_TAMANHO,;
           SX3->X3_DECIMAL,;
           "",;
           "",;
           SX3->X3_TIPO,;
           "",;
           "" } )
		EndIf
   DbSkip()
	End

Return


/*����������������������������������������������������������������������������
Function  ? MCoBrw1() - Monta aCols da MsNewGetDados para o Alias: SA5
����������������������������������������������������������������������������*/
Static Function MCoBrw1()

Local nI   := 0

Aadd(aCoBrw1,Array(noBrw1+1))
	For nI := 1 To noBrw1
   aCoBrw1[1][nI] := CriaVar(aHoBrw1[nI][2])
	Next
aCoBrw1[1][noBrw1+1] := .F.

Return

User Function GatDesc()

	If Len(Alltrim(cPro)) == 8 .AND. AllTrim(cPro) != ""
    cDescr := Alltrim(POSICIONE("SB4",1,xFilial("SB4")+AllTrim(cPro),"B4_DESC"))
		
		If cDescr !=" "
	    oDlg1:refresh(oBrw1)
		EndIF
        
	EndIf

Return 

Static Function ValidMp(cPro)

Local bOk := {||.T.}

	If Len(Alltrim(cPro)) == 8 .AND. !Empty(Alltrim(cPro))

	ElseIf !Empty(Alltrim(cPro))
	MessageBox("Favor verificar o c�digo digitado! ","TOTVS",16)
	oGet1:setfocus()
 bOk := {||.F.}
	EndIf

Return bOk

Static Function GravSA5()

Local cQuery    := ""
Local lOk       := .T.
Local lAlt      := .F.
Local lInc      := .F.
Local cProdcor  := Alltrim(cPro+cCorr)
Local nx        := 0
Local ns        := 0
Local nz        := 0
Local nmin      := 0
Local nRet      := 0
Local llead     := .T.
Local ncout 	:= 0	

If Empty(Alltrim(cPro))
    MessageBox("Favor informar o PRODUTO !","TOTVS",16)
    lOk := .F.
ElseIf Empty(Alltrim(cCorr))
    MessageBox("Favor informar a COR !","TOTVS",16)
    lOk := .F.
EndIf
	For ns := 1 to  Len(oBrw1:aCols)
		If oBrw1:aCols[ns][9] != 0 .AND. Alltrim(oBrw1:aCols[ns][10]) == ""
    	  MessageBox("Favor o Tipo de prazo !","TOTVS",16)
        lOk := .F.
		exit
		EndIf
	Next
If lOk
	For ns := 1 to  Len(oBrw1:aCols)
		For nz := 5 to  Len(oBrw1:aCols[ns])-8
			If Empty(oBrw1:aCols[ns][nz]) .AND. nRet == 0
			nRet:= MessageBox("Existem campos em branco, deseja continuar mesmo assim ?","TOTVS",4)
				If nRet == 6
					lOk := .T.
					Exit
				Else
					lOk := .F.
					exit
				EndIf
			EndIf
			If nReT !=0
			Exit
			EndIf
		Next
	Next
EndIf

If lOk
	For ns := 1 to  Len(oBrw1:aCols)
		For nz := 7 to  Len(oBrw1:aCols[ns])-5
			If oBrw1:aCols[ns][nz] == 0 .AND. nRet == 0
			nRet:= MessageBox("Existem campos zerados, deseja continuar mesmo assim ?","TOTVS",4)
				If nRet == 6
					lOk := .T.
					Exit
				Else
					MessageBox("Cancelado pelo Usu�rio !","TOTVS",16)
					lOk := .F.
					exit
				EndIf
			EndIf
			If nReT !=0
			Exit
			EndIf
		Next
	Next
EndIf

	If lcont
		If lOk
			For ns := 1 to  Len(oBrw1:aCols)
				iF oBrw1:aCols[ns][11] == "S"
            nmin += 1
				EndIf
			Next

			If nmin <= 1

           	   cQuery += "SELECT * FROM " +RetSQLName("SB1") + " (NOLOCK) WHERE D_E_L_E_T_ = '' AND LEFT(B1_COD,11) =  '"+cProdcor+"' "

				If Select("VERFB1") > 0
                VERFB1->(DbCloseArea())
				EndIf
        
            	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VERFB1",.T.,.T.)
        
				If VERFB1->(EOF())
                MessageBox("Produto/Cor N�O CADASTRADO !","N�O CADASTRADO",16)
           		 lOk := .F.
				EndIf

				If lOk
        
					While VERFB1->(!EOF())
						For nx := 1 to  Len(oBrw1:aCols)
                
							If oBrw1:aCols[nx][11] == "S" .AND. ncout == 0 // ATUALIZA��O DE MIN E MULTIPLO SB1

								_qry := "UPDATE "+RetSqlName("SB1")+" SET B1_QE = '"+Alltrim(str(oBrw1:aCols[nx][8]))+"',B1_LE = '"+Alltrim(str(oBrw1:aCols[nx][7]))+"', "
								_qry += "B1_PE = '"+Alltrim(str(oBrw1:aCols[nx][9]))+"',B1_TIPE = '"+Alltrim(oBrw1:aCols[nx][10])+"' WHERE LEFT(B1_COD,11) =  '"+cProdcor+"' AND D_E_L_E_T_ = '' "
					
								nStatus := TcSqlExec(_qry)
								If (nStatus < 0)
									MsgBox(TCSQLError(), "Erro no comando", "Stop")
								EndIf

								_qry := "UPDATE "+RetSqlName("SA5")+" SET A5_YPCE = 'N' WHERE LEFT(A5_PRODUTO,11) =  '"+cProdcor+"' AND D_E_L_E_T_ = '' "
					
								nStatus := TcSqlExec(_qry)
								If (nStatus < 0)
									MsgBox(TCSQLError(), "Erro no comando", "Stop")
								EndIf
								ncout := 1	

							EndIf
							
							If Alltrim(oBrw1:aCols[nx][1]) != ""
                         
                                DbSelectArea("SA5")
                                DbSetOrder(1)
								If !DbSeek(xFilial("SA5")+Alltrim(oBrw1:aCols[nx][1])+Alltrim(oBrw1:aCols[nx][2])+VERFB1->B1_COD)

                                Reclock("SA5",.T.)
                                
                                  	SA5->A5_FILIAL      := xFilial("SA5")
                                    SA5->A5_FORNECE     := Alltrim(oBrw1:aCols[nx][1])
                                    SA5->A5_LOJA        := Alltrim(oBrw1:aCols[nx][2])
                                    SA5->A5_NOMEFOR     := Alltrim(oBrw1:aCols[nx][3])
                                    SA5->A5_PRODUTO     := VERFB1->B1_COD
                                    SA5->A5_NOMPROD     := Alltrim(VERFB1->B1_DESC)
                                    SA5->A5_CODPRF      := Alltrim(oBrw1:aCols[nx][6])
                                    SA5->A5_YCORFOR     := Alltrim(oBrw1:aCols[nx][5])
                                    SA5->A5_YQE         := oBrw1:aCols[nx][8]
                                    SA5->A5_YLE         := oBrw1:aCols[nx][7]
                                    SA5->A5_YPE         := oBrw1:aCols[nx][9]
                                    SA5->A5_YTIPE       := Alltrim(oBrw1:aCols[nx][10])
									SA5->A5_YOBS        := Alltrim(oBrw1:aCols[nx][12])
									If oBrw1:aCols[nx][11] == "S"
                                    SA5->A5_YPCE       := "S" 
									Else
                                    SA5->A5_YPCE       := "N" 
									EndIf

                                MsUnLock()
                                lInc := .T.
								Else
									If !lAlt .AND. nRet == 0
                        nRet:= MessageBox("Produto/Cor J� EXISTENTE deseja realmente atualizar o cadastro ?","j� existente",4)
									EndIf
									If nRet == 6 .OR. lAlt
                            lAlt := .T.
                                DbSelectArea("SA5")
                                DbSetOrder(1)
                                DbSeek(xFilial("SA5")+Alltrim(oBrw1:aCols[nx][1])+Alltrim(oBrw1:aCols[nx][2])+VERFB1->B1_COD)
                                Reclock("SA5",.F.)
                                
                                    SA5->A5_FILIAL      := xFilial("SA5")
                                    SA5->A5_FORNECE     := Alltrim(oBrw1:aCols[nx][1])
                                    SA5->A5_LOJA        := Alltrim(oBrw1:aCols[nx][2])
                                    SA5->A5_NOMEFOR     := Alltrim(oBrw1:aCols[nx][3])
                                    SA5->A5_PRODUTO     := VERFB1->B1_COD
                                    SA5->A5_NOMPROD     := Alltrim(VERFB1->B1_DESC)
                                    SA5->A5_CODPRF      := Alltrim(oBrw1:aCols[nx][6])
                                    SA5->A5_YCORFOR     := Alltrim(oBrw1:aCols[nx][5])
                                    SA5->A5_YQE         := oBrw1:aCols[nx][8]
                                    SA5->A5_YLE         := oBrw1:aCols[nx][7]
                                    SA5->A5_YPE         := oBrw1:aCols[nx][9]
                                    SA5->A5_YTIPE       := Alltrim(oBrw1:aCols[nx][10])
									SA5->A5_YOBS        := Alltrim(oBrw1:aCols[nx][12])
										If oBrw1:aCols[nx][11] == "S"
                                    SA5->A5_YPCE       := "S" 
										Else
                                    SA5->A5_YPCE       := "N" 
										EndIf

                                MsUnLock()
									EndIf
								EndIf
							EndIf
						Next
           	 VERFB1->(DbSkip()) 
					Enddo
				 ncout := 0
					If !lAlt .AND. lInc
                MessageBox("Produto/Cor INCLU�DO COM SUCESSO !","TOTVS",0)
					ElseIf !lInc .AND. lAlt
                MessageBox("Produto/Cor ALTERADO COM SUCESSO !","TOTVS",0)
					ElseIf nRet == 7
                MessageBox("Cancelado pelo Usu�rio !","TOTVS",16)
					EndIf
				EndIf
			Else
            MessageBox("S� � permitido a inclus�o de 1 Min/Multiplo por SKU !","TOTVS",16)
            llead := .F.
			EndIf
		EndIf
		//If llead // VARIAVEL QUE IMPEDE A LIMPEZA CASO TENHA MAIS DE UM LEAD TIME
        If lAlt .OR. lInc
		// LIMPEZA DE VARI�VEIS    
		cCorr        := Space(3)
        cForne       := Space(6)
        cPro         := Space(8) 
        cDescr       := "" 
        //noBrw1       := 0 
        aCoBrw1      := {}
        MCoBrw1()
        oBrw1:aCols  := aCoBrw1
        oDlg1:refresh() 
        oGet1:setfocus()
			
		EndIf
	Else
 MessageBox("Fun��o CONSULTA utilizada, favor limpar o Grid !","TOTVS",16)  
	EndIf

Return

Static Function ExclSA5()

Local cQuery    := ""
Local lOk       := .T.
Local cProdcor  := Alltrim(cPro+cCorr)
Local _exc      := ""
Local lExcl		:= .F.
	
	If Empty(Alltrim(cPro))
        MessageBox("Favor informar o PRODUTO !","TOTVS",16)
        lOk := .F.
	ElseIf Empty(Alltrim(cCorr))
        MessageBox("Favor informar a COR !","TOTVS",16)
        lOk := .F.
	ElseIf Empty(Alltrim(cForne))
        MessageBox("Favor informar o FORNECEDOR !","TOTVS",16)
        lOk := .F.
	EndIf
	If lcont

		If lOk
      
       cQuery += "SELECT A5_FORNECE,A5_LOJA,A5_PRODUTO,* FROM "+RetSqlName("SA5")+" (NOLOCK) WHERE LEFT(A5_PRODUTO,11)  = '"+cProdcor+"' AND A5_FORNECE = '"+Alltrim(cForne)+"' AND D_E_L_E_T_ = '' "

			If Select("VERFA5") > 0
            VERFA5->(DbCloseArea())
			EndIf
        
    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VERFA5",.T.,.T.)
        
			If VERFA5->(!Eof())

            _exc += " UPDATE "+RetSqlName("SA5")+" SET D_E_L_E_T_ = '*',R_E_C_D_E_L_ = R_E_C_N_O_ WHERE LEFT(A5_PRODUTO,11) = '"+cProdcor+"' " 
            _exc += " AND A5_FORNECE = '"+Alltrim(cForne)+"' AND D_E_L_E_T_ = '' "

            nStatus := TcSqlExec(_exc)
				If (nStatus < 0)
                MsgBox(TCSQLError(), "Erro no comando", "Stop")
				EndIf
            MessageBox("Produto/Cor exclu�do com SUCESSO !","TOTVS",0)
			lExcl		:= .T.
			Else
         MessageBox("Produto/Cor n�o localizado, favor revisar os dados !","TOTVS",16)
		 lExcl		:= .F.
			EndIf
		EndIf
		If lExcl
// LIMPEZA DE VARI�VEIS    
cCorr        := Space(3)
cForne       := Space(6)
cPro         := Space(8) 
cDescr       := "" 
//noBrw1       := 0 
aCoBrw1      := {}
MCoBrw1()
oBrw1:aCols  := aCoBrw1
oDlg1:refresh() 
oGet1:setfocus()
		EndIf
	Else
 MessageBox("Fun��o CONSULTA utilizada, favor limpar o Grid !","TOTVS",16)  
	EndIf
Return

Static Function DadosA5(cPro,cCorr,cForne)

Local cQuery := ""
Local aforne := {}

	If !Empty(Alltrim(cPro))

lcont := .F.

cQuery += " SELECT A5_FORNECE,A5_LOJA,A5_NOMEFOR,A5_CODPRF,                  "
cQuery += " A5_YCORFOR,A5_YLE,A5_YQE,A5_YPE,A5_YTIPE,A5_YPCE,A5_PRODUTO,     "
cQuery += " A5_YOBS,A5_CODTAB FROM "+RetSqlName("SA5")+" (NOLOCK) WHERE      "
cQuery += " D_E_L_E_T_ = '' AND LEFT(A5_PRODUTO,8) = '"+Alltrim(cPro)+"' 	 "

		If !Empty(Alltrim(cCorr))
  cQuery += " AND SUBSTRING(A5_PRODUTO,9,3) =  '"+Alltrim(cCorr)+"'			 "
		EndIf

		If !Empty(Alltrim(cForne))
  cQuery += " AND A5_FORNECE =  '"+Alltrim(cForne)+"'			             "
		EndIf

		If Select("CONSA5") > 0
        CONSA5->(DbCloseArea())
		EndIf
        
    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"CONSA5",.T.,.T.)

		While CONSA5->(!Eof())
       aAdd(aforne,{CONSA5->A5_FORNECE,CONSA5->A5_LOJA,CONSA5->A5_NOMEFOR,CONSA5->A5_PRODUTO,CONSA5->A5_YCORFOR,CONSA5->A5_CODPRF,CONSA5->A5_YLE,CONSA5->A5_YQE,CONSA5->A5_YPE,CONSA5->A5_YTIPE,CONSA5->A5_YPCE,CONSA5->A5_YOBS,CONSA5->A5_CODTAB,"",.F.})

      CONSA5->(DbSkip()) 
		EndDo
    

oBrw1:aCols  :={} 
oBrw1:aCols  := aforne
oDlg1:refresh() 
	Else
	MessageBox(" Favor informar o produto !","SEM-PROD",16)
	oGet1:setfocus()
	EndIf
Return

Static Function LimpaDA5 ()

lcont := .T.
aCoBrw1 := {}
MCoBrw1()
oBrw1:aCols  := aCoBrw1
oDlg1:refresh() 

Return

Static Function VERTAM(cPro,cCorr)

	Local cColuna := POSICIONE("SB4",1,xFilial("SB4")+cPro,"B4_COLUNA")
	Local cEOL	     := +Chr(13)+Chr(10)
	Local cQuery1
	Local cQuery2
    Local bOk := {||.T.}


	If AllTrim(cCorr) != ""

//	Define field properties
		If alltrim(cColuna) <> ""
		cQuery1 := " SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI "+cEOL
		cQuery1 += " FROM "+RetSQLName("SBV")+" (NOLOCK) "+cEOL
		cQuery1 += " WHERE D_E_L_E_T_ = '' "+cEOL
		cQuery1 += " AND BV_FILIAL = '" +xFilial("SBV") +"' "+cEOL
		cQuery1 += " AND BV_TABELA = '"+SB4->B4_COLUNA+"' "+cEOL
		cQuery1 += " ORDER BY R_E_C_N_O_ "+cEOL

			If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
			EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSBV",.T.,.T.)

		cQuery2  := " SELECT * FROM "+RetSQLName("SZD")+" (NOLOCK) "+cEOL
		cQuery2  += " WHERE D_E_L_E_T_ = ' ' "+cEOL
		cQuery2  += " AND ZD_FILIAL = '"+xFilial("SZD")+"' "+cEOL
		cQuery2  += " AND ZD_PRODUTO='"+AllTrim(cPro)+"' AND ZD_COR = '"+Alltrim(cCorr)+"'"+cEOL

			If Select("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
			EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)
		DbSelectArea("TMPSZD")
		
			If TMPSZD->(EOF())
				bOk := {||.F.}
				MessageBox(" O Produto "+AllTrim(cPro)+" n�o possui cadastro na cor " +Alltrim(cCorr)+ " !","SEM-COR",16)
				oGet3:setfocus()
			EndIf
		EndIf

	EndIf


Return bOk
