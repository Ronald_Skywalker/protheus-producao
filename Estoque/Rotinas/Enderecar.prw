#include 'protheus.ch'
#include 'parmtype.ch'
#Include "RwMake.CH"
#include "tbiconn.ch"
#INCLUDE "TOPCONN.CH"

/*
Weskley Silva
04.07.2017
*/

user function Enderecar()

Local dDataL    := DATE()
Local aCabSDA    := {}
Local aItSDB         := {}
Local _aItensSDB := {} 
Local nQuant 

Private	lMsErroAuto := .F.

dbSelectArea("SDA")
dbGotop()

cQuery := " SELECT * FROM SDA010 WHERE DA_LOCAL='A1' AND DA_SALDO > 0 AND D_E_L_E_T_='' and DA_FILIAL='0101' "

TCQUERY cQuery NEW ALIAS ENDE

ProcRegua(RecCount())

WHILE ENDE->(!EOF())

//Cabe�alho com a informa��o do item e NumSeq que sera endere�ado.

nQuant := ENDE->DA_SALDO

Begin Transaction
IncProc()
aCabSDA := {{"DA_PRODUTO" ,ENDE->DA_PRODUTO     ,Nil},;
            {"DA_NUMSEQ"  ,ENDE->DA_NUMSEQ      ,Nil}} 

//Dados do item que ser� endere�ado
aItSDB := {{"DB_ITEM"	  ,"0001"	    ,Nil},;
           {"DB_ESTORNO"  ," "	        ,Nil},;
           {"DB_LOCALIZ"  ,"PADRAO"     ,Nil},;
           {"DB_DATA"	  ,dDataL       ,Nil},;
           {"DB_QUANT"    ,nQuant       ,Nil}}       

aadd(_aItensSDB,aitSDB)

//Executa o endere�amento do item 
MATA265( aCabSDA, _aItensSDB, 3)

If lMsErroAuto    
    MostraErro()
Else   
    MsgAlert("Processamento Ok!","HOPE")
Endif

END Transaction
_aItensSDB := {}
aitSDB := {}
ENDE->(DBSKIP())

ENDDO
ENDE->(DBCLOSEAREA())
return