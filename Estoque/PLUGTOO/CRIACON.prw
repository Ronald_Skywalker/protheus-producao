#INCLUDE "TOTVS.CH"
#INCLUDE "TBICONN.CH"


/*/{Protheus.doc} CriaCond

	Atualizacao Condicao de Pagamento

	@author 	Milton J. dos Santos
	@since		02/02/2020
	@version	1.0
	@type function
/*/

User Function CriaCond()
Local cCond := ""
Local lRet 	:= .T.

PRIVATE lJob	:= GetRemoteType() == -1 // Verifica se e job

RpcSetEnv("99","01","Admin")

DbSelectArea("SE4")
SE4->( DbGoTop() )
Do While .NOT. SE4->( EOF() )
	DbSelectArea("SE4")
	Reclock("SE4",.F.)
	DbDelete()
	MsUnLock()
	SE4->( DBSKIP() )
Enddo
cCond	:= SuperGetMv("HP_PLUGCND",.F.,"V01") 	// V01-BOLETO (LOJA VIRTUAL)
DbSelectArea("ZBA")
ZBA->( DbGoTop() )
Do While .NOT. ZBA->( EOF() )
	If Empty( ZBA->ZBA_CNDSC5 )
		If U_AchaCond()
			DbSelectArea("ZBA") 
			Reclock("ZBA",.F.)
			ZBA->ZBA_CNDSC5 := SE4->E4_CODIGO		// C5_CONDPAG
			ZBA->( MsUnLock() )
		Endif
	Endif
	DbSelectArea("ZBA")
	ZBA->( DBSKIP() )
Enddo

RESET ENVIRONMENT

Return

User Function AchaCond
Local lRet := .T.
//Local cCond:= SuperGetMv("HP_PLUGCND",.F.,"V01")	// V01-BOLETO (LOJA VIRTUAL)
Local cCond:= SuperGetMv("HP_PLUGCND",.F.,"C61")	// Solicitacao do FERRARI em 19/03/2021
//	CASE PARA O CANAL ZZMALL
U_LogProc( "- Verificando condicao de pagamento" )
Do Case
//	Metodo: Credito - Cartao de Credito 
	Case Upper( Alltrim( ZBA->ZBA_PAYTY) ) == 'CREDIT'	.and. Upper( Alltrim( ZBA->ZBA_PAYM) ) == 'CREDIT_CARD'	
//		lRet := U_GeraCARD()		// Solicitacao do FERRARI em 19/03/2021
		DbSelectArea("SE4")
		DbSetOrder(1)
		If SE4->( DbSeek( xFilial("SE4")  + cCond ) )
			lRet := .T.
		Endif
//	Metodo: Debito
	Case Upper( Alltrim( ZBA->ZBA_PAYTY) ) == 'DEBIT'
		DbSelectArea("SE4")
		DbSetOrder(1)
		If SE4->( DbSeek( xFilial("SE4")  + cCond ) )
			lRet := .T.
		Endif
//	Metodo: Boleto
	Case Upper( Alltrim( ZBA->ZBA_PAYTY) ) == 'TICKET'
		DbSelectArea("SE4")
		DbSetOrder(1)
		If SE4->( DbSeek( xFilial("SE4")  + cCond ) )
			lRet := .T.
		Endif
//	Metodo: Conta
	Case Upper( Alltrim( ZBA->ZBA_PAYTY) ) == 'VOUCHER'	
		DbSelectArea("SE4")
		DbSetOrder(1)
		If SE4->( DbSeek( xFilial("SE4")  + cCond ) )
			lRet := .T.
		Endif
//	Metodo: Transferencia
	Case Upper( Alltrim( ZBA->ZBA_PAYTY) ) == 'TRANSFER'	
		DbSelectArea("SE4")
		DbSetOrder(1)
		If SE4->( DbSeek( xFilial("SE4")  + cCond ) )
			lRet := .T.
		Endif
	Otherwise
		DbSelectArea("SE4")
		DbSetOrder(2)
		If ! SE4->( DbSeek( xFilial("SE4")  + "A VISTA" ) )
			lRet := U_GeraCOND()
		Endif
EndCase
Return lRet

User Function GeraCard
Local lRet		:= .T.
Local nParc		:= 0
Local cParc		:= Alltrim( Str( ZBA->ZBA_PAYIN  ))
Local cParcela	:= StrZero( ZBA->ZBA_PAYIN, 2 )
Local cPrazo	:= ""
Local aCabecAux := {}
Local aItemAux  := {}
Local lRet		:= .T.
Local cCond		:= ""
Local cDesc		:= ""
Local cPolCom	:= SuperGetMv("HP_PLUGPCO",.F.,"018")	// 018-POLITICA LOJA VIRTUAL
Local cMsg		:= ""

Private lMsErroAuto	:= .F.

cNum	:= "Z" + cParcela 

DbSelectArea("SE4")
DbSetOrder(1)
If SE4->( DbSeek( xFilial("SE4")  + cNum ) )
	If SE4->E4_XORIGEM <> "P"
		lRet := .F.
		cMsg := "Nao encontrou uma condicao PLUGG.TO. Verifique !!!"
		MsgStop( cMsg ,"Bloqueio")
	Endif
Else
	cPrazo := ""
	For nParc := 1 to ZBA->ZBA_PAYIN
		cPrazo	:= Alltrim( Str( nParc * 30 ) )
		cCond	+= cPrazo
		If nParc < ZBA->ZBA_PAYIN
			cCond	+= ','
		Endif
	Next
	cDesc	:= "CARTAO DE CREDITO - PLUGG.TO " + cParc + "x"
	aAdd(aCabecAux, {'E4_FILIAL'	, xFilial("SE4")		, Nil} )
	aAdd(aCabecAux, {'E4_CODIGO'	, cNum					, Nil} )
	aAdd(aCabecAux, {'E4_TIPO'  	, "1"  					, Nil} )
	aAdd(aCabecAux, {'E4_COND'  	, cCond					, Nil} )
	aAdd(aCabecAux, {'E4_DESCRI'	, cDesc					, Nil} )
	aAdd(aCabecAux, {'E4_IPI'		, "N"					, Nil} )
	aAdd(aCabecAux, {'E4_DDD'		, "L"					, Nil} )
	aAdd(aCabecAux, {'E4_MSBLQL'	, "2"					, Nil} )
	aAdd(aCabecAux, {'E4_SOLID'		, "N"					, Nil} )
	aAdd(aCabecAux, {'E4_ACRES'		, "N"					, Nil} )
	aAdd(aCabecAux, {'E4_CTRADT'	, "2"					, Nil} )
	aAdd(aCabecAux, {'E4_AGRACRS'	, "2"					, Nil} )
	aAdd(aCabecAux, {'E4_CCORREN'	, "2"					, Nil} )
	aAdd(aCabecAux, {'E4_XECOM'		, "1"					, Nil} )
	aAdd(aCabecAux, {'E4_XORIGEM'	, "P"					, Nil} )

	lRet := GrvCond( aCabecAux, aItemAux )
Endif
// Verifica as regras das politicas comerciais x Condicao de pagamento
If lRet
	DbSelectArea("SZ3")
	DbSetOrder(1)
	If ! DbSeek( xFilial("SZ3") + cPolCom + cNum )
		RecLock("SZ3",.T.)
		Z3_FILIAL	:= xFilial("SZ3")
		Z3_POLITIC	:= cPolCom
		Z3_COND		:= cNum
		Z3_DESC		:= cDesc
		SZ3->( MsUnLock() )
	Endif
	DbSelectArea("SZ6")
	DbSetOrder(1)
	If ! DbSeek( xFilial("SZ6") + cPolCom + cNum )
		RecLock("SZ6",.T.)
		Z6_FILIAL	:= xFilial("SZ6")
		Z6_POLITIC	:= cPolCom
		Z6_PRAZO	:= cNum
		Z6_DESC		:= cDesc
		SZ6->( MsUnLock() )
	Endif
Endif

U_LogProc( cMsg )

Return lRet

User Function GeraCOND
Local aCabecAux := {}
Local aItemAux  := {}
Local aItens	:= {}
Local nConta  	:= 0
Local cEdit5  	:= 0
Local cEdit6  	:= 0
Local cEdit7  	:= 0
Local lRet		:= .T.

Private lMsErroAuto	:= .F.

cNum	:= MT360Num()

aAdd(aCabecAux, {'E4_FILIAL'	, xFilial("SE4")		, Nil} )
aAdd(aCabecAux, {'E4_CODIGO'	, cNum					, Nil} )
aAdd(aCabecAux, {'E4_TIPO'  	, "A"  					, Nil} )
aAdd(aCabecAux, {'E4_COND'  	, "0"  					, Nil} )
aAdd(aCabecAux, {'E4_DESCRI'	, "A VISTA"				, Nil} )
aAdd(aCabecAux, {'E4_IPI'		, "N"					, Nil} )
aAdd(aCabecAux, {'E4_DDD'		, "D"					, Nil} )
aAdd(aCabecAux, {'E4_MSBLQL'	, "2"					, Nil} )
aAdd(aCabecAux, {'E4_SOLID'		, "N"					, Nil} )
aAdd(aCabecAux, {'E4_ACRES'		, "N"					, Nil} )
aAdd(aCabecAux, {'E4_CTRADT'	, "2"					, Nil} )
aAdd(aCabecAux, {'E4_AGRACRS'	, "2"					, Nil} )
aAdd(aCabecAux, {'E4_CCORREN'	, "2"					, Nil} )

/*Checa se Gera a alinha de Saldo*/
If cEdit7 > 0
	nContagem := 3
Else
	nContagem := 2
EndIf
nContagem := 0
_nData := "0"

//Populando Item auxiliar
Do While nConta <= nContagem
	aAdd(aItemAux, {'EC_FILIAL'	,xFilial("SEC")		, Nil} )
	aAdd(aItemAux, {'EC_ITEM'	,StrZero(nConta,2)	, Nil} )
	aAdd(aItemAux, {'EC_TIPO'	,'1'				, Nil} )
	aAdd(aItemAux, {'EC_COND'	,cValToChar(nConta)	, Nil} )
	aAdd(aItemAux, {'EC_IPI' 	,'N'				, Nil} )
	aAdd(aItemAux, {'EC_DDD' 	,'L'				, Nil} )
	aAdd(aItemAux, {'EC_SOLID'	,'N'				, Nil} )
	Do Case
    	Case nConta == 1
	    	aAdd(aItemAux, {'EC_RATEIO', cEdit5, Nil} )
		Case nConta == 2
			aAdd(aItemAux, {'EC_RATEIO', cEdit6, Nil} )
		Case nConta == 3
			aAdd(aItemAux, {'EC_RATEIO', cEdit7, Nil} )
	EndCase
	nConta++
	aAdd(aItens, aItemAux)
	aItemAux := {}
Enddo

//  Chamando rotina autom�tica de inclus�o
// MSExecAuto({|x,y,z|mata360(x,y,z)},aCabecAux,aItens, 3)	// 3=INCLUIR, 4=ALTERAR

GrvCond( aCabecAux,aItens )

//  Verificando status da rotina executada
If lMsErroAuto
	lRet := .F.
	If IsBlind()	// Se executado sem interface grafica (JOB ou WebService) 	
		ConOut("Erro na inclusao!")
	Else
//	Alert("Erro na inclusao!")
 		MostraErro()	// n�o usar via JOB
	Endif
Else
	If IsBlind()	// Se executado sem interface grafica (JOB ou WebService)
		ConOut("Incluido com sucesso! ")
	Else
		Alert("Condicao n� " + SE4->E4_CODIGO + " incluida com sucesso! ")
	Endif
EndIf
ConOut("Fim")

Return( lRet )

Static Function MT360Num()
Local cProxNum := ""

	cProxNum := GETSX8NUM("SE4","E4_CODIGO")
	DbSelectArea("SE4")
	While .T.
		If SE4->( DbSeek( xFilial("SE4")  + cProxNum ) )
			ConfirmSX8()
			cProxNum := GetSXeNum("SE4","E4_CODIGO")
		Else
			Exit
		Endif
	Enddo

Return(cProxNum)

Static Function GrvCond( _aCabec, _aItens  )
Local cAlias1 := "SE4"
Local cAlias2 := "SEC"
Local cNumPed := _aCabec[2,2]
Local cCampo  := ""
Local cContem := ""
Local nCabec  := 0
Local nCampo  := 0
Local nLinha  := 0

DbSelectArea( cAlias1 )
(cAlias1)->( DbSetOrder(1) )
RECLOCK(cAlias1,.T.)
For nCabec := 1 TO Len( _aCabec )
	cCampo  := _aCabec[nCabec,1]
	cContem := _aCabec[nCabec,2]
	(cAlias1)->(FieldPut(FieldPos(cCampo) , cContem ))
NEXT
(cAlias1)->( MsUnLock() )
DbSelectArea( cAlias2 )
(cAlias2)->( DbSetOrder(1) )
For nLinha := 1 to Len( _aItens )
	If _aItens[nLinha,2,2] = cNumPed
		RECLOCK(cAlias2,.T.)
		For nCampo 	:= 1 TO Len( _aItens[ nLinha ] )
			cCampo  := _aItens[nLinha][nCampo][1]
			cContem := _aItens[nLinha][nCampo][2]
			(cAlias2)->(FieldPut(FieldPos(cCampo) ,cContem )) 
		NEXT
		(cAlias2)->( MsUnLock() )
	EndIf
Next

Return
