#INCLUDE "Totvs.ch"
#INCLUDE "Protheus.ch"
#INCLUDE "Topconn.ch"
#INCLUDE "Rwmake.ch"

/*/{Protheus.doc} PLANZB9
    Planilha em EXCEL para PRODUTOS PLUGG.TO

    @author	   Milton J.dos Santos
    @since	   25/05/2021
    @version   P12
    @obs	   

 /*/

User Function PLANZB9()

    Local oDlg
    Local nTipo   := 1
    Local cVinc   := "3"

    Private oFwMs   := Nil
    Private aPrbx   := {}
    Private aPrmt   := {}
    Private aColun  := {} //-- Colunas
    Private aItens  := {} //-- Linhas

    Private cFiliDe := AvKey("","D2_FILIAL")
    Private cFiliAt := AvKey("","D2_FILIAL")
    Private cNotaDe := AvKey("","F2_DOC")
    Private cNotaAt := AvKey("","F2_DOC")
    Private cProdDe := AvKey("","D2_COD")
    Private cProdAt := AvKey("","D2_COD")
    Private dDataDe := FirstDate(Date())
    Private dDataAt := LastDate(Date())
    Private cClieDe := AvKey("","A1_COD")
    Private cClieAt := AvKey("","A1_COD")
    
    aSituacao := {"1=Habilitado","2=Desabilitado","3=Ambos"}        
 //   AADD( aPrbx , { 1 , "Produto de"            , cProdDe , , , "SB1" , , 060 , .F. } )
 //   AADD( aPrbx , { 1 , "Produto at�"           , cProdAt , , , "SB1" , , 060 , .F. } )
 //   AADD( aPrbx , { 2 , "Situacao do Produto"   , 3       , aSituacao, 60, , ".T.", 060 , .F. } )
 //   aAdd(aPrbx, {2, "Tipo Importa��o",               nTipo, {"1=Pr� Nota",              "2=Classifica��o em Documento de Entrada"},     122, ".T.", .F.})
    aAdd(aPrbx, {2, "Situacao do Produto ",            cVinc, aSituacao,                                       090, ".T.", .F.})

    If ParamBox(aPrbx,"",aPrmt)        
        FwMsgRun(,{|oSay| FMntCln(oSay), FMntRlt(oSay), FOpnRlt(oSay)},"Dados do Produtos","Processando produtos...",)
    Endif

Return(Nil)
//-------------------------------------------------------------------
/*/{Protheus.doc} FMntCln
Fun��o respons�vel por definir as colunas do relat�rio.

@author	   Milton J.dos Santos
@since	   25/05/2021
@version   P12
@obs	   

Alteracoes Realizadas desde a Estruturacao Inicial
Data       Programador     Motivo
/*/
//------------------------------------------------------------------- 
Static Function FMntCln(oSay)

    AADD( aColun , "SKU" )
    AADD( aColun , "SKU Pai" )
    AADD( aColun , "Descricao" )
    AADD( aColun , "Marca" )
    AADD( aColun , "Modelo" )
    AADD( aColun , "Categoria" )
    AADD( aColun , "Preco" )
    AADD( aColun , "Preco Especial" )
    AADD( aColun , "Estoque" )
    AADD( aColun , "Peso Bruto")
    AADD( aColun , "Altura")
    AADD( aColun , "Largura")
    AADD( aColun , "Comprimento")
    AADD( aColun , "Imagem 1")
    AADD( aColun , "Imagem 2")
    AADD( aColun , "Imagem 3")
    AADD( aColun , "Imagem 4")
    AADD( aColun , "NCM")
    AADD( aColun , "EAN")
    AADD( aColun , "Data Inclusao" )
    AADD( aColun , "Integrar?" )   

Return(aColun)
//-------------------------------------------------------------------
/*/{Protheus.doc} FMntRlt
Fun��o respons�vel por montar e definir as caracter�sticas do
relat�rio.

@author	   Milton J.dos Santos
@since	   25/05/2021
@version   P12
@obs	   

Alteracoes Realizadas desde a Estruturacao Inicial
Data       Programador     Motivo
/*/
//------------------------------------------------------------------- 
Static Function FMntRlt(oSay)

    Local nX := 0
    Local cNomeP := "Dados"
    Local cTabeP := "Dados do Produtos"

    //-- Instanciando Objeto
    oFwMs := FWMSExcel():New()

    //-- Adicionando Planilha
    oFwMs:AddworkSheet(cNomeP)
    
    //-- Criando Tabela
    oFwMs:AddTable(cNomeP,cTabeP)

    //-- Criando Colunas
    For nX := 1 To Len(aColun)
        If Substr(aColun[nX],1,5)  == 'Preco'
            oFwMs:AddColumn( cNomeP , cTabeP , aColun[nX] , 1 , 3 )
        Else
            oFwMs:AddColumn( cNomeP , cTabeP , aColun[nX] , 1 , 2 )
        Endif
    Next

    //-- Definindo Caracter�sticas
    oFwMs:SetFont('Calibri')			//-- Fonte da planilha
    //oFwMs:SetHeaderSizeFont(10)			//-- Tamanho da fonte do cabe�alho
    //oFwMs:SetLineSizeFont(10)			//-- Tamanho da fonte da linha 1
    //oFwMs:Set2LineSizeFont(10)			//-- Tamanho da fonte da linha 2
    //oFwMs:SetBgColorHeader('#4F4F4F')	//-- Cor da c�lula do cabe�alho
    //oFwMs:SetTitleFrColor('#000000')	//-- Cor da fonte do t�tulo
    //oFwMs:SetLineBgColor('#DCDCDC')		//-- Cor da c�lula da linha 1
    //oFwMs:Set2LineBgColor('#D3D3D3')	//-- Cor da c�lula da linha 2

    //-- Criando Linhas
    aItens := FMntItn()
    For nX := 1 To Len(aItens)
        oFwMs:AddRow(cNomeP,cTabeP,aItens[nX])
    Next nX
    
    //-- Ativando Relat�rio
    oFwMs:Activate()
Return
//-------------------------------------------------------------------
/*/{Protheus.doc} FMntItn
Fun��o respons�vel por definir as linhas do relat�rio, conforme os
par�metros informados pelo usu�rio.

@author	   Milton J.dos Santos
@since	   25/05/2021
@version   P12
@obs	   

Alteracoes Realizadas desde a Estruturacao Inicial
Data       Programador     Motivo
/*/
//------------------------------------------------------------------- 
Static Function FMntItn(oSay)

    Local aIteRel:= {}
    Local cQuery := ""
    Local cAlias := 'ZB9'
    Local cCond  := ".T."

    If MV_PAR01 == "1"
        cCond := "ZB9_INTEGRA = '1'"
    ElseIf MV_PAR01 == "2"
        cCond := "ZB9_INTEGRA = '2'"
    Endif
    DbSelectArea(cAlias)
    (cAlias)->(DbGoTop())
    Do While (cAlias)->(!EOF())
        If &cCond
            AAdd(aIteRel,{  (cAlias)->ZB9_COD,    ;                   
                            (cAlias)->ZB9_CODPAI, ;
                            (cAlias)->ZB9_DESC2,  ;                   
                            (cAlias)->ZB9_MARCA,  ;
                            (cAlias)->ZB9_MODELO, ;
                            (cAlias)->ZB9_CATEG,  ;
                            (cAlias)->ZB9_PRCVEN, ;
                            (cAlias)->ZB9_PRCESP, ;
                            (cAlias)->ZB9_QATU,   ;
                            (cAlias)->ZB9_PESBRU, ;
                            (cAlias)->ZB9_ALTULC, ;
                            (cAlias)->ZB9_LARGLC, ;
                            (cAlias)->ZB9_COMPLC, ;
                            (cAlias)->ZB9_IMG1,   ;
                            (cAlias)->ZB9_IMG2,   ;
                            (cAlias)->ZB9_IMG3,   ;
                            (cAlias)->ZB9_IMG4,   ;
                            (cAlias)->ZB9_NCM,    ;
                            (cAlias)->ZB9_EAN,    ;
                            (cAlias)->ZB9_DTINC,  ;
                            IIF((cAlias)->ZB9_INTEGRA == "1","1-Sim","2-Nao")  })

        Endif
        (cAlias)->(DbSkip())

    EndDo
  //  (cAlias)->(DbCloseArea())

Return(aIteRel)
//-------------------------------------------------------------------
/*/{Protheus.doc} FOpnRlt
Fun��o respons�vel por realizar a abertura do arquivo ap�s ele ter
sido gerado na pasta escolhida pelo usu�rio.

@author	   Milton J.dos Santos
@since	   25/05/2021
@version   P12
@obs	   

Alteracoes Realizadas desde a Estruturacao Inicial
Data       Programador     Motivo
/*/
//------------------------------------------------------------------- 
Static Function FOpnRlt(oSay)

    Local oExc := Nil
    Local cLoc := cGetFile( '.xml |*.xml|' , 'Arquivos XML', 1 , /*'C:\'*/ , .F. , GETF_LOCALHARD+GETF_RETDIRECTORY , .T. , .T. )
    Local cArq := 'ENVCOBR_' + DtoS( Date() ) + '.xml'

    oFwMs:GetXMLFile(cLoc+cArq)

    If !ApOleClient("MsExcel")
        MsgInfo('Microsoft Excel n�o instalado!' + CRLF + CRLF + 'Acesse o arquivo em:' + CRLF + CRLF + cLoc+cArq,'Error!')
    Else
        oExc := MsExcel():New()			//-- Abre uma nova conex�o com o excel
        oExc:WorkBooks:Open(cLoc+cArq)  //-- Abre uma planilha
        oExc:SetVisible(.T.)        	//-- Visualiza a planilha
        oExc:Destroy()              	//-- Encerra o processo
    Endif

Return(Nil)
