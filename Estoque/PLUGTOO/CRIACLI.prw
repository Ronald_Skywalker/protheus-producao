#Include "Protheus.ch"
#include "TBICONN.CH"

/*/
	{Protheus.doc}  CriaCli

	Consulta clientes do sistema PLUGG.TO 

	@type    function
	@author  Milton J.dos Santos
	@version P12.1.25
	@since   21/01/2021
	@param   _cIdPedido
	@return  lOk		= .T. ou .F. Se a operacao foi concluida  
/*/

User Function CriaCli
LOCAL cCGC
LOCAL cEnd
LOCAL cCep
LOCAL nReg

PRIVATE lJob	:= GetRemoteType() == -1 // Verifica se e job

RpcSetEnv("99","01","Admin")

// INICIO - TRECHO TEMPORARIO PARA LIMPAR O CLIENTE
LIMPACLI()
// FIM - TRECHO TEMPORARIO PARA LIMPAR O CLIENTE

DbSelectArea("ZBA")
ZBA->( DbGoTop() )
IF ZBA->(EOF())
	MSGINFO("SUA TABELA ZBA ESTA VAZIA!")
ENDIF
Do While .NOT. ZBA->( EOF() )
	If Empty( ZBA->ZBA_CLISC5 )		
		if Empty(ZBA->ZBA_PCPF)
			cCGC	:= U_RetiraPT( ZBA->ZBA_CNPJ )			
			cPessoa	:= "J"
		else
			cCGC	:= U_RetiraPT( ZBA->ZBA_PCPF )			
			cPessoa	:= "F"
		endif
		cEnd	:= Upper( Alltrim( NoAcento(ZBA->ZBA_PEND) ) + ", " + Alltrim( ZBA->ZBA_PNC ) )
		cCep 	:= U_RetiraPT(	ZBA->ZBA_PZIP	)
		nReg 	:= U_FindEnd(cCGC,cEnd,cCep)
		If nReg == 0
			lRet := U_GeraCli(cPessoa, .F. )
		Else
			DbSelectArea("SA1")
			Dbgoto( nReg )
		Endif
		If lRet
			DbSelectArea("ZBA") 
			Reclock("ZBA",.F.)
			ZBA->ZBA_CLISC5 := SA1->A1_COD
			ZBA->ZBA_LOJSC5 := SA1->A1_LOJA
			ZBA->ZBA_CLIEEN := SA1->A1_COD
			ZBA->ZBA_LOJAEN := SA1->A1_LOJA
			ZBA->( MsUnLock() )
		Endif
	ENDIF
	DbSelectArea("ZBA")
	ZBA->( DBSKIP() )
Enddo

RpcClearEnv()

Return

User Function GeraCli(_cPessoa, _lUnico)
Local aSA1Auto	:= {}
Local aAI0Auto	:= {}
Local nOpcAuto	:= 3
Local lRet		:= .T.
Local cCliCd	:= '000001'
Local cCliLj	:= '0001'
Local cNatureza	:= PADR(SuperGetMv("HP_PLUGNAT",.F.,"021990"), TAMSX3("A1_NATUREZ")[1]) 
Local cCanal	:= SuperGetMv("HP_PLUGCNL",.F.,"007")						// A1_XCANAL	- Cod.Canal         = 007
Local cCanalD	:= POSICIONE("ZA3",1,xFILIAL("ZA3") + cCanal,"ZA3_DESCRI")	// A1_XCANALD	- Descr.Canal       = E-COMMERCE
Local cTpEmp	:= SuperGetMv("HP_PLUGTPE",.F.,"01")						// A1_XEMP		- Tipo de Empresa	= CONSUMIDOR FINAL
Local cDivisao	:= SuperGetMv("HP_PLUGDIV",.F.,"000001")					// A1_XCODDIV	- Divisao			= CLIENTE LOJA VIRTUAL     
Local cCodReg	:= ""
Local cCodMacr	:= ""
Local cCodGrp	:= ""
Local cCodGrpN	:= ""
Local cErroExec	:= ''
Local cErro		:= ''
Local cArqLog	:= ''
Local cDir	    := '\system'
Local cDoc		:= ""		
Local cMun		:= ""		
Local cContabil := PADR(SuperGetMv("HP_PLUGCNT",.F.,"101020203008"), TAMSX3("A1_CONTA")[1])  // Conta contabil padrao para clientes PLUGG.TO      
Local cMsg		:= ""

Private lMsErroAuto := .F.

Default _lUnico	:= .F.

CfgCanal( ZBA->ZBA_CANAL, @cCanal, @cCanalD, @cTpEmp, @cCodReg, @cCodMacr, @cCodGrp, @cCodGrpN, @cDivisao )

//lRet := RpcSetEnv("99","01","Admin")

//If lRet

	 //----------------------------------
	 // Dados do Cliente
	 //----------------------------------

	cCliCd	:= MT030Num()
	cNome	:= Upper( Alltrim( ZBA->ZBA_PNOM ) + " " + Alltrim( ZBA->ZBA_PLAST ) )
	cEnder	:= Upper( Alltrim(NoAcento(ZBA->ZBA_REND))+', '+Alltrim(ZBA->ZBA_RNUM)+' '+Alltrim(NoAcento(ZBA->ZBA_RCOM)) )
	cCodMun := ValidaMun( ZBA->ZBA_PEST , ZBA->ZBA_PMUN )
	cMun	:= Upper( alltrim(	ZBA->ZBA_RMUN	))
	ConOut( "Endereço novo - Complemento:"+cEnder )

	IF _cPessoa	== "F"
		cDoc	:= alltrim(	U_RetiraPT( ZBA->ZBA_PCPF ))								// CHAR 	CPF/CNPJ
	ELSEIF SA1->A1_PESSOA == "J"
		cDoc	:= alltrim(	U_RetiraPT( ZBA->ZBA_CNPJ ))								// CHAR		CNPJ	// VAZIO
	ENDIF
	// Campos OBRIGATORIOS
	aAdd(aSA1Auto,{"A1_COD" 		,alltrim(	cCliCd	)						,Nil})	// CHAR 06
	aAdd(aSA1Auto,{"A1_LOJA" 		,alltrim(	cCliLj	)						,Nil})	// CHAR 04
	aAdd(aSA1Auto,{"A1_TIPO" 		,alltrim(	"F"		)						,Nil})	// CHAR 01
	aAdd(aSA1Auto,{"A1_PESSOA" 		,alltrim(	_cPessoa 	)					,Nil})	// CHAR 01
	aAdd(aSA1Auto,{"A1_INSCR"	 	,alltrim(	"ISENTO"	)					,Nil})	// CHAR 
	aAdd(aSA1Auto,{"A1_CGC"			,cDoc										,Nil})	// CHAR 	CPF/CNPJ
	aAdd(aSA1Auto,{"A1_NOME" 		,cNome										,Nil})	// CHAR 
	aAdd(aSA1Auto,{"A1_NREDUZ" 		,cNome										,Nil})	// CHAR 20
	aAdd(aSA1Auto,{"A1_END" 		,Upper( alltrim( cEnder	))					,Nil})	// CHAR 
	aAdd(aSA1Auto,{"A1_TIPO" 		,alltrim(	"F"				)				,Nil})	// CHAR 01
	aAdd(aSA1Auto,{"A1_PESSOA" 		,alltrim(	_cPessoa	 	)				,Nil})	// CHAR 01
	aAdd(aSA1Auto,{"A1_NOME" 		,alltrim(	cNome	)						,Nil})	// CHAR 
	aAdd(aSA1Auto,{"A1_NREDUZ" 		,alltrim(	cNome	)						,Nil})	// CHAR 20
	aAdd(aSA1Auto,{"A1_BAIRRO"		,Upper( alltrim(	ZBA->ZBA_RNEIGH	))		,Nil})	// CHAR 05
	aAdd(aSA1Auto,{"A1_CEP" 		,U_RetiraPT(	ZBA->ZBA_RZIP	)			,Nil})	// CHAR 08
	aAdd(aSA1Auto,{"A1_REGIAO" 		,alltrim(	"001"	)						,Nil})	// CHAR 03 
	aAdd(aSA1Auto,{"A1_PAIS"	,VerPais( ZBA->ZBA_RPAIS )						,Nil})	// CHAR 05
	IF ! EMPTY(ZBA->ZBA_PEST)
		aAdd(aSA1Auto,{"A1_EST" 	,Upper( alltrim(	ZBA->ZBA_REST 	))		,Nil})	// CHAR 
//		aAdd(aSA1Auto,{"A1_MUN" 	,cMun										,Nil})	// CHAR 
		aAdd(aSA1Auto,{"A1_COD_MUN"	,alltrim(	cCodMun	)						,Nil})	// CHAR 05
	ENDIF
	aAdd(aSA1Auto,{"A1_EMAIL" 		,Lower( alltrim(	ZBA->ZBA_PEMA ))		,Nil})	// CHAR

	//Preenchimento da aba Fiscal
	aAdd( aSa1Auto, { "A1_ENDENT" 	, Upper( alltrim( cEnder	)) 	, Nil } ) 
	aAdd( aSa1Auto, { "A1_CEPE" 	, U_RetiraPT(	ZBA->ZBA_RZIP	)		, Nil } )
	aAdd( aSa1Auto, { "A1_BAIRROE" 	, Upper( alltrim(	ZBA->ZBA_RNEIGH	)) 	, Nil } )
	aAdd( aSa1Auto, { "A1_CODPAIS" 	, SuperGetMv( "MV_XCDPAIS", .F., "01058", "" ) , Nil } )
	aAdd( aSa1Auto, { "A1_MUNE" 	, Upper( alltrim(	ZBA->ZBA_RMUN	))		, Nil } )
	aAdd( aSa1Auto, { "A1_ESTE" 	, Upper( alltrim(	ZBA->ZBA_REST 	))		, Nil } )

	aAdd(aSA1Auto,{"A1_CONTATO"		,alltrim(	cNome			)				,Nil})	// CHAR 20		
	aAdd(aSA1Auto,{"A1_DDI" 		,alltrim(	"55"			)				,Nil})	// CHAR 03		
	aAdd(aSA1Auto,{"A1_DDD" 		,alltrim(	ZBA->ZBA_PPHA	)				,Nil})	// CHAR 03		
	aAdd(aSA1Auto,{"A1_TEL" 		,alltrim(	ZBA->ZBA_PPHO	)				,Nil})	// CHAR 15
	aAdd(aSA1Auto,{"A1_NATUREZ" 	,alltrim(	cNatureza		)				,Nil})	// CHAR 01
	aAdd(aSA1Auto,{"A1_MSBLQL" 		,"2"										,Nil})	// CHAR 01 - Status = 1-Inativo ou 2-Ativo
	aAdd(aSA1Auto,{"A1_CONTA" 		,alltrim(	cContabil	)					,Nil})	// CHAR 		// DEVE SER ANALITICA
	aAdd(aSA1Auto,{"A1_CODPAIS" 	,alltrim(	"01058"	)						,Nil})	// CHAR
	aAdd(aSA1Auto,{"A1_CONTRIB" 	,SuperGetMv( "MV_XCONTRI", .F., "2", "" )   ,Nil})	// CHAR 01 - Contribuinte do ICMS: 1-SIM ou 2-NAO
	aAdd(aSA1Auto,{"A1_XCANAL" 		,alltrim(	cCanal	)						,Nil})	// CHAR 03 - Canal			    	= 007 
	aAdd(aSA1Auto,{"A1_XCANALD" 	,alltrim(	cCanalD	)						,Nil})	// CHAR 50 - Descricao do CANAL		= E-COMMERCE HOPE
	aAdd(aSA1Auto,{"A1_XGRUPO"		,alltrim(	cCodGrp	)						,Nil})	// CHAR 01 - Cod Grupo				= 00010
	aAdd(aSA1Auto,{"A1_XGRUPON" 	,alltrim(	cCodGrpN )						,Nil})	// CHAR 50 - Nome.Grupo do Cliente  = B2W - LOJA VIRTUAL
	aAdd(aSA1Auto,{"A1_XBLQVEN"		,"N"										,Nil})	// CHAR 01 - Bloq.de Venda			= S=Sim ou N=Nao
	aAdd(aSA1Auto,{"A1_XEMP"		,cTpEmp										,Nil})	// CHAR 02 - Tipo de Empresa		= CONSUMIDOR FINAL
	aAdd(aSA1Auto,{"A1_XCODREG"		,cCodReg									,Nil})	// CHAR 08 - Cod.Regiao				= B2W - LOJA VIRTUAL
	aAdd(aSA1Auto,{"A1_XMICRRE"		,cCodMacr									,Nil})	// CHAR 05 - Cod.Macro				= MAGAZINE LUIZA
	aAdd(aSA1Auto,{"A1_XCODDIV"		,cDivisao									,Nil})	// CHAR 06 - Divisao				= CLIENTE LOJA VIRTUAL

	//---------------------------------------------------------
	// Dados do Complemento do Cliente
	//---------------------------------------------------------

	aAdd(aAI0Auto,{"AI0_SALDO" ,30 ,Nil})

	//------------------------------------
	// Chamada para cadastrar o cliente
	//------------------------------------

	U_LogProc( "Incluido cliente: " + cNome )

	DbSelectArea("CT1")
	DbSetOrder(1)
	DbSelectArea("CC2")
	DbSetOrder(1)
	DbSelectArea("SA1")
	
	MSExecAuto({|a,b,c| MATA030(a,b,c)}, aSA1Auto, nOpcAuto, aAI0Auto)

//	TestaExec(nOpcAuto, aSA1Auto, "SA1") 

	If lMsErroAuto
	 	lRet := .F.
		cMsg := "Erro na inclusao!"
		If lJob	
			ConOut( cMsg )
//	 	Salva os erros em memoria para passar para o Pedido
			cArqLog := "ErrorLog_" 	+ cValToChar(Alltrim( cDoc ))
			cArqLog += "_D_" 		+ cValToChar(AllTrim( DtOS( Date() ) ) )
			cArqLog	+= "_H_" 		+ cValToChar(SubStr(Time(),1,2 ))
			cArqLog	+= "_M_" 		+ cValToChar(SubStr(Time(),4,2 ))
			cArqLog	+= ".log"
			cErroExec	:= FwNoAccent(MostraErro(cDir, cArqLog))
			cErro		:= TratErr(cErroExec)
		Else
//	 		Mostra o erro em tela
	 		MostraErro()	// não usar via JOB
		Endif
	Else
		cMsg := "Cliente No." + SA1->A1_COD + " incluido com sucesso! "
		If lJob	// Se executado se interface grafica (JOB ou WebService)
			ConOut( cMsg )
		Else
			If _lUnico
				Alert( cMsg)
			Endif
		Endif
	EndIf
	If _lUnico
		U_LogProc( cMsg )
	Endif
//EndIf
 
//RpcClearEnv()
Return lRet

Static Function MT030Num()
Local cProxNum := ""

	cProxNum := GETSX8NUM("SA1","A1_COD")
	DbSelectArea("SA1")
	DbSetOrder(1)
	While .T.
		If SA1->( DbSeek( xFilial("SA1")  + cProxNum ) )
			ConfirmSX8()
			cProxNum := GetSX8Num("SA1","A1_COD")
		Else
			Exit
		Endif
	Enddo

Return(cProxNum)

STATIC FUNCTION LIMPACLI
DbSelectArea("SA1")
SA1->( DbGoTop() )
Do WHILE ! EOF()
	RECLOCK("SA1",.F.)
	DbDelete()
	MSUNLOCK()
	DBSKIP()
ENDDO
DbSelectArea("ZBA")
ZBA->( DbGoTop() )
Do WHILE ! EOF()
	RECLOCK("ZBA",.F.)
	ZBA->ZBA_CLISC5 := ""
	ZBA->ZBA_LOJSC5 := ""
	ZBA->ZBA_CLIEEN := ""
	ZBA->ZBA_LOJAEN := ""
	MSUNLOCK()
	DBSKIP()
ENDDO
// FIM - TRECHO TEMPORARIO PARA LIMPAR O PEDIDO DE VENDA
RETURN

Static Function VerPais( _cPais )
Local cRet	:= ""
Local aPais	:= {}
Local nPos	:= 0

aAdd(aPais, {"  ","105"} )
aAdd(aPais, {"BR","105"} )

nPos := aScan( aPais, { |x| x[1] == _cPais})
If nPos  > 0
	cRet := aPais[nPos][2]
Endif
Return cRet

User Function RetiraPT( _cDoc )
Local cRet := ""
Local i

For i := 1 to Len( _cDoc )
	If substr( _cDoc , i, 1) $ '1234567890'
		cRet += substr( _cDoc , i, 1)
	Endif
Next
Return( cRet )			

Static Function ValidaMun( _cUF, _cNmun )
Local cCodIBGE	:= Space( TamSX3("CC2_CODMUN")[1] )
Local cUF		:= PadR( Upper( _cUF )	, TamSX3("CC2_EST")[1])
Local cNMun		:= PadR( Upper( NOACENTO(_cNmun) ), TamSX3("CC2_MUN")[1])
Local cArea		:= GetArea()

DbSelectArea("CC2")
DbSetOrder(4)
If DbSeek( xFilial("CC2") + cUF + cNMun)
	cCodIBGE	:= CC2->CC2_CODMUN
Endif

RestArea(cArea)

Return( cCodIBGE )

// Trata erro do execauto
////////////////////////////////////////////////////////////////////////////////////////////////////////
Static Function TratErr(_cErroExec)
////////////////////////////////////////////////////////////////////////////////////////////////////////
Local cErro		:= ''
Local cRet		:= ''
Local cBuffer	:= ''
Local nX		:=  0

For nX		:= 	1 To mlcount(_cErroExec)
	cBuffer := 	RTrim( MemoLine(_cErroExec,, nX,, .F. )) 
	If "Invalido" $ cBuffer
		cErro += cValToChar(cBuffer)
		cRet  += Alltrim(Substr(cErro,1,AT("-",cErro)-1)) + ':' + Alltrim(Substr(cErro,39,AT("<",cErro)-39)) + " nao encontrado(a)"
	Endif
Next nX

Return (cRet)
/*
	Rotina original >> MsRotAuto
*/
Static Function TestaExec(nOpcAuto,xRotAuto,sAlias,uFunCpo)
Local nCnt0,nCnt1, nCnt2, bBlock, aFuncoes,nOrderSX3,nPos
Local AutoReadVar, lObrigat ,cConteudo,uVar,cErro
Local lInit := .f.
Local cAlias

//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Parametro utilizado para validar campos obrigatorios      ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
Local lVldObrig := SuperGetMv("MV_VLDOBRI",.F.,.F.)
Local aErros	:= {}

Private N := 0
Private aTELA[0][0],aGETS[0]
Private aRotAuto

If type("aRotina") # "A"
	aRotina	:= {}
Endif

nOpcAuto := if(nOpcAuto==NIL,3,nOpcAuto)
If type("INCLUI")# "L"
   _SetOwnerPrvt("inclui",nOpcAuto==3)
EndIf
If type("ALTERA")# "L"
   _SetOwnerPrvt("altera",nOpcAuto==4)
EndIf

If Type("lMSErroAuto") == "U"
	Private lMSErroAuto := .F.
EndIf
If Type("lMsHelpAuto") == "U"
	Private lMsHelpAuto := .t.
EndIf
cAlias := sAlias

aRotAuto := (cAlias)->(MSArrayXDB(xRotAuto,.T.,nOpcAuto,,uFunCpo))
//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ se funcao retornar vetor com 0 signIfica que gerou arquivo de log ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
If !( Len(aRotAuto) > 0 )
	Return .t.
EndIf

If  aRotAuto # NIL
	nOrderSX3:= SX3->(IndexOrd())
	SX3->(DbSetOrder(2))
	For nCnt1 := 1 To Len( aRotAuto )
		If ( aRotAuto[nCnt1,1] = "AUT" )
			Loop
		EndIf
		AutoReadVar  := "M->" + Upper( aRotAuto[nCnt1,1 ] )
		&AutoReadVar := aRotAuto[nCnt1,2 ]
		If aRotAuto[nCnt1,3 ] == NIL
			SX3->(DbSeek( aRotAuto[nCnt1,1 ] ))
			If SX3->(Found()) .And. !Empty(SX3->X3_VALID )
				aRotAuto[nCnt1,3 ] := Upper(AllTrim( SX3->X3_VALID ))
			EndIf
		EndIf
		If ( Empty(aRotAuto[nCnt1,3 ]) )
			aRotAuto[nCnt1,3 ] := ".t."
		EndIf
	Next
	
	For nCnt1 := 1 To Len( aRotAuto )
      If ( aRotAuto[nCnt1,1] = "AUT" ) //Variavel serah utilizada dentro dos programas
			Loop
		EndIf
		AutoReadVar := "M->" + Upper( aRotAuto[ nCnt1,1 ] )
		If &AutoReadVar== NIL .or. Empty(&AutoReadVar) .or. Empty(aRotAuto[ nCnt1,2])
			lInit := .f.
			If ExistIni(Subs(AutoReadVar,4))
				lInit := .t.
				&AutoReadVar := InitPad(SX3->X3_RELACAO)
				If ValType(&AutoReadVar) = "C"
					&AutoReadVar := PADR(&AutoReadVar,SX3->X3_TAMANHO)
				EndIf
				If &AutoReadVar == NIL .or. Empty(aRotAuto[ nCnt1,2])
					lInit := .f.
				EndIf
			EndIf
			//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
			//³Manter a variavel de memoria caso uma validacao as atualize e nao tenha   ³
			//³sido mensionada no arquivo texto, somente no caso de inclusao             ³
			//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
			If Empty(aRotAuto[ nCnt1,2]) .and. !Empty(&AutoReadVar) .and. nOpcAuto == 3
				lInit := .T.
			EndIf
			If !lInit
				&AutoReadVar := aRotAuto[ nCnt1,2 ]
			EndIf
		EndIf
		//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
		//³Se for alteracao e alguma validacao atualizar a variavel e a mesma³
		//³nao for um campo que foi passado no array, respeitar a alteracao  ³
		//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
		If nOpcAuto == 4 .and. !aRotAuto[ nCnt1,4 ] .and. (&AutoReadVar != aRotAuto[ nCnt1,2])
			aRotAuto[ nCnt1,2 ] := &AutoReadVar
		EndIf
		//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
		//³Caso alguma validacao atualize a variavel manter o que foi passado³
		//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
		If ( &AutoReadVar != aRotAuto[ nCnt1,2] .and. !Empty(aRotAuto[ nCnt1,2]) )
			&AutoReadVar := aRotAuto[ nCnt1,2 ]
		EndIf
		__READVAR := Upper(AllTrim(AutoReadVar))
		If !( aRotAuto[ nCnt1,3 ] == NIL )
			SX3->(DbSetOrder(2))
			SX3->(DbSeek(Upper(AllTrim(Subs(AutoReadVar,4)))))
			lObrigat := x3uso(SX3->X3_USADO) .and. ((SubStr(BIN2STR(SX3->X3_OBRIGAT),1,1) == "x") .or. VerByte(SX3->x3_reserv,7))
			lMsHelpAuto := .T.
			If (lObrigat .or. ! Empty(&AutoReadVar))
				bBlock := &( "{ || " + AllTrim( aRotAuto[nCnt1,3 ] ) + " }" )
				If SX3->X3_VISUAL # "V"
					lErro	:= ! Eval( bBlock ) 
					lEmpty	:= IIf(lVldObrig, lObrigat .And. Empty(aRotAuto[ nCnt1,2 ]),.F.)
					If lErro .Or. lEmpty 
						lMSErroAuto	:= .T.
						uVar := aRotAuto[nCnt1,2]
						If     (ValType(uVar) == "C")
							cConteudo := uVar
						ElseIf (ValType(uVar) == "N")
							cConteudo := Str(uVar)
						ElseIf (ValType(uVar) == "D")
							cConteudo := DtoC(uVar)
						ElseIf (ValType(uVar) == "L")
							cConteudo := If(uVar,"True","False")
						ElseIf (ValType(uVar) == "M")
							cConteudo := "Memo"
						ElseIf (ValType(uVar) == "U")
							cConteudo := "Nil"
						EndIf
						If (Padr(aRotAuto[nCnt1,1],12) == Padr(Subs(AutoReadVar,4),12))
							cErro := " <-" + Iif(lErro,"- Invalido","") + Iif( lObrigat,"- Obrigatorio","")
						Else
							cErro := ""
						EndIf
						aAdd( aErros, Padr(aRotAuto[nCnt1,1],12)+":="+cConteudo+cErro) 
					EndIf
					SX3->(DbSetOrder(2))
 					SX3->(DbSeek(Upper(AllTrim(Subs(AutoReadVar,4)))))
					If SX3->(X3_TRIGGER) == "S"
						RunTrigger(1)
					EndIf
				EndIf
			EndIf
		EndIf
	Next
	If ( lMSErroAuto )
		AutoGRLog("Tabela "+cAlias+" "+Dtoc(MsDate())+' '+Time() )
		For nCnt2 := 1 To Len(aErros)
			AutoGRLog( aErros[nCnt2] )
		Next
		AutoGRLog(Repl("-",80))
		SX3->(DbSetOrder(nOrderSX3))
	EndIf
	
	SX3->(DbSetOrder(nOrderSX3))
EndIf
lMsHelpAuto := .F.
Return .f.

Static Function CfgCanal( _cCanal, cCanal, cCanalD, cTpEmp, cCodReg, cCodMacr, cCodGrp, cCodGrpN, cDivisao )
cTpEmp	:= "01"					// A1_XEMP		- Tipo de Empresa	= CONSUMIDOR FINAL
cCanal	:= "007"				// A1_XCANAL	- Canal 			= 007
cCanalD	:= POSICIONE("ZA3",1,xFILIAL("ZA3") + cCanal,"ZA3_DESCRI")		// E-COMMERCE
//cCanalD	:= "E-COMMERCE HOPE"	// A1_XCANALD	- Canal 			= E-COMMERCE HOPE
cDivisao:= "000001"				// A1_XCODDIV	- Divisao			= CLIENTE LOJA VIRTUAL     
Do Case
	Case Upper( AllTrim( _cCanal )) == "B2W"
		cCodReg	:= "11045001"			// A1_XCODREG	- Cod.Regiao		= B2W - LOJA VIRTUAL
		cCodMacr:= "11045"				// A1_XMICRRE	- Cod.Macro			= B2W - LOJA VIRTUAL
		cCodGrp	:= "00010"				// A1_XGRUPO	- Cod Grupo			= B2W - LOJA VIRTUAL		
//		cCodGrpN:= "B2W - LOJA VIRTUAL"	// A1_XGRUPON	- Cod Grupo			= B2W - LOJA VIRTUAL		
		cCodGrpN:= POSICIONE("ZA2",1,xFILIAL("ZA2") + cCodGrp,"ZA2_DESCRI")		
	Case Upper( AllTrim( _cCanal )) == "MAGALU"
		cCodReg	:= "11130505"			// A1_XCODREG	- Cod.Regiao		= MAGAZINE LUIZA 
		cCodMacr:= "11130"				// A1_XMICRRE	- Cod.Macro			= SP2-INTERIOR
		cCodGrp	:= "00015"				// A1_XGRUPO	- Cod Grupo			= MAGALU		
//		cCodGrpN:= "MAGALU"				// A1_XGRUPON	- Cod Grupo			= MAGALU		
		cCodGrpN:= POSICIONE("ZA2",1,xFILIAL("ZA2") + cCodGrp,"ZA2_DESCRI")		
	Case Upper( AllTrim( _cCanal )) == "ZZMALL"
		cCodReg	:= "11040010"			// A1_XCODREG	- Cod.Regiao		= LOJA VIRTUAL- INTER
		cCodMacr:= "11040"				// A1_XMICRRE	- Cod.Macro			= LOJA VIRTUAL
		cCodGrp	:= "00022"				// A1_XGRUPO	- Cod Grupo			= FAST ECOMMERCE		
//		cCodGrpN:= "FAST ECOMMERCE"		// A1_XGRUPON	- Cod Grupo			= FAST ECOMMERCE		
		cCodGrpN:= POSICIONE("ZA2",1,xFILIAL("ZA2") + cCodGrp,"ZA2_DESCRI")		
EndCase
Return

