#Include "totvs.ch"
#Include "rwmake.ch"

#DEFINE ENTER CHR(13)+CHR(10)

/*/
	{Protheus.doc} MONZBA
                 
	Monitor de Pedidos de Venda PLUGG.TO

	@author Milton J.dos Santos
	@since 12/01/2021
	@version 1.0
/*/

User Function MONZBA() 
Private cAlias1		:= "ZBA"
Private cAlias2		:= "ZBB"
Private aLegenda := {	{"Alltrim(ZBA_STATUS) 	== 'pending'"	,			"BR_BRANCO"			},; // Ped.Venda PENDENTE     ( pending )
						{"Alltrim(ZBA_STATUS) 	== 'approved'"	,			"BR_VERDE"			},;	// Ped.Venda APROVADO     ( approved )
						{"Alltrim(ZBA_STATUS) 	== 'partial_payment'"	,	"BR_CINZA"			},; // Pagamento Parcial      ( partial_payment )
						{"Alltrim(ZBA_STATUS) 	== 'waiting_invoice'"	,	"BR_PINK"			},;	// Aguardando Faturamento ( waiting_invoice )
						{"Alltrim(ZBA_STATUS) 	== 'invoice_error'"	,		"BR_VIOLETA"		},;	// Nota Fiscal Cancelada  ( invoice_error)
						{"Alltrim(ZBA_STATUS) 	== 'invoiced'"	,			"BR_AZUL"			},;	// Pedido Faturado        ( invoiced )
						{"Alltrim(ZBA_STATUS) 	== 'shipping_informed'"	,	"BR_AZUL_CLARO"		},;	// Aguardando Coleta      ( shipped informed )
						{"Alltrim(ZBA_STATUS) 	== 'shipped'"	,			"BR_AMARELO"		},;	// Em transito	 		  ( shipped )
						{"Alltrim(ZBA_STATUS) 	== 'shipping_error'"	,	"BR_LARANJA"		},;	// Erro no transporte     ( shipping ERROR )
						{"Alltrim(ZBA_STATUS) 	== 'delivered'"	,			"BR_VERMELHO"		},;	// Pedido Entregue        ( delivered )
						{"Alltrim(ZBA_STATUS) 	== 'canceled'"	,			"BR_CANCEL"			}}	// Pedido Cancelado       ( canceled )

PRIVATE _dDataFim
PRIVATE cCadastro		:= "Ped.Venda PLUGG.TO"
PRIVATE cTitulo			:=	cCadastro 
PRIVATE cAliasEnchoice	:=	cAlias1
PRIVATE cAliasGetD		:=	cAlias2
PRIVATE cLinOk  		:= 	"U_ZBALinOk()"
PRIVATE cTudOk  		:= 	"U_ZBATudOk()"
PRIVATE cFieldOk		:= 	"AllwaysTrue()"
PRIVATE aCpoEnchoice	:=	{"ZBA_DATA"}
PRIVATE aRotina		 	:= {{ "Pesquisa"		 ,	"AxPesqui"				,	0,1},;
							{ "Visualizar"		 ,	"U_ZBAVisual"			,	0,2},;
							{ "Alterar"			 ,	"U_ZBAALTEX"			,	0,4},;
							{ "Consulta Status"	 ,	"U_ConsZBA(.T.)"		,	0,5},;
							{ "Parametros"		 ,	"U_PLGX000({'001'})"	,	0,5},;
							{ "Forca Status"	 ,	"U_ForcZBA"				,	0,5},;
							{ "Importar Correios",	"U_ImpCorr"				,	0,5},;
							{ "Historico"		 ,	"U_LogZBA"				,	0,5},;
							{ "Legenda"			 ,	"U_LegZBA"				,	0,6} }

//							{ "Gerar Pedido",	"U_GeraSC5()"			,	0,5},;	// Transferido para U_ForcZBA()
//							{ "Cancelar",		"U_CancZBA()"			,	0,5},;	// Transferido para U_ForcZBA()

DbSelectArea (cAlias1)
DbSelectArea (cAlias2)

BEGIN SEQUENCE

//		VerIfica se as tabelas necessarias existem no dicionario de dados
		If !AliasInDic(cAlias1)
			ShowHelpDlg("SX",{"Dicionario de dados desatualizado."},5,{"Atualize o dicionario de dados para usar esta rotina."},5)
			BREAK
		endIf
				
		DbSelectArea(cAlias1)
		ZBA->(DbSetOrder(1))

		mBrowse(,,,,cAlias1,,,,,, aLegenda )
				
END SEQUENCE
											
RETURN 

User Function LegZBA
Local aLegenda := {}

aAdd( aLegenda, { 'BR_BRANCO'		,"Pedido Pendente"			})	// Pedido Pendente        ( pending )
aAdd( aLegenda, { 'BR_CINZA'		,"Pagamento Parcial"		})  // Pagamento Parcial      ( partial_payment )
aAdd( aLegenda, { 'BR_VERDE'		,"Pedido APROVADO"			})	// Pedido APROVADO        ( approved )
aAdd( aLegenda, { 'BR_PINK'			,"Aguardando Faturamento"	})	// Aguardando Faturamento ( waiting_invoice )
aAdd( aLegenda, { 'BR_AZUL'			,"Pedido Faturado"			})	// Pedido Faturado        ( invoiced )
aAdd( aLegenda, { 'BR_VIOLETA'		,"Nota Fiscal Cancelada"	})	// Nota Fiscal Cancelada  ( invoice_error)
aAdd( aLegenda, { 'BR_AZUL_CLARO'	,"Aguardando Coleta"		})	// Aguardando Coleta      ( shipped informed ) 
aAdd( aLegenda, { 'BR_AMARELO'		,"Em transito"				})	// Em transito	 		  ( shipped ) 
aAdd( aLegenda, { 'BR_LARANJA'		,"Erro no transporte"		})	// Erro no transporte     ( shipping_error ) 
aAdd( aLegenda, { 'BR_VERMELHO'		,"Pedido Entregue"			})	// Pedido Entregue		  ( delivered )
aAdd( aLegenda, { 'BR_CANCEL'		,"Pedido Cancelado"			})	// Pedido Cancelado       ( canceled )

// Cores ainda disponiveis = BR_PRETO, BR_PRETO_0, BR_PRETO_1, BR_PRETO_3, BR_VERDE_ESCURO, BR_MARROM, BR_MARRON_OCEAN

BrwLegenda(cCadastro,'Legenda', aLegenda)

Return

/*
	Inclusao
*/
User Function ZBAInclui() 

Local _ni 

    Local cLinok     := "Allwaystrue"
    Local cTudook     := "Allwaystrue"
    Local nOpce     := 4     //define modo de altera��o para a enchoice
    Local nOpcg     := 4     //define modo de altera��o para o grid
    Local cFieldok     := "Allwaystrue"
    Local lRet         := .T.
    Local cMensagem := ""
    Local lVirtual  := .T.     //Mostra campos virtuais se houver
    Local nFreeze    := 0    
    Local nAlturaEnc:= 400    //Altura da Enchoic
	Local nSizeHeader:=300  

//+--------------------------------------------------------------+
//| Opcoes de acesso para a Modelo 3                             |
//+--------------------------------------------------------------+
cOpcao:="INCLUIR"
Do Case	
	Case cOpcao=="INCLUIR" 
		nOpcE:=3  
		nOpcG:=3	
	Case cOpcao=="ALTERAR" 
		nOpcE:=3  
		nOpcG:=3	
	Case cOpcao=="VISUALIZAR" 
		nOpcE:=2  
		nOpcG:=2
EndCase
DbSelectArea(cAlias1)
DbSetOrder(1)
DbGotop()
//+--------------------------------------------------------------+
//| Cria variaveis M->????? da Enchoice                          |
//+--------------------------------------------------------------+
RegToMemory(cAlias1,(cOpcao=="INCLUIR"))
//+--------------------------------------------------------------+
//| Cria aHeader e aCols da GetDados                             |
//+--------------------------------------------------------------+
nUsado:=0
DbSelectArea("SX3")
DbSetOrder(1)
DbSeek(cAlias2)
aHeader:={}
While !Eof().And.(x3_arquivo== cAlias2)	
	If Alltrim(x3_campo)=="C6_ITEM"		
		dbSkip()		
		Loop	
	EndIf	
	If X3USO(x3_usado).And.cNivel>=x3_nivel    	
		nUsado:=nUsado+1        
		Aadd(aHeader,{	TRIM(x3_titulo),;
						x3_campo, 		;
						x3_picture,		;
						x3_tamanho, 	;
						x3_decimal,		;
						"AllwaysTrue()",;    	     
						x3_usado,		;
						x3_tipo,		;
						x3_arquivo,		;
						x3_context } )	
	EndIf    
	dbSkip()
Enddo
If cOpcao=="INCLUIR"	
	aCols:={Array(nUsado+1)}	
	aCols[1,nUsado+1]:=.F.	
	For _ni:=1 to nUsado		
		aCols[1,_ni]:=CriaVar(aHeader[_ni,2])	
	Next
Else	
	aCols:={}	
	DbSelectArea(cAlias2)	
	DbSetOrder(1)	
	dbSeek(xFilial()+M->C5_NUM)	
	While !eof().and. (cAlias2)->C6_NUM == M->C5_NUM		
		AADD(aCols,Array(nUsado+1))		
		For _ni:=1 to nUsado			
			aCols[Len(aCols),_ni]:=FieldGet(FieldPos(aHeader[_ni,2]))		
		Next 		
		aCols[Len(aCols),nUsado+1]:=.F.		
		dbSkip()	
	Enddo
EndIf
If Len(aCols) > 0
//+--------------------------------------------------------------+	
//| Executa a Modelo 3                                           |	
//+--------------------------------------------------------------+	
	cTitulo			:= "Pedido de Vendas PLUGG.TO"	
	cAliasEnchoice	:= cAlias1	
	cAliasGetD		:= cAlias2
	cLinOk			:= "AllwaysTrue()"	
	cTudOk			:= "AllwaysTrue()"	
	cFieldOk		:= "AllwaysTrue()"
	//	aCpoEnchoice:={} 
	//{"C5_CLIENTE"}	

  _lRet:=Modelo3(cTitulo,cAliasEnchoice,cAliasGetD,,cLinOk,cTudOk,nOpcE,nOpcG,cFieldOk,nSizeHeader)

//+--------------------------------------------------------------+	
//| Executar processamento                                       |	
//+--------------------------------------------------------------+	
	If _lRet		
		Aviso("Modelo3()","Confirmada operacao!",{"Ok"})	
	EndIf
EndIf                 
Return

User Function ZBAAltera() 
Local _i
Local nUsado
Local _nx

//Local cLinok    := "Allwaystrue"
//Local cTudook   := "Allwaystrue"
//Local nOpce     := 4     //define modo de altera��o para a enchoice
//Local nOpcg     := 4     //define modo de altera��o para o grid
//Local cFieldok  := "Allwaystrue"
//Local lRet      := .T.
//Local cMensagem := ""
//Local lVirtual  := .T.     //Mostra campos virtuais se houver
//Local nFreeze    := 0    

Local nAlturaEnc:= 400
Local nSizeHeader:= 300

//Modelo3(cTitulo,cAliasEnchoice,cAliasGetD,,cLinOk,cTudOk,nOpcE,nOpcG,cFieldOk,,,,,,,nAlturaEnc,nSizeHeader)

/*
/// VALIDA OPERACAO 
If ZBA->ZBA_DP $ "DG" .AND. ZBA->ZBA_DATA <= dDATABASE 
   	Aviso("ATENCAO","NAO pode se alterar uma tabela de - DESCONTO "+iIf(ZBA->ZBA_DP = "D","PADRAO","PROGRESSIVO")+". Crie uma nova tabela ou utilize VISUALIZAR",{"OK"})
	RETURN( nil ) 
END 
*/
//+--------------------------------------------------------------+
//� OPCAO  de acesso para a Modelo 3                             �
//+--------------------------------------------------------------+
//	Case cOpcao=="INCLUIR"; nOpcE:=3 ; nOpcG:=3
//	Case cOpcao=="ALTERAR"; nOpcE:=3 ; nOpcG:=3
//	Case cOpcao=="VISUALIZAR"; nOpcE:=2 ; nOpcG:=2
nOpcE	:=	3 
nOpcG	:=	3          
_COPCAO	:= "ALTERA"

//+--------------------------------------------------------------+
//� Cria aHeader e aCols da GetDados                             �
//+--------------------------------------------------------------+
nUsado	:=	0
aHeader	:=	{}
aCOLS	:= 	{}
DbSelectArea("SX3")
dbSeek(cAlias2)

While !Eof().And.(x3_arquivo==cAlias2)
	
	If X3USO(x3_usado) .And. cNivel >= x3_nivel .AND. X3_BROWSE !="N"
		nUsado ++ 
		Aadd(aHeader,{ TRIM(x3_titulo), ;
						x3_campo, ;
						x3_picture,;
						x3_tamanho,;
						x3_decimal,;
						"AllwaysTrue()",;
						x3_usado,;
						x3_tipo,;
						x3_arquivo,;
						x3_context } )
	EndIf
	dbSkip()
End

//+--------------------------------------------------------------+
// Cria variaveis M->????? da Enchoice                           |
//+--------------------------------------------------------------+
DbSelectArea(cAlias1)
RegToMemory(cAlias1, .F. ) 

DbSelectArea(cAlias2)
DbSetOrder(1)

If 	dbSeek( xFilial(cAlias2) + ZBA->ZBA_ID )
	WHILE 	ZBB->ZBB_FILIAL == xFilial(cAlias2) .AND. ZBB->ZBB_ID == ZBA->ZBA_ID .AND.  !EOF() 
			aAdd(aCols,Array(nUsado+1))
			For _i := 1 to nUsado
				aCols[Len(aCols),_i]:=FieldGet(FieldPos(aHeader[_i,2]))
			Next
			aCols[Len(aCols),nUsado+1] := .F.
			dbSkip()
	END
ELSE
	aCols:={Array(nUsado+1)}
	aCols[1,nUsado+1]:=.F.
	For _i:=1 to nUsado
		aCols[1,_i]:=CriaVar(aHeader[_i,2])
	Next
END 

//+--------------------------------------------------------------+
// Executa a Modelo 3                                            |
//+--------------------------------------------------------------+
DbSelectArea(cAlias1) 

lRETORNO 		:=	Modelo3(cTitulo,cAliasEnchoice,cAliasGetD,,cLinOk,cTudOk,nOpcE,nOpcG,cFieldOk,,,,,,,nAlturaEnc,nSizeHeader)
  
If ! lRETORNO
	RETURN( nil ) 
END

///////////////////////////////////////////////////////
/// ALTERA registro - ZBA                           ///
///////////////////////////////////////////////////////
DbSelectArea(cAlias1)
DbSetOrder(1)

RecLock('ZBA',.F.)
ZBA->ZBA_DATA    := M->ZBA_DATA
ZBA->ZBA_DATAF   := M->ZBA_DATAF
ZBA->ZBA_CATEG   := M->ZBA_CATEG
ZBA->ZBA_DCATEG  := M->ZBA_DCATEG
ZBA->ZBA_CLASSE  := M->ZBA_CLASSE
ZBA->ZBA_DCLASSE := M->ZBA_DCLASSE
ZBA->ZBA_DP      := M->ZBA_DP
ZBA->ZBA_OBJETIV := M->ZBA_OBJETIV
ZBA->ZBA_META    := M->ZBA_META
ZBA->ZBA_PALVO   := M->ZBA_PALVO
ZBA->ZBA_PRECORI := M->ZBA_PRECORI
ZBA->ZBA_TPMETA  := M->ZBA_TPMETA  
ZBA->ZBA_PERCDES := M->ZBA_PERCDES
ZBA->ZBA_CONDP   := M->ZBA_CONDP
ZBA->(msUnlock()) 		

///////////////////////////////////////////////////////
/// EXCLUI/INCLUI registros - ZBB                   ///
///////////////////////////////////////////////////////
DbSelectArea(cAlias2)
DbSetOrder(4)

If 	dbSeek( ZBA->ZBA_FILIAL + ZBA->ZBA_ID )
	WHILE 	ZBB->( ZBB_FILIAL + ZBB_ID ) == ZBA->( ZBA_FILIAL + ZBA_ID ) .AND.  !EOF() 
		RecLock(cAlias2,.F.)
		dbDelete()
		ZBB->(MsUnlock())
		ZBB->(dbSkip())
	END
END

FOR _nx := 1 TO Len(aCols)    			// Localiza todos os itens em aCols

	If 	aCols[_nx,len(aHeader)+1] 		// VERIfICA linha deletada
		LOOP
	END

    RECLOCK('ZBB',.T.)
  	ZBB->ZBB_Filial  := xFilial(cAlias2)
  	ZBB->ZBB_DATA    := ZBA->ZBA_DATA
	ZBB->ZBB_DATAF   := ZBA->ZBA_DATAF
	ZBB->ZBB_CATEG   := ZBA->ZBA_CATEG
	ZBB->ZBB_CLASSE  := ZBA->ZBA_CLASSE
	ZBB->ZBB_DP      := ZBA->ZBA_DP
	ZBB->ZBB_NDIAS   := (ZBA->ZBA_DATAF - ZBA->ZBA_DATA) +  1
	ZBB->ZBB_DIAINI  := DAY(ZBA->ZBA_DATA)  * -1 //valor negativo
	ZBB->ZBB_NUMDATA := VAL(DTOS(ZBA->ZBA_DATA)) * -1 //valor negativo

 	ZBB->ZBB_CODPD   := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_CODPD"	})]
	ZBB->ZBB_LINHA   := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_LINHA"	})]
	ZBB->ZBB_DLINHA  := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_DLINHA"	})]
	ZBB->ZBB_SEGMENT := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_SEGMENT"	})]
	ZBB->ZBB_DSEG    := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_DSEG"		})]
	ZBB->ZBB_COLEC   := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_COLEC"	})]
	ZBB->ZBB_DCOLEC  := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_DCOLEC"	})]
	ZBB->ZBB_PRODUTO := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_PRODUTO"	})]
  	ZBB->ZBB_DESC    := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_DESC"		})]
	ZBB->ZBB_CONDPAG := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_CONDPAG"	})]
	ZBB->ZBB_PRCPROM := aCols[_nx,aScan(aHeader,{|x| Upper(Alltrim(x[2])) == "ZBB_PRCPROM"	})]
	ZBB->(MSUNLOCK())

NEXT

RETURN nil


/*
	Programa  �ZBAAYVisual() �Autor  �Milton J.dos Santos � Data �  14/08/01 ���
	Desc.     �Visualiza                                                   ���
*/

USER FUNCTION ZBAVisual()
Local cOldArea  	:= GETAREA()
Local _i			:= 0
Local nSizeHeader	:= 300

//+--------------------------------------------------------------+
//| OPCAO  de acesso para a Modelo 3                             |
//+--------------------------------------------------------------+
//	Case cOpcao=="INCLUIR"; nOpcE:=3 ; nOpcG:=3
//	Case cOpcao=="ALTERAR"; nOpcE:=3 ; nOpcG:=3
//	Case cOpcao=="VISUALIZAR"; nOpcE:=2 ; nOpcG:=2
nOpcE	:=	2 
nOpcG	:=	2

//+--------------------------------------------------------------+
//| Cria aHeader e aCols da GetDados                             |
//+--------------------------------------------------------------+
nUsado	:=	0
aHeader	:=	{}
aCols	:= 	{}
DbSelectArea("SX3")
DbSetOrder(1)
dbSeek("ZBB")

While !Eof().And.(x3_arquivo == "ZBB")
	
	If X3USO(x3_usado) .And. cNivel >= x3_nivel .AND. X3_BROWSE != "N"

		nUsado ++ 

		Aadd(aHeader,{ TRIM(x3_titulo), ;
						x3_campo, ;
						x3_picture,;
						x3_tamanho,;
						x3_decimal,;
						"AllwaysTrue()",;
						x3_usado,;
						x3_tipo,;
						x3_arquivo,;
						x3_context } )
	EndIf
	dbSkip()
End

//+--------------------------------------------------------------+
//| Cria variaveis M->????? da Enchoice                          |
//+--------------------------------------------------------------+
DbSelectArea("ZBA")
RegToMemory("ZBA", .F. ) 

DbSelectArea("ZBB")
DbSetOrder(1)

If 	dbSeek(xFilial("ZBA") + ZBA->ZBA_ID )
	WHILE ZBB->(!EOF()) .AND. ZBB->( ZBB_FILIAL + ZBB_ID) == ZBA->( ZBA_FILIAL + ZBA_ID ) 
			aAdd(aCols,Array(nUsado+1))
			For _i := 1 to nUsado
				aCols[Len(aCols),_i] := FieldGet(FieldPos(aHeader[_i,2]))
			Next
			aCols[Len(aCols),nUsado+1] := .F.
			dbSkip()
	END
ELSE
	aCols:={Array(nUsado+1)}
	aCols[1,nUsado+1]:=.F.
	For _i:=1 to nUsado
		aCols[1,_i]:=CriaVar(aHeader[_i,2])
	Next
END 

RESTAREA( cOldArea )
//+--------------------------------------------------------------+
//| Executa a Modelo 3                                           |
//+--------------------------------------------------------------+

DbSelectArea("ZBA") 
Modelo3(cTitulo,cAliasEnchoice,cAliasGetD,,cLinOk,cTudOk,nOpcE,nOpcG,cFieldOk,,,,,,,nSizeHeader)

//Modelo3(cTitulo,cAlias1,cAlias2,aMyEncho,cLinOk,cTudoOk,nOpcE,nOpcG,cFieldOk,lVirtual,nLinhas,aAltEnchoice,nFreeze,aButtons,aCordW, nSizeHeader)
RETURN( nil ) 

User Function ZBALinOk()

LOCAL _aGETAREA		:= GETAREA()
LOCAL lRETORNO 		:= .T. 
LOCAL _nPosDESC     := aScan(aHeader,{|x| Alltrim(x[2]) == "ZBB_DESC"	})
LOCAL _nPosCONDPAG  := aScan(aHeader,{|x| Alltrim(x[2]) == "ZBB_CONDPAG"	})
LOCAL _nPosPRCPROM  := aScan(aHeader,{|x| Alltrim(x[2]) == "ZBB_PRCPROM"	})
LOCAL _nPosCODPD    := aScan(aHeader,{|x| Alltrim(x[2]) == "ZBB_CODPD"	})
LOCAL lMETA  		:= .F.

If 	_cOPCAO == "EXCLUIR"
	RETURN( .T. ) 
END

If aCols[n,len(aHeader)+1] //se linha nao for deletada.
	RETURN( .T. ) 
END

If EMPTY( ACols[n,_nPosCODPD] ) 
	Aviso("ATENCAO","Codigo sequencial do CADASTRO nao INFORMADO!!! - ZBB_CODPD - ",{"OK"})
	RETURN( .F. ) 
END 

If !EMPTY( M->ZBA_PERCDES ) 
	RETURN( .T. ) 
END

If 	M->ZBA_DP = 'D'
	If EMPTY( ACols[n,_nPosDESC] ) .AND. EMPTY( ACols[n,_nPosCONDPAG] )
		Aviso("ATENCAO","Informe o PERCENTUAL DE DESCONTO ou CONDICAO DE PAGAMENTO!!!",{"OK"})
   		lRetorno := .F.
   	END 
ELSE 
   	DbSelectArea("SZW")
  	DbSetOrder(1)

   If 	dbSeek(xFilial("SZW")+M->ZBA_NUM)  //procura pelas metas com codigo do ZA //rejane
		lMeta := .T.
	END

   	If 	EMPTY(ACols[n,_nPosDESC]) .and. empty(ACols[n,_nPosCONDPAG]) .and. empty(ACols[n,_nPosPRCPROM]) .and. !lMETA
   		Aviso("ATENCAO","Pelo menos um dos campos (% Desconto, Cond.Pagto. ou Preco) deve ser preenchido, conforme os campos Preco Alvo e Preco Orig.",{"OK"})
      	lRETORNO := .F.
   	ELSE 
       	If M->ZBA_PALVO = 'S' .and. empty(ACols[n,_nPosCONDPAG]) .and. empty(ACols[n,_nPosPRCPROM]) .and. !lMETA
          	Aviso("ATENCAO","Para esta promocao, voce deve preencher a Cond.Pagto. ou o Preco Prom.",{"OK"})
          	lRETORNO := .F. 
       	END 
       	If M->ZBA_PALVO $ 'CD' .and. empty(ACols[n,_nPosDESC]) .and. !lMETA 
          	Aviso("ATENCAO","Para esta promocao, voce deve preencher o percentual de desconto",{"OK"})
          	lRETORNO := .F.
       	END 
   	END
END 

/// VERIfICA preenchimento LINHA/SEGMENTO
If (BUSCACOLS("ZBB_LINHA") != LEFT(BUSCACOLS("ZBB_SEGMENT"),2) ) .and. !empty(BUSCACOLS("ZBB_SEGMENT"))
   Aviso("ATENCAO","LINHA / SEGMENTO Incompativeis!!!",{"OK"},1,"VERIfIQUE")
	lRETORNO := .F.
END	

RestArea(_aGETAREA)

Return( lRETORNO )

User Function ZBATudOk()

LOCAL _aGETAR		:= GETAREA()
LOCAL lRETORNO 		:= .T. 

LOCAL _nPosCODPD    := aScan(aHeader,{|x| Alltrim(x[2]) == "ZBB_CODPD"})

If M->ZBA_DP == "P" .AND. M->ZBA_META # "S"
   DbSelectArea("SZW")
   DbSetOrder(1)
   If !dbSeek(xFilial("SZW")+M->ZBA_NUM,.F.)
      Aviso("ATENCAO","Voce deve cadastrar a",{"OK"})
      lRETORNO := .F. 
   END 
END 

RestArea(_aGETAR)
Return( lRETORNO )

User Function XMLRBR()
Local cFile := ""

If ExistBlock("GERAPED")
	cFile := U_AchaXML()
	If .not. Empty( cFile )
		U_GERAPED( .T. )
	EndIf
ENDIf

RETURN

User Function ConsZBA( _lUnico )
Local cIdOrder	:= ZBA->ZBA_ID
Local aDados	:= {}
Local aCabec  	:= {}
Local aItens  	:= {}
Local nDIf		:= 0

PRIVATE lJob	:= GetRemoteType() == -1
PRIVATE cToken  := U_GetToken( 'ZBA', cIdOrder , .T. )
PRIVATE cTxtLog	:= ""

If lJob
	aDados	:= U_HOPPLUG2( cIdOrder, 1 ) 
Else
	Processa({|| aDados	:= U_HOPPLUG2( cIdOrder, 1 ) }, "Atualizando STATUS", "Pedido no. " + cIdOrder + "... ", .F. )
EndIf
If Len( aDados ) == 0
	cMsg := "Nao conseguiu resgatar status do Pedido No " + cIdOrder
	If lJob
		Conout( cMsg )
	Else
		If _lUnico
//			MsgStop( cMsg ,"Bloqueio")
		EndIf
	EndIf
	U_LogProc( cMsg )
Else
	aCabec := aDados[1][1]
	aItens := aDados[1][2]

	VerDIf( aCabec, aItens, _lUnico )

EndIf

If _lUnico
	U_ShowErrLog("Consulta Status", cTxtLog, .F.)
EndIf

Return

User Function ForcZBA()
Local oButton
Local oCombo
Local cStatus	:= Alltrim( ZBA->ZBA_STATUS )
Local aItems	:= {'1=Cancelar Pedido     ',;	// Pedido Cancelado       ( canceled )
					'2=Informar Pagamento  ',;	// Pagamento Efetuado     ( partial_payment )
					'3=Aprovar Pedido      ',;	// Pedido Aprovado        ( approved )
					'4=Criar Pedido Interno',;	// Aguardando Faturamento ( waiting_invoice )
					'5=Faturar Pedido      ',;	// Pedido Faturado        ( invoiced )
					'6=Cancelar Nota Fiscal',;	// Nota Fiscal Cancelada  ( invoice_error)
					'7=Informar Transporte ',;	// Aguardando Coleta      ( shipped informed )
					'8=Em transito         ',;	// Em transito   		  ( shipped )	
					'9=Erro no transporte  ',;	// Erro no transporte     ( shipping ERROR )
					'A=Informar Entrega    '}	// Pedido Entregue		  ( delivered )

Local lRet		:= .T.
Local lMuda		:= .T.

Private lJob	:= GetRemoteType() == -1
Private cSHINF	:= Space(30)
Private cSHINL	:= Space(25)
Private cSHINU	:= Space(06)
Private cSHISE	:= Space(01)
Private dSHDA	:= CtoD(" ")
Private cSHICF	:= '5102'

If Upper( Alltrim( ZBA->ZBA_STATUS ) ) == 'CANCELED'
	MsgStop("Atualizacao bloqueada, pedido esta cancelado","Bloqueado")
	lRet := .F.
EndIf
If lRet
	DEFINE MsDialog oDlg TITLE "Atualiza��o de Status" FROM 100,100 TO 350,540 PIXEL
	oDlg:lEscClose := .F.

	@ 15,10 	SAY 	"Qual STATUS deseja aplicar?" 	SIZE 80,20 of oDlg PIXEL

// Cria o Objeto tCheckBox usando o comando @ .. CHECKBOX
	@ 30,10 CHECKBOX oChkBox VAR lMuda PROMPT "Permite Alterar" SIZE 60,15 OF oDlg PIXEL 

	cStatus	:= aItems[1]
	oCombo	:= TComboBox():New(11,100,{|u|If(PCount()>0,cStatus:=u,cStatus)},	aItems,100,100,oDlg,,,,,,.T.,,,,,,,,,'cStatus')

	@ 113,142 BUTTON oButton  PROMPT "CANCELA"	SIZE 035, 010 OF oDlg ACTION (lRet:=.F.,oDlg:End()) PIXEL MESSAGE "SAIR"
	@ 113,185 BUTTON oButton  PROMPT "OK"    	SIZE 035, 010 OF oDlg ACTION (lRet:=.T.,oDlg:End()) PIXEL MESSAGE "CONFIRMA"

	ACTIVATE MsDialog oDlg CENTERED
EndIf

If cStatus $ "123569"
	MsgAlert("Atencao. Esta opcao nao deve ser forcada !")
	lRet := .F.
EndIf
If lRet
	Do Case
		Case cStatus == "1"
			lRet	:= U_CancZBA()

		Case cStatus == "2"
			nPAYT	:= TotPed()
			nPAYIN	:= 1
			cPAYM	:= "credit_card        "
			cPAYTY	:= "credit     "
			nPAYQ	:= 30
			nPAYI	:= 3

			DEFINE MsDialog oDlg TITLE "Dados do Pagamento" FROM 100,100 TO 350,540 PIXEL
			oDlg:lEscClose := .F.

			@  15,10  SAY 	"Tipo Pagamento"	 	 SIZE 80,20 of oDlg PIXEL
		   	@  12,65  MSGET	cPAYTY					 SIZE 40,10 Of oDlg PIXEL  When lMuda 
			@  30,10  SAY 	"Metodo de Pagamento" 	 SIZE 80,20 of oDlg PIXEL
		   	@  27,65  MSGET	cPAYM					 SIZE 40,10 Of oDlg PIXEL  When lMuda 
			@  45,10  SAY 	"Numero de Parcelas" 	 SIZE 80,20 of oDlg PIXEL
		   	@  42,65  MSGET	nPAYIN					 SIZE 40,10 Of oDlg PIXEL  When lMuda 
			@  60,10  SAY 	"Valor Total"	 		 SIZE 80,20 of oDlg PIXEL
		   	@  57,65  MSGET	nPAYT					 SIZE 80,10 Of oDlg PIXEL  When lMuda Picture "@E 999,999,999.9999" 
			@  75,10  SAY 	"Quota"			 		 SIZE 80,20 of oDlg PIXEL
		   	@  72,65  MSGET	nPAYQ					 SIZE 40,10 Of oDlg PIXEL  When lMuda Picture "@E 999" 
			@  90,10  SAY 	"Interest"		 		 SIZE 80,20 of oDlg PIXEL
		   	@  87,65  MSGET	nPAYI					 SIZE 40,10 Of oDlg PIXEL  When lMuda Picture "@E 999" 

			@ 113,142 BUTTON oButton  PROMPT "CANCELA"	SIZE 035, 010 OF oDlg ACTION (lRet:=.F.,oDlg:End()) PIXEL MESSAGE "SAIR"
			@ 113,185 BUTTON oButton  PROMPT "OK"    	SIZE 035, 010 OF oDlg ACTION (lRet:=.T.,oDlg:End()) PIXEL MESSAGE "CONFIRMA"

			ACTIVATE MsDialog oDlg CENTERED

			If lRet
				RecLock("ZBA",.F.)
				ZBA->ZBA_STATUS	:= "approved"
				ZBA->ZBA_PAYTY	:= cPAYTY
				ZBA->ZBA_PAYM	:= cPAYM
				ZBA->ZBA_PAYIN	:= nPAYIN
				ZBA->ZBA_PAYT	:= nPAYT
				ZBA->ZBA_PAYQ	:= nPAYQ
				ZBA->ZBA_PAYI	:= nPAYI
				ZBA->( MsUnLock() )
			EndIf
		Case cStatus == "3"
			If Upper( Alltrim( ZBA->ZBA_STATUS ) ) == 'PENDING'
				RecLock("ZBA",.F.)
				ZBA->ZBA_STATUS	:= "approved"
				ZBA->( MsUnLock() )
			Else
				MsgStop("Atualizacao bloqueada, veja o status do pedido","Bloqueado")
				lRet := .F.
			EndIf

		Case cStatus == "4"
//			lRet := Continua( cStatus )
			If Empty( ZBA->ZBA_NUMSC5 )
				If Upper( Alltrim( ZBA->ZBA_STATUS ) ) == 'PENDING'
					MsgStop("Pedido encontra-se pendente !")
					lRet := .F.
				Else
					lRet := MsgYesNo("Pedido ainda nao existe. Deseja criar agora?")
					If lRet
						lRet := U_GeraSC5( .T. ) 
					EndIf
				EndIf
			EndIf
			If lRet
				cNumSC5	:= ZBA->ZBA_NUMSC5

				DEFINE MsDialog oDlg TITLE "Dados do Pedido Interno" FROM 100,100 TO 350,540 PIXEL
				oDlg:lEscClose := .F.

				@  15,10  SAY 	"Numero do Pedido" 		 SIZE  80,20 of oDlg PIXEL
				@  12,65  MSGET	cNumSC5				 	 SIZE 100,10 Of oDlg PIXEL  When lMuda 

				@ 113,142 BUTTON oButton  PROMPT "CANCELA"	SIZE 035, 010 OF oDlg ACTION (lRet:=.F.,oDlg:End()) PIXEL MESSAGE "SAIR"
				@ 113,185 BUTTON oButton  PROMPT "OK"    	SIZE 035, 010 OF oDlg ACTION (lRet:=.T.,oDlg:End()) PIXEL MESSAGE "CONFIRMA"

				ACTIVATE MsDialog oDlg CENTERED
			EndIf
			If lRet
				RecLock("ZBA",.F.)
//				ZBA->ZBA_STATUS	:= "waiting_invoice"
				ZBA->ZBA_NUMSC5	:= cNumSC5 
				ZBA->( MsUnLock() )
			EndIf

		Case cStatus == "5"
			cSHINF	:= ZBA->ZBA_SHINF		// "44444444444444444444444444444"
			cSHINL	:= ZBA->ZBA_SHINL		// "http://nfe.com.br/nsaasa"
			cSHINU	:= ZBA->ZBA_SHINU		// "12"
			cSHISE	:= ZBA->ZBA_SHISE		// "1"
			dSHDA	:= ZBA->ZBA_SHDA		// Date()
			cSHICF	:= 'CFOP'				// CFOP'
			lRet	:= Continua( cStatus )
			If lRet
				cSHIST	:= "invoiced"

				DEFINE MsDialog oDlg TITLE "Dados do Faturamento" FROM 100,100 TO 350,540 PIXEL
				oDlg:lEscClose := .F.

				@  15,10  SAY 	"NFE Chave" 			SIZE  80,20 of oDlg PIXEL
				@  12,65  MSGET	cSHINF					SIZE 100,10 Of oDlg PIXEL  When lMuda 
				@  30,10  SAY 	"NFE Link"			 	SIZE  80,20 of oDlg PIXEL
				@  27,65  MSGET	cSHINL					SIZE 100,10 Of oDlg PIXEL  When lMuda 
				@  45,10  SAY 	"NFE Numero"		 	SIZE  80,20 of oDlg PIXEL
				@  42,65  MSGET	cSHINU					SIZE  40,10 Of oDlg PIXEL  When lMuda 
				@  60,10  SAY 	"NFE Serie"			 	SIZE  80,20 of oDlg PIXEL
				@  57,65  MSGET	cSHISE					SIZE  40,10 Of oDlg PIXEL  When lMuda 
				@  75,10  SAY 	"NFE Date"			 	SIZE  80,20 of oDlg PIXEL
				@  72,65  MSGET	dSHDA					SIZE  40,10 Of oDlg PIXEL  When lMuda 
				@  90,10  SAY 	"NFE CFOP"			 	SIZE  80,20 of oDlg PIXEL
			   	@  87,65  MSGET	cSHICF					SIZE  40,10 Of oDlg PIXEL  When lMuda 

				@ 113,142 BUTTON oButton  PROMPT "CANCELA"	SIZE 035, 010 OF oDlg ACTION (lRet:=.F.,oDlg:End()) PIXEL MESSAGE "SAIR"
				@ 113,185 BUTTON oButton  PROMPT "OK"   	SIZE 035, 010 OF oDlg ACTION (lRet:=.T.,oDlg:End()) PIXEL MESSAGE "CONFIRMA"

				ACTIVATE MsDialog oDlg CENTERED
			EndIf
			If lRet
				RecLock("ZBA",.F.)
				ZBA->ZBA_STATUS	:= "invoiced"					
				ZBA->ZBA_SHIST	:= cSHIST
				ZBA->ZBA_SHINF	:= cSHINF
				ZBA->ZBA_SHINL	:= cSHINL
				ZBA->ZBA_SHINU	:= cSHINU
				ZBA->ZBA_SHISE	:= cSHISE
				ZBA->ZBA_SHDA	:= dSHDA
				ZBA->ZBA_SHICF	:= cSHICF
				ZBA->( MsUnLock() )
			EndIf

		Case cStatus == "6"	
			RecLock("ZBA",.F.)
			ZBA->ZBA_STATUS	:= "invoice_error"
			ZBA->( MsUnLock() )

		Case cStatus == "7"
			cSHINF	:= ZBA->ZBA_SHINF		// "44444444444444444444444444444"
			cSHINL	:= ZBA->ZBA_SHINL		// "http://nfe.com.br/nsaasa"
			cSHINU	:= ZBA->ZBA_SHINU		// "12"
			cSHISE	:= ZBA->ZBA_SHISE		// "1"
			dSHDA	:= ZBA->ZBA_SHDA		// Date()
			cSHICF	:= 'CFOP'				// CFOP'

			lRet := Continua( cStatus )
			If lRet
				cSHIC	:= PADR("Correios"				, 10)
				cSHIME	:= PADR("SEDEX"					, 10)
				cSHITR	:= ZBA->ZBA_SHITR
				If Empty( ZBA->ZBA_SHIRA )
//					cSHIRA	:= PADR("http://correios.com.br",100)
					cSHIRA	:= PADR("https://www2.correios.com.br/sistemas/rastreamento/default.cfm",100)
				Else
					cSHIRA	:= ZBA->ZBA_SHIRA
				EndIf
				dSHIDT	:= ZBA->ZBA_SHIDT
				cSHIST	:= "shipping_informed"

				DEFINE MsDialog oDlg TITLE "Dados de transporte" FROM 50,100 TO 500,540 PIXEL
				oDlg:lEscClose := .F.

				@  15,10  SAY 	"Transportadora" 		SIZE  80,20 of oDlg PIXEL
				@  12,65  MSGET	cSHIC					SIZE  40,10 Of oDlg PIXEL  When lMuda  
				@  30,10  SAY 	"Tipo de Envio"			SIZE  80,20 of oDlg PIXEL
				@  27,65  MSGET	cSHIME					SIZE  40,10 Of oDlg PIXEL  When lMuda 
				@  45,10  SAY 	"Codigo de Rastreio"	SIZE  80,20 of oDlg PIXEL
				@  42,65  MSGET	cSHITR					SIZE  80,10 Of oDlg PIXEL  When lMuda 
				@  60,10  SAY 	"URL de Rastreio"	 	SIZE 100,20 of oDlg PIXEL  
				@  57,65  MSGET	cSHIRA					SIZE 180,10 Of oDlg PIXEL  When lMuda
				@  75,10  SAY 	"Data Enviada" 			SIZE  80,20 of oDlg PIXEL
				@  72,65  MSGET	dSHIDT					SIZE  40,10 Of oDlg PIXEL  When lMuda  
				@  90,10  SAY 	"NFE Chave" 			SIZE  80,20 of oDlg PIXEL
				@  87,65  MSGET	cSHINF					SIZE 100,10 Of oDlg PIXEL  When lMuda 
				@ 105,10  SAY 	"NFE Link"			 	SIZE  80,20 of oDlg PIXEL
				@ 102,65  MSGET	cSHINL					SIZE 100,10 Of oDlg PIXEL  When lMuda 
				@ 120,10  SAY 	"NFE Numero"		 	SIZE  80,20 of oDlg PIXEL
				@ 117,65  MSGET	cSHINU					SIZE  40,10 Of oDlg PIXEL  When lMuda 
				@ 135,10  SAY 	"NFE Serie"			 	SIZE  80,20 of oDlg PIXEL
				@ 132,65  MSGET	cSHISE					SIZE  40,10 Of oDlg PIXEL  When lMuda 
				@ 150,10  SAY 	"NFE Date"			 	SIZE  80,20 of oDlg PIXEL
				@ 147,65  MSGET	dSHDA					SIZE  40,10 Of oDlg PIXEL  When lMuda 
				@ 165,10  SAY 	"NFE CFOP"			 	SIZE  80,20 of oDlg PIXEL
				@ 162,65  MSGET	cSHICF					SIZE  40,10 Of oDlg PIXEL  When lMuda 

				@ 200,142 BUTTON oButton  PROMPT "CANCELA"	SIZE 035, 010 OF oDlg ACTION (lRet:=.F.,oDlg:End()) PIXEL MESSAGE "SAIR"
				@ 200,185 BUTTON oButton  PROMPT "OK"    	SIZE 035, 010 OF oDlg ACTION (lRet:=.T.,oDlg:End()) PIXEL MESSAGE "CONFIRMA"

				ACTIVATE MsDialog oDlg CENTERED
			EndIf
			If lRet
				RecLock("ZBA",.F.)
				ZBA->ZBA_STATUS	:= "shipping_informed"				
				ZBA->ZBA_SHIC	:= cSHIC
				ZBA->ZBA_SHIME	:= cSHIME
				ZBA->ZBA_SHITR	:= cSHITR
				ZBA->ZBA_SHIRA	:= cSHIRA
				ZBA->ZBA_SHIST	:= cSHIST
				ZBA->ZBA_SHIDT	:= dSHIDT
				ZBA->ZBA_SHINF	:= cSHINF
				ZBA->ZBA_SHINL	:= cSHINL
				ZBA->ZBA_SHINU	:= cSHINU
				ZBA->ZBA_SHISE	:= cSHISE
				ZBA->ZBA_SHDA	:= dSHDA
				ZBA->ZBA_SHICF	:= cSHICF
				ZBA->( MsUnLock() )
			EndIf

		Case cStatus == "8"	
			RecLock("ZBA",.F.)
			ZBA->ZBA_STATUS	:= "shipped"
			ZBA->( MsUnLock() )

		Case cStatus == "9"	
			RecLock("ZBA",.F.)
			ZBA->ZBA_STATUS	:= "shipping_error"
			ZBA->( MsUnLock() )

		Case cStatus == "A"	
			If Upper( Alltrim( ZBA->ZBA_STATUS ) ) == 'SHIPPING_INFORMED'
				RecLock("ZBA",.F.)
				ZBA->ZBA_STATUS	:= "delivered"
				ZBA->( MsUnLock() )
			Else
				MsgStop("Atualizacao bloqueada, pedido nao teve entrega informada","Bloqueado")
				lRet := .F.
			EndIf
	EndCase
	If lRet 
		U_HPPED2( ZBA->ZBA_ID, cStatus )		// Atualiza PLUGG.TO
	EndIf
EndIf

Return 

User Function LogZBA()

U_LOGPINT( 'ZBA', ZBA->ZBA_ID)  

Return

User Function CancZBA()
Local lRet := .T.
If Upper( Alltrim( ZBA_STATUS ) ) == 'PENDING'
	RecLock("ZBA",.F.)
	ZBA->ZBA_STATUS	:= "canceled"				
	ZBA->( MsUnLock() )
Else
	MsgStop("Cancelamento bloqueado, veja o status do Pedido","Bloqueado")
	lRet := .F.
EndIf

Return lRet

User Function GeraSC5( _lUnico )
Local lRet	:= .T.
Local cArea	:= GetArea()

PRIVATE cTxtLog := ""
PRIVATE lJob	:= GetRemoteType() == -1

If Empty( ZBA->ZBA_CLISC5 )
	If ExistBlock("GERACLI")
		If empty(ZBA->ZBA_PCPF)
			cCGC:= U_RetiraPT( ZBA->ZBA_CNPJ )			
			cPessoa:= "J"
		else
			cCGC:= U_RetiraPT( ZBA->ZBA_PCPF )			
			cPessoa:= "F"
		endIf
		DbSelectArea("SA1")
		DbSetOrder(3)		// CNPJ / CPF
		If ! DBSEEK( xFilial("SA1") + cCGC )
			lRet := U_GeraCli(cPessoa, _lUnico )
		EndIf
		If lRet
			DbSelectArea("ZBA") 
			Reclock("ZBA",.F.)
			ZBA->ZBA_CLISC5 := SA1->A1_COD
			ZBA->ZBA_LOJSC5 := SA1->A1_LOJA
			ZBA->( MsUnLock() )
		EndIf
	EndIf
EndIf
If lRet
	If ExistBlock("GERACOND")
//		If Empty( ZBA->ZBA_CNDSC5 )
			lRet := U_AchaCond( _lUnico )
			If lRet
				DbSelectArea("ZBA") 
				Reclock("ZBA",.F.)
				ZBA->ZBA_CNDSC5 := SE4->E4_CODIGO		// C5_CONDPAG
				ZBA->( MsUnLock() )
			EndIf
//		EndIf
	EndIf
EndIf
If lRet
	If ExistBlock("GERAPED")
		If Empty( ZBA->ZBA_NUMSC5 )
			lRet := U_GeraPed( _lUnico )
		else
			lRet := .F.
		EndIf
	EndIf
EndIf
If lRet 
	If cEmpAnt <> '99'
		U_LogProc ( "- Criando DAP"	)
		aPedido := {}
		aAdd( aPedido, { SC5->C5_NUM, SA1->A1_COD, SA1->A1_LOJA } )
		U_HFTGER002( aPedido, .T., .F. )	// Teste com o FERRARI
	EndIf
EndIf

RestArea( cArea )

If lRet
	cMsg := "Pedido Criado com Sucesso!"
Else
	cMsg := "Falha na criacao do Pedido!"
EndIf
U_LogProc ( cMsg )
U_LogProc ( "")
U_LogProc ( ".Fim" )
U_LogProc ( "")

If !lJob .and. _lUnico
	Alert( cMsg )
//	U_ShowErrLog("Criacao de Pedido", cTxtLog, .F.)
EndIf

Return( lRet )

User Function ZBAALTEX
If Upper( Alltrim( ZBA_STATUS ) ) == 'PENDING'
	U_ZBAAltera()
Else
	MsgStop("Alteracao bloqueada. VerIfique o STATUS","BLOQUEIO")
EndIf
Return

Static Function Continua( _cStatus )
Local lRet := .T.
If Empty( ZBA->ZBA_NUMSC5 )
	MsgStop("Ainda nao possui numero de pedido interno !")
	lRet := .F.
Else
	DbSelectArea("SC5")
	DbSetOrder(1)
	If DbSeek( ZBA->ZBA_NUMSC5 )
		If Empty( SC5->C5_NOTA )
			MsgStop("Pedido ainda nao possue NOTA FISCAL","Bloqueio")
			lRet := .F.
		Else
			DbSelectArea("SF2")
			DbSetOrder(1)
			If DbSeek( xFilial("SF2") + SC5->( C5_NOTA + C5_SERIE + C5_CLIENTE + C5_LOJACLI) )
				cSHINF	:= SF2->F2_CHVNFE
				cSHINU	:= SF2->F2_DOC
				cSHISE	:= SF2->F2_SERIE
				dSHDA	:= SF2->F2_EMISSAO
			Else
				lRet := .F.
			EndIf
		EndIf
	EndIf
EndIf
Return lRet

Static Function TotPed
Local nRet := 0 

DbSelectArea("ZBB")
DbSetOrder(1)
If DbSeek( ZBA->( ZBA_FILIAL + ZBA_ID ) )
	Do while .not. ZBB->( Eof() ) .AND. ZBB->( ZBB_FILIAL + ZBB_ID ) = ZBA->( ZBA_FILIAL + ZBA_ID )
		nRet += ZBB->( ZBB_QTDVEN * ZBB_PRCVEN )
		ZBB->( DBSKIP() )
	Enddo
EndIf
Return nRet

Static Function VerDIf( _aCabec, _aItens, _lUnico )
Local nRet 			:= 0
Local aDados  		:= {}
//Local aCabec  	:= {}
Local aItens  		:= {}
Local aDIf      	:= {}
Local nX      		:= 0
Local nY      		:= 0
Local cDoc    		:= ""
Local nPedidos		:= 0
Local cStatus		:= ""
Local cCampoTab 	:= ""
Local cCampoJson	:= ""
Local cIdOrder		:= ""
Local cCampo		:= ""
Local nCabec		:= 0
Local cMsg			:=	""

PRIVATE nDIf    	:= 0
PRIVATE aCabec  	:= {}
PRIVATE lMsErroAuto := .F.
PRIVATE nPosID		:= 0


aCabec	:= _aCabec
aItens	:= _aItens

nPos := aScan( aCabec, {|x| Alltrim( x[1]  )== "ZBA_STATUS"} )
If nPos > 0
	cStatus := Alltrim( aCabec[nPos][2] )
EndIf

nPosID := aScan( aCabec, {|x| Alltrim( x[1]  )== "ZBA_ID"} )

If Empty( cStatus )
	cMsg := "Pedido " + aCabec[nPosID][2] + " ignorado !"  + CRLF
Else
	cMsg := "Pedido " + aCabec[nPosID][2] + " encontrado!"  + CRLF
	DbSelectArea("ZBA")
	ZBA->( DbSetOrder(1) )		// Filial + ERP Code
	If ZBA->( DbSeek( xFilial("ZBA") + aCabec[nPosID][2] )  )
		For nCabec := 1 to Len( aCabec )
			cCampo := aCabec[nCabec][1] 
			If ZBA->(FieldPos(cCampo)) > 0
				cContem		:= AjustaCMP(ValType( aCabec[nCabec][2] ), ValType(ZBA->&cCampo), aCabec[nCabec][2]) 
                cCampoTab	:= AjustaCMP(ValType( ZBA->&cCampo ), "C", ZBA->&cCampo	) 
				cCampoJson	:= AjustaCMP(ValType( cContem )		, "C", cContem		) 
				If Alltrim( cCampoTab ) <> Alltrim( cCampoJson )
                    aAdd( aDIf, {aCabec[nPosID][2], { cCampo, aCabec[nCabec][2]}})
                    nDIf++
					cMsg	+= " " + StrZero(nDIf,3) + " >> " + PADR(cCampo,10) + " - Tabela: " + cCampoTab + " - JSON: " + cCampoJson + CRLF
				EndIf
			EndIf
		Next
	EndIf
	If nDIf == 0
		cMsg += "- Sem dIferencas" + CRLF
	Else
		cMsg += "- Foi(ram) encontrado(s) " + Transform( nDIf,"@999,999") + " dIferenca(s)"  + CRLF
	EndIf
//	cMsg += "" + CRLF
EndIf	
If nDIf > 0
	If lJob .or. ! _lUnico
		AtuALL(aDIf)	
	Else
		ShowDIf(cMsg,aDIf)
	EndIf
EndIf

U_LogProc( cMsg )

Return( nRet )

Static Function ShowDIf( _cMsgLog,_aDIf )
Local cPrograma := Substr(ProcName(1),3,Len(ProcName(1)))
Local cMask := "Arquivos Texto (*.TXT) |*.txt|"
Local oFont, oDlg
Local aLog := Array(1)
	// Local cArq := cPrograma + "_" + SM0->M0_CODIGO + SM0->M0_CODFIL + "_" + Dtos(Date()) + "_" + StrTran(Time(),":","") + ".LOG"
Local cArq := cPrograma + "_" + SM0->M0_CODIGO + SM0->M0_CODFIL + "_" + Dtos(dDataBase) + "_" + StrTran(Time(),":","") + ".LOG"
	
Private lError	 := .F.	
Default _cMsgLog :=  ""

nPos := aScan( aCabec, {|x| Alltrim( x[1]  )== "ZBA_STATUS"} )
If nPos > 0
	cStatus := Alltrim( aCabec[nPos][2] )
EndIf

aLog[1] := {_cMsgLog}

DEFINE FONT oFont NAME "Courier New" SIZE 5,0
DEFINE MSDIALOG oDlg TITLE "Atualizacao de Pedido - Comparacao de Campos" From 3,0 to 340,417 COLOR CLR_BLACK,CLR_WHITE PIXEL 
@ 5,5 GET oMemo  VAR _cMsgLog MEMO SIZE 200,145 OF oDlg PIXEL
		oMemo:bRClicked := {||AllwaysTrue()}
		oMemo:oFont := oFont
		oMemo:lReadOnly := .T.

@ 153, 05 BUTTON oButton  PROMPT "Atualiza TUDO"	SIZE 60, 010 OF oDlg ACTION (lRet:=ATUALL(_aDIf),oDlg:End()) 		PIXEL MESSAGE "ATUALIZA TUDO"
@ 153, 76 BUTTON oButton  PROMPT "Atualiza STATUS"  SIZE 60, 010 OF oDlg ACTION (lRet:=U_GrvStat( cStatus ),oDlg:End()) PIXEL MESSAGE "ATUALIZA STATUS" 
@ 153,146 BUTTON oButton  PROMPT "Cancelar"			SIZE 50, 010 OF oDlg ACTION (lRet:=.T.,oDlg:End()) 					PIXEL MESSAGE "NAO FAZ NADA"

ACTIVATE MSDIALOG oDlg CENTERED
	
FERASE(cArq)

Return NIL 

Static Function AtuALL(_aDIf) 
Local lRet		:= .F.
Local nX		:= 0
Local cContem	:= ""
Local cCampo	:= ""

DbSelectArea("ZBA")
Reclock("ZBA",.F.)
for nX := 1 to len(_aDIf)
	cCampo := _aDIf[nX][2][1]
	If ZBA->(FieldPos(cCampo)) > 0
		cContem := AjustaCMP(ValType( _aDIf[nX][2][2] ), ValType(ZBA->&cCampo), _aDIf[nX][2][2]) 
		ZBA->&cCampo := cContem	
	EndIf	
next
ZBA->(MsUnLock())
Return(lRet) 

Static Function AjustaCMP( _cDe, _cPara, _cContem  )
Local xContem := _cContem
If _cDe <> _cPara
	If		_cDe  == "C" .and. _cPara = "N"  
		xContem := Val( _cContem )
	ElseIf		_cDe  == "C" .and. _cPara = "D"  
		xContem := DtoC(_cContem) 
	ElseIf		_cDe  == "N" .and. _cPara = "C"  
		xContem := Str( _cContem,5,2 )
	ElseIf		_cDe  == "D" .and. _cPara = "C"  
		xContem := DtoC( _cContem )
	ElseIf		_cDe  == "N" .and. _cPara = "C"  
		xContem := Str( _cContem )
	Else
		xContem := _cContem
	EndIf   
EndIf   

Return xContem

User Function ImpCorr
Local oOK			:= LoadBitmap(GetResources(),'br_verde')
Local oNO			:= LoadBitmap(GetResources(),'br_vermelho')  
Local cArquivo		:= ""
Local nRetPasta		:= 1
Local cCaminho		:= "C:\Temp\"
Local cTitulo	  	:= "Selecione o arquivo"
Local cExtenso   	:= "Todos os Arquivos | *.csv*"
Local aRet			:= {}
Local aBrowse		:= {}
Local cLinha		:= ""
Local nLin			:= 0
Local lLinha		:= .F.
Local lRet			:= .F.
Local nPed			:= 0

Private lJob	:= GetRemoteType() == -1

// Escolher o arquivo CSV
	nRetPasta	:= GETF_LOCALHARD + GETF_LOCALFLOPPY + GETF_NETWORKDRIVE

	cArquivo	:= cGetFile(cExtenso,cTitulo,0,cCaminho,.T.,nRetPasta,.T.,.T.)

// Carregar a planilha
If Empty( cArquivo )
	return
Endif
nHandle := FT_FUse(cArquivo)
If nHandle = -1
	MsgStop("Problemas na abertura do Arquivo !","Planilha dos Correios")
	return
endIf
nLinTot := FT_FLastRec()

nPos1 := 49	// aScan( aRet, {|x| Upper(x[1]) == "Obs 2"			})
nPos2 := 33	// aScan( aRet, {|x| Upper(x[1]) == "Reg. Correios"	})
nPos3 := 39	// aScan( aRet, {|x| Upper(x[1]) == "Nro NF	"		})
nPos4 := 40	// aScan( aRet, {|x| Upper(x[1]) == "Ser. NF"		})
nPos5 := 11 // aScan( aRet, {|x| Upper(x[1]) == "Dta Evento"		})

ProcRegua(nLinTot)
FT_FGoTop()
While !FT_FEOF()
	cLine := FT_FReadLn()
	If Len(Alltrim(cLine))== 0
		FT_FSKIP()
		Loop
	EndIf
	nRecno := FT_FRecno()
	cLinha := StrTran(cLine,'"',"'")
	cLinha := '{"'+cLinha+'"}'
	// Adiciona o cLinha no array trocando o delimitador ; por , para ser reconhecido como elementos de um array
	cLinha := StrTran(cLinha,';','","')
	aAdd(aRet, &cLinha)
	nLin++
	If nLin == 1 .and. Alltrim(aRet[1][nPos1]) <> "Obs 2"
		aAdd(aErros,{aRet[1][3]+ "  Nao encontrou a coluna do pedido -  Linha ref. "+strzero(nlin,6) })
		lGrava       := .F.
		Exit
	Endif
	IncProc("Processando  Linha "+Alltrim(Str(nLin))+" de "+Alltrim(Str(nLinTot)) )
	If lLinha
		xFILIAL	:= FwXFilial("ZBA")
		cNumPed	:= aRet[1, nPos1]
		If .not. Empty( cNumPed )
			dbSelectArea("ZBA")
			ZBA->(dbSetOrder(4))  // FILIAL + NUMERO PEDIDO
			If ZBA->( dbSeek(xFilial("ZBA") + Padr(cNumPed,40) ) )
				If Alltrim(UPPER(ZBA->( ZBA_STATUS) )) == 'INVOICED'	// Pedido faturado 
					If StrZero( Val(aRet[1,nPos3]) ,9) == Alltrim(ZBA->ZBA_SHINU)
						lGrava := .T.
					Else
						lGrava := .F.
					Endif
				Else                                        
//					aAdd(aErros,{aRet[1][3]+ " Dados j� com T�tulo emitidos, n�o ser� alterados-  Linha ref. "+strzero(nlin,6) })
					lGrava := .F.
				EndIf
			Else
				lGrava := .F.
			EndIf
			dData	:= CtoD(substr(aRet[1][nPos5],1,10))
			cNota	:= StrZero(Val(aRet[1][nPos3]),  8)
			aAdd( aBrowse, {lGrava,aRet[1][nPos1],aRet[1][nPos4],cNota,aRet[1][nPos2],dData} ) 
		EndIf
	Else
		lLinha:= .T.
	EndIf
	// Pula para pr�xima linha
	FT_FSKIP()
	aRet      := {}
Enddo

If Empty( aBrowse )
	MsgStop("Sem dados informados","Planilha dos Correios")
	Return
EndIf

// Mostrar os dados

DEFINE MsDialog oDlg TITLE "Planilha dos Correios" FROM 180,180 TO 550,700 PIXEL	    

oBrowse := TWBrowse():New( 01 , 01, 260,150,,{'','Nr Pedido','Serie','N.Fiscal','Nr Conhecimento','Data Envio'},{20,30,20,30,50,10},;                              
oDlg,,,,,{||},,,,,,,.F.,,.T.,,.F.,,, )    

oBrowse:SetArray(aBrowse)    
oBrowse:bLine := {||{If(aBrowse[oBrowse:nAt,01],oOK,oNO),;
						aBrowse[oBrowse:nAt,02],;                      
						aBrowse[oBrowse:nAt,03],;
						aBrowse[oBrowse:nAt,04],;
						aBrowse[oBrowse:nAt,05],;
						aBrowse[oBrowse:nAt,06]} }    

// Troca a imagem no duplo click do mouse    
oBrowse:bLDblClick := {|| aBrowse[oBrowse:nAt][1] := !aBrowse[oBrowse:nAt][1],;                               
oBrowse:DrawSelect()}  

oButton:=tButton():New(160, 50,'Cancelar' ,oDlg,{||lRet := .F.,oDlg:End()},75,15,,,,.T.)
oButton:=tButton():New(160,150,'Confirmar',oDlg,{||lRet := .T.,oDlg:End()},75,15,,,,.T.)

ACTIVATE MsDialog oDlg CENTERED 

// Salvar os numero de conhecimento
If lRet
	dbSelectArea("ZBA")
	ZBA->(dbSetOrder(4))  // FILIAL + NUMERO PEDIDO
	For nPed := 1 to Len( aBrowse )
		If aBrowse[nPed][ 1 ]
			cNumPed := aBrowse[nPed][ 2 ]
			If ZBA->( dbSeek(xFilial("ZBA") + Padr(cNumPed,40) ) )
				Reclock("ZBA",.F.)
				ZBA->ZBA_STATUS	:= "shipping_informed"				
				ZBA->ZBA_SHITR	:= aBrowse[ nPed ][5]	// Codigo de Rastreio
				ZBA->ZBA_SHIDT	:= aBrowse[ nPed ][6]	// Data de Rastreio
				ZBA->( MsUnLock() )
//				Atualizar o status na PLUGG.TO
				U_HPPED2( ZBA->ZBA_ID, "7" )		// Atualiza PLUGG.TO
			EndIf
		EndIf
	Next
	MsgInfo("Atualizacao concluida!","Planilha dos Correios")
Endif

Return
