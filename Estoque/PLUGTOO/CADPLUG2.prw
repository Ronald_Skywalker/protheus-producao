#include "Protheus.ch"
#include "FileIO.ch"

/*/{Protheus.doc} CADPLUG2

   Consulta Saldo do SKU

	@author 	Milton J. dos Santos
	@since		12/03/2021
	@version	1.0
	@type   	function
/*/

User Function CADPLUG2( )
Local oButton
Local aArea     := GetArea()
Local dData		:= Date()
Local cHora		:= Time()
Local lMuda		:= .F.
Local oOK		:= LoadBitmap(GetResources(),'br_verde')
Local oNO		:= LoadBitmap(GetResources(),'br_vermelho')  
Local aSaldos	:= {}
Local aEstoque	:= {}
Local aPedidos	:= {}
Local nSaldo	:= 0

	aSaldos     :=	U_PegaEstq( ZB9->ZB9_COD )

	aEstoque	:= aSaldos[1]		// Detalhe dos Locais
	aPedidos	:= aSaldos[2]		// Detalhe dos Pedidos
	nSaldo      := aSaldos[3]		// Saldo Final	


	DEFINE MsDialog oDlg TITLE "Consulta de Saldo" FROM 50,50 TO 500,700 PIXEL

	@  03,05  SAY 	"Saldo Atual"	SIZE 80,20 of oDlg PIXEL
   	@  10,05  MSGET	nSaldo			SIZE 80,10 Of oDlg PIXEL When lMuda PICTURE "@E 999,999,999.999999"//X3Picture("ZB9_QATU") 

	@  03,95  SAY 	"Data"	    SIZE 50,20 of oDlg PIXEL
   	@  10,85  MSGET	dData       SIZE 50,10 Of oDlg PIXEL When lMuda 

	@  03,135  SAY 	"Hora"		SIZE 30,20 of oDlg PIXEL
   	@  10,135  MSGET cHora		SIZE 30,10 Of oDlg PIXEL When lMuda 

	oEstoque := TWBrowse():New( 30 , 05, 310, 50,,{'','Local','Descricao','Saldo'},{20,30,200,30},;                              
	oDlg,,,,,{||},,,,,,,.F.,,.T.,,.F.,,, ) 

	oEstoque:SetArray(aEstoque)    
	oEstoque:bLine := {||{If(aEstoque[oEstoque:nAt,01],oOK,oNO),aEstoque[oEstoque:nAt,02],;                      
	aEstoque[oEstoque:nAt,03],aEstoque[oEstoque:nAt,04] } }    

	// Troca a imagem no duplo click do mouse    
	oEstoque:bLDblClick := {|| aEstoque[oEstoque:nAt][1] := !aEstoque[oEstoque:nAt][1],;                               
	oEstoque:DrawSelect()}  

	oPedidos := TWBrowse():New( 90 , 05, 310, 70,,{'','Pedidos','Cliente','QTDE'},{20,30,200,30},;                              
	oDlg,,,,,{||},,,,,,,.F.,,.T.,,.F.,,, )  

	oPedidos:SetArray(aPedidos)    
	oPedidos:bLine := {||{If(aPedidos[oPedidos:nAt,01],oOK,oNO),aPedidos[oPedidos:nAt,02],;                      
	aPedidos[oPedidos:nAt,03],aPedidos[oPedidos:nAt,04] } }    

	// Troca a imagem no duplo click do mouse    
	oPedidos:bLDblClick := {|| aPedidos[oPedidos:nAt][1] := !aPedidos[oPedidos:nAt][1],;                               
	oPedidos:DrawSelect()}  

	@ 213,280 BUTTON oButton  PROMPT "Retornar"    SIZE 040, 010 OF oDlg ACTION (oDlg:End()) PIXEL MESSAGE "OK"

	ACTIVATE MsDialog oDlg CENTERED

RestArea(aArea)

Return

