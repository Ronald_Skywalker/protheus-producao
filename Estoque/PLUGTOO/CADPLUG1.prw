#include "Protheus.ch"
#include "FileIO.ch"

/*/{Protheus.doc} CADPLUG1

   Atualização do Cadastro de SKU

	@author  Milton J. dos Santos
	@since	09/12/2020
	@version	1.0
	@type    function
/*/

Static cFileName := ""
Static nHandle   := -1
Static lShow     := .T.

User Function CADPLUG1( _nOpc)
Local aArea     := GetArea()
Local cTexto	:= ""
Local lRet		:= .T.

Do Case
   Case _nOpc == 1
	    cTexto := "consultar "
   Case _nOpc == 2
    	cTexto := "atualizar "
   Case _nOpc == 4
		cTexto := "atualizar saldo d"
   Case _nOpc == 5
		cTexto := "apagar "
   Case _nOpc == 6
		cTexto := "atualizar tabela de preco d"
	Otherwise
		Return
EndCase

If MsgYesNo("Deseja " + cTexto + "o SKU [" + SB1->B1_COD + "] ?", "Integracao de SKU")
	Processa({|| U_HOPPLUG1(_nOpc) }, "Integração PLUGG.TO", "Integrando SKU de " + SB1->B1_COD + "... ", .F. )
Endif

RestArea(aArea)

Return( lRet )

