#Include "Protheus.ch"

/*
	{Protheus.doc}  MONZB9

	Consulta Produtos do sistema PLUGG.TO 

	@type    function
	@author  Milton J.dos Santos
	@version P12.1.25
	@since   26/01/2021
	@param   nil
	@return  nil 
*/

User Function MONZB9()
Local aButtons := {}
Local aRotAdic := {}

//aAdd( aRotAdic,{ "Saldo em Estoque"	,"U_CADPLUG2()", 0 , 6 })
aAdd( aRotAdic,{ "Historico"				,"U_LOGSKU(ZB9->ZB9_COD)"	, 0 , 6 })
aAdd( aRotAdic,{ "Parametros"				,"U_PLGX000({'001'})"		, 0 , 6 })
aAdd( aRotAdic,{ "Habilitar/Desabilitar"	,"U_HABZB9(ZB9->ZB9_COD)"	, 0 , 6 })
aAdd( aRotAdic,{ "Planilha"					,"U_PLANZB9()"				, 0 , 6 })

aAdd( aButtons, {"NOTE"			,{|| U_CADPLUG2() }, 					"Saldo em Estoque"	})
//aAdd( aButtons, {"HISTORICO"	,{|| U_LOGSKU(ZB9->ZB9_COD)  },"Historico"     	}) 

AxCadastro("ZB9", "Intermediaria Produto PLUGG.TO", , , aRotAdic, , , , , , , aButtons, , )  

Return

User Function HABZB9
Local oButton
Local oDlg
Local oDlg2
Local aArea     := GetArea()
Local dData		:= Date()
Local cHora		:= Time()
Local lMuda		:= .F.
Local oOK		:= LoadBitmap(GetResources(),'br_verde')
Local oNO		:= LoadBitmap(GetResources(),'br_vermelho')  
Local cTitulo   := "Ativacao da Integracao"

Local oPnlTop
Local cProduto		:= ""
Local cDescri		:= ""
Local aIntegra	 	:= {"1=Sim","2=Nao"}
Local cIntegra		:= ZB9->ZB9_INTEGRA

Private cCodPai		:= U_RetSKU( ZB9->ZB9_COD )
Private lHabil		:= ( cIntegra == "1" )
Private nTotal		:= 0
Private aSaldos		:= {}
Private aEstoque	:= {}
Private aPedidos	:= {}
Private aVariacao	:= {}
Private lErros 		:= .F.
Private lAlertas	:= .F.
Private cAlertas	:= ""
Private cErros		:= ""


	LerProd( cCodPai, @cDescri, @lHabil )


	DEFINE MSDIALOG oDlg TITLE cTitulo From 0,0 To 37,108 OF oMainWnd // 35

	@  03,05  SAY 	"Saldo Total"	SIZE 80,20 of oDlg PIXEL
   	@  10,05  MSGET	nTotal			SIZE 80,10 Of oDlg PIXEL When lMuda PICTURE "@E 999,999,999.999999"//X3Picture("ZB9_QATU")

	@  03,85  SAY 	"Produto Pai"	SIZE 80,55 of oDlg PIXEL 
   	@  10,85  MSGET	cCodPai			SIZE 80,10 Of oDlg PIXEL When lMuda 

    @  03,165  SAY 	"Descricao"	    SIZE 80,65 of oDlg PIXEL
   	@  10,165  MSGET cDescri		SIZE 80,10 Of oDlg PIXEL When lMuda PICTURE "@!"  

	@  03,245  SAY 	"Data"	    	SIZE 50,20 of oDlg PIXEL
   	@  10,245  MSGET dData       	SIZE 50,10 Of oDlg PIXEL When lMuda 

	@  03,295  SAY 	"Hora"			SIZE 30,20 of oDlg PIXEL
   	@  10,295  MSGET cHora			SIZE 30,10 Of oDlg PIXEL When lMuda 

	@  02,355 SAY 'Integra ?' OF oDlg PIXEL  
	
    @  09,355 COMBOBOX oCbx VAR cIntegra ITEMS aIntegra SIZE 35,14 PIXEL OF oDlg WHEN lMuda


//	DEFINE DIALOG oDlg2 TITLE "Habilitar Produto PLUGG.TO" FROM 0,0 TO 600,800 PIXEL
 
	@ 30,03 FOLDER oFolder SIZE 420, 215 OF oDlg  PIXEL
 
	oFolder:AddItem("Saldos",.T.)
	oFolder:AddItem("Alertas e Erros",.T.)


//////////////////////////////////////////////////////////////////////////////////////////////


//	Produtos Variacao
	oVariacao := TWBrowse():New( 05 , 04, 415, 55,,{'','Codigo','Descricao','Saldo'},{20,30,200,30},;                              
	oFolder:aDialogs[1],,,,,{||},,,,,,,.F.,,.T.,,.F.,,, ) 

	oVariacao:SetArray(aVariacao)    
	oVariacao:bLine := {||{If(aVariacao[oVariacao:nAt,01],oOK,oNO),aVariacao[oVariacao:nAt,02],;                      
	aVariacao[oVariacao:nAt,03],aVariacao[oVariacao:nAt,04] } }    

	// Troca a imagem no duplo click do mouse    
	oVariacao:bLDblClick := {|| aVariacao[oVariacao:nAt][1] := !aVariacao[oVariacao:nAt][1],;                               
	oVariacao:DrawSelect()}  

//	Estoque
	oEstoque := TWBrowse():New( 70 , 04, 415, 55,,{'','Local','Descricao','Saldo'},{20,30,200,30},;                              
	oFolder:aDialogs[1],,,,,{||},,,,,,,.F.,,.T.,,.F.,,, ) 

	oEstoque:SetArray(aEstoque)    
	oEstoque:bLine := {||{If(aEstoque[oEstoque:nAt,01],oOK,oNO),aEstoque[oEstoque:nAt,02],;                      
	aEstoque[oEstoque:nAt,03],aEstoque[oEstoque:nAt,04] } }    

	// Troca a imagem no duplo click do mouse    
	oEstoque:bLDblClick := {|| aEstoque[oEstoque:nAt][1] := !aEstoque[oEstoque:nAt][1],;                               
	oEstoque:DrawSelect()}  

//	Pedidos
	oPedidos := TWBrowse():New( 135 , 04, 415, 55,,{'','Pedidos','Cliente','QTDE'},{20,30,200,30},;                              
	oFolder:aDialogs[1],,,,,{||},,,,,,,.F.,,.T.,,.F.,,, )  

	oPedidos:SetArray(aPedidos)    
	oPedidos:bLine := {||{If(aPedidos[oPedidos:nAt,01],oOK,oNO),aPedidos[oPedidos:nAt,02],;                      
	aPedidos[oPedidos:nAt,03],aPedidos[oPedidos:nAt,04] } }    

	// Troca a imagem no duplo click do mouse    
	oPedidos:bLDblClick := {|| aPedidos[oPedidos:nAt][1] := !aPedidos[oPedidos:nAt][1],;                               
	oPedidos:DrawSelect()}  

///////////////////////////////////////////////////////////////////////////////////////////////

	@ 05, 10 SAY "Alertas"		SIZE 180,30 OF oFolder:aDialogs[2] PIXEL //+11

	oTMultiget1 := TMultiget():New(20,006,{|u|If(Pcount()>0,cAlertas:=u,cAlertas)},oFolder:aDialogs[2],410,40,,,,,,.T.,,,,,,.T.) //+8

	@ 70, 10 SAY "Erros"			SIZE 180,30 OF oFolder:aDialogs[2] PIXEL //+11
	oTMultiget2 := TMultiget():New(85,006,{|u|If(Pcount()>0,cErros:=u,cErros)},oFolder:aDialogs[2],410,40,,,,,,.T.,,,,,,.T.) //+8

	oButton1   := tButton():New(265,285,'Habilitar'	    ,oDlg,{|| (ConfZB9(),oDlg:End())},030,010,,,,.T.) //+9
	oButton2   := tButton():New(265,340,'Desabilitar'	,oDlg,{|| (ConfZB9(),oDlg:End())},030,010,,,,.T.) //+9
	oButton3   := tButton():New(265,390,'Cancelar'		,oDlg,{|| oDlg:End()},030,010,,,,.T.) //+9

	If lHabil
		oButton1:Disable()
	Else
		oButton2:Disable()
	Endif		

ACTIVATE MSDIALOG oDlg CENTER

Return

Static Function LerProd( _cSku, cDescri, lHabil )
Local cAlias	:= GetArea()
Local nSaldo	:= 0

DbSelectArea("ZB9")
ZB9->( DbSetOrder(1) )
IF ZB9->( DbSeek( xFilial("ZB9") + cCodPai ) )

	If Empty( ZB9->ZB9_DESC2 )
		Mostra(lHabil,"PRODUTO PAI " + ZB9->ZB9_COD + " sem DESCRICAO")
	Else
		cDescri		:= ZB9->ZB9_DESC2
	Endif
	If Empty( ZB9->ZB9_MARCA )
		Mostra(lHabil,"PRODUTO PAI " + ZB9->ZB9_COD + " sem MARCA")
	Endif
	If Empty( ZB9->ZB9_DESC3 )
		Mostra(lHabil,"PRODUTO PAI " + ZB9->ZB9_COD + " sem DESCRITIVO")
	Endif
	If Empty( ZB9->ZB9_CATEG )
		Mostra(lHabil,"PRODUTO PAI " + ZB9->ZB9_COD + " sem CATEGORIA")
	Endif
	If Empty( ZB9->ZB9_PESBRU )
		Mostra(lHabil,"PRODUTO PAI " + ZB9->ZB9_COD + " sem PESO BRUTO")
	Endif
	If Empty( ZB9->ZB9_ALTULC )
		Mostra(lHabil,"PRODUTO PAI " + ZB9->ZB9_COD + " sem ALTURA")
	Endif
	If Empty( ZB9->ZB9_LARGLC )
		Mostra(lHabil,"PRODUTO PAI " + ZB9->ZB9_COD + " sem LARGURA")
	Endif
	If Empty( ZB9->ZB9_COMPLC )
		Mostra(lHabil,"PRODUTO PAI " + ZB9->ZB9_COD + " sem COMPRIMENTO")
	Endif
	If Empty( ZB9->ZB9_IMG1 )
		Mostra(lHabil,"PRODUTO PAI " + ZB9->ZB9_COD + " sem IMAGEM")
	Endif
Else
	cErros	+= Erros("PRODUTO PAI " + cCodPai + " nao encontrado")
Endif
ZB9->( DbSkip() )

aSaldos     := U_PegaEstq( ZB9->ZB9_COD )
aEstoque	:= aSaldos[1]		// Detalhe dos Locais
aPedidos	:= aSaldos[2]		// Detalhe dos Pedidos
nSaldo      := aSaldos[3]		// Saldo Final	
nTotal		:= nSaldo
Do While .not. ZB9->( Eof() ) .and. Substr( ZB9->ZB9_COD,1,8) == Substr( _cSku, 1, 8 )
	aSaldos     := U_PegaEstq( ZB9->ZB9_COD )
	aEstoque	:= aSaldos[1]		// Detalhe dos Locais
	aPedidos	:= aSaldos[2]		// Detalhe dos Pedidos
	nSaldo      := aSaldos[3]		// Saldo Final	
	nTotal		+= nSaldo
	aAdd( aVariacao, { ZB9->ZB9_INTEGR == "1",ZB9->ZB9_COD, ZB9->ZB9_DESC1, nSaldo } )
	If lHabil
		If nSaldo > 0
			cErros	+= Erros("Impeditivo - PRODUTO " + ZB9->ZB9_COD	+ " com saldo")
		Endif
	Else
		If nSaldo <= 0
			cAlertas	+= Alertas("PRODUTO " + ZB9->ZB9_COD 	+ " com saldo")
		Endif
		nNrAtrib := 0
		For nAtrib := 1 to 20
			cCampo1 := "ZB9->ZB9_ATRN" + Alltrim( Str(nAtrib,2) )
			cCampo2 := "ZB9->ZB9_ATRV" + Alltrim( Str(nAtrib,2) )
			If ! Empty( &cCampo1 )
				nNrAtrib++
			Endif
		Next
		If nNrAtrib <= 0
			cErros	+= Erros("Impeditivo - PRODUTO " + ZB9->ZB9_COD	+ " sem atributos")
		Endif
		nNrFoto := 0
		For nPhoto := 1 to 6
			cCampo := "ZB9->ZB9_IMG" + Str(nPhoto,1)
			If ! Empty( &cCampo )
				nNrFoto ++
			Endif
		Next
		If nNrFoto < 4
			cErros	+= Erros("Impeditivo - PRODUTO " + ZB9->ZB9_COD	+ " menos de 4 fotos")
		Endif
	Endif
	ZB9->( DbSkip() )
Enddo
If Len( aVariacao ) == 0
	cErros	+= Erros("Impeditivo - Nao encontrou PRODUTO FILHO")
	aAdd( aVariacao, {.F.,Space(10),Space(100),0})
EndIf
RestArea( cAlias )

Return( cCodPai )

Static Function Mostra( _lErros , _cTexto )
If _lErros
	cAlertas+= Alertas( _cTexto)
Else
	cErros	+= Erros( _cTexto)
Endif
Return

Static Function Alertas( _cTexto )
Local nAlertas	:=  0
Local cMsg		:= _cTexto + CRLF

lAlertas := .T.

Return( cMsg )

Static Function Erros( _cTexto )
Local nERRO 	:=	0
Local cMsg		:= _cTexto + CRLF

lErros := .T.

Return( cMsg )

Static Function ConfZB9()
Local lRet	:= .T.
Local cAlias:= GetArea()
Local cImg1 := ''
Local cImg2 := ''
Local cImg3 := ''
Local cImg4 := ''
Local cImg5 := ''
Local cImg6 := ''	
Local nDimensao  := 10	// Peso, Altura, Largura, Comprimento

If 	lAlertas
	MsgInfo("Encontrou alertas")
Endif

If 	lErros
	MsgStop("Sistema encontrou erros, que impedem a " + iif(lHabil,"desabilitar","habilitar") + " a integracao do produto","BLOQUEIO")
    if lHabil .and. nTotal > 0
		lRet := MsgYesNo("O produto tem saldo. Deseja zerar na PLUGG.TO ?")
		if lRet
//			Quando tem saldo primeiro ZERA saldo da ZB9
			ZeraQATU()		

//			Quando tem saldo em segundo tenta forcar a atualizacao
			lRet := ForcaAtu( .F. )

		Endif
	Else
		lRet := MsgYesNo("Deseja forcar ?")
	Endif
Endif
If lRet
	If lHabil
		cHabilita := "2"
	Else
		cHabilita := "1"
	Endif
	DbSelectArea("ZB9")
	ZB9->( DbSetOrder(1) )
	IF ZB9->( DbSeek( xFilial("ZB9") + cCodPai ) )
		Do While .not. ZB9->( Eof() ) .and. Substr( ZB9->ZB9_COD,1,8) == Substr(cCodPai,1,8) 

			RECLOCK('ZB9',.F.)
			ZB9->ZB9_INTEGRA  := cHabilita
//	Inicio do trecho temporario 
			If cHabilita == "1" .and. Empty( ZB9->ZB9_CODPAI ) 
				If Empty( ZB9->ZB9_IMG1 )
					U_CriaIMG( ZB9->ZB9_COD, @cImg1,@cImg2,@cImg3,@cImg4,@cImg5,@cImg6 )
				Endif
				If Empty(ZB9->ZB9_PESBRU)
					ZB9->ZB9_PESBRU := nDimensao
				EndIf
				If Empty(ZB9->ZB9_ALTULC)
					ZB9->ZB9_ALTULC := nDimensao
				EndIf
				If Empty(ZB9->ZB9_LARGLC)
					ZB9->ZB9_LARGLC := nDimensao
				EndIf
				If Empty(ZB9->ZB9_COMPLC)
					ZB9->ZB9_COMPLC := nDimensao
				EndIf
				If Empty(ZB9->ZB9_IMG1)
					ZB9->ZB9_IMG1 := cImg1
				Endif
				If Empty(ZB9->ZB9_IMG2)
					ZB9->ZB9_IMG2 := cImg2
				Endif
				If Empty(ZB9->ZB9_IMG3)
					ZB9->ZB9_IMG3 := cImg3
				Endif
				If Empty(ZB9->ZB9_IMG4)
					ZB9->ZB9_IMG4 := cImg4
				Endif
				If Empty(ZB9->ZB9_IMG5)
					ZB9->ZB9_IMG5 := cImg5
				Endif
				If Empty(ZB9->ZB9_IMG6)
					ZB9->ZB9_IMG6 := cImg6
				Endif
			Endif
//	Fim do trecho temporario 
			ZB9->(MSUNLOCK())
			ZB9->( DbSkip() )
		Enddo
	Endif
	If cHabilita == "1"
		DbSelectArea("ZB9")
		ZB9->( DbSetOrder(1) )
		IF ZB9->( DbSeek( xFilial("ZB9") + cCodPai ) )
			lRet := ForcaAtu( .T. )
		Endif
	Endif
Endif
RestArea( cAlias )
Return( lRet )

// Logica para zerar o saldo 
Static Function ZeraQATU()
Local aAlias := GetArea()

DbSelectArea("ZB9")
ZB9->( DbSetOrder(1) )
IF ZB9->( DbSeek( xFilial("ZB9") + cCodPai ) )
	Do While .not. ZB9->( Eof() ) .and. Substr( ZB9->ZB9_COD,1,8) == Substr(cCodPai,1,8) 
	    RECLOCK('ZB9',.F.)
	  	ZB9->ZB9_QATU  := 0
		ZB9->(MSUNLOCK())
		ZB9->( DbSkip() )
	Enddo
Endif
RestArea( aAlias )
Return

// Atualizar produto zerado na PLUGG.TO
Static Function ForcaAtu( _lQATU )
Local aArea	:= GetArea()
Local lRet	:= .F.

PRIVATE lJob		:= .F.
PRIVATE cToken		:= U_GetToken( 'SB1', ZB9->ZB9_COD , .T. )
PRIVATE cEndPoint	:= Alltrim( Lower(	SuperGetMv( "HP_PLUGURL"	, .F., "https://api.plugg.to"	) ) )

DbSelectArea("ZB9")
ZB9->( DbSetOrder(1) )
IF ZB9->( DbSeek( xFilial("ZB9") + cCodPai ) )
//	Atualiza Produto		
	lRet := U_HPPRD2( ZB9->ZB9_COD, _lQATU )
Endif
RestArea( aArea )

Return( lRet )

User Function CriaIMG( _cCod,cImg1,cImg2,cImg3,cImg4,cImg5,cImg6 )
Local cAliasQry := GetNextAlias()
Local cAlias	:= GetArea()

cImg1 := ""
cImg2 := ""
cImg3 := ""
cImg4 := ""
cImg5 := ""
cImg6 := ""

cQuery := "SELECT TOP 1 ZB9_IMG1, ZB9_IMG2, ZB9_IMG3, ZB9_IMG4, ZB9_IMG5, ZB9_IMG6 "
cQuery += " FROM "+RetSqlName("ZB9") + " ZB9 "
cQuery += " WHERE ZB9_CODPAI = '" + _cCod + "' AND ZB9_IMG1 <> ' ' AND ZB9.D_E_L_E_T_ = ' ' "

dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAliasQry, .T., .T. )
If !(cAliasQry)->(Eof())
	cImg1 := (cAliasQry)->ZB9_IMG1
	cImg2 := (cAliasQry)->ZB9_IMG2
	cImg3 := (cAliasQry)->ZB9_IMG3
	cImg4 := (cAliasQry)->ZB9_IMG4
	cImg5 := (cAliasQry)->ZB9_IMG5
	cImg6 := (cAliasQry)->ZB9_IMG6	
Endif			
(cAliasQry)->( DBCloseArea() )

RestArea( cAlias )

Return			
