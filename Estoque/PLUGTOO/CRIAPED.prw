#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "TOPCONN.CH"

/*/
	{Protheus.doc} CriaPed

	Atualizacao Condicao de Pagamento

	@author 	Milton J. dos Santos
	@since		02/02/2020
	@version	1.0
	@type function
/*/

User Function CriaPed()

PRIVATE lJob	:= GetRemoteType() == -1 // Verifica se e job

RpcSetEnv("99","01","Admin")

// INICIO - TRECHO TEMPORARIO PARA LIMPAR O PEDIDO
LIMPAPED()
// FIM - TRECHO TEMPORARIO PARA LIMPAR O PEDIDO DE VENDA

DbSelectArea("ZBA")
ZBA->( DbGoTop() )
Do While .NOT. ZBA->( EOF() )
	If Empty( ZBA->ZBA_NUMSC5 )
		If	Upper( Alltrim(TMP->ZBA_STATUS)) <> 'PENDING'
			lRet := U_GeraPed( .F. )
		Endif
	EndIf
	DbSelectArea("ZBA")
	ZBA->( DBSKIP() )
Enddo

RESET ENVIRONMENT

Return

User Function GeraPed( _lUnico )
Local aCabec 	:= {}
Local aItens 	:= {}
Local aLinha 	:= {}
Local nX     	:= 0
Local lRet		:= .T.
Local aAreaSC5	:= SC5->( GetArea() )
Local cNum	 	:= '000001'
Local cC6Tes	:=�""
Local cC6Oper	:= "01"
//Local cVend	:= SuperGetMv("HP_PLUGVND",.F.,"001")
//Local cTabPre	:= Alltrim( 	SuperGetMv( "HP_PLUGTAB"	, .F., "998"	) )
Local cPolCom	:= SuperGetMv("HP_PLUGPCO",.F.,"018")	// 018-POLITICA LOJA VIRTUAL
Local cTpPed	:= SuperGetMv("HP_PLUGLVI",.F.,"143")	// 143-ZZMALL                >> TIPO DE PEDIDO
Local cCanal	:= SuperGetMv("HP_PLUGCNL",.F.,"007")	// 007-E-COMMERCE
Local cPRAZO	:= SuperGetMv("HP_PLUGPRZ",.F.,"001") 	// 001-IMEDIATO
Local aPedido	:= {}

Private lMsErroAuto := .F.

Default _lUnico		:= .T.

//+--------------------------------------------+
//| Abertura do ambiente                       |
//+--------------------------------------------+

//	lRet := RpcSetEnv("99","01","Admin")0
//	PREPARE ENVIRONMENT EMPRESA "99" FILIAL "01" MODULO "FAT" TABLES "SC5","SC6","SA1","SA2","SB1","SB2","SF4"

CfgCanal( ZBA->ZBA_CANAL, @cTpPed )

cNum	:= MT410Num()

aCabec	:= {}  
aItens	:= {}  

DbSelectArea("SA1")
If ! Empty( ZBA->ZBA_CLISC5 )
	DbSetOrder(1)
	If ! DbSeek( xFilial("SA1") + ZBA->ZBA_CLISC5 + ZBA->ZBA_LOJSC5 )
		lRet := .F.
	Endif
Endif
If lRet
	aAdd(aCabec,{"C5_NUM"   	,cNum				,Nil})  
	aAdd(aCabec,{"C5_CLIENTE"	,ZBA->ZBA_CLISC5	,Nil})    
	aAdd(aCabec,{"C5_LOJACLI"	,ZBA->ZBA_LOJSC5	,Nil})  
	aAdd(aCabec,{"C5_POLCOM"	,cPOLCOM		    ,Nil})	// Politica Comercial = 018-POLITICA COMERCIAL 
	aAdd(aCabec,{"C5_TIPO"		,"N"	 			,Nil})  // Tipo N=Normal
	aAdd(aCabec,{"C5_TPPED"		,cTpPed			    ,Nil}) 	// Tipo do Pedido = 016-LOJA VIRTUAL
	aAdd(aCabec,{"C5_XPEDRAK"	,ZBA->ZBA_NUM	    ,Nil})	// Pedido RAKUTEN
	aAdd(aCabec,{"C5_XPEDWEB"	,ZBA->ZBA_NUM	    ,Nil})	// Pedido WEB
	aAdd(aCabec,{"C5_PRAZO"		,cPRAZO			    ,Nil}) 	// Prazo de Entrega = 001-IMEDIATO
	aAdd(aCabec,{"C5_XCANAL"	,cCanal				,Nil}) 	// Canal de Vendas (143-ZZMALL, 132-B2W ou 133-MAGALU)
	aAdd(aCabec,{"C5_TPFRETE"	,"F"				,Nil}) 	// Tipo de Frete >> F=FOB ou C=CIF
	aAdd(aCabec,{"C5_FRETE"		,ZBA->ZBA_FRETAU	,Nil}) 	// Valor do Frete
	aAdd(aCabec,{"C5_CONDPAG"	,ZBA->ZBA_CNDSC5    ,Nil})  // Condicao de Pagamento
	aAdd(aCabec,{"C5_NATUREZ"	,SA1->A1_NATUREZ    ,Nil}) 
	aAdd(aCabec,{"C5_SERIE"	    ,ZBA->ZBA_SERIE     ,Nil})  
	aAdd(aCabec,{"C5_NOTA"	    ,ZBA->ZBA_NOTA      ,Nil})  
	aAdd(aCabec,{"C5_DESCONT"   ,ZBA->ZBA_DESCON    ,Nil})  
	aAdd(aCabec,{"C5_ORIGEM"	,"PLUGGTO"			,Nil})  
	aAdd(aCabec,{"C5_XBLQ"		,"L"				,Nil})		

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSelectArea("ZBB")
	DbSetOrder(1)
	If DbSeek( ZBA->( ZBA_FILIAL + ZBA_ID ) )//SE N�O ENCONTRAR O REGISTRO, VAI TENTAR CONSULTAR DE QUALQUER FORMA, GERANDO ERRORLOG PARA O USU�RIO, AITEM E ALINHAR �PRECISA TER CONTE�DO PARA REALIZAR O EXECAUTO
		Do While .not. ZBB->( EOF() ) .AND. ZBB->( ZBB_FILIAL + ZBB_ID) == ZBA->( ZBA_FILIAL + ZBA_ID )
			nx++
			aLinha	:= {}   		
			cC6Tes	:=�""
			cC6Oper	:= "01"
			DbSelectArea("SB1")
			If DbSeek( ZBB->( xFilial("SB1") + ZBB->ZBB_PRODUT ) )
				cC6Tes	:=�MaTesInt(�2,�cC6Oper,�SA1->A1_COD,�SA1->A1_LOJA,�"C",�SB1->B1_COD,�)
				If Empty( cC6Tes )
					cC6Tes	:=�"501"
				Endif
			Endif
			aAdd(aLinha,{"C6_CLI"		,ZBA->ZBA_CLISC5	,Nil})   
			aAdd(aLinha,{"C6_ITEM"		,StrZero(nx,2)		,Nil})   
			aAdd(aLinha,{"C6_PRODUTO"	,ZBB->ZBB_PRODUT	,Nil})   
			aAdd(aLinha,{"C6_QTDVEN"	,ZBB->ZBB_QTDVEN	,Nil})   
			aAdd(aLinha,{"C6_PRCVEN"	,ZBB->ZBB_PRCVEN	,Nil})   
			aAdd(aLinha,{"C6_PRUNIT"	,ZBB->ZBB_PRCVEN	,Nil})   
			aAdd(aLinha,{"C6_VALOR"		,ZBB->ZBB_VALOR 	,Nil})   
			aAdd(aLinha,{"C6_OPER"		,cC6Oper			,Nil})   
			aAdd(aLinha,{"C6_TES"		,cC6Tes				,Nil})   	// 501
			aAdd(aLinha,{"C6_LOCAL"		,"E0"				,Nil})   

			aAdd(aItens,aLinha)  

			ZBB->( DbSkip() )
		Enddo
		If Empty( aItens )
			lRet := .F.
		Endif
	EndIf
Endif
If lRet
	nOpcX := 3
	MSExecAuto({|a, b, c, d| MATA410(a, b, c, d)}, aCabec, aItens, nOpcX, .F.)
Endif
If lRet
	If lMsErroAuto
		lRet := .F.
		cMsg := "Erro na inclusao!"
		If lJob .or. ! _lUnico
			ConOut( cMsg )
		Else
//			Alert( cMsg )
			MostraErro()
		Endif
	Else
		cMsg := "Pedido no. " + SC5->C5_NUM + " incluido com sucesso! "
		If lJob	.or. ! _lUnico
			ConOut( cMsg )
		Else
			If _lUnico
				MsgInfo( cMsg )
			Endif
		Endif
		DbSelectArea("SC5") 
		RecLock("SC5",.F.)
		SC5->C5_XBLQ := "L"
		SC5->( MsUnLock() )
		DbSelectArea("ZBA") 
		RecLock("ZBA",.F.)
		ZBA->ZBA_STATUS := "waiting_invoice"
		ZBA->ZBA_NUMSC5 := SC5->(C5_FILIAL + C5_NUM)
		ZBA->( MsUnLock() )
	EndIf

	U_LogProc ( cMsg )
Endif

If lRet
	If cEmpAnt <> '99'
		U_LogProc ( "- Criando DAP"	)
		aPedido	:= {}
		aAdd( aPedido, { SC5->C5_NUM, SA1->A1_COD, SA1->A1_LOJA } )
		U_HFTGER002( aPedido, .T., .F. )
	Endif
Endif

RestArea(aAreaSC5)

//	RESET ENVIRONMENT

Return(lRet)

Static Function MT410Num()
Local cProxNum := ""

cProxNum := GETSX8NUM("SC5","C5_NUM")
DbSelectArea("SC5")
DbSetOrder(1)
Do While .T.
	If SC5->( DbSeek( xFilial("SC5")  +cProxNum ) )
		ConfirmSX8()
		cProxNum := GetSXeNum("SC5","C5_NUM")
	Else
		Exit
	Endif
Enddo

Return(cProxNum)

Static Function LIMPAPED
DbSelectArea("SC5")
SC5->( DbGoTop() )
Do While ! EOF()
	RecLock("SC5",.F.)
	DbDelete()
	MsUnLock()
	DBSKIP()
ENDDO
DbSelectArea("SC6")
SC6->( DbGoTop() )
Do While ! EOF()
	RecLock("SC6",.F.)
	DbDelete()
	MsUnLock()
	DBSKIP()
ENDDO
DbSelectArea("ZBA")
ZBA->( DbGoTop() )
Do While ! EOF()
	RecLock("ZBA",.F.)
	ZBA->ZBA_NUMSC5	:= ""
	MsUnLock()
	DBSKIP()
ENDDO
// FIM - TRECHO TEMPORARIO PARA LIMPAR O PEDIDO DE VENDA
RETURN

Static Function CfgCanal( _cCanal, cTpPed )
Do Case
	Case Upper( AllTrim( _cCanal )) == "B2W"
		cTpPed	:= "132"	// 132-B2W                >> TIPO DE PEDIDO
	Case Upper( AllTrim( _cCanal )) == "MAGAZINELUIZA"
		cTpPed	:= "133"	// 133-Magalu             >> TIPO DE PEDIDO
	Case Upper( AllTrim( _cCanal )) == "ZZMALL"
		cTpPed	:= "143"	// 143-ZZMALL             >> TIPO DE PEDIDO
EndCase
Return
