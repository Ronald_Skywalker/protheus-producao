//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de Picking
Rotina para gerar as informa��es de Pickins;

@author Ronaldo Pereira
@since 25 de Janeiro de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL045()

	Local aRet := {}
	Local aPerg := {}

	aAdd(aPerg,{1,"Regi�o: " 	,Space(10),""   ,""		,""	    ,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Rua: "		,Space(10),""	,""		,""		,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Lado: "		,Space(10),""	,""		,""		,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Esta��o: "	,Space(10),""	,""		,""		,""		,0	,.F.}) // Tipo caractere
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 

	 	Processa({|| Geradados(aRet)}, "Exportando dados")
		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Pickings '+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Endere�os E0"
    Local cTable1    	:= "PICKINGS" 

        
    //Dados tabela 1          
    cQry := "SELECT T.ARMAZEM, "
    cQry += "       T.REGIAO, "
    cQry += "       T.RUA, "
    cQry += "       T.LADO, "
    cQry += "       T.ESTACAO, "
    cQry += "       T.ENDERECO, "
    cQry += "       T.PRODUTO, "
    cQry += "       T.QTDE_E0, "
    cQry += "       T.RESERVA, "    
    cQry += "       T.QTDE_E1, "
    cQry += "       T.QTDE_EB, "
	cQry += "       T.QTDE_EC, "
	cQry += "       T.QTDE_IN, "
	cQry += "       T.QTDE_QB, "
	cQry += " 	    T.QTDE_QL, "
	cQry += "   	T.QTDE_CQ, "    	       
    cQry += "       ISNULL(T.SALDO, 0) AS EM_PROCESSO, "
    cQry += "       T.SITUACAO, "
    cQry += "       IIF((T.QTDE_E0 + T.QTDE_E1 + ISNULL(T.SALDO, 0))>0, 'OCUPADO', 'VAZIO') AS STATUS "
    cQry += "FROM "
    cQry += "  (SELECT BE_LOCAL AS ARMAZEM, "
    cQry += "          BE_XREGIAO AS REGIAO, "
    cQry += "          BE_XRUA AS RUA, "
    cQry += "          BE_XLADO AS LADO, "
    cQry += "          BE_XSEQUEN AS SEQUENCIA, "
    cQry += "          BE_XCODEST AS ESTACAO, "
    cQry += "          BE_LOCALIZ AS ENDERECO, "
    cQry += "          BE_CODPRO AS PRODUTO, "
    cQry += "          ISNULL(BF_QUANT, 0) AS QTDE_E0, "
    
    cQry += "          ISNULL((SELECT SUM(BF1.BF_QUANT) "
    cQry += "                    FROM "+RetSqlName("SBF")+" AS BF1 WITH (NOLOCK) "
    cQry += "                    WHERE BF1.D_E_L_E_T_ = '' "
    cQry += "                      AND BF1.BF_LOCAL = 'E1' "
    cQry += "                      AND BF1.BF_PRODUTO = BE_CODPRO "
    cQry += "                    GROUP BY BF1.BF_LOCAL, BF1.BF_PRODUTO),0) AS QTDE_E1, "

    cQry += "          ISNULL((SELECT SUM(BF1.BF_QUANT) "
    cQry += "                    FROM "+RetSqlName("SBF")+" AS BF1 WITH (NOLOCK) "
    cQry += "                    WHERE BF1.D_E_L_E_T_ = '' "
    cQry += "                      AND BF1.BF_LOCAL = 'EB' "
    cQry += "                      AND BF1.BF_PRODUTO = BE_CODPRO "
    cQry += "                    GROUP BY BF1.BF_LOCAL, BF1.BF_PRODUTO),0) AS QTDE_EB, "

    cQry += "          ISNULL((SELECT SUM(BF1.BF_QUANT) "
    cQry += "                    FROM "+RetSqlName("SBF")+" AS BF1 WITH (NOLOCK) "
    cQry += "                    WHERE BF1.D_E_L_E_T_ = '' "
    cQry += "                      AND BF1.BF_LOCAL = 'EC' "
    cQry += "                      AND BF1.BF_PRODUTO = BE_CODPRO "
    cQry += "                    GROUP BY BF1.BF_LOCAL, BF1.BF_PRODUTO),0) AS QTDE_EC, "

    cQry += "          ISNULL((SELECT SUM(BF1.BF_QUANT) "
    cQry += "                    FROM "+RetSqlName("SBF")+" AS BF1 WITH (NOLOCK) "
    cQry += "                    WHERE BF1.D_E_L_E_T_ = '' "
    cQry += "                      AND BF1.BF_LOCAL = 'IN' "
    cQry += "                      AND BF1.BF_PRODUTO = BE_CODPRO "
    cQry += "                    GROUP BY BF1.BF_LOCAL, BF1.BF_PRODUTO),0) AS QTDE_IN, "

    cQry += "          ISNULL((SELECT SUM(BF1.BF_QUANT) "
    cQry += "                    FROM "+RetSqlName("SBF")+" AS BF1 WITH (NOLOCK) "
    cQry += "                    WHERE BF1.D_E_L_E_T_ = '' "
    cQry += "                      AND BF1.BF_LOCAL = 'QB' "
    cQry += "                      AND BF1.BF_PRODUTO = BE_CODPRO "
    cQry += "                    GROUP BY BF1.BF_LOCAL, BF1.BF_PRODUTO),0) AS QTDE_QB, "

    cQry += "          ISNULL((SELECT SUM(BF1.BF_QUANT) "
    cQry += "                    FROM "+RetSqlName("SBF")+" AS BF1 WITH (NOLOCK) "
    cQry += "                    WHERE BF1.D_E_L_E_T_ = '' "
    cQry += "                      AND BF1.BF_LOCAL = 'QL' "
    cQry += "                      AND BF1.BF_PRODUTO = BE_CODPRO "
    cQry += "                    GROUP BY BF1.BF_LOCAL, BF1.BF_PRODUTO),0) AS QTDE_QL, "

    cQry += "          ISNULL((SELECT SUM(BF1.BF_QUANT) "
    cQry += "                    FROM "+RetSqlName("SBF")+" AS BF1 WITH (NOLOCK) "
    cQry += "                    WHERE BF1.D_E_L_E_T_ = '' "
    cQry += "                      AND BF1.BF_LOCAL = 'CQ' "
    cQry += "                      AND BF1.BF_PRODUTO = BE_CODPRO "
    cQry += "                    GROUP BY BF1.BF_LOCAL, BF1.BF_PRODUTO),0) AS QTDE_CQ, "
    
    cQry += "          ISNULL((SELECT SUM(B2.B2_RESERVA + B2.B2_XRESERV) "
    cQry += "                    FROM "+RetSqlName("SB2")+" AS B2 WITH (NOLOCK) "
    cQry += "                    WHERE B2.D_E_L_E_T_ = '' "
    cQry += "                      AND B2.B2_LOCAL = 'E0' "
    cQry += "                      AND B2.B2_COD = BE_CODPRO "
    cQry += "                    GROUP BY B2.B2_LOCAL, B2.B2_COD),0) AS RESERVA, "    
    
    cQry += "     (SELECT SUM(C2_QUANT-C2_QUJE-C2_PERDA) AS SALDO "
    cQry += "   FROM "+RetSqlName("SC2")+" (NOLOCK) "
    cQry += "   WHERE D_E_L_E_T_ = '' "
    cQry += "        AND C2_DATRF = '' "
    cQry += "        AND C2_XRES <> '' "
    cQry += "        AND C2_PRODUTO = BE_CODPRO "
    cQry += "      GROUP BY C2_PRODUTO) AS SALDO, "
    cQry += "      ISNULL(B1.B1_YSITUAC,'') AS SITUACAO "
    
    cQry += "   FROM "+RetSqlName("SBE")+" AS BE WITH (NOLOCK) " 
    cQry += "   LEFT JOIN "+RetSqlName("SB1")+" AS B1 WITH (NOLOCK) ON (B1.B1_COD = BE.BE_CODPRO "
    cQry += "                                            AND B1.D_E_L_E_T_ = '') "   
    cQry += "   LEFT JOIN "+RetSqlName("SBF")+" AS BF WITH (NOLOCK) ON (BE.BE_FILIAL = BF.BF_FILIAL " 
    cQry += "                                            AND BE.BE_LOCAL = BF.BF_LOCAL  "
    cQry += "                                            AND BE.BE_LOCALIZ = BF.BF_LOCALIZ " 
    cQry += "                                            AND BE.BE_CODPRO = BF.BF_PRODUTO " 
    cQry += "                                            AND BF.D_E_L_E_T_ = '') "   
    cQry += "   WHERE BE.D_E_L_E_T_ <> '*' "
    cQry += "     AND BE_STATUS <> '3' "
    cQry += "     AND BE_LOCAL = 'E0' "
    
    If !Empty(aRet[1])
    	cQry += "     AND BE_XREGIAO = '"+Alltrim(aRet[1])+"' "
    EndIf
    
    If !Empty(aRet[2])
    	cQry += "     AND BE_XRUA = '"+Alltrim(aRet[2])+"' "
    EndIf
    
    If !Empty(aRet[3])
    	cQry += "     AND BE_XLADO = '"+Alltrim(aRet[3])+"' "
    EndIf	
    
    If !Empty(aRet[4])
    	cQry += "     AND BE_XCODEST = '"+Alltrim(aRet[4])+"' "
    EndIf
     
    cQry += ") T ORDER BY T.SEQUENCIA "

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Armazem",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Regi�o",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Rua",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Lado",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Esta��o",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Endere�o",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Produto",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Qtde_E0",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Reserva",1) 
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Qtde_E1",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Qtde_EB",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Qtde_EC",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Qtde_IN",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Qtde_QB",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Qtde_QL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Qtde_CQ",1)               
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Em_Processo",1)  
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Situa��o",1)       
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Status",1)
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRY->ARMAZEM,;
                                               QRY->REGIAO,;
                                               QRY->RUA,;
                                               QRY->LADO,;
                                               QRY->ESTACAO,;
                                               QRY->ENDERECO,;
                                               QRY->PRODUTO,;                                               
                                               QRY->QTDE_E0,;
                                               QRY->RESERVA,;
                                               QRY->QTDE_E1,;
                                               QRY->QTDE_EB,;
                                               QRY->QTDE_EC,;
                                               QRY->QTDE_IN,;
                                               QRY->QTDE_QB,;
                                               QRY->QTDE_QL,;
                                               QRY->QTDE_CQ,;
                                               QRY->EM_PROCESSO,;
                                               QRY->SITUACAO,;
                                               QRY->STATUS;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return