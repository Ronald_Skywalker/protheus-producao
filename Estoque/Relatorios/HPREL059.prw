#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

User Function HPREL059()
	Private oReport

	Processa( {|| U_EXPORTGI() }, "Aguarde...", "Processando ...",.F.)

Return( Nil )

User Function EXPORTGI() 

	Local aRet    := {} 
	Local aRet1   := {} 
	Local Campos  := {}
	Local nRegAtu := 0 
	Local x       := 0 
	Local cQuery  := ""

	If Select("_SGI") > 0
			_SGI->(DbCloseArea())
	EndIf
	
	cQuery:= "SELECT GI_FILIAL,GI_ORDEM,GI_PRODORI,GI_PRODALT,GI_TIPOCON,GI_FATOR,GI_MRP,R_E_C_N_O_ "
    cQuery+= "FROM " +RetSQLName("SGI") + " (NOLOCK) where D_E_L_E_T_ = '' "
    
    dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"_SGI",.T.,.T.)


	dbSelectArea("_SGI") 
	aRet1   := Array(Fcount()) 
	ProcRegua(RecCount()*2)
	nRegAtu := 1 
	y=0
	While !Eof() 
		y++
		For x:=1 To Fcount() 

			if y = 1
				aadd(Campos,FIELD(x))
			endif

			aRet1[x] := FieldGet(x) 
		Next 
		Aadd(aRet,aclone(aRet1)) 
		IncProc()

		dbSkip() 
		nRegAtu += 1 
	Enddo 

	MontaXML(Campos,aRet)

	dbSelectArea("_SGI") 
	_SGI->(DbCloseArea()) 

Return ()

Static Function MontaXML(colunas, dados)

	Local oExcel := FWMSEXCEL():New()
    Local x := 0

	oExcel:AddworkSheet("Relato")
	oExcel:AddTable ("Relato","Dados")

	for x := 1 to len(colunas)
		oExcel:AddColumn("Relato","Dados",colunas[x],1,1)
	next x

	for x := 1 to len(dados)
		IncProc()
		oExcel:AddRow("Relato","Dados",dados[x])
	next x

	oExcel:Activate()
	oExcel:GetXMLFile("c:\temp\"+ALLTRIM('SGI')+".xml")

Return
