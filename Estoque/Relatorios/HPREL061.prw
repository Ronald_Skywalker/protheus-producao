//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio Empenho Almoxarifado.
Rotina para gerar todos os Empenhos das OPs da Tecidos e Rendas par analise de atendimento;

@author Ronaldo Pereira
@since 27 de Setembro de 2021
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL061()

	Local aRet  := {}
	Local aPerg := {}
	
	aAdd(aPerg,{1,"Data Empenho de: "  ,dDataBase,PesqPict("SD4", "D4_DATA"),'.T.'	,""		,'.T.'	,50	,.F.}) 
	aAdd(aPerg,{1,"Data Empenho At�: " ,dDataBase,PesqPict("SD4", "D4_DATA"),'.T.'	,""		,'.T.'	,50	,.F.}) 
	aAdd(aPerg,{1,"Produto: " 		   ,Space(15),""   ,""		,""	    ,""		,0	,.F.}) 
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local cQuery    	:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'ANALISE_EMPENHO'+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "BASE OPs"
    Local cTable1    	:= "ALMOX - ANALISE EMPENHO OPS"
    Local cPlan2		:= "BASE ESTOQUE"
    Local cTable2	    := "ALMOX - ANALISE EMPENHO ESTOQUE"
        
    //Tabela 1 - ALMOX - ANALISE EMPENHO OPS    
    cQry := "SELECT T.DATA_EMP, "
    cQry += "     T.PRODUTO, "
    cQry += "     T.DESCRICAO, "
    cQry += "     T.GRUPO, "
    cQry += "     T.OP +' - '+ "
    cQry += "     (SELECT B4.B4_DESC "
    cQry += "         FROM "+RetSqlName("SC2")+" AS C2 WITH (NOLOCK) "
    cQry += "         INNER JOIN "+RetSqlName("SB4")+" AS B4 WITH (NOLOCK) ON (B4.B4_COD = LEFT(C2.C2_PRODUTO, 8) AND B4.D_E_L_E_T_='') "
    cQry += "         WHERE C2.C2_NUM = T.OP "
    cQry += "         AND C2.C2_ITEM = '01' "
    cQry += "         AND C2.C2_SEQUEN = '001' "
    cQry += "         AND C2.D_E_L_E_T_ = '' "
    cQry += "         GROUP BY B4.B4_DESC) AS OP, "
    cQry += "     T.QTDE_OP "
    cQry += " FROM "
    cQry += " (SELECT CONVERT(CHAR, CAST(D4.D4_DATA AS SMALLDATETIME), 103) AS DATA_EMP, "
    cQry += "         D4.D4_COD AS PRODUTO, "
    cQry += "         B1.B1_DESC AS DESCRICAO, "
    cQry += "         B1.B1_GRUPO +' - '+ BM.BM_DESC AS GRUPO, "
    cQry += "         D4_XOPP AS OP, "
    cQry += "         SUM(D4.D4_QTDEORI) AS QTDE_OP "
    cQry += " FROM "+RetSqlName("SD4")+" AS D4 WITH (NOLOCK) "
    cQry += " INNER JOIN "+RetSqlName("SB1")+" AS B1 WITH (NOLOCK) ON (B1.B1_COD = D4.D4_COD "
    cQry += "                                             AND B1.D_E_L_E_T_='') "
    cQry += " INNER JOIN "+RetSqlName("SBM")+" AS BM WITH (NOLOCK) ON (BM.BM_GRUPO = B1.B1_GRUPO "
    cQry += "                                             AND BM.D_E_L_E_T_ = '') "
    cQry += " WHERE D4.D_E_L_E_T_ = '' "
    cQry += "     AND B1.B1_GRUPO IN ('MP01','MP02') "
    cQry += "     AND D4_XOPP <> '' "
    cQry += "     AND D4.D4_DATA BETWEEN '"+ DTOS(aRet[1]) +"' AND '"+ DTOS(aRet[2]) +"' "

    If !Empty(aRet[3])
        cQry += "  AND D4.D4_COD = '"+Alltrim(aRet[3])+"' "
    Endif 

    cQry += " GROUP BY D4.D4_DATA, "
    cQry += "             D4.D4_COD, "
    cQry += "             B1.B1_DESC, "
    cQry += "             D4_XOPP, "
    cQry += "             B1.B1_GRUPO, "
    cQry += "             BM.BM_DESC "
    cQry += " ) T "
    cQry += " ORDER BY T.DATA_EMP,T.PRODUTO "
    
    TCQuery cQry New Alias "QRYOPS"
           
        
    //Tabela 2 - ALMOX - ANALISE EMPENHO ESTOQUE         
    cQuery := " SELECT T.ARMAZEM, "
    cQuery += "     T.PRODUTO, "
    cQuery += "     T.DESCRICAO, "
    cQuery += "     T.UNI_MED, "
    cQuery += "     T.GRUPO, "
    cQuery += "     T.ENDERECO, "
    cQuery += "     T.ESTOQUE "
    cQuery += " FROM "
    cQuery += " (SELECT BF.BF_LOCAL AS ARMAZEM, "
    cQuery += "         D4.D4_COD AS PRODUTO, "
    cQuery += "         B1.B1_DESC AS DESCRICAO, "
    cQuery += "         B1.B1_UM AS UNI_MED, "
    cQuery += "         B1.B1_GRUPO +' - '+ BM.BM_DESC AS GRUPO, "
    cQuery += "         ISNULL(BF.BF_LOCALIZ, '') AS ENDERECO, "
    cQuery += "         ISNULL(BF.BF_QUANT, 0) AS ESTOQUE "
    cQuery += " FROM "+RetSqlName("SD4")+" AS D4 WITH (NOLOCK) "
    cQuery += " INNER JOIN "+RetSqlName("SB1")+" AS B1 WITH (NOLOCK) ON (B1.B1_COD = D4.D4_COD "
    cQuery += "                                             AND B1.D_E_L_E_T_='') "
    cQuery += " INNER JOIN "+RetSqlName("SBM")+" AS BM WITH (NOLOCK) ON (BM.BM_GRUPO = B1.B1_GRUPO "
    cQuery += "                                             AND BM.D_E_L_E_T_ = '') "
    cQuery += " INNER  JOIN "+RetSqlName("SBF")+" AS BF WITH (NOLOCK) ON (BF.BF_FILIAL = '0101' "
    cQuery += "                                             AND BF.BF_PRODUTO = D4.D4_COD "
    cQuery += "                                             AND BF.BF_LOCAL = 'A1' "
    cQuery += "                                             AND BF.BF_QUANT > 0 "
    cQuery += "                                             AND BF.D_E_L_E_T_ = '') "
    cQuery += " WHERE D4.D_E_L_E_T_ = '' "
    cQuery += "     AND B1.B1_GRUPO IN ('MP01','MP02') "
    cQuery += "     AND D4_XOPP <> '' "
    cQuery += "     AND D4.D4_DATA BETWEEN '"+ DTOS(aRet[1]) +"' AND '"+ DTOS(aRet[2]) +"' "

    If !Empty(aRet[3])
        cQuery += " AND D4.D4_COD = '"+Alltrim(aRet[3])+"' "
    Endif 

    cQuery += " GROUP BY BF.BF_LOCAL, "
    cQuery += "             D4.D4_COD, "
    cQuery += "             B1.B1_DESC, "
    cQuery += "             B1.B1_GRUPO, "
    cQuery += "             BM.BM_DESC, "
    cQuery += "             B1.B1_UM, "
    cQuery += "             BF.BF_LOCALIZ, "
    cQuery += "             BF.BF_QUANT "
    cQuery += " ) T "
    cQuery += " ORDER BY T.PRODUTO "																					 
        
    TCQuery cQuery New Alias "QRYEST"
    
    
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)                 //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")                //Fonte utilizada
    //oFWMsExcel:SetBgGeneralColor("#000000")    //Cor de Fundo Geral
    oFWMsExcel:SetTitleBold(.T.)               //T�tulo Negrito
    //oFWMsExcel:SetTitleFrColor("#94eaff")      //Cor da Fonte do t�tulo - Azul Claro
    //oFWMsExcel:SetLineFrColor("#d4d4d4")       //Cor da Fonte da primeira linha - Cinza Claro
    //oFWMsExcel:Set2LineFrColor("#ffffff")      //Cor da Fonte da segunda linha - Branco
     
    //Aba 01 - OPs
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DATA_EMP",1,1) 
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PRODUTO",1,1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCRICAO",1,1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"GRUPO",1,1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"OP",1,1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE_OP",1,1)
        //Criando as Linhas
        While !(QRYOPS->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRYOPS->DATA_EMP,;
                                               QRYOPS->PRODUTO,;
                                               QRYOPS->DESCRICAO,;
                                               QRYOPS->GRUPO,;
                                               QRYOPS->OP,;
                                               QRYOPS->QTDE_OP;
            })
            //Pulando Registro
            QRYOPS->(DbSkip())
        EndDo  


    //Aba 02 - Estoque
    oFWMsExcel:AddworkSheet(cPlan2)
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan2,cTable2)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"ARMAZEM",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"PRODUTO",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"DESCRICAO",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"UNI_MED",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"GRUPO",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"ENDERECO",1)
        oFWMsExcel:AddColumn(cPlan2,cTable2,"ESTOQUE",1)                        
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRYEST->(EoF()))
            oFWMsExcel:AddRow(cPlan2,cTable2,{;
                                               QRYEST->ARMAZEM,;
                                               QRYEST->PRODUTO,;
                                               QRYEST->DESCRICAO,;
                                               QRYEST->UNI_MED,;
                                               QRYEST->GRUPO,;
                                               QRYEST->ENDERECO,;
                                               QRYEST->ESTOQUE;
            })
            
            //Pulando Registro
            QRYEST->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRYOPS->(DbCloseArea()) 
    QRYEST->(DbCloseArea())
    RestArea(aArea)
    
Return
