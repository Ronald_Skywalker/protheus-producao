//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio Rastreamento de Estoque.
Rotina para rastrear o estoque dos produtos;

@author Ronaldo Pereira
@since 18 de Julho de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL055()

	Local aRet := {}
	Local aPerg := {}

	aAdd(aPerg,{1,"Data In�cio"		    ,dDataBase,PesqPict("SD3", "D3_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.})
	aAdd(aPerg,{1,"Data Fim"		    ,dDataBase,PesqPict("SD3", "D3_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.})
	aAdd(aPerg,{1,"Armaz�m Origem: " 	,Space(02),""   ,""		,""	    ,""		,0	,.F.})
	aAdd(aPerg,{1,"Armaz�m Destino: " 	,Space(02),""   ,""		,""	    ,""		,0	,.F.})		 
	aAdd(aPerg,{1,"Produto: " 		    ,Space(15),""   ,""		,""	    ,""		,0	,.F.})
	aAdd(aPerg,{1,"Sub-Lote: " 		    ,Space(06),""   ,""		,""	    ,""		,0	,.F.}) 
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| Geradados(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    	
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'RASTREAMENTO'+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Base"
    Local cTable1    	:= "RASTREAMENTO DE ESTOQUE" 

        
    //Dados tabela 1          
    cQry := " SELECT SD3.D3_FILIAL AS FILIAL, "
    cQry += "        CONVERT(CHAR, CAST(SD3.D3_EMISSAO AS SMALLDATETIME), 103) AS DATA_MOVIMENTO, "
    cQry += "        SDB.DB_HRINI AS HORA, "
    cQry += "        SD30.D3_DOC AS DOCUMENTO, "
	cQry += "    	 SD3.D3_NUMSEQ AS SEQUENCIAL, "
	cQry += " 		 SD3.D3_XLOTDAP AS LOTEDAP, "    
    cQry += "        SB4.B4_TIPO AS TIPO, "
    cQry += "        SD3.D3_COD AS SKU, "
    cQry += "        SB4.B4_DESC AS DESCRICAO, "
    cQry += " 	     SD3.D3_LOTECTL AS LOTE, "
    cQry += " 	     SD3.D3_NUMLOTE AS SUBLOTE, "
    cQry += "        SD3.D3_QUANT AS QUANTIDADE, "
    cQry += "        SD3.D3_LOCAL AS ARMAZEN_ORIGEM, "
    cQry += "        SD3.D3_LOCALIZ AS ENDERECO_ORIGEM, "
    cQry += "        SD30.D3_LOCAL AS ARMAZEN_DESTINO, "
    cQry += "        SD30.D3_LOCALIZ AS ENDERECO_DESTINO, "
    cQry += "        SD3.D3_USUARIO AS USUARIO "
    cQry += " FROM "+RetSqlName("SD3")+" AS SD3 WITH (NOLOCK) "
    cQry += " INNER JOIN "+RetSqlName("SD3")+" AS SD30 WITH (NOLOCK) ON SD30.D3_COD=SD3.D3_COD "
    cQry += " AND SD30.D3_DOC=SD3.D3_DOC "
    cQry += " AND SD30.D3_NUMSEQ=SD3.D3_NUMSEQ "
    cQry += " AND SD30.D3_ESTORNO = '' "
    cQry += " AND SD30.D3_QUANT > 0 "
    cQry += " AND SD30.D_E_L_E_T_= '' "
    cQry += " LEFT JOIN "+RetSqlName("SDB")+" AS SDB WITH (NOLOCK) ON SDB.DB_PRODUTO=SD3.D3_COD "
    cQry += " AND SDB.DB_DOC=SD3.D3_DOC "
    cQry += " AND SDB.DB_NUMSEQ=SD3.D3_NUMSEQ "
    cQry += " AND SDB.DB_ESTORNO = '' "
    cQry += " AND SDB.DB_QUANT > 0 "
    cQry += " AND SDB.DB_LOCAL = SD3.D3_LOCAL "
    cQry += " AND SDB.DB_LOCALIZ = SD3.D3_LOCALIZ "
    cQry += " AND SDB.D_E_L_E_T_= '' "
    cQry += " INNER JOIN "+RetSqlname("SB4")+" AS SB4 WITH (NOLOCK) ON SB4.B4_COD=LEFT(SD3.D3_COD, 8) "
    cQry += " AND SB4.D_E_L_E_T_='' "
    cQry += " INNER JOIN "+RetSqlName("SB1")+" AS SB1 WITH (NOLOCK) ON SB1.B1_COD = SD30.D3_COD "
    cQry += " AND SB1.D_E_L_E_T_ = '' "
    cQry += " WHERE SD3.D_E_L_E_T_='' "
    cQry += "   AND SD3.D3_CF IN ('RE4', "
    cQry += "                     'RE6', "
    cQry += "                     'PR0', "
    cQry += "                     'DE0') "    
    cQry += "   AND SD30.D3_CF IN ('DE4', "
    cQry += "                      'DE6', "
    cQry += "                      'PR0', "
    cQry += "                      'DE0') "    
    cQry += "   AND SD3.D3_ESTORNO = '' "
    cQry += "   AND SD3.D3_QUANT > 0 "
    cQry += "   AND SD3.D3_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"' "

    If !Empty(aRet[3])
    	cQry += "  AND SD3.D3_LOCAL = '"+Alltrim(aRet[3])+"' "
    EndIf

    If !Empty(aRet[4])
    	cQry += "  AND SD30.D3_LOCAL = '"+Alltrim(aRet[4])+"' "
    EndIf
    
    If !Empty(aRet[5])
    	cQry += "  AND SD3.D3_COD LIKE '"+Alltrim(aRet[5])+"%' "
    EndIf
    
    If !Empty(aRet[6])
    	cQry += "  AND SD3.D3_NUMLOTE = '"+Alltrim(aRet[6])+"' "
    EndIf
  
    cQry += " ORDER BY SD3.R_E_C_N_O_ "

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DATA_MOVIMENTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"HORA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DOCUMENTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SEQUENCIAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"LOTEDAP",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SKU",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCRICAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"LOTE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SUBLOTE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QUANTIDADE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ARMAZEN_ORIGEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ENDERECO_ORIGEM",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ARMAZEN_DESTINO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ENDERECO_DESTINO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"USUARIO",1)
        
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;                                             
                                               FILIAL,;
                                               DATA_MOVIMENTO,;
                                               HORA,;
                                               DOCUMENTO,;
                                               SEQUENCIAL,;
                                               LOTEDAP,;
                                               TIPO,;
                                               SKU,;
                                               DESCRICAO,;
                                               LOTE,;
                                               SUBLOTE,;
                                               QUANTIDADE,;
                                               ARMAZEN_ORIGEM,;
                                               ENDERECO_ORIGEM,;
                                               ARMAZEN_DESTINO,;
                                               ENDERECO_DESTINO,;                                             
                                               USUARIO;                                               
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return
