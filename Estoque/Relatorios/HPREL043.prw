//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//Constantes
#Define STR_PULA    Chr(13)+Chr(10)

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de Estoque
Rotina para gerar as informa��es de Endere�os Aereo Dispon�veis;

@author Ronaldo Pereira
@since 11 de Janeiro de 2019
@version V.01
/*/
//_____________________________________________________________________________      

user function HPREL043()
	Private cPerg	:= 'HPREL043'
	
	AjustaSX1(cPerg)
	If !Pergunte(cPerg,.T.)
    	Return
	EndIf
	
	lGera := .T.
    Processa( {|| lGera := GeraDados() }, "Aguarde...", "Processando...",.F.)
    
Static Function Geradados()
    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Endere�os Dispon�veis.xml'
    Local cPlan1		:= "Regi�o1"
    Local cTable1    	:= "Endere�os Aereo Dispon�veis"

        
    //Dados tabela 1          
    cQry := " SELECT R.ARMAZEM, "									                                                                    + STR_PULA
    cQry += "        R.REGIAO, "									                                                                    + STR_PULA
    cQry += "        R.RUA, "									                                                                        + STR_PULA
    cQry += "        R.LADO, "									                                                                        + STR_PULA
    cQry += "        R.ENDERECO, "									                                                                    + STR_PULA
    cQry += "        R.CAPACIDADE, "									                                                                + STR_PULA
    cQry += "        R.ARMAZENADO, "									                                                                + STR_PULA
    cQry += "        CASE "									                                                                            + STR_PULA
    cQry += "            WHEN (R.CAPACIDADE - R.ARMAZENADO) < 0 THEN 0 "									                            + STR_PULA
    cQry += "            ELSE (R.CAPACIDADE - R.ARMAZENADO) "									                                        + STR_PULA
    cQry += "        END DISPONIVEL FROM "									                                                            + STR_PULA
    cQry += "   (SELECT BE_LOCAL AS ARMAZEM, BE_XREGIAO AS REGIAO, BE_XRUA AS RUA, BE_XLADO AS LADO, BE_XSEQUEN AS SEQUENCIA, "    		+ STR_PULA 
    cQry += "           BE_LOCALIZ AS ENDERECO, BE_XQTDREC AS CAPACIDADE, COUNT(BF_LOCALIZ) AS ARMAZENADO "    							+ STR_PULA
    cQry += "    FROM "+RetSqlName("SBE")+" AS BE WITH (NOLOCK) "									                                    + STR_PULA
    cQry += "    LEFT JOIN "+RetSqlName("SBF")+" AS BF WITH (NOLOCK) ON (BF_FILIAL = BE_FILIAL "                                        + STR_PULA
    cQry += "                                          AND BF_LOCAL = BE_LOCAL "                                                        + STR_PULA
    cQry += "                                          AND BF_LOCALIZ = BE_LOCALIZ "                                                   	+ STR_PULA
    cQry += "                                          AND BF_QUANT > 0 "                                                               + STR_PULA
    cQry += "                                          AND BF.D_E_L_E_T_ <> '*') "                                                   	+ STR_PULA    
    cQry += "    WHERE BE.D_E_L_E_T_ <> '*' "                                                                        					+ STR_PULA
    cQry += "      AND BE_STATUS <> '3' "                                                                        						+ STR_PULA    
    cQry += "      AND BE_LOCAL = 'E1' "                                                                        						+ STR_PULA
    cQry += "      AND BE_XREGIAO = '1' "                                                                        						+ STR_PULA
    
    If !Empty(MV_PAR01)
    	cQry += " AND BE_XRUA = '"+MV_PAR01+"' "   																						+ STR_PULA
    EndIf    

    If !Empty(MV_PAR02)
    	cQry += " AND BE_XLADO = '"+MV_PAR02+"' "   																					+ STR_PULA
    EndIf 
       
    cQry += "    GROUP BY BE_LOCAL, BE_XREGIAO, BE_XRUA, BE_XLADO, BE_XSEQUEN, BE_LOCALIZ, BE_XQTDREC) R "                              + STR_PULA
    cQry += " GROUP BY R.ARMAZEM, "                                                                        								+ STR_PULA
    cQry += "          R.REGIAO, "                                                                        								+ STR_PULA
    cQry += "          R.RUA, "                                                                                           				+ STR_PULA
    cQry += "          R.LADO, "                                                                                           				+ STR_PULA
    cQry += "          R.SEQUENCIA, "                                                                                           		+ STR_PULA
    cQry += "          R.ENDERECO, "                                                                                           			+ STR_PULA
    cQry += "          R.CAPACIDADE, "                                                                                           		+ STR_PULA
    cQry += "          R.ARMAZENADO "                                                                                           		+ STR_PULA
    cQry += " HAVING (R.CAPACIDADE - R.ARMAZENADO) > 0 "                                                                                + STR_PULA
    cQry += " ORDER BY R.SEQUENCIA "                                                                                           			+ STR_PULA
            
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)                 //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")                //Fonte utilizada
    //oFWMsExcel:SetBgGeneralColor("#000000")    //Cor de Fundo Geral
    oFWMsExcel:SetTitleBold(.T.)               //T�tulo Negrito
    //oFWMsExcel:SetTitleFrColor("#94eaff")      //Cor da Fonte do t�tulo - Azul Claro
    //oFWMsExcel:SetLineFrColor("#d4d4d4")       //Cor da Fonte da primeira linha - Cinza Claro
    //oFWMsExcel:Set2LineFrColor("#ffffff")      //Cor da Fonte da segunda linha - Branco
     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Armaz�m",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Regi�o",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Rua",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Lado",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Endere�o",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Capacidade",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Armazenado",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"Dispon�vel",1) 
                               
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRY->ARMAZEM,;
                                               QRY->REGIAO,;
                                               QRY->RUA,;
                                               QRY->LADO,;
                                               QRY->ENDERECO,;
                                               QRY->CAPACIDADE,;
                                               QRY->ARMAZENADO,;
                                               QRY->DISPONIVEL;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
    return
    
return

Static Function AjustaSX1(cPerg)
	PutSx1(cPerg, "01","Rua ? "		,""		,""		,"mv_ch1","C",09,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPerg, "02","Lado ? "	,""		,""		,"mv_ch2","C",09,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
return